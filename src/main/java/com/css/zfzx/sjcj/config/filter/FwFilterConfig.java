package com.css.zfzx.sjcj.config.filter;

import com.css.bpm.platform.constants.PlatformConstants;
import com.css.zfzx.sjcj.web.filter.AppFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.servlet.DispatcherType;

/**
 * @author dc
 * @date 2020-05-22
 * @since 1.0.0
 */

@Configuration
@PropertySource(value = "classpath:platform-config.yml")
public class FwFilterConfig {
    @Value("${filter.sessionFilter.ignoreUrlPatterns}")
    private String sessionIgnoreUrlPatterns;
    @Bean
    public FilterRegistrationBean appFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setDispatcherTypes(DispatcherType.REQUEST, DispatcherType.FORWARD, DispatcherType.INCLUDE, DispatcherType.ASYNC);
        registration.setFilter(new AppFilter());
        registration.addUrlPatterns("/app/*");
        registration.setName("appFilter");
        registration.addInitParameter("charset", PlatformConstants.DEFAULT_CHARSET);
        registration.addInitParameter("ignoreUrl",sessionIgnoreUrlPatterns);
        registration.setOrder(50);
        return registration;
    }

}
