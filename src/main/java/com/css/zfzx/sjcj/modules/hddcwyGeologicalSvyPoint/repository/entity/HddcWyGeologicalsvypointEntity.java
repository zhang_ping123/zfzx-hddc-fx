package com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPoint.repository.entity;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-12-01
 */
@Data
@Entity
@Table(name="hddc_wy_geologicalsvypoint")
public class HddcWyGeologicalsvypointEntity implements Serializable {

    /**
     * 是否在图中显示
     */
    @Column(name="isinmap")
    private Integer isinmap;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 观测方法
     */
    @Column(name="svymethods")
    private String svymethods;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 观测目的
     */
    @Column(name="purpose")
    private String purpose;
    /**
     * 观测点描述
     */
    @Column(name="spcomment_info")
    private String spcommentInfo;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 纬度
     */
    @Column(name="lat")
    private Double lat;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 海拔高度 [米]
     */
    @Column(name="elevation")
    private Integer elevation;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 观测点野外编号
     */
    @Column(name="fieldid")
    private String fieldid;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 观测点编号
     */
    //@Id
    @Column(name="id")
    private String id;
    /**
     * 观测点编号
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 典型照片原始文件编号
     */
    @Column(name="photo_arwid")
    private String photoArwid;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 是否地貌点
     */
    @Column(name="isgeomorphpoint")
    private Integer isgeomorphpoint;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 拍摄者
     */
    @Column(name="photographer")
    private String photographer;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 典型照片文件编号
     */
    @Column(name="photo_aiid")
    private String photoAiid;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 地质剖面线编号
     */
    @Column(name="profilesvylineid")
    private String profilesvylineid;
    /**
     * 是否断点
     */
    @Column(name="isfaultpoint")
    private Integer isfaultpoint;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 照片镜向
     */
    @Column(name="photoviewingto")
    private Integer photoviewingto;
    /**
     * 采集样品总数
     */
    @Column(name="collectedsamplecount")
    private Integer collectedsamplecount;
    /**
     * 备注
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 获得测试结果样品数
     */
    @Column(name="datingsamplecount")
    private Integer datingsamplecount;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;
    /**
     * 观测点地名
     */
    @Column(name="locationname")
    private String locationname;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 工程编号
     */
    @Column(name="projectid")
    private String projectid;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 经度
     */
    @Column(name="lon")
    private Double lon;
    /**
     * 送样总数
     */
    @Column(name="samplecount")
    private Integer samplecount;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 观测日期
     */
    @Column(name="svydate")
    private String svydate;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 是否地层点
     */
    @Column(name="isstratigraphypoint")
    private Integer isstratigraphypoint;
    /**
     * 照片镜向字典名称
     */
    @Column(name="photoviewingto_name")
    private String photoviewingtoName;
}

