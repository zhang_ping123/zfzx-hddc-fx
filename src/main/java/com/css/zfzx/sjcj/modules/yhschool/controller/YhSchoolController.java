package com.css.zfzx.sjcj.modules.yhschool.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.zfzx.sjcj.modules.yhschool.repository.entity.YhSchoolEntity;
import com.css.zfzx.sjcj.modules.yhschool.service.YhSchoolService;
import com.css.zfzx.sjcj.modules.yhschool.viewobjects.YhSchoolQueryParams;
import com.css.bpm.platform.utils.PlatformPageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author yyd
 * @date 2020-11-03
 */
@Slf4j
@RestController
@RequestMapping("/yhSchools")
public class YhSchoolController {
    @Autowired
    private YhSchoolService yhSchoolService;

    @GetMapping("/queryYhSchools")
    public RestResponse queryYhSchools(HttpServletRequest request, YhSchoolQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = yhSchoolService.queryYhSchools(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getYhSchool(@PathVariable String id) {
        RestResponse response = null;
        try{
            YhSchoolEntity yhSchool = yhSchoolService.getYhSchool(id);
            response = RestResponse.succeed(yhSchool);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveYhSchool(@RequestBody YhSchoolEntity yhSchool) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            yhSchoolService.saveYhSchool(yhSchool);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateYhSchool(@RequestBody YhSchoolEntity yhSchool)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            yhSchoolService.updateYhSchool(yhSchool);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteYhSchools(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            yhSchoolService.deleteYhSchools(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(yhSchoolService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }

}