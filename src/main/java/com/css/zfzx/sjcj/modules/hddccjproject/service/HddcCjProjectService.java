package com.css.zfzx.sjcj.modules.hddccjproject.service;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.zfzx.sjcj.modules.hddccjproject.repository.entity.HddcCjProjectEntity;
import com.css.zfzx.sjcj.modules.hddccjproject.viewobjects.HddcCjProjectQueryParams;
import com.css.zfzx.sjcj.modules.hddccjproject.viewobjects.HddcCjProjectVO;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import java.util.List;

/**
 * @author zhangping
 * @date 2020-11-26
 */

public interface HddcCjProjectService {

    public JSONObject queryHddcCjProjects(HddcCjProjectQueryParams queryParams, int curPage, int pageSize);

    public HddcCjProjectVO getHddcCjProject(String id);

    public HddcCjProjectEntity saveHddcCjProject(HddcCjProjectEntity hddcCjProject);

    public HddcCjProjectEntity updateHddcCjProject(HddcCjProjectEntity hddcCjProject);

    public void deleteHddcCjProjects(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    List<HddcCjProjectEntity> findAll();

    List<HddcCjProjectEntity> findHddcCjProjectEntityByPersonId(String personid);

    public JSONObject queryHddcCjProjectsByPersonId(String personId, int curPage, int pageSize);
}
