package com.css.zfzx.sjcj.modules.yhchemical.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.zfzx.sjcj.modules.yhchemical.repository.entity.YhChemicalEntity;
import com.css.zfzx.sjcj.modules.yhchemical.service.YhChemicalService;
import com.css.zfzx.sjcj.modules.yhchemical.viewobjects.YhChemicalQueryParams;
import com.css.bpm.platform.utils.PlatformPageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author ly
 * @date 2020-11-04
 */
@Slf4j
@RestController
@RequestMapping("/yhChemicals")
public class YhChemicalController {
    @Autowired
    private YhChemicalService yhChemicalService;

    @GetMapping("/queryYhChemicals")
    public RestResponse queryYhChemicals(HttpServletRequest request, YhChemicalQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = yhChemicalService.queryYhChemicals(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getYhChemical(@PathVariable String id) {
        RestResponse response = null;
        try{
            YhChemicalEntity yhChemical = yhChemicalService.getYhChemical(id);
            response = RestResponse.succeed(yhChemical);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveYhChemical(@RequestBody YhChemicalEntity yhChemical) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            yhChemicalService.saveYhChemical(yhChemical);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateYhChemical(@RequestBody YhChemicalEntity yhChemical)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            yhChemicalService.updateYhChemical(yhChemical);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteYhChemicals(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            yhChemicalService.deleteYhChemicals(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(yhChemicalService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }

}