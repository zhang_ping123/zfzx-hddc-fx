package com.css.zfzx.sjcj.modules.hddcCrustIsoline.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcCrustIsoline.repository.entity.HddcCrustisolineEntity;
import com.css.zfzx.sjcj.modules.hddcCrustIsoline.viewobjects.HddcCrustisolineQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-26
 */

public interface HddcCrustisolineService {

    public JSONObject queryHddcCrustisolines(HddcCrustisolineQueryParams queryParams, int curPage, int pageSize);

    public HddcCrustisolineEntity getHddcCrustisoline(String id);

    public HddcCrustisolineEntity saveHddcCrustisoline(HddcCrustisolineEntity hddcCrustisoline);

    public HddcCrustisolineEntity updateHddcCrustisoline(HddcCrustisolineEntity hddcCrustisoline);

    public void deleteHddcCrustisolines(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcCrustisolineQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
