package com.css.zfzx.sjcj.modules.hddcRock5LinePre.repository;

import com.css.zfzx.sjcj.modules.hddcRock5LinePre.repository.entity.HddcRock5linepreEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcRock5linepreRepository extends JpaRepository<HddcRock5linepreEntity, String> {
}
