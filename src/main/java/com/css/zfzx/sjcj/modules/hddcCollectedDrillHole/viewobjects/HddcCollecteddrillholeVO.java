package com.css.zfzx.sjcj.modules.hddcCollectedDrillHole.viewobjects;

import com.fasterxml.jackson.annotation.JsonFormat;
import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Data
public class HddcCollecteddrillholeVO implements Serializable {

    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 是否开展地球物理测井
     */
    @Excel(name = "是否开展地球物理测井",orderNum = "4")
    private Integer isgeophywell;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 下更新统厚度 [米]
     */
    @Excel(name = "下更新统厚度 [米]",orderNum = "5")
    private Double lowpleithickness;
    /**
     * 省
     */
    @Excel(name = "省",orderNum = "1")
    private String province;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 钻孔柱状图原始档案编号
     */
    @Excel(name = "钻孔柱状图原始档案编号",orderNum = "6")
    private String columnchartArwid;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 孔深
     */
    @Excel(name = "孔深",orderNum = "7")
    private Double depth;
    /**
     * 钻孔柱状图图像文件编号
     */
    @Excel(name = "钻孔柱状图图像文件编号",orderNum = "8")
    private String columnchartAiid;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 孔位经度
     */
    @Excel(name = "孔位经度",orderNum = "9")
    private Double lon;
    /**
     * 全新统厚度 [米]
     */
    @Excel(name = "全新统厚度 [米]",orderNum = "10")
    private Double holocenethickness;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 中更新统厚度 [米]
     */
    @Excel(name = "中更新统厚度 [米]",orderNum = "11")
    private Double midpleithickness;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 孔口标高 [米]
     */
    @Excel(name = "孔口标高 [米]",orderNum = "12")
    private Double elevation;
    /**
     * 岩芯总长 [米]
     */
    @Excel(name = "岩芯总长 [米]",orderNum = "13")
    private Double coretotalthickness;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）",orderNum = "3")
    private String area;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 前第四纪厚度 [米]
     */
    @Excel(name = "前第四纪厚度 [米]",orderNum = "14")
    private Double prepleithickness;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 孔位纬度
     */
    @Excel(name = "孔位纬度",orderNum = "15")
    private Double lat;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 钻孔编号
     */
    private String id;
    /**
     * 上更新统厚度 [米]
     */
    @Excel(name = "上更新统厚度 [米]",orderNum = "16")
    private Double uppleithickness;
    /**
     * 钻探日期
     */
    @Excel(name = "钻探日期",orderNum = "17")
    private String drilldate;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 钻探地点
     */
    @Excel(name = "钻探地点",orderNum = "18")
    private String locationname;
    /**
     * 市
     */
    @Excel(name = "市",orderNum = "2")
    private String city;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 钻孔描述
     */
    @Excel(name = "钻孔描述",orderNum = "19")
    private String commentInfo;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 创建人
     */
    private String createUser;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer isgeophywellName;
}