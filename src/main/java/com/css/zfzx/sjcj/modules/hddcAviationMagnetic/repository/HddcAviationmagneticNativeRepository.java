package com.css.zfzx.sjcj.modules.hddcAviationMagnetic.repository;

import com.css.zfzx.sjcj.modules.hddcAviationMagnetic.repository.entity.HddcAviationmagneticEntity;
import com.css.zfzx.sjcj.modules.hddcAviationMagnetic.viewobjects.HddcAviationmagneticQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-26
 */
public interface HddcAviationmagneticNativeRepository {

    Page<HddcAviationmagneticEntity> queryHddcAviationmagnetics(HddcAviationmagneticQueryParams queryParams, int curPage, int pageSize);

    List<HddcAviationmagneticEntity> exportYhDisasters(HddcAviationmagneticQueryParams queryParams);
}
