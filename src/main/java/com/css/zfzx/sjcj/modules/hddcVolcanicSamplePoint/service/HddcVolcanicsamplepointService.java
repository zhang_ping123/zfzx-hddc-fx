package com.css.zfzx.sjcj.modules.hddcVolcanicSamplePoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcVolcanicSamplePoint.repository.entity.HddcVolcanicsamplepointEntity;
import com.css.zfzx.sjcj.modules.hddcVolcanicSamplePoint.viewobjects.HddcVolcanicsamplepointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-27
 */

public interface HddcVolcanicsamplepointService {

    public JSONObject queryHddcVolcanicsamplepoints(HddcVolcanicsamplepointQueryParams queryParams, int curPage, int pageSize);

    public HddcVolcanicsamplepointEntity getHddcVolcanicsamplepoint(String id);

    public HddcVolcanicsamplepointEntity saveHddcVolcanicsamplepoint(HddcVolcanicsamplepointEntity hddcVolcanicsamplepoint);

    public HddcVolcanicsamplepointEntity updateHddcVolcanicsamplepoint(HddcVolcanicsamplepointEntity hddcVolcanicsamplepoint);

    public void deleteHddcVolcanicsamplepoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcVolcanicsamplepointQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
