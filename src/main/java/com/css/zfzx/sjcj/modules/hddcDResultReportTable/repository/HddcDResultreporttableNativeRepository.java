package com.css.zfzx.sjcj.modules.hddcDResultReportTable.repository;

import com.css.zfzx.sjcj.modules.hddcDResultReportTable.repository.entity.HddcDResultreporttableEntity;
import com.css.zfzx.sjcj.modules.hddcDResultReportTable.viewobjects.HddcDResultreporttableQueryParams;
import org.springframework.data.domain.Page;

/**
 * @author zhangcong
 * @date 2020-12-19
 */
public interface HddcDResultreporttableNativeRepository {

    Page<HddcDResultreporttableEntity> queryHddcDResultreporttables(HddcDResultreporttableQueryParams queryParams, int curPage, int pageSize,String sort,String order);
}
