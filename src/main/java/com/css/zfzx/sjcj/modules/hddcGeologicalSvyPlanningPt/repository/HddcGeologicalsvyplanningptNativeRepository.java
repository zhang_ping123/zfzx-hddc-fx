package com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningPt.repository;

import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningPt.repository.entity.HddcGeologicalsvyplanningptEntity;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningPt.viewobjects.HddcGeologicalsvyplanningptQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-12-07
 */
public interface HddcGeologicalsvyplanningptNativeRepository {

    Page<HddcGeologicalsvyplanningptEntity> queryHddcGeologicalsvyplanningpts(HddcGeologicalsvyplanningptQueryParams queryParams, int curPage, int pageSize);

    List<HddcGeologicalsvyplanningptEntity> exportYhDisasters(HddcGeologicalsvyplanningptQueryParams queryParams);
}
