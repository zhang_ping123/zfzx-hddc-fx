package com.css.zfzx.sjcj.modules.hddcStationhddcStation.repository.entity;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Data
@Entity
@Table(name="hddc_station")
public class HddcStationEntity implements Serializable {

    /**
     * 台站纬度
     */
    @Column(name="stationlatitude")
    private Double stationlatitude;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 起始时间
     */
    @Column(name="starttime")
    private String starttime;
    /**
     * 台站经度
     */
    @Column(name="stationlongitude")
    private Double stationlongitude;
    /**
     * 所在城市
     */
    @Column(name="city")
    private String city;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 仪器南北响应灵敏度 [count*s/μm]
     */
    @Column(name="sensitivityns")
    private Integer sensitivityns;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 仪器类型
     */
    @Column(name="instrument")
    private String instrument;
    /**
     * 测定日期
     */
    @Column(name="testtime")
    private String testtime;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 仪器东西响应灵敏度 [count*s/μm]
     */
    @Column(name="sensitivityew")
    private Integer sensitivityew;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 备注
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 台站类型
     */
    @Column(name="stationtype")
    private String stationtype;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 台基岩性
     */
    @Column(name="lithlogy")
    private String lithlogy;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 台站编码
     */
    //@Id
    @Column(name="id")
    private String id;
    /**
     * 台站编码
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 台站名称
     */
    @Column(name="stationname")
    private String stationname;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 台站海拔高度[米]
     */
    @Column(name="elevation")
    private Double elevation;
    /**
     * 仪器垂直响应灵敏度 [count*s/μm]
     */
    @Column(name="sensitivityv")
    private Integer sensitivityv;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 市(编码需要)
     */
    @Column(name="city_code")
    private String cityCode;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;

}

