package com.css.zfzx.sjcj.modules.hddcStationhddcStation.repository;

import com.css.zfzx.sjcj.modules.hddcStationhddcStation.repository.entity.HddcStationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-28
 */
public interface HddcStationRepository extends JpaRepository<HddcStationEntity, String> {
}
