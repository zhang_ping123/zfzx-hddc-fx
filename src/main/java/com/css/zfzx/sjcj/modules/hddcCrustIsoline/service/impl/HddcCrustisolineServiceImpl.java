package com.css.zfzx.sjcj.modules.hddcCrustIsoline.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcCrustIsoline.repository.HddcCrustisolineNativeRepository;
import com.css.zfzx.sjcj.modules.hddcCrustIsoline.repository.HddcCrustisolineRepository;
import com.css.zfzx.sjcj.modules.hddcCrustIsoline.repository.entity.HddcCrustisolineEntity;
import com.css.zfzx.sjcj.modules.hddcCrustIsoline.service.HddcCrustisolineService;
import com.css.zfzx.sjcj.modules.hddcCrustIsoline.viewobjects.HddcCrustisolineQueryParams;
import com.css.zfzx.sjcj.modules.hddcCrustIsoline.viewobjects.HddcCrustisolineVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zyb
 * @date 2020-11-26
 */
@Service
public class HddcCrustisolineServiceImpl implements HddcCrustisolineService {

	@Autowired
    private HddcCrustisolineRepository hddcCrustisolineRepository;
    @Autowired
    private HddcCrustisolineNativeRepository hddcCrustisolineNativeRepository;

    @Override
    public JSONObject queryHddcCrustisolines(HddcCrustisolineQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcCrustisolineEntity> hddcCrustisolinePage = this.hddcCrustisolineNativeRepository.queryHddcCrustisolines(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcCrustisolinePage);
        return jsonObject;
    }


    @Override
    public HddcCrustisolineEntity getHddcCrustisoline(String id) {
        HddcCrustisolineEntity hddcCrustisoline = this.hddcCrustisolineRepository.findById(id).orElse(null);
         return hddcCrustisoline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcCrustisolineEntity saveHddcCrustisoline(HddcCrustisolineEntity hddcCrustisoline) {
        String uuid = UUIDGenerator.getUUID();
        hddcCrustisoline.setUuid(uuid);
        hddcCrustisoline.setCreateUser(PlatformSessionUtils.getUserId());
        hddcCrustisoline.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcCrustisolineRepository.save(hddcCrustisoline);
        return hddcCrustisoline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcCrustisolineEntity updateHddcCrustisoline(HddcCrustisolineEntity hddcCrustisoline) {
        HddcCrustisolineEntity entity = hddcCrustisolineRepository.findById(hddcCrustisoline.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcCrustisoline);
        hddcCrustisoline.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcCrustisoline.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcCrustisolineRepository.save(hddcCrustisoline);
        return hddcCrustisoline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcCrustisolines(List<String> ids) {
        List<HddcCrustisolineEntity> hddcCrustisolineList = this.hddcCrustisolineRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcCrustisolineList) && hddcCrustisolineList.size() > 0) {
            for(HddcCrustisolineEntity hddcCrustisoline : hddcCrustisolineList) {
                this.hddcCrustisolineRepository.delete(hddcCrustisoline);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcCrustisolineQueryParams queryParams, HttpServletResponse response) {
        List<HddcCrustisolineEntity> yhDisasterEntities = hddcCrustisolineNativeRepository.exportYhDisasters(queryParams);
        List<HddcCrustisolineVO> list=new ArrayList<>();
        for (HddcCrustisolineEntity entity:yhDisasterEntities) {
            HddcCrustisolineVO yhDisasterVO=new HddcCrustisolineVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "地壳厚度-线", "地壳厚度-线", HddcCrustisolineVO.class, "地壳厚度-线.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcCrustisolineVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcCrustisolineVO.class, params);
            List<HddcCrustisolineVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcCrustisolineVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcCrustisolineVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcCrustisolineVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcCrustisolineEntity yhDisasterEntity = new HddcCrustisolineEntity();
            HddcCrustisolineVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcCrustisoline(yhDisasterEntity);
        }
    }

}
