package com.css.zfzx.sjcj.modules.hddcISCatalog.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcISCatalog.repository.entity.HddcIscatalogEntity;
import com.css.zfzx.sjcj.modules.hddcISCatalog.viewobjects.HddcIscatalogQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */

public interface HddcIscatalogService {

    public JSONObject queryHddcIscatalogs(HddcIscatalogQueryParams queryParams, int curPage, int pageSize);

    public HddcIscatalogEntity getHddcIscatalog(String id);

    public HddcIscatalogEntity saveHddcIscatalog(HddcIscatalogEntity hddcIscatalog);

    public HddcIscatalogEntity updateHddcIscatalog(HddcIscatalogEntity hddcIscatalog);

    public void deleteHddcIscatalogs(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcIscatalogQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
