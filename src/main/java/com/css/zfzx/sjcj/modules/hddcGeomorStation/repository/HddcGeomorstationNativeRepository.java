package com.css.zfzx.sjcj.modules.hddcGeomorStation.repository;

import com.css.zfzx.sjcj.modules.hddcGeomorStation.repository.entity.HddcGeomorstationEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorStation.viewobjects.HddcGeomorstationQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author whj
 * @date 2020-11-30
 */
public interface HddcGeomorstationNativeRepository {

    Page<HddcGeomorstationEntity> queryHddcGeomorstations(HddcGeomorstationQueryParams queryParams, int curPage, int pageSize);

    List<HddcGeomorstationEntity> exportYhDisasters(HddcGeomorstationQueryParams queryParams);
}
