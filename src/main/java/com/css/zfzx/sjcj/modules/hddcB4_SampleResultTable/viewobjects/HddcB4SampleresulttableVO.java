package com.css.zfzx.sjcj.modules.hddcB4_SampleResultTable.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Data
public class HddcB4SampleresulttableVO implements Serializable {

    /**
     * 采样点图原始文件编号
     */
    @Excel(name="采样点图原始文件编号",orderNum = "14")
    private String samplelayoutArwid;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 省
     */
    @Excel(name="省",orderNum = "1")
    private String province;
    /**
     * 采样点编号
     */
    @Excel(name="采样点编号",orderNum = "4")
    private String samplepointid;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 区（县）
     */
    @Excel(name="区（县）",orderNum = "3")
    private String area;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 照片集镜向及拍摄者说明文档
     */
    @Excel(name="照片集镜向及拍摄者说明文档",orderNum = "17")
    private String photodescArwid;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 采样深度
     */
    @Excel(name="采样深度",orderNum = "9")
    private Double depth;
    /**
     * 可靠性评估
     */
    @Excel(name="可靠性评估",orderNum = "11")
    private Integer reliability;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 采样照片原始文件编号
     */
    @Excel(name="采样照片原始文件编号",orderNum = "15")
    private String photoArwid;
    /**
     * 采样照片文件编号
     */
    @Excel(name="采样照片文件编号",orderNum = "14")
    private String photoAiid;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 测试结果数据表编号
     */
    @Excel(name="测试结果数据表编号",orderNum = "12")
    private String testeddataArwid;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 记录编号
     */
    private String id;
    /**
     * 采样类型
     */
    @Excel(name="采样类型",orderNum = "7")
    private Integer sampletype;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 样品野外编号
     */
    @Excel(name="样品野外编号",orderNum = "6")
    private String fieldid;
    /**
     * 测年结果[年龄±误差(单位)]
     */
    @Excel(name="测年结果[年龄±误差(单位)]",orderNum = "10")
    private String dateage;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 乡
     */
    private String town;
    /**
     * 市
     */
    @Excel(name="市",orderNum = "2")
    private String city;
    /**
     * 备注（对测年结果说明）
     */
    @Excel(name="备注（对测年结果说明）",orderNum = "18")
    private String commentInfo;
    /**
     * 所属测年工程编号
     */
    @Excel(name="所属测年工程编号",orderNum = "5")
    private String projectnum;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 村
     */
    private String village;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 测试方法
     */
    @Excel(name="测试方法",orderNum = "8")
    private Integer sampletestedmethod;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 采样点图图像文件编号
     */
    @Excel(name="采样点图图像文件编号",orderNum = "13")
    private String samplelayoutAiid;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段1
     */
    private String extends1;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer reliabilityName;
    private Integer sampletypeName;
    private Integer sampletestedmethodName;
    private String rowNum;
    private String errorMsg;

}