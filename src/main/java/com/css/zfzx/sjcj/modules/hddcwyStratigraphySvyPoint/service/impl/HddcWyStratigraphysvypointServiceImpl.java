package com.css.zfzx.sjcj.modules.hddcwyStratigraphySvyPoint.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcStratigraphySvyPoint.repository.HddcStratigraphysvypointRepository;
import com.css.zfzx.sjcj.modules.hddcStratigraphySvyPoint.repository.entity.HddcStratigraphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyStratigraphySvyPoint.repository.HddcWyStratigraphysvypointNativeRepository;
import com.css.zfzx.sjcj.modules.hddcwyStratigraphySvyPoint.repository.HddcWyStratigraphysvypointRepository;
import com.css.zfzx.sjcj.modules.hddcwyStratigraphySvyPoint.repository.entity.HddcWyStratigraphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyStratigraphySvyPoint.service.HddcWyStratigraphysvypointService;
import com.css.zfzx.sjcj.modules.hddcwyStratigraphySvyPoint.viewobjects.HddcWyStratigraphysvypointQueryParams;
import com.css.zfzx.sjcj.modules.hddcwyStratigraphySvyPoint.viewobjects.HddcWyStratigraphysvypointVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author zyb
 * @date 2020-12-01
 */
@Service
@PropertySource(value = "classpath:platform-config.yml")
public class HddcWyStratigraphysvypointServiceImpl implements HddcWyStratigraphysvypointService {

	@Autowired
    private HddcWyStratigraphysvypointRepository hddcWyStratigraphysvypointRepository;
    @Autowired
    private HddcWyStratigraphysvypointNativeRepository hddcWyStratigraphysvypointNativeRepository;
    @Autowired
    private HddcStratigraphysvypointRepository hddcStratigraphysvypointRepository;
    @Value("${image.localDir}")
    private String localDir;
    @Value("${image.urlPre}")
    private String urlPre;
    @Override
    public JSONObject queryHddcWyStratigraphysvypoints(HddcWyStratigraphysvypointQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcWyStratigraphysvypointEntity> hddcWyStratigraphysvypointPage = this.hddcWyStratigraphysvypointNativeRepository.queryHddcWyStratigraphysvypoints(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcWyStratigraphysvypointPage);
        return jsonObject;
    }


    @Override
    public HddcWyStratigraphysvypointEntity getHddcWyStratigraphysvypoint(String uuid) {
        HddcWyStratigraphysvypointEntity hddcWyStratigraphysvypoint = this.hddcWyStratigraphysvypointRepository.findById(uuid).orElse(null);
         return hddcWyStratigraphysvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyStratigraphysvypointEntity saveHddcWyStratigraphysvypoint(HddcWyStratigraphysvypointEntity hddcWyStratigraphysvypoint) {
        String uuid = UUIDGenerator.getUUID();
        hddcWyStratigraphysvypoint.setUuid(uuid);
        if(StringUtils.isEmpty(hddcWyStratigraphysvypoint.getCreateUser())){
            hddcWyStratigraphysvypoint.setCreateUser(PlatformSessionUtils.getUserId());
        }
        hddcWyStratigraphysvypoint.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        hddcWyStratigraphysvypoint.setIsValid("1");

        HddcStratigraphysvypointEntity hddcStratigraphysvypointEntity=new HddcStratigraphysvypointEntity();
        BeanUtils.copyProperties(hddcWyStratigraphysvypoint,hddcStratigraphysvypointEntity);
        hddcStratigraphysvypointEntity.setExtends4(hddcWyStratigraphysvypoint.getTown());
        hddcStratigraphysvypointEntity.setExtends5(String.valueOf(hddcWyStratigraphysvypoint.getLon()));
        hddcStratigraphysvypointEntity.setExtends6(String.valueOf(hddcWyStratigraphysvypoint.getLat()));
        this.hddcStratigraphysvypointRepository.save(hddcStratigraphysvypointEntity);

        this.hddcWyStratigraphysvypointRepository.save(hddcWyStratigraphysvypoint);
        return hddcWyStratigraphysvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyStratigraphysvypointEntity updateHddcWyStratigraphysvypoint(HddcWyStratigraphysvypointEntity hddcWyStratigraphysvypoint) {
        HddcWyStratigraphysvypointEntity entity = hddcWyStratigraphysvypointRepository.findById(hddcWyStratigraphysvypoint.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcWyStratigraphysvypoint);
        hddcWyStratigraphysvypoint.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcWyStratigraphysvypoint.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcWyStratigraphysvypointRepository.save(hddcWyStratigraphysvypoint);
        return hddcWyStratigraphysvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcWyStratigraphysvypoints(List<String> ids) {
        List<HddcWyStratigraphysvypointEntity> hddcWyStratigraphysvypointList = this.hddcWyStratigraphysvypointRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcWyStratigraphysvypointList) && hddcWyStratigraphysvypointList.size() > 0) {
            for(HddcWyStratigraphysvypointEntity hddcWyStratigraphysvypoint : hddcWyStratigraphysvypointList) {
                this.hddcWyStratigraphysvypointRepository.delete(hddcWyStratigraphysvypoint);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public BigInteger queryHddcWyStratigraphysvypoint(HddcAppZztCountVo queryParams) {
        BigInteger bigInteger = hddcWyStratigraphysvypointNativeRepository.queryHddcWyStratigraphysvypoint(queryParams);
        return bigInteger;
    }

    @Override
    public List<HddcWyStratigraphysvypointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid) {
        List<HddcWyStratigraphysvypointEntity> list = hddcWyStratigraphysvypointRepository.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, isValid);
        return list;
    }

    @Override
    public void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response) {
        List<HddcWyStratigraphysvypointEntity> hddcWyStratigraphysvypointEntity = hddcWyStratigraphysvypointNativeRepository.exportStratiPoint(queryParams);
        List<HddcWyStratigraphysvypointVO> list=new ArrayList<>();
        for (HddcWyStratigraphysvypointEntity entity:hddcWyStratigraphysvypointEntity) {
            HddcWyStratigraphysvypointVO hddcWyStratigraphysvypointVO=new HddcWyStratigraphysvypointVO();
            BeanUtils.copyProperties(entity,hddcWyStratigraphysvypointVO);
            list.add(hddcWyStratigraphysvypointVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"地层观测点-点","地层观测点-点", HddcWyStratigraphysvypointVO.class,"地层观测点-点.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcWyStratigraphysvypointVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcWyStratigraphysvypointVO.class, params);
            List<HddcWyStratigraphysvypointVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcWyStratigraphysvypointVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcWyStratigraphysvypointVO hddcWyStratigraphysvypointVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + hddcWyStratigraphysvypointVO.getRowNum() + "行" + hddcWyStratigraphysvypointVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcWyStratigraphysvypointVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcWyStratigraphysvypointEntity hddcWyStratigraphysvypointEntity = new HddcWyStratigraphysvypointEntity();
            HddcWyStratigraphysvypointVO hddcWyStratigraphysvypointVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(hddcWyStratigraphysvypointVO, hddcWyStratigraphysvypointEntity);
            saveHddcWyStratigraphysvypoint(hddcWyStratigraphysvypointEntity);
        }
    }


    @Override
    public List<HddcWyStratigraphysvypointEntity> findAllByCreateUserAndIsValid(String userId,String isValid) {
        List<HddcWyStratigraphysvypointEntity> list = hddcWyStratigraphysvypointRepository.findAllByCreateUserAndIsValid(userId,isValid);
        return list;
    }


    /**
     * 逻辑删除-根据项目id删除数据
     * @param projectIds
     */
    @Override
    public void deleteByProjectId(List<String> projectIds) {
        List<HddcWyStratigraphysvypointEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyStratigraphysvypointRepository.queryHddcA1InvrgnhasmaterialtablesByProjectId(projectIds);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyStratigraphysvypointEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyStratigraphysvypointRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }
    }

    /**
     * 逻辑删除-根据任务id删除数据
     * @param taskId
     */
    @Override
    public void deleteByTaskId(List<String> taskId) {
        List<HddcWyStratigraphysvypointEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyStratigraphysvypointRepository.queryHddcA1InvrgnhasmaterialtablesByTaskId(taskId);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyStratigraphysvypointEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyStratigraphysvypointRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }

    }














}
