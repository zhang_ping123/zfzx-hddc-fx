package com.css.zfzx.sjcj.modules.hddcCrustIsoline.repository;

import com.css.zfzx.sjcj.modules.hddcCrustIsoline.repository.entity.HddcCrustisolineEntity;
import com.css.zfzx.sjcj.modules.hddcCrustIsoline.viewobjects.HddcCrustisolineQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-26
 */
public interface HddcCrustisolineNativeRepository {

    Page<HddcCrustisolineEntity> queryHddcCrustisolines(HddcCrustisolineQueryParams queryParams, int curPage, int pageSize);

    List<HddcCrustisolineEntity> exportYhDisasters(HddcCrustisolineQueryParams queryParams);
}
