package com.css.zfzx.sjcj.modules.hddcA6StrongSeiHasFractBlt.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcA6StrongSeiHasFractBlt.repository.entity.HddcA6StrongseihasfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcA6StrongSeiHasFractBlt.viewobjects.HddcA6StrongseihasfractbltQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */

public interface HddcA6StrongseihasfractbltService {

    public JSONObject queryHddcA6Strongseihasfractblts(HddcA6StrongseihasfractbltQueryParams queryParams, int curPage, int pageSize);

    public HddcA6StrongseihasfractbltEntity getHddcA6Strongseihasfractblt(String id);

    public HddcA6StrongseihasfractbltEntity saveHddcA6Strongseihasfractblt(HddcA6StrongseihasfractbltEntity hddcA6Strongseihasfractblt);

    public HddcA6StrongseihasfractbltEntity updateHddcA6Strongseihasfractblt(HddcA6StrongseihasfractbltEntity hddcA6Strongseihasfractblt);

    public void deleteHddcA6Strongseihasfractblts(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcA6StrongseihasfractbltQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
