package com.css.zfzx.sjcj.modules.hddcSamplePoint.repository;

import com.css.zfzx.sjcj.modules.hddcSamplePoint.repository.entity.HddcSamplepointEntity;
import com.css.zfzx.sjcj.modules.hddcSamplePoint.viewobjects.HddcSamplepointQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
public interface HddcSamplepointNativeRepository {

    Page<HddcSamplepointEntity> queryHddcSamplepoints(HddcSamplepointQueryParams queryParams, int curPage, int pageSize);

    List<HddcSamplepointEntity> exportYhDisasters(HddcSamplepointQueryParams queryParams);
}
