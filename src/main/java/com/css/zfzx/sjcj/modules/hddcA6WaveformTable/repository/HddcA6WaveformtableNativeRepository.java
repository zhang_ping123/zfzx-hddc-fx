package com.css.zfzx.sjcj.modules.hddcA6WaveformTable.repository;

import com.css.zfzx.sjcj.modules.hddcA6WaveformTable.repository.entity.HddcA6WaveformtableEntity;
import com.css.zfzx.sjcj.modules.hddcA6WaveformTable.viewobjects.HddcA6WaveformtableQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */
public interface HddcA6WaveformtableNativeRepository {

    Page<HddcA6WaveformtableEntity> queryHddcA6Waveformtables(HddcA6WaveformtableQueryParams queryParams, int curPage, int pageSize);

    List<HddcA6WaveformtableEntity> exportYhDisasters(HddcA6WaveformtableQueryParams queryParams);
}
