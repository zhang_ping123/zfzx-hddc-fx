package com.css.zfzx.sjcj.modules.hddcwyGeophysvypoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeophysvypoint.repository.entity.HddcWyGeophysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeophysvypoint.viewobjects.HddcWyGeophysvypointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */

public interface HddcWyGeophysvypointService {

    public JSONObject queryHddcWyGeophysvypoints(HddcWyGeophysvypointQueryParams queryParams, int curPage, int pageSize);

    public HddcWyGeophysvypointEntity getHddcWyGeophysvypoint(String uuid);

    public HddcWyGeophysvypointEntity saveHddcWyGeophysvypoint(HddcWyGeophysvypointEntity hddcWyGeophysvypoint);

    public HddcWyGeophysvypointEntity updateHddcWyGeophysvypoint(HddcWyGeophysvypointEntity hddcWyGeophysvypoint);

    public void deleteHddcWyGeophysvypoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    BigInteger queryHddcWyGeophysvypoint(HddcAppZztCountVo queryParams);

    List<HddcWyGeophysvypointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);

    List<HddcWyGeophysvypointEntity> findAllByCreateUserAndIsValid(String userId, String isValid);

    /**
     * 逻辑删除-根据项目id删除数据
     * @param ids
     */
    void deleteByProjectId(List<String> ids);

    /**
     * 逻辑删除-根据任务id删除数据
     * @param ids
     */
    void deleteByTaskId(List<String> ids);
}
