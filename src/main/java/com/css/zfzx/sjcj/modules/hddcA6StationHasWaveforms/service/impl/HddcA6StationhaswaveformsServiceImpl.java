package com.css.zfzx.sjcj.modules.hddcA6StationHasWaveforms.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcA6StationHasWaveforms.repository.HddcA6StationhaswaveformsNativeRepository;
import com.css.zfzx.sjcj.modules.hddcA6StationHasWaveforms.repository.HddcA6StationhaswaveformsRepository;
import com.css.zfzx.sjcj.modules.hddcA6StationHasWaveforms.repository.entity.HddcA6StationhaswaveformsEntity;
import com.css.zfzx.sjcj.modules.hddcA6StationHasWaveforms.service.HddcA6StationhaswaveformsService;
import com.css.zfzx.sjcj.modules.hddcA6StationHasWaveforms.viewobjects.HddcA6StationhaswaveformsQueryParams;
import com.css.zfzx.sjcj.modules.hddcA6StationHasWaveforms.viewobjects.HddcA6StationhaswaveformsVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Service
public class HddcA6StationhaswaveformsServiceImpl implements HddcA6StationhaswaveformsService {

	@Autowired
    private HddcA6StationhaswaveformsRepository hddcA6StationhaswaveformsRepository;
    @Autowired
    private HddcA6StationhaswaveformsNativeRepository hddcA6StationhaswaveformsNativeRepository;

    @Override
    public JSONObject queryHddcA6Stationhaswaveformss(HddcA6StationhaswaveformsQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcA6StationhaswaveformsEntity> hddcA6StationhaswaveformsPage = this.hddcA6StationhaswaveformsNativeRepository.queryHddcA6Stationhaswaveformss(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcA6StationhaswaveformsPage);
        return jsonObject;
    }


    @Override
    public HddcA6StationhaswaveformsEntity getHddcA6Stationhaswaveforms(String id) {
        HddcA6StationhaswaveformsEntity hddcA6Stationhaswaveforms = this.hddcA6StationhaswaveformsRepository.findById(id).orElse(null);
         return hddcA6Stationhaswaveforms;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcA6StationhaswaveformsEntity saveHddcA6Stationhaswaveforms(HddcA6StationhaswaveformsEntity hddcA6Stationhaswaveforms) {
        String uuid = UUIDGenerator.getUUID();
        hddcA6Stationhaswaveforms.setUuid(uuid);
        hddcA6Stationhaswaveforms.setCreateUser(PlatformSessionUtils.getUserId());
        hddcA6Stationhaswaveforms.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcA6StationhaswaveformsRepository.save(hddcA6Stationhaswaveforms);
        return hddcA6Stationhaswaveforms;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcA6StationhaswaveformsEntity updateHddcA6Stationhaswaveforms(HddcA6StationhaswaveformsEntity hddcA6Stationhaswaveforms) {
        HddcA6StationhaswaveformsEntity entity = hddcA6StationhaswaveformsRepository.findById(hddcA6Stationhaswaveforms.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcA6Stationhaswaveforms);
        hddcA6Stationhaswaveforms.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcA6Stationhaswaveforms.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcA6StationhaswaveformsRepository.save(hddcA6Stationhaswaveforms);
        return hddcA6Stationhaswaveforms;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcA6Stationhaswaveformss(List<String> ids) {
        List<HddcA6StationhaswaveformsEntity> hddcA6StationhaswaveformsList = this.hddcA6StationhaswaveformsRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcA6StationhaswaveformsList) && hddcA6StationhaswaveformsList.size() > 0) {
            for(HddcA6StationhaswaveformsEntity hddcA6Stationhaswaveforms : hddcA6StationhaswaveformsList) {
                this.hddcA6StationhaswaveformsRepository.delete(hddcA6Stationhaswaveforms);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcA6StationhaswaveformsQueryParams queryParams, HttpServletResponse response) {
        List<HddcA6StationhaswaveformsEntity> yhDisasterEntities = hddcA6StationhaswaveformsNativeRepository.exportYhDisasters(queryParams);
        List<HddcA6StationhaswaveformsVO> list=new ArrayList<>();
        for (HddcA6StationhaswaveformsEntity entity:yhDisasterEntities) {
            HddcA6StationhaswaveformsVO yhDisasterVO=new HddcA6StationhaswaveformsVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"台站与小震波形数据关联表","台站与小震波形数据关联表",HddcA6StationhaswaveformsVO.class,"台站与小震波形数据关联表.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcA6StationhaswaveformsVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcA6StationhaswaveformsVO.class, params);
            List<HddcA6StationhaswaveformsVO> list = result.getList();
            // Excel条数据
           // int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcA6StationhaswaveformsVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcA6StationhaswaveformsVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcA6StationhaswaveformsVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcA6StationhaswaveformsEntity yhDisasterEntity = new HddcA6StationhaswaveformsEntity();
            HddcA6StationhaswaveformsVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcA6Stationhaswaveforms(yhDisasterEntity);
        }
    }

}
