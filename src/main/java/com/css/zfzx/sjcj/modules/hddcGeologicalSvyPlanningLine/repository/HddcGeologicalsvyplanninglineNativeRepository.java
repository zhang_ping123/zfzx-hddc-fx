package com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningLine.repository;

import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningLine.repository.entity.HddcGeologicalsvyplanninglineEntity;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningLine.viewobjects.HddcGeologicalsvyplanninglineQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-12-07
 */
public interface HddcGeologicalsvyplanninglineNativeRepository {

    Page<HddcGeologicalsvyplanninglineEntity> queryHddcGeologicalsvyplanninglines(HddcGeologicalsvyplanninglineQueryParams queryParams, int curPage, int pageSize);

    List<HddcGeologicalsvyplanninglineEntity> exportYhDisasters(HddcGeologicalsvyplanninglineQueryParams queryParams);
}
