package com.css.zfzx.sjcj.modules.hddcRock25LinePre.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcRock25LinePre.repository.entity.HddcRock25linepreEntity;
import com.css.zfzx.sjcj.modules.hddcRock25LinePre.viewobjects.HddcRock25linepreQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */

public interface HddcRock25linepreService {

    public JSONObject queryHddcRock25linepres(HddcRock25linepreQueryParams queryParams, int curPage, int pageSize);

    public HddcRock25linepreEntity getHddcRock25linepre(String id);

    public HddcRock25linepreEntity saveHddcRock25linepre(HddcRock25linepreEntity hddcRock25linepre);

    public HddcRock25linepreEntity updateHddcRock25linepre(HddcRock25linepreEntity hddcRock25linepre);

    public void deleteHddcRock25linepres(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcRock25linepreQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
