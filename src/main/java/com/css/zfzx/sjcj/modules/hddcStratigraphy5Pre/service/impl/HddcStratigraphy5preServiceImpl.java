package com.css.zfzx.sjcj.modules.hddcStratigraphy5Pre.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltVO;
import com.css.zfzx.sjcj.modules.hddcStratigraphy5Pre.repository.HddcStratigraphy5preNativeRepository;
import com.css.zfzx.sjcj.modules.hddcStratigraphy5Pre.repository.HddcStratigraphy5preRepository;
import com.css.zfzx.sjcj.modules.hddcStratigraphy5Pre.repository.entity.HddcStratigraphy5preEntity;
import com.css.zfzx.sjcj.modules.hddcStratigraphy5Pre.service.HddcStratigraphy5preService;
import com.css.zfzx.sjcj.modules.hddcStratigraphy5Pre.viewobjects.HddcStratigraphy5preQueryParams;
import com.css.zfzx.sjcj.modules.hddcStratigraphy5Pre.viewobjects.HddcStratigraphy5preVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Service
public class HddcStratigraphy5preServiceImpl implements HddcStratigraphy5preService {

	@Autowired
    private HddcStratigraphy5preRepository hddcStratigraphy5preRepository;
    @Autowired
    private HddcStratigraphy5preNativeRepository hddcStratigraphy5preNativeRepository;

    @Override
    public JSONObject queryHddcStratigraphy5pres(HddcStratigraphy5preQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcStratigraphy5preEntity> hddcStratigraphy5prePage = this.hddcStratigraphy5preNativeRepository.queryHddcStratigraphy5pres(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcStratigraphy5prePage);
        return jsonObject;
    }


    @Override
    public HddcStratigraphy5preEntity getHddcStratigraphy5pre(String id) {
        HddcStratigraphy5preEntity hddcStratigraphy5pre = this.hddcStratigraphy5preRepository.findById(id).orElse(null);
         return hddcStratigraphy5pre;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcStratigraphy5preEntity saveHddcStratigraphy5pre(HddcStratigraphy5preEntity hddcStratigraphy5pre) {
        String uuid = UUIDGenerator.getUUID();
        hddcStratigraphy5pre.setUuid(uuid);
        hddcStratigraphy5pre.setCreateUser(PlatformSessionUtils.getUserId());
        hddcStratigraphy5pre.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcStratigraphy5preRepository.save(hddcStratigraphy5pre);
        return hddcStratigraphy5pre;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcStratigraphy5preEntity updateHddcStratigraphy5pre(HddcStratigraphy5preEntity hddcStratigraphy5pre) {
        HddcStratigraphy5preEntity entity = hddcStratigraphy5preRepository.findById(hddcStratigraphy5pre.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcStratigraphy5pre);
        hddcStratigraphy5pre.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcStratigraphy5pre.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcStratigraphy5preRepository.save(hddcStratigraphy5pre);
        return hddcStratigraphy5pre;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcStratigraphy5pres(List<String> ids) {
        List<HddcStratigraphy5preEntity> hddcStratigraphy5preList = this.hddcStratigraphy5preRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcStratigraphy5preList) && hddcStratigraphy5preList.size() > 0) {
            for(HddcStratigraphy5preEntity hddcStratigraphy5pre : hddcStratigraphy5preList) {
                this.hddcStratigraphy5preRepository.delete(hddcStratigraphy5pre);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcStratigraphy5preQueryParams queryParams, HttpServletResponse response) {
        List<HddcStratigraphy5preEntity> yhDisasterEntities = hddcStratigraphy5preNativeRepository.exportYhDisasters(queryParams);
        List<HddcStratigraphy5preVO> list=new ArrayList<>();
        for (HddcStratigraphy5preEntity entity:yhDisasterEntities) {
            HddcStratigraphy5preVO yhDisasterVO=new HddcStratigraphy5preVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"1：5万工作底图地层-面","1：5万工作底图地层-面",HddcStratigraphy5preVO.class,"1：5万工作底图地层-面.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcStratigraphy5preVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcStratigraphy5preVO.class, params);
            List<HddcStratigraphy5preVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcStratigraphy5preVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcStratigraphy5preVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcStratigraphy5preVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcStratigraphy5preEntity yhDisasterEntity = new HddcStratigraphy5preEntity();
            HddcStratigraphy5preVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcStratigraphy5pre(yhDisasterEntity);
        }
    }

}
