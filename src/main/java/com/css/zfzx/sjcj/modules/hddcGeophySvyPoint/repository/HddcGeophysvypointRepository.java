package com.css.zfzx.sjcj.modules.hddcGeophySvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcGeophySvyPoint.repository.entity.HddcGeophysvypointEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
public interface HddcGeophysvypointRepository extends JpaRepository<HddcGeophysvypointEntity, String> {
}
