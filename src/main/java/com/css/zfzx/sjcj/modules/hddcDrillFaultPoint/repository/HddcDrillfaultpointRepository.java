package com.css.zfzx.sjcj.modules.hddcDrillFaultPoint.repository;

import com.css.zfzx.sjcj.modules.hddcDrillFaultPoint.repository.entity.HddcDrillfaultpointEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcDrillfaultpointRepository extends JpaRepository<HddcDrillfaultpointEntity, String> {
}
