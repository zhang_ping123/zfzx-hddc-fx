package com.css.zfzx.sjcj.modules.hddcStratigraphy25Pre.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltVO;
import com.css.zfzx.sjcj.modules.hddcStratigraphy25Pre.repository.HddcStratigraphy25preNativeRepository;
import com.css.zfzx.sjcj.modules.hddcStratigraphy25Pre.repository.HddcStratigraphy25preRepository;
import com.css.zfzx.sjcj.modules.hddcStratigraphy25Pre.repository.entity.HddcStratigraphy25preEntity;
import com.css.zfzx.sjcj.modules.hddcStratigraphy25Pre.service.HddcStratigraphy25preService;
import com.css.zfzx.sjcj.modules.hddcStratigraphy25Pre.viewobjects.HddcStratigraphy25preQueryParams;
import com.css.zfzx.sjcj.modules.hddcStratigraphy25Pre.viewobjects.HddcStratigraphy25preVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Service
public class HddcStratigraphy25preServiceImpl implements HddcStratigraphy25preService {

	@Autowired
    private HddcStratigraphy25preRepository hddcStratigraphy25preRepository;
    @Autowired
    private HddcStratigraphy25preNativeRepository hddcStratigraphy25preNativeRepository;

    @Override
    public JSONObject queryHddcStratigraphy25pres(HddcStratigraphy25preQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcStratigraphy25preEntity> hddcStratigraphy25prePage = this.hddcStratigraphy25preNativeRepository.queryHddcStratigraphy25pres(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcStratigraphy25prePage);
        return jsonObject;
    }


    @Override
    public HddcStratigraphy25preEntity getHddcStratigraphy25pre(String id) {
        HddcStratigraphy25preEntity hddcStratigraphy25pre = this.hddcStratigraphy25preRepository.findById(id).orElse(null);
         return hddcStratigraphy25pre;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcStratigraphy25preEntity saveHddcStratigraphy25pre(HddcStratigraphy25preEntity hddcStratigraphy25pre) {
        String uuid = UUIDGenerator.getUUID();
        hddcStratigraphy25pre.setUuid(uuid);
        hddcStratigraphy25pre.setCreateUser(PlatformSessionUtils.getUserId());
        hddcStratigraphy25pre.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcStratigraphy25preRepository.save(hddcStratigraphy25pre);
        return hddcStratigraphy25pre;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcStratigraphy25preEntity updateHddcStratigraphy25pre(HddcStratigraphy25preEntity hddcStratigraphy25pre) {
        HddcStratigraphy25preEntity entity = hddcStratigraphy25preRepository.findById(hddcStratigraphy25pre.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcStratigraphy25pre);
        hddcStratigraphy25pre.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcStratigraphy25pre.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcStratigraphy25preRepository.save(hddcStratigraphy25pre);
        return hddcStratigraphy25pre;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcStratigraphy25pres(List<String> ids) {
        List<HddcStratigraphy25preEntity> hddcStratigraphy25preList = this.hddcStratigraphy25preRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcStratigraphy25preList) && hddcStratigraphy25preList.size() > 0) {
            for(HddcStratigraphy25preEntity hddcStratigraphy25pre : hddcStratigraphy25preList) {
                this.hddcStratigraphy25preRepository.delete(hddcStratigraphy25pre);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcStratigraphy25preQueryParams queryParams, HttpServletResponse response) {
        List<HddcStratigraphy25preEntity> yhDisasterEntities = hddcStratigraphy25preNativeRepository.exportYhDisasters(queryParams);
        List<HddcStratigraphy25preVO> list=new ArrayList<>();
        for (HddcStratigraphy25preEntity entity:yhDisasterEntities) {
            HddcStratigraphy25preVO yhDisasterVO=new HddcStratigraphy25preVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"1：25万工作底图地层-面","1：25万工作底图地层-面",HddcStratigraphy25preVO.class,"1：25万工作底图地层-面.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcStratigraphy25preVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcStratigraphy25preVO.class, params);
            List<HddcStratigraphy25preVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcStratigraphy25preVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcStratigraphy25preVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcStratigraphy25preVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcStratigraphy25preEntity yhDisasterEntity = new HddcStratigraphy25preEntity();
            HddcStratigraphy25preVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcStratigraphy25pre(yhDisasterEntity);
        }
    }

}
