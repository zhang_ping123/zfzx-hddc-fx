package com.css.zfzx.sjcj.modules.hddcVerticalDeformation.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-26
 */
@Data
public class HddcVerticaldeformationQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

}
