package com.css.zfzx.sjcj.modules.hddcGeochemicalSvyPoint.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Data
public class HddcGeochemicalsvypointQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String labelinfo;

}
