package com.css.zfzx.sjcj.modules.hddcB5_GeophySvyDataTable.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyDataTable.repository.entity.HddcB5GeophysvydatatableEntity;
import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyDataTable.viewobjects.HddcB5GeophysvydatatableQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */

public interface HddcB5GeophysvydatatableService {

    public JSONObject queryHddcB5Geophysvydatatables(HddcB5GeophysvydatatableQueryParams queryParams, int curPage, int pageSize);

    public HddcB5GeophysvydatatableEntity getHddcB5Geophysvydatatable(String id);

    public HddcB5GeophysvydatatableEntity saveHddcB5Geophysvydatatable(HddcB5GeophysvydatatableEntity hddcB5Geophysvydatatable);

    public HddcB5GeophysvydatatableEntity updateHddcB5Geophysvydatatable(HddcB5GeophysvydatatableEntity hddcB5Geophysvydatatable);

    public void deleteHddcB5Geophysvydatatables(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB5GeophysvydatatableQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
