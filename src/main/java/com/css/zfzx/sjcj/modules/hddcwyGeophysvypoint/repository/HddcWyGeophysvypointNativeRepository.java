package com.css.zfzx.sjcj.modules.hddcwyGeophysvypoint.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyGeophysvypoint.repository.entity.HddcWyGeophysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeophysvypoint.viewobjects.HddcWyGeophysvypointQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */
public interface HddcWyGeophysvypointNativeRepository {

    Page<HddcWyGeophysvypointEntity> queryHddcWyGeophysvypoints(HddcWyGeophysvypointQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyGeophysvypoint(HddcAppZztCountVo queryParams);

    List<HddcWyGeophysvypointEntity> exportPhyPoint(HddcAppZztCountVo queryParams);
}
