package com.css.zfzx.sjcj.modules.hddcGeomorphySvySamplePoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvySamplePoint.repository.entity.HddcGeomorphysvysamplepointEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvySamplePoint.viewobjects.HddcGeomorphysvysamplepointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */

public interface HddcGeomorphysvysamplepointService {

    public JSONObject queryHddcGeomorphysvysamplepoints(HddcGeomorphysvysamplepointQueryParams queryParams, int curPage, int pageSize);

    public HddcGeomorphysvysamplepointEntity getHddcGeomorphysvysamplepoint(String id);

    public HddcGeomorphysvysamplepointEntity saveHddcGeomorphysvysamplepoint(HddcGeomorphysvysamplepointEntity hddcGeomorphysvysamplepoint);

    public HddcGeomorphysvysamplepointEntity updateHddcGeomorphysvysamplepoint(HddcGeomorphysvysamplepointEntity hddcGeomorphysvysamplepoint);

    public void deleteHddcGeomorphysvysamplepoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGeomorphysvysamplepointQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
