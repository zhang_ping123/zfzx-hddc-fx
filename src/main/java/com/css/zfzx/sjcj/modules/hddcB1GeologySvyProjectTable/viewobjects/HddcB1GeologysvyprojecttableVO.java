package com.css.zfzx.sjcj.modules.hddcB1GeologySvyProjectTable.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
public class HddcB1GeologysvyprojecttableVO implements Serializable {

    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 工作区编号
     */
    @Excel(name = "工作区编号")
    private String workregionid;
    /**
     * 省
     */
    @Excel(name = "省",orderNum = "1")
    private String province;
    /**
     * 备注
     */
    @Excel(name = "备注")
    private String commentInfo;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 市
     */
    @Excel(name = "市",orderNum = "2")
    private String city;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）",orderNum = "2")
    private String area;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 工程名称
     */
    @Excel(name = "工程名称")
    private String name;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 工程设计报告原始文件编号
     */
    @Excel(name = "工程设计报告原始文件编号")
    private String dbArwid;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 工程编号
     */
    private String id;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 获得测试结果样品总数
     */
    @Excel(name = "获得测试结果样品总数")
    private Integer datingsamplecount;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 成果报告文件报告编号
     */
    @Excel(name = "成果报告文件报告编号")
    private String reportArid;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 变更情况
     */
    @Excel(name = "变更情况")
    private String csArid;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 样品送样总数
     */
    @Excel(name = "样品送样总数")
    private Integer samplecount;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 变更情况原始
     */
    @Excel(name = "变更情况原始")
    private String csArwid;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 地质填图区编号
     */
    @Excel(name = "地质填图区编号")
    private String mainafsregionid;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 地质测量点总数
     */
    @Excel(name = "地质测量点总数")
    private Integer geolosvyptcount;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 目标区编号
     */
    @Excel(name = "目标区编号")
    private String targetregionid;
    /**
     * 备注
     */
    private String remark;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 探测总土方量 [m³]
     */
    @Excel(name = "探测总土方量 [m³]")
    private Double trenchvolume;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 探槽总数
     */
    @Excel(name = "探槽总数")
    private Integer trenchcount;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 工程设计报告编号
     */
    @Excel(name = "工程设计报告编号")
    private String dbArid;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 成果报告原始文件编号
     */
    @Excel(name = "成果报告原始文件编号")
    private String reportArwid;
    /**
     * 采集样品总数
     */
    @Excel(name = "采集样品总数")
    private Integer collectedsamplecount;
    /**
     * 项目ID
     */
    private String projectId;

    private String provinceName;
    private String cityName;
    private String areaName;

    private String rowNum;
    private String errorMsg;
}