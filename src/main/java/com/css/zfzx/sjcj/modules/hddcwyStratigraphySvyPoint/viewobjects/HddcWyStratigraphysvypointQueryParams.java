package com.css.zfzx.sjcj.modules.hddcwyStratigraphySvyPoint.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-12-01
 */
@Data
public class HddcWyStratigraphysvypointQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String stratigraphyname;
    private String startTime;
    private String endTime;


    private String userId;
    private String taskId;
    private String projectId;
    private String isVaild;

}
