package com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
public class HddcWyFaultsvypointVO implements Serializable {

    /**
     * 送样数
     */
    @Excel(name="送样数",orderNum = "29")
    private Integer samplecount;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 比例尺（分母）
     */
    @Excel(name="比例尺（分母）",orderNum = "21")
    private Integer scale;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 是否在图中显示
     */
    @Excel(name="是否在图中显示",orderNum = "26")
    private Integer isinmap;
    /**
     * 水平//张缩位移 [米]
     */
    @Excel(name="水平//张缩位移 [米]",orderNum = "24")
    private Double tensionaldisplacement;
    /**
     * 典型剖面图图像文件编号
     */
    @Excel(name="典型剖面图图像文件编号",orderNum = "12")
    private String typicalprofileAiid;
    /**
     * 断层倾向 [度]
     */
    @Excel(name="断层倾向 [度]",orderNum = "28")
    private Integer measurefaultdip;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 典型剖面图原始文件编号
     */
    @Excel(name="典型剖面图原始文件编号",orderNum = "32")
    private String typicalprofileArwid;
    /**
     * 断层走向 [度]
     */
    @Excel(name="断层走向 [度]",orderNum = "37")
    private Integer faultstrike;
    /**
     * 村
     */
    private String village;
    /**
     * 走向水平位移 [米]
     */
    @Excel(name="走向水平位移 [米]",orderNum = "16")
    private Double horizentaloffset;
    /**
     * 平面图原始文件编号
     */
    @Excel(name="平面图原始文件编号",orderNum = "40")
    private String sketchArwid;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 符号或标注旋转角度
     */
    @Excel(name="符号或标注旋转角度",orderNum = "22")
    private Double lastangle;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 水平/张缩位移误差
     */
    @Excel(name="水平/张缩位移误差",orderNum = "20")
    private Double tensionaldisplacementerror;
    /**
     * 走向水平位移误差
     */
    @Excel(name="走向水平位移误差",orderNum = "31")
    private Double horizentaloffseterror;
    /**
     * 观测点野外编号
     */
    @Excel(name="观测点野外编号",orderNum = "19")
    private String fieldid;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 垂直位移误差
     */
    @Excel(name="垂直位移误差",orderNum = "38")
    private Double verticaldisplacementerror;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 目标断层编号
     */
    @Excel(name="目标断层编号",orderNum = "13")
    private String targetfaultid;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 数据来源
     */
    @Excel(name="数据来源",orderNum = "14")
    private String datasource;
    /**
     * 断层倾角 [度]
     */
    @Excel(name="断层倾角 [度]",orderNum = "17")
    private Integer faultclination;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 典型照片原始文件编号
     */
    @Excel(name="典型照片原始文件编号",orderNum = "35")
    private String photoArwid;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 照片镜向
     */
    @Excel(name="照片镜向",orderNum = "39")
    private Integer photoviewingto;
    /**
     * 获得测试结果样品数
     */
    @Excel(name="获得测试结果样品数",orderNum = "15")
    private Integer datingsamplecount;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备注
     */
    @Excel(name="备注",orderNum = "34")
    private String remark;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 断层性质
     */
    @Excel(name="断层性质",orderNum = "25")
    private Integer feature;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 市
     */
    @Excel(name="市",orderNum = "3")
    private String city;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 目标断层名称
     */
    @Excel(name="目标断层名称",orderNum = "27")
    private String targetfaultname;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 项目名称
     */
    @Excel(name="项目名称",orderNum = "8")
    private String projectName;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 拍摄者
     */
    @Excel(name="拍摄者",orderNum = "18")
    private String photographer;
    /**
     * 目标断层来源
     */
    @Excel(name="目标断层来源",orderNum = "36")
    private String targetfaultsource;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 平面图文件编号
     */
    @Excel(name="平面图文件编号",orderNum = "30")
    private String sketchAiid;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 区（县）
     */
    @Excel(name="区（县）",orderNum = "4")
    private String area;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 任务名称
     */
    @Excel(name="任务名称",orderNum = "9")
    private String taskName;
    /**
     * 备注
     */
    private String commentInfo;
    /**
     * 产状点编号
     */
    @Excel(name="产状点编号",orderNum = "1")
    private String id;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 地质调查观测点编号
     */
    @Excel(name="地质调查观测点编号",orderNum = "10")
    private String geologicalsvypointid;
    /**
     * 典型照片文件编号
     */
    @Excel(name="典型照片文件编号",orderNum = "23")
    private String photoAiid;
    /**
     * 是否修改工作底图
     */
    @Excel(name="是否修改工作底图",orderNum = "11")
    private Integer ismodifyworkmap;
    /**
     * 乡
     */
    @Excel(name = "详细地址", orderNum = "5")
    private String town;
    /**
     * 省
     */
    @Excel(name="省",orderNum = "2")
    private String province;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 垂直位移 [米]
     */
    @Excel(name="垂直位移 [米]",orderNum = "33")
    private Double verticaldisplacement;
    /**
     * 经度
     */
    @Excel(name="经度",orderNum = "6")
    private Double lon;
    /**
     * 维度
     */
    @Excel(name="纬度",orderNum = "7")
    private Double lat;

    private String provinceName;
    private String cityName;
    private String areaName;

    private String rowNum;
    private String errorMsg;
}