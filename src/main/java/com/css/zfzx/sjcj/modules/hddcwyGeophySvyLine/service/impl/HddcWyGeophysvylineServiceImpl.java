package com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcGeophySvyLine.repository.HddcGeophysvylineRepository;
import com.css.zfzx.sjcj.modules.hddcGeophySvyLine.repository.entity.HddcGeophysvylineEntity;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.repository.HddcWyGeophysvylineNativeRepository;
import com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.repository.HddcWyGeophysvylineRepository;
import com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.repository.entity.HddcWyGeophysvylineEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.service.HddcWyGeophysvylineService;
import com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.viewobjects.HddcWyGeophysvylineQueryParams;
import com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.viewobjects.HddcWyGeophysvylineVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zyb
 * @date 2020-12-01
 */
@Service
public class HddcWyGeophysvylineServiceImpl implements HddcWyGeophysvylineService {

	@Autowired
    private HddcWyGeophysvylineRepository hddcWyGeophysvylineRepository;
    @Autowired
    private HddcWyGeophysvylineNativeRepository hddcWyGeophysvylineNativeRepository;
    @Autowired
    private HddcGeophysvylineRepository hddcGeophysvylineRepository;
    @Override
    public JSONObject queryHddcWyGeophysvylines(HddcWyGeophysvylineQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcWyGeophysvylineEntity> hddcWyGeophysvylinePage = this.hddcWyGeophysvylineNativeRepository.queryHddcWyGeophysvylines(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcWyGeophysvylinePage);
        return jsonObject;
    }


    @Override
    public HddcWyGeophysvylineEntity getHddcWyGeophysvyline(String uuid) {
        HddcWyGeophysvylineEntity hddcWyGeophysvyline = this.hddcWyGeophysvylineRepository.findById(uuid).orElse(null);
         return hddcWyGeophysvyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyGeophysvylineEntity saveHddcWyGeophysvyline(HddcWyGeophysvylineEntity hddcWyGeophysvyline) {
        String uuid = UUIDGenerator.getUUID();
        hddcWyGeophysvyline.setUuid(uuid);
        if(StringUtils.isEmpty(hddcWyGeophysvyline.getCreateUser())){
            hddcWyGeophysvyline.setCreateUser(PlatformSessionUtils.getUserId());
        }
        hddcWyGeophysvyline.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        hddcWyGeophysvyline.setIsValid("1");

        HddcGeophysvylineEntity hddcGeophysvylineEntity=new HddcGeophysvylineEntity();
        BeanUtils.copyProperties(hddcWyGeophysvyline,hddcGeophysvylineEntity);
        hddcGeophysvylineEntity.setExtends4(hddcWyGeophysvyline.getTown());
        hddcGeophysvylineEntity.setExtends5(String.valueOf(hddcWyGeophysvyline.getLon()));
        hddcGeophysvylineEntity.setExtends6(String.valueOf(hddcWyGeophysvyline.getLat()));
        this.hddcGeophysvylineRepository.save(hddcGeophysvylineEntity);

        this.hddcWyGeophysvylineRepository.save(hddcWyGeophysvyline);
        return hddcWyGeophysvyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyGeophysvylineEntity updateHddcWyGeophysvyline(HddcWyGeophysvylineEntity hddcWyGeophysvyline) {
        HddcWyGeophysvylineEntity entity = hddcWyGeophysvylineRepository.findById(hddcWyGeophysvyline.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcWyGeophysvyline);
        hddcWyGeophysvyline.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcWyGeophysvyline.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcWyGeophysvylineRepository.save(hddcWyGeophysvyline);
        return hddcWyGeophysvyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcWyGeophysvylines(List<String> ids) {
        List<HddcWyGeophysvylineEntity> hddcWyGeophysvylineList = this.hddcWyGeophysvylineRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcWyGeophysvylineList) && hddcWyGeophysvylineList.size() > 0) {
            for(HddcWyGeophysvylineEntity hddcWyGeophysvyline : hddcWyGeophysvylineList) {
                this.hddcWyGeophysvylineRepository.delete(hddcWyGeophysvyline);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public BigInteger queryHddcWyGeophysvyline(HddcAppZztCountVo queryParams) {
        BigInteger bigInteger = hddcWyGeophysvylineNativeRepository.queryHddcWyGeophysvyline(queryParams);
        return bigInteger;
    }

    @Override
    public List<HddcWyGeophysvylineEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid) {
        List<HddcWyGeophysvylineEntity> list = hddcWyGeophysvylineRepository.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, isValid);
        return list;
    }

    @Override
    public void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response) {
        List<HddcWyGeophysvylineEntity> hddcWyGeophysvylineEntity = hddcWyGeophysvylineNativeRepository.exportPhyLinr(queryParams);
        List<HddcWyGeophysvylineVO> list=new ArrayList<>();
        for (HddcWyGeophysvylineEntity entity:hddcWyGeophysvylineEntity) {
            HddcWyGeophysvylineVO hddcWyGeophysvylineVO=new HddcWyGeophysvylineVO();
            BeanUtils.copyProperties(entity,hddcWyGeophysvylineVO);
            list.add(hddcWyGeophysvylineVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"地球物理测线-线","地球物理测线-线", HddcWyGeophysvylineVO.class,"地球物理测线-线.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcWyGeophysvylineVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcWyGeophysvylineVO.class, params);
            List<HddcWyGeophysvylineVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcWyGeophysvylineVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcWyGeophysvylineVO hddcWyGeophysvylineVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + hddcWyGeophysvylineVO.getRowNum() + "行" + hddcWyGeophysvylineVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcWyGeophysvylineVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcWyGeophysvylineEntity hddcWyGeophysvylineEntity = new HddcWyGeophysvylineEntity();
            HddcWyGeophysvylineVO hddcWyGeophysvylineVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(hddcWyGeophysvylineVO, hddcWyGeophysvylineEntity);
            saveHddcWyGeophysvyline(hddcWyGeophysvylineEntity);
        }
    }

    @Override
    public List<HddcWyGeophysvylineEntity> findAllByCreateUserAndIsValid(String userId,String isValid) {
        List<HddcWyGeophysvylineEntity> list = hddcWyGeophysvylineRepository.findAllByCreateUserAndIsValid(userId,isValid);
        return list;
    }

    /**
     * 逻辑删除-根据项目id删除数据
     * @param projectIds
     */
    @Override
    public void deleteByProjectId(List<String> projectIds) {
        List<HddcWyGeophysvylineEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyGeophysvylineRepository.queryHddcA1InvrgnhasmaterialtablesByProjectId(projectIds);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyGeophysvylineEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyGeophysvylineRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }
    }

    /**
     * 逻辑删除-根据任务id删除数据
     * @param taskId
     */
    @Override
    public void deleteByTaskId(List<String> taskId) {
        List<HddcWyGeophysvylineEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyGeophysvylineRepository.queryHddcA1InvrgnhasmaterialtablesByTaskId(taskId);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyGeophysvylineEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyGeophysvylineRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }

    }











}
