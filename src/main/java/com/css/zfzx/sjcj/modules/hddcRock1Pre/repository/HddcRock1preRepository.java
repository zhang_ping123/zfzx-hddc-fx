package com.css.zfzx.sjcj.modules.hddcRock1Pre.repository;

import com.css.zfzx.sjcj.modules.hddcRock1Pre.repository.entity.HddcRock1preEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcRock1preRepository extends JpaRepository<HddcRock1preEntity, String> {
}
