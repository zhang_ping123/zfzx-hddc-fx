package com.css.zfzx.sjcj.modules.hddcImageIndexLayer.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangping
 * @date 2020-11-30
 */
@Data
public class HddcImageindexlayerVO implements Serializable {

    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 市(编码需要)
     */
    @Excel(name = "市(编码需要)", orderNum = "2")
    private String citycode;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 时相
     */
    @Excel(name = "时相", orderNum = "5")
    private String imagedate;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 村
     */
    private String village;
    /**
     * 编号
     */
    private String id;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 投影信息
     */
    @Excel(name = "投影信息", orderNum = "6")
    private String projection;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 城市名称
     */
    @Excel(name = "城市名称", orderNum = "4")
    private String city;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 传感器
     */
    @Excel(name = "传感器", orderNum = "7")
    private String sensor;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 处理过程
     */
    @Excel(name = "处理过程", orderNum = "8")
    private String processinfo;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 数据格式
     */
    @Excel(name = "数据格式", orderNum = "9")
    private String format;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "10")
    private String commentInfo;
    /**
     * 影像名称
     */
    @Excel(name = "影像名称", orderNum = "11")
    private String imagename;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 波段信息
     */
    @Excel(name = "波段信息", orderNum = "12")
    private String bandinfo;
    /**
     * 数据源
     */
    @Excel(name = "数据源", orderNum = "13")
    private String datasource;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 编号
     */
    private String objectCode;

    private String provinceName;
    private String citycodeName;
    private String areaName;
    private String rowNum;
    private String errorMsg;
}