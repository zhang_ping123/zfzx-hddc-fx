package com.css.zfzx.sjcj.modules.hddcActiveFault.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcActiveFault.repository.entity.HddcActivefaultEntity;
import com.css.zfzx.sjcj.modules.hddcActiveFault.viewobjects.HddcActivefaultQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */

public interface HddcActivefaultService {

    public JSONObject queryHddcActivefaults(HddcActivefaultQueryParams queryParams, int curPage, int pageSize);

    public HddcActivefaultEntity getHddcActivefault(String id);

    public HddcActivefaultEntity saveHddcActivefault(HddcActivefaultEntity hddcActivefault);

    public HddcActivefaultEntity updateHddcActivefault(HddcActivefaultEntity hddcActivefault);

    public List<HddcActivefaultEntity> findAll();

    public void deleteHddcActivefaults(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcActivefaultQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
