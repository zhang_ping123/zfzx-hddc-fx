package com.css.zfzx.sjcj.modules.hddcGeoProfileLine.repository;

import com.css.zfzx.sjcj.modules.hddcGeoProfileLine.repository.entity.HddcGeoprofilelineEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcGeoprofilelineRepository extends JpaRepository<HddcGeoprofilelineEntity, String> {
}
