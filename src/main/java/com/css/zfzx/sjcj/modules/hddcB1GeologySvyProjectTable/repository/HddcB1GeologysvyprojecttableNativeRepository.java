package com.css.zfzx.sjcj.modules.hddcB1GeologySvyProjectTable.repository;

import com.css.zfzx.sjcj.modules.hddcB1GeologySvyProjectTable.repository.entity.HddcB1GeologysvyprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeologySvyProjectTable.viewobjects.HddcB1GeologysvyprojecttableQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcB1GeologysvyprojecttableNativeRepository {

    Page<HddcB1GeologysvyprojecttableEntity> queryHddcB1Geologysvyprojecttables(HddcB1GeologysvyprojecttableQueryParams queryParams, int curPage, int pageSize);

    List<HddcB1GeologysvyprojecttableEntity> exportYhDisasters(HddcB1GeologysvyprojecttableQueryParams queryParams);
}
