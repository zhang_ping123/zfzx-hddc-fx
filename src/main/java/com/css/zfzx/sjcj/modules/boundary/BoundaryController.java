package com.css.zfzx.sjcj.modules.boundary;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.bpm.platform.utils.PlatformPageUtils;
import com.css.zfzx.sjcj.common.utils.AreaBoundaryUtil;
/*import com.css.zfzx.sjcj.modules.active.repository.entity.HddcActivefaultEntity;
import com.css.zfzx.sjcj.modules.active.service.HddcActivefaultService;
import com.css.zfzx.sjcj.modules.active.viewobjects.HddcActivefaultQueryParams;*/
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author andy
 * @date 2020-12-23
 */
@Slf4j
@RestController
@RequestMapping("/boundary")
public class BoundaryController {

    @GetMapping("/getSingleProvinceBoundary")
    public RestResponse getSingleProvinceBoundary(@RequestParam("provinceName") String provinceName) {
        RestResponse response = null;
        try {
            Map<String, String> coords = AreaBoundaryUtil.getProvinceBoundary(provinceName);
            response = RestResponse.succeed(coords);
        } catch (Exception e) {
            String errorMessage = "查询失败!";
            log.error(errorMessage, e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getSingleCityBoundary")
    public RestResponse getSingleCityBoundary(@RequestParam("provinceName") String provinceName, @RequestParam("cityName") String cityName) {
        RestResponse response = null;
        try {
            Map<String, String> city2boundary = AreaBoundaryUtil.getAllCitiesBoundaryByProvince(provinceName);
            response = RestResponse.succeed(city2boundary.get(cityName));
        } catch (Exception e) {
            String errorMessage = "查询失败!";
            log.error(errorMessage, e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getSingleAreaBoundary")
    public RestResponse getSingleCityBoundary(@RequestParam("provinceName") String provinceName, @RequestParam("cityName") String cityName, @RequestParam("areaName") String areaName) {
        RestResponse response = null;
        try {
            Map.Entry<String, Map<String, String>> coords = AreaBoundaryUtil.getCoordsByCityName(provinceName, cityName);
            response = RestResponse.succeed(coords.getValue().get(areaName));
        } catch (Exception e) {
            String errorMessage = "查询失败!";
            log.error(errorMessage, e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

}