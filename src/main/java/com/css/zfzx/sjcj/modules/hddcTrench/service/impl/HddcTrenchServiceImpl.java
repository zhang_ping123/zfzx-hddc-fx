package com.css.zfzx.sjcj.modules.hddcTrench.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltVO;
import com.css.zfzx.sjcj.modules.hddcTrench.repository.HddcTrenchNativeRepository;
import com.css.zfzx.sjcj.modules.hddcTrench.repository.HddcTrenchRepository;
import com.css.zfzx.sjcj.modules.hddcTrench.repository.entity.HddcTrenchEntity;
import com.css.zfzx.sjcj.modules.hddcTrench.service.HddcTrenchService;
import com.css.zfzx.sjcj.modules.hddcTrench.viewobjects.HddcTrenchQueryParams;
import com.css.zfzx.sjcj.modules.hddcTrench.viewobjects.HddcTrenchVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Service
public class HddcTrenchServiceImpl implements HddcTrenchService {

	@Autowired
    private HddcTrenchRepository hddcTrenchRepository;
    @Autowired
    private HddcTrenchNativeRepository hddcTrenchNativeRepository;

    @Override
    public JSONObject queryHddcTrenchs(HddcTrenchQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcTrenchEntity> hddcTrenchPage = this.hddcTrenchNativeRepository.queryHddcTrenchs(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcTrenchPage);
        return jsonObject;
    }


    @Override
    public HddcTrenchEntity getHddcTrench(String id) {
        HddcTrenchEntity hddcTrench = this.hddcTrenchRepository.findById(id).orElse(null);
         return hddcTrench;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcTrenchEntity saveHddcTrench(HddcTrenchEntity hddcTrench) {
        String uuid = UUIDGenerator.getUUID();
        hddcTrench.setUuid(uuid);
        hddcTrench.setCreateUser(PlatformSessionUtils.getUserId());
        hddcTrench.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcTrenchRepository.save(hddcTrench);
        return hddcTrench;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcTrenchEntity updateHddcTrench(HddcTrenchEntity hddcTrench) {
        HddcTrenchEntity entity = hddcTrenchRepository.findById(hddcTrench.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcTrench);
        hddcTrench.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcTrench.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcTrenchRepository.save(hddcTrench);
        return hddcTrench;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcTrenchs(List<String> ids) {
        List<HddcTrenchEntity> hddcTrenchList = this.hddcTrenchRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcTrenchList) && hddcTrenchList.size() > 0) {
            for(HddcTrenchEntity hddcTrench : hddcTrenchList) {
                this.hddcTrenchRepository.delete(hddcTrench);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcTrenchQueryParams queryParams, HttpServletResponse response) {
        List<HddcTrenchEntity> yhDisasterEntities = hddcTrenchNativeRepository.exportYhDisasters(queryParams);
        List<HddcTrenchVO> list=new ArrayList<>();
        for (HddcTrenchEntity entity:yhDisasterEntities) {
            HddcTrenchVO yhDisasterVO=new HddcTrenchVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"探槽-点","探槽-点",HddcTrenchVO.class,"探槽-点.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcTrenchVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcTrenchVO.class, params);
            List<HddcTrenchVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcTrenchVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcTrenchVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcTrenchVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcTrenchEntity yhDisasterEntity = new HddcTrenchEntity();
            HddcTrenchVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcTrench(yhDisasterEntity);
        }
    }

}
