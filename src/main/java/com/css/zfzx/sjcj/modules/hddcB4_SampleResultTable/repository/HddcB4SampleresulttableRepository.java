package com.css.zfzx.sjcj.modules.hddcB4_SampleResultTable.repository;

import com.css.zfzx.sjcj.modules.hddcB4_SampleResultTable.repository.entity.HddcB4SampleresulttableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcB4SampleresulttableRepository extends JpaRepository<HddcB4SampleresulttableEntity, String> {
}
