package com.css.zfzx.sjcj.modules.hddcRock1LinePre.repository;

import com.css.zfzx.sjcj.modules.hddcRock1LinePre.repository.entity.HddcRock1linepreEntity;
import com.css.zfzx.sjcj.modules.hddcRock1LinePre.viewobjects.HddcRock1linepreQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-26
 */
public interface HddcRock1linepreNativeRepository {

    Page<HddcRock1linepreEntity> queryHddcRock1linepres(HddcRock1linepreQueryParams queryParams, int curPage, int pageSize);

    List<HddcRock1linepreEntity> exportYhDisasters(HddcRock1linepreQueryParams queryParams);
}
