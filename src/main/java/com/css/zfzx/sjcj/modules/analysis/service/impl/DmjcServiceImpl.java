package com.css.zfzx.sjcj.modules.analysis.service.impl;

import com.css.zfzx.sjcj.modules.analysis.service.DmjcService;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyGeomorStation.service.HddcWyGeomorstationService;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyLine.service.HddcWyGeomorphysvylineService;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyLine.viewobjects.HddcWyGeomorphysvylineQueryParams;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.service.HddcWyGeomorphysvypointService;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyReProf.service.HddcWyGeomorphysvyreprofService;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyRegion.service.HddcWyGeomorphysvyregionService;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvySamplePoint.service.HddcWyGeomorphysvysamplepointService;
import com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.viewobjects.HddcWyGeophysvylineQueryParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO
 *
 * @author 王昊杰
 * @version 1.0
 * @date 2020/12/8  17:03
 */

@Service
public class DmjcServiceImpl implements DmjcService {
    @Autowired
    private HddcWyGeomorphysvylineService hddcWyGeomorphysvylineService;
    @Autowired
    private HddcWyGeomorphysvypointService hddcWyGeomorphysvypointService;
    @Autowired
    private HddcWyGeomorphysvyregionService hddcWyGeomorphysvyregionService;
    @Autowired
    private HddcWyGeomorphysvyreprofService hddcWyGeomorphysvyreprofService;
    @Autowired
    private HddcWyGeomorphysvysamplepointService hddcWyGeomorphysvysamgaplepointService;
    @Autowired
    private HddcWyGeomorstationService hddcWyGeomorstationService;

    public Map<String,Object> hddcDmjcNumData(HddcAppZztCountVo queryParams){
        //微地貌测量线
        //BigInteger lineCount= hddcWyGeomorphysvylineService.queryHddcWyGeomorphysvyline(queryParams);
       //微地貌测量点
        BigInteger pointCount=hddcWyGeomorphysvypointService.queryHddcWyGeomorphysvypoint(queryParams);
       //微地貌测量面
       // BigInteger regionfCount = hddcWyGeomorphysvyregionService.queryHddcWyGeomorphysvyregion(queryParams);
       //微地貌测量切线
        //BigInteger reprofCount = hddcWyGeomorphysvyreprofService.queryHddcWyGeomorphysvyreprof(queryParams);
       //微地貌测量采样点
        BigInteger samgapleCount = hddcWyGeomorphysvysamgaplepointService.queryHddcWyGeomorphysvysamplepoint(queryParams);
        //微地貌测量基站点
        BigInteger stationCount = hddcWyGeomorstationService.queryHddcWyGeomorstation(queryParams);
        //X轴数据
        ArrayList<String> x=new ArrayList<>();
        //x.add("微地貌测量线-线");
        x.add("微地貌测量点-点");
        //x.add("微地貌测量面-面");
        //x.add("微地貌面测量切线-线");
        x.add("微地貌测量采样点-点");
        x.add("微地貌测量基站点-点");
        //Y轴
        ArrayList<BigInteger> y=new ArrayList<>();
        //y.add(lineCount);
        y.add(pointCount);
        //y.add(regionfCount);
        //y.add(reprofCount);
        y.add(samgapleCount);
        y.add(stationCount);
        HashMap<String,Object> map=new HashMap<>();
        map.put("x",x);
        map.put("y",y);
        return map;
    }
}
