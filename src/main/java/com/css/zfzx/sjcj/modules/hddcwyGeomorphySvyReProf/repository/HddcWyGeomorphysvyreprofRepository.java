package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyReProf.repository;

import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyReProf.repository.entity.HddcWyGeomorphysvyreprofEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author lihelei
 * @date 2020-12-01
 */
public interface HddcWyGeomorphysvyreprofRepository extends JpaRepository<HddcWyGeomorphysvyreprofEntity, String> {

    List<HddcWyGeomorphysvyreprofEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    List<HddcWyGeomorphysvyreprofEntity> findAllByCreateUserAndIsValid(String userId,String isValid);


    @Query(nativeQuery = true, value = "select * from hddc_wy_geomorphysvyreprof where is_valid!=0 project_name in :projectIds")
    List<HddcWyGeomorphysvyreprofEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_geomorphysvyreprof where task_name in :projectIds")
    List<HddcWyGeomorphysvyreprofEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);


}
