package com.css.zfzx.sjcj.modules.hddcEpiMechanismResult.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcEpiMechanismResult.repository.HddcEpimechanismresultNativeRepository;
import com.css.zfzx.sjcj.modules.hddcEpiMechanismResult.repository.HddcEpimechanismresultRepository;
import com.css.zfzx.sjcj.modules.hddcEpiMechanismResult.repository.entity.HddcEpimechanismresultEntity;
import com.css.zfzx.sjcj.modules.hddcEpiMechanismResult.service.HddcEpimechanismresultService;
import com.css.zfzx.sjcj.modules.hddcEpiMechanismResult.viewobjects.HddcEpimechanismresultQueryParams;
import com.css.zfzx.sjcj.modules.hddcEpiMechanismResult.viewobjects.HddcEpimechanismresultVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Service
public class HddcEpimechanismresultServiceImpl implements HddcEpimechanismresultService {

	@Autowired
    private HddcEpimechanismresultRepository hddcEpimechanismresultRepository;
    @Autowired
    private HddcEpimechanismresultNativeRepository hddcEpimechanismresultNativeRepository;

    @Override
    public JSONObject queryHddcEpimechanismresults(HddcEpimechanismresultQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcEpimechanismresultEntity> hddcEpimechanismresultPage = this.hddcEpimechanismresultNativeRepository.queryHddcEpimechanismresults(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcEpimechanismresultPage);
        return jsonObject;
    }


    @Override
    public HddcEpimechanismresultEntity getHddcEpimechanismresult(String id) {
        HddcEpimechanismresultEntity hddcEpimechanismresult = this.hddcEpimechanismresultRepository.findById(id).orElse(null);
         return hddcEpimechanismresult;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcEpimechanismresultEntity saveHddcEpimechanismresult(HddcEpimechanismresultEntity hddcEpimechanismresult) {
        String uuid = UUIDGenerator.getUUID();
        hddcEpimechanismresult.setUuid(uuid);
        hddcEpimechanismresult.setCreateUser(PlatformSessionUtils.getUserId());
        hddcEpimechanismresult.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcEpimechanismresultRepository.save(hddcEpimechanismresult);
        return hddcEpimechanismresult;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcEpimechanismresultEntity updateHddcEpimechanismresult(HddcEpimechanismresultEntity hddcEpimechanismresult) {
        HddcEpimechanismresultEntity entity = hddcEpimechanismresultRepository.findById(hddcEpimechanismresult.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcEpimechanismresult);
        hddcEpimechanismresult.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcEpimechanismresult.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcEpimechanismresultRepository.save(hddcEpimechanismresult);
        return hddcEpimechanismresult;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcEpimechanismresults(List<String> ids) {
        List<HddcEpimechanismresultEntity> hddcEpimechanismresultList = this.hddcEpimechanismresultRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcEpimechanismresultList) && hddcEpimechanismresultList.size() > 0) {
            for(HddcEpimechanismresultEntity hddcEpimechanismresult : hddcEpimechanismresultList) {
                this.hddcEpimechanismresultRepository.delete(hddcEpimechanismresult);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcEpimechanismresultQueryParams queryParams, HttpServletResponse response) {
        List<HddcEpimechanismresultEntity> yhDisasterEntities = hddcEpimechanismresultNativeRepository.exportYhDisasters(queryParams);
        List<HddcEpimechanismresultVO> list=new ArrayList<>();
        for (HddcEpimechanismresultEntity entity:yhDisasterEntities) {
            HddcEpimechanismresultVO yhDisasterVO=new HddcEpimechanismresultVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "震源机制解数据-点", "震源机制解数据-点", HddcEpimechanismresultVO.class, "震源机制解数据-点.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcEpimechanismresultVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcEpimechanismresultVO.class, params);
            List<HddcEpimechanismresultVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcEpimechanismresultVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcEpimechanismresultVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcEpimechanismresultVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcEpimechanismresultEntity yhDisasterEntity = new HddcEpimechanismresultEntity();
            HddcEpimechanismresultVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcEpimechanismresult(yhDisasterEntity);
        }
    }
}
