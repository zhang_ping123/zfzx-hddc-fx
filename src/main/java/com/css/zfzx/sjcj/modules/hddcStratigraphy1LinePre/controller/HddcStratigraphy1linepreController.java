package com.css.zfzx.sjcj.modules.hddcStratigraphy1LinePre.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.bpm.platform.utils.PlatformPageUtils;
import com.css.zfzx.sjcj.modules.hddcStratigraphy1LinePre.repository.entity.HddcStratigraphy1linepreEntity;
import com.css.zfzx.sjcj.modules.hddcStratigraphy1LinePre.service.HddcStratigraphy1linepreService;
import com.css.zfzx.sjcj.modules.hddcStratigraphy1LinePre.viewobjects.HddcStratigraphy1linepreQueryParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcStratigraphy1linepres")
public class HddcStratigraphy1linepreController {
    @Autowired
    private HddcStratigraphy1linepreService hddcStratigraphy1linepreService;

    @GetMapping("/queryHddcStratigraphy1linepres")
    public RestResponse queryHddcStratigraphy1linepres(HttpServletRequest request, HddcStratigraphy1linepreQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcStratigraphy1linepreService.queryHddcStratigraphy1linepres(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcStratigraphy1linepre(@PathVariable String id) {
        RestResponse response = null;
        try{
            HddcStratigraphy1linepreEntity hddcStratigraphy1linepre = hddcStratigraphy1linepreService.getHddcStratigraphy1linepre(id);
            response = RestResponse.succeed(hddcStratigraphy1linepre);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcStratigraphy1linepre(@RequestBody HddcStratigraphy1linepreEntity hddcStratigraphy1linepre) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcStratigraphy1linepreService.saveHddcStratigraphy1linepre(hddcStratigraphy1linepre);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcStratigraphy1linepre(@RequestBody HddcStratigraphy1linepreEntity hddcStratigraphy1linepre)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcStratigraphy1linepreService.updateHddcStratigraphy1linepre(hddcStratigraphy1linepre);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcStratigraphy1linepres(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcStratigraphy1linepreService.deleteHddcStratigraphy1linepres(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcStratigraphy1linepreService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }

    @GetMapping("/findByDictCodeAndDictItemCode")
    public RestResponse findByDictCodeAndDictItemCode(@RequestParam String dictCode, @RequestParam String dictItemCode){
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcStratigraphy1linepreService.findByDictCodeAndDictItemCode(dictCode, dictItemCode));
        } catch (Exception e) {
            String errorMsg = "字典名获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }
    /***
     * 导出
     * @param response
     * @return
     */
    @GetMapping("/exportFile")
    public RestResponse exportFileYhDisasters(HttpServletResponse response,
                                              @RequestParam("projectName")String projectName,
                                              @RequestParam("province") String province, @RequestParam("city")String city, @RequestParam("area")String area) {
        RestResponse responseRest = null;
        JSONObject jsonObject = new JSONObject();
        try{
            HddcStratigraphy1linepreQueryParams queryParams=new HddcStratigraphy1linepreQueryParams();
            queryParams.setArea(area);
            queryParams.setCity(city);
            queryParams.setProvince(province);
            queryParams.setProjectName(projectName);
            hddcStratigraphy1linepreService.exportFile(queryParams,response);
            jsonObject.put("message", "导出成功");
            responseRest = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            responseRest = RestResponse.fail(errorMessage);
        }
        return responseRest;
    }

    /**
     * 导入
     *
     * @param file
     * @param response
     * @return
     */
    @PostMapping("/importDisaster")
    public RestResponse export(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        RestResponse restResponse = null;
        try {
            String s = hddcStratigraphy1linepreService.exportExcel( file, response);
            restResponse = RestResponse.succeed(s);
        } catch (Exception e) {
            String errormessage = "导入失败";
            log.error(errormessage, e);
            restResponse = RestResponse.fail(errormessage);
        }
        return restResponse;
    }
}