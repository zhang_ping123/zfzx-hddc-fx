package com.css.zfzx.sjcj.modules.hddcDResultmaptable.repository.entity;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-12-19
 */
@Data
@Entity
@Table(name="hddc_d_resultmaptable")
public class HddcDResultmaptableEntity implements Serializable {

    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 编制时间
     */
    @Column(name="finishedtime")
    private String finishedtime;
    /**
     * 主键
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 资料/数据处理记录
     */
    @Column(name="processingrecord")
    private String processingrecord;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 投影信息
     */
    @Column(name="projection")
    private String projection;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 图件比例尺（分母）
     */
    @Column(name="scale")
    private Integer scale;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 项目id
     */
    @Column(name="project_code")
    private String projectCode;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 使用资料记录
     */
    @Column(name="materialrecord")
    private String materialrecord;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 探测城市
     */
    @Column(name="cityname")
    private String cityname;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 作者
     */
    @Column(name="authors")
    private String authors;
    /**
     * 工作区编号
     */
    @Column(name="workregionid")
    private String workregionid;
    /**
     * 坐标系
     */
    @Column(name="coordinatesystem")
    private String coordinatesystem;
    /**
     * 附件id
     */
    @Column(name="attachId")
    private String attachid;
    /**
     * 附件路径
     */
    @Column(name="attachPath")
    private String attachpath;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 工程编号
     */
    @Column(name="projectid")
    private String projectId;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 编号
     */
    @Column(name="id")
    private String id;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 目标区编号
     */
    @Column(name="targetregionid")
    private String targetregionid;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 图书在版编目（cip）数据
     */
    @Column(name="cip")
    private String cip;
    /**
     * 出版日期
     */
    @Column(name="publishdate")
    private String publishdate;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 任务id
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 出版单位
     */
    @Column(name="publisher")
    private String publisher;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 成果图原始档案文件编号
     */
    @Column(name="resultmap_arwid")
    private String resultmapArwid;
    /**
     * 描述信息
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 地质填图区编号
     */
    @Column(name="mainafsregionid")
    private String mainafsregionid;
    /**
     * 成果图图像文件编号
     */
    @Column(name="resultmap_aiid")
    private String resultmapAiid;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 名称
     */
    @Column(name="filename")
    private String filename;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;

}

