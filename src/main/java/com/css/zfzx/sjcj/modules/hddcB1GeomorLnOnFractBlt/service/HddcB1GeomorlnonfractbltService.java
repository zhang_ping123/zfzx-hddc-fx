package com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */

public interface HddcB1GeomorlnonfractbltService {

    public JSONObject queryHddcB1Geomorlnonfractblts(HddcB1GeomorlnonfractbltQueryParams queryParams, int curPage, int pageSize);

    public HddcB1GeomorlnonfractbltEntity getHddcB1Geomorlnonfractblt(String id);

    public HddcB1GeomorlnonfractbltEntity saveHddcB1Geomorlnonfractblt(HddcB1GeomorlnonfractbltEntity hddcB1Geomorlnonfractblt);

    public HddcB1GeomorlnonfractbltEntity updateHddcB1Geomorlnonfractblt(HddcB1GeomorlnonfractbltEntity hddcB1Geomorlnonfractblt);

    public void deleteHddcB1Geomorlnonfractblts(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB1GeomorlnonfractbltQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
