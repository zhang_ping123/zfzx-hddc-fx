package com.css.zfzx.sjcj.modules.hddccjfilemanage.repository;

import com.css.zfzx.sjcj.modules.hddccjfilemanage.repository.entity.HddcFileManageEntity;
import com.css.zfzx.sjcj.modules.hddccjfilemanage.viewobjects.HddcFileManageQueryParams;
import org.springframework.data.domain.Page;

/**
 * @author zyb
 * @date 2020-12-10
 */
public interface HddcFileManageNativeRepository {

    Page<HddcFileManageEntity> queryHddcFileManages(HddcFileManageQueryParams queryParams, int curPage, int pageSize);

}
