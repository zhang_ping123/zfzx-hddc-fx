package com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyLine.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyLine.repository.HddcGeologicalsvylineRepository;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyLine.repository.entity.HddcGeologicalsvylineEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyLine.repository.HddcWyGeologicalsvylineNativeRepository;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyLine.repository.HddcWyGeologicalsvylineRepository;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyLine.repository.entity.HddcWyGeologicalsvylineEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyLine.service.HddcWyGeologicalsvylineService;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyLine.viewobjects.HddcWyGeologicalsvylineQueryParams;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyLine.viewobjects.HddcWyGeologicalsvylineVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * @author zyb
 * @date 2020-12-01
 */
@Service
@PropertySource(value = "classpath:platform-config.yml")
public class HddcWyGeologicalsvylineServiceImpl implements HddcWyGeologicalsvylineService {

	@Autowired
    private HddcWyGeologicalsvylineRepository hddcWyGeologicalsvylineRepository;
    @Autowired
    private HddcWyGeologicalsvylineNativeRepository hddcWyGeologicalsvylineNativeRepository;
    @Autowired
    private HddcGeologicalsvylineRepository hddcGeologicalsvylineRepository;
    @Value("${image.localDir}")
    private String localDir;
    @Value("${image.urlPre}")
    private String urlPre;
    @Override
    public JSONObject queryHddcWyGeologicalsvylines(HddcWyGeologicalsvylineQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcWyGeologicalsvylineEntity> hddcWyGeologicalsvylinePage = this.hddcWyGeologicalsvylineNativeRepository.queryHddcWyGeologicalsvylines(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcWyGeologicalsvylinePage);
        return jsonObject;
    }


    @Override
    public HddcWyGeologicalsvylineEntity getHddcWyGeologicalsvyline(String uuid) {
        HddcWyGeologicalsvylineEntity hddcWyGeologicalsvyline = this.hddcWyGeologicalsvylineRepository.findById(uuid).orElse(null);
         return hddcWyGeologicalsvyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyGeologicalsvylineEntity saveHddcWyGeologicalsvyline(HddcWyGeologicalsvylineEntity hddcWyGeologicalsvyline) {
        String uuid = UUIDGenerator.getUUID();
        hddcWyGeologicalsvyline.setUuid(uuid);
        if(StringUtils.isEmpty(hddcWyGeologicalsvyline.getCreateUser())){
            hddcWyGeologicalsvyline.setCreateUser(PlatformSessionUtils.getUserId());
        }
        hddcWyGeologicalsvyline.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        hddcWyGeologicalsvyline.setIsValid("1");

        HddcGeologicalsvylineEntity hddcGeologicalsvylineEntity=new HddcGeologicalsvylineEntity();
        BeanUtils.copyProperties(hddcWyGeologicalsvyline,hddcGeologicalsvylineEntity);
        hddcGeologicalsvylineEntity.setExtends4(hddcWyGeologicalsvyline.getTown());
        hddcGeologicalsvylineEntity.setExtends5(String.valueOf(hddcWyGeologicalsvyline.getLon()));
        hddcGeologicalsvylineEntity.setExtends6(String.valueOf(hddcWyGeologicalsvyline.getLat()));
        this.hddcGeologicalsvylineRepository.save(hddcGeologicalsvylineEntity);

        this.hddcWyGeologicalsvylineRepository.save(hddcWyGeologicalsvyline);
        return hddcWyGeologicalsvyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyGeologicalsvylineEntity updateHddcWyGeologicalsvyline(HddcWyGeologicalsvylineEntity hddcWyGeologicalsvyline) {
        HddcWyGeologicalsvylineEntity entity = hddcWyGeologicalsvylineRepository.findById(hddcWyGeologicalsvyline.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcWyGeologicalsvyline);
        hddcWyGeologicalsvyline.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcWyGeologicalsvyline.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcWyGeologicalsvylineRepository.save(hddcWyGeologicalsvyline);
        return hddcWyGeologicalsvyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcWyGeologicalsvylines(List<String> ids) {
        List<HddcWyGeologicalsvylineEntity> hddcWyGeologicalsvylineList = this.hddcWyGeologicalsvylineRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcWyGeologicalsvylineList) && hddcWyGeologicalsvylineList.size() > 0) {
            for(HddcWyGeologicalsvylineEntity hddcWyGeologicalsvyline : hddcWyGeologicalsvylineList) {
                this.hddcWyGeologicalsvylineRepository.delete(hddcWyGeologicalsvyline);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public BigInteger queryHddcWyGeologicalsvyline(HddcAppZztCountVo queryParams) {
        BigInteger bigInteger = hddcWyGeologicalsvylineNativeRepository.queryHddcWyGeologicalsvyline(queryParams);
        return bigInteger;
    }

    @Override
    public List<HddcWyGeologicalsvylineEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid) {
        List<HddcWyGeologicalsvylineEntity> list = hddcWyGeologicalsvylineRepository.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, isValid);
        return list;
    }

    @Override
    public void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response) {
        List<HddcWyGeologicalsvylineEntity> hddcWyGeologicalsvylineEntity = hddcWyGeologicalsvylineNativeRepository.exportLine(queryParams);
        List<HddcWyGeologicalsvylineVO> list=new ArrayList<>();
        for (HddcWyGeologicalsvylineEntity entity:hddcWyGeologicalsvylineEntity) {
            HddcWyGeologicalsvylineVO hddcWyGeologicalsvylineVO=new HddcWyGeologicalsvylineVO();
            BeanUtils.copyProperties(entity,hddcWyGeologicalsvylineVO);
            list.add(hddcWyGeologicalsvylineVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"地质调查路线-线","地质调查路线-线", HddcWyGeologicalsvylineVO.class,"地质调查路线-线.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcWyGeologicalsvylineVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcWyGeologicalsvylineVO.class, params);
            List<HddcWyGeologicalsvylineVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcWyGeologicalsvylineVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcWyGeologicalsvylineVO hddcWyGeologicalsvylineVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + hddcWyGeologicalsvylineVO.getRowNum() + "行" + hddcWyGeologicalsvylineVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcWyGeologicalsvylineVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcWyGeologicalsvylineEntity hddcWyGeologicalsvylineEntity = new HddcWyGeologicalsvylineEntity();
            HddcWyGeologicalsvylineVO hddcWyGeologicalsvylineVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(hddcWyGeologicalsvylineVO, hddcWyGeologicalsvylineEntity);
            saveHddcWyGeologicalsvyline(hddcWyGeologicalsvylineEntity);
        }
    }

    @Override
    public List<HddcWyGeologicalsvylineEntity> findAllByCreateUserAndIsValid(String userId, String isValid) {
        List<HddcWyGeologicalsvylineEntity> list = hddcWyGeologicalsvylineRepository.findAllByCreateUserAndIsValid(userId,isValid);
        return list;
    }


    /**
     * 逻辑删除-根据项目id删除数据
     * @param projectIds
     */
    @Override
    public void deleteByProjectId(List<String> projectIds) {
        List<HddcWyGeologicalsvylineEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyGeologicalsvylineRepository.queryHddcA1InvrgnhasmaterialtablesByProjectId(projectIds);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyGeologicalsvylineEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyGeologicalsvylineRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }
    }

    /**
     * 逻辑删除-根据任务id删除数据
     * @param taskId
     */
    @Override
    public void deleteByTaskId(List<String> taskId) {
        List<HddcWyGeologicalsvylineEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyGeologicalsvylineRepository.queryHddcA1InvrgnhasmaterialtablesByTaskId(taskId);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyGeologicalsvylineEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyGeologicalsvylineRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }

    }


}
