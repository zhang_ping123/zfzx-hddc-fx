package com.css.zfzx.sjcj.modules.hddcRock1Pre.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Data
public class HddcRock1preVO implements Serializable {

    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 岩体符号基础位
     */
    @Excel(name = "岩体符号基础位", orderNum = "4")
    private String nsb1;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "5")
    private String commentInfo;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 岩体类别
     */
    @Excel(name = "岩体类别", orderNum = "6")
    private Integer symbol;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 岩体编号
     */
    private String id;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 岩体符号下标位
     */
    @Excel(name = "岩体符号下标位", orderNum = "7")
    private String nsb2;
    /**
     * 岩体名称
     */
    @Excel(name = "岩体名称", orderNum = "8")
    private String rockname;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 岩体符号上标位
     */
    @Excel(name = "岩体符号上标位", orderNum = "9")
    private String nsb3;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备注
     */
    private String remark;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 岩体描述
     */
    @Excel(name = "岩体描述", orderNum = "10")
    private String description;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 侵入时代
     */
    private Integer qdho;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 岩体合并
     */
    @Excel(name = "岩体合并", orderNum = "11")
    private String rockunion;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段20
     */
    private String extends20;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String symbolName;
    private Integer qdhoName;
    private String rowNum;
    private String errorMsg;
}