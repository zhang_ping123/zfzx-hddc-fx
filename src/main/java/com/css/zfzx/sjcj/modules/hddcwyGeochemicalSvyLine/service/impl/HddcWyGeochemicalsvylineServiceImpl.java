package com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyLine.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyLine.repository.HddcGeochemicalsvylineRepository;
import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyLine.repository.entity.HddcGeochemicalsvylineEntity;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyLine.repository.HddcWyGeochemicalsvylineNativeRepository;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyLine.repository.HddcWyGeochemicalsvylineRepository;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyLine.repository.entity.HddcWyGeochemicalsvylineEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyLine.service.HddcWyGeochemicalsvylineService;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyLine.viewobjects.HddcWyGeochemicalsvylineQueryParams;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyLine.viewobjects.HddcWyGeochemicalsvylineVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zhangcong
 * @date 2020-12-02
 */
@Service
public class HddcWyGeochemicalsvylineServiceImpl implements HddcWyGeochemicalsvylineService {

	@Autowired
    private HddcWyGeochemicalsvylineRepository hddcWyGeochemicalsvylineRepository;
    @Autowired
    private HddcWyGeochemicalsvylineNativeRepository hddcWyGeochemicalsvylineNativeRepository;
    @Autowired
    private HddcGeochemicalsvylineRepository hddcGeochemicalsvylineRepository;
    @Override
    public JSONObject queryHddcWyGeochemicalsvylines(HddcWyGeochemicalsvylineQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcWyGeochemicalsvylineEntity> hddcWyGeochemicalsvylinePage = this.hddcWyGeochemicalsvylineNativeRepository.queryHddcWyGeochemicalsvylines(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcWyGeochemicalsvylinePage);
        return jsonObject;
    }


    @Override
    public HddcWyGeochemicalsvylineEntity getHddcWyGeochemicalsvyline(String uuid) {
        HddcWyGeochemicalsvylineEntity hddcWyGeochemicalsvyline = this.hddcWyGeochemicalsvylineRepository.findById(uuid).orElse(null);
         return hddcWyGeochemicalsvyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyGeochemicalsvylineEntity saveHddcWyGeochemicalsvyline(HddcWyGeochemicalsvylineEntity hddcWyGeochemicalsvyline) {
        String uuid = UUIDGenerator.getUUID();
        hddcWyGeochemicalsvyline.setUuid(uuid);
        if(StringUtils.isEmpty(hddcWyGeochemicalsvyline.getCreateUser())){
            hddcWyGeochemicalsvyline.setCreateUser(PlatformSessionUtils.getUserId());
        }
        hddcWyGeochemicalsvyline.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        hddcWyGeochemicalsvyline.setIsValid("1");

        HddcGeochemicalsvylineEntity hddcGeochemicalsvylineEntity=new HddcGeochemicalsvylineEntity();
        BeanUtils.copyProperties(hddcWyGeochemicalsvyline,hddcGeochemicalsvylineEntity);
        hddcGeochemicalsvylineEntity.setExtends4(hddcWyGeochemicalsvyline.getTown());
        hddcGeochemicalsvylineEntity.setExtends5(String.valueOf(hddcWyGeochemicalsvyline.getLon()));
        hddcGeochemicalsvylineEntity.setExtends6(String.valueOf(hddcWyGeochemicalsvyline.getLat()));
        this.hddcGeochemicalsvylineRepository.save(hddcGeochemicalsvylineEntity);

        this.hddcWyGeochemicalsvylineRepository.save(hddcWyGeochemicalsvyline);
        return hddcWyGeochemicalsvyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyGeochemicalsvylineEntity updateHddcWyGeochemicalsvyline(HddcWyGeochemicalsvylineEntity hddcWyGeochemicalsvyline) {
        HddcWyGeochemicalsvylineEntity entity = hddcWyGeochemicalsvylineRepository.findById(hddcWyGeochemicalsvyline.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcWyGeochemicalsvyline);
        hddcWyGeochemicalsvyline.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcWyGeochemicalsvyline.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcWyGeochemicalsvylineRepository.save(hddcWyGeochemicalsvyline);
        return hddcWyGeochemicalsvyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcWyGeochemicalsvylines(List<String> ids) {
        List<HddcWyGeochemicalsvylineEntity> hddcWyGeochemicalsvylineList = this.hddcWyGeochemicalsvylineRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcWyGeochemicalsvylineList) && hddcWyGeochemicalsvylineList.size() > 0) {
            for(HddcWyGeochemicalsvylineEntity hddcWyGeochemicalsvyline : hddcWyGeochemicalsvylineList) {
                this.hddcWyGeochemicalsvylineRepository.delete(hddcWyGeochemicalsvyline);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public BigInteger queryHddcWyGeochemicalsvyline(HddcAppZztCountVo queryParams) {
        BigInteger bigInteger = hddcWyGeochemicalsvylineNativeRepository.queryHddcWyGeochemicalsvyline(queryParams);
        return bigInteger;
    }

    @Override
    public List<HddcWyGeochemicalsvylineEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid) {
        List<HddcWyGeochemicalsvylineEntity> list = hddcWyGeochemicalsvylineRepository.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, isValid);
        return list;
    }

    @Override
    public void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response) {
        List<HddcWyGeochemicalsvylineEntity> hddcWyGeochemicalsvylineEntity = hddcWyGeochemicalsvylineNativeRepository.exportChemLine(queryParams);
        List<HddcWyGeochemicalsvylineVO> list=new ArrayList<>();
        for (HddcWyGeochemicalsvylineEntity entity:hddcWyGeochemicalsvylineEntity) {
            HddcWyGeochemicalsvylineVO hddcWyGeochemicalsvylineVO=new HddcWyGeochemicalsvylineVO();
            BeanUtils.copyProperties(entity,hddcWyGeochemicalsvylineVO);
            list.add(hddcWyGeochemicalsvylineVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"地球化学探测测线-线","地球化学探测测线-线", HddcWyGeochemicalsvylineVO.class,"地球化学探测测线-线.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcWyGeochemicalsvylineVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcWyGeochemicalsvylineVO.class, params);
            List<HddcWyGeochemicalsvylineVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcWyGeochemicalsvylineVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcWyGeochemicalsvylineVO hddcWyGeochemicalsvylineVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + hddcWyGeochemicalsvylineVO.getRowNum() + "行" + hddcWyGeochemicalsvylineVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcWyGeochemicalsvylineVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcWyGeochemicalsvylineEntity hddcWyGeochemicalsvylineEntity = new HddcWyGeochemicalsvylineEntity();
            HddcWyGeochemicalsvylineVO hddcWyGeochemicalsvylineVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(hddcWyGeochemicalsvylineVO, hddcWyGeochemicalsvylineEntity);
            saveHddcWyGeochemicalsvyline(hddcWyGeochemicalsvylineEntity);
        }
    }

    @Override
    public List<HddcWyGeochemicalsvylineEntity> findAllByCreateUserAndIsValid(String userId, String isValid) {
        List<HddcWyGeochemicalsvylineEntity> list = hddcWyGeochemicalsvylineRepository.findAllByCreateUserAndIsValid(userId, isValid);
        return list;
    }


    /**
     * 逻辑删除-根据项目id删除数据
     * @param projectIds
     */
    @Override
    public void deleteByProjectId(List<String> projectIds) {
        List<HddcWyGeochemicalsvylineEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyGeochemicalsvylineRepository.queryHddcA1InvrgnhasmaterialtablesByProjectId(projectIds);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyGeochemicalsvylineEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyGeochemicalsvylineRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }
    }

    /**
     * 逻辑删除-根据任务id删除数据
     * @param taskId
     */
    @Override
    public void deleteByTaskId(List<String> taskId) {
        List<HddcWyGeochemicalsvylineEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyGeochemicalsvylineRepository.queryHddcA1InvrgnhasmaterialtablesByTaskId(taskId);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyGeochemicalsvylineEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyGeochemicalsvylineRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }

    }

}
