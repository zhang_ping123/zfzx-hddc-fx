package com.css.zfzx.sjcj.modules.hddcImportantSurfaceFeature.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcImportantSurfaceFeature.repository.entity.HddcImportantsurfacefeatureEntity;
import com.css.zfzx.sjcj.modules.hddcImportantSurfaceFeature.viewobjects.HddcImportantsurfacefeatureQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author lihelei
 * @date 2020-11-30
 */

public interface HddcImportantsurfacefeatureService {

    public JSONObject queryHddcImportantsurfacefeatures(HddcImportantsurfacefeatureQueryParams queryParams, int curPage, int pageSize);

    public HddcImportantsurfacefeatureEntity getHddcImportantsurfacefeature(String id);

    public HddcImportantsurfacefeatureEntity saveHddcImportantsurfacefeature(HddcImportantsurfacefeatureEntity hddcImportantsurfacefeature);

    public HddcImportantsurfacefeatureEntity updateHddcImportantsurfacefeature(HddcImportantsurfacefeatureEntity hddcImportantsurfacefeature);

    public void deleteHddcImportantsurfacefeatures(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcImportantsurfacefeatureQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
