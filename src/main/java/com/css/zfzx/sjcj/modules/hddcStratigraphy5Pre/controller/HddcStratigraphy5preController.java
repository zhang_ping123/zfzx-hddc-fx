package com.css.zfzx.sjcj.modules.hddcStratigraphy5Pre.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.bpm.platform.utils.PlatformPageUtils;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltQueryParams;
import com.css.zfzx.sjcj.modules.hddcStratigraphy5Pre.repository.entity.HddcStratigraphy5preEntity;
import com.css.zfzx.sjcj.modules.hddcStratigraphy5Pre.service.HddcStratigraphy5preService;
import com.css.zfzx.sjcj.modules.hddcStratigraphy5Pre.viewobjects.HddcStratigraphy5preQueryParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcStratigraphy5pres")
public class HddcStratigraphy5preController {
    @Autowired
    private HddcStratigraphy5preService hddcStratigraphy5preService;

    @GetMapping("/queryHddcStratigraphy5pres")
    public RestResponse queryHddcStratigraphy5pres(HttpServletRequest request, HddcStratigraphy5preQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcStratigraphy5preService.queryHddcStratigraphy5pres(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcStratigraphy5pre(@PathVariable String id) {
        RestResponse response = null;
        try{
            HddcStratigraphy5preEntity hddcStratigraphy5pre = hddcStratigraphy5preService.getHddcStratigraphy5pre(id);
            response = RestResponse.succeed(hddcStratigraphy5pre);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcStratigraphy5pre(@RequestBody HddcStratigraphy5preEntity hddcStratigraphy5pre) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcStratigraphy5preService.saveHddcStratigraphy5pre(hddcStratigraphy5pre);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcStratigraphy5pre(@RequestBody HddcStratigraphy5preEntity hddcStratigraphy5pre)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcStratigraphy5preService.updateHddcStratigraphy5pre(hddcStratigraphy5pre);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcStratigraphy5pres(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcStratigraphy5preService.deleteHddcStratigraphy5pres(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcStratigraphy5preService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }
    /***
     * 导出
     * @param response
     * @return
     */
    @GetMapping("/exportFile")
    public RestResponse exportFileYhDisasters(HttpServletResponse response,
                                              @RequestParam("projectName")String projectName,@RequestParam("stratigraphyname") String stratigraphyname,
                                              @RequestParam("province") String province,@RequestParam("city")String city,@RequestParam("area")String area) {
        RestResponse responseRest = null;
        JSONObject jsonObject = new JSONObject();
        try{
            HddcStratigraphy5preQueryParams queryParams=new HddcStratigraphy5preQueryParams();
            queryParams.setArea(area);
            queryParams.setCity(city);
            queryParams.setProvince(province);
            queryParams.setProjectName(projectName);
            queryParams.setStratigraphyname(stratigraphyname);
            hddcStratigraphy5preService.exportFile(queryParams,response);
            jsonObject.put("message", "导出成功");
            responseRest = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            responseRest = RestResponse.fail(errorMessage);
        }
        return responseRest;
    }

    /**
     * 导入
     *
     * @param file
     * @param response
     * @return
     */
    @PostMapping("/importDisaster")
    public RestResponse export(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        RestResponse restResponse = null;
        try {
            String s = hddcStratigraphy5preService.exportExcel( file, response);
            restResponse = RestResponse.succeed(s);
        } catch (Exception e) {
            String errormessage = "导入失败";
            log.error(errormessage, e);
            restResponse = RestResponse.fail(errormessage);
        }
        return restResponse;
    }
}