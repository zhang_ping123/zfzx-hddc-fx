package com.css.zfzx.sjcj.modules.hddccjproject.viewobjects;

import lombok.Data;

/**
 * @author zhangping
 * @date 2020-11-26
 */
@Data
public class HddcCjProjectQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

}
