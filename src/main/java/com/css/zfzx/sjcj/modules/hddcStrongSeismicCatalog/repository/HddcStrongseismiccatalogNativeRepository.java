package com.css.zfzx.sjcj.modules.hddcStrongSeismicCatalog.repository;

import com.css.zfzx.sjcj.modules.hddcStrongSeismicCatalog.repository.entity.HddcStrongseismiccatalogEntity;
import com.css.zfzx.sjcj.modules.hddcStrongSeismicCatalog.viewobjects.HddcStrongseismiccatalogQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */
public interface HddcStrongseismiccatalogNativeRepository {

    Page<HddcStrongseismiccatalogEntity> queryHddcStrongseismiccatalogs(HddcStrongseismiccatalogQueryParams queryParams, int curPage, int pageSize);

    List<HddcStrongseismiccatalogEntity> exportYhDisasters(HddcStrongseismiccatalogQueryParams queryParams);
}
