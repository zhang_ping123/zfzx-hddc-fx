package com.css.zfzx.sjcj.modules.hddcB5_GeophySvyProjectTable.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltQueryParams;
import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyProjectTable.repository.entity.HddcB5GeophysvyprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyProjectTable.service.HddcB5GeophysvyprojecttableService;
import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyProjectTable.viewobjects.HddcB5GeophysvyprojecttableQueryParams;
import com.css.bpm.platform.utils.PlatformPageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcB5Geophysvyprojecttables")
public class HddcB5GeophysvyprojecttableController {
    @Autowired
    private HddcB5GeophysvyprojecttableService hddcB5GeophysvyprojecttableService;

    @GetMapping("/queryHddcB5Geophysvyprojecttables")
    public RestResponse queryHddcB5Geophysvyprojecttables(HttpServletRequest request, HddcB5GeophysvyprojecttableQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcB5GeophysvyprojecttableService.queryHddcB5Geophysvyprojecttables(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcB5Geophysvyprojecttable(@PathVariable String id) {
        RestResponse response = null;
        try{
            HddcB5GeophysvyprojecttableEntity hddcB5Geophysvyprojecttable = hddcB5GeophysvyprojecttableService.getHddcB5Geophysvyprojecttable(id);
            response = RestResponse.succeed(hddcB5Geophysvyprojecttable);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcB5Geophysvyprojecttable(@RequestBody HddcB5GeophysvyprojecttableEntity hddcB5Geophysvyprojecttable) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB5GeophysvyprojecttableService.saveHddcB5Geophysvyprojecttable(hddcB5Geophysvyprojecttable);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcB5Geophysvyprojecttable(@RequestBody HddcB5GeophysvyprojecttableEntity hddcB5Geophysvyprojecttable)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB5GeophysvyprojecttableService.updateHddcB5Geophysvyprojecttable(hddcB5Geophysvyprojecttable);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcB5Geophysvyprojecttables(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB5GeophysvyprojecttableService.deleteHddcB5Geophysvyprojecttables(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcB5GeophysvyprojecttableService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }
    /***
     * 导出
     * @param response
     * @return
     */
    @GetMapping("/exportFile")
    public RestResponse exportFileYhDisasters(HttpServletResponse response,
                                              @RequestParam("projectName")String projectName,@RequestParam("name") String name,
                                              @RequestParam("province") String province,@RequestParam("city")String city,@RequestParam("area")String area) {
        RestResponse responseRest = null;
        JSONObject jsonObject = new JSONObject();
        try{
            HddcB5GeophysvyprojecttableQueryParams queryParams=new HddcB5GeophysvyprojecttableQueryParams();
            queryParams.setArea(area);
            queryParams.setCity(city);
            queryParams.setProvince(province);
            queryParams.setProjectName(projectName);
            queryParams.setName(name);
            hddcB5GeophysvyprojecttableService.exportFile(queryParams,response);
            jsonObject.put("message", "导出成功");
            responseRest = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            responseRest = RestResponse.fail(errorMessage);
        }
        return responseRest;
    }

    /**
     * 导入
     *
     * @param file
     * @param response
     * @return
     */
    @PostMapping("/importDisaster")
    public RestResponse export(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        RestResponse restResponse = null;
        try {
            String s = hddcB5GeophysvyprojecttableService.exportExcel( file, response);
            restResponse = RestResponse.succeed(s);
        } catch (Exception e) {
            String errormessage = "导入失败";
            log.error(errormessage, e);
            restResponse = RestResponse.fail(errormessage);
        }
        return restResponse;
    }
}