package com.css.zfzx.sjcj.modules.hddcRock5LinePre.repository;

import com.css.zfzx.sjcj.modules.hddcRock5LinePre.repository.entity.HddcRock5linepreEntity;
import com.css.zfzx.sjcj.modules.hddcRock5LinePre.viewobjects.HddcRock5linepreQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcRock5linepreNativeRepository {

    Page<HddcRock5linepreEntity> queryHddcRock5linepres(HddcRock5linepreQueryParams queryParams, int curPage, int pageSize);

    List<HddcRock5linepreEntity> exportYhDisasters(HddcRock5linepreQueryParams queryParams);
}
