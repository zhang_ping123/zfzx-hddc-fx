package com.css.zfzx.sjcj.modules.hddcGeomorphySvyRegion.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyRegion.repository.entity.HddcGeomorphysvyregionEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyRegion.viewobjects.HddcGeomorphysvyregionQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */

public interface HddcGeomorphysvyregionService {

    public JSONObject queryHddcGeomorphysvyregions(HddcGeomorphysvyregionQueryParams queryParams, int curPage, int pageSize);

    public HddcGeomorphysvyregionEntity getHddcGeomorphysvyregion(String id);

    public HddcGeomorphysvyregionEntity saveHddcGeomorphysvyregion(HddcGeomorphysvyregionEntity hddcGeomorphysvyregion);

    public HddcGeomorphysvyregionEntity updateHddcGeomorphysvyregion(HddcGeomorphysvyregionEntity hddcGeomorphysvyregion);

    public void deleteHddcGeomorphysvyregions(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGeomorphysvyregionQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
