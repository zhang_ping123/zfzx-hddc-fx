package com.css.zfzx.sjcj.modules.hddcB1GeomorPlyHasGeoSvyPt.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyHasGeoSvyPt.repository.entity.HddcB1GeomorplyhasgeosvyptEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyHasGeoSvyPt.viewobjects.HddcB1GeomorplyhasgeosvyptQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */

public interface HddcB1GeomorplyhasgeosvyptService {

    public JSONObject queryHddcB1Geomorplyhasgeosvypts(HddcB1GeomorplyhasgeosvyptQueryParams queryParams, int curPage, int pageSize);

    public HddcB1GeomorplyhasgeosvyptEntity getHddcB1Geomorplyhasgeosvypt(String id);

    public HddcB1GeomorplyhasgeosvyptEntity saveHddcB1Geomorplyhasgeosvypt(HddcB1GeomorplyhasgeosvyptEntity hddcB1Geomorplyhasgeosvypt);

    public HddcB1GeomorplyhasgeosvyptEntity updateHddcB1Geomorplyhasgeosvypt(HddcB1GeomorplyhasgeosvyptEntity hddcB1Geomorplyhasgeosvypt);

    public void deleteHddcB1Geomorplyhasgeosvypts(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB1GeomorplyhasgeosvyptQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
