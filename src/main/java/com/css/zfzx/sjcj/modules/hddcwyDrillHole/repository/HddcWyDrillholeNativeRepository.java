package com.css.zfzx.sjcj.modules.hddcwyDrillHole.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyDrillHole.repository.entity.HddcWyDrillholeEntity;
import com.css.zfzx.sjcj.modules.hddcwyDrillHole.viewobjects.HddcWyDrillholeQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */
public interface HddcWyDrillholeNativeRepository {

    Page<HddcWyDrillholeEntity> queryHddcWyDrillholes(HddcWyDrillholeQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyDrillhole(HddcAppZztCountVo queryParams);

    List<HddcWyDrillholeEntity> exportHole(HddcAppZztCountVo queryParams);
}
