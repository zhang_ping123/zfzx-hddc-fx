package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvySamplePoint.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvySamplePoint.repository.entity.HddcWyGeomorphysvysamplepointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvySamplePoint.viewobjects.HddcWyGeomorphysvysamplepointQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;
import java.util.List;

/**
 * @author lihelei
 * @date 2020-12-01
 */
public interface HddcWyGeomorphysvysamplepointNativeRepository {

    Page<HddcWyGeomorphysvysamplepointEntity> queryHddcWyGeomorphysvysamplepoints(HddcWyGeomorphysvysamplepointQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyGeomorphysvysamplepoint(HddcAppZztCountVo queryParams);

    List<HddcWyGeomorphysvysamplepointEntity> exportSamplePoint(HddcAppZztCountVo queryParams);
}
