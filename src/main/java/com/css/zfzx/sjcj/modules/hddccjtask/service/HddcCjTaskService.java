package com.css.zfzx.sjcj.modules.hddccjtask.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddccjtask.repository.entity.HddcCjTaskEntity;
import com.css.zfzx.sjcj.modules.hddccjtask.viewobjects.HddcCjTaskQueryParams;
import com.css.zfzx.sjcj.modules.hddccjtask.viewobjects.HddcCjTaskVO;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import java.util.List;

/**
 * @author zhangping
 * @date 2020-11-26
 */

public interface HddcCjTaskService {

    public JSONObject queryHddcCjTasks(HddcCjTaskQueryParams queryParams, int curPage, int pageSize);

    public HddcCjTaskVO getHddcCjTask(String id);

    public HddcCjTaskEntity saveHddcCjTask(HddcCjTaskEntity hddcCjTask);

    public HddcCjTaskEntity updateHddcCjTask(HddcCjTaskEntity hddcCjTask);

    public void deleteHddcCjTasks(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    List<HddcCjTaskEntity> findAll();

    List<HddcCjTaskEntity> findHddcCjTaskEntityByProjectIds(String projectid);

    public JSONObject queryHddcCjTasksByProjectId(String projectId, int curPage, int pageSize);

}
