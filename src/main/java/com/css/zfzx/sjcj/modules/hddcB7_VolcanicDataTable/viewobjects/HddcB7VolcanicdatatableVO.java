package com.css.zfzx.sjcj.modules.hddcB7_VolcanicDataTable.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-26
 */
@Data
public class HddcB7VolcanicdatatableVO implements Serializable {

    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备注
     */
    @Excel(name = "备注")
    private String commentinfo;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 元素类型
     */
    @Excel(name = "元素类型")
    private Integer elementtype;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 采样点编号
     */
    @Excel(name = "采样点编号")
    private String samplepointid;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）",orderNum = "3")
    private String area;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 采样照片文件编号
     */
    @Excel(name = "采样照片文件编号")
    private String photoAiid;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 采样点图原始文件编号
     */
    @Excel(name = "采样点图原始文件编号")
    private String samplelayoutArwid;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 野外编号
     */
    @Excel(name = "野外编号")
    private String fieldid;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 村
     */
    private String village;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 采样点图图表文件编号
     */
    @Excel(name = "采样点图图表文件编号")
    private String samplelayoutAiid;
    /**
     * 市
     */
    @Excel(name = "市",orderNum = "2")
    private String city;
    /**
     * 照片集镜向及拍摄者说明文档
     */
    @Excel(name = "照片集镜向及拍摄者说明文档")
    private String photodescArwid;
    /**
     * 所属工程编号
     */
    @Excel(name = "所属工程编号")
    private String projectid;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 采样照片原始文件编号
     */
    @Excel(name = "采样照片原始文件编号")
    private String photoArwid;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 可靠性评估
     */
    @Excel(name = "可靠性评估")
    private Integer reliability;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 测量结果
     */
    @Excel(name = "测量结果")
    private String svyresult;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 记录编号
     */
    private String id;
    /**
     * 元素名称
     */
    @Excel(name = "元素名称")
    private String name;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 省
     */
    @Excel(name = "省",orderNum = "1")
    private String province;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备注
     */
    private String remark;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer reliabilityName;

    private String rowNum;
    private String errorMsg;
}