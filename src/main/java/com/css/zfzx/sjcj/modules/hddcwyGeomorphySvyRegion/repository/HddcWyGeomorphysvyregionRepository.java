package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyRegion.repository;

import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyRegion.repository.entity.HddcWyGeomorphysvyregionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author lihelei
 * @date 2020-12-01
 */
public interface HddcWyGeomorphysvyregionRepository extends JpaRepository<HddcWyGeomorphysvyregionEntity, String> {

    List<HddcWyGeomorphysvyregionEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    List<HddcWyGeomorphysvyregionEntity> findAllByCreateUserAndIsValid(String userId,String isValid);

    @Query(nativeQuery = true, value = "select * from hddc_wy_geomorphysvyregion where is_valid!=0 project_name in :projectIds")
    List<HddcWyGeomorphysvyregionEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_geomorphysvyregion where task_name in :projectIds")
    List<HddcWyGeomorphysvyregionEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);
}
