package com.css.zfzx.sjcj.modules.hddcB1GeomorPlyOnFractBlt.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyOnFractBlt.repository.HddcB1GeomorplyonfractbltNativeRepository;
import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyOnFractBlt.repository.HddcB1GeomorplyonfractbltRepository;
import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyOnFractBlt.repository.entity.HddcB1GeomorplyonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyOnFractBlt.service.HddcB1GeomorplyonfractbltService;
import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyOnFractBlt.viewobjects.HddcB1GeomorplyonfractbltQueryParams;
import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyOnFractBlt.viewobjects.HddcB1GeomorplyonfractbltVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Service
public class HddcB1GeomorplyonfractbltServiceImpl implements HddcB1GeomorplyonfractbltService {

	@Autowired
    private HddcB1GeomorplyonfractbltRepository hddcB1GeomorplyonfractbltRepository;
    @Autowired
    private HddcB1GeomorplyonfractbltNativeRepository hddcB1GeomorplyonfractbltNativeRepository;

    @Override
    public JSONObject queryHddcB1Geomorplyonfractblts(HddcB1GeomorplyonfractbltQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcB1GeomorplyonfractbltEntity> hddcB1GeomorplyonfractbltPage = this.hddcB1GeomorplyonfractbltNativeRepository.queryHddcB1Geomorplyonfractblts(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcB1GeomorplyonfractbltPage);
        return jsonObject;
    }


    @Override
    public HddcB1GeomorplyonfractbltEntity getHddcB1Geomorplyonfractblt(String id) {
        HddcB1GeomorplyonfractbltEntity hddcB1Geomorplyonfractblt = this.hddcB1GeomorplyonfractbltRepository.findById(id).orElse(null);
         return hddcB1Geomorplyonfractblt;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB1GeomorplyonfractbltEntity saveHddcB1Geomorplyonfractblt(HddcB1GeomorplyonfractbltEntity hddcB1Geomorplyonfractblt) {
        String uuid = UUIDGenerator.getUUID();
        hddcB1Geomorplyonfractblt.setUuid(uuid);
        hddcB1Geomorplyonfractblt.setCreateUser(PlatformSessionUtils.getUserId());
        hddcB1Geomorplyonfractblt.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB1GeomorplyonfractbltRepository.save(hddcB1Geomorplyonfractblt);
        return hddcB1Geomorplyonfractblt;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB1GeomorplyonfractbltEntity updateHddcB1Geomorplyonfractblt(HddcB1GeomorplyonfractbltEntity hddcB1Geomorplyonfractblt) {
        HddcB1GeomorplyonfractbltEntity entity = hddcB1GeomorplyonfractbltRepository.findById(hddcB1Geomorplyonfractblt.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcB1Geomorplyonfractblt);
        hddcB1Geomorplyonfractblt.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcB1Geomorplyonfractblt.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB1GeomorplyonfractbltRepository.save(hddcB1Geomorplyonfractblt);
        return hddcB1Geomorplyonfractblt;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcB1Geomorplyonfractblts(List<String> ids) {
        List<HddcB1GeomorplyonfractbltEntity> hddcB1GeomorplyonfractbltList = this.hddcB1GeomorplyonfractbltRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcB1GeomorplyonfractbltList) && hddcB1GeomorplyonfractbltList.size() > 0) {
            for(HddcB1GeomorplyonfractbltEntity hddcB1Geomorplyonfractblt : hddcB1GeomorplyonfractbltList) {
                this.hddcB1GeomorplyonfractbltRepository.delete(hddcB1Geomorplyonfractblt);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcB1GeomorplyonfractbltQueryParams queryParams, HttpServletResponse response) {
        List<HddcB1GeomorplyonfractbltEntity> yhDisasterEntities = hddcB1GeomorplyonfractbltNativeRepository.exportYhDisasters(queryParams);
        List<HddcB1GeomorplyonfractbltVO> list=new ArrayList<>();
        for (HddcB1GeomorplyonfractbltEntity entity:yhDisasterEntities) {
            HddcB1GeomorplyonfractbltVO yhDisasterVO=new HddcB1GeomorplyonfractbltVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"地貌面与地表破裂带关联表","地貌面与地表破裂带关联表",HddcB1GeomorplyonfractbltVO.class,"地貌面与地表破裂带关联表.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcB1GeomorplyonfractbltVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcB1GeomorplyonfractbltVO.class, params);
            List<HddcB1GeomorplyonfractbltVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcB1GeomorplyonfractbltVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcB1GeomorplyonfractbltVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcB1GeomorplyonfractbltVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcB1GeomorplyonfractbltEntity yhDisasterEntity = new HddcB1GeomorplyonfractbltEntity();
            HddcB1GeomorplyonfractbltVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcB1Geomorplyonfractblt(yhDisasterEntity);
        }
    }

}
