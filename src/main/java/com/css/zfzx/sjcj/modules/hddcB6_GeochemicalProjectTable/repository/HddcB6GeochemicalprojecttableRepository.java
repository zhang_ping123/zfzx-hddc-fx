package com.css.zfzx.sjcj.modules.hddcB6_GeochemicalProjectTable.repository;

import com.css.zfzx.sjcj.modules.hddcB6_GeochemicalProjectTable.repository.entity.HddcB6GeochemicalprojecttableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
public interface HddcB6GeochemicalprojecttableRepository extends JpaRepository<HddcB6GeochemicalprojecttableEntity, String> {
}
