package com.css.zfzx.sjcj.modules.hddcTargetRegion.repository;

import com.css.zfzx.sjcj.modules.hddcTargetRegion.repository.entity.HddcTargetregionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcTargetregionRepository extends JpaRepository<HddcTargetregionEntity, String> {
}
