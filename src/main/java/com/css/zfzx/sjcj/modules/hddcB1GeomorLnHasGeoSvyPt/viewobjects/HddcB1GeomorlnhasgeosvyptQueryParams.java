package com.css.zfzx.sjcj.modules.hddcB1GeomorLnHasGeoSvyPt.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Data
public class HddcB1GeomorlnhasgeosvyptQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

}
