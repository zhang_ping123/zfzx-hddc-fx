package com.css.zfzx.sjcj.modules.hddcB1GeologySvyProjectTable.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeologySvyProjectTable.repository.HddcB1GeologysvyprojecttableNativeRepository;
import com.css.zfzx.sjcj.modules.hddcB1GeologySvyProjectTable.repository.HddcB1GeologysvyprojecttableRepository;
import com.css.zfzx.sjcj.modules.hddcB1GeologySvyProjectTable.repository.entity.HddcB1GeologysvyprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeologySvyProjectTable.service.HddcB1GeologysvyprojecttableService;
import com.css.zfzx.sjcj.modules.hddcB1GeologySvyProjectTable.viewobjects.HddcB1GeologysvyprojecttableQueryParams;
import com.css.zfzx.sjcj.modules.hddcB1GeologySvyProjectTable.viewobjects.HddcB1GeologysvyprojecttableVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Service
public class HddcB1GeologysvyprojecttableServiceImpl implements HddcB1GeologysvyprojecttableService {

	@Autowired
    private HddcB1GeologysvyprojecttableRepository hddcB1GeologysvyprojecttableRepository;
    @Autowired
    private HddcB1GeologysvyprojecttableNativeRepository hddcB1GeologysvyprojecttableNativeRepository;

    @Override
    public JSONObject queryHddcB1Geologysvyprojecttables(HddcB1GeologysvyprojecttableQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcB1GeologysvyprojecttableEntity> hddcB1GeologysvyprojecttablePage = this.hddcB1GeologysvyprojecttableNativeRepository.queryHddcB1Geologysvyprojecttables(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcB1GeologysvyprojecttablePage);
        return jsonObject;
    }


    @Override
    public HddcB1GeologysvyprojecttableEntity getHddcB1Geologysvyprojecttable(String id) {
        HddcB1GeologysvyprojecttableEntity hddcB1Geologysvyprojecttable = this.hddcB1GeologysvyprojecttableRepository.findById(id).orElse(null);
         return hddcB1Geologysvyprojecttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB1GeologysvyprojecttableEntity saveHddcB1Geologysvyprojecttable(HddcB1GeologysvyprojecttableEntity hddcB1Geologysvyprojecttable) {
        String uuid = UUIDGenerator.getUUID();
        hddcB1Geologysvyprojecttable.setUuid(uuid);
        hddcB1Geologysvyprojecttable.setCreateUser(PlatformSessionUtils.getUserId());
        hddcB1Geologysvyprojecttable.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB1GeologysvyprojecttableRepository.save(hddcB1Geologysvyprojecttable);
        return hddcB1Geologysvyprojecttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB1GeologysvyprojecttableEntity updateHddcB1Geologysvyprojecttable(HddcB1GeologysvyprojecttableEntity hddcB1Geologysvyprojecttable) {
        HddcB1GeologysvyprojecttableEntity entity = hddcB1GeologysvyprojecttableRepository.findById(hddcB1Geologysvyprojecttable.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcB1Geologysvyprojecttable);
        hddcB1Geologysvyprojecttable.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcB1Geologysvyprojecttable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB1GeologysvyprojecttableRepository.save(hddcB1Geologysvyprojecttable);
        return hddcB1Geologysvyprojecttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcB1Geologysvyprojecttables(List<String> ids) {
        List<HddcB1GeologysvyprojecttableEntity> hddcB1GeologysvyprojecttableList = this.hddcB1GeologysvyprojecttableRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcB1GeologysvyprojecttableList) && hddcB1GeologysvyprojecttableList.size() > 0) {
            for(HddcB1GeologysvyprojecttableEntity hddcB1Geologysvyprojecttable : hddcB1GeologysvyprojecttableList) {
                this.hddcB1GeologysvyprojecttableRepository.delete(hddcB1Geologysvyprojecttable);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcB1GeologysvyprojecttableQueryParams queryParams, HttpServletResponse response) {
        List<HddcB1GeologysvyprojecttableEntity> yhDisasterEntities = hddcB1GeologysvyprojecttableNativeRepository.exportYhDisasters(queryParams);
        List<HddcB1GeologysvyprojecttableVO> list=new ArrayList<>();
        for (HddcB1GeologysvyprojecttableEntity entity:yhDisasterEntities) {
            HddcB1GeologysvyprojecttableVO yhDisasterVO=new HddcB1GeologysvyprojecttableVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "地质调查工程表", "地质调查工程表", HddcB1GeologysvyprojecttableVO.class, "地质调查工程表.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcB1GeologysvyprojecttableVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcB1GeologysvyprojecttableVO.class, params);
            List<HddcB1GeologysvyprojecttableVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcB1GeologysvyprojecttableVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcB1GeologysvyprojecttableVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcB1GeologysvyprojecttableVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcB1GeologysvyprojecttableEntity yhDisasterEntity = new HddcB1GeologysvyprojecttableEntity();
            HddcB1GeologysvyprojecttableVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcB1Geologysvyprojecttable(yhDisasterEntity);
        }
    }

}
