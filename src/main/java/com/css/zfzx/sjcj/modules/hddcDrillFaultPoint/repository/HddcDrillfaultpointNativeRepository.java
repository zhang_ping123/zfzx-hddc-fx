package com.css.zfzx.sjcj.modules.hddcDrillFaultPoint.repository;

import com.css.zfzx.sjcj.modules.hddcDrillFaultPoint.repository.entity.HddcDrillfaultpointEntity;
import com.css.zfzx.sjcj.modules.hddcDrillFaultPoint.viewobjects.HddcDrillfaultpointQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcDrillfaultpointNativeRepository {

    Page<HddcDrillfaultpointEntity> queryHddcDrillfaultpoints(HddcDrillfaultpointQueryParams queryParams, int curPage, int pageSize);

    List<HddcDrillfaultpointEntity> exportYhDisasters(HddcDrillfaultpointQueryParams queryParams);
}
