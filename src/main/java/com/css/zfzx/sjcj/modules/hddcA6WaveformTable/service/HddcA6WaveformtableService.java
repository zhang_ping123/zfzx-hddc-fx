package com.css.zfzx.sjcj.modules.hddcA6WaveformTable.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcA6WaveformTable.repository.entity.HddcA6WaveformtableEntity;
import com.css.zfzx.sjcj.modules.hddcA6WaveformTable.viewobjects.HddcA6WaveformtableQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */

public interface HddcA6WaveformtableService {

    public JSONObject queryHddcA6Waveformtables(HddcA6WaveformtableQueryParams queryParams, int curPage, int pageSize);

    public HddcA6WaveformtableEntity getHddcA6Waveformtable(String id);

    public HddcA6WaveformtableEntity saveHddcA6Waveformtable(HddcA6WaveformtableEntity hddcA6Waveformtable);

    public HddcA6WaveformtableEntity updateHddcA6Waveformtable(HddcA6WaveformtableEntity hddcA6Waveformtable);

    public void deleteHddcA6Waveformtables(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcA6WaveformtableQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
