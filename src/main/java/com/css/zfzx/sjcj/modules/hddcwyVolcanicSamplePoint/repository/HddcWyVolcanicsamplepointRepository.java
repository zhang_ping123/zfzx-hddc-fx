package com.css.zfzx.sjcj.modules.hddcwyVolcanicSamplePoint.repository;

import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyVolcanicSamplePoint.repository.entity.HddcWyVolcanicsamplepointEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-02
 */
public interface HddcWyVolcanicsamplepointRepository extends JpaRepository<HddcWyVolcanicsamplepointEntity, String> {

    List<HddcWyVolcanicsamplepointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    List<HddcWyVolcanicsamplepointEntity> findAllByCreateUserAndIsValid(String userId,String isValid);

    @Query(nativeQuery = true, value = "select * from hddc_wy_volcanicsamplepoint where is_valid!=0 project_name in :projectIds")
    List<HddcWyVolcanicsamplepointEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_volcanicsamplepoint where task_name in :projectIds")
    List<HddcWyVolcanicsamplepointEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);
}

