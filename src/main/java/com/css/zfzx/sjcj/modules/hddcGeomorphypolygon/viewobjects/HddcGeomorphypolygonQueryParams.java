package com.css.zfzx.sjcj.modules.hddcGeomorphypolygon.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-12-09
 */
@Data
public class HddcGeomorphypolygonQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String geomorphyname;

}
