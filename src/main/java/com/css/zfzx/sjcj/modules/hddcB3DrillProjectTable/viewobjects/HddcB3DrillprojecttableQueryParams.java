package com.css.zfzx.sjcj.modules.hddcB3DrillProjectTable.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Data
public class HddcB3DrillprojecttableQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String name;

}
