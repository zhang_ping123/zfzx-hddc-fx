package com.css.zfzx.sjcj.modules.yhchemical.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.yhchemical.repository.entity.YhChemicalEntity;
import com.css.zfzx.sjcj.modules.yhchemical.viewobjects.YhChemicalQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import java.util.List;

/**
 * @author ly
 * @date 2020-11-04
 */

public interface YhChemicalService {

    public JSONObject queryYhChemicals(YhChemicalQueryParams queryParams, int curPage, int pageSize);

    public YhChemicalEntity getYhChemical(String id);

    public YhChemicalEntity saveYhChemical(YhChemicalEntity yhChemical);

    public YhChemicalEntity updateYhChemical(YhChemicalEntity yhChemical);

    public void deleteYhChemicals(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

}
