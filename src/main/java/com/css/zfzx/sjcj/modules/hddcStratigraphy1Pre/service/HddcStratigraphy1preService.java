package com.css.zfzx.sjcj.modules.hddcStratigraphy1Pre.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcStratigraphy1Pre.repository.entity.HddcStratigraphy1preEntity;
import com.css.zfzx.sjcj.modules.hddcStratigraphy1Pre.viewobjects.HddcStratigraphy1preQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */

public interface HddcStratigraphy1preService {

    public JSONObject queryHddcStratigraphy1pres(HddcStratigraphy1preQueryParams queryParams, int curPage, int pageSize);

    public HddcStratigraphy1preEntity getHddcStratigraphy1pre(String id);

    public HddcStratigraphy1preEntity saveHddcStratigraphy1pre(HddcStratigraphy1preEntity hddcStratigraphy1pre);

    public HddcStratigraphy1preEntity updateHddcStratigraphy1pre(HddcStratigraphy1preEntity hddcStratigraphy1pre);

    public void deleteHddcStratigraphy1pres(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    String findByDictCodeAndDictItemCode(String dictCode, String dictItemCode);

    void exportFile(HddcStratigraphy1preQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
