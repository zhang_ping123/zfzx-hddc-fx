package com.css.zfzx.sjcj.modules.hddcVolcanicSvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcVolcanicSvyPoint.repository.entity.HddcVolcanicsvypointEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
public interface HddcVolcanicsvypointRepository extends JpaRepository<HddcVolcanicsvypointEntity, String> {
}
