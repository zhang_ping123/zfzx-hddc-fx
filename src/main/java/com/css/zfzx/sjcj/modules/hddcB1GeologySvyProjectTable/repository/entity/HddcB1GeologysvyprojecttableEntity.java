package com.css.zfzx.sjcj.modules.hddcB1GeologySvyProjectTable.repository.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
@Entity
@Table(name="hddc_b1_geologysvyprojecttable")
public class HddcB1GeologysvyprojecttableEntity implements Serializable {

    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 工作区编号
     */
    @Column(name="workregionid")
    private String workregionid;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 备注
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 工程名称
     */
    @Column(name="name")
    private String name;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 工程设计报告原始文件编号
     */
    @Column(name="db_arwid")
    private String dbArwid;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;
    /**
     * 工程编号
     */
    @Column(name="id")
    private String id;
    /**
     * 编号
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 获得测试结果样品总数
     */
    @Column(name="datingsamplecount")
    private Integer datingsamplecount;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 成果报告文件报告编号
     */
    @Column(name="report_arid")
    private String reportArid;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 变更情况
     */
    @Column(name="cs_arid")
    private String csArid;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 样品送样总数
     */
    @Column(name="samplecount")
    private Integer samplecount;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 变更情况原始
     */
    @Column(name="cs_arwid")
    private String csArwid;
    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 地质填图区编号
     */
    @Column(name="mainafsregionid")
    private String mainafsregionid;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 地质测量点总数
     */
    @Column(name="geolosvyptcount")
    private Integer geolosvyptcount;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 目标区编号
     */
    @Column(name="targetregionid")
    private String targetregionid;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 探测总土方量 [m³]
     */
    @Column(name="trenchvolume")
    private Double trenchvolume;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 探槽总数
     */
    @Column(name="trenchcount")
    private Integer trenchcount;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 工程设计报告编号
     */
    @Column(name="db_arid")
    private String dbArid;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 成果报告原始文件编号
     */
    @Column(name="report_arwid")
    private String reportArwid;
    /**
     * 采集样品总数
     */
    @Column(name="collectedsamplecount")
    private Integer collectedsamplecount;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;

}

