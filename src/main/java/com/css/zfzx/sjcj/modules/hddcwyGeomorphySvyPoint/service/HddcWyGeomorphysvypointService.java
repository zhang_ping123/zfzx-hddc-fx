package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.repository.entity.HddcWyGeomorphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.viewobjects.HddcWyGeomorphysvypointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

/**
 * @author lihelei
 * @date 2020-12-01
 */

public interface HddcWyGeomorphysvypointService {

    public JSONObject queryHddcWyGeomorphysvypoints(HddcWyGeomorphysvypointQueryParams queryParams, int curPage, int pageSize);

    public HddcWyGeomorphysvypointEntity getHddcWyGeomorphysvypoint(String uuid);

    public HddcWyGeomorphysvypointEntity saveHddcWyGeomorphysvypoint(HddcWyGeomorphysvypointEntity hddcWyGeomorphysvypoint);

    public HddcWyGeomorphysvypointEntity updateHddcWyGeomorphysvypoint(HddcWyGeomorphysvypointEntity hddcWyGeomorphysvypoint);

    public void deleteHddcWyGeomorphysvypoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    BigInteger queryHddcWyGeomorphysvypoint(HddcAppZztCountVo queryParams);

    List<HddcWyGeomorphysvypointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);

    List<HddcWyGeomorphysvypointEntity> findAllByCreateUserAndIsValid(String userId,String isValid);


    /**
     * 逻辑删除-根据项目id删除数据
     * @param ids
     */
    void deleteByProjectId(List<String> ids);

    /**
     * 逻辑删除-根据任务id删除数据
     * @param ids
     */
    void deleteByTaskId(List<String> ids);



}
