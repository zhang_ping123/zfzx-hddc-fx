package com.css.zfzx.sjcj.modules.hddcAviationMagnetic.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-26
 */
@Data
public class HddcAviationmagneticVO implements Serializable {

    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 乡
     */
    private String town;
    /**
     * 市
     */
    @Excel(name = "市",orderNum = "2")
    private String city;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）",orderNum = "3")
    private String area;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 数值
     */
    @Excel(name = "数值")
    private Integer magnitude;
    /**
     * 编号
     */
    private String id;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备注
     */
    @Excel(name = "备注")
    private String remark;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备注
     */
    private String commentInfo;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 名称
     */
    @Excel(name = "名称")
    private String magnitudename;
    /**
     * 省
     */
    @Excel(name = "省",orderNum = "1")
    private String province;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 显示码
     */
    @Excel(name = "显示码")
    private Integer showcode;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段6
     */
    private String extends6;

    private String provinceName;
    private String cityName;
    private String areaName;

    private String rowNum;
    private String errorMsg;
}