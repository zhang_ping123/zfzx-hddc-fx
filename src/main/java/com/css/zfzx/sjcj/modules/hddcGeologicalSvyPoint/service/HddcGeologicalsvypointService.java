package com.css.zfzx.sjcj.modules.hddcGeologicalSvyPoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPoint.repository.entity.HddcGeologicalsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPoint.viewobjects.HddcGeologicalsvypointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author lihelei
 * @date 2020-11-27
 */

public interface HddcGeologicalsvypointService {

    public JSONObject queryHddcGeologicalsvypoints(HddcGeologicalsvypointQueryParams queryParams, int curPage, int pageSize);

    public HddcGeologicalsvypointEntity getHddcGeologicalsvypoint(String id);

    public HddcGeologicalsvypointEntity saveHddcGeologicalsvypoint(HddcGeologicalsvypointEntity hddcGeologicalsvypoint);

    public HddcGeologicalsvypointEntity updateHddcGeologicalsvypoint(HddcGeologicalsvypointEntity hddcGeologicalsvypoint);

    public void deleteHddcGeologicalsvypoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGeologicalsvypointQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
