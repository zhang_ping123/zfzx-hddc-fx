package com.css.zfzx.sjcj.modules.hddcGeomorphySvyPoint.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Data
public class HddcGeomorphysvypointQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

}
