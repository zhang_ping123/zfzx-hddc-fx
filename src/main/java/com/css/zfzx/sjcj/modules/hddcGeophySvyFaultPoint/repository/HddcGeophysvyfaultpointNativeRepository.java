package com.css.zfzx.sjcj.modules.hddcGeophySvyFaultPoint.repository;

import com.css.zfzx.sjcj.modules.hddcGeophySvyFaultPoint.repository.entity.HddcGeophysvyfaultpointEntity;
import com.css.zfzx.sjcj.modules.hddcGeophySvyFaultPoint.viewobjects.HddcGeophysvyfaultpointQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcGeophysvyfaultpointNativeRepository {

    Page<HddcGeophysvyfaultpointEntity> queryHddcGeophysvyfaultpoints(HddcGeophysvyfaultpointQueryParams queryParams, int curPage, int pageSize);

    List<HddcGeophysvyfaultpointEntity> exportYhDisasters(HddcGeophysvyfaultpointQueryParams queryParams);
}
