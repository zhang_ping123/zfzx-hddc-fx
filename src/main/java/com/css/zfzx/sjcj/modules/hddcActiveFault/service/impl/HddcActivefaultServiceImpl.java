package com.css.zfzx.sjcj.modules.hddcActiveFault.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcActiveFault.repository.HddcActivefaultNativeRepository;
import com.css.zfzx.sjcj.modules.hddcActiveFault.repository.HddcActivefaultRepository;
import com.css.zfzx.sjcj.modules.hddcActiveFault.repository.entity.HddcActivefaultEntity;
import com.css.zfzx.sjcj.modules.hddcActiveFault.service.HddcActivefaultService;
import com.css.zfzx.sjcj.modules.hddcActiveFault.viewobjects.HddcActivefaultQueryParams;
import com.css.zfzx.sjcj.modules.hddcActiveFault.viewobjects.HddcActivefaultVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Service
public class HddcActivefaultServiceImpl implements HddcActivefaultService {

	@Autowired
    private HddcActivefaultRepository hddcActivefaultRepository;
    @Autowired
    private HddcActivefaultNativeRepository hddcActivefaultNativeRepository;

    @Override
    public JSONObject queryHddcActivefaults(HddcActivefaultQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcActivefaultEntity> hddcActivefaultPage = this.hddcActivefaultNativeRepository.queryHddcActivefaults(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcActivefaultPage);
        return jsonObject;
    }


    @Override
    public HddcActivefaultEntity getHddcActivefault(String id) {
        HddcActivefaultEntity hddcActivefault = this.hddcActivefaultRepository.findById(id).orElse(null);
         return hddcActivefault;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcActivefaultEntity saveHddcActivefault(HddcActivefaultEntity hddcActivefault) {
        String uuid = UUIDGenerator.getUUID();
        hddcActivefault.setUuid(uuid);
        hddcActivefault.setCreateUser(PlatformSessionUtils.getUserId());
        hddcActivefault.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcActivefaultRepository.save(hddcActivefault);
        return hddcActivefault;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcActivefaultEntity updateHddcActivefault(HddcActivefaultEntity hddcActivefault) {
        HddcActivefaultEntity entity = hddcActivefaultRepository.findById(hddcActivefault.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcActivefault);
        hddcActivefault.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcActivefault.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcActivefaultRepository.save(hddcActivefault);
        return hddcActivefault;
    }

    @Override
    public List<HddcActivefaultEntity> findAll() {
        List<HddcActivefaultEntity> hddcActivefaultList = this.hddcActivefaultRepository.findAll();
        return hddcActivefaultList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcActivefaults(List<String> ids) {
        List<HddcActivefaultEntity> hddcActivefaultList = this.hddcActivefaultRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcActivefaultList) && hddcActivefaultList.size() > 0) {
            for(HddcActivefaultEntity hddcActivefault : hddcActivefaultList) {
                this.hddcActivefaultRepository.delete(hddcActivefault);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcActivefaultQueryParams queryParams, HttpServletResponse response) {
        List<HddcActivefaultEntity> yhDisasterEntities = hddcActivefaultNativeRepository.exportYhDisasters(queryParams);
        List<HddcActivefaultVO> list=new ArrayList<>();
        for (HddcActivefaultEntity entity:yhDisasterEntities) {
            HddcActivefaultVO yhDisasterVO=new HddcActivefaultVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "活动断层-线", "活动断层-线", HddcActivefaultVO.class, "活动断层-线.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcActivefaultVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcActivefaultVO.class, params);
            List<HddcActivefaultVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcActivefaultVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcActivefaultVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    public void saveDisasterList( List<HddcActivefaultVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcActivefaultEntity yhDisasterEntity = new HddcActivefaultEntity();
            HddcActivefaultVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcActivefault(yhDisasterEntity);
        }
    }



}
