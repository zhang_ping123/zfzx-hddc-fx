package com.css.zfzx.sjcj.modules.hddcStratigraphy1Pre.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltVO;
import com.css.zfzx.sjcj.modules.hddcStratigraphy1Pre.repository.HddcStratigraphy1preNativeRepository;
import com.css.zfzx.sjcj.modules.hddcStratigraphy1Pre.repository.HddcStratigraphy1preRepository;
import com.css.zfzx.sjcj.modules.hddcStratigraphy1Pre.repository.entity.HddcStratigraphy1preEntity;
import com.css.zfzx.sjcj.modules.hddcStratigraphy1Pre.service.HddcStratigraphy1preService;
import com.css.zfzx.sjcj.modules.hddcStratigraphy1Pre.viewobjects.HddcStratigraphy1preQueryParams;
import com.css.zfzx.sjcj.modules.hddcStratigraphy1Pre.viewobjects.HddcStratigraphy1preVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Service
public class HddcStratigraphy1preServiceImpl implements HddcStratigraphy1preService {

	@Autowired
    private HddcStratigraphy1preRepository hddcStratigraphy1preRepository;
    @Autowired
    private HddcStratigraphy1preNativeRepository hddcStratigraphy1preNativeRepository;

    @Override
    public JSONObject queryHddcStratigraphy1pres(HddcStratigraphy1preQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcStratigraphy1preEntity> hddcStratigraphy1prePage = this.hddcStratigraphy1preNativeRepository.queryHddcStratigraphy1pres(queryParams, curPage, pageSize);
        List<HddcStratigraphy1preVO> hddcStratigraphy1preVOList = new ArrayList<>();
        List<HddcStratigraphy1preEntity> hddcStratigraphy1preEntityList = hddcStratigraphy1prePage.getContent();
        for(HddcStratigraphy1preEntity hddcStratigraphy1preEntity : hddcStratigraphy1preEntityList){
            HddcStratigraphy1preVO hddcStratigraphy1preVO = new HddcStratigraphy1preVO();
            BeanUtils.copyProperties(hddcStratigraphy1preEntity, hddcStratigraphy1preVO);
            if(PlatformObjectUtils.isNotEmpty(hddcStratigraphy1preEntity.getSymbol())) {
                hddcStratigraphy1preVO.setSymbolName(findByDictCodeAndDictItemCode("StratigraphyCVD", String.valueOf(hddcStratigraphy1preEntity.getSymbol())));
            }
            hddcStratigraphy1preVOList.add(hddcStratigraphy1preVO);
        }
        Page<HddcStratigraphy1preVO> HddcStratigraphy1preVOPage = new PageImpl(hddcStratigraphy1preVOList, hddcStratigraphy1prePage.getPageable(), hddcStratigraphy1prePage.getTotalElements());
        JSONObject jsonObject = PlatformPageUtils.formatPageData(HddcStratigraphy1preVOPage);
        return jsonObject;
    }


    @Override
    public HddcStratigraphy1preEntity getHddcStratigraphy1pre(String id) {
        HddcStratigraphy1preEntity hddcStratigraphy1pre = this.hddcStratigraphy1preRepository.findById(id).orElse(null);
         return hddcStratigraphy1pre;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcStratigraphy1preEntity saveHddcStratigraphy1pre(HddcStratigraphy1preEntity hddcStratigraphy1pre) {
        String uuid = UUIDGenerator.getUUID();
        hddcStratigraphy1pre.setUuid(uuid);
        hddcStratigraphy1pre.setCreateUser(PlatformSessionUtils.getUserId());
        hddcStratigraphy1pre.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcStratigraphy1preRepository.save(hddcStratigraphy1pre);
        return hddcStratigraphy1pre;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcStratigraphy1preEntity updateHddcStratigraphy1pre(HddcStratigraphy1preEntity hddcStratigraphy1pre) {
        HddcStratigraphy1preEntity entity = hddcStratigraphy1preRepository.findById(hddcStratigraphy1pre.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcStratigraphy1pre);
        hddcStratigraphy1pre.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcStratigraphy1pre.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcStratigraphy1preRepository.save(hddcStratigraphy1pre);
        return hddcStratigraphy1pre;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcStratigraphy1pres(List<String> ids) {
        List<HddcStratigraphy1preEntity> hddcStratigraphy1preList = this.hddcStratigraphy1preRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcStratigraphy1preList) && hddcStratigraphy1preList.size() > 0) {
            for(HddcStratigraphy1preEntity hddcStratigraphy1pre : hddcStratigraphy1preList) {
                this.hddcStratigraphy1preRepository.delete(hddcStratigraphy1pre);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public String findByDictCodeAndDictItemCode(String dictCode, String dictItemCode){
        DictItemEntity dictItem = PlatformAPI.getDictAPI().getValidDictItemByDictCodeAndDictItemCode(dictCode, dictItemCode);
        String dictItemName = "";
        if(PlatformObjectUtils.isNotEmpty(dictItem)){
            dictItemName = dictItem.getDictItemName();
        }
        return dictItemName;
    }

    @Override
    public void exportFile(HddcStratigraphy1preQueryParams queryParams, HttpServletResponse response) {
        List<HddcStratigraphy1preEntity> yhDisasterEntities = hddcStratigraphy1preNativeRepository.exportYhDisasters(queryParams);
        List<HddcStratigraphy1preVO> list=new ArrayList<>();
        for (HddcStratigraphy1preEntity entity:yhDisasterEntities) {
            HddcStratigraphy1preVO yhDisasterVO=new HddcStratigraphy1preVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"1：1万工作底图地层-面","1：1万工作底图地层-面",HddcStratigraphy1preVO.class,"1：1万工作底图地层-面.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcStratigraphy1preVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcStratigraphy1preVO.class, params);
            List<HddcStratigraphy1preVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcStratigraphy1preVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcStratigraphy1preVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcStratigraphy1preVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcStratigraphy1preEntity yhDisasterEntity = new HddcStratigraphy1preEntity();
            HddcStratigraphy1preVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcStratigraphy1pre(yhDisasterEntity);
        }
    }
}
