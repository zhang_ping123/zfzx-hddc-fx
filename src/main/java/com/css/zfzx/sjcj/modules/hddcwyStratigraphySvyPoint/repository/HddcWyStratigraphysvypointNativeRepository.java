package com.css.zfzx.sjcj.modules.hddcwyStratigraphySvyPoint.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyStratigraphySvyPoint.repository.entity.HddcWyStratigraphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyStratigraphySvyPoint.viewobjects.HddcWyStratigraphysvypointQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */
public interface HddcWyStratigraphysvypointNativeRepository {

    Page<HddcWyStratigraphysvypointEntity> queryHddcWyStratigraphysvypoints(HddcWyStratigraphysvypointQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyStratigraphysvypoint(HddcAppZztCountVo queryParams);

    List<HddcWyStratigraphysvypointEntity> exportStratiPoint(HddcAppZztCountVo queryParams);
}
