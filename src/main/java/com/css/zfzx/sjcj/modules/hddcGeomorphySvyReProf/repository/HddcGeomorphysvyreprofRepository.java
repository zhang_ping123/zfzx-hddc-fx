package com.css.zfzx.sjcj.modules.hddcGeomorphySvyReProf.repository;

import com.css.zfzx.sjcj.modules.hddcGeomorphySvyReProf.repository.entity.HddcGeomorphysvyreprofEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcGeomorphysvyreprofRepository extends JpaRepository<HddcGeomorphysvyreprofEntity, String> {
    @Query(nativeQuery = true, value = "select * from hddc_Geomorphysvyreprof where project_name in :projectIds")
    List<HddcGeomorphysvyreprofEntity> queryHddcGeomorphysvyreprofsByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_Geomorphysvyreprof where task_name in :projectIds")
    List<HddcGeomorphysvyreprofEntity> queryHddcGeomorphysvyreprofsByTaskId(List<String> projectIds);






    @Query(nativeQuery = true, value = "select * from hddc_Geomorphysvyreprof where create_user = :createID")
    List<HddcGeomorphysvyreprofEntity> queryHddcGeomorphysvyreprofByCreateuser(String createID);
}
