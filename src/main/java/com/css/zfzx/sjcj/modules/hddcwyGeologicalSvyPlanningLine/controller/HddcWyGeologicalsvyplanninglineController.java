package com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningLine.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningLine.repository.entity.HddcWyGeologicalsvyplanninglineEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningLine.service.HddcWyGeologicalsvyplanninglineService;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningLine.viewobjects.HddcWyGeologicalsvyplanninglineQueryParams;
import com.css.bpm.platform.utils.PlatformPageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhangping
 * @date 2020-12-01
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcWyGeologicalsvyplanninglines")
public class HddcWyGeologicalsvyplanninglineController {
    @Autowired
    private HddcWyGeologicalsvyplanninglineService hddcWyGeologicalsvyplanninglineService;

    @GetMapping("/queryHddcWyGeologicalsvyplanninglines")
    public RestResponse queryHddcWyGeologicalsvyplanninglines(HttpServletRequest request, HddcWyGeologicalsvyplanninglineQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcWyGeologicalsvyplanninglineService.queryHddcWyGeologicalsvyplanninglines(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/findAllByhddcWyGeologicalsvyplanninglines")
    public RestResponse findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid){
        RestResponse response=null;
        try {
            List<HddcWyGeologicalsvyplanninglineEntity> list = hddcWyGeologicalsvyplanninglineService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, isValid);
            response=RestResponse.succeed(list);
        }catch (Exception e){
            String errorMessage="查询失败!";
            log.error(errorMessage,e);
            response=RestResponse.fail(errorMessage);
        }
        return response;
    }
    @GetMapping("{uuid}")
    public RestResponse getHddcWyGeologicalsvyplanningline(@PathVariable String uuid) {
        RestResponse response = null;
        try{
            HddcWyGeologicalsvyplanninglineEntity hddcWyGeologicalsvyplanningline = hddcWyGeologicalsvyplanninglineService.getHddcWyGeologicalsvyplanningline(uuid);
            response = RestResponse.succeed(hddcWyGeologicalsvyplanningline);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcWyGeologicalsvyplanningline(@RequestBody HddcWyGeologicalsvyplanninglineEntity hddcWyGeologicalsvyplanningline) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcWyGeologicalsvyplanninglineService.saveHddcWyGeologicalsvyplanningline(hddcWyGeologicalsvyplanningline);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcWyGeologicalsvyplanningline(@RequestBody HddcWyGeologicalsvyplanninglineEntity hddcWyGeologicalsvyplanningline)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcWyGeologicalsvyplanninglineService.updateHddcWyGeologicalsvyplanningline(hddcWyGeologicalsvyplanningline);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcWyGeologicalsvyplanninglines(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcWyGeologicalsvyplanninglineService.deleteHddcWyGeologicalsvyplanninglines(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcWyGeologicalsvyplanninglineService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }

    /**
     *图片批量上传
     * @param multipartFiles
     * @param type
     * @return
     */
    @PostMapping("/uploadimags")
    public RestResponse uploadImg(MultipartFile[] multipartFiles, String type){
        RestResponse response = null;
        try{
            String imgURL="";
            if(multipartFiles!=null && multipartFiles.length > 0 && multipartFiles.length <= 10){
                imgURL = hddcWyGeologicalsvyplanninglineService.uploadPic(multipartFiles,type);
                response = RestResponse.succeed(imgURL);
            }else if(multipartFiles.length > 11){
                response = RestResponse.fail("每次上传图片最多不能超过5张!");
            }
        }catch (Exception e){
            String errormessage="上传图片失败";
            log.error(errormessage,e);
            response = RestResponse.fail(errormessage);
    }
        return response;
    }
}