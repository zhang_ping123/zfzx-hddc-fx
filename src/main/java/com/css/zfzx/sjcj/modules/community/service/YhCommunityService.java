package com.css.zfzx.sjcj.modules.community.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.community.repository.entity.YhCommunityEntity;
import com.css.zfzx.sjcj.modules.community.viewobjects.YhCommunityQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import java.util.List;

/**
 * @author yyd
 * @date 2020-11-04
 */

public interface YhCommunityService {

    public JSONObject queryYhCommunitys(YhCommunityQueryParams queryParams, int curPage, int pageSize);

    public YhCommunityEntity getYhCommunity(String id);

    public YhCommunityEntity saveYhCommunity(YhCommunityEntity yhCommunity);

    public YhCommunityEntity updateYhCommunity(YhCommunityEntity yhCommunity);

    public void deleteYhCommunitys(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

}
