package com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
@Entity
@Table(name="hddc_wy_faultsvypoint")
public class HddcWyFaultsvypointEntity implements Serializable {

    /**
     * 送样数
     */
    @Column(name="samplecount")
    private Integer samplecount;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 比例尺（分母）
     */
    @Column(name="scale")
    private Integer scale;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 是否在图中显示
     */
    @Column(name="isinmap")
    private Integer isinmap;
    /**
     * 水平//张缩位移 [米]
     */
    @Column(name="tensionaldisplacement")
    private Double tensionaldisplacement;
    /**
     * 典型剖面图图像文件编号
     */
    @Column(name="typicalprofile_aiid")
    private String typicalprofileAiid;
    /**
     * 断层倾向 [度]
     */
    @Column(name="measurefaultdip")
    private Integer measurefaultdip;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 典型剖面图原始文件编号
     */
    @Column(name="typicalprofile_arwid")
    private String typicalprofileArwid;
    /**
     * 断层走向 [度]
     */
    @Column(name="faultstrike")
    private Integer faultstrike;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 走向水平位移 [米]
     */
    @Column(name="horizentaloffset")
    private Double horizentaloffset;
    /**
     * 平面图原始文件编号
     */
    @Column(name="sketch_arwid")
    private String sketchArwid;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 符号或标注旋转角度
     */
    @Column(name="lastangle")
    private Double lastangle;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 水平/张缩位移误差
     */
    @Column(name="tensionaldisplacementerror")
    private Double tensionaldisplacementerror;
    /**
     * 走向水平位移误差
     */
    @Column(name="horizentaloffseterror")
    private Double horizentaloffseterror;
    /**
     * 观测点野外编号
     */
    @Column(name="fieldid")
    private String fieldid;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;

    /**
     * 垂直位移误差
     */
    @Column(name="verticaldisplacementerror")
    private Double verticaldisplacementerror;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 目标断层编号
     */
    @Column(name="targetfaultid")
    private String targetfaultid;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 数据来源
     */
    @Column(name="datasource")
    private String datasource;
    /**
     * 断层倾角 [度]
     */
    @Column(name="faultclination")
    private Integer faultclination;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 典型照片原始文件编号
     */
    @Column(name="photo_arwid")
    private String photoArwid;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 照片镜向
     */
    @Column(name="photoviewingto")
    private Integer photoviewingto;
    /**
     * 获得测试结果样品数
     */
    @Column(name="datingsamplecount")
    private Integer datingsamplecount;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 断层性质
     */
    @Column(name="feature")
    private Integer feature;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;

    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 目标断层名称
     */
    @Column(name="targetfaultname")
    private String targetfaultname;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 拍摄者
     */
    @Column(name="photographer")
    private String photographer;
    /**
     * 目标断层来源
     */
    @Column(name="targetfaultsource")
    private String targetfaultsource;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 平面图文件编号
     */
    @Column(name="sketch_aiid")
    private String sketchAiid;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 备注
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 产状点编号
     */
    //@Id
    @Column(name="id")
    private String id;
    /**
     * 产状点编号
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;

    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 地质调查观测点编号
     */
    @Column(name="geologicalsvypointid")
    private String geologicalsvypointid;
    /**
     * 典型照片文件编号
     */
    @Column(name="photo_aiid")
    private String photoAiid;
    /**
     * 是否修改工作底图
     */
    @Column(name="ismodifyworkmap")
    private Integer ismodifyworkmap;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 垂直位移 [米]
     */
    @Column(name="verticaldisplacement")
    private Double verticaldisplacement;
    /**
     * 目标断层来源字典名称
     */
    @Column(name="targetfaultsource_name")
    private String targetfaultsourceName;
    /**
     * 断层性质字典名称
     */
    @Column(name="feature_name")
    private String featureName;
    /**
     * 照片镜向字典名称
     */
    @Column(name="photoviewingto_name")
    private String photoviewingtoName;
    /**
     * 经度
     */
    @Column(name="lon")
    private Double lon;
    /**
     * 维度
     */
    @Column(name="lat")
    private Double lat;
}

