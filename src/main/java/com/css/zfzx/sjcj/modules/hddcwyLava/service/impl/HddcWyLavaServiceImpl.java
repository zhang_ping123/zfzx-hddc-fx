package com.css.zfzx.sjcj.modules.hddcwyLava.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcLava.repository.HddcLavaRepository;
import com.css.zfzx.sjcj.modules.hddcLava.repository.entity.HddcLavaEntity;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyLava.repository.HddcWyLavaNativeRepository;
import com.css.zfzx.sjcj.modules.hddcwyLava.repository.HddcWyLavaRepository;
import com.css.zfzx.sjcj.modules.hddcwyLava.repository.entity.HddcWyLavaEntity;
import com.css.zfzx.sjcj.modules.hddcwyLava.service.HddcWyLavaService;
import com.css.zfzx.sjcj.modules.hddcwyLava.viewobjects.HddcWyLavaQueryParams;
import com.css.zfzx.sjcj.modules.hddcwyLava.viewobjects.HddcWyLavaVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zhangcong
 * @date 2020-12-02
 */
@Service
public class HddcWyLavaServiceImpl implements HddcWyLavaService {

	@Autowired
    private HddcWyLavaRepository hddcWyLavaRepository;
    @Autowired
    private HddcWyLavaNativeRepository hddcWyLavaNativeRepository;
    @Autowired
    private HddcLavaRepository hddcLavaRepository;
    @Override
    public JSONObject queryHddcWyLavas(HddcWyLavaQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcWyLavaEntity> hddcWyLavaPage = this.hddcWyLavaNativeRepository.queryHddcWyLavas(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcWyLavaPage);
        return jsonObject;
    }


    @Override
    public HddcWyLavaEntity getHddcWyLava(String uuid) {
        HddcWyLavaEntity hddcWyLava = this.hddcWyLavaRepository.findById(uuid).orElse(null);
         return hddcWyLava;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyLavaEntity saveHddcWyLava(HddcWyLavaEntity hddcWyLava) {
        String uuid = UUIDGenerator.getUUID();
        hddcWyLava.setUuid(uuid);
        if(StringUtils.isEmpty(hddcWyLava.getCreateUser())){
            hddcWyLava.setCreateUser(PlatformSessionUtils.getUserId());
        }
        hddcWyLava.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        hddcWyLava.setIsValid("1");

        HddcLavaEntity hddcLavaEntity=new HddcLavaEntity();
        BeanUtils.copyProperties(hddcWyLava,hddcLavaEntity);
        hddcLavaEntity.setExtends4(hddcWyLava.getTown());
        hddcLavaEntity.setExtends5(String.valueOf(hddcWyLava.getLon()));
        hddcLavaEntity.setExtends6(String.valueOf(hddcWyLava.getLat()));
        this.hddcLavaRepository.save(hddcLavaEntity);

        this.hddcWyLavaRepository.save(hddcWyLava);
        return hddcWyLava;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyLavaEntity updateHddcWyLava(HddcWyLavaEntity hddcWyLava) {
        HddcWyLavaEntity entity = hddcWyLavaRepository.findById(hddcWyLava.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcWyLava);
        hddcWyLava.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcWyLava.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcWyLavaRepository.save(hddcWyLava);
        return hddcWyLava;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcWyLavas(List<String> ids) {
        List<HddcWyLavaEntity> hddcWyLavaList = this.hddcWyLavaRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcWyLavaList) && hddcWyLavaList.size() > 0) {
            for(HddcWyLavaEntity hddcWyLava : hddcWyLavaList) {
                this.hddcWyLavaRepository.delete(hddcWyLava);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public BigInteger queryHddcWyLava(HddcAppZztCountVo queryParams) {
        BigInteger bigInteger = hddcWyLavaNativeRepository.queryHddcWyLava(queryParams);
        return bigInteger;
    }

    @Override
    public List<HddcWyLavaEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid) {
        List<HddcWyLavaEntity> list = hddcWyLavaRepository.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, isValid);
        return list;
    }

    @Override
    public void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response) {
        List<HddcWyLavaEntity> hddcWyLavaEntity = hddcWyLavaNativeRepository.exportLava(queryParams);
        List<HddcWyLavaVO> list=new ArrayList<>();
        for (HddcWyLavaEntity entity:hddcWyLavaEntity) {
            HddcWyLavaVO hddcWyLavaVO=new HddcWyLavaVO();
            BeanUtils.copyProperties(entity,hddcWyLavaVO);
            list.add(hddcWyLavaVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"熔岩流-面","熔岩流-面", HddcWyLavaVO.class,"熔岩流-面.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcWyLavaVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcWyLavaVO.class, params);
            List<HddcWyLavaVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcWyLavaVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcWyLavaVO hddcWyLavaVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + hddcWyLavaVO.getRowNum() + "行" + hddcWyLavaVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcWyLavaVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcWyLavaEntity hddcWyLavaEntity = new HddcWyLavaEntity();
            HddcWyLavaVO hddcWyLavaVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(hddcWyLavaVO, hddcWyLavaEntity);
            saveHddcWyLava(hddcWyLavaEntity);
        }
    }

    @Override
    public List<HddcWyLavaEntity> findAllByCreateUserAndIsValid(String userId,String isValid) {
        List<HddcWyLavaEntity> list = hddcWyLavaRepository.findAllByCreateUserAndIsValid(userId,isValid);
        return list;
    }


    /**
     * 逻辑删除-根据项目id删除数据
     * @param projectIds
     */
    @Override
    public void deleteByProjectId(List<String> projectIds) {
        List<HddcWyLavaEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyLavaRepository.queryHddcA1InvrgnhasmaterialtablesByProjectId(projectIds);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyLavaEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyLavaRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }
    }

    /**
     * 逻辑删除-根据任务id删除数据
     * @param taskId
     */
    @Override
    public void deleteByTaskId(List<String> taskId) {
        List<HddcWyLavaEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyLavaRepository.queryHddcA1InvrgnhasmaterialtablesByTaskId(taskId);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyLavaEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyLavaRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }

    }






}
