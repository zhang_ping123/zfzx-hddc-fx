package com.css.zfzx.sjcj.modules.hddcGeomorphySvySamplePoint.repository;

import com.css.zfzx.sjcj.modules.hddcGeomorphySvySamplePoint.repository.entity.HddcGeomorphysvysamplepointEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvySamplePoint.viewobjects.HddcGeomorphysvysamplepointQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcGeomorphysvysamplepointNativeRepository {

    Page<HddcGeomorphysvysamplepointEntity> queryHddcGeomorphysvysamplepoints(HddcGeomorphysvysamplepointQueryParams queryParams, int curPage, int pageSize);

    List<HddcGeomorphysvysamplepointEntity> exportYhDisasters(HddcGeomorphysvysamplepointQueryParams queryParams);
}
