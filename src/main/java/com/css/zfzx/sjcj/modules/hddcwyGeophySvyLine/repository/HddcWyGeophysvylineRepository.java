package com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.repository;

import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.repository.entity.HddcWyGeophysvylineEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */
public interface HddcWyGeophysvylineRepository extends JpaRepository<HddcWyGeophysvylineEntity, String> {

    List<HddcWyGeophysvylineEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    List<HddcWyGeophysvylineEntity> findAllByCreateUserAndIsValid(String userId, String isValid);


    @Query(nativeQuery = true, value = "select * from hddc_wy_geophysvyline where is_valid!=0 project_name in :projectIds")
    List<HddcWyGeophysvylineEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_geophysvyline where task_name in :projectIds")
    List<HddcWyGeophysvylineEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);

}
