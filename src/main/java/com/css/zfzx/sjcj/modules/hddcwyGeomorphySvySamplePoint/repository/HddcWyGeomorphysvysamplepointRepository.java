package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvySamplePoint.repository;

import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvySamplePoint.repository.entity.HddcWyGeomorphysvysamplepointEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author lihelei
 * @date 2020-12-01
 */
public interface HddcWyGeomorphysvysamplepointRepository extends JpaRepository<HddcWyGeomorphysvysamplepointEntity, String> {

    List<HddcWyGeomorphysvysamplepointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    List<HddcWyGeomorphysvysamplepointEntity> findAllByCreateUserAndIsValid(String userId,String isValid);

    @Query(nativeQuery = true, value = "select * from hddc_wy_geomorphysvysamplepoint where is_valid!=0 project_name in :projectIds")
    List<HddcWyGeomorphysvysamplepointEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_geomorphysvysamplepoint where task_name in :projectIds")
    List<HddcWyGeomorphysvysamplepointEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);
}
