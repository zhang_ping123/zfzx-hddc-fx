package com.css.zfzx.sjcj.modules.app.vo;

import lombok.Data;

@Data
public class LoginParamVO {

    private String userName;

    private String password;

    private String appCode;
}
