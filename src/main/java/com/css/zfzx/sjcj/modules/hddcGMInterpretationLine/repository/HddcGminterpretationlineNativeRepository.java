package com.css.zfzx.sjcj.modules.hddcGMInterpretationLine.repository;

import com.css.zfzx.sjcj.modules.hddcGMInterpretationLine.repository.entity.HddcGminterpretationlineEntity;
import com.css.zfzx.sjcj.modules.hddcGMInterpretationLine.viewobjects.HddcGminterpretationlineQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-26
 */
public interface HddcGminterpretationlineNativeRepository {

    Page<HddcGminterpretationlineEntity> queryHddcGminterpretationlines(HddcGminterpretationlineQueryParams queryParams, int curPage, int pageSize);

    List<HddcGminterpretationlineEntity> exportYhDisasters(HddcGminterpretationlineQueryParams queryParams);
}
