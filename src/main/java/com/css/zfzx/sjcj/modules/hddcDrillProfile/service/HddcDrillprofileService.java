package com.css.zfzx.sjcj.modules.hddcDrillProfile.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcDrillProfile.repository.entity.HddcDrillprofileEntity;
import com.css.zfzx.sjcj.modules.hddcDrillProfile.viewobjects.HddcDrillprofileQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */

public interface HddcDrillprofileService {

    public JSONObject queryHddcDrillprofiles(HddcDrillprofileQueryParams queryParams, int curPage, int pageSize);

    public HddcDrillprofileEntity getHddcDrillprofile(String id);

    public HddcDrillprofileEntity saveHddcDrillprofile(HddcDrillprofileEntity hddcDrillprofile);

    public HddcDrillprofileEntity updateHddcDrillprofile(HddcDrillprofileEntity hddcDrillprofile);

    public void deleteHddcDrillprofiles(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcDrillprofileQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
