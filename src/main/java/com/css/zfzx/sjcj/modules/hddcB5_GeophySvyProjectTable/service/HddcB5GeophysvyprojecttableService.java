package com.css.zfzx.sjcj.modules.hddcB5_GeophySvyProjectTable.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyProjectTable.repository.entity.HddcB5GeophysvyprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyProjectTable.viewobjects.HddcB5GeophysvyprojecttableQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */

public interface HddcB5GeophysvyprojecttableService {

    public JSONObject queryHddcB5Geophysvyprojecttables(HddcB5GeophysvyprojecttableQueryParams queryParams, int curPage, int pageSize);

    public HddcB5GeophysvyprojecttableEntity getHddcB5Geophysvyprojecttable(String id);

    public HddcB5GeophysvyprojecttableEntity saveHddcB5Geophysvyprojecttable(HddcB5GeophysvyprojecttableEntity hddcB5Geophysvyprojecttable);

    public HddcB5GeophysvyprojecttableEntity updateHddcB5Geophysvyprojecttable(HddcB5GeophysvyprojecttableEntity hddcB5Geophysvyprojecttable);

    public void deleteHddcB5Geophysvyprojecttables(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB5GeophysvyprojecttableQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
