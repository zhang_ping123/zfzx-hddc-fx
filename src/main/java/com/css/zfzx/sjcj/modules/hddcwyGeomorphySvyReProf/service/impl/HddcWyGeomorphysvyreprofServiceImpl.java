package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyReProf.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyReProf.repository.HddcGeomorphysvyreprofRepository;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyReProf.repository.entity.HddcGeomorphysvyreprofEntity;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyReProf.repository.HddcWyGeomorphysvyreprofNativeRepository;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyReProf.repository.HddcWyGeomorphysvyreprofRepository;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyReProf.repository.entity.HddcWyGeomorphysvyreprofEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyReProf.service.HddcWyGeomorphysvyreprofService;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyReProf.viewobjects.HddcWyGeomorphysvyreprofQueryParams;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyReProf.viewobjects.HddcWyGeomorphysvyreprofVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author lihelei
 * @date 2020-12-01
 */
@Service
@PropertySource(value = "classpath:platform-config.yml")
public class HddcWyGeomorphysvyreprofServiceImpl implements HddcWyGeomorphysvyreprofService {

	@Autowired
    private HddcWyGeomorphysvyreprofRepository hddcWyGeomorphysvyreprofRepository;
    @Autowired
    private HddcWyGeomorphysvyreprofNativeRepository hddcWyGeomorphysvyreprofNativeRepository;
    @Autowired
    private HddcGeomorphysvyreprofRepository hddcGeomorphysvyreprofRepository;
    @Value("${image.localDir}")
    private String localDir;
    @Value("${image.urlPre}")
    private String urlPre;
    @Override
    public JSONObject queryHddcWyGeomorphysvyreprofs(HddcWyGeomorphysvyreprofQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcWyGeomorphysvyreprofEntity> hddcWyGeomorphysvyreprofPage = this.hddcWyGeomorphysvyreprofNativeRepository.queryHddcWyGeomorphysvyreprofs(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcWyGeomorphysvyreprofPage);
        return jsonObject;
    }


    @Override
    public HddcWyGeomorphysvyreprofEntity getHddcWyGeomorphysvyreprof(String uuid) {
        HddcWyGeomorphysvyreprofEntity hddcWyGeomorphysvyreprof = this.hddcWyGeomorphysvyreprofRepository.findById(uuid).orElse(null);
         return hddcWyGeomorphysvyreprof;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyGeomorphysvyreprofEntity saveHddcWyGeomorphysvyreprof(HddcWyGeomorphysvyreprofEntity hddcWyGeomorphysvyreprof) {
        String uuid = UUIDGenerator.getUUID();
        hddcWyGeomorphysvyreprof.setUuid(uuid);
        if(StringUtils.isEmpty(hddcWyGeomorphysvyreprof.getCreateUser())){
            hddcWyGeomorphysvyreprof.setCreateUser(PlatformSessionUtils.getUserId());
        }
        hddcWyGeomorphysvyreprof.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        hddcWyGeomorphysvyreprof.setIsValid("1");

        HddcGeomorphysvyreprofEntity hddcGeomorphysvyreprofEntity=new HddcGeomorphysvyreprofEntity();
        BeanUtils.copyProperties(hddcWyGeomorphysvyreprof,hddcGeomorphysvyreprofEntity);
        hddcGeomorphysvyreprofEntity.setExtends4(hddcWyGeomorphysvyreprof.getTown());
        hddcGeomorphysvyreprofEntity.setExtends5(String.valueOf(hddcWyGeomorphysvyreprof.getLon()));
        hddcGeomorphysvyreprofEntity.setExtends6(String.valueOf(hddcWyGeomorphysvyreprof.getLat()));
        this.hddcGeomorphysvyreprofRepository.save(hddcGeomorphysvyreprofEntity);

        this.hddcWyGeomorphysvyreprofRepository.save(hddcWyGeomorphysvyreprof);
        return hddcWyGeomorphysvyreprof;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyGeomorphysvyreprofEntity updateHddcWyGeomorphysvyreprof(HddcWyGeomorphysvyreprofEntity hddcWyGeomorphysvyreprof) {
        HddcWyGeomorphysvyreprofEntity entity = hddcWyGeomorphysvyreprofRepository.findById(hddcWyGeomorphysvyreprof.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcWyGeomorphysvyreprof);
        hddcWyGeomorphysvyreprof.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcWyGeomorphysvyreprof.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcWyGeomorphysvyreprofRepository.save(hddcWyGeomorphysvyreprof);
        return hddcWyGeomorphysvyreprof;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcWyGeomorphysvyreprofs(List<String> ids) {
        List<HddcWyGeomorphysvyreprofEntity> hddcWyGeomorphysvyreprofList = this.hddcWyGeomorphysvyreprofRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcWyGeomorphysvyreprofList) && hddcWyGeomorphysvyreprofList.size() > 0) {
            for(HddcWyGeomorphysvyreprofEntity hddcWyGeomorphysvyreprof : hddcWyGeomorphysvyreprofList) {
                this.hddcWyGeomorphysvyreprofRepository.delete(hddcWyGeomorphysvyreprof);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public BigInteger queryHddcWyGeomorphysvyreprof(HddcAppZztCountVo queryParams) {
        BigInteger bigInteger = hddcWyGeomorphysvyreprofNativeRepository.queryHddcWyGeomorphysvyreprof(queryParams);
        return bigInteger;
    }

    @Override
    public List<HddcWyGeomorphysvyreprofEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid) {
        List<HddcWyGeomorphysvyreprofEntity> list = hddcWyGeomorphysvyreprofRepository.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, isValid);
        return list;
    }

    @Override
    public void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response) {
        List<HddcWyGeomorphysvyreprofEntity> hddcWyGeomorphysvyreprofEntity = hddcWyGeomorphysvyreprofNativeRepository.exportReprof(queryParams);
        List<HddcWyGeomorphysvyreprofVO> list=new ArrayList<>();
        for (HddcWyGeomorphysvyreprofEntity entity:hddcWyGeomorphysvyreprofEntity) {
            HddcWyGeomorphysvyreprofVO hddcWyGeomorphysvyreprofVO=new HddcWyGeomorphysvyreprofVO();
            BeanUtils.copyProperties(entity,hddcWyGeomorphysvyreprofVO);
            list.add(hddcWyGeomorphysvyreprofVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"微地貌面测量切线-线","微地貌面测量切线-线", HddcWyGeomorphysvyreprofVO.class,"微地貌面测量切线-线.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcWyGeomorphysvyreprofVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcWyGeomorphysvyreprofVO.class, params);
            List<HddcWyGeomorphysvyreprofVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcWyGeomorphysvyreprofVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcWyGeomorphysvyreprofVO hddcWyGeomorphysvyregionVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + hddcWyGeomorphysvyregionVO.getRowNum() + "行" + hddcWyGeomorphysvyregionVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcWyGeomorphysvyreprofVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcWyGeomorphysvyreprofEntity hddcWyGeomorphysvyreprofEntity = new HddcWyGeomorphysvyreprofEntity();
            HddcWyGeomorphysvyreprofVO hddcWyGeomorphysvyreprofVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(hddcWyGeomorphysvyreprofVO, hddcWyGeomorphysvyreprofEntity);
            saveHddcWyGeomorphysvyreprof(hddcWyGeomorphysvyreprofEntity);
        }
    }

    @Override
    public List<HddcWyGeomorphysvyreprofEntity> findAllByCreateUserAndIsValid(String userId,String isValid) {
        List<HddcWyGeomorphysvyreprofEntity> list = hddcWyGeomorphysvyreprofRepository.findAllByCreateUserAndIsValid(userId,isValid);
        return list;
    }

    /**
     * 逻辑删除-根据项目id删除数据
     * @param projectIds
     */
    @Override
    public void deleteByProjectId(List<String> projectIds) {
        List<HddcWyGeomorphysvyreprofEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyGeomorphysvyreprofRepository.queryHddcA1InvrgnhasmaterialtablesByProjectId(projectIds);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyGeomorphysvyreprofEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyGeomorphysvyreprofRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }
    }

    /**
     * 逻辑删除-根据任务id删除数据
     * @param taskId
     */
    @Override
    public void deleteByTaskId(List<String> taskId) {
        List<HddcWyGeomorphysvyreprofEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyGeomorphysvyreprofRepository.queryHddcA1InvrgnhasmaterialtablesByTaskId(taskId);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyGeomorphysvyreprofEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyGeomorphysvyreprofRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }

    }














}
