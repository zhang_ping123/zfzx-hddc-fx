package com.css.zfzx.sjcj.modules.hddcMaterialPolygonLayer.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltVO;
import com.css.zfzx.sjcj.modules.hddcMaterialPolygonLayer.repository.HddcMaterialpolygonlayerNativeRepository;
import com.css.zfzx.sjcj.modules.hddcMaterialPolygonLayer.repository.HddcMaterialpolygonlayerRepository;
import com.css.zfzx.sjcj.modules.hddcMaterialPolygonLayer.repository.entity.HddcMaterialpolygonlayerEntity;
import com.css.zfzx.sjcj.modules.hddcMaterialPolygonLayer.service.HddcMaterialpolygonlayerService;
import com.css.zfzx.sjcj.modules.hddcMaterialPolygonLayer.viewobjects.HddcMaterialpolygonlayerQueryParams;
import com.css.zfzx.sjcj.modules.hddcMaterialPolygonLayer.viewobjects.HddcMaterialpolygonlayerVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-03
 */
@Service
public class HddcMaterialpolygonlayerServiceImpl implements HddcMaterialpolygonlayerService {

	@Autowired
    private HddcMaterialpolygonlayerRepository hddcMaterialpolygonlayerRepository;
    @Autowired
    private HddcMaterialpolygonlayerNativeRepository hddcMaterialpolygonlayerNativeRepository;

    @Override
    public JSONObject queryHddcMaterialpolygonlayers(HddcMaterialpolygonlayerQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcMaterialpolygonlayerEntity> hddcMaterialpolygonlayerPage = this.hddcMaterialpolygonlayerNativeRepository.queryHddcMaterialpolygonlayers(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcMaterialpolygonlayerPage);
        return jsonObject;
    }


    @Override
    public HddcMaterialpolygonlayerEntity getHddcMaterialpolygonlayer(String id) {
        HddcMaterialpolygonlayerEntity hddcMaterialpolygonlayer = this.hddcMaterialpolygonlayerRepository.findById(id).orElse(null);
         return hddcMaterialpolygonlayer;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcMaterialpolygonlayerEntity saveHddcMaterialpolygonlayer(HddcMaterialpolygonlayerEntity hddcMaterialpolygonlayer) {
        String uuid = UUIDGenerator.getUUID();
        hddcMaterialpolygonlayer.setUuid(uuid);
        hddcMaterialpolygonlayer.setCreateUser(PlatformSessionUtils.getUserId());
        hddcMaterialpolygonlayer.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcMaterialpolygonlayerRepository.save(hddcMaterialpolygonlayer);
        return hddcMaterialpolygonlayer;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcMaterialpolygonlayerEntity updateHddcMaterialpolygonlayer(HddcMaterialpolygonlayerEntity hddcMaterialpolygonlayer) {
        HddcMaterialpolygonlayerEntity entity = hddcMaterialpolygonlayerRepository.findById(hddcMaterialpolygonlayer.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcMaterialpolygonlayer);
        hddcMaterialpolygonlayer.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcMaterialpolygonlayer.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcMaterialpolygonlayerRepository.save(hddcMaterialpolygonlayer);
        return hddcMaterialpolygonlayer;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcMaterialpolygonlayers(List<String> ids) {
        List<HddcMaterialpolygonlayerEntity> hddcMaterialpolygonlayerList = this.hddcMaterialpolygonlayerRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcMaterialpolygonlayerList) && hddcMaterialpolygonlayerList.size() > 0) {
            for(HddcMaterialpolygonlayerEntity hddcMaterialpolygonlayer : hddcMaterialpolygonlayerList) {
                this.hddcMaterialpolygonlayerRepository.delete(hddcMaterialpolygonlayer);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcMaterialpolygonlayerQueryParams queryParams, HttpServletResponse response) {
        List<HddcMaterialpolygonlayerEntity> yhDisasterEntities = hddcMaterialpolygonlayerNativeRepository.exportYhDisasters(queryParams);
        List<HddcMaterialpolygonlayerVO> list=new ArrayList<>();
        for (HddcMaterialpolygonlayerEntity entity:yhDisasterEntities) {
            HddcMaterialpolygonlayerVO yhDisasterVO=new HddcMaterialpolygonlayerVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"面状资料图层-面","面状资料图层-面",HddcMaterialpolygonlayerVO.class,"面状资料图层-面.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcMaterialpolygonlayerVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcMaterialpolygonlayerVO.class, params);
            List<HddcMaterialpolygonlayerVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcMaterialpolygonlayerVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcMaterialpolygonlayerVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcMaterialpolygonlayerVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcMaterialpolygonlayerEntity yhDisasterEntity = new HddcMaterialpolygonlayerEntity();
            HddcMaterialpolygonlayerVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcMaterialpolygonlayer(yhDisasterEntity);
        }
    }

}
