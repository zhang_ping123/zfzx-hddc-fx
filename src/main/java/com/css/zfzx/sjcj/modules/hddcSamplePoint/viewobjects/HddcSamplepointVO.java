package com.css.zfzx.sjcj.modules.hddcSamplePoint.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Data
public class HddcSamplepointVO implements Serializable {

    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 野外编号
     */
    @Excel(name = "野外编号", orderNum = "4")
    private String fieldid;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备注
     */
    private String remark;
    /**
     * 数据来源
     */
    @Excel(name = "数据来源", orderNum = "5")
    private String samplesource;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "6")
    private String commentInfo;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 采样数据来源点编号
     */
    @Excel(name = "采样数据来源点编号", orderNum = "7")
    private String samplesourceid;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 采样点符号
     */
    @Excel(name = "采样点符号", orderNum = "8")
    private Integer symbolinfo;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 采样情况
     */
    @Excel(name = "采样情况", orderNum = "9")
    private String samplevariety;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 采样点编号
     */
    private String id;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 标注名称
     */
    @Excel(name = "标注名称", orderNum = "10")
    private String labelinfo;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 是否联合采样出单一测试结果
     */
    @Excel(name = "是否联合采样出单一测试结果", orderNum = "11")
    private Integer isunitedresult;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String samplesourceName;
    private Integer symbolinfoName;
    private String samplevarietyName;
    private Integer isunitedresultName;
    private String rowNum;
    private String errorMsg;
}