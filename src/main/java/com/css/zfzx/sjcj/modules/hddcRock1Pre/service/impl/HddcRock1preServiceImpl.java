package com.css.zfzx.sjcj.modules.hddcRock1Pre.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltVO;
import com.css.zfzx.sjcj.modules.hddcRock1Pre.repository.HddcRock1preNativeRepository;
import com.css.zfzx.sjcj.modules.hddcRock1Pre.repository.HddcRock1preRepository;
import com.css.zfzx.sjcj.modules.hddcRock1Pre.repository.entity.HddcRock1preEntity;
import com.css.zfzx.sjcj.modules.hddcRock1Pre.service.HddcRock1preService;
import com.css.zfzx.sjcj.modules.hddcRock1Pre.viewobjects.HddcRock1preQueryParams;
import com.css.zfzx.sjcj.modules.hddcRock1Pre.viewobjects.HddcRock1preVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Service
public class HddcRock1preServiceImpl implements HddcRock1preService {

	@Autowired
    private HddcRock1preRepository hddcRock1preRepository;
    @Autowired
    private HddcRock1preNativeRepository hddcRock1preNativeRepository;

    @Override
    public JSONObject queryHddcRock1pres(HddcRock1preQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcRock1preEntity> hddcRock1prePage = this.hddcRock1preNativeRepository.queryHddcRock1pres(queryParams, curPage, pageSize);
        List<HddcRock1preVO> hddcRock1preVOList = new ArrayList<>();
        List<HddcRock1preEntity> hddcRock1preEntityList = hddcRock1prePage.getContent();
        for(HddcRock1preEntity hddcRock1preEntity : hddcRock1preEntityList){
            HddcRock1preVO hddcRock1preVO = new HddcRock1preVO();
            BeanUtils.copyProperties(hddcRock1preEntity, hddcRock1preVO);
            if(PlatformObjectUtils.isNotEmpty(hddcRock1preEntity.getSymbol())) {
                String dictItemCode=String.valueOf(hddcRock1preEntity.getSymbol());
                hddcRock1preVO.setSymbolName(findByDictCodeAndDictItemCode("RockTypeCVD", dictItemCode));
            }
            hddcRock1preVOList.add(hddcRock1preVO);
        }
        Page<HddcRock1preVO> HddcRock1preVOPage = new PageImpl(hddcRock1preVOList, hddcRock1prePage.getPageable(), hddcRock1prePage.getTotalElements());
        JSONObject jsonObject = PlatformPageUtils.formatPageData(HddcRock1preVOPage);
        return jsonObject;
    }


    @Override
    public HddcRock1preEntity getHddcRock1pre(String id) {
        HddcRock1preEntity hddcRock1pre = this.hddcRock1preRepository.findById(id).orElse(null);
         return hddcRock1pre;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcRock1preEntity saveHddcRock1pre(HddcRock1preEntity hddcRock1pre) {
        String uuid = UUIDGenerator.getUUID();
        hddcRock1pre.setUuid(uuid);
        hddcRock1pre.setCreateUser(PlatformSessionUtils.getUserId());
        hddcRock1pre.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcRock1preRepository.save(hddcRock1pre);
        return hddcRock1pre;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcRock1preEntity updateHddcRock1pre(HddcRock1preEntity hddcRock1pre) {
        HddcRock1preEntity entity = hddcRock1preRepository.findById(hddcRock1pre.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcRock1pre);
        hddcRock1pre.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcRock1pre.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcRock1preRepository.save(hddcRock1pre);
        return hddcRock1pre;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcRock1pres(List<String> ids) {
        List<HddcRock1preEntity> hddcRock1preList = this.hddcRock1preRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcRock1preList) && hddcRock1preList.size() > 0) {
            for(HddcRock1preEntity hddcRock1pre : hddcRock1preList) {
                this.hddcRock1preRepository.delete(hddcRock1pre);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public String findByDictCodeAndDictItemCode(String dictCode, String dictItemCode){
        DictItemEntity dictItem = PlatformAPI.getDictAPI().getValidDictItemByDictCodeAndDictItemCode(dictCode, dictItemCode);
        String dictItemName = "";
        if(PlatformObjectUtils.isNotEmpty(dictItem)){
            dictItemName = dictItem.getDictItemName();
        }
        return dictItemName;
    }

    @Override
    public void exportFile(HddcRock1preQueryParams queryParams, HttpServletResponse response) {
        List<HddcRock1preEntity> yhDisasterEntities = hddcRock1preNativeRepository.exportYhDisasters(queryParams);
        List<HddcRock1preVO> list=new ArrayList<>();
        for (HddcRock1preEntity entity:yhDisasterEntities) {
            HddcRock1preVO yhDisasterVO=new HddcRock1preVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"1：1万工作底图岩体-面","1：1万工作底图岩体-面",HddcRock1preVO.class,"1：1万工作底图岩体-面.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcRock1preVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcRock1preVO.class, params);
            List<HddcRock1preVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcRock1preVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcRock1preVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcRock1preVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcRock1preEntity yhDisasterEntity = new HddcRock1preEntity();
            HddcRock1preVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcRock1pre(yhDisasterEntity);
        }
    }
}
