package com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPoint.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-12-01
 */
@Data
public class HddcWyGeologicalsvypointVO implements Serializable {

    /**
     * 是否在图中显示
     */
    @Excel(name="是否在图中显示",orderNum = "17")
    private Integer isinmap;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 观测方法
     */
    @Excel(name="观测方法",orderNum = "20")
    private String svymethods;
    /**
     * 省
     */
    @Excel(name="省",orderNum = "2")
    private String province;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 观测目的
     */
    @Excel(name="观测目的",orderNum = "22")
    private String purpose;
    /**
     * 观测点描述
     */
    @Excel(name="观测点描述",orderNum = "26")
    private String spcommentInfo;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 纬度
     */
    @Excel(name="纬度",orderNum = "7")
    private Double lat;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 海拔高度 [米]
     */
    @Excel(name="海拔高度 [米]",orderNum = "14")
    private Integer elevation;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 观测点野外编号
     */
    @Excel(name="观测点野外编号",orderNum = "15")
    private String fieldid;
    /**
     * 备注
     */
    @Excel(name="备注",orderNum = "23")
    private String remark;
    /**
     * 观测点编号
     */
    @Excel(name="观测点编号",orderNum = "1")
    private String id;
    /**
     * 典型照片原始文件编号
     */
    @Excel(name="典型照片原始文件编号",orderNum = "13")
    private String photoArwid;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 是否地貌点
     */
    @Excel(name="是否地貌点",orderNum = "21")
    private Integer isgeomorphpoint;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 拍摄者
     */
    @Excel(name="拍摄者",orderNum = "9")
    private String photographer;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 任务名称
     */
    @Excel(name="任务名称",orderNum = "10")
    private String taskName;
    /**
     * 典型照片文件编号
     */
    @Excel(name="典型照片文件编号",orderNum = "16")
    private String photoAiid;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 地质剖面线编号
     */
    @Excel(name="地质剖面线编号",orderNum = "19")
    private String profilesvylineid;
    /**
     * 是否断点
     */
    @Excel(name="是否断点",orderNum = "25")
    private Integer isfaultpoint;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 区（县）
     */
    @Excel(name="区（县）",orderNum = "4")
    private String area;
    /**
     * 照片镜向
     */
    @Excel(name="照片镜向",orderNum = "27")
    private Integer photoviewingto;
    /**
     * 采集样品总数
     */
    @Excel(name="采集样品总数",orderNum = "29")
    private Integer collectedsamplecount;
    /**
     * 备注
     */
    private String commentInfo;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 市
     */
    @Excel(name="市",orderNum = "3")
    private String city;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 项目ID
     */
    private String projectcode;
    /**
     * 乡
     */
    @Excel(name="详细地址",orderNum = "5")
    private String town;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 获得测试结果样品数
     */
    @Excel(name="获得测试结果样品数",orderNum = "28")
    private Integer datingsamplecount;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 观测点地名
     */
    @Excel(name="观测点地名",orderNum = "18")
    private String locationname;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 工程编号
     */
    @Excel(name="工程编号",orderNum = "11")
    private String projectid;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 经度
     */
    @Excel(name="经度",orderNum = "6")
    private Double lon;
    /**
     * 送样总数
     */
    @Excel(name="送样总数",orderNum = "12")
    private Integer samplecount;
    /**
     * 项目名称
     */
    @Excel(name="项目名称",orderNum = "8")
    private String projectName;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 观测日期
     */
    @Excel(name="观测日期",orderNum = "30")
    private String svydate;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 是否地层点
     */
    @Excel(name="是否地层点",orderNum = "24")
    private Integer isstratigraphypoint;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String rowNum;
    private String errorMsg;
}