package com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.viewobjects.HddcWyFaultsvypointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */

public interface HddcWyFaultsvypointService {

    public JSONObject queryHddcWyFaultsvypoints(HddcWyFaultsvypointQueryParams queryParams, int curPage, int pageSize);

    public HddcWyFaultsvypointEntity getHddcWyFaultsvypoint(String uuid);

    public HddcWyFaultsvypointEntity saveHddcWyFaultsvypoint(HddcWyFaultsvypointEntity hddcWyFaultsvypoint);

    public HddcWyFaultsvypointEntity updateHddcWyFaultsvypoint(HddcWyFaultsvypointEntity hddcWyFaultsvypoint);

    public void deleteHddcWyFaultsvypoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    BigInteger queryHddcWyFaultsvypoint(HddcAppZztCountVo queryParams);

    List<HddcWyFaultsvypointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId,String taskId,String projectId,String isValid);

    void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);

    List<HddcWyFaultsvypointEntity> findAllByCreateUserAndIsValid(String userId,String isValid);


    /**
     * 逻辑删除-根据项目id删除数据
     * @param ids
     */
    void deleteByProjectId(List<String> ids);

    /**
     * 逻辑删除-根据任务id删除数据
     * @param ids
     */
    void deleteByTaskId(List<String> ids);
}
