package com.css.zfzx.sjcj.modules.hddcGeoGeomorphySvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcGeoGeomorphySvyPoint.repository.entity.HddcGeogeomorphysvypointEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author lihelei
 * @date 2020-11-26
 */
public interface HddcGeogeomorphysvypointRepository extends JpaRepository<HddcGeogeomorphysvypointEntity, String> {
}
