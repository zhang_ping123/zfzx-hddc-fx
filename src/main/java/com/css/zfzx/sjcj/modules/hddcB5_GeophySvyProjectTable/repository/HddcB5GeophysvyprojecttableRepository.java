package com.css.zfzx.sjcj.modules.hddcB5_GeophySvyProjectTable.repository;

import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyProjectTable.repository.entity.HddcB5GeophysvyprojecttableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
public interface HddcB5GeophysvyprojecttableRepository extends JpaRepository<HddcB5GeophysvyprojecttableEntity, String> {
}
