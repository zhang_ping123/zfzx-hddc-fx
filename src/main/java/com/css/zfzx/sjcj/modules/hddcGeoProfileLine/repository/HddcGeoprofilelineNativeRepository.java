package com.css.zfzx.sjcj.modules.hddcGeoProfileLine.repository;

import com.css.zfzx.sjcj.modules.hddcGeoProfileLine.repository.entity.HddcGeoprofilelineEntity;
import com.css.zfzx.sjcj.modules.hddcGeoProfileLine.viewobjects.HddcGeoprofilelineQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcGeoprofilelineNativeRepository {

    Page<HddcGeoprofilelineEntity> queryHddcGeoprofilelines(HddcGeoprofilelineQueryParams queryParams, int curPage, int pageSize);

    List<HddcGeoprofilelineEntity> exportYhDisasters(HddcGeoprofilelineQueryParams queryParams);
}
