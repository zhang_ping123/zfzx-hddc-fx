package com.css.zfzx.sjcj.modules.hddcImportantSurfaceFeature.repository;

import com.css.zfzx.sjcj.modules.hddcImportantSurfaceFeature.repository.entity.HddcImportantsurfacefeatureEntity;
import com.css.zfzx.sjcj.modules.hddcImportantSurfaceFeature.viewobjects.HddcImportantsurfacefeatureQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author lihelei
 * @date 2020-11-30
 */
public interface HddcImportantsurfacefeatureNativeRepository {

    Page<HddcImportantsurfacefeatureEntity> queryHddcImportantsurfacefeatures(HddcImportantsurfacefeatureQueryParams queryParams, int curPage, int pageSize);

    List<HddcImportantsurfacefeatureEntity> exportYhDisasters(HddcImportantsurfacefeatureQueryParams queryParams);
}
