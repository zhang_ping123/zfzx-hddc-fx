package com.css.zfzx.sjcj.modules.hddcB7_VolcanicDataTable.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-11-26
 */
@Data
public class HddcB7VolcanicdatatableQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String name;

}
