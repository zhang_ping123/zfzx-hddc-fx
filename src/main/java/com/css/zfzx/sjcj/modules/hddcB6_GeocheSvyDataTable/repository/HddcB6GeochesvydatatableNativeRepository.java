package com.css.zfzx.sjcj.modules.hddcB6_GeocheSvyDataTable.repository;

import com.css.zfzx.sjcj.modules.hddcB6_GeocheSvyDataTable.repository.entity.HddcB6GeochesvydatatableEntity;
import com.css.zfzx.sjcj.modules.hddcB6_GeocheSvyDataTable.viewobjects.HddcB6GeochesvydatatableQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
public interface HddcB6GeochesvydatatableNativeRepository {

    Page<HddcB6GeochesvydatatableEntity> queryHddcB6Geochesvydatatables(HddcB6GeochesvydatatableQueryParams queryParams, int curPage, int pageSize);

    List<HddcB6GeochesvydatatableEntity> exportYhDisasters(HddcB6GeochesvydatatableQueryParams queryParams);
}
