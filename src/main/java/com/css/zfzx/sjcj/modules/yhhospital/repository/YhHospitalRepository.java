package com.css.zfzx.sjcj.modules.yhhospital.repository;

import com.css.zfzx.sjcj.modules.yhhospital.repository.entity.YhHospitalEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author yyd
 * @date 2020-11-03
 */
public interface YhHospitalRepository extends JpaRepository<YhHospitalEntity, String> {
}
