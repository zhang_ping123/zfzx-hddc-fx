package com.css.zfzx.sjcj.modules.hddcGeophySvyLine.repository;

import com.css.zfzx.sjcj.modules.hddcGeophySvyLine.repository.entity.HddcGeophysvylineEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
public interface HddcGeophysvylineRepository extends JpaRepository<HddcGeophysvylineEntity, String> {
}
