package com.css.zfzx.sjcj.modules.hddccjtask.repository;

import com.css.zfzx.sjcj.modules.hddccjtask.repository.entity.HddcCjTaskEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * @author zhangping
 * @date 2020-11-26
 */
public interface HddcCjTaskRepository extends JpaRepository<HddcCjTaskEntity, String> {
    @Query(nativeQuery = true,value = "select * from base.hddc_cj_task where belong_project_id = :projectid  and is_valid=1")
    List<HddcCjTaskEntity> findHddcCjTaskEntityByProjectIds(@Param("projectid") String projectid);
}
