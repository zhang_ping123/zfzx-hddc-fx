package com.css.zfzx.sjcj.modules.hddcStratigraphy5Pre.repository;

import com.css.zfzx.sjcj.modules.hddcStratigraphy5Pre.repository.entity.HddcStratigraphy5preEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcStratigraphy5preRepository extends JpaRepository<HddcStratigraphy5preEntity, String> {
}
