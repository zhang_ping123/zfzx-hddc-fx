package com.css.zfzx.sjcj.modules.hddcLava.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
@Data
public class HddcLavaVO implements Serializable {

    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 照片集镜向及拍摄者说明文档
     */
    @Excel(name = "照片集镜向及拍摄者说明文档", orderNum = "4")
    private String photodescArwid;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "5")
    private String commentInfo;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 素描图图像编码
     */
    @Excel(name = "素描图图像编码", orderNum = "6")
    private String sketchAiid;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 熔岩流时代
     */
    @Excel(name = "熔岩流时代", orderNum = "7")
    private Integer age;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 岩性描述
     */
    @Excel(name = "岩性描述", orderNum = "8")
    private String rockdescription;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 熔岩流结构分带
     */
    @Excel(name = "熔岩流结构分带", orderNum = "9")
    private String structurezone;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 熔岩流单元划分
     */
    @Excel(name = "熔岩流单元划分", orderNum = "10")
    private String unit;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 熔岩流表面形态
     */
    @Excel(name = "熔岩流表面形态", orderNum = "11")
    private String surfacemorphology;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 熔岩流规模
     */
    @Excel(name = "熔岩流规模", orderNum = "12")
    private String scope;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 熔岩流编号
     */
    @Excel(name = "熔岩流编号", orderNum = "13")
    private String id;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 熔岩流描述
     */
    @Excel(name = "熔岩流描述", orderNum = "14")
    private String description;
    /**
     * 照片文件编号
     */
    @Excel(name = "照片文件编号", orderNum = "15")
    private String photoAiid;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 素描图原始文件编码
     */
    @Excel(name = "素描图原始文件编码", orderNum = "16")
    private String sketchArwid;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 岩石名称
     */
    @Excel(name = "岩石名称", orderNum = "17")
    private String rockname;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 照片原始文件编号
     */
    @Excel(name = "照片原始文件编号", orderNum = "18")
    private String photoArwid;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 熔岩流名称
     */
    @Excel(name = "熔岩流名称", orderNum = "19")
    private String name;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 熔岩流符号代码
     */
    @Excel(name = "熔岩流符号代码", orderNum = "20")
    private String type;
    /**
     * 拍摄者
     */
    @Excel(name = "拍摄者", orderNum = "21")
    private String photographer;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 岩石类型
     */
    @Excel(name = "岩石类型", orderNum = "22")
    private Integer rocktype;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer ageName;
    private String rowNum;
    private String errorMsg;
}