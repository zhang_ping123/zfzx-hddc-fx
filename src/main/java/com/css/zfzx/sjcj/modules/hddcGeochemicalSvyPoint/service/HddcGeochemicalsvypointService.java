package com.css.zfzx.sjcj.modules.hddcGeochemicalSvyPoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyPoint.repository.entity.HddcGeochemicalsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyPoint.viewobjects.HddcGeochemicalsvypointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */

public interface HddcGeochemicalsvypointService {

    public JSONObject queryHddcGeochemicalsvypoints(HddcGeochemicalsvypointQueryParams queryParams, int curPage, int pageSize);

    public HddcGeochemicalsvypointEntity getHddcGeochemicalsvypoint(String id);

    public HddcGeochemicalsvypointEntity saveHddcGeochemicalsvypoint(HddcGeochemicalsvypointEntity hddcGeochemicalsvypoint);

    public HddcGeochemicalsvypointEntity updateHddcGeochemicalsvypoint(HddcGeochemicalsvypointEntity hddcGeochemicalsvypoint);

    public void deleteHddcGeochemicalsvypoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGeochemicalsvypointQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
