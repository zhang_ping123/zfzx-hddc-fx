package com.css.zfzx.sjcj.modules.hddcGeomorphypolygon.repository;

import com.css.zfzx.sjcj.modules.hddcGeomorphypolygon.repository.entity.HddcGeomorphypolygonEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphypolygon.viewobjects.HddcGeomorphypolygonQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-12-09
 */
public interface HddcGeomorphypolygonNativeRepository {

    Page<HddcGeomorphypolygonEntity> queryHddcGeomorphypolygons(HddcGeomorphypolygonQueryParams queryParams, int curPage, int pageSize);

    List<HddcGeomorphypolygonEntity> exportYhDisasters(HddcGeomorphypolygonQueryParams queryParams);
}
