package com.css.zfzx.sjcj.modules.hddcB4_SampleResultTable.repository;

import com.css.zfzx.sjcj.modules.hddcB4_SampleResultTable.repository.entity.HddcB4SampleresulttableEntity;
import com.css.zfzx.sjcj.modules.hddcB4_SampleResultTable.viewobjects.HddcB4SampleresulttableQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcB4SampleresulttableNativeRepository {

    Page<HddcB4SampleresulttableEntity> queryHddcB4Sampleresulttables(HddcB4SampleresulttableQueryParams queryParams, int curPage, int pageSize);

    List<HddcB4SampleresulttableEntity> exportYhDisasters(HddcB4SampleresulttableQueryParams queryParams);
}
