package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyRegion.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyRegion.repository.entity.HddcWyGeomorphysvyregionEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyRegion.viewobjects.HddcWyGeomorphysvyregionQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;
import java.util.List;

/**
 * @author lihelei
 * @date 2020-12-01
 */
public interface HddcWyGeomorphysvyregionNativeRepository {

    Page<HddcWyGeomorphysvyregionEntity> queryHddcWyGeomorphysvyregions(HddcWyGeomorphysvyregionQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyGeomorphysvyregion(HddcAppZztCountVo queryParams);

    List<HddcWyGeomorphysvyregionEntity> exportRegion(HddcAppZztCountVo queryParams);
}
