package com.css.zfzx.sjcj.modules.hddcGeomorStation.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcGeomorStation.repository.entity.HddcGeomorstationEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorStation.viewobjects.HddcGeomorstationQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author whj
 * @date 2020-11-30
 */

public interface HddcGeomorstationService {

    public JSONObject queryHddcGeomorstations(HddcGeomorstationQueryParams queryParams, int curPage, int pageSize);

    public HddcGeomorstationEntity getHddcGeomorstation(String id);

    public HddcGeomorstationEntity saveHddcGeomorstation(HddcGeomorstationEntity hddcGeomorstation);

    public HddcGeomorstationEntity updateHddcGeomorstation(HddcGeomorstationEntity hddcGeomorstation);

    public void deleteHddcGeomorstations(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGeomorstationQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
