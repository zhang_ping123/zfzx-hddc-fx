package com.css.zfzx.sjcj.modules.analysis.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.zfzx.sjcj.modules.analysis.service.DmjcService;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * TODO
 *
 * @author 王昊杰
 * @version 1.0
 * @date 2020/12/8  16:56
 */

@Slf4j
@RestController
@RequestMapping("/hddcDmjcAppCount")
public class dmjcController {
    @Autowired
    private DmjcService dmjcService;

    @GetMapping("/findAppCount")
    public RestResponse buildingNum(HddcAppZztCountVo queryParams) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try {
            Map<String, Object> data = dmjcService.hddcDmjcNumData(queryParams);
            json.put("data", data);
            json.put("message", "查询成功!");
            response = RestResponse.succeed(json);
        } catch (Exception e) {
            String errorMessage = "查询失败!";
            log.error(errorMessage, e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
}
