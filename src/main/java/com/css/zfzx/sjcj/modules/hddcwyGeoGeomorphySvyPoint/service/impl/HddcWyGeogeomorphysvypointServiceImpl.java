package com.css.zfzx.sjcj.modules.hddcwyGeoGeomorphySvyPoint.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcGeoGeomorphySvyPoint.repository.HddcGeogeomorphysvypointRepository;
import com.css.zfzx.sjcj.modules.hddcGeoGeomorphySvyPoint.repository.entity.HddcGeogeomorphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeoGeomorphySvyPoint.repository.HddcWyGeogeomorphysvypointNativeRepository;
import com.css.zfzx.sjcj.modules.hddcwyGeoGeomorphySvyPoint.repository.HddcWyGeogeomorphysvypointRepository;
import com.css.zfzx.sjcj.modules.hddcwyGeoGeomorphySvyPoint.repository.entity.HddcWyGeogeomorphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeoGeomorphySvyPoint.service.HddcWyGeogeomorphysvypointService;
import com.css.zfzx.sjcj.modules.hddcwyGeoGeomorphySvyPoint.viewobjects.HddcWyGeogeomorphysvypointQueryParams;
import com.css.zfzx.sjcj.modules.hddcwyGeoGeomorphySvyPoint.viewobjects.HddcWyGeogeomorphysvypointVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author zyb
 * @date 2020-12-01
 */
@Service
public class HddcWyGeogeomorphysvypointServiceImpl implements HddcWyGeogeomorphysvypointService {

	@Autowired
    private HddcWyGeogeomorphysvypointRepository hddcWyGeogeomorphysvypointRepository;
    @Autowired
    private HddcWyGeogeomorphysvypointNativeRepository hddcWyGeogeomorphysvypointNativeRepository;
    @Autowired
    private HddcGeogeomorphysvypointRepository hddcGeogeomorphysvypointRepository;

    @Override
    public JSONObject queryHddcWyGeogeomorphysvypoints(HddcWyGeogeomorphysvypointQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcWyGeogeomorphysvypointEntity> hddcWyGeogeomorphysvypointPage = this.hddcWyGeogeomorphysvypointNativeRepository.queryHddcWyGeogeomorphysvypoints(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcWyGeogeomorphysvypointPage);
        return jsonObject;
    }


    @Override
    public HddcWyGeogeomorphysvypointEntity getHddcWyGeogeomorphysvypoint(String uuid) {
        HddcWyGeogeomorphysvypointEntity hddcWyGeogeomorphysvypoint = this.hddcWyGeogeomorphysvypointRepository.findById(uuid).get();
         return hddcWyGeogeomorphysvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyGeogeomorphysvypointEntity saveHddcWyGeogeomorphysvypoint(HddcWyGeogeomorphysvypointEntity hddcWyGeogeomorphysvypoint) {
        String uuid = UUIDGenerator.getUUID();
        hddcWyGeogeomorphysvypoint.setUuid(uuid);
        if(StringUtils.isEmpty(hddcWyGeogeomorphysvypoint.getCreateUser())){
            hddcWyGeogeomorphysvypoint.setCreateUser(PlatformSessionUtils.getUserId());
        }
        hddcWyGeogeomorphysvypoint.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        hddcWyGeogeomorphysvypoint.setIsValid("1");

        HddcGeogeomorphysvypointEntity hddcGeogeomorphysvypointEntity=new HddcGeogeomorphysvypointEntity();
        BeanUtils.copyProperties(hddcWyGeogeomorphysvypoint,hddcGeogeomorphysvypointEntity);
        hddcGeogeomorphysvypointEntity.setExtends4(hddcWyGeogeomorphysvypoint.getTown());
        hddcGeogeomorphysvypointEntity.setExtends5(String.valueOf(hddcWyGeogeomorphysvypoint.getLon()));
        hddcGeogeomorphysvypointEntity.setExtends6(String.valueOf(hddcWyGeogeomorphysvypoint.getLat()));
        this.hddcGeogeomorphysvypointRepository.save(hddcGeogeomorphysvypointEntity);

        this.hddcWyGeogeomorphysvypointRepository.save(hddcWyGeogeomorphysvypoint);
        return hddcWyGeogeomorphysvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyGeogeomorphysvypointEntity updateHddcWyGeogeomorphysvypoint(HddcWyGeogeomorphysvypointEntity hddcWyGeogeomorphysvypoint) {
        HddcWyGeogeomorphysvypointEntity entity = hddcWyGeogeomorphysvypointRepository.findById(hddcWyGeogeomorphysvypoint.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcWyGeogeomorphysvypoint);
        hddcWyGeogeomorphysvypoint.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcWyGeogeomorphysvypoint.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcWyGeogeomorphysvypointRepository.save(hddcWyGeogeomorphysvypoint);
        return hddcWyGeogeomorphysvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcWyGeogeomorphysvypoints(List<String> ids) {
        List<HddcWyGeogeomorphysvypointEntity> hddcWyGeogeomorphysvypointList = this.hddcWyGeogeomorphysvypointRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcWyGeogeomorphysvypointList) && hddcWyGeogeomorphysvypointList.size() > 0) {
            for(HddcWyGeogeomorphysvypointEntity hddcWyGeogeomorphysvypoint : hddcWyGeogeomorphysvypointList) {
                this.hddcWyGeogeomorphysvypointRepository.delete(hddcWyGeogeomorphysvypoint);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public BigInteger queryHddcWyGeogeomorphysvypoint(HddcAppZztCountVo queryParams) {
        BigInteger bigInteger = hddcWyGeogeomorphysvypointNativeRepository.queryHddcWyGeogeomorphysvypoint(queryParams);
        return bigInteger;
    }

    @Override
    public List<HddcWyGeogeomorphysvypointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid) {
        List<HddcWyGeogeomorphysvypointEntity> list = hddcWyGeogeomorphysvypointRepository.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, isValid);
        return list;
    }

    @Override
    public void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response) {
        List<HddcWyGeogeomorphysvypointEntity> hddcWyGeogeomorphysvypointEntity = hddcWyGeogeomorphysvypointNativeRepository.exportGeoPoint(queryParams);
        List<HddcWyGeogeomorphysvypointVO> list=new ArrayList<>();
        for (HddcWyGeogeomorphysvypointEntity entity:hddcWyGeogeomorphysvypointEntity) {
            HddcWyGeogeomorphysvypointVO hddcWyGeogeomorphysvypointVO=new HddcWyGeogeomorphysvypointVO();
            BeanUtils.copyProperties(entity,hddcWyGeogeomorphysvypointVO);
            list.add(hddcWyGeogeomorphysvypointVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"地质地貌调查观测点-点","地质地貌调查观测点-点", HddcWyGeogeomorphysvypointVO.class,"地质地貌调查观测点-点.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcWyGeogeomorphysvypointVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcWyGeogeomorphysvypointVO.class, params);
            List<HddcWyGeogeomorphysvypointVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcWyGeogeomorphysvypointVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcWyGeogeomorphysvypointVO hddcWyGeogeomorphysvypointVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + hddcWyGeogeomorphysvypointVO.getRowNum() + "行" + hddcWyGeogeomorphysvypointVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcWyGeogeomorphysvypointVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcWyGeogeomorphysvypointEntity hddcWyGeogeomorphysvypointEntity = new HddcWyGeogeomorphysvypointEntity();
            HddcWyGeogeomorphysvypointVO hddcWyGeogeomorphysvypointVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(hddcWyGeogeomorphysvypointVO, hddcWyGeogeomorphysvypointEntity);
            saveHddcWyGeogeomorphysvypoint(hddcWyGeogeomorphysvypointEntity);
        }
    }
    @Override
    public List<HddcWyGeogeomorphysvypointEntity> findAllByCreateUserAndIsValid(String userId,String isValid) {
        List<HddcWyGeogeomorphysvypointEntity> list = hddcWyGeogeomorphysvypointRepository.findAllByCreateUserAndIsValid(userId,isValid);
        return list;
    }



    /**
     * 逻辑删除-根据项目id删除数据
     * @param projectIds
     */
    @Override
    public void deleteByProjectId(List<String> projectIds) {
        List<HddcWyGeogeomorphysvypointEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyGeogeomorphysvypointRepository.queryHddcA1InvrgnhasmaterialtablesByProjectId(projectIds);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyGeogeomorphysvypointEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyGeogeomorphysvypointRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }
    }

    /**
     * 逻辑删除-根据任务id删除数据
     * @param taskId
     */
    @Override
    public void deleteByTaskId(List<String> taskId) {
        List<HddcWyGeogeomorphysvypointEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyGeogeomorphysvypointRepository.queryHddcA1InvrgnhasmaterialtablesByTaskId(taskId);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyGeogeomorphysvypointEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyGeogeomorphysvypointRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }

    }














}
