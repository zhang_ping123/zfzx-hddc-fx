package com.css.zfzx.sjcj.modules.hddcB7_VolcanicSampleResultTable.repository;

import com.css.zfzx.sjcj.modules.hddcB7_VolcanicSampleResultTable.repository.entity.HddcB7VolcanicsampleresulttableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-26
 */
public interface HddcB7VolcanicsampleresulttableRepository extends JpaRepository<HddcB7VolcanicsampleresulttableEntity, String> {
}
