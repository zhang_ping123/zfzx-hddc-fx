package com.css.zfzx.sjcj.modules.hddcMainAFSvyRegion.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-12-14
 */
@Data
public class HddcMainafsvyregionQueryParams {


    private String province;
    private String city;
    private String area;
    private String proName;
    private String projectname;

}
