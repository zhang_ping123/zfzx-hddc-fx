package com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyPoint.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-12-02
 */
@Data
public class HddcWyGeochemicalsvypointQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String labelinfo;

    private String startTime;
    private String endTime;

    private String userId;
    private String taskId;
    private String projectId;
    private String isVaild;

}
