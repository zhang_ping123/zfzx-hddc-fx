package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyLine.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author lihelei
 * @date 2020-12-01
 */
@Data
public class HddcWyGeomorphysvylineVO implements Serializable {

    /**
     * 编号
     */
    private String objectCode;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 典型照片原始文件编号
     */
    @Excel(name="典型照片原始文件编号",orderNum = "17")
    private String photoArwid;
    /**
     * 拍摄者
     */
    @Excel(name="拍摄者",orderNum = "18")
    private String photographer;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 照片镜向
     */
    @Excel(name="照片镜向",orderNum = "15")
    private Integer photoviewingto;
    /**
     * 典型照片文件编号
     */
    @Excel(name = "典型照片文件编号",orderNum = "12")
    private String photoAiid;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 剖面线分析结果图原始文件编号
     */
    @Excel(name="剖面线分析结果图原始文件编号",orderNum = "10")
    private String profileArwid;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 任务名称
     */
    @Excel(name="任务名称",orderNum = "8")
    private String taskName;
    /**
     * 项目名称
     */
    @Excel(name="项目名称",orderNum = "7")
    private String projectName;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "4")
    private String area;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 剖面线分析结果图图像文件编号
     */
    @Excel(name="剖面线分析结果图图像文件编号",orderNum = "11")
    private String profileAiid;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 乡
     */
    @Excel(name="详细地址",orderNum = "13")
    private String town;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段5
     */
    @Excel(name="区域",orderNum = "6")
    private String extends5;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 野外编号
     */
    @Excel(name="野外编号",orderNum = "9")
    private String fieldid;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "3")
    private String city;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 测线名称
     */
    @Excel(name="测线名称",orderNum = "16")
    private String name;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 测线编号
     */
    @Excel(name="测线编号",orderNum = "1")
    private String id;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 微地貌测量工程编号
     */
    @Excel(name = "微地貌测量工程编号",orderNum = "5")
    private String geomorphysvyprjid;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备注-测量线描述及目的
     */
    @Excel(name="备注-测量线描述及目的",orderNum = "14")
    private String commentInfo;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "2")
    private String province;
    /**
     * 经度
     */
    private Double lon;
    /**
     * 维度
     */
    private Double lat;

    private String provinceName;
    private String cityName;
    private String areaName;

    private String rowNum;
    private String errorMsg;
}