package com.css.zfzx.sjcj.modules.hddcwyTrench.repository;

import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyTrench.repository.entity.HddcWyTrenchEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zyb
 * @date 2020-12-02
 */
public interface HddcWyTrenchRepository extends JpaRepository<HddcWyTrenchEntity, String> {

    List<HddcWyTrenchEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    List<HddcWyTrenchEntity> findAllByCreateUserAndIsValid(String userId, String isValid);

    @Query(nativeQuery = true, value = "select * from hddc_wy_trench where is_valid!=0 project_name in :projectIds")
    List<HddcWyTrenchEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_trench where task_name in :projectIds")
    List<HddcWyTrenchEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);

}
