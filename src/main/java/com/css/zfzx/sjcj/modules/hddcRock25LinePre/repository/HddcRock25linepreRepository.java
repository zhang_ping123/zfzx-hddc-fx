package com.css.zfzx.sjcj.modules.hddcRock25LinePre.repository;

import com.css.zfzx.sjcj.modules.hddcRock25LinePre.repository.entity.HddcRock25linepreEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcRock25linepreRepository extends JpaRepository<HddcRock25linepreEntity, String> {
}
