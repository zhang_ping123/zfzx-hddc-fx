package com.css.zfzx.sjcj.modules.hddcVolcanicSamplePoint.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltVO;
import com.css.zfzx.sjcj.modules.hddcVolcanicSamplePoint.repository.HddcVolcanicsamplepointNativeRepository;
import com.css.zfzx.sjcj.modules.hddcVolcanicSamplePoint.repository.HddcVolcanicsamplepointRepository;
import com.css.zfzx.sjcj.modules.hddcVolcanicSamplePoint.repository.entity.HddcVolcanicsamplepointEntity;
import com.css.zfzx.sjcj.modules.hddcVolcanicSamplePoint.service.HddcVolcanicsamplepointService;
import com.css.zfzx.sjcj.modules.hddcVolcanicSamplePoint.viewobjects.HddcVolcanicsamplepointQueryParams;
import com.css.zfzx.sjcj.modules.hddcVolcanicSamplePoint.viewobjects.HddcVolcanicsamplepointVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
@Service
public class HddcVolcanicsamplepointServiceImpl implements HddcVolcanicsamplepointService {

	@Autowired
    private HddcVolcanicsamplepointRepository hddcVolcanicsamplepointRepository;
    @Autowired
    private HddcVolcanicsamplepointNativeRepository hddcVolcanicsamplepointNativeRepository;

    @Override
    public JSONObject queryHddcVolcanicsamplepoints(HddcVolcanicsamplepointQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcVolcanicsamplepointEntity> hddcVolcanicsamplepointPage = this.hddcVolcanicsamplepointNativeRepository.queryHddcVolcanicsamplepoints(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcVolcanicsamplepointPage);
        return jsonObject;
    }


    @Override
    public HddcVolcanicsamplepointEntity getHddcVolcanicsamplepoint(String id) {
        HddcVolcanicsamplepointEntity hddcVolcanicsamplepoint = this.hddcVolcanicsamplepointRepository.findById(id).orElse(null);
         return hddcVolcanicsamplepoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcVolcanicsamplepointEntity saveHddcVolcanicsamplepoint(HddcVolcanicsamplepointEntity hddcVolcanicsamplepoint) {
        String uuid = UUIDGenerator.getUUID();
        hddcVolcanicsamplepoint.setUuid(uuid);
        hddcVolcanicsamplepoint.setCreateUser(PlatformSessionUtils.getUserId());
        hddcVolcanicsamplepoint.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcVolcanicsamplepointRepository.save(hddcVolcanicsamplepoint);
        return hddcVolcanicsamplepoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcVolcanicsamplepointEntity updateHddcVolcanicsamplepoint(HddcVolcanicsamplepointEntity hddcVolcanicsamplepoint) {
        HddcVolcanicsamplepointEntity entity = hddcVolcanicsamplepointRepository.findById(hddcVolcanicsamplepoint.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcVolcanicsamplepoint);
        hddcVolcanicsamplepoint.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcVolcanicsamplepoint.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcVolcanicsamplepointRepository.save(hddcVolcanicsamplepoint);
        return hddcVolcanicsamplepoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcVolcanicsamplepoints(List<String> ids) {
        List<HddcVolcanicsamplepointEntity> hddcVolcanicsamplepointList = this.hddcVolcanicsamplepointRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcVolcanicsamplepointList) && hddcVolcanicsamplepointList.size() > 0) {
            for(HddcVolcanicsamplepointEntity hddcVolcanicsamplepoint : hddcVolcanicsamplepointList) {
                this.hddcVolcanicsamplepointRepository.delete(hddcVolcanicsamplepoint);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcVolcanicsamplepointQueryParams queryParams, HttpServletResponse response) {
        List<HddcVolcanicsamplepointEntity> yhDisasterEntities = hddcVolcanicsamplepointNativeRepository.exportYhDisasters(queryParams);
        List<HddcVolcanicsamplepointVO> list=new ArrayList<>();
        for (HddcVolcanicsamplepointEntity entity:yhDisasterEntities) {
            HddcVolcanicsamplepointVO yhDisasterVO=new HddcVolcanicsamplepointVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"火山采样点-点","火山采样点-点",HddcVolcanicsamplepointVO.class,"火山采样点-点.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcVolcanicsamplepointVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcVolcanicsamplepointVO.class, params);
            List<HddcVolcanicsamplepointVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcVolcanicsamplepointVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcVolcanicsamplepointVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcVolcanicsamplepointVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcVolcanicsamplepointEntity yhDisasterEntity = new HddcVolcanicsamplepointEntity();
            HddcVolcanicsamplepointVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcVolcanicsamplepoint(yhDisasterEntity);
        }
    }

}
