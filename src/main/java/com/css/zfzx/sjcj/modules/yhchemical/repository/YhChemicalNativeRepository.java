package com.css.zfzx.sjcj.modules.yhchemical.repository;

import com.css.zfzx.sjcj.modules.yhchemical.repository.entity.YhChemicalEntity;
import com.css.zfzx.sjcj.modules.yhchemical.viewobjects.YhChemicalQueryParams;
import org.springframework.data.domain.Page;

/**
 * @author ly
 * @date 2020-11-04
 */
public interface YhChemicalNativeRepository {

    Page<YhChemicalEntity> queryYhChemicals(YhChemicalQueryParams queryParams, int curPage, int pageSize);
}
