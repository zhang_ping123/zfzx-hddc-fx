package com.css.zfzx.sjcj.modules.hddcwyVolcanicSvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyVolcanicSvyPoint.repository.entity.HddcWyVolcanicsvypointEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zyb
 * @date 2020-12-02
 */
public interface HddcWyVolcanicsvypointRepository extends JpaRepository<HddcWyVolcanicsvypointEntity, String> {

    List<HddcWyVolcanicsvypointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    List<HddcWyVolcanicsvypointEntity> findAllByCreateUserAndIsValid(String userId,  String isValid);




    @Query(nativeQuery = true, value = "select * from hddc_wy_volcanicsvypoint where is_valid!=0 project_name in :projectIds")
    List<HddcWyVolcanicsvypointEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_volcanicsvypoint where task_name in :projectIds")
    List<HddcWyVolcanicsvypointEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);
}
