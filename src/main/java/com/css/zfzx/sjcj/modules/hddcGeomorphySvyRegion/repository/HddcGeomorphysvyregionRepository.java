package com.css.zfzx.sjcj.modules.hddcGeomorphySvyRegion.repository;

import com.css.zfzx.sjcj.modules.hddcGeomorphySvyRegion.repository.entity.HddcGeomorphysvyregionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcGeomorphysvyregionRepository extends JpaRepository<HddcGeomorphysvyregionEntity, String> {
}
