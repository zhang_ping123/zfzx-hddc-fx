package com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.repository.entity.HddcC4MicrotremortableEntity;
import com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.service.HddcC4MicrotremortableService;
import com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.viewobjects.HddcC4MicrotremortableQueryParams;
import com.css.bpm.platform.utils.PlatformPageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangping
 * @date 2020-11-23
 */
@Slf4j
@RestController
@RequestMapping("/hddcC4Microtremortables")
public class HddcC4MicrotremortableController {
    @Autowired
    private HddcC4MicrotremortableService hddcC4MicrotremortableService;

    @GetMapping("/queryHddcC4Microtremortables")
    public RestResponse queryHddcC4Microtremortables(HttpServletRequest request, HddcC4MicrotremortableQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcC4MicrotremortableService.queryHddcC4Microtremortables(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcC4Microtremortable(@PathVariable String id) {
        RestResponse response = null;
        try{
            HddcC4MicrotremortableEntity hddcC4Microtremortable = hddcC4MicrotremortableService.getHddcC4Microtremortable(id);
            response = RestResponse.succeed(hddcC4Microtremortable);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcC4Microtremortable(@RequestBody HddcC4MicrotremortableEntity hddcC4Microtremortable) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcC4MicrotremortableService.saveHddcC4Microtremortable(hddcC4Microtremortable);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcC4Microtremortable(@RequestBody HddcC4MicrotremortableEntity hddcC4Microtremortable)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcC4MicrotremortableService.updateHddcC4Microtremortable(hddcC4Microtremortable);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcC4Microtremortables(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcC4MicrotremortableService.deleteHddcC4Microtremortables(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }


}