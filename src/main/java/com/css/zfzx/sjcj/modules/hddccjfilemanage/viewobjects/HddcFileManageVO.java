package com.css.zfzx.sjcj.modules.hddccjfilemanage.viewobjects;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-12-10
 */
@Data
public class HddcFileManageVO implements Serializable {

    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 文件编号
     */
    private String fileId;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 区（县）
     */
    private String area;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 文件名称
     */
    private String fileName;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 主键
     */
    private String uuid;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 乡
     */
    private String town;
    /**
     * 文件路径
     */
    private String filePath;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 市
     */
    private String city;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 省
     */
    private String province;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 分区标识
     */
    private Integer partionFlag;

    private String provinceName;
}