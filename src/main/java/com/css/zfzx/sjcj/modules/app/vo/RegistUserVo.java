package com.css.zfzx.sjcj.modules.app.vo;

import lombok.Data;

import java.util.Date;
@Data
public class RegistUserVo {
    /***
     * 用户等级
     */
    private String userLevel;
    /**
     * 用户名
     */
    private String userName;
    /***
     * 密码
     */
    private String pwd;
    /***
     *联系方式
     */
    private String mobilePhone;
    /**
     * 真实姓名
     */
    private String relName;
    /**
     * 身份证号
     */
    private String idNumber;
    /**
     * 所属单位
     */
    private String unit;
    /**
     * 行政区划（ID）
     */
    private String division;
    /**
     * 有效标识0、无效，1、有效
     */
    private String isValid;
    /***
     * 用户地址
     */
    private String userAddress;
}
