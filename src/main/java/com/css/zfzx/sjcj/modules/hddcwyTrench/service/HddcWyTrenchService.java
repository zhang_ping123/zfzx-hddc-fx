package com.css.zfzx.sjcj.modules.hddcwyTrench.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyTrench.repository.entity.HddcWyTrenchEntity;
import com.css.zfzx.sjcj.modules.hddcwyTrench.viewobjects.HddcWyTrenchQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-02
 */

public interface HddcWyTrenchService {

    public JSONObject queryHddcWyTrenchs(HddcWyTrenchQueryParams queryParams, int curPage, int pageSize);

    public HddcWyTrenchEntity getHddcWyTrench(String uuid);

    public HddcWyTrenchEntity saveHddcWyTrench(HddcWyTrenchEntity hddcWyTrench);

    public HddcWyTrenchEntity updateHddcWyTrench(HddcWyTrenchEntity hddcWyTrench);

    public void deleteHddcWyTrenchs(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    BigInteger queryHddcWyTrench(HddcAppZztCountVo queryParams);

    List<HddcWyTrenchEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);

    List<HddcWyTrenchEntity> findAllByCreateUserAndIsValid(String userId, String isValid);



    /**
     * 逻辑删除-根据项目id删除数据
     * @param ids
     */
    void deleteByProjectId(List<String> ids);

    /**
     * 逻辑删除-根据任务id删除数据
     * @param ids
     */
    void deleteByTaskId(List<String> ids);



}
