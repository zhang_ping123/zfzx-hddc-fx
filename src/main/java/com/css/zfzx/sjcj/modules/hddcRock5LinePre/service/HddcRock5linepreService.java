package com.css.zfzx.sjcj.modules.hddcRock5LinePre.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcRock5LinePre.repository.entity.HddcRock5linepreEntity;
import com.css.zfzx.sjcj.modules.hddcRock5LinePre.viewobjects.HddcRock5linepreQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */

public interface HddcRock5linepreService {

    public JSONObject queryHddcRock5linepres(HddcRock5linepreQueryParams queryParams, int curPage, int pageSize);

    public HddcRock5linepreEntity getHddcRock5linepre(String id);

    public HddcRock5linepreEntity saveHddcRock5linepre(HddcRock5linepreEntity hddcRock5linepre);

    public HddcRock5linepreEntity updateHddcRock5linepre(HddcRock5linepreEntity hddcRock5linepre);

    public void deleteHddcRock5linepres(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcRock5linepreQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
