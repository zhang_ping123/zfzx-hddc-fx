package com.css.zfzx.sjcj.modules.hddcRelocationISCatalog.repository.entity;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Data
@Entity
@Table(name="hddc_relocationiscatalog")
public class HddcRelocationiscatalogEntity implements Serializable {

    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 编号
     */
    //@Id
    @Column(name="id")
    private String id;
    /**
     * 编号
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 地名
     */
    @Column(name="locationname")
    private String locationname;
    /**
     * 所用P波到时数
     */
    @Column(name="nctp")
    private Integer nctp;
    /**
     * 秒（至少小数点后两位）
     */
    @Column(name="occurrencesecond")
    private Double occurrencesecond;
    /**
     * 震级 [Ml]
     */
    @Column(name="magnitude")
    private Double magnitude;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 深度定位误差 [米]
     */
    @Column(name="locationerrorz")
    private Double locationerrorz;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 时（24小时制）
     */
    @Column(name="occurrencehour")
    private String occurrencehour;
    /**
     * 震相数
     */
    @Column(name="seismicphasenum")
    private Integer seismicphasenum;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 分
     */
    @Column(name="occurrenceminute")
    private String occurrenceminute;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;
    /**
     * 所用S波到时数
     */
    @Column(name="ncts")
    private Integer ncts;
    /**
     * 东西向定位误差 [米]
     */
    @Column(name="locationerrorx")
    private Double locationerrorx;
    /**
     * 南北向定位误差 [米]
     */
    @Column(name="locationerrory")
    private Double locationerrory;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 震源深度 [公里]
     */
    @Column(name="depth")
    private Double depth;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 经度
     */
    @Column(name="lon")
    private Double lon;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 日期
     */
    @Column(name="occurrencedate")
    private String occurrencedate;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 宏观震中烈度
     */
    @Column(name="epicenter")
    private Integer epicenter;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 纬度
     */
    @Column(name="lat")
    private Double lat;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 定位残差 [秒]
     */
    @Column(name="rms")
    private Double rms;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 备注
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;

}

