package com.css.zfzx.sjcj.modules.hddcB4_SampleDataTable.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Data
public class HddcB4SampledatatableVO implements Serializable {

    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 区（县）
     */
    @Excel(name="区（县）",orderNum = "3")
    private String area;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 成果类型
     */
    @Excel(name="成果类型",orderNum = "9")
    private Integer resulttype;
    /**
     * 野外编号
     */
    @Excel(name="野外编号",orderNum = "6")
    private String fieldid;
    /**
     * 备注
     */
    private String remark;
    /**
     * 省
     */
    @Excel(name="省",orderNum = "1")
    private String province;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 成果数据原始文件编号
     */
    @Excel(name="成果数据原始文件编号",orderNum = "12")
    private String resultArwid;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 成果图件文件编号
     */
    @Excel(name="成果图件文件编号",orderNum = "11")
    private String resultAiid;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 测试方法
     */
    @Excel(name="测试方法",orderNum = "7")
    private Integer sampletestedmethod;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 市
     */
    @Excel(name="市",orderNum = "2")
    private String city;
    /**
     * 采样点编号
     */
    @Excel(name="采样点编号",orderNum = "5")
    private String samplepointid;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 采样工程编号
     */
    @Excel(name="采样工程编号",orderNum = "4")
    private String projectid;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 样品原始数据编号
     */
    @Excel(name="样品原始数据编号",orderNum = "8")
    private String surveyArwid;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备注
     */
    @Excel(name="备注",orderNum = "13")
    private String commentinfo;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 编号
     */
    private String id;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 成果名称
     */
    @Excel(name="成果名称",orderNum = "9")
    private String resultname;
    /**
     * 备选字段8
     */
    private String extends8;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String rowNum;
    private String errorMsg;

}