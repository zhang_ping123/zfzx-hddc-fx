package com.css.zfzx.sjcj.modules.hddcActiveFault.repository.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
@Entity
@Table(name="hddc_activefault")
public class HddcActivefaultEntity implements Serializable {

    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 平均垂直速率 [毫米/年]
     */
    @Column(name="avevrate")
    private Double avevrate;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 走向 [16方位]
     */
    @Column(name="strikedirection")
    private Integer strikedirection;
    /**
     * 平均破裂长度
     */
    @Column(name="averagerupturelength")
    private Integer averagerupturelength;
    /**
     * 最新垂直速率 [毫米/年]
     */
    @Column(name="newvrate")
    private Double newvrate;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 断层显示码
     */
    @Column(name="showcode")
    private Integer showcode;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;
    /**
     * 最新活动时代
     */
    @Column(name="latestactiveperiod")
    private Integer latestactiveperiod;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 破碎带宽度 [米]
     */
    @Column(name="width")
    private Double width;
    /**
     * 断层编号
     */
    @Column(name="id")
    private String id;
    /**
     * 编号
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 误差
     */
    @Column(name="maxhrateer")
    private Double maxhrateer;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 位移与平均速率起算时间
     */
    @Column(name="starttimeest")
    private String starttimeest;
    /**
     * 蠕动速率误差
     */
    @Column(name="creeprateer")
    private Double creeprateer;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 备注
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 断层符号上标位
     */
    @Column(name="nsb3")
    private Integer nsb3;
    /**
     * 最大水平速率 [毫米/年]
     */
    @Column(name="maxhrate")
    private Double maxhrate;
    /**
     * 垂直位移 [米]
     */
    @Column(name="vdisplaceest")
    private Double vdisplaceest;
    /**
     * 变形带宽度 [米]
     */
    @Column(name="fracturebeltwidth")
    private Double fracturebeltwidth;
    /**
     * 长度 [公里]
     */
    @Column(name="length")
    private Double length;
    /**
     * 误差
     */
    @Column(name="newvrateer")
    private Double newvrateer;
    /**
     * 误差
     */
    @Column(name="avevrateer")
    private Double avevrateer;
    /**
     * 平均同震位移 [米]
     */
    @Column(name="coseismicaverageslipest")
    private Double coseismicaverageslipest;
    /**
     * 平均水平速率 [毫米/年]
     */
    @Column(name="avehrate")
    private Double avehrate;
    /**
     * 错动深度
     */
    @Column(name="slipdepthest")
    private Integer slipdepthest;
    /**
     * 确定手段
     */
    @Column(name="method")
    private String method;
    /**
     * 误差
     */
    @Column(name="vdisplaceer")
    private Double vdisplaceer;
    /**
     * 最大同震位移 [米]
     */
    @Column(name="coseismicmaxslipest")
    private Double coseismicmaxslipest;
    /**
     * 误差
     */
    @Column(name="newhrateer")
    private Double newhrateer;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 误差
     */
    @Column(name="tdisplaceer")
    private Double tdisplaceer;
    /**
     * 平均滑动速率 [毫米/年]
     */
    @Column(name="averagesliprateest")
    private Double averagesliprateest;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 误差
     */
    @Column(name="hdisplaceer")
    private Double hdisplaceer;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 蠕动速率 [毫米/年]
     */
    @Column(name="creeprateest")
    private Double creeprateest;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 错动深度误差
     */
    @Column(name="slipdepther")
    private Integer slipdepther;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 古地震复发间隔上限
     */
    @Column(name="eqeventritop")
    private Integer eqeventritop;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 最大垂直速率 [米]
     */
    @Column(name="maxvrate")
    private Double maxvrate;
    /**
     * 平均同震位移误差
     */
    @Column(name="coseismicaveragesliper")
    private Double coseismicaveragesliper;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 误差
     */
    @Column(name="maxvrateer")
    private Double maxvrateer;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 最新地震平均位移
     */
    @Column(name="latestcoseismicslipest")
    private Double latestcoseismicslipest;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 断层段名称
     */
    @Column(name="faultsegmentname")
    private String faultsegmentname;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 最新地震平均位移误差
     */
    @Column(name="latestcoseismicsliper")
    private Double latestcoseismicsliper;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 最大同震位移误差
     */
    @Column(name="coseismicmaxsliper")
    private Double coseismicmaxsliper;
    /**
     * 断层性质
     */
    @Column(name="feature")
    private Integer feature;
    /**
     * 断层符号基础位
     */
    @Column(name="nsb1")
    private String nsb1;
    /**
     * 最新水平速率 [毫米/年]
     */
    @Column(name="newhrate")
    private Double newhrate;
    /**
     * 误差
     */
    @Column(name="avehrateer")
    private Double avehrateer;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 水平//张缩位移 [米]
     */
    @Column(name="tdisplaceest")
    private Double tdisplaceest;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 平均滑动速率误差
     */
    @Column(name="averagesliprateer")
    private Double averagesliprateer;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 最新速率起算时间
     */
    @Column(name="starttimenewest")
    private Double starttimenewest;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 断层符号下标位
     */
    @Column(name="nsb2")
    private Integer nsb2;
    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 工作底图比例尺（分母）
     */
    @Column(name="scale")
    private Integer scale;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 最大破裂长度
     */
    @Column(name="maxrupturelength")
    private Integer maxrupturelength;
    /**
     * 上断点埋深 [米]
     */
    @Column(name="topdepth")
    private String topdepth;
    /**
     * 断层名称
     */
    @Column(name="name")
    private String name;
    /**
     * 断裂带名称
     */
    @Column(name="fracturezonename")
    private String fracturezonename;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 倾角 [度]
     */
    @Column(name="clination")
    private Integer clination;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 古地震复发间隔
     */
    @Column(name="eqeventribottom")
    private Integer eqeventribottom;
    /**
     * 古地震事件次数
     */
    @Column(name="eqeventcount")
    private Integer eqeventcount;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 断层走向 [度]
     */
    @Column(name="strike")
    private Integer strike;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 走向水平位移 [米]
     */
    @Column(name="hdisplaceest")
    private Double hdisplaceest;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 倾向 [16方位]
     */
    @Column(name="direction")
    private Integer direction;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 最晚地震离逝时间
     */
    @Column(name="elapsetimeforlatesteq")
    private Integer elapsetimeforlatesteq;

}

