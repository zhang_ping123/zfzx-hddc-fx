package com.css.zfzx.sjcj.modules.hddcGeochemicalAbnPoint.repository;

import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnPoint.repository.entity.HddcGeochemicalabnpointEntity;
import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnPoint.viewobjects.HddcGeochemicalabnpointQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
public interface HddcGeochemicalabnpointNativeRepository {

    Page<HddcGeochemicalabnpointEntity> queryHddcGeochemicalabnpoints(HddcGeochemicalabnpointQueryParams queryParams, int curPage, int pageSize);

    List<HddcGeochemicalabnpointEntity> exportYhDisasters(HddcGeochemicalabnpointQueryParams queryParams);
}
