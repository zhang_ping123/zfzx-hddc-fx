package com.css.zfzx.sjcj.modules.hddcGeomorphypolygon.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcGeomorphypolygon.repository.HddcGeomorphypolygonNativeRepository;
import com.css.zfzx.sjcj.modules.hddcGeomorphypolygon.repository.HddcGeomorphypolygonRepository;
import com.css.zfzx.sjcj.modules.hddcGeomorphypolygon.repository.entity.HddcGeomorphypolygonEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphypolygon.service.HddcGeomorphypolygonService;
import com.css.zfzx.sjcj.modules.hddcGeomorphypolygon.viewobjects.HddcGeomorphypolygonQueryParams;
import com.css.zfzx.sjcj.modules.hddcGeomorphypolygon.viewobjects.HddcGeomorphypolygonVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-09
 */
@Service
public class HddcGeomorphypolygonServiceImpl implements HddcGeomorphypolygonService {

	@Autowired
    private HddcGeomorphypolygonRepository hddcGeomorphypolygonRepository;
    @Autowired
    private HddcGeomorphypolygonNativeRepository hddcGeomorphypolygonNativeRepository;

    @Override
    public JSONObject queryHddcGeomorphypolygons(HddcGeomorphypolygonQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcGeomorphypolygonEntity> hddcGeomorphypolygonPage = this.hddcGeomorphypolygonNativeRepository.queryHddcGeomorphypolygons(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcGeomorphypolygonPage);
        return jsonObject;
    }


    @Override
    public HddcGeomorphypolygonEntity getHddcGeomorphypolygon(String id) {
        HddcGeomorphypolygonEntity hddcGeomorphypolygon = this.hddcGeomorphypolygonRepository.findById(id).orElse(null);
         return hddcGeomorphypolygon;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeomorphypolygonEntity saveHddcGeomorphypolygon(HddcGeomorphypolygonEntity hddcGeomorphypolygon) {
        String uuid = UUIDGenerator.getUUID();
        hddcGeomorphypolygon.setUuid(uuid);
        hddcGeomorphypolygon.setCreateUser(PlatformSessionUtils.getUserId());
        hddcGeomorphypolygon.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeomorphypolygonRepository.save(hddcGeomorphypolygon);
        return hddcGeomorphypolygon;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeomorphypolygonEntity updateHddcGeomorphypolygon(HddcGeomorphypolygonEntity hddcGeomorphypolygon) {
        HddcGeomorphypolygonEntity entity = hddcGeomorphypolygonRepository.findById(hddcGeomorphypolygon.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcGeomorphypolygon);
        hddcGeomorphypolygon.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcGeomorphypolygon.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeomorphypolygonRepository.save(hddcGeomorphypolygon);
        return hddcGeomorphypolygon;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcGeomorphypolygons(List<String> ids) {
        List<HddcGeomorphypolygonEntity> hddcGeomorphypolygonList = this.hddcGeomorphypolygonRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcGeomorphypolygonList) && hddcGeomorphypolygonList.size() > 0) {
            for(HddcGeomorphypolygonEntity hddcGeomorphypolygon : hddcGeomorphypolygonList) {
                this.hddcGeomorphypolygonRepository.delete(hddcGeomorphypolygon);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcGeomorphypolygonQueryParams queryParams, HttpServletResponse response) {
        List<HddcGeomorphypolygonEntity> yhDisasterEntities = hddcGeomorphypolygonNativeRepository.exportYhDisasters(queryParams);
        List<HddcGeomorphypolygonVO> list=new ArrayList<>();
        for (HddcGeomorphypolygonEntity entity:yhDisasterEntities) {
            HddcGeomorphypolygonVO yhDisasterVO=new HddcGeomorphypolygonVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"地貌面-面","地貌面-面",HddcGeomorphypolygonVO.class,"地貌面-面.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcGeomorphypolygonVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcGeomorphypolygonVO.class, params);
            List<HddcGeomorphypolygonVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcGeomorphypolygonVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcGeomorphypolygonVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcGeomorphypolygonVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcGeomorphypolygonEntity yhDisasterEntity = new HddcGeomorphypolygonEntity();
            HddcGeomorphypolygonVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcGeomorphypolygon(yhDisasterEntity);
        }
    }

}
