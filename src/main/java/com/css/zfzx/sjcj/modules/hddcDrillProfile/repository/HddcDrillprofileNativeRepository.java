package com.css.zfzx.sjcj.modules.hddcDrillProfile.repository;

import com.css.zfzx.sjcj.modules.hddcDrillProfile.repository.entity.HddcDrillprofileEntity;
import com.css.zfzx.sjcj.modules.hddcDrillProfile.viewobjects.HddcDrillprofileQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcDrillprofileNativeRepository {

    Page<HddcDrillprofileEntity> queryHddcDrillprofiles(HddcDrillprofileQueryParams queryParams, int curPage, int pageSize);

    List<HddcDrillprofileEntity> exportYhDisasters(HddcDrillprofileQueryParams queryParams);
}
