package com.css.zfzx.sjcj.modules.hddcStratigraphy5LinePre.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltVO;
import com.css.zfzx.sjcj.modules.hddcStratigraphy5LinePre.repository.HddcStratigraphy5linepreNativeRepository;
import com.css.zfzx.sjcj.modules.hddcStratigraphy5LinePre.repository.HddcStratigraphy5linepreRepository;
import com.css.zfzx.sjcj.modules.hddcStratigraphy5LinePre.repository.entity.HddcStratigraphy5linepreEntity;
import com.css.zfzx.sjcj.modules.hddcStratigraphy5LinePre.service.HddcStratigraphy5linepreService;
import com.css.zfzx.sjcj.modules.hddcStratigraphy5LinePre.viewobjects.HddcStratigraphy5linepreQueryParams;
import com.css.zfzx.sjcj.modules.hddcStratigraphy5LinePre.viewobjects.HddcStratigraphy5linepreVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Service
public class HddcStratigraphy5linepreServiceImpl implements HddcStratigraphy5linepreService {

	@Autowired
    private HddcStratigraphy5linepreRepository hddcStratigraphy5linepreRepository;
    @Autowired
    private HddcStratigraphy5linepreNativeRepository hddcStratigraphy5linepreNativeRepository;

    @Override
    public JSONObject queryHddcStratigraphy5linepres(HddcStratigraphy5linepreQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcStratigraphy5linepreEntity> hddcStratigraphy5lineprePage = this.hddcStratigraphy5linepreNativeRepository.queryHddcStratigraphy5linepres(queryParams, curPage, pageSize);
        List<HddcStratigraphy5linepreVO> hddcStratigraphy5linepreVOList = new ArrayList<>();
        List<HddcStratigraphy5linepreEntity> hddcStratigraphy5linepreEntityList = hddcStratigraphy5lineprePage.getContent();
        for(HddcStratigraphy5linepreEntity hddcStratigraphy5linepreEntity : hddcStratigraphy5linepreEntityList){
            HddcStratigraphy5linepreVO hddcStratigraphy5linepreVO = new HddcStratigraphy5linepreVO();
            BeanUtils.copyProperties(hddcStratigraphy5linepreEntity, hddcStratigraphy5linepreVO);
            if(PlatformObjectUtils.isNotEmpty(hddcStratigraphy5linepreEntity.getGeologyboundaryline())) {
                String dictItemCode=String.valueOf(hddcStratigraphy5linepreEntity.getGeologyboundaryline());
                hddcStratigraphy5linepreVO.setGeologyboundarylineName(findByDictCodeAndDictItemCode("StraTouchTypeCVD", dictItemCode));
            }
            hddcStratigraphy5linepreVOList.add(hddcStratigraphy5linepreVO);
        }
        Page<HddcStratigraphy5linepreVO> HddcStratigraphy5linepreVOPage = new PageImpl(hddcStratigraphy5linepreVOList, hddcStratigraphy5lineprePage.getPageable(), hddcStratigraphy5lineprePage.getTotalElements());
        JSONObject jsonObject = PlatformPageUtils.formatPageData(HddcStratigraphy5linepreVOPage);
        return jsonObject;
    }


    @Override
    public HddcStratigraphy5linepreEntity getHddcStratigraphy5linepre(String id) {
        HddcStratigraphy5linepreEntity hddcStratigraphy5linepre = this.hddcStratigraphy5linepreRepository.findById(id).orElse(null);
         return hddcStratigraphy5linepre;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcStratigraphy5linepreEntity saveHddcStratigraphy5linepre(HddcStratigraphy5linepreEntity hddcStratigraphy5linepre) {
        String uuid = UUIDGenerator.getUUID();
        hddcStratigraphy5linepre.setUuid(uuid);
        hddcStratigraphy5linepre.setCreateUser(PlatformSessionUtils.getUserId());
        hddcStratigraphy5linepre.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcStratigraphy5linepreRepository.save(hddcStratigraphy5linepre);
        return hddcStratigraphy5linepre;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcStratigraphy5linepreEntity updateHddcStratigraphy5linepre(HddcStratigraphy5linepreEntity hddcStratigraphy5linepre) {
        HddcStratigraphy5linepreEntity entity = hddcStratigraphy5linepreRepository.findById(hddcStratigraphy5linepre.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcStratigraphy5linepre);
        hddcStratigraphy5linepre.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcStratigraphy5linepre.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcStratigraphy5linepreRepository.save(hddcStratigraphy5linepre);
        return hddcStratigraphy5linepre;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcStratigraphy5linepres(List<String> ids) {
        List<HddcStratigraphy5linepreEntity> hddcStratigraphy5linepreList = this.hddcStratigraphy5linepreRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcStratigraphy5linepreList) && hddcStratigraphy5linepreList.size() > 0) {
            for(HddcStratigraphy5linepreEntity hddcStratigraphy5linepre : hddcStratigraphy5linepreList) {
                this.hddcStratigraphy5linepreRepository.delete(hddcStratigraphy5linepre);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public String findByDictCodeAndDictItemCode(String dictCode, String dictItemCode){
        DictItemEntity dictItem = PlatformAPI.getDictAPI().getValidDictItemByDictCodeAndDictItemCode(dictCode, dictItemCode);
        String dictItemName = "";
        if(PlatformObjectUtils.isNotEmpty(dictItem)){
            dictItemName = dictItem.getDictItemName();
        }
        return dictItemName;
    }

    @Override
    public void exportFile(HddcStratigraphy5linepreQueryParams queryParams, HttpServletResponse response) {
        List<HddcStratigraphy5linepreEntity> yhDisasterEntities = hddcStratigraphy5linepreNativeRepository.exportYhDisasters(queryParams);
        List<HddcStratigraphy5linepreVO> list=new ArrayList<>();
        for (HddcStratigraphy5linepreEntity entity:yhDisasterEntities) {
            HddcStratigraphy5linepreVO yhDisasterVO=new HddcStratigraphy5linepreVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"1：5万底图地层线-线","1：5万底图地层线-线",HddcStratigraphy5linepreVO.class,"1：5万底图地层线-线.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcStratigraphy5linepreVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcStratigraphy5linepreVO.class, params);
            List<HddcStratigraphy5linepreVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcStratigraphy5linepreVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcStratigraphy5linepreVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcStratigraphy5linepreVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcStratigraphy5linepreEntity yhDisasterEntity = new HddcStratigraphy5linepreEntity();
            HddcStratigraphy5linepreVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcStratigraphy5linepre(yhDisasterEntity);
        }
    }
}
