package com.css.zfzx.sjcj.modules.hddcwyStratigraphySvyPoint.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-12-01
 */
@Data
public class HddcWyStratigraphysvypointVO implements Serializable {

    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 倾角 [度]
     */
    @Excel(name="倾角 [度]",orderNum = "20")
    private Integer clination;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 接触关系
     */
    @Excel(name="接触关系",orderNum = "11")
    private Integer touchrelation;
    /**
     * 备注
     */
    private String commentInfo;
    /**
     * 地层点编号
     */
    @Excel(name="地层点编号",orderNum = "1")
    private String id;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 市
     */
    @Excel(name="市",orderNum = "3")
    private String city;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 区（县）
     */
    @Excel(name="区（县）",orderNum = "4")
    private String area;
    /**
     * 是否修改工作底图
     */
    @Excel(name="是否修改工作底图",orderNum = "12")
    private Integer ismodifyworkmap;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 地层描述
     */
    @Excel(name="地层描述",orderNum = "33")
    private String stratigraphydescription;
    /**
     * 照片镜向
     */
    @Excel(name="照片镜向",orderNum = "19")
    private Integer photoviewingto;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 典型剖面图图像文件编号
     */
    @Excel(name="典型剖面图图像文件编号",orderNum = "25")
    private String typicalprofileAiid;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 典型照片文件编号
     */
    @Excel(name="典型照片文件编号",orderNum = "21")
    private String photoAiid;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 任务名称
     */
    @Excel(name="任务名称",orderNum = "9")
    private String taskName;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 平面图原始文件编号
     */
    @Excel(name="平面图原始文件编号",orderNum = "14")
    private String sketchArwid;
    /**
     * 平面图文件编号
     */
    @Excel(name="平面图文件编号",orderNum = "19")
    private String sketchAiid;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 符号或标注旋转角度
     */
    @Excel(name="符号或标注旋转角度",orderNum = "28")
    private Double lastangle;
    /**
     * 地质调查观测点编号
     */
    @Excel(name="地质调查观测点编号",orderNum = "13")
    private String geologicalsvypointid;
    /**
     * 项目名称
     */
    @Excel(name="项目名称",orderNum = "8")
    private String projectName;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 是否倒转地层产状
     */
    @Excel(name="是否倒转地层产状",orderNum = "30")
    private Integer isreversed;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 是否在图中显示
     */
    @Excel(name="是否在图中显示",orderNum = "11")
    private Integer isinmap;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 地层厚度 [米]
     */
    @Excel(name="地层厚度 [米]",orderNum = "15")
    private String thickness;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 实测倾向 [度]
     */
    @Excel(name="实测倾向 [度]",orderNum = "24")
    private Integer dip;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 拍摄者
     */
    @Excel(name="拍摄者",orderNum = "26")
    private String photographer;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 典型照片原始文件编号
     */
    @Excel(name="典型照片原始文件编号",orderNum = "27")
    private String photoArwid;
    /**
     * 典型剖面图原始文件编号
     */
    @Excel(name="典型剖面图原始文件编号",orderNum = "17")
    private String typicalprofileArwid;
    /**
     * 备选字段28
     */
    private String extends28;

    /**
     * 野外编号
     */
    @Excel(name="野外编号",orderNum = "18")
    private String fieldid;
    /**
     * 接触关系描述
     */
    @Excel(name="接触关系描述",orderNum = "32")
    private String touchredescription;
    /**
     * 数据来源
     */
    @Excel(name="数据来源",orderNum = "23")
    private String datasource;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 走向 [°]
     */
    @Excel(name="走向 [°]",orderNum = "31")
    private Integer strike;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备注
     */
    @Excel(name="备注",orderNum = "16")
    private String remark;
    /**
     * 省
     */
    @Excel(name="省",orderNum = "2")
    private String province;
    /**
     * 地层名称
     */
    @Excel(name="地层名称",orderNum = "22")
    private String stratigraphyname;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 乡
     */
    @Excel(name="详细地址",orderNum = "5")
    private String town;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 村
     */
    private String village;
    /**
     * 经度
     */
    @Excel(name="经度",orderNum = "6")
    private Double lon;
    /**
     * 维度
     */
    @Excel(name="纬度",orderNum = "7")
    private Double lat;


    private String provinceName;
    private String cityName;
    private String areaName;

    private String rowNum;
    private String errorMsg;
}