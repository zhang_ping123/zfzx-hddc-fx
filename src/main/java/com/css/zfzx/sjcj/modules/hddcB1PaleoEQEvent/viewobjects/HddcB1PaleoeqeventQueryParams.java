package com.css.zfzx.sjcj.modules.hddcB1PaleoEQEvent.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Data
public class HddcB1PaleoeqeventQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

}
