package com.css.zfzx.sjcj.modules.hddcB7_VolcanicSvyProjectTable.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB7_VolcanicSvyProjectTable.repository.entity.HddcB7VolcanicsvyprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB7_VolcanicSvyProjectTable.viewobjects.HddcB7VolcanicsvyprojecttableQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-26
 */

public interface HddcB7VolcanicsvyprojecttableService {

    public JSONObject queryHddcB7Volcanicsvyprojecttables(HddcB7VolcanicsvyprojecttableQueryParams queryParams, int curPage, int pageSize);

    public HddcB7VolcanicsvyprojecttableEntity getHddcB7Volcanicsvyprojecttable(String id);

    public HddcB7VolcanicsvyprojecttableEntity saveHddcB7Volcanicsvyprojecttable(HddcB7VolcanicsvyprojecttableEntity hddcB7Volcanicsvyprojecttable);

    public HddcB7VolcanicsvyprojecttableEntity updateHddcB7Volcanicsvyprojecttable(HddcB7VolcanicsvyprojecttableEntity hddcB7Volcanicsvyprojecttable);

    public void deleteHddcB7Volcanicsvyprojecttables(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB7VolcanicsvyprojecttableQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
