package com.css.zfzx.sjcj.modules.hddcC4MagnitudeEstimateTable.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.zfzx.sjcj.modules.hddcC4MagnitudeEstimateTable.repository.entity.HddcC4MagnitudeestimatetableEntity;
import com.css.zfzx.sjcj.modules.hddcC4MagnitudeEstimateTable.service.HddcC4MagnitudeestimatetableService;
import com.css.zfzx.sjcj.modules.hddcC4MagnitudeEstimateTable.viewobjects.HddcC4MagnitudeestimatetableQueryParams;
import com.css.bpm.platform.utils.PlatformPageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangping
 * @date 2020-11-23
 */
@Slf4j
@RestController
@RequestMapping("/hddcC4Magnitudeestimatetables")
public class HddcC4MagnitudeestimatetableController {
    @Autowired
    private HddcC4MagnitudeestimatetableService hddcC4MagnitudeestimatetableService;

    @GetMapping("/queryHddcC4Magnitudeestimatetables")
    public RestResponse queryHddcC4Magnitudeestimatetables(HttpServletRequest request, HddcC4MagnitudeestimatetableQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcC4MagnitudeestimatetableService.queryHddcC4Magnitudeestimatetables(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcC4Magnitudeestimatetable(@PathVariable String id) {
        RestResponse response = null;
        try{
            HddcC4MagnitudeestimatetableEntity hddcC4Magnitudeestimatetable = hddcC4MagnitudeestimatetableService.getHddcC4Magnitudeestimatetable(id);
            response = RestResponse.succeed(hddcC4Magnitudeestimatetable);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcC4Magnitudeestimatetable(@RequestBody HddcC4MagnitudeestimatetableEntity hddcC4Magnitudeestimatetable) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcC4MagnitudeestimatetableService.saveHddcC4Magnitudeestimatetable(hddcC4Magnitudeestimatetable);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcC4Magnitudeestimatetable(@RequestBody HddcC4MagnitudeestimatetableEntity hddcC4Magnitudeestimatetable)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcC4MagnitudeestimatetableService.updateHddcC4Magnitudeestimatetable(hddcC4Magnitudeestimatetable);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcC4Magnitudeestimatetables(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcC4MagnitudeestimatetableService.deleteHddcC4Magnitudeestimatetables(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }


}