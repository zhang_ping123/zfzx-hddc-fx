package com.css.zfzx.sjcj.modules.hddcDResultReportTable.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.bpm.platform.utils.PlatformPageUtils;
import com.css.zfzx.sjcj.modules.hddcDResultReportTable.repository.entity.HddcDResultreporttableEntity;
import com.css.zfzx.sjcj.modules.hddcDResultReportTable.service.HddcDResultreporttableService;
import com.css.zfzx.sjcj.modules.hddcDResultReportTable.viewobjects.HddcDResultreporttableQueryParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-19
 */
@Slf4j
@RestController
@RequestMapping("hddc/hddcDResultreporttables")
public class HddcDResultreporttableController {
    @Autowired
    private HddcDResultreporttableService hddcDResultreporttableService;

    @GetMapping("/queryHddcDResultreporttables")
    public RestResponse queryHddcDResultreporttables(HttpServletRequest request, HddcDResultreporttableQueryParams queryParams) {
        RestResponse response = null;
        try {
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            String sort=request.getParameter("sort");
            String order=request.getParameter("order");
            JSONObject jsonObject = hddcDResultreporttableService.queryHddcDResultreporttables(queryParams, curPage, pageSize,sort,order);
            response = RestResponse.succeed(jsonObject);
        } catch (Exception e) {
            String errorMessage = "查询失败!";
            log.error(errorMessage, e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcDResultreporttable(@PathVariable String id) {
        RestResponse response = null;
        try {
            HddcDResultreporttableEntity hddcDResultreporttable = hddcDResultreporttableService.getHddcDResultreporttable(id);
            response = RestResponse.succeed(hddcDResultreporttable);
        } catch (Exception e) {
            String errorMessage = "获取失败!";
            log.error(errorMessage, e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @PostMapping
    public RestResponse saveHddcDResultreporttable(@RequestBody HddcDResultreporttableEntity hddcDResultreporttable) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try {
            hddcDResultreporttableService.saveHddcDResultreporttable(hddcDResultreporttable);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        } catch (Exception e) {
            String errorMessage = "新增失败!";
            log.error(errorMessage, e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }

    @PutMapping
    public RestResponse updateHddcDResultreporttable(@RequestBody HddcDResultreporttableEntity hddcDResultreporttable) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try {
            hddcDResultreporttableService.updateHddcDResultreporttable(hddcDResultreporttable);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        } catch (Exception e) {
            String errorMessage = "修改失败!";
            log.error(errorMessage, e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @DeleteMapping
    public RestResponse deleteHddcDResultreporttables(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try {
            hddcDResultreporttableService.deleteHddcDResultreporttables(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        } catch (Exception e) {
            String errorMessage = "删除失败!";
            log.error(errorMessage, e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcDResultreporttableService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }

}