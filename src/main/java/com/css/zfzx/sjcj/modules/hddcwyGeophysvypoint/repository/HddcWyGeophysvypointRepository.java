package com.css.zfzx.sjcj.modules.hddcwyGeophysvypoint.repository;

import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeophysvypoint.repository.entity.HddcWyGeophysvypointEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */
public interface HddcWyGeophysvypointRepository extends JpaRepository<HddcWyGeophysvypointEntity, String> {

    List<HddcWyGeophysvypointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    List<HddcWyGeophysvypointEntity> findAllByCreateUserAndIsValid(String userId, String isValid);



    @Query(nativeQuery = true, value = "select * from hddc_wy_geophysvypoint where is_valid!=0 project_name in :projectIds")
    List<HddcWyGeophysvypointEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_geophysvypoint where task_name in :projectIds")
    List<HddcWyGeophysvypointEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);

}
