package com.css.zfzx.sjcj.modules.hddcISCatalog.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Data
public class HddcIscatalogQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String locationname;

}
