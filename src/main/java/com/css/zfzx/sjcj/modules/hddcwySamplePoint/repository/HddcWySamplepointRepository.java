package com.css.zfzx.sjcj.modules.hddcwySamplePoint.repository;

import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwySamplePoint.repository.entity.HddcWySamplepointEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */
public interface HddcWySamplepointRepository extends JpaRepository<HddcWySamplepointEntity, String> {

    List<HddcWySamplepointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    List<HddcWySamplepointEntity> findAllByCreateUserAndIsValid(String userId, String isValid);


    @Query(nativeQuery = true, value = "select * from hddc_wy_samplepoint where is_valid!=0 project_name in :projectIds")
    List<HddcWySamplepointEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_samplepoint where task_name in :projectIds")
    List<HddcWySamplepointEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);

}
