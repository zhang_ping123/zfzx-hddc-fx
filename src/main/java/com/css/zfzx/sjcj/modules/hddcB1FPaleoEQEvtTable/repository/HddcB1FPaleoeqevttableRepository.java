package com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtTable.repository;

import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtTable.repository.entity.HddcB1FPaleoeqevttableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcB1FPaleoeqevttableRepository extends JpaRepository<HddcB1FPaleoeqevttableEntity, String> {
}
