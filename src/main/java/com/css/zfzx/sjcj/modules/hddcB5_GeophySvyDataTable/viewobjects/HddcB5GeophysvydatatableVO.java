package com.css.zfzx.sjcj.modules.hddcB5_GeophySvyDataTable.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Data
public class HddcB5GeophysvydatatableVO implements Serializable {

    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 探测原始数据编号
     */
    @Excel(name = "探测原始数据编号")
    private String surveyArwid;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 乡
     */
    private String town;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 市
     */
    @Excel(name = "市",orderNum = "2")
    private String city;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 记录编号
     */
    private String id;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 成果数据原始文件编号
     */
    @Excel(name = "成果数据原始文件编号")
    private String resultArwid;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 探测方法
     */
    @Excel(name = "探测方法")
    private Integer svymethod;
    /**
     * 省
     */
    @Excel(name = "省",orderNum = "1")
    private String province;
    /**
     * 探测工程编号
     */
    @Excel(name = "探测工程编号")
    private String projectid;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）",orderNum = "3")
    private String area;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 测线编号
     */
    @Excel(name = "测线编号")
    private String svylineid;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 测点编号
     */
    @Excel(name = "测点编号")
    private String svypointid;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备注
     */
    @Excel(name = "备注")
    private String commentInfo;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 成果类型
     */
    @Excel(name = "成果类型")
    private Integer resulttype;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 成果名称
     */
    @Excel(name = "成果名称")
    private String resultname;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 成果图件文件编号
     */
    @Excel(name = "成果图件文件编号")
    private String resultAiid;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段18
     */
    private String extends18;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer svymethodName;
    private Integer resulttypeName;

    private String rowNum;
    private String errorMsg;
}