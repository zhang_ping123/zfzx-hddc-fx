package com.css.zfzx.sjcj.modules.hddcGMInterpretationLine.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-26
 */
@Data
public class HddcGminterpretationlineVO implements Serializable {

    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 重力异常解释依据
     */
    @Excel(name = "重力异常解释依据", orderNum = "4")
    private String interpretationaccordingg;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 断裂发育深度
     */
    @Excel(name = "断裂发育深度", orderNum = "5")
    private String faultdepth;
    /**
     * 解译线编号
     */
    @Excel(name = "解译线编号", orderNum = "6")
    private String id;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "7")
    private String commentinfo;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 活动时代
     */
    @Excel(name = "活动时代", orderNum = "8")
    private String faultage;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备注
     */
    private String remark;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 航磁异常解释依据
     */
    @Excel(name = "航磁异常解释依据", orderNum = "9")
    private String interpretationaccordingm;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 断裂代码
     */
    @Excel(name = "断裂代码", orderNum = "10")
    private String code;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 断裂名称
     */
    @Excel(name = "断裂名称", orderNum = "11")
    private String faultname;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 质检人
     */
    private String qualityinspectionUser;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String rowNum;
    private String errorMsg;
}