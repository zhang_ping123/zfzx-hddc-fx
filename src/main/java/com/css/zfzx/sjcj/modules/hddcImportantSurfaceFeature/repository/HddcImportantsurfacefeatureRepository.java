package com.css.zfzx.sjcj.modules.hddcImportantSurfaceFeature.repository;

import com.css.zfzx.sjcj.modules.hddcImportantSurfaceFeature.repository.entity.HddcImportantsurfacefeatureEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author lihelei
 * @date 2020-11-30
 */
public interface HddcImportantsurfacefeatureRepository extends JpaRepository<HddcImportantsurfacefeatureEntity, String> {
}
