package com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.repository.entity;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-12-01
 */
@Data
@Entity
@Table(name="hddc_wy_geophysvyline")
public class HddcWyGeophysvylineEntity implements Serializable {

    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 测线长度 [米]
     */
    @Column(name="length")
    private Double length;
    /**
     * 终点桩号
     */
    @Column(name="endmilestonenum")
    private Integer endmilestonenum;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 测线所属探测工程编号
     */
    @Column(name="projectid")
    private String projectId;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 显示码（制图用）
     */
    @Column(name="showcode")
    private Integer showcode;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;

    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 断点解释剖面图图像编号
     */
    @Column(name="faultptprofile_aiid")
    private String faultptprofileAiid;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 测线名称
     */
    @Column(name="name")
    private String name;
    /**
     * 测线来源与类型
     */
    @Column(name="svylinesource")
    private Integer svylinesource;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 综合解释剖面名称
     */
    @Column(name="resultname")
    private String resultname;
    /**
     * 项目ID
     */
    @Column(name="project_code")
    private String projectcode;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 综合解释剖面矢量图原始文件编号
     */
    @Column(name="resultmap_arwid")
    private String resultmapArwid;
    /**
     * 收集地球物理测线来源补充说明来源
     */
    @Column(name="collectedlinesource")
    private String collectedlinesource;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 起点桩号
     */
    @Column(name="startmilestonenum")
    private Integer startmilestonenum;
    /**
     * 探测方法
     */
    @Column(name="svymethod")
    private Integer svymethod;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 测线代码
     */
    @Column(name="fieldid")
    private String fieldid;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;

    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 断点解释剖面图原始文件编号
     */
    @Column(name="faultptprofile_arwid")
    private String faultptprofileArwid;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 综合解释剖面原始数据文件编号（sgy等）
     */
    @Column(name="expdata_arwid")
    private String expdataArwid;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 探测目的
     */
    @Column(name="purpose")
    private String purpose;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 测线编号
     */
    //@Id
    @Column(name="id")
    private String id;
    /**
     * 测线编号
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 综合解释剖面栅格图原始文件编号
     */
    @Column(name="resultmap_aiid")
    private String resultmapAiid;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 测线备注
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 测线来源与类型字典名称
     */
    @Column(name="svylinesource_name")
    private String svylinesourceName;
    /**
     * 探测方法字典名称
     */
    @Column(name="svymethod_name")
    private String svymethodName;
    /**
     * 经度
     */
    @Column(name="lon")
    private Double lon;
    /**
     * 维度
     */
    @Column(name="lat")
    private Double lat;
}

