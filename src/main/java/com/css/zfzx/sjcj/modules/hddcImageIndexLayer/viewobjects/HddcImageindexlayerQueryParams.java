package com.css.zfzx.sjcj.modules.hddcImageIndexLayer.viewobjects;

import lombok.Data;

/**
 * @author zhangping
 * @date 2020-11-30
 */
@Data
public class HddcImageindexlayerQueryParams {


    private String province;
    private String citycode;
    private String area;
    private String projectName;
    private String imagename;

}
