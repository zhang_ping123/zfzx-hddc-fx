package com.css.zfzx.sjcj.modules.hddcCrater.repository.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
@Data
@Entity
@Table(name="hddc_crater")
public class HddcCraterEntity implements Serializable {

    /**
     * 外坡度
     */
    @Column(name="outsideslopeangle")
    private Double outsideslopeangle;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 堆积物类型
     */
    @Column(name="deposittype")
    private String deposittype;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 拍摄者
     */
    @Column(name="photographer")
    private String photographer;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 岩石包体数量
     */
    @Column(name="rockinclusionnum")
    private Integer rockinclusionnum;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 锥体结构组成剖面图图片
     */
    @Column(name="conestructureprofile_aiid")
    private String conestructureprofileAiid;
    /**
     * 锥体名称
     */
    @Column(name="conename")
    private String conename;
    /**
     * 火口垣直径
     */
    @Column(name="craterwallsdiameter")
    private Double craterwallsdiameter;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 内坡度
     */
    @Column(name="insideslopeangle")
    private Double insideslopeangle;
    /**
     * 照片集镜向及拍摄者说明文档
     */
    @Column(name="photodesc_arwid")
    private String photodescArwid;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 塑性熔岩饼单体尺寸
     */
    @Column(name="lavadribletsize")
    private Double lavadribletsize;
    /**
     * 堆积物粒度
     */
    @Column(name="depositgranularity")
    private String depositgranularity;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 火口直径
     */
    @Column(name="craterdiameter")
    private Double craterdiameter;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 锥体高度[米]
     */
    @Column(name="coneheight")
    private Double coneheight;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 锥体底部直径
     */
    @Column(name="bottomdiameter")
    private Double bottomdiameter;
    /**
     * 溢出口方向
     */
    @Column(name="overfalldirection")
    private Integer overfalldirection;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 火口深度[米]
     */
    @Column(name="craterdepth")
    private Double craterdepth;
    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;
    /**
     * 照片原始文件编号
     */
    @Column(name="photo_arwid")
    private String photoArwid;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 岩石包体形状
     */
    @Column(name="rockinclusionshape")
    private String rockinclusionshape;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 锥体形态
     */
    @Column(name="conemorphology")
    private String conemorphology;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 素描图图像
     */
    @Column(name="sketch_aiid")
    private String sketchAiid;
    /**
     * 岩石包体类型
     */
    @Column(name="rockinclusiontype")
    private String rockinclusiontype;
    /**
     * DepositThickness
     */
    @Column(name="depositthickness")
    private Double depositthickness;
    /**
     * 素描图原始文件
     */
    @Column(name="sketch_arwid")
    private String sketchArwid;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 岩石包体产出状态
     */
    @Column(name="rockinclusionoutputstate")
    private String rockinclusionoutputstate;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 锥体类型
     */
    @Column(name="conetype")
    private Integer conetype;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;
    /**
     * 岩石包体粒度
     */
    @Column(name="rockinclusiongranularity")
    private String rockinclusiongranularity;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 备注
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 火山口编号
     */
    @Column(name="id")
    private String id;
    /**
     * 编号
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 照片文件编号
     */
    @Column(name="photo_aiid")
    private String photoAiid;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 锥体结构组成剖面图原始文件
     */
    @Column(name="conestructureprofile_arwid")
    private String conestructureprofileArwid;

}

