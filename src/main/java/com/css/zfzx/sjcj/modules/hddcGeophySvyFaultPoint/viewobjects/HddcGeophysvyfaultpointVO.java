package com.css.zfzx.sjcj.modules.hddcGeophySvyFaultPoint.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Data
public class HddcGeophysvyfaultpointVO implements Serializable {

    /**
     * 断点编号
     */
    private String id;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 目标断层编号
     */
    @Excel(name = "目标断层编号", orderNum = "4")
    private String targetfaultid;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 上断点界面年代
     */
    @Excel(name = "上断点界面年代", orderNum = "5")
    private Integer age0;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 上断点埋深 [米]
     */
    @Excel(name = "上断点埋深 [米]", orderNum = "6")
    private String m0depth;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 上断点断距值 [米]
     */
    @Excel(name = "上断点断距值 [米]", orderNum = "7")
    private String m0;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 断点纬度
     */
    @Excel(name = "断点纬度", orderNum = "8")
    private Double lat;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 可靠程度
     */
    @Excel(name = "可靠程度", orderNum = "9")
    private Integer reliability;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 所属测线编号
     */
    @Excel(name = "所属测线编号", orderNum = "10")
    private String svylineid;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 视倾向 [度]
     */
    @Excel(name = "视倾向 [度]", orderNum = "11")
    private Integer viewdirection;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 断点经度
     */
    @Excel(name = "断点经度", orderNum = "12")
    private Double lon;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "13")
    private String commentInfo;
    /**
     * 断层名称
     */
    @Excel(name = "断层名称", orderNum = "14")
    private String targetfaultname;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 断层性质
     */
    @Excel(name = "断层性质", orderNum = "15")
    private Integer faulttype;
    /**
     * 断点距起点距离 [米]
     */
    @Excel(name = "断点距起点距离 [米]", orderNum = "16")
    private Double measurelength;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 显示码
     */
    @Excel(name = "显示码", orderNum = "17")
    private Integer showcode;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 目标断层来源
     */
    @Excel(name = "目标断层来源", orderNum = "18")
    private String targetfaultsource;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 视倾角 [度]
     */
    @Excel(name = "视倾角 [度]", orderNum = "19")
    private Integer viewclination;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 断点描述
     */
    @Excel(name = "断点描述", orderNum = "20")
    private String faultdescription;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer viewdirectionName;
    private String targetfaultsourceName;
    private Integer viewclinationName;
    private Integer age0Name;
    private Integer reliabilityName;
    private Integer faulttypeName;
    private String rowNum;
    private String errorMsg;
}