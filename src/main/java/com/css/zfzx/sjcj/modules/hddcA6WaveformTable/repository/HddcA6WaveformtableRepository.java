package com.css.zfzx.sjcj.modules.hddcA6WaveformTable.repository;

import com.css.zfzx.sjcj.modules.hddcA6WaveformTable.repository.entity.HddcA6WaveformtableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-28
 */
public interface HddcA6WaveformtableRepository extends JpaRepository<HddcA6WaveformtableEntity, String> {
}
