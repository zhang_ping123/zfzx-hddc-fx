package com.css.zfzx.sjcj.modules.sys.login;

import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.bpm.platform.constants.PlatformConstants;
import com.css.bpm.platform.org.division.repository.entity.DivisionEntity;
import com.css.bpm.platform.org.role.repository.entity.RoleEntity;
import com.css.bpm.platform.org.user.repository.entity.UserEntity;
import com.css.bpm.platform.utils.PlatformSessionUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @Description: zfzx-hddc-fx
 * @Author: lhl
 * @CreateDate: 2021/7/27 15:19
 */
@RestController
@Slf4j
public class SysController {
    /**
     * 行政区划
     *
     * @return
     */
    @GetMapping("sys/getLoginUsers")
    public RestResponse getLoginUsers(HttpServletRequest req) {
        RestResponse restResponse = null;
        try {
            HttpSession session = req.getSession();
            String userId = (String)session.getAttribute(PlatformConstants.CUR_USER_ID_IN_SESSION);
            //String userId = PlatformSessionUtils.getUserId();
            UserEntity user = PlatformAPI.getOrgAPI().getUserAPI().getUser(userId);
            String userName = user.getUserName();
            restResponse = RestResponse.succeed(userName);
        } catch (Exception e) {
            String errormessage = "查询失败";
            log.error(errormessage, e);
            restResponse = RestResponse.fail(errormessage);
        }
        return restResponse;
    }
}
