package com.css.zfzx.sjcj.modules.hddcDrillFaultPoint.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcDrillFaultPoint.repository.HddcDrillfaultpointNativeRepository;
import com.css.zfzx.sjcj.modules.hddcDrillFaultPoint.repository.HddcDrillfaultpointRepository;
import com.css.zfzx.sjcj.modules.hddcDrillFaultPoint.repository.entity.HddcDrillfaultpointEntity;
import com.css.zfzx.sjcj.modules.hddcDrillFaultPoint.service.HddcDrillfaultpointService;
import com.css.zfzx.sjcj.modules.hddcDrillFaultPoint.viewobjects.HddcDrillfaultpointQueryParams;
import com.css.zfzx.sjcj.modules.hddcDrillFaultPoint.viewobjects.HddcDrillfaultpointVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Service
public class HddcDrillfaultpointServiceImpl implements HddcDrillfaultpointService {

	@Autowired
    private HddcDrillfaultpointRepository hddcDrillfaultpointRepository;
    @Autowired
    private HddcDrillfaultpointNativeRepository hddcDrillfaultpointNativeRepository;

    @Override
    public JSONObject queryHddcDrillfaultpoints(HddcDrillfaultpointQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcDrillfaultpointEntity> hddcDrillfaultpointPage = this.hddcDrillfaultpointNativeRepository.queryHddcDrillfaultpoints(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcDrillfaultpointPage);
        return jsonObject;
    }


    @Override
    public HddcDrillfaultpointEntity getHddcDrillfaultpoint(String id) {
        HddcDrillfaultpointEntity hddcDrillfaultpoint = this.hddcDrillfaultpointRepository.findById(id).orElse(null);
         return hddcDrillfaultpoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcDrillfaultpointEntity saveHddcDrillfaultpoint(HddcDrillfaultpointEntity hddcDrillfaultpoint) {
        String uuid = UUIDGenerator.getUUID();
        hddcDrillfaultpoint.setUuid(uuid);
        hddcDrillfaultpoint.setCreateUser(PlatformSessionUtils.getUserId());
        hddcDrillfaultpoint.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcDrillfaultpointRepository.save(hddcDrillfaultpoint);
        return hddcDrillfaultpoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcDrillfaultpointEntity updateHddcDrillfaultpoint(HddcDrillfaultpointEntity hddcDrillfaultpoint) {
        HddcDrillfaultpointEntity entity = hddcDrillfaultpointRepository.findById(hddcDrillfaultpoint.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcDrillfaultpoint);
        hddcDrillfaultpoint.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcDrillfaultpoint.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcDrillfaultpointRepository.save(hddcDrillfaultpoint);
        return hddcDrillfaultpoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcDrillfaultpoints(List<String> ids) {
        List<HddcDrillfaultpointEntity> hddcDrillfaultpointList = this.hddcDrillfaultpointRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcDrillfaultpointList) && hddcDrillfaultpointList.size() > 0) {
            for(HddcDrillfaultpointEntity hddcDrillfaultpoint : hddcDrillfaultpointList) {
                this.hddcDrillfaultpointRepository.delete(hddcDrillfaultpoint);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcDrillfaultpointQueryParams queryParams, HttpServletResponse response) {
        List<HddcDrillfaultpointEntity> yhDisasterEntities = hddcDrillfaultpointNativeRepository.exportYhDisasters(queryParams);
        List<HddcDrillfaultpointVO> list=new ArrayList<>();
        for (HddcDrillfaultpointEntity entity:yhDisasterEntities) {
            HddcDrillfaultpointVO yhDisasterVO=new HddcDrillfaultpointVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "跨断层钻探断点-点", "跨断层钻探断点-点", HddcDrillfaultpointVO.class, "跨断层钻探断点-点.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcDrillfaultpointVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcDrillfaultpointVO.class, params);
            List<HddcDrillfaultpointVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcDrillfaultpointVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcDrillfaultpointVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcDrillfaultpointVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcDrillfaultpointEntity yhDisasterEntity = new HddcDrillfaultpointEntity();
            HddcDrillfaultpointVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcDrillfaultpoint(yhDisasterEntity);
        }
    }

}
