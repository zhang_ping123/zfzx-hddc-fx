package com.css.zfzx.sjcj.modules.hddcImageIndexLayer.repository;

import com.css.zfzx.sjcj.modules.hddcImageIndexLayer.repository.entity.HddcImageindexlayerEntity;
import com.css.zfzx.sjcj.modules.hddcImageIndexLayer.viewobjects.HddcImageindexlayerQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangping
 * @date 2020-11-30
 */
public interface HddcImageindexlayerNativeRepository {

    Page<HddcImageindexlayerEntity> queryHddcImageindexlayers(HddcImageindexlayerQueryParams queryParams, int curPage, int pageSize);

    List<HddcImageindexlayerEntity> exportYhDisasters(HddcImageindexlayerQueryParams queryParams);
}
