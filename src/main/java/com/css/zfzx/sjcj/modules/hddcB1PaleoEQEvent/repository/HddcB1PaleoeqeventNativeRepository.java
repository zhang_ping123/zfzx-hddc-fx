package com.css.zfzx.sjcj.modules.hddcB1PaleoEQEvent.repository;

import com.css.zfzx.sjcj.modules.hddcB1PaleoEQEvent.repository.entity.HddcB1PaleoeqeventEntity;
import com.css.zfzx.sjcj.modules.hddcB1PaleoEQEvent.viewobjects.HddcB1PaleoeqeventQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcB1PaleoeqeventNativeRepository {

    Page<HddcB1PaleoeqeventEntity> queryHddcB1Paleoeqevents(HddcB1PaleoeqeventQueryParams queryParams, int curPage, int pageSize);

    List<HddcB1PaleoeqeventEntity> exportYhDisasters(HddcB1PaleoeqeventQueryParams queryParams);
}
