package com.css.zfzx.sjcj.modules.hddcB7_VolcanicSampleResultTable.repository;

import com.css.zfzx.sjcj.modules.hddcB7_VolcanicSampleResultTable.repository.entity.HddcB7VolcanicsampleresulttableEntity;
import com.css.zfzx.sjcj.modules.hddcB7_VolcanicSampleResultTable.viewobjects.HddcB7VolcanicsampleresulttableQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-26
 */
public interface HddcB7VolcanicsampleresulttableNativeRepository {

    Page<HddcB7VolcanicsampleresulttableEntity> queryHddcB7Volcanicsampleresulttables(HddcB7VolcanicsampleresulttableQueryParams queryParams, int curPage, int pageSize);

    List<HddcB7VolcanicsampleresulttableEntity> exportYhDisasters(HddcB7VolcanicsampleresulttableQueryParams queryParams);
}
