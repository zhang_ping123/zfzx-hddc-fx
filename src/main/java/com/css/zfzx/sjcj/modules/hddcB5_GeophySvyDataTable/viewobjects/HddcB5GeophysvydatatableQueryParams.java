package com.css.zfzx.sjcj.modules.hddcB5_GeophySvyDataTable.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Data
public class HddcB5GeophysvydatatableQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

}
