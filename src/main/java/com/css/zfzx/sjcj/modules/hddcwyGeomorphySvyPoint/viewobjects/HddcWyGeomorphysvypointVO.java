package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author lihelei
 * @date 2020-12-01
 */
@Data
public class HddcWyGeomorphysvypointVO implements Serializable {

    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 任务名称
     */
    @Excel(name="任务名称",orderNum = "9")
    private String taskName;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备注-观测点说明
     */
    @Excel(name="备注-观测点说明",orderNum = "12")
    private String commentInfo;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备注
     */
    private String remark;
    /**
     * 项目名称
     */
    @Excel(name="项目名称",orderNum = "8")
    private String projectName;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 乡
     */
    @Excel(name="详细地址",orderNum = "5")
    private String town;
    /**
     * 所属微地貌面编号
     */
    @Excel(name="所属微地貌面编号",orderNum = "10")
    private String regionid;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 测点编号
     */
    @Excel(name="测点编号",orderNum = "1")
    private String id;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 野外编号
     */
    @Excel(name = "野外编号",orderNum = "15")
    private String fieldid;
    /**
     * 市
     */
    @Excel(name="市",orderNum = "3")
    private String city;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 经度
     */
    @Excel(name="经度",orderNum = "6")
    private Double lon;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 高程 [米]
     */
    @Excel(name="高程 [米]",orderNum = "14")
    private Double elevation;
    /**
     * 省
     */
    @Excel(name="省",orderNum = "2")
    private String province;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 区（县）
     */
    @Excel(name="区（县）",orderNum = "4")
    private String area;
    /**
     * 纬度
     */
    @Excel(name="纬度",orderNum = "7")
    private Double lat;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 微地貌测量工程编号
     */
    @Excel(name="微地貌测量工程编号",orderNum = "11")
    private String geomorphysvyprjid;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 所属微地貌线编号
     */
    @Excel(name="所属微地貌线编号",orderNum = "13")
    private String svylineid;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;

    private String provinceName;
    private String cityName;
    private String areaName;

    private String rowNum;
    private String errorMsg;
}