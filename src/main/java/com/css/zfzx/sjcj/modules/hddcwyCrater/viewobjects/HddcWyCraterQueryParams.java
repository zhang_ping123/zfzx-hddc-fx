package com.css.zfzx.sjcj.modules.hddcwyCrater.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-12-02
 */
@Data
public class HddcWyCraterQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String conename;

    private String taskName;
    private String startTime;
    private String endTime;
}
