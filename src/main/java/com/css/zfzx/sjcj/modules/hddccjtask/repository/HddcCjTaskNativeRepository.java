package com.css.zfzx.sjcj.modules.hddccjtask.repository;

import com.css.zfzx.sjcj.modules.hddccjtask.repository.entity.HddcCjTaskEntity;
import com.css.zfzx.sjcj.modules.hddccjtask.viewobjects.HddcCjTaskQueryParams;
import org.springframework.data.domain.Page;

/**
 * @author zhangping
 * @date 2020-11-26
 */
public interface HddcCjTaskNativeRepository {

    Page<HddcCjTaskEntity> queryHddcCjTasks(HddcCjTaskQueryParams queryParams, int curPage, int pageSize);

    Page<HddcCjTaskEntity> queryHddcCjTasksByProjectId(String projectId, int curPage, int pageSize);
}
