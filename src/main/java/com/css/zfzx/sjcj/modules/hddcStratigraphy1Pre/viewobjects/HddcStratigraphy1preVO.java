package com.css.zfzx.sjcj.modules.hddcStratigraphy1Pre.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Data
public class HddcStratigraphy1preVO implements Serializable {

    /**
     * 乡
     */
    private String town;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 地层年代
     */
    @Excel(name = "地层年代", orderNum = "4")
    private Integer symbol;
    /**
     * 合并地层名称
     */
    @Excel(name = "合并地层名称", orderNum = "5")
    private String unionname;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 备注
     */
    private String remark;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 地层名称
     */
    @Excel(name = "地层名称", orderNum = "6")
    private String stratigraphyname;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 地层符号基础位
     */
    @Excel(name = "地层符号基础位", orderNum = "7")
    private String symbolmain;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "8")
    private String commentInfo;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 地层编号
     */
    private String id;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 符号下标
     */
    @Excel(name = "符号下标", orderNum = "9")
    private String symbollow;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 地层厚度 [米]
     */
    @Excel(name = "地层厚度 [米]", orderNum = "10")
    private String thickness;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 沉积相（符号）
     */
    @Excel(name = "沉积相（符号）", orderNum = "11")
    private String sedimentaryfacies;
    /**
     * 符号上标
     */
    @Excel(name = "符号上标", orderNum = "12")
    private String symbolup;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 地层描述
     */
    @Excel(name = "地层描述", orderNum = "13")
    private String description;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 分区标识
     */
    private Integer partionFlag;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String symbolName;
    private String rowNum;
    private String errorMsg;
}