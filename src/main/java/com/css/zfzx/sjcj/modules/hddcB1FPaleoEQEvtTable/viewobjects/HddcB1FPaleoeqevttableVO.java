package com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtTable.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
public class HddcB1FPaleoeqevttableVO implements Serializable {

    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 编号
     */
    private String id;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 最大同震走向水平位移 [米]
     */
    @Excel(name = "最大同震走向水平位移 [米]")
    private Double maxhdisplaceest;
    /**
     * 最大同震垂直位移 [米]
     */
    @Excel(name = "最大同震垂直位移 [米]")
    private Double maxvdisplaceest;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 市
     */
    @Excel(name = "市",orderNum = "2")
    private String city;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 古地震距今年龄区间(区间值[单位])
     */
    @Excel(name = "古地震距今年龄区间(区间值[单位])")
    private String paleoseismageinterval;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 断裂断层/段名称
     */
    @Excel(name = "断裂断层/段名称")
    private String faultname;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 古地震距今年龄
     */
    @Excel(name = "古地震距今年龄")
    private Integer paleoseismage;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 重复间隔
     */
    @Excel(name = "重复间隔")
    private Double recurrenceinterval;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 古地震距今年龄误差
     */
    @Excel(name = "古地震距今年龄误差")
    private Integer ageer;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）",orderNum = "3")
    private String area;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 乡
     */
    private String town;
    /**
     * 备注
     */
    private String commentInfo;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 省
     */
    @Excel(name = "省",orderNum = "1")
    private String province;
    /**
     * 最大同震水平//张缩位移
     */
    @Excel(name = "最大同震水平//张缩位移")
    private Double maxtdisplaceest;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备注
     */
    @Excel(name = "备注")
    private String remark;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段5
     */
    private String extends5;

    private String provinceName;
    private String cityName;
    private String areaName;

    private String rowNum;
    private String errorMsg;
}