package com.css.zfzx.sjcj.modules.hddcLava.repository;

import com.css.zfzx.sjcj.modules.hddcLava.repository.entity.HddcLavaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
public interface HddcLavaRepository extends JpaRepository<HddcLavaEntity, String> {
}
