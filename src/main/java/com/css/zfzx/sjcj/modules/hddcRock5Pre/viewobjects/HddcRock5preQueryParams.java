package com.css.zfzx.sjcj.modules.hddcRock5Pre.viewobjects;

import lombok.Data;

/**
 * @author lhl
 * @date 2020-11-27
 */
@Data
public class HddcRock5preQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String rockname;

}
