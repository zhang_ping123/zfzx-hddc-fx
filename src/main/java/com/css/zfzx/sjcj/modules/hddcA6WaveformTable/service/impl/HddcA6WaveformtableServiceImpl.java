package com.css.zfzx.sjcj.modules.hddcA6WaveformTable.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcA6WaveformTable.repository.HddcA6WaveformtableNativeRepository;
import com.css.zfzx.sjcj.modules.hddcA6WaveformTable.repository.HddcA6WaveformtableRepository;
import com.css.zfzx.sjcj.modules.hddcA6WaveformTable.repository.entity.HddcA6WaveformtableEntity;
import com.css.zfzx.sjcj.modules.hddcA6WaveformTable.service.HddcA6WaveformtableService;
import com.css.zfzx.sjcj.modules.hddcA6WaveformTable.viewobjects.HddcA6WaveformtableQueryParams;
import com.css.zfzx.sjcj.modules.hddcA6WaveformTable.viewobjects.HddcA6WaveformtableVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Service
public class HddcA6WaveformtableServiceImpl implements HddcA6WaveformtableService {

	@Autowired
    private HddcA6WaveformtableRepository hddcA6WaveformtableRepository;
    @Autowired
    private HddcA6WaveformtableNativeRepository hddcA6WaveformtableNativeRepository;

    @Override
    public JSONObject queryHddcA6Waveformtables(HddcA6WaveformtableQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcA6WaveformtableEntity> hddcA6WaveformtablePage = this.hddcA6WaveformtableNativeRepository.queryHddcA6Waveformtables(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcA6WaveformtablePage);
        return jsonObject;
    }


    @Override
    public HddcA6WaveformtableEntity getHddcA6Waveformtable(String id) {
        HddcA6WaveformtableEntity hddcA6Waveformtable = this.hddcA6WaveformtableRepository.findById(id).orElse(null);
         return hddcA6Waveformtable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcA6WaveformtableEntity saveHddcA6Waveformtable(HddcA6WaveformtableEntity hddcA6Waveformtable) {
        String uuid = UUIDGenerator.getUUID();
        hddcA6Waveformtable.setUuid(uuid);
        hddcA6Waveformtable.setCreateUser(PlatformSessionUtils.getUserId());
        hddcA6Waveformtable.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcA6WaveformtableRepository.save(hddcA6Waveformtable);
        return hddcA6Waveformtable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcA6WaveformtableEntity updateHddcA6Waveformtable(HddcA6WaveformtableEntity hddcA6Waveformtable) {
        HddcA6WaveformtableEntity entity = hddcA6WaveformtableRepository.findById(hddcA6Waveformtable.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcA6Waveformtable);
        hddcA6Waveformtable.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcA6Waveformtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcA6WaveformtableRepository.save(hddcA6Waveformtable);
        return hddcA6Waveformtable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcA6Waveformtables(List<String> ids) {
        List<HddcA6WaveformtableEntity> hddcA6WaveformtableList = this.hddcA6WaveformtableRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcA6WaveformtableList) && hddcA6WaveformtableList.size() > 0) {
            for(HddcA6WaveformtableEntity hddcA6Waveformtable : hddcA6WaveformtableList) {
                this.hddcA6WaveformtableRepository.delete(hddcA6Waveformtable);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcA6WaveformtableQueryParams queryParams, HttpServletResponse response) {
        List<HddcA6WaveformtableEntity> yhDisasterEntities = hddcA6WaveformtableNativeRepository.exportYhDisasters(queryParams);
        List<HddcA6WaveformtableVO> list=new ArrayList<>();
        for (HddcA6WaveformtableEntity entity:yhDisasterEntities) {
            HddcA6WaveformtableVO yhDisasterVO=new HddcA6WaveformtableVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"小震波形数据记录表","小震波形数据记录表",HddcA6WaveformtableVO.class,"小震波形数据记录表.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcA6WaveformtableVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcA6WaveformtableVO.class, params);
            List<HddcA6WaveformtableVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcA6WaveformtableVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcA6WaveformtableVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcA6WaveformtableVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcA6WaveformtableEntity yhDisasterEntity = new HddcA6WaveformtableEntity();
            HddcA6WaveformtableVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcA6Waveformtable(yhDisasterEntity);
        }
    }

}
