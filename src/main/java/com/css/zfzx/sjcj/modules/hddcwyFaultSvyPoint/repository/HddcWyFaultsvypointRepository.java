package com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcWyFaultsvypointRepository extends JpaRepository<HddcWyFaultsvypointEntity, String> {

    List<HddcWyFaultsvypointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId,String taskId,String projectId,String isValid);

    List<HddcWyFaultsvypointEntity> findAllByCreateUserAndIsValid(String userId,String isValid);

    @Query(nativeQuery = true, value = "select * from hddc_wy_faultsvypoint where is_valid!=0 project_name in :projectIds")
    List<HddcWyFaultsvypointEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_faultsvypoint where task_name in :projectIds")
    List<HddcWyFaultsvypointEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);

}
