package com.css.zfzx.sjcj.modules.hddcB1PaleoEQEvent.repository;

import com.css.zfzx.sjcj.modules.hddcB1PaleoEQEvent.repository.entity.HddcB1PaleoeqeventEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcB1PaleoeqeventRepository extends JpaRepository<HddcB1PaleoeqeventEntity, String> {
}
