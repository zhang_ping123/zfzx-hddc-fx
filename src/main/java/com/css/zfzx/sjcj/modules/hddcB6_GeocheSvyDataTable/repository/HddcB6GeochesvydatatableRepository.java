package com.css.zfzx.sjcj.modules.hddcB6_GeocheSvyDataTable.repository;

import com.css.zfzx.sjcj.modules.hddcB6_GeocheSvyDataTable.repository.entity.HddcB6GeochesvydatatableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
public interface HddcB6GeochesvydatatableRepository extends JpaRepository<HddcB6GeochesvydatatableEntity, String> {
}
