package com.css.zfzx.sjcj.modules.hddcDResultReportTable.repository;

import com.css.zfzx.sjcj.modules.hddcDResultReportTable.repository.entity.HddcDResultreporttableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-12-19
 */
public interface HddcDResultreporttableRepository extends JpaRepository<HddcDResultreporttableEntity, String> {
}
