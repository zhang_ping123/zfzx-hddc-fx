package com.css.zfzx.sjcj.modules.hddcB4_SampleDataTable.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltVO;
import com.css.zfzx.sjcj.modules.hddcB4_SampleDataTable.repository.HddcB4SampledatatableNativeRepository;
import com.css.zfzx.sjcj.modules.hddcB4_SampleDataTable.repository.HddcB4SampledatatableRepository;
import com.css.zfzx.sjcj.modules.hddcB4_SampleDataTable.repository.entity.HddcB4SampledatatableEntity;
import com.css.zfzx.sjcj.modules.hddcB4_SampleDataTable.service.HddcB4SampledatatableService;
import com.css.zfzx.sjcj.modules.hddcB4_SampleDataTable.viewobjects.HddcB4SampledatatableQueryParams;
import com.css.zfzx.sjcj.modules.hddcB4_SampleDataTable.viewobjects.HddcB4SampledatatableVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Service
public class HddcB4SampledatatableServiceImpl implements HddcB4SampledatatableService {

	@Autowired
    private HddcB4SampledatatableRepository hddcB4SampledatatableRepository;
    @Autowired
    private HddcB4SampledatatableNativeRepository hddcB4SampledatatableNativeRepository;

    @Override
    public JSONObject queryHddcB4Sampledatatables(HddcB4SampledatatableQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcB4SampledatatableEntity> hddcB4SampledatatablePage = this.hddcB4SampledatatableNativeRepository.queryHddcB4Sampledatatables(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcB4SampledatatablePage);
        return jsonObject;
    }


    @Override
    public HddcB4SampledatatableEntity getHddcB4Sampledatatable(String id) {
        HddcB4SampledatatableEntity hddcB4Sampledatatable = this.hddcB4SampledatatableRepository.findById(id).orElse(null);
         return hddcB4Sampledatatable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB4SampledatatableEntity saveHddcB4Sampledatatable(HddcB4SampledatatableEntity hddcB4Sampledatatable) {
        String uuid = UUIDGenerator.getUUID();
        hddcB4Sampledatatable.setUuid(uuid);
        hddcB4Sampledatatable.setCreateUser(PlatformSessionUtils.getUserId());
        hddcB4Sampledatatable.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB4SampledatatableRepository.save(hddcB4Sampledatatable);
        return hddcB4Sampledatatable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB4SampledatatableEntity updateHddcB4Sampledatatable(HddcB4SampledatatableEntity hddcB4Sampledatatable) {
        HddcB4SampledatatableEntity entity = hddcB4SampledatatableRepository.findById(hddcB4Sampledatatable.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcB4Sampledatatable);
        hddcB4Sampledatatable.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcB4Sampledatatable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB4SampledatatableRepository.save(hddcB4Sampledatatable);
        return hddcB4Sampledatatable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcB4Sampledatatables(List<String> ids) {
        List<HddcB4SampledatatableEntity> hddcB4SampledatatableList = this.hddcB4SampledatatableRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcB4SampledatatableList) && hddcB4SampledatatableList.size() > 0) {
            for(HddcB4SampledatatableEntity hddcB4Sampledatatable : hddcB4SampledatatableList) {
                this.hddcB4SampledatatableRepository.delete(hddcB4Sampledatatable);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcB4SampledatatableQueryParams queryParams, HttpServletResponse response) {
        List<HddcB4SampledatatableEntity> yhDisasterEntities = hddcB4SampledatatableNativeRepository.exportYhDisasters(queryParams);
        List<HddcB4SampledatatableVO> list=new ArrayList<>();
        for (HddcB4SampledatatableEntity entity:yhDisasterEntities) {
            HddcB4SampledatatableVO yhDisasterVO=new HddcB4SampledatatableVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"样品数据表","样品数据表",HddcB4SampledatatableVO.class,"样品数据表.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcB4SampledatatableVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcB4SampledatatableVO.class, params);
            List<HddcB4SampledatatableVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcB4SampledatatableVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcB4SampledatatableVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcB4SampledatatableVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcB4SampledatatableEntity yhDisasterEntity = new HddcB4SampledatatableEntity();
            HddcB4SampledatatableVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcB4Sampledatatable(yhDisasterEntity);
        }
    }

}
