package com.css.zfzx.sjcj.modules.hddcA1LiteratureDocumentTable.repository;

import com.css.zfzx.sjcj.modules.hddcA1LiteratureDocumentTable.repository.entity.HddcA1LiteraturedocumenttableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcA1LiteraturedocumenttableRepository extends JpaRepository<HddcA1LiteraturedocumenttableEntity, String> {
}
