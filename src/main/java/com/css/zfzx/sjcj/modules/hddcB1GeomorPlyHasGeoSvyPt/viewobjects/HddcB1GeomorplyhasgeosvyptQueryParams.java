package com.css.zfzx.sjcj.modules.hddcB1GeomorPlyHasGeoSvyPt.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Data
public class HddcB1GeomorplyhasgeosvyptQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

}
