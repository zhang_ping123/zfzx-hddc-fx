package com.css.zfzx.sjcj.modules.hddcCollectedDrillHole.repository.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Data
@Entity
@Table(name="hddc_collecteddrillhole")
public class HddcCollecteddrillholeEntity implements Serializable {

    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 是否开展地球物理测井
     */
    @Column(name="isgeophywell")
    private Integer isgeophywell;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 下更新统厚度 [米]
     */
    @Column(name="lowpleithickness")
    private Double lowpleithickness;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 钻孔柱状图原始档案编号
     */
    @Column(name="columnchart_arwid")
    private String columnchartArwid;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 孔深
     */
    @Column(name="depth")
    private Double depth;
    /**
     * 钻孔柱状图图像文件编号
     */
    @Column(name="columnchart_aiid")
    private String columnchartAiid;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 孔位经度
     */
    @Column(name="lon")
    private Double lon;
    /**
     * 全新统厚度 [米]
     */
    @Column(name="holocenethickness")
    private Double holocenethickness;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 中更新统厚度 [米]
     */
    @Column(name="midpleithickness")
    private Double midpleithickness;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 孔口标高 [米]
     */
    @Column(name="elevation")
    private Double elevation;
    /**
     * 岩芯总长 [米]
     */
    @Column(name="coretotalthickness")
    private Double coretotalthickness;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 前第四纪厚度 [米]
     */
    @Column(name="prepleithickness")
    private Double prepleithickness;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 孔位纬度
     */
    @Column(name="lat")
    private Double lat;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 钻孔编号
     */
    @Column(name="id")
    private String id;
    /**
     * 编号
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 上更新统厚度 [米]
     */
    @Column(name="uppleithickness")
    private Double uppleithickness;
    /**
     * 钻探日期
     */
    @Column(name="drilldate")
    private String drilldate;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 钻探地点
     */
    @Column(name="locationname")
    private String locationname;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 钻孔描述
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;

}

