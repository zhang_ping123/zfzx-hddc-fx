package com.css.zfzx.sjcj.modules.yhschool.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.yhschool.repository.entity.YhSchoolEntity;
import com.css.zfzx.sjcj.modules.yhschool.viewobjects.YhSchoolQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import java.util.List;

/**
 * @author yyd
 * @date 2020-11-03
 */

public interface YhSchoolService {

    public JSONObject queryYhSchools(YhSchoolQueryParams queryParams, int curPage, int pageSize);

    public YhSchoolEntity getYhSchool(String id);

    public YhSchoolEntity saveYhSchool(YhSchoolEntity yhSchool);

    public YhSchoolEntity updateYhSchool(YhSchoolEntity yhSchool);

    public void deleteYhSchools(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

}
