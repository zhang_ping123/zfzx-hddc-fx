package com.css.zfzx.sjcj.modules.hddcGeomorphyline.repository;

import com.css.zfzx.sjcj.modules.hddcGeomorphyline.repository.entity.HddcGeomorphylineEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphyline.viewobjects.HddcGeomorphylineQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-12-09
 */
public interface HddcGeomorphylineNativeRepository {

    Page<HddcGeomorphylineEntity> queryHddcGeomorphylines(HddcGeomorphylineQueryParams queryParams, int curPage, int pageSize);

    List<HddcGeomorphylineEntity> exportYhDisasters(HddcGeomorphylineQueryParams queryParams);
}
