package com.css.zfzx.sjcj.modules.yhhospital.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.yhhospital.repository.entity.YhHospitalEntity;
import com.css.zfzx.sjcj.modules.yhhospital.viewobjects.YhHospitalQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import java.util.List;

/**
 * @author yyd
 * @date 2020-11-03
 */

public interface YhHospitalService {

    public JSONObject queryYhHospitals(YhHospitalQueryParams queryParams, int curPage, int pageSize);

    public YhHospitalEntity getYhHospital(String id);

    public YhHospitalEntity saveYhHospital(YhHospitalEntity yhHospital);

    public YhHospitalEntity updateYhHospital(YhHospitalEntity yhHospital);

    public void deleteYhHospitals(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

}
