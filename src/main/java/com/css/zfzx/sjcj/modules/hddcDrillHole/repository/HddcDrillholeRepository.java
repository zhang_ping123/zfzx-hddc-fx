package com.css.zfzx.sjcj.modules.hddcDrillHole.repository;

import com.css.zfzx.sjcj.modules.hddcDrillHole.repository.entity.HddcDrillholeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcDrillholeRepository extends JpaRepository<HddcDrillholeEntity, String> {

    @Query(nativeQuery = true, value = "select * from hddc_drillhole where is_valid !=0 and project_name in :projectIds")
    List<HddcDrillholeEntity> queryHddcDrillholesByProjectId(List<String> projectIds);

    @Query(nativeQuery = true, value = "select * from hddc_drillhole where is_valid !=0 and task_name in :taskIds")
    List<HddcDrillholeEntity> queryHddcDrillholesByTaskId(List<String> taskIds);
}
