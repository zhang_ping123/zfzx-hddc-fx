package com.css.zfzx.sjcj.modules.hddcVerticalDeformation.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcVerticalDeformation.repository.entity.HddcVerticaldeformationEntity;
import com.css.zfzx.sjcj.modules.hddcVerticalDeformation.viewobjects.HddcVerticaldeformationQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-26
 */

public interface HddcVerticaldeformationService {

    public JSONObject queryHddcVerticaldeformations(HddcVerticaldeformationQueryParams queryParams, int curPage, int pageSize);

    public HddcVerticaldeformationEntity getHddcVerticaldeformation(String id);

    public HddcVerticaldeformationEntity saveHddcVerticaldeformation(HddcVerticaldeformationEntity hddcVerticaldeformation);

    public HddcVerticaldeformationEntity updateHddcVerticaldeformation(HddcVerticaldeformationEntity hddcVerticaldeformation);

    public void deleteHddcVerticaldeformations(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcVerticaldeformationQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
