package com.css.zfzx.sjcj.modules.hddcVolcanicSvyPoint.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
@Data
public class HddcVolcanicsvypointVO implements Serializable {

    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 观测点描述
     */
    @Excel(name = "观测点描述", orderNum = "4")
    private String spcommentInfo;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 获得测试结果样品数
     */
    @Excel(name = "获得测试结果样品数", orderNum = "5")
    private Integer datingsamplecount;
    /**
     * 观测点地名
     */
    @Excel(name = "观测点地名", orderNum = "6")
    private String locationname;
    /**
     * 照片集镜向及拍摄者说明文档
     */
    @Excel(name = "照片集镜向及拍摄者说明文档", orderNum = "7")
    private String photodescArwid;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 经度
     */
    @Excel(name = "经度", orderNum = "8")
    private Double lon;
    /**
     * 典型照片原始文件编号
     */
    @Excel(name = "典型照片原始文件编号", orderNum = "9")
    private String photoArwid;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 是否火山口观测点
     */
    @Excel(name = "是否火山口观测点", orderNum = "10")
    private Integer iscrater;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 典型照片文件编号
     */
    @Excel(name = "典型照片文件编号", orderNum = "11")
    private String photoAiid;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 拍摄者
     */
    @Excel(name = "拍摄者", orderNum = "12")
    private String photographer;
    /**
     * 是否熔岩流观测点
     */
    @Excel(name = "是否熔岩流观测点", orderNum = "13")
    private Integer islava;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 纬度
     */
    @Excel(name = "纬度", orderNum = "14")
    private Double lat;
    /**
     * 乡
     */
    private String town;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 是否在图中显示
     */
    @Excel(name = "是否在图中显示", orderNum = "15")
    private Integer isinmap;
    /**
     * 送样总数
     */
    @Excel(name = "送样总数", orderNum = "16")
    private Integer samplecount;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 观测日期
     */
    @Excel(name = "观测日期", orderNum = "17")
    private String svydate;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 海拔高度 [米]
     */
    @Excel(name = "海拔高度 [米]", orderNum = "18")
    private Integer elevation;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 观测方法
     */
    @Excel(name = "观测方法", orderNum = "19")
    private String svymethods;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 观测目的
     */
    @Excel(name = "观测目的", orderNum = "20")
    private String purpose;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 是否火山锥观测点
     */
    @Excel(name = "是否火山锥观测点", orderNum = "21")
    private Integer isvocaniccone;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 典型剖面图文件原始编号
     */
    @Excel(name = "典型剖面图文件原始编号", orderNum = "22")
    private String typicalprofileArwid;
    /**
     * 典型剖面图文件名图表编号
     */
    @Excel(name = "典型剖面图文件名图表编号", orderNum = "23")
    private String typicalprofileAcid;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 采集样品总数
     */
    @Excel(name = "采集样品总数", orderNum = "24")
    private Integer collectedsamplecount;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 观测点编号
     */
    private String id;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 观测点野外编号
     */
    @Excel(name = "观测点野外编号", orderNum = "25")
    private String fieldid;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 工程编号
     */
    private String projectid;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备注
     */
    private String commentInfo;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 删除标识
     */
    private String isValid;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer islavaName;
    private Integer iscraterName;
    private Integer isinmapName;
    private Integer isvocanicconeName;
    private String rowNum;
    private String errorMsg;
}