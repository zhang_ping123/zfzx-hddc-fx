package com.css.zfzx.sjcj.modules.hddcStratigraphy1LinePre.repository;

import com.css.zfzx.sjcj.modules.hddcStratigraphy1LinePre.repository.entity.HddcStratigraphy1linepreEntity;
import com.css.zfzx.sjcj.modules.hddcStratigraphy1LinePre.viewobjects.HddcStratigraphy1linepreQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcStratigraphy1linepreNativeRepository {

    Page<HddcStratigraphy1linepreEntity> queryHddcStratigraphy1linepres(HddcStratigraphy1linepreQueryParams queryParams, int curPage, int pageSize);

    List<HddcStratigraphy1linepreEntity> exportYhDisasters(HddcStratigraphy1linepreQueryParams queryParams);
}
