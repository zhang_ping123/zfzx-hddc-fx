package com.css.zfzx.sjcj.modules.hddcGeophySvyFaultPoint.repository;

import com.css.zfzx.sjcj.modules.hddcGeophySvyFaultPoint.repository.entity.HddcGeophysvyfaultpointEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcGeophysvyfaultpointRepository extends JpaRepository<HddcGeophysvyfaultpointEntity, String> {
}
