package com.css.zfzx.sjcj.modules.hddcwyVolcanicSvyPoint.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyVolcanicSvyPoint.repository.entity.HddcWyVolcanicsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyVolcanicSvyPoint.viewobjects.HddcWyVolcanicsvypointQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-02
 */
public interface HddcWyVolcanicsvypointNativeRepository {

    Page<HddcWyVolcanicsvypointEntity> queryHddcWyVolcanicsvypoints(HddcWyVolcanicsvypointQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyVolcanicsvypoint(HddcAppZztCountVo queryParams);

    List<HddcWyVolcanicsvypointEntity> exportVolcanicPoint (HddcAppZztCountVo queryParams);
}
