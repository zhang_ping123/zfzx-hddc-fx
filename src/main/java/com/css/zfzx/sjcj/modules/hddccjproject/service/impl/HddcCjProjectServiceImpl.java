package com.css.zfzx.sjcj.modules.hddccjproject.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddccjproject.repository.HddcCjProjectNativeRepository;
import com.css.zfzx.sjcj.modules.hddccjproject.repository.HddcCjProjectRepository;
import com.css.zfzx.sjcj.modules.hddccjproject.repository.entity.HddcCjProjectEntity;
import com.css.zfzx.sjcj.modules.hddccjproject.service.HddcCjProjectService;
import com.css.zfzx.sjcj.modules.hddccjproject.viewobjects.HddcCjProjectQueryParams;
import com.css.zfzx.sjcj.modules.hddccjproject.viewobjects.HddcCjProjectVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.org.user.repository.entity.UserEntity;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author zhangping
 * @date 2020-11-26
 */
@Service
public class HddcCjProjectServiceImpl implements HddcCjProjectService {

	@Autowired
    private HddcCjProjectRepository hddcCjProjectRepository;
    @Autowired
    private HddcCjProjectNativeRepository hddcCjProjectNativeRepository;

    @Override
    public JSONObject queryHddcCjProjects(HddcCjProjectQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcCjProjectEntity> hddcCjProjectPage = this.hddcCjProjectNativeRepository.queryHddcCjProjects(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcCjProjectPage);
        return jsonObject;
    }


    @Override
    public HddcCjProjectVO getHddcCjProject(String id) {
        HddcCjProjectEntity hddcCjProject = this.hddcCjProjectRepository.findById(id).orElse(null);
        HddcCjProjectVO hddcCjProjectVO = new HddcCjProjectVO();
        if(hddcCjProject!=null){
            BeanUtils.copyProperties(hddcCjProject, hddcCjProjectVO);
            UserEntity userOfPersonIds = PlatformAPI.getOrgAPI().getUserAPI().getUser(hddcCjProject.getPersonIds());
            String valuePersonIds = "";
            if(PlatformObjectUtils.isNotEmpty(userOfPersonIds)) {
                valuePersonIds = userOfPersonIds.getUserName();
            }
            hddcCjProjectVO.setPersonIdsName(valuePersonIds);
        }
        return hddcCjProjectVO;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcCjProjectEntity saveHddcCjProject(HddcCjProjectEntity hddcCjProject) {
        String uuid = UUIDGenerator.getUUID();
        hddcCjProject.setId(uuid);
        hddcCjProject.setCreateUser(PlatformSessionUtils.getUserId());
        hddcCjProject.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcCjProjectRepository.save(hddcCjProject);
        return hddcCjProject;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcCjProjectEntity updateHddcCjProject(HddcCjProjectEntity hddcCjProject) {
        HddcCjProjectEntity entity = hddcCjProjectRepository.findById(hddcCjProject.getId()).get();
        UpdateUtil.copyNullProperties(entity,hddcCjProject);
        hddcCjProject.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcCjProject.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcCjProjectRepository.save(hddcCjProject);
        return hddcCjProject;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcCjProjects(List<String> ids) {
        List<HddcCjProjectEntity> hddcCjProjectList = this.hddcCjProjectRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcCjProjectList) && hddcCjProjectList.size() > 0) {
            for(HddcCjProjectEntity hddcCjProject : hddcCjProjectList) {
                this.hddcCjProjectRepository.delete(hddcCjProject);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public List<HddcCjProjectEntity> findAll() {
        /*List<HddcCjProjectEntity> hddcCjProjectList = this.hddcCjProjectRepository.findAll();
        return hddcCjProjectList;*/
        return hddcCjProjectNativeRepository.queryHddcCjProjectName();
    }

    @Override
    public List<HddcCjProjectEntity> findHddcCjProjectEntityByPersonId(String personid) {
        List<HddcCjProjectEntity> hddcCjProjectList = this.hddcCjProjectRepository.findHddcCjProjectEntityByPersonId(personid);
        return hddcCjProjectList;
    }

    @Override
    public JSONObject queryHddcCjProjectsByPersonId(String personId, int curPage, int pageSize) {
        Page<HddcCjProjectEntity> hddcCjProjectPage = this.hddcCjProjectNativeRepository.queryHddcCjProjectsByPersonId(personId, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcCjProjectPage);
        return jsonObject;
    }

}
