package com.css.zfzx.sjcj.modules.hddcWorkRegion.repository;

import com.css.zfzx.sjcj.modules.hddcWorkRegion.repository.entity.HddcWorkregionEntity;
import com.css.zfzx.sjcj.modules.hddcWorkRegion.viewobjects.HddcWorkregionQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcWorkregionNativeRepository {

    Page<HddcWorkregionEntity> queryHddcWorkregions(HddcWorkregionQueryParams queryParams, int curPage, int pageSize);

    List<HddcWorkregionEntity> exportYhDisasters(HddcWorkregionQueryParams queryParams);
}
