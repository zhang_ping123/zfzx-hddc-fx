package com.css.zfzx.sjcj.modules.hddcTargetRegion.repository.entity;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
@Entity
@Table(name="hddc_targetregion")
public class HddcTargetregionEntity implements Serializable {

    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 地形变监测工程数
     */
    @Column(name="crustaldfmprojectcount")
    private Integer crustaldfmprojectcount;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 是否开展地震危害性评价
     */
    @Column(name="sda")
    private Integer sda;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 目标区研究断层总数
     */
    @Column(name="trstudiedfaultcount")
    private Integer trstudiedfaultcount;
    /**
     * 地球物理探测工程数
     */
    @Column(name="geophysicalsvyprojectcount")
    private Integer geophysicalsvyprojectcount;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 备注
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 获得测试结果样品数
     */
    @Column(name="datingsamplecount")
    private Integer datingsamplecount;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;
    /**
     * 是否开展地震危险性评价
     */
    @Column(name="sra")
    private Integer sra;
    /**
     * 探测总土方量 [m³]
     */
    @Column(name="trenchvolume")
    private Double trenchvolume;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 地球化学探测工程数
     */
    @Column(name="geochemicalprojectcount")
    private Integer geochemicalprojectcount;
    /**
     * 描述信息
     */
    @Column(name="description")
    private String description;
    /**
     * 是否火山地质调查填图
     */
    @Column(name="isvolcanic")
    private Integer isvolcanic;
    /**
     * 遥感影像处理数目
     */
    @Column(name="rsprocess")
    private Integer rsprocess;
    /**
     * 采集样品总数
     */
    @Column(name="collectedsamplecount")
    private Integer collectedsamplecount;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 地质填图面积 [km²]
     */
    @Column(name="mappingarea")
    private Integer mappingarea;
    /**
     * 送样总数
     */
    @Column(name="samplecount")
    private Integer samplecount;
    /**
     * 目标区确定活动断层条数
     */
    @Column(name="afaultcount")
    private Integer afaultcount;
    /**
     * 目标区名称（课题名称）
     */
    @Column(name="targetname")
    private String targetname;
    /**
     * 工作区编号
     */
    @Column(name="workregionid")
    private String workregionid;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 野外观测点数
     */
    @Column(name="fieldsvyptcount")
    private Integer fieldsvyptcount;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 编号
     */
    //@Id
    @Column(name="id")
    private String id;
    /**
     * 编号
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 微地貌测量工程总数
     */
    @Column(name="geomorphysvyprojectcount")
    private Integer geomorphysvyprojectcount;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 探槽数
     */
    @Column(name="trenchcount")
    private Integer trenchcount;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 地震监测工程数
     */
    @Column(name="seismicprojectcount")
    private Integer seismicprojectcount;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 钻孔进尺 [米]
     */
    @Column(name="drilllength")
    private Integer drilllength;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 目标区断层总数
     */
    @Column(name="trfaultcount")
    private Integer trfaultcount;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 钻孔数
     */
    @Column(name="drillcount")
    private Integer drillcount;
    /**
     * 项目名称（大项目名称）
     */
    @Column(name="name")
    private String name;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 显示码
     */
    @Column(name="showcode")
    private String showcode;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 断层三维数值模拟总数
     */
    @Column(name="numsimulationcount")
    private Integer numsimulationcount;
    /**
     * 地球物理测井数
     */
    @Column(name="gphwellcount")
    private Integer gphwellcount;
    /**
     * 目标区面积 [km²]
     */
    @Column(name="trarea")
    private Integer trarea;

}

