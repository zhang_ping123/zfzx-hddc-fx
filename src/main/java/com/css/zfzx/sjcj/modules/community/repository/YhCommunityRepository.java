package com.css.zfzx.sjcj.modules.community.repository;

import com.css.zfzx.sjcj.modules.community.repository.entity.YhCommunityEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author yyd
 * @date 2020-11-04
 */
public interface YhCommunityRepository extends JpaRepository<YhCommunityEntity, String> {
}
