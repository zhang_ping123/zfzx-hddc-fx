package com.css.zfzx.sjcj.modules.hddcGeoGeomorphySvyPoint.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.bpm.platform.utils.PlatformPageUtils;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltQueryParams;
import com.css.zfzx.sjcj.modules.hddcGeoGeomorphySvyPoint.repository.entity.HddcGeogeomorphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcGeoGeomorphySvyPoint.service.HddcGeogeomorphysvypointService;
import com.css.zfzx.sjcj.modules.hddcGeoGeomorphySvyPoint.viewobjects.HddcGeogeomorphysvypointQueryParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author lihelei
 * @date 2020-11-26
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcGeogeomorphysvypoints")
public class HddcGeogeomorphysvypointController {
    @Autowired
    private HddcGeogeomorphysvypointService hddcGeogeomorphysvypointService;

    @GetMapping("/queryHddcGeogeomorphysvypoints")
    public RestResponse queryHddcGeogeomorphysvypoints(HttpServletRequest request, HddcGeogeomorphysvypointQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcGeogeomorphysvypointService.queryHddcGeogeomorphysvypoints(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcGeogeomorphysvypoint(@PathVariable String id) {
        RestResponse response = null;
        try{
            HddcGeogeomorphysvypointEntity hddcGeogeomorphysvypoint = hddcGeogeomorphysvypointService.getHddcGeogeomorphysvypoint(id);
            response = RestResponse.succeed(hddcGeogeomorphysvypoint);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcGeogeomorphysvypoint(@RequestBody HddcGeogeomorphysvypointEntity hddcGeogeomorphysvypoint) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcGeogeomorphysvypointService.saveHddcGeogeomorphysvypoint(hddcGeogeomorphysvypoint);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcGeogeomorphysvypoint(@RequestBody HddcGeogeomorphysvypointEntity hddcGeogeomorphysvypoint)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcGeogeomorphysvypointService.updateHddcGeogeomorphysvypoint(hddcGeogeomorphysvypoint);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcGeogeomorphysvypoints(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcGeogeomorphysvypointService.deleteHddcGeogeomorphysvypoints(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcGeogeomorphysvypointService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }
    /***
     * 导出
     * @param response
     * @return
     */
    @GetMapping("/exportFile")
    public RestResponse exportFileYhDisasters(HttpServletResponse response,
                                              @RequestParam("projectName")String projectName,@RequestParam("geomorphyname") String geomorphyname,
                                              @RequestParam("province") String province,@RequestParam("city")String city,@RequestParam("area")String area) {
        RestResponse responseRest = null;
        JSONObject jsonObject = new JSONObject();
        try{
            HddcGeogeomorphysvypointQueryParams queryParams=new HddcGeogeomorphysvypointQueryParams();
            queryParams.setArea(area);
            queryParams.setCity(city);
            queryParams.setProvince(province);
            queryParams.setProjectName(projectName);
            queryParams.setGeomorphyname(geomorphyname);
            hddcGeogeomorphysvypointService.exportFile(queryParams,response);
            jsonObject.put("message", "导出成功");
            responseRest = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            responseRest = RestResponse.fail(errorMessage);
        }
        return responseRest;
    }

    /**
     * 导入
     *
     * @param file
     * @param response
     * @return
     */
    @PostMapping("/importDisaster")
    public RestResponse export(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        RestResponse restResponse = null;
        try {
            String s = hddcGeogeomorphysvypointService.exportExcel( file, response);
            restResponse = RestResponse.succeed(s);
        } catch (Exception e) {
            String errormessage = "导入失败";
            log.error(errormessage, e);
            restResponse = RestResponse.fail(errormessage);
        }
        return restResponse;
    }
}