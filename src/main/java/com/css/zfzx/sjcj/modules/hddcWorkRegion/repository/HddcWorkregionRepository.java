package com.css.zfzx.sjcj.modules.hddcWorkRegion.repository;

import com.css.zfzx.sjcj.modules.hddcWorkRegion.repository.entity.HddcWorkregionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcWorkregionRepository extends JpaRepository<HddcWorkregionEntity, String> {
}
