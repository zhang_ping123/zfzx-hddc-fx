package com.css.zfzx.sjcj.modules.hddcGeophySvyPoint.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Data
public class HddcGeophysvypointQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String labelinfo;

}
