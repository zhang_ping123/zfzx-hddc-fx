package com.css.zfzx.sjcj.modules.hddcA1LiteratureDocumentTable.repository;

import com.css.zfzx.sjcj.modules.hddcA1LiteratureDocumentTable.repository.entity.HddcA1LiteraturedocumenttableEntity;
import com.css.zfzx.sjcj.modules.hddcA1LiteratureDocumentTable.viewobjects.HddcA1LiteraturedocumenttableQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcA1LiteraturedocumenttableNativeRepository {

    List<HddcA1LiteraturedocumenttableEntity> exportYhDisasters(HddcA1LiteraturedocumenttableQueryParams queryParams);

    Page<HddcA1LiteraturedocumenttableEntity> queryHddcA1Literaturedocumenttables(HddcA1LiteraturedocumenttableQueryParams queryParams, int curPage, int pageSize);
}
