package com.css.zfzx.sjcj.modules.analysis.vo;

import lombok.Data;

/**
 * TODO
 *
 * @author 王昊杰
 * @version 1.0
 * @date 2020/12/11  9:41
 */

@Data
public class HddcAppZztCountVo {

    private String startTime;
    private String endTime;
    private String projectName;
    private String taskName;

    private String province;
    private String city;
    private String area;
    private String name;

    private String RowNum;
    private String ErrorMsg;
}
