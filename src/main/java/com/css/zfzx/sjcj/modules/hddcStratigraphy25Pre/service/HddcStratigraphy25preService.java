package com.css.zfzx.sjcj.modules.hddcStratigraphy25Pre.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcStratigraphy25Pre.repository.entity.HddcStratigraphy25preEntity;
import com.css.zfzx.sjcj.modules.hddcStratigraphy25Pre.viewobjects.HddcStratigraphy25preQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */

public interface HddcStratigraphy25preService {

    public JSONObject queryHddcStratigraphy25pres(HddcStratigraphy25preQueryParams queryParams, int curPage, int pageSize);

    public HddcStratigraphy25preEntity getHddcStratigraphy25pre(String id);

    public HddcStratigraphy25preEntity saveHddcStratigraphy25pre(HddcStratigraphy25preEntity hddcStratigraphy25pre);

    public HddcStratigraphy25preEntity updateHddcStratigraphy25pre(HddcStratigraphy25preEntity hddcStratigraphy25pre);

    public void deleteHddcStratigraphy25pres(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcStratigraphy25preQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
