package com.css.zfzx.sjcj.modules.hddcStratigraphy25LinePre.repository;

import com.css.zfzx.sjcj.modules.hddcStratigraphy25LinePre.repository.entity.HddcStratigraphy25linepreEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcStratigraphy25linepreRepository extends JpaRepository<HddcStratigraphy25linepreEntity, String> {
}
