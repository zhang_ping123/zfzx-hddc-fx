package com.css.zfzx.sjcj.modules.hddcEpiMechanismResult.repository.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Data
@Entity
@Table(name="hddc_epimechanismresult")
public class HddcEpimechanismresultEntity implements Serializable {

    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;
    /**
     * 备注
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * T轴方位
     */
    @Column(name="tazimuth")
    private Integer tazimuth;
    /**
     * 震级
     */
    @Column(name="magnitude")
    private Double magnitude;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 水平最大主应力方位
     */
    @Column(name="shazimuth")
    private Integer shazimuth;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 震源深度 [公里]
     */
    @Column(name="depth")
    private Double depth;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * Ⅰ节面走向
     */
    @Column(name="plane1strike")
    private Integer plane1strike;
    /**
     * Ⅱ节面走向
     */
    @Column(name="plane2strike")
    private Integer plane2strike;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * Ⅰ节面滑动角
     */
    @Column(name="plane1slip")
    private Integer plane1slip;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 资料来源
     */
    @Column(name="reference")
    private String reference;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 发震时间
     */
    @Column(name="occurrencetime")
    private String occurrencetime;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * B轴方位
     */
    @Column(name="bazimuth")
    private Integer bazimuth;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 编号
     */
    @Column(name="id")
    private String id;
    /**
     * 编号
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * Ⅱ节面滑动角
     */
    @Column(name="plane2slip")
    private Integer plane2slip;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 震源机制解的求解方法
     */
    @Column(name="method")
    private String method;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 震级单位 [Ms、Ml、Mw等]
     */
    @Column(name="unit")
    private String unit;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 地名
     */
    @Column(name="locationname")
    private String locationname;
    /**
     * 发震日期
     */
    @Column(name="occurrencedate")
    private String occurrencedate;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * T轴倾角
     */
    @Column(name="tplunge")
    private Integer tplunge;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * P轴倾角
     */
    @Column(name="pplunge")
    private Integer pplunge;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * Ⅱ节面倾角
     */
    @Column(name="plane2dip")
    private Integer plane2dip;
    /**
     * B轴倾角
     */
    @Column(name="bplunge")
    private Integer bplunge;
    /**
     * 震中纬度
     */
    @Column(name="lat")
    private Double lat;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 震中经度
     */
    @Column(name="lon")
    private Double lon;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * Ⅰ节面倾角
     */
    @Column(name="plane1dip")
    private Integer plane1dip;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 应力状态
     */
    @Column(name="stressregime")
    private Integer stressregime;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * P轴方位
     */
    @Column(name="pazimuth")
    private Integer pazimuth;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;

}

