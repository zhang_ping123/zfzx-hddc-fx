package com.css.zfzx.sjcj.modules.hddcA6StationHasWaveforms.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Data
public class HddcA6StationhaswaveformsQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

}
