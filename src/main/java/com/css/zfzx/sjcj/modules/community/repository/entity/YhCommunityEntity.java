package com.css.zfzx.sjcj.modules.community.repository.entity;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author yyd
 * @date 2020-11-04
 */
@Data
@Entity
@Table(name="yh_community")
public class YhCommunityEntity implements Serializable {

    /**
     * 区
     */
    @Column(name="area")
    private String area;
    /**
     * 建筑高度
     */
    @Column(name="building_height")
    private Double buildingHeight;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 状态
     */
    @Column(name="status")
    private String status;
    /**
     * 隐患等级
     */
    @Column(name="yh_level")
    private String yhLevel;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 建筑等级
     */
    @Column(name="building_level")
    private String buildingLevel;
    /**
     * 经度
     */
    @Column(name="longitude")
    private Double longitude;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 主键ID
     */
    @Id
    @Column(name="id")
    private String id;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 建筑面积
     */
    @Column(name="building_area")
    private Double buildingArea;
    /**
     * 是否有效,0:无效,1:有效
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 纬度
     */
    @Column(name="latitude")
    private Double latitude;
    /**
     * 承灾体名称
     */
    @Column(name="czt_name")
    private String cztName;
    /**
     * 原因
     */
    @Column(name="reason")
    private String reason;

}

