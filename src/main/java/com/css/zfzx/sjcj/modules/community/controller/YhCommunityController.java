package com.css.zfzx.sjcj.modules.community.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.zfzx.sjcj.modules.community.repository.entity.YhCommunityEntity;
import com.css.zfzx.sjcj.modules.community.service.YhCommunityService;
import com.css.zfzx.sjcj.modules.community.viewobjects.YhCommunityQueryParams;
import com.css.bpm.platform.utils.PlatformPageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yyd
 * @date 2020-11-04
 */
@Slf4j
@RestController
@RequestMapping("/yhCommunitys")
public class YhCommunityController {
    @Autowired
    private YhCommunityService yhCommunityService;

    @GetMapping("/queryYhCommunitys")
    public RestResponse queryYhCommunitys(HttpServletRequest request, YhCommunityQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = yhCommunityService.queryYhCommunitys(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getYhCommunity(@PathVariable String id) {
        RestResponse response = null;
        try{
            YhCommunityEntity yhCommunity = yhCommunityService.getYhCommunity(id);
            response = RestResponse.succeed(yhCommunity);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveYhCommunity(@RequestBody YhCommunityEntity yhCommunity) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            yhCommunityService.saveYhCommunity(yhCommunity);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateYhCommunity(@RequestBody YhCommunityEntity yhCommunity)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            yhCommunityService.updateYhCommunity(yhCommunity);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteYhCommunitys(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            yhCommunityService.deleteYhCommunitys(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(yhCommunityService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }

}