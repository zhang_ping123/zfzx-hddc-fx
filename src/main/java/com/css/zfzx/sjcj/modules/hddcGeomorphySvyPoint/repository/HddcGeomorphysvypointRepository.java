package com.css.zfzx.sjcj.modules.hddcGeomorphySvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcGeomorphySvyPoint.repository.entity.HddcGeomorphysvypointEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcGeomorphysvypointRepository extends JpaRepository<HddcGeomorphysvypointEntity, String> {
}
