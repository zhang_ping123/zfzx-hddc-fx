package com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningPt.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningPt.repository.entity.HddcWyGeologicalsvyplanningptEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningPt.viewobjects.HddcWyGeologicalsvyplanningptQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcWyGeologicalsvyplanningptNativeRepository {

    Page<HddcWyGeologicalsvyplanningptEntity> queryHddcWyGeologicalsvyplanningpts(HddcWyGeologicalsvyplanningptQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyGeologicalsvyplanningpt(HddcAppZztCountVo queryParams);
}
