package com.css.zfzx.sjcj.modules.hddcGMInterpretationLine.repository;

import com.css.zfzx.sjcj.modules.hddcGMInterpretationLine.repository.entity.HddcGminterpretationlineEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-26
 */
public interface HddcGminterpretationlineRepository extends JpaRepository<HddcGminterpretationlineEntity, String> {
}
