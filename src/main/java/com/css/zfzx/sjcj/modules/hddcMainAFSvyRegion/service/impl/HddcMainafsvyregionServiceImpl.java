package com.css.zfzx.sjcj.modules.hddcMainAFSvyRegion.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcMainAFSvyRegion.repository.HddcMainafsvyregionNativeRepository;
import com.css.zfzx.sjcj.modules.hddcMainAFSvyRegion.repository.HddcMainafsvyregionRepository;
import com.css.zfzx.sjcj.modules.hddcMainAFSvyRegion.repository.entity.HddcMainafsvyregionEntity;
import com.css.zfzx.sjcj.modules.hddcMainAFSvyRegion.service.HddcMainafsvyregionService;
import com.css.zfzx.sjcj.modules.hddcMainAFSvyRegion.viewobjects.HddcMainafsvyregionQueryParams;
import com.css.zfzx.sjcj.modules.hddcMainAFSvyRegion.viewobjects.HddcMainafsvyregionVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-14
 */
@Service
public class HddcMainafsvyregionServiceImpl implements HddcMainafsvyregionService {

	@Autowired
    private HddcMainafsvyregionRepository hddcMainafsvyregionRepository;
    @Autowired
    private HddcMainafsvyregionNativeRepository hddcMainafsvyregionNativeRepository;

    @Override
    public JSONObject queryHddcMainafsvyregions(HddcMainafsvyregionQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcMainafsvyregionEntity> hddcMainafsvyregionPage = this.hddcMainafsvyregionNativeRepository.queryHddcMainafsvyregions(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcMainafsvyregionPage);
        return jsonObject;
    }


    @Override
    public HddcMainafsvyregionEntity getHddcMainafsvyregion(String id) {
        HddcMainafsvyregionEntity hddcMainafsvyregion = this.hddcMainafsvyregionRepository.findById(id).orElse(null);
         return hddcMainafsvyregion;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcMainafsvyregionEntity saveHddcMainafsvyregion(HddcMainafsvyregionEntity hddcMainafsvyregion) {
        String uuid = UUIDGenerator.getUUID();
        hddcMainafsvyregion.setUuid(uuid);
        hddcMainafsvyregion.setCreateUser(PlatformSessionUtils.getUserId());
        hddcMainafsvyregion.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcMainafsvyregionRepository.save(hddcMainafsvyregion);
        return hddcMainafsvyregion;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcMainafsvyregionEntity updateHddcMainafsvyregion(HddcMainafsvyregionEntity hddcMainafsvyregion) {
        HddcMainafsvyregionEntity entity = hddcMainafsvyregionRepository.findById(hddcMainafsvyregion.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcMainafsvyregion);
        hddcMainafsvyregion.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcMainafsvyregion.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcMainafsvyregionRepository.save(hddcMainafsvyregion);
        return hddcMainafsvyregion;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcMainafsvyregions(List<String> ids) {
        List<HddcMainafsvyregionEntity> hddcMainafsvyregionList = this.hddcMainafsvyregionRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcMainafsvyregionList) && hddcMainafsvyregionList.size() > 0) {
            for(HddcMainafsvyregionEntity hddcMainafsvyregion : hddcMainafsvyregionList) {
                this.hddcMainafsvyregionRepository.delete(hddcMainafsvyregion);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcMainafsvyregionQueryParams queryParams, HttpServletResponse response) {
        List<HddcMainafsvyregionEntity> yhDisasterEntities = hddcMainafsvyregionNativeRepository.exportYhDisasters(queryParams);
        List<HddcMainafsvyregionVO> list=new ArrayList<>();
        for (HddcMainafsvyregionEntity entity:yhDisasterEntities) {
            HddcMainafsvyregionVO yhDisasterVO=new HddcMainafsvyregionVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"主要活动断层制图区-面","主要活动断层制图区-面",HddcMainafsvyregionVO.class,"主要活动断层制图区-面.xls",response);
    }


    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcMainafsvyregionVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcMainafsvyregionVO.class, params);
            List<HddcMainafsvyregionVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcMainafsvyregionVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcMainafsvyregionVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcMainafsvyregionVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcMainafsvyregionEntity yhDisasterEntity = new HddcMainafsvyregionEntity();
            HddcMainafsvyregionVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcMainafsvyregion(yhDisasterEntity);
        }
    }

}
