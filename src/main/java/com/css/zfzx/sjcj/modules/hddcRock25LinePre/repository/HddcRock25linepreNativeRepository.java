package com.css.zfzx.sjcj.modules.hddcRock25LinePre.repository;

import com.css.zfzx.sjcj.modules.hddcRock25LinePre.repository.entity.HddcRock25linepreEntity;
import com.css.zfzx.sjcj.modules.hddcRock25LinePre.viewobjects.HddcRock25linepreQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcRock25linepreNativeRepository {

    Page<HddcRock25linepreEntity> queryHddcRock25linepres(HddcRock25linepreQueryParams queryParams, int curPage, int pageSize);

    List<HddcRock25linepreEntity> exportYhDisasters(HddcRock25linepreQueryParams queryParams);
}
