package com.css.zfzx.sjcj.modules.hddcGeologicalSvyPoint.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author lihelei
 * @date 2020-11-27
 */
@Data
public class HddcGeologicalsvypointVO implements Serializable {

    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 是否地层点
     */
    @Excel(name = "是否地层点", orderNum = "4")
    private Integer isstratigraphypoint;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 地质剖面线编号
     */
    @Excel(name = "地质剖面线编号", orderNum = "5")
    private String profilesvylineid;
    /**
     * 纬度
     */
    @Excel(name = "纬度", orderNum = "6")
    private Double lat;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 工程编号
     */
    @Excel(name = "工程编号", orderNum = "7")
    private String projectid;
    /**
     * 拍摄者
     */
    @Excel(name = "拍摄者", orderNum = "8")
    private String photographer;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 送样总数
     */
    @Excel(name = "送样总数", orderNum = "9")
    private Integer samplecount;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 典型照片原始文件编号
     */
    @Excel(name = "典型照片原始文件编号", orderNum = "10")
    private String photoArwid;
    /**
     * 海拔高度 [米]
     */
    @Excel(name = "海拔高度 [米]", orderNum = "11")
    private Integer elevation;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 观测点野外编号
     */
    @NotNull
    @Excel(name = "观测点野外编号", orderNum = "12")
    private String fieldid;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 典型照片文件编号
     */
    @Excel(name = "典型照片文件编号", orderNum = "13")
    private String photoAiid;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 是否在图中显示
     */
    @Excel(name = "是否在图中显示", orderNum = "14")
    private Integer isinmap;
    /**
     * 观测点地名
     */
    @Excel(name = "观测点地名", orderNum = "15")
    private String locationname;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 经度
     */
    @Excel(name = "经度", orderNum = "16")
    private Double lon;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 观测方法
     */
    @Excel(name = "观测方法", orderNum = "17")
    private String svymethods;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 是否地貌点
     */
    @Excel(name = "是否地貌点", orderNum = "18")
    private Integer isgeomorphpoint;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 观测目的
     */
    @Excel(name = "观测目的", orderNum = "19")
    private String purpose;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "20")
    private String commentInfo;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 是否断点
     */
    @Excel(name = "是否断点", orderNum = "21")
    private Integer isfaultpoint;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 观测点描述
     */
    @Excel(name = "观测点描述", orderNum = "22")
    private String spcommentInfo;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 照片镜向
     */
    @Excel(name = "照片镜向", orderNum = "23")
    private Integer photoviewingto;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 观测点编号
     */
    private String id;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 获得测试结果样品数
     */
    @Excel(name = "获得测试结果样品数", orderNum = "24")
    private Integer datingsamplecount;
    /**
     * 采集样品总数
     */
    @Excel(name = "采集样品总数", orderNum = "25")
    private Integer collectedsamplecount;
    /**
     * 观测日期
     */
    @Excel(name = "观测日期", orderNum = "26")
    private String svydate;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer photoviewingtoName;
    private String rowNum;
    private String errorMsg;
}