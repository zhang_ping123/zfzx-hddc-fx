package com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningPt.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningPt.repository.entity.HddcWyGeologicalsvyplanningptEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningPt.viewobjects.HddcWyGeologicalsvyplanningptQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */

public interface HddcWyGeologicalsvyplanningptService {

    public JSONObject queryHddcWyGeologicalsvyplanningpts(HddcWyGeologicalsvyplanningptQueryParams queryParams, int curPage, int pageSize);

    public HddcWyGeologicalsvyplanningptEntity getHddcWyGeologicalsvyplanningpt(String uuid);

    public HddcWyGeologicalsvyplanningptEntity saveHddcWyGeologicalsvyplanningpt(HddcWyGeologicalsvyplanningptEntity hddcWyGeologicalsvyplanningpt);

    public HddcWyGeologicalsvyplanningptEntity updateHddcWyGeologicalsvyplanningpt(HddcWyGeologicalsvyplanningptEntity hddcWyGeologicalsvyplanningpt);

    public void deleteHddcWyGeologicalsvyplanningpts(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    BigInteger queryHddcWyGeologicalsvyplanningpt(HddcAppZztCountVo queryParams);

    List<HddcWyGeologicalsvyplanningptEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId,String taskId,String projectId,String isValid);

    List<HddcWyGeologicalsvyplanningptEntity> findAllByCreateUserAndIsValid(String userId,String isValid);

    /**
     * 逻辑删除-根据项目id删除数据
     * @param ids
     */
    void deleteByProjectId(List<String> ids);

    /**
     * 逻辑删除-根据任务id删除数据
     * @param ids
     */
    void deleteByTaskId(List<String> ids);

}
