package com.css.zfzx.sjcj.modules.hddcDrillProfile.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
public class HddcDrillprofileVO implements Serializable {

    /**
     * 创建人
     */
    private String createUser;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 剖面编号
     */
    private String id;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 钻孔剖面图原始文件
     */
    @Excel(name = "钻孔剖面图原始文件", orderNum = "4")
    private String holesectionArwid;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 目标断层编号
     */
    @Excel(name = "目标断层编号", orderNum = "5")
    private String targetfaultid;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 工程编号
     */
    @Excel(name = "工程编号", orderNum = "6")
    private String projectid;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 跨断层钻探描述
     */
    @Excel(name = "跨断层钻探描述", orderNum = "7")
    private String commentInfo;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 剖面长度 [米]
     */
    @Excel(name = "剖面长度 [米]", orderNum = "8")
    private Double length;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备注
     */
    private String remark;
    /**
     * 目标断层名称
     */
    @Excel(name = "目标断层名称", orderNum = "9")
    private String targetfaultname;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 剖面名称
     */
    @Excel(name = "剖面名称", orderNum = "10")
    private String name;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 钻孔数
     */
    @Excel(name = "钻孔数", orderNum = "11")
    private Integer drillingholecount;
    /**
     * 断点数
     */
    @Excel(name = "断点数", orderNum = "12")
    private Integer faultpointcount;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 村
     */
    private String village;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 目标断层来源
     */
    private String targetfaultsource;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 钻探地点
     */
    @Excel(name = "钻探地点", orderNum = "13")
    private String locationname;
    /**
     * 钻孔剖面图图像文件编号
     */
    @Excel(name = "钻孔剖面图图像文件编号", orderNum = "14")
    private String holesectionAiid;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 任务名称
     */
    private String taskName;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String rowNum;
    private String errorMsg;
}