package com.css.zfzx.sjcj.modules.hddcB7_VolSvyPtObservationObject.repository;

import com.css.zfzx.sjcj.modules.hddcB7_VolSvyPtObservationObject.repository.entity.HddcB7VolsvyptobservationobjectEntity;
import com.css.zfzx.sjcj.modules.hddcB7_VolSvyPtObservationObject.viewobjects.HddcB7VolsvyptobservationobjectQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
public interface HddcB7VolsvyptobservationobjectNativeRepository {

    Page<HddcB7VolsvyptobservationobjectEntity> queryHddcB7Volsvyptobservationobjects(HddcB7VolsvyptobservationobjectQueryParams queryParams, int curPage, int pageSize);

    List<HddcB7VolsvyptobservationobjectEntity> exportYhDisasters(HddcB7VolsvyptobservationobjectQueryParams queryParams);
}
