package com.css.zfzx.sjcj.modules.yhhospital.repository.impl;

import com.css.zfzx.sjcj.modules.yhhospital.repository.YhHospitalNativeRepository;
import com.css.zfzx.sjcj.modules.yhhospital.repository.entity.YhHospitalEntity;
import com.css.zfzx.sjcj.modules.yhhospital.viewobjects.YhHospitalQueryParams;
import com.css.bpm.platform.utils.PlatformObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
/**
 * @author yyd
 * @date 2020-11-03
 */
@Repository
public class YhHospitalNativeRepositoryImpl implements YhHospitalNativeRepository {
    @PersistenceContext
    private EntityManager em;

    private static final Logger log = LoggerFactory.getLogger(YhHospitalNativeRepositoryImpl.class);


    @Override
    public Page<YhHospitalEntity> queryYhHospitals(YhHospitalQueryParams queryParams, int curPage, int pageSize) {
        StringBuilder sql = new StringBuilder("select * from yh_hospital");
        StringBuilder whereSql = new StringBuilder(" where 1=1");
        if(!PlatformObjectUtils.isEmpty(queryParams.getCztName())) {
            whereSql.append(" and czt_name = :cztName");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getProvince())) {
            whereSql.append(" and province = :province");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getCity())) {
            whereSql.append(" and city = :city");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getArea())) {
            whereSql.append(" and area = :area");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getBuildingLevel())) {
            whereSql.append(" and building_level = :buildingLevel");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getYhLevel())) {
            whereSql.append(" and yh_level = :yhLevel");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getStatus())) {
            whereSql.append(" and status = :status");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getCreateTime())) {
            whereSql.append(" and create_time = :createTime");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getCreateUser())) {
            whereSql.append(" and create_user = :createUser");
        }
        Query query = this.em.createNativeQuery(sql.append(whereSql).toString(), YhHospitalEntity.class);
        StringBuilder countSql = new StringBuilder("select count(*) from yh_hospital ");
        Query countQuery = this.em.createNativeQuery(countSql.append(whereSql).toString());
        if(!PlatformObjectUtils.isEmpty(queryParams.getCztName())) {
            query.setParameter("cztName", queryParams.getCztName());
            countQuery.setParameter("cztName", queryParams.getCztName());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getProvince())) {
            query.setParameter("province", queryParams.getProvince());
            countQuery.setParameter("province", queryParams.getProvince());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getCity())) {
            query.setParameter("city", queryParams.getCity());
            countQuery.setParameter("city", queryParams.getCity());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getArea())) {
            query.setParameter("area", queryParams.getArea());
            countQuery.setParameter("area", queryParams.getArea());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getBuildingLevel())) {
            query.setParameter("buildingLevel", queryParams.getBuildingLevel());
            countQuery.setParameter("buildingLevel", queryParams.getBuildingLevel());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getYhLevel())) {
            query.setParameter("yhLevel", queryParams.getYhLevel());
            countQuery.setParameter("yhLevel", queryParams.getYhLevel());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getStatus())) {
            query.setParameter("status", queryParams.getStatus());
            countQuery.setParameter("status", queryParams.getStatus());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getCreateTime())) {
            Date createTime = null;
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                createTime = sf.parse(queryParams.getCreateTime());
            } catch (Exception e) {
                log.error("日期格式转换错误");
            }
            query.setParameter("createTime", createTime);
            countQuery.setParameter("createTime", createTime);
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getCreateUser())) {
            query.setParameter("createUser", queryParams.getCreateUser());
            countQuery.setParameter("createUser", queryParams.getCreateUser());
        }
        Pageable pageable = PageRequest.of(curPage - 1, pageSize);
        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());
        List<YhHospitalEntity> list = query.getResultList();
        BigInteger count = (BigInteger) countQuery.getSingleResult();
        Page<YhHospitalEntity> yhHospitalPage = new PageImpl<>(list, pageable, count.longValue());
        return yhHospitalPage;
    }
}
