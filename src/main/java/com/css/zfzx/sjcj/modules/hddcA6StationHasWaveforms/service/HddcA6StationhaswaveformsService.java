package com.css.zfzx.sjcj.modules.hddcA6StationHasWaveforms.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcA6StationHasWaveforms.repository.entity.HddcA6StationhaswaveformsEntity;
import com.css.zfzx.sjcj.modules.hddcA6StationHasWaveforms.viewobjects.HddcA6StationhaswaveformsQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */

public interface HddcA6StationhaswaveformsService {

    public JSONObject queryHddcA6Stationhaswaveformss(HddcA6StationhaswaveformsQueryParams queryParams, int curPage, int pageSize);

    public HddcA6StationhaswaveformsEntity getHddcA6Stationhaswaveforms(String id);

    public HddcA6StationhaswaveformsEntity saveHddcA6Stationhaswaveforms(HddcA6StationhaswaveformsEntity hddcA6Stationhaswaveforms);

    public HddcA6StationhaswaveformsEntity updateHddcA6Stationhaswaveforms(HddcA6StationhaswaveformsEntity hddcA6Stationhaswaveforms);

    public void deleteHddcA6Stationhaswaveformss(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcA6StationhaswaveformsQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
