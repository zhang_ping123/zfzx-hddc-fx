package com.css.zfzx.sjcj.modules.app.service;

import com.css.bpm.platform.org.user.repository.entity.UserEntity;
import com.css.bpm.platform.page.login.viewobjects.PwdCheck;
import com.css.zfzx.sjcj.modules.app.vo.RegistUserVo;

import java.util.List;

public interface AppLoginService {

    /***
     * App端登录验证
     * @param userName
     * @param password
     * @param privateKey
     * @return
     * @throws Exception
     */
    PwdCheck doLogin(String userName, String password, String privateKey) throws Exception;

    /**
     * 注册用户
     * @param registUserVo
     */
    void registUser(RegistUserVo registUserVo);
    /**
     * 校验身份证号
     */
    List<UserEntity> checkIdCard(String idCard);
}
