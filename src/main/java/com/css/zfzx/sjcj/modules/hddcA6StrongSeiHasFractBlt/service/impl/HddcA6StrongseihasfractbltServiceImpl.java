package com.css.zfzx.sjcj.modules.hddcA6StrongSeiHasFractBlt.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcA6StrongSeiHasFractBlt.repository.HddcA6StrongseihasfractbltNativeRepository;
import com.css.zfzx.sjcj.modules.hddcA6StrongSeiHasFractBlt.repository.HddcA6StrongseihasfractbltRepository;
import com.css.zfzx.sjcj.modules.hddcA6StrongSeiHasFractBlt.repository.entity.HddcA6StrongseihasfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcA6StrongSeiHasFractBlt.service.HddcA6StrongseihasfractbltService;
import com.css.zfzx.sjcj.modules.hddcA6StrongSeiHasFractBlt.viewobjects.HddcA6StrongseihasfractbltQueryParams;
import com.css.zfzx.sjcj.modules.hddcA6StrongSeiHasFractBlt.viewobjects.HddcA6StrongseihasfractbltVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Service
public class HddcA6StrongseihasfractbltServiceImpl implements HddcA6StrongseihasfractbltService {

	@Autowired
    private HddcA6StrongseihasfractbltRepository hddcA6StrongseihasfractbltRepository;
    @Autowired
    private HddcA6StrongseihasfractbltNativeRepository hddcA6StrongseihasfractbltNativeRepository;

    @Override
    public JSONObject queryHddcA6Strongseihasfractblts(HddcA6StrongseihasfractbltQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcA6StrongseihasfractbltEntity> hddcA6StrongseihasfractbltPage = this.hddcA6StrongseihasfractbltNativeRepository.queryHddcA6Strongseihasfractblts(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcA6StrongseihasfractbltPage);
        return jsonObject;
    }


    @Override
    public HddcA6StrongseihasfractbltEntity getHddcA6Strongseihasfractblt(String id) {
        HddcA6StrongseihasfractbltEntity hddcA6Strongseihasfractblt = this.hddcA6StrongseihasfractbltRepository.findById(id).orElse(null);
         return hddcA6Strongseihasfractblt;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcA6StrongseihasfractbltEntity saveHddcA6Strongseihasfractblt(HddcA6StrongseihasfractbltEntity hddcA6Strongseihasfractblt) {
        String uuid = UUIDGenerator.getUUID();
        hddcA6Strongseihasfractblt.setUuid(uuid);
        hddcA6Strongseihasfractblt.setCreateUser(PlatformSessionUtils.getUserId());
        hddcA6Strongseihasfractblt.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcA6StrongseihasfractbltRepository.save(hddcA6Strongseihasfractblt);
        return hddcA6Strongseihasfractblt;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcA6StrongseihasfractbltEntity updateHddcA6Strongseihasfractblt(HddcA6StrongseihasfractbltEntity hddcA6Strongseihasfractblt) {
        HddcA6StrongseihasfractbltEntity entity = hddcA6StrongseihasfractbltRepository.findById(hddcA6Strongseihasfractblt.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcA6Strongseihasfractblt);
        hddcA6Strongseihasfractblt.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcA6Strongseihasfractblt.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcA6StrongseihasfractbltRepository.save(hddcA6Strongseihasfractblt);
        return hddcA6Strongseihasfractblt;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcA6Strongseihasfractblts(List<String> ids) {
        List<HddcA6StrongseihasfractbltEntity> hddcA6StrongseihasfractbltList = this.hddcA6StrongseihasfractbltRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcA6StrongseihasfractbltList) && hddcA6StrongseihasfractbltList.size() > 0) {
            for(HddcA6StrongseihasfractbltEntity hddcA6Strongseihasfractblt : hddcA6StrongseihasfractbltList) {
                this.hddcA6StrongseihasfractbltRepository.delete(hddcA6Strongseihasfractblt);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcA6StrongseihasfractbltQueryParams queryParams, HttpServletResponse response) {
        List<HddcA6StrongseihasfractbltEntity> yhDisasterEntities = hddcA6StrongseihasfractbltNativeRepository.exportYhDisasters(queryParams);
        List<HddcA6StrongseihasfractbltVO> list=new ArrayList<>();
        for (HddcA6StrongseihasfractbltEntity entity:yhDisasterEntities) {
            HddcA6StrongseihasfractbltVO yhDisasterVO=new HddcA6StrongseihasfractbltVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"强震与地表破裂关联表","强震与地表破裂关联表",HddcA6StrongseihasfractbltVO.class,"强震与地表破裂关联表.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcA6StrongseihasfractbltVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcA6StrongseihasfractbltVO.class, params);
            List<HddcA6StrongseihasfractbltVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcA6StrongseihasfractbltVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcA6StrongseihasfractbltVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcA6StrongseihasfractbltVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcA6StrongseihasfractbltEntity yhDisasterEntity = new HddcA6StrongseihasfractbltEntity();
            HddcA6StrongseihasfractbltVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcA6Strongseihasfractblt(yhDisasterEntity);
        }
    }

}
