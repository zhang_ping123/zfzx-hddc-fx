package com.css.zfzx.sjcj.modules.hddcwyGeomorStation.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyGeomorStation.repository.entity.HddcWyGeomorstationEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorStation.viewobjects.HddcWyGeomorstationQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-01
 */
public interface HddcWyGeomorstationNativeRepository {

    Page<HddcWyGeomorstationEntity> queryHddcWyGeomorstations(HddcWyGeomorstationQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyGeomorstation(HddcAppZztCountVo queryParams);

    List<HddcWyGeomorstationEntity> exportStation(HddcAppZztCountVo queryParams);
}
