package com.css.zfzx.sjcj.modules.hddcA6StrongSeiHasFractBlt.repository;

import com.css.zfzx.sjcj.modules.hddcA6StrongSeiHasFractBlt.repository.entity.HddcA6StrongseihasfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcA6StrongSeiHasFractBlt.viewobjects.HddcA6StrongseihasfractbltQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */
public interface HddcA6StrongseihasfractbltNativeRepository {

    Page<HddcA6StrongseihasfractbltEntity> queryHddcA6Strongseihasfractblts(HddcA6StrongseihasfractbltQueryParams queryParams, int curPage, int pageSize);

    List<HddcA6StrongseihasfractbltEntity> exportYhDisasters(HddcA6StrongseihasfractbltQueryParams queryParams);
}
