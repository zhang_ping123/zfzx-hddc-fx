package com.css.zfzx.sjcj.modules.boundary;

import lombok.Data;

import java.io.Serializable;

@Data
public class Point implements Serializable {

    public double x;
    public double y;

}
