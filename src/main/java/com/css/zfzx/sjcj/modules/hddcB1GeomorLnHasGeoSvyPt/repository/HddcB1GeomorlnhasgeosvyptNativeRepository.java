package com.css.zfzx.sjcj.modules.hddcB1GeomorLnHasGeoSvyPt.repository;

import com.css.zfzx.sjcj.modules.hddcB1GeomorLnHasGeoSvyPt.repository.entity.HddcB1GeomorlnhasgeosvyptEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnHasGeoSvyPt.viewobjects.HddcB1GeomorlnhasgeosvyptQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcB1GeomorlnhasgeosvyptNativeRepository {

    Page<HddcB1GeomorlnhasgeosvyptEntity> queryHddcB1Geomorlnhasgeosvypts(HddcB1GeomorlnhasgeosvyptQueryParams queryParams, int curPage, int pageSize);

    List<HddcB1GeomorlnhasgeosvyptEntity> exportYhDisasters(HddcB1GeomorlnhasgeosvyptQueryParams queryParams);
}
