package com.css.zfzx.sjcj.modules.hddcB5_GeophySvyDataTable.repository;

import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyDataTable.repository.entity.HddcB5GeophysvydatatableEntity;
import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyDataTable.viewobjects.HddcB5GeophysvydatatableQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
public interface HddcB5GeophysvydatatableNativeRepository {

    Page<HddcB5GeophysvydatatableEntity> queryHddcB5Geophysvydatatables(HddcB5GeophysvydatatableQueryParams queryParams, int curPage, int pageSize);

    List<HddcB5GeophysvydatatableEntity> exportYhDisasters(HddcB5GeophysvydatatableQueryParams queryParams);
}
