package com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtHasTrench.repository;

import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtHasTrench.repository.entity.HddcB1FPaleoeqevthastrenchEntity;
import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtHasTrench.viewobjects.HddcB1FPaleoeqevthastrenchQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcB1FPaleoeqevthastrenchNativeRepository {

    Page<HddcB1FPaleoeqevthastrenchEntity> queryHddcB1FPaleoeqevthastrenchs(HddcB1FPaleoeqevthastrenchQueryParams queryParams, int curPage, int pageSize);

    List<HddcB1FPaleoeqevthastrenchEntity> exportYhDisasters(HddcB1FPaleoeqevthastrenchQueryParams queryParams);
}
