package com.css.zfzx.sjcj.modules.hddcCollectedDrillHole.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Data
public class HddcCollecteddrillholeQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

}
