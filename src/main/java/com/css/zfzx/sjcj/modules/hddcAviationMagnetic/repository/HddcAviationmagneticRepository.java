package com.css.zfzx.sjcj.modules.hddcAviationMagnetic.repository;

import com.css.zfzx.sjcj.modules.hddcAviationMagnetic.repository.entity.HddcAviationmagneticEntity;
import com.css.zfzx.sjcj.modules.hddcAviationMagnetic.viewobjects.HddcAviationmagneticQueryParams;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-26
 */
public interface HddcAviationmagneticRepository extends JpaRepository<HddcAviationmagneticEntity, String> {
}
