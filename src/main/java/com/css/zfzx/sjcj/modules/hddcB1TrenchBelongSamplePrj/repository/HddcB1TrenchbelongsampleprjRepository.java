package com.css.zfzx.sjcj.modules.hddcB1TrenchBelongSamplePrj.repository;

import com.css.zfzx.sjcj.modules.hddcB1TrenchBelongSamplePrj.repository.entity.HddcB1TrenchbelongsampleprjEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcB1TrenchbelongsampleprjRepository extends JpaRepository<HddcB1TrenchbelongsampleprjEntity, String> {
}
