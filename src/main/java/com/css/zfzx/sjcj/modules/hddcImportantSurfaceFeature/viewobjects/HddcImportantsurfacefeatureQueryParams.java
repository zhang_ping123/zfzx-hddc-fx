package com.css.zfzx.sjcj.modules.hddcImportantSurfaceFeature.viewobjects;

import lombok.Data;

/**
 * @author lihelei
 * @date 2020-11-30
 */
@Data
public class HddcImportantsurfacefeatureQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String name;

}
