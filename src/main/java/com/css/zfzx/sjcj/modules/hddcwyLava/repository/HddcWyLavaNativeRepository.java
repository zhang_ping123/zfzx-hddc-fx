package com.css.zfzx.sjcj.modules.hddcwyLava.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyLava.repository.entity.HddcWyLavaEntity;
import com.css.zfzx.sjcj.modules.hddcwyLava.viewobjects.HddcWyLavaQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-02
 */
public interface HddcWyLavaNativeRepository {

    Page<HddcWyLavaEntity> queryHddcWyLavas(HddcWyLavaQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyLava(HddcAppZztCountVo queryParams);

    List<HddcWyLavaEntity> exportLava(HddcAppZztCountVo queryParams);
}
