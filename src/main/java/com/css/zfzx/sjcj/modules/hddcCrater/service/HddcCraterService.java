package com.css.zfzx.sjcj.modules.hddcCrater.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcCrater.repository.entity.HddcCraterEntity;
import com.css.zfzx.sjcj.modules.hddcCrater.viewobjects.HddcCraterQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-27
 */

public interface HddcCraterService {

    public JSONObject queryHddcCraters(HddcCraterQueryParams queryParams, int curPage, int pageSize);

    public HddcCraterEntity getHddcCrater(String id);

    public HddcCraterEntity saveHddcCrater(HddcCraterEntity hddcCrater);

    public HddcCraterEntity updateHddcCrater(HddcCraterEntity hddcCrater);

    public void deleteHddcCraters(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcCraterQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
