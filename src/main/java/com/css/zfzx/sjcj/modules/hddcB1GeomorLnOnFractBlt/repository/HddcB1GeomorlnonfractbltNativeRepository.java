package com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository;

import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcB1GeomorlnonfractbltNativeRepository {

    Page<HddcB1GeomorlnonfractbltEntity> queryHddcB1Geomorlnonfractblts(HddcB1GeomorlnonfractbltQueryParams queryParams, int curPage, int pageSize);

    List<HddcB1GeomorlnonfractbltEntity> exportYhDisasters(HddcB1GeomorlnonfractbltQueryParams queryParams);
}
