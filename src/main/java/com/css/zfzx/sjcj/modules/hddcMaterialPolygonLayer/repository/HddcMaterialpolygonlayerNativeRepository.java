package com.css.zfzx.sjcj.modules.hddcMaterialPolygonLayer.repository;

import com.css.zfzx.sjcj.modules.hddcMaterialPolygonLayer.repository.entity.HddcMaterialpolygonlayerEntity;
import com.css.zfzx.sjcj.modules.hddcMaterialPolygonLayer.viewobjects.HddcMaterialpolygonlayerQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-12-03
 */
public interface HddcMaterialpolygonlayerNativeRepository {

    Page<HddcMaterialpolygonlayerEntity> queryHddcMaterialpolygonlayers(HddcMaterialpolygonlayerQueryParams queryParams, int curPage, int pageSize);

    List<HddcMaterialpolygonlayerEntity> exportYhDisasters(HddcMaterialpolygonlayerQueryParams queryParams);
}
