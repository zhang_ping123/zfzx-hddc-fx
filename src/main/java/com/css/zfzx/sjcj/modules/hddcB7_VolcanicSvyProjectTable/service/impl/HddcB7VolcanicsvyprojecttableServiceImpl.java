package com.css.zfzx.sjcj.modules.hddcB7_VolcanicSvyProjectTable.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB7_VolcanicSvyProjectTable.repository.HddcB7VolcanicsvyprojecttableNativeRepository;
import com.css.zfzx.sjcj.modules.hddcB7_VolcanicSvyProjectTable.repository.HddcB7VolcanicsvyprojecttableRepository;
import com.css.zfzx.sjcj.modules.hddcB7_VolcanicSvyProjectTable.repository.entity.HddcB7VolcanicsvyprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB7_VolcanicSvyProjectTable.service.HddcB7VolcanicsvyprojecttableService;
import com.css.zfzx.sjcj.modules.hddcB7_VolcanicSvyProjectTable.viewobjects.HddcB7VolcanicsvyprojecttableQueryParams;
import com.css.zfzx.sjcj.modules.hddcB7_VolcanicSvyProjectTable.viewobjects.HddcB7VolcanicsvyprojecttableVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-26
 */
@Service
public class HddcB7VolcanicsvyprojecttableServiceImpl implements HddcB7VolcanicsvyprojecttableService {

	@Autowired
    private HddcB7VolcanicsvyprojecttableRepository hddcB7VolcanicsvyprojecttableRepository;
    @Autowired
    private HddcB7VolcanicsvyprojecttableNativeRepository hddcB7VolcanicsvyprojecttableNativeRepository;

    @Override
    public JSONObject queryHddcB7Volcanicsvyprojecttables(HddcB7VolcanicsvyprojecttableQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcB7VolcanicsvyprojecttableEntity> hddcB7VolcanicsvyprojecttablePage = this.hddcB7VolcanicsvyprojecttableNativeRepository.queryHddcB7Volcanicsvyprojecttables(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcB7VolcanicsvyprojecttablePage);
        return jsonObject;
    }


    @Override
    public HddcB7VolcanicsvyprojecttableEntity getHddcB7Volcanicsvyprojecttable(String id) {
        HddcB7VolcanicsvyprojecttableEntity hddcB7Volcanicsvyprojecttable = this.hddcB7VolcanicsvyprojecttableRepository.findById(id).orElse(null);
         return hddcB7Volcanicsvyprojecttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB7VolcanicsvyprojecttableEntity saveHddcB7Volcanicsvyprojecttable(HddcB7VolcanicsvyprojecttableEntity hddcB7Volcanicsvyprojecttable) {
        String uuid = UUIDGenerator.getUUID();
        hddcB7Volcanicsvyprojecttable.setUuid(uuid);
        hddcB7Volcanicsvyprojecttable.setCreateUser(PlatformSessionUtils.getUserId());
        hddcB7Volcanicsvyprojecttable.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB7VolcanicsvyprojecttableRepository.save(hddcB7Volcanicsvyprojecttable);
        return hddcB7Volcanicsvyprojecttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB7VolcanicsvyprojecttableEntity updateHddcB7Volcanicsvyprojecttable(HddcB7VolcanicsvyprojecttableEntity hddcB7Volcanicsvyprojecttable) {
        HddcB7VolcanicsvyprojecttableEntity entity = hddcB7VolcanicsvyprojecttableRepository.findById(hddcB7Volcanicsvyprojecttable.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcB7Volcanicsvyprojecttable);
        hddcB7Volcanicsvyprojecttable.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcB7Volcanicsvyprojecttable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB7VolcanicsvyprojecttableRepository.save(hddcB7Volcanicsvyprojecttable);
        return hddcB7Volcanicsvyprojecttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcB7Volcanicsvyprojecttables(List<String> ids) {
        List<HddcB7VolcanicsvyprojecttableEntity> hddcB7VolcanicsvyprojecttableList = this.hddcB7VolcanicsvyprojecttableRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcB7VolcanicsvyprojecttableList) && hddcB7VolcanicsvyprojecttableList.size() > 0) {
            for(HddcB7VolcanicsvyprojecttableEntity hddcB7Volcanicsvyprojecttable : hddcB7VolcanicsvyprojecttableList) {
                this.hddcB7VolcanicsvyprojecttableRepository.delete(hddcB7Volcanicsvyprojecttable);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcB7VolcanicsvyprojecttableQueryParams queryParams, HttpServletResponse response) {
        List<HddcB7VolcanicsvyprojecttableEntity> yhDisasterEntities = hddcB7VolcanicsvyprojecttableNativeRepository.exportYhDisasters(queryParams);
        List<HddcB7VolcanicsvyprojecttableVO> list=new ArrayList<>();
        for (HddcB7VolcanicsvyprojecttableEntity entity:yhDisasterEntities) {
            HddcB7VolcanicsvyprojecttableVO yhDisasterVO=new HddcB7VolcanicsvyprojecttableVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"火山化学测量工程表","火山化学测量工程表",HddcB7VolcanicsvyprojecttableVO.class,"火山化学测量工程表.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcB7VolcanicsvyprojecttableVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcB7VolcanicsvyprojecttableVO.class, params);
            List<HddcB7VolcanicsvyprojecttableVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcB7VolcanicsvyprojecttableVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcB7VolcanicsvyprojecttableVO yhDisasterVO = iterator.next();
                    String error = "";
                    /*returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");*/
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcB7VolcanicsvyprojecttableVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcB7VolcanicsvyprojecttableEntity yhDisasterEntity = new HddcB7VolcanicsvyprojecttableEntity();
            HddcB7VolcanicsvyprojecttableVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcB7Volcanicsvyprojecttable(yhDisasterEntity);
        }
    }
}
