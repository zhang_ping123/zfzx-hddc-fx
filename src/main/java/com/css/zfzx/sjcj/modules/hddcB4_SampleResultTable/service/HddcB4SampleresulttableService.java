package com.css.zfzx.sjcj.modules.hddcB4_SampleResultTable.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB4_SampleResultTable.repository.entity.HddcB4SampleresulttableEntity;
import com.css.zfzx.sjcj.modules.hddcB4_SampleResultTable.viewobjects.HddcB4SampleresulttableQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */

public interface HddcB4SampleresulttableService {

    public JSONObject queryHddcB4Sampleresulttables(HddcB4SampleresulttableQueryParams queryParams, int curPage, int pageSize);

    public HddcB4SampleresulttableEntity getHddcB4Sampleresulttable(String id);

    public HddcB4SampleresulttableEntity saveHddcB4Sampleresulttable(HddcB4SampleresulttableEntity hddcB4Sampleresulttable);

    public HddcB4SampleresulttableEntity updateHddcB4Sampleresulttable(HddcB4SampleresulttableEntity hddcB4Sampleresulttable);

    public void deleteHddcB4Sampleresulttables(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB4SampleresulttableQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
