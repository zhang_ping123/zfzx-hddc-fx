package com.css.zfzx.sjcj.modules.hddcEpiMechanismResult.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcEpiMechanismResult.repository.entity.HddcEpimechanismresultEntity;
import com.css.zfzx.sjcj.modules.hddcEpiMechanismResult.viewobjects.HddcEpimechanismresultQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */

public interface HddcEpimechanismresultService {

    public JSONObject queryHddcEpimechanismresults(HddcEpimechanismresultQueryParams queryParams, int curPage, int pageSize);

    public HddcEpimechanismresultEntity getHddcEpimechanismresult(String id);

    public HddcEpimechanismresultEntity saveHddcEpimechanismresult(HddcEpimechanismresultEntity hddcEpimechanismresult);

    public HddcEpimechanismresultEntity updateHddcEpimechanismresult(HddcEpimechanismresultEntity hddcEpimechanismresult);

    public void deleteHddcEpimechanismresults(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcEpimechanismresultQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
