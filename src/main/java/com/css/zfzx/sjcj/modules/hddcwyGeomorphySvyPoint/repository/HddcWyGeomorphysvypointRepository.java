package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.repository.entity.HddcWyGeomorphysvypointEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author lihelei
 * @date 2020-12-01
 */
public interface HddcWyGeomorphysvypointRepository extends JpaRepository<HddcWyGeomorphysvypointEntity, String> {

    List<HddcWyGeomorphysvypointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    List<HddcWyGeomorphysvypointEntity> findAllByCreateUserAndIsValid(String userId,String isValid);

    @Query(nativeQuery = true, value = "select * from hddc_wy_geomorphysvypoint where is_valid!=0 project_name in :projectIds")
    List<HddcWyGeomorphysvypointEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_geomorphysvypoint where task_name in :projectIds")
    List<HddcWyGeomorphysvypointEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);

}
