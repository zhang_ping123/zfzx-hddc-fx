package com.css.zfzx.sjcj.modules.community.repository.impl;

import com.css.zfzx.sjcj.modules.community.repository.YhCommunityNativeRepository;
import com.css.zfzx.sjcj.modules.community.repository.entity.YhCommunityEntity;
import com.css.zfzx.sjcj.modules.community.viewobjects.YhCommunityQueryParams;
import com.css.bpm.platform.utils.PlatformObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
/**
 * @author yyd
 * @date 2020-11-04
 */
@Repository
public class YhCommunityNativeRepositoryImpl implements YhCommunityNativeRepository {
    @PersistenceContext
    private EntityManager em;

    private static final Logger log = LoggerFactory.getLogger(YhCommunityNativeRepositoryImpl.class);


    @Override
    public Page<YhCommunityEntity> queryYhCommunitys(YhCommunityQueryParams queryParams, int curPage, int pageSize) {
        StringBuilder sql = new StringBuilder("select * from yh_community ");
        StringBuilder whereSql = new StringBuilder(" where 1=1 ");
        if(!PlatformObjectUtils.isEmpty(queryParams.getCztName())) {
            whereSql.append(" and czt_name = :cztName");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getProvince())) {
            whereSql.append(" and province = :province");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getArea())) {
            whereSql.append(" and area = :area");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getCity())) {
            whereSql.append(" and city = :city");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getBuildingLevel())) {
            whereSql.append(" and building_level = :buildingLevel");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getYhLevel())) {
            whereSql.append(" and yh_level = :yhLevel");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getStatus())) {
            whereSql.append(" and status = :status");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getCreateTime())) {
            whereSql.append(" and create_time = :createTime");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getCreateUser())) {
            whereSql.append(" and create_user = :createUser");
        }
        Query query = this.em.createNativeQuery(sql.append(whereSql).toString(), YhCommunityEntity.class);
        StringBuilder countSql = new StringBuilder("select count(*) from yh_community ");
        Query countQuery = this.em.createNativeQuery(countSql.append(whereSql).toString());
        if(!PlatformObjectUtils.isEmpty(queryParams.getCztName())) {
            query.setParameter("cztName", queryParams.getCztName());
            countQuery.setParameter("cztName", queryParams.getCztName());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getProvince())) {
            query.setParameter("province", queryParams.getProvince());
            countQuery.setParameter("province", queryParams.getProvince());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getArea())) {
            query.setParameter("area", queryParams.getArea());
            countQuery.setParameter("area", queryParams.getArea());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getCity())) {
            query.setParameter("city", queryParams.getCity());
            countQuery.setParameter("city", queryParams.getCity());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getBuildingLevel())) {
            query.setParameter("buildingLevel", queryParams.getBuildingLevel());
            countQuery.setParameter("buildingLevel", queryParams.getBuildingLevel());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getYhLevel())) {
            query.setParameter("yhLevel", queryParams.getYhLevel());
            countQuery.setParameter("yhLevel", queryParams.getYhLevel());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getStatus())) {
            query.setParameter("status", queryParams.getStatus());
            countQuery.setParameter("status", queryParams.getStatus());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getCreateTime())) {
            query.setParameter("createTime", queryParams.getCreateTime());
            countQuery.setParameter("createTime", queryParams.getCreateTime());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getCreateUser())) {
            query.setParameter("createUser", queryParams.getCreateUser());
            countQuery.setParameter("createUser", queryParams.getCreateUser());
        }
        Pageable pageable = PageRequest.of(curPage - 1, pageSize);
        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());
        List<YhCommunityEntity> list = query.getResultList();
        BigInteger count = (BigInteger) countQuery.getSingleResult();
        Page<YhCommunityEntity> yhCommunityPage = new PageImpl<>(list, pageable, count.longValue());
        return yhCommunityPage;
    }
}
