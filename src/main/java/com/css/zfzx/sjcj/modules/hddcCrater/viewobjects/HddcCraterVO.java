package com.css.zfzx.sjcj.modules.hddcCrater.viewobjects;

import com.fasterxml.jackson.annotation.JsonFormat;
import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
@Data
public class HddcCraterVO implements Serializable {

    /**
     * 外坡度
     */
    @Excel(name = "外坡度", orderNum = "4")
    private Double outsideslopeangle;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 堆积物类型
     */
    @Excel(name = "堆积物类型", orderNum = "5")
    private String deposittype;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 拍摄者
     */
    @Excel(name = "拍摄者", orderNum = "6")
    private String photographer;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 岩石包体数量
     */
    @Excel(name = "岩石包体数量", orderNum = "7")
    private Integer rockinclusionnum;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 锥体结构组成剖面图图片
     */
    @Excel(name = "锥体结构组成剖面图图片", orderNum = "8")
    private String conestructureprofileAiid;
    /**
     * 锥体名称
     */
    @Excel(name = "锥体名称", orderNum = "9")
    private String conename;
    /**
     * 火口垣直径
     */
    @Excel(name = "火口垣直径", orderNum = "10")
    private Double craterwallsdiameter;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备注
     */
    private String remark;
    /**
     * 内坡度
     */
    @Excel(name = "内坡度", orderNum = "11")
    private Double insideslopeangle;
    /**
     * 照片集镜向及拍摄者说明文档
     */
    @Excel(name = "照片集镜向及拍摄者说明文档", orderNum = "12")
    private String photodescArwid;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 塑性熔岩饼单体尺寸
     */
    @Excel(name = "塑性熔岩饼单体尺寸", orderNum = "13")
    private Double lavadribletsize;
    /**
     * 堆积物粒度
     */
    @Excel(name = "堆积物粒度", orderNum = "14")
    private String depositgranularity;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 火口直径
     */
    @Excel(name = "火口直径", orderNum = "15")
    private Double craterdiameter;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 锥体高度[米]
     */
    @Excel(name = "锥体高度[米]", orderNum = "16")
    private Double coneheight;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 锥体底部直径
     */
    @Excel(name = "锥体底部直径", orderNum = "17")
    private Double bottomdiameter;
    /**
     * 溢出口方向
     */
    @Excel(name = "溢出口方向", orderNum = "18")
    private Integer overfalldirection;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 火口深度[米]
     */
    @Excel(name = "火口深度[米]", orderNum = "19")
    private Double craterdepth;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 照片原始文件编号
     */
    @Excel(name = "照片原始文件编号", orderNum = "20")
    private String photoArwid;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 岩石包体形状
     */
    @Excel(name = "岩石包体形状", orderNum = "21")
    private String rockinclusionshape;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 锥体形态
     */
    @Excel(name = "锥体形态", orderNum = "22")
    private String conemorphology;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 素描图图像
     */
    @Excel(name = "素描图图像", orderNum = "23")
    private String sketchAiid;
    /**
     * 岩石包体类型
     */
    @Excel(name = "岩石包体类型", orderNum = "24")
    private String rockinclusiontype;
    /**
     * DepositThickness
     */
    @Excel(name = "DepositThickness", orderNum = "25")
    private Double depositthickness;
    /**
     * 素描图原始文件
     */
    @Excel(name = "素描图原始文件", orderNum = "26")
    private String sketchArwid;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 岩石包体产出状态
     */
    @Excel(name = "岩石包体产出状态", orderNum = "27")
    private String rockinclusionoutputstate;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 锥体类型
     */
    @Excel(name = "锥体类型", orderNum = "28")
    private Integer conetype;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 岩石包体粒度
     */
    @Excel(name = "岩石包体粒度", orderNum = "29")
    private String rockinclusiongranularity;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "30")
    private String commentInfo;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 火山口编号
     */
    @Excel(name = "火山口编号", orderNum = "31")
    private String id;
    /**
     * 村
     */
    private String village;
    /**
     * 照片文件编号
     */
    @Excel(name = "照片文件编号", orderNum = "32")
    private String photoAiid;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 锥体结构组成剖面图原始文件
     */
    @Excel(name = "锥体结构组成剖面图原始文件", orderNum = "33")
    private String conestructureprofileArwid;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer overfalldirectionName;
}