package com.css.zfzx.sjcj.modules.hddcVolcanicSamplePoint.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
@Data
public class HddcVolcanicsamplepointVO implements Serializable {

    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 采样点编号
     */
    private String id;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 野外编号
     */
    @Excel(name = "野外编号", orderNum = "4")
    private String fieldid;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 标注名称
     */
    @Excel(name = "标注名称", orderNum = "5")
    private String labelinfo;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "6")
    private String commentInfo;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 火山调查观测点编号
     */
    @Excel(name = "火山调查观测点编号", orderNum = "7")
    private String volcanicsvypointid;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 乡
     */
    private String town;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 采样点符号
     */
    @Excel(name = "采样点符号", orderNum = "8")
    private Integer symbolinfo;
    /**
     * 是否单次采样
     */
    @Excel(name = "是否单次采样", orderNum = "9")
    private Integer issinglesample;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer symbolinfoName;
    private Integer issinglesampleName;
    private String rowNum;
    private String errorMsg;
}