package com.css.zfzx.sjcj.modules.hddcC4MagnitudeEstimateTable.repository.entity;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangping
 * @date 2020-11-23
 */
@Data
@Entity
@Table(name="hddc_c4_magnitudeestimatetable")
public class HddcC4MagnitudeestimatetableEntity implements Serializable {

    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 震级上限
     */
    @Column(name="MaxM")
    private Double maxm;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 备注
     */
    @Column(name="CommentInfo")
    private String commentinfo;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 权重
     */
    @Column(name="MWeight")
    private Double mweight;
    /**
     * 断层分段编号
     */
    @Column(name="SegmentID")
    private String segmentid;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 震级上限误差
     */
    @Column(name="MaxMEr")
    private Double maxmer;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 断层编号
     */
    @Id
    @Column(name="FaultID")
    private String faultid;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 震级估计方法
     */
    @Column(name="MagnitudeMethod")
    private String magnitudemethod;

}

