package com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningPt.repository;

import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningPt.repository.entity.HddcGeologicalsvyplanningptEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-12-07
 */
public interface HddcGeologicalsvyplanningptRepository extends JpaRepository<HddcGeologicalsvyplanningptEntity, String> {
}
