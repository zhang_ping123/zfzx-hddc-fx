package com.css.zfzx.sjcj.modules.hddcA1InvRgnHasMaterialTable.service;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.zfzx.sjcj.modules.hddcA1InvRgnHasMaterialTable.repository.entity.HddcA1InvrgnhasmaterialtableEntity;
import com.css.zfzx.sjcj.modules.hddcA1InvRgnHasMaterialTable.viewobjects.HddcA1InvrgnhasmaterialtableQueryParams;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */

public interface HddcA1InvrgnhasmaterialtableService {

    public JSONObject queryHddcA1Invrgnhasmaterialtables(HddcA1InvrgnhasmaterialtableQueryParams queryParams, int curPage, int pageSize);

    public HddcA1InvrgnhasmaterialtableEntity getHddcA1Invrgnhasmaterialtable(String id);

    public HddcA1InvrgnhasmaterialtableEntity saveHddcA1Invrgnhasmaterialtable(HddcA1InvrgnhasmaterialtableEntity hddcA1Invrgnhasmaterialtable);

    public HddcA1InvrgnhasmaterialtableEntity updateHddcA1Invrgnhasmaterialtable(HddcA1InvrgnhasmaterialtableEntity hddcA1Invrgnhasmaterialtable);

    public void deleteHddcA1Invrgnhasmaterialtables(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcA1InvrgnhasmaterialtableQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
