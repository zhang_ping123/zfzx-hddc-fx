package com.css.zfzx.sjcj.modules.hddcGeologicalSvyLine.viewobjects;

import lombok.Data;

/**
 * @author lihelei
 * @date 2020-11-27
 */
@Data
public class HddcGeologicalsvylineQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String svylinename;

}
