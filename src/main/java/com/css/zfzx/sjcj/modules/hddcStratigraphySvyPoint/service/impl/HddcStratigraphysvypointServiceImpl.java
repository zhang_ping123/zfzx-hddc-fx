package com.css.zfzx.sjcj.modules.hddcStratigraphySvyPoint.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltVO;
import com.css.zfzx.sjcj.modules.hddcStratigraphySvyPoint.repository.HddcStratigraphysvypointNativeRepository;
import com.css.zfzx.sjcj.modules.hddcStratigraphySvyPoint.repository.HddcStratigraphysvypointRepository;
import com.css.zfzx.sjcj.modules.hddcStratigraphySvyPoint.repository.entity.HddcStratigraphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcStratigraphySvyPoint.service.HddcStratigraphysvypointService;
import com.css.zfzx.sjcj.modules.hddcStratigraphySvyPoint.viewobjects.HddcStratigraphysvypointQueryParams;
import com.css.zfzx.sjcj.modules.hddcStratigraphySvyPoint.viewobjects.HddcStratigraphysvypointVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author lihelei
 * @date 2020-11-27
 */
@Service
public class HddcStratigraphysvypointServiceImpl implements HddcStratigraphysvypointService {

	@Autowired
    private HddcStratigraphysvypointRepository hddcStratigraphysvypointRepository;
    @Autowired
    private HddcStratigraphysvypointNativeRepository hddcStratigraphysvypointNativeRepository;

    @Override
    public JSONObject queryHddcStratigraphysvypoints(HddcStratigraphysvypointQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcStratigraphysvypointEntity> hddcStratigraphysvypointPage = this.hddcStratigraphysvypointNativeRepository.queryHddcStratigraphysvypoints(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcStratigraphysvypointPage);
        return jsonObject;
    }


    @Override
    public HddcStratigraphysvypointEntity getHddcStratigraphysvypoint(String id) {
        HddcStratigraphysvypointEntity hddcStratigraphysvypoint = this.hddcStratigraphysvypointRepository.findById(id).orElse(null);
         return hddcStratigraphysvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcStratigraphysvypointEntity saveHddcStratigraphysvypoint(HddcStratigraphysvypointEntity hddcStratigraphysvypoint) {
        String uuid = UUIDGenerator.getUUID();
        hddcStratigraphysvypoint.setUuid(uuid);
        hddcStratigraphysvypoint.setCreateUser(PlatformSessionUtils.getUserId());
        hddcStratigraphysvypoint.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcStratigraphysvypointRepository.save(hddcStratigraphysvypoint);
        return hddcStratigraphysvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcStratigraphysvypointEntity updateHddcStratigraphysvypoint(HddcStratigraphysvypointEntity hddcStratigraphysvypoint) {
        HddcStratigraphysvypointEntity entity = hddcStratigraphysvypointRepository.findById(hddcStratigraphysvypoint.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcStratigraphysvypoint);
        hddcStratigraphysvypoint.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcStratigraphysvypoint.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcStratigraphysvypointRepository.save(hddcStratigraphysvypoint);
        return hddcStratigraphysvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcStratigraphysvypoints(List<String> ids) {
        List<HddcStratigraphysvypointEntity> hddcStratigraphysvypointList = this.hddcStratigraphysvypointRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcStratigraphysvypointList) && hddcStratigraphysvypointList.size() > 0) {
            for(HddcStratigraphysvypointEntity hddcStratigraphysvypoint : hddcStratigraphysvypointList) {
                this.hddcStratigraphysvypointRepository.delete(hddcStratigraphysvypoint);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcStratigraphysvypointQueryParams queryParams, HttpServletResponse response) {
        List<HddcStratigraphysvypointEntity> yhDisasterEntities = hddcStratigraphysvypointNativeRepository.exportYhDisasters(queryParams);
        List<HddcStratigraphysvypointVO> list=new ArrayList<>();
        for (HddcStratigraphysvypointEntity entity:yhDisasterEntities) {
            HddcStratigraphysvypointVO yhDisasterVO=new HddcStratigraphysvypointVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"地层观测点-点","地层观测点-点",HddcStratigraphysvypointVO.class,"地层观测点-点.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcStratigraphysvypointVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcStratigraphysvypointVO.class, params);
            List<HddcStratigraphysvypointVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcStratigraphysvypointVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcStratigraphysvypointVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcStratigraphysvypointVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcStratigraphysvypointEntity yhDisasterEntity = new HddcStratigraphysvypointEntity();
            HddcStratigraphysvypointVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcStratigraphysvypoint(yhDisasterEntity);
        }
    }

}
