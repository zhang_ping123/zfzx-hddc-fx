package com.css.zfzx.sjcj.modules.hddcGravityField.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-26
 */
@Data
public class HddcGravityfieldQueryParams {


    private String province;
    private String city;
    private String area;
    private String gravityname;
    private String projectName;

}
