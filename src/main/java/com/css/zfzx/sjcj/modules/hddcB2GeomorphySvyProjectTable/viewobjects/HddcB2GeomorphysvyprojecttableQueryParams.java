package com.css.zfzx.sjcj.modules.hddcB2GeomorphySvyProjectTable.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-12-07
 */
@Data
public class HddcB2GeomorphysvyprojecttableQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String taskName;
    private String name;

}
