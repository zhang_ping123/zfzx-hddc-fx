package com.css.zfzx.sjcj.modules.hddcRock1Pre.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcRock1Pre.repository.entity.HddcRock1preEntity;
import com.css.zfzx.sjcj.modules.hddcRock1Pre.viewobjects.HddcRock1preQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */

public interface HddcRock1preService {

    public JSONObject queryHddcRock1pres(HddcRock1preQueryParams queryParams, int curPage, int pageSize);

    public HddcRock1preEntity getHddcRock1pre(String id);

    public HddcRock1preEntity saveHddcRock1pre(HddcRock1preEntity hddcRock1pre);

    public HddcRock1preEntity updateHddcRock1pre(HddcRock1preEntity hddcRock1pre);

    public void deleteHddcRock1pres(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    String findByDictCodeAndDictItemCode(String dictCode, String dictItemCode);

    void exportFile(HddcRock1preQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
