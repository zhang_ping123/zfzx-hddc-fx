package com.css.zfzx.sjcj.modules.hddcTrench.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Data
public class HddcTrenchQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String name;

}
