package com.css.zfzx.sjcj.modules.yhschool.repository;

import com.css.zfzx.sjcj.modules.yhschool.repository.entity.YhSchoolEntity;
import com.css.zfzx.sjcj.modules.yhschool.viewobjects.YhSchoolQueryParams;
import org.springframework.data.domain.Page;

/**
 * @author yyd
 * @date 2020-11-03
 */
public interface YhSchoolNativeRepository {

    Page<YhSchoolEntity> queryYhSchools(YhSchoolQueryParams queryParams, int curPage, int pageSize);
}
