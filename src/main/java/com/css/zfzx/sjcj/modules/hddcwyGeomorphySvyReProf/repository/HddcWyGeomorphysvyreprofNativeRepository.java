package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyReProf.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyReProf.repository.entity.HddcWyGeomorphysvyreprofEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyReProf.viewobjects.HddcWyGeomorphysvyreprofQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;
import java.util.List;

/**
 * @author lihelei
 * @date 2020-12-01
 */
public interface HddcWyGeomorphysvyreprofNativeRepository {

    Page<HddcWyGeomorphysvyreprofEntity> queryHddcWyGeomorphysvyreprofs(HddcWyGeomorphysvyreprofQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyGeomorphysvyreprof(HddcAppZztCountVo queryParams);

    List<HddcWyGeomorphysvyreprofEntity> exportReprof(HddcAppZztCountVo queryParams);
}
