package com.css.zfzx.sjcj.modules.hddcGeomorphySvySamplePoint.repository;

import com.css.zfzx.sjcj.modules.hddcGeomorphySvySamplePoint.repository.entity.HddcGeomorphysvysamplepointEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcGeomorphysvysamplepointRepository extends JpaRepository<HddcGeomorphysvysamplepointEntity, String> {
}
