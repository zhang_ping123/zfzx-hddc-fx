package com.css.zfzx.sjcj.modules.hddcwyGeoGeomorphySvyPoint.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyGeoGeomorphySvyPoint.repository.entity.HddcWyGeogeomorphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeoGeomorphySvyPoint.viewobjects.HddcWyGeogeomorphysvypointQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */
public interface HddcWyGeogeomorphysvypointNativeRepository {

    Page<HddcWyGeogeomorphysvypointEntity> queryHddcWyGeogeomorphysvypoints(HddcWyGeogeomorphysvypointQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyGeogeomorphysvypoint(HddcAppZztCountVo queryParams);

    List<HddcWyGeogeomorphysvypointEntity> exportGeoPoint(HddcAppZztCountVo queryParams);
}
