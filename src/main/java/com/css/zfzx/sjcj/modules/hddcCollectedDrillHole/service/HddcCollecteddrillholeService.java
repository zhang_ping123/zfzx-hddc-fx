package com.css.zfzx.sjcj.modules.hddcCollectedDrillHole.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcCollectedDrillHole.repository.entity.HddcCollecteddrillholeEntity;
import com.css.zfzx.sjcj.modules.hddcCollectedDrillHole.viewobjects.HddcCollecteddrillholeQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */

public interface HddcCollecteddrillholeService {

    public JSONObject queryHddcCollecteddrillholes(HddcCollecteddrillholeQueryParams queryParams, int curPage, int pageSize);

    public HddcCollecteddrillholeEntity getHddcCollecteddrillhole(String id);

    public HddcCollecteddrillholeEntity saveHddcCollecteddrillhole(HddcCollecteddrillholeEntity hddcCollecteddrillhole);

    public HddcCollecteddrillholeEntity updateHddcCollecteddrillhole(HddcCollecteddrillholeEntity hddcCollecteddrillhole);

    public void deleteHddcCollecteddrillholes(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcCollecteddrillholeQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
