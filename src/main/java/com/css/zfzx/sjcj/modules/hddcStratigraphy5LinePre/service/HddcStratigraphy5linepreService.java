package com.css.zfzx.sjcj.modules.hddcStratigraphy5LinePre.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcStratigraphy5LinePre.repository.entity.HddcStratigraphy5linepreEntity;
import com.css.zfzx.sjcj.modules.hddcStratigraphy5LinePre.viewobjects.HddcStratigraphy5linepreQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */

public interface HddcStratigraphy5linepreService {

    public JSONObject queryHddcStratigraphy5linepres(HddcStratigraphy5linepreQueryParams queryParams, int curPage, int pageSize);

    public HddcStratigraphy5linepreEntity getHddcStratigraphy5linepre(String id);

    public HddcStratigraphy5linepreEntity saveHddcStratigraphy5linepre(HddcStratigraphy5linepreEntity hddcStratigraphy5linepre);

    public HddcStratigraphy5linepreEntity updateHddcStratigraphy5linepre(HddcStratigraphy5linepreEntity hddcStratigraphy5linepre);

    public void deleteHddcStratigraphy5linepres(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    String findByDictCodeAndDictItemCode(String dictCode, String dictItemCode);

    void exportFile(HddcStratigraphy5linepreQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
