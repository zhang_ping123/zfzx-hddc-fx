package com.css.zfzx.sjcj.modules.hddcRock25LinePre.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltVO;
import com.css.zfzx.sjcj.modules.hddcRock25LinePre.repository.HddcRock25linepreNativeRepository;
import com.css.zfzx.sjcj.modules.hddcRock25LinePre.repository.HddcRock25linepreRepository;
import com.css.zfzx.sjcj.modules.hddcRock25LinePre.repository.entity.HddcRock25linepreEntity;
import com.css.zfzx.sjcj.modules.hddcRock25LinePre.service.HddcRock25linepreService;
import com.css.zfzx.sjcj.modules.hddcRock25LinePre.viewobjects.HddcRock25linepreQueryParams;
import com.css.zfzx.sjcj.modules.hddcRock25LinePre.viewobjects.HddcRock25linepreVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Service
public class HddcRock25linepreServiceImpl implements HddcRock25linepreService {

	@Autowired
    private HddcRock25linepreRepository hddcRock25linepreRepository;
    @Autowired
    private HddcRock25linepreNativeRepository hddcRock25linepreNativeRepository;

    @Override
    public JSONObject queryHddcRock25linepres(HddcRock25linepreQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcRock25linepreEntity> hddcRock25lineprePage = this.hddcRock25linepreNativeRepository.queryHddcRock25linepres(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcRock25lineprePage);
        return jsonObject;
    }


    @Override
    public HddcRock25linepreEntity getHddcRock25linepre(String id) {
        HddcRock25linepreEntity hddcRock25linepre = this.hddcRock25linepreRepository.findById(id).orElse(null);
         return hddcRock25linepre;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcRock25linepreEntity saveHddcRock25linepre(HddcRock25linepreEntity hddcRock25linepre) {
        String uuid = UUIDGenerator.getUUID();
        hddcRock25linepre.setUuid(uuid);
        hddcRock25linepre.setCreateUser(PlatformSessionUtils.getUserId());
        hddcRock25linepre.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcRock25linepreRepository.save(hddcRock25linepre);
        return hddcRock25linepre;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcRock25linepreEntity updateHddcRock25linepre(HddcRock25linepreEntity hddcRock25linepre) {
        HddcRock25linepreEntity entity = hddcRock25linepreRepository.findById(hddcRock25linepre.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcRock25linepre);
        hddcRock25linepre.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcRock25linepre.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcRock25linepreRepository.save(hddcRock25linepre);
        return hddcRock25linepre;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcRock25linepres(List<String> ids) {
        List<HddcRock25linepreEntity> hddcRock25linepreList = this.hddcRock25linepreRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcRock25linepreList) && hddcRock25linepreList.size() > 0) {
            for(HddcRock25linepreEntity hddcRock25linepre : hddcRock25linepreList) {
                this.hddcRock25linepreRepository.delete(hddcRock25linepre);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcRock25linepreQueryParams queryParams, HttpServletResponse response) {
        List<HddcRock25linepreEntity> yhDisasterEntities = hddcRock25linepreNativeRepository.exportYhDisasters(queryParams);
        List<HddcRock25linepreVO> list=new ArrayList<>();
        for (HddcRock25linepreEntity entity:yhDisasterEntities) {
            HddcRock25linepreVO yhDisasterVO=new HddcRock25linepreVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"1：25万底图岩体线-线","1：25万底图岩体线-线",HddcRock25linepreVO.class,"1：25万底图岩体线-线.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcRock25linepreVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcRock25linepreVO.class, params);
            List<HddcRock25linepreVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcRock25linepreVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcRock25linepreVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcRock25linepreVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcRock25linepreEntity yhDisasterEntity = new HddcRock25linepreEntity();
            HddcRock25linepreVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcRock25linepre(yhDisasterEntity);
        }
    }

}
