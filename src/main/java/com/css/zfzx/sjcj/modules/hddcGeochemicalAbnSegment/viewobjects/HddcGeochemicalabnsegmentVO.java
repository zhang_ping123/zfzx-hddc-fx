package com.css.zfzx.sjcj.modules.hddcGeochemicalAbnSegment.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Data
public class HddcGeochemicalabnsegmentVO implements Serializable {

    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 目标断层名称
     */
    @Excel(name = "目标断层名称", orderNum = "4")
    private String targetfaultname;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 距起点距离2
     */
    @Excel(name = "距起点距离2", orderNum = "5")
    private Double distance2;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 目标断层编号
     */
    @Excel(name = "目标断层编号", orderNum = "6")
    private String targetfaultid;
    /**
     * 异常区段编号
     */
    @Excel(name = "异常区段编号", orderNum = "7")
    private String id;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 乡
     */
    private String town;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 异常分类
     */
    @Excel(name = "异常分类", orderNum = "8")
    private String abnormaltype;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 显示码
     */
    @Excel(name = "显示码", orderNum = "9")
    private Integer showcode;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 所属测线编号
     */
    @Excel(name = "所属测线编号", orderNum = "10")
    private String svylineid;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 距起点距离1
     */
    @Excel(name = "距起点距离1", orderNum = "11")
    private Double distance1;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "12")
    private String commentInfo;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 目标断层来源
     */
    @Excel(name = "目标断层来源", orderNum = "13")
    private String targetfaultsource;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段7
     */
    private String extends7;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String targetfaultsourceName;
    private String rowNum;
    private String errorMsg;
}