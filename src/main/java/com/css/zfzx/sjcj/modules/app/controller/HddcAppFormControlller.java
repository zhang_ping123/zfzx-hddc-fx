package com.css.zfzx.sjcj.modules.app.controller;

import com.css.bpm.platform.base.response.RestResponse;
import com.css.zfzx.sjcj.modules.app.service.HddcAppFormService;
import com.css.zfzx.sjcj.modules.hddcwyCrater.repository.entity.HddcWyCraterEntity;
import com.css.zfzx.sjcj.modules.hddcwyCrater.service.HddcWyCraterService;
import com.css.zfzx.sjcj.modules.hddcwyDrillHole.repository.entity.HddcWyDrillholeEntity;
import com.css.zfzx.sjcj.modules.hddcwyDrillHole.service.HddcWyDrillholeService;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.service.HddcWyFaultsvypointService;
import com.css.zfzx.sjcj.modules.hddcwyGeoGeomorphySvyPoint.repository.entity.HddcWyGeogeomorphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeoGeomorphySvyPoint.service.HddcWyGeogeomorphysvypointService;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyLine.repository.entity.HddcWyGeochemicalsvylineEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyLine.service.HddcWyGeochemicalsvylineService;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyPoint.repository.entity.HddcWyGeochemicalsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyPoint.service.HddcWyGeochemicalsvypointService;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyLine.repository.entity.HddcWyGeologicalsvylineEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyLine.service.HddcWyGeologicalsvylineService;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningLine.repository.entity.HddcWyGeologicalsvyplanninglineEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningLine.service.HddcWyGeologicalsvyplanninglineService;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningPt.repository.entity.HddcWyGeologicalsvyplanningptEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningPt.service.HddcWyGeologicalsvyplanningptService;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPoint.repository.entity.HddcWyGeologicalsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPoint.service.HddcWyGeologicalsvypointService;
import com.css.zfzx.sjcj.modules.hddcwyGeomorStation.repository.entity.HddcWyGeomorstationEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorStation.service.HddcWyGeomorstationService;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyLine.repository.entity.HddcWyGeomorphysvylineEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyLine.service.HddcWyGeomorphysvylineService;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.repository.entity.HddcWyGeomorphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.service.HddcWyGeomorphysvypointService;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyReProf.repository.entity.HddcWyGeomorphysvyreprofEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyReProf.service.HddcWyGeomorphysvyreprofService;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyRegion.repository.entity.HddcWyGeomorphysvyregionEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyRegion.service.HddcWyGeomorphysvyregionService;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvySamplePoint.repository.entity.HddcWyGeomorphysvysamplepointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvySamplePoint.service.HddcWyGeomorphysvysamplepointService;
import com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.repository.entity.HddcWyGeophysvylineEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.service.HddcWyGeophysvylineService;
import com.css.zfzx.sjcj.modules.hddcwyGeophysvypoint.repository.entity.HddcWyGeophysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeophysvypoint.service.HddcWyGeophysvypointService;
import com.css.zfzx.sjcj.modules.hddcwyLava.repository.entity.HddcWyLavaEntity;
import com.css.zfzx.sjcj.modules.hddcwyLava.service.HddcWyLavaService;
import com.css.zfzx.sjcj.modules.hddcwySamplePoint.repository.entity.HddcWySamplepointEntity;
import com.css.zfzx.sjcj.modules.hddcwySamplePoint.service.HddcWySamplepointService;
import com.css.zfzx.sjcj.modules.hddcwyStratigraphySvyPoint.repository.entity.HddcWyStratigraphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyStratigraphySvyPoint.service.HddcWyStratigraphysvypointService;
import com.css.zfzx.sjcj.modules.hddcwyTrench.repository.entity.HddcWyTrenchEntity;
import com.css.zfzx.sjcj.modules.hddcwyTrench.service.HddcWyTrenchService;
import com.css.zfzx.sjcj.modules.hddcwyVolcanicSamplePoint.repository.entity.HddcWyVolcanicsamplepointEntity;
import com.css.zfzx.sjcj.modules.hddcwyVolcanicSamplePoint.service.HddcWyVolcanicsamplepointService;
import com.css.zfzx.sjcj.modules.hddcwyVolcanicSvyPoint.repository.entity.HddcWyVolcanicsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyVolcanicSvyPoint.service.HddcWyVolcanicsvypointService;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * TODO
 *
 * @author 王昊杰
 * @version 1.0
 * @date 2020/12/10  14:58
 */

@Slf4j
@RestController
@RequestMapping("/hddc/hddcAppForminfo")
public class HddcAppFormControlller {
    @Autowired
    private HddcAppFormService hddcAppFormService;
    @Autowired
    private HddcWyGeologicalsvyplanninglineService hddcWyGeologicalsvyplanninglineService;
    @Autowired
    private HddcWyGeologicalsvyplanningptService hddcWyGeologicalsvyplanningptService;
    @Autowired
    private HddcWyFaultsvypointService hddcWyFaultsvypointService;
    @Autowired
    private HddcWyGeogeomorphysvypointService hddcWyGeogeomorphysvypointService;
    @Autowired
    private HddcWyGeologicalsvylineService hddcWyGeologicalsvylineService;
    @Autowired
    private HddcWyGeologicalsvypointService hddcWyGeologicalsvypointService;
    @Autowired
    private HddcWyStratigraphysvypointService hddcWyStratigraphysvypointService;
    @Autowired
    private HddcWyTrenchService hddcWyTrenchService;
    @Autowired
    private HddcWyGeomorphysvylineService hddcWyGeomorphysvylineService;
    @Autowired
    private HddcWyGeomorphysvypointService hddcWyGeomorphysvypointService;
    @Autowired
    private HddcWyGeomorphysvyregionService hddcWyGeomorphysvyregionService;
    @Autowired
    private HddcWyGeomorphysvyreprofService hddcWyGeomorphysvyreprofService;
    @Autowired
    private HddcWyGeomorphysvysamplepointService hddcWyGeomorphysvysamplepointService;
    @Autowired
    private HddcWyGeomorstationService hddcWyGeomorstationService;
    @Autowired
    private HddcWyDrillholeService hddcWyDrillholeService;
    @Autowired
    private HddcWySamplepointService hddcWySamplepointService;
    @Autowired
    private HddcWyGeophysvylineService hddcWyGeophysvylineService;
    @Autowired
    private HddcWyGeophysvypointService hddcWyGeophysvypointService;
    @Autowired
    private HddcWyGeochemicalsvylineService hddcWyGeochemicalsvylineService;
    @Autowired
    private HddcWyGeochemicalsvypointService hddcWyGeochemicalsvypointService;
    @Autowired
    private HddcWyCraterService hddcWyCraterService;
    @Autowired
    private HddcWyLavaService hddcWyLavaService;
    @Autowired
    private HddcWyVolcanicsamplepointService hddcWyVolcanicsamplepointService;
    @Autowired
    private HddcWyVolcanicsvypointService hddcWyVolcanicsvypointService;

    @GetMapping("/findAppFormData")
    public RestResponse findAppFormData(String userId, String taskId, String projectId, String type) {
        RestResponse response = null;
        try {
            if ("1".equals(type)) {
                List<HddcWyGeologicalsvyplanninglineEntity> list = hddcWyGeologicalsvyplanninglineService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("2".equals(type)) {
                List<HddcWyGeologicalsvyplanningptEntity> list = hddcWyGeologicalsvyplanningptService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("3".equals(type)) {
                List<HddcWyFaultsvypointEntity> list = hddcWyFaultsvypointService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("4".equals(type)) {
                List<HddcWyGeogeomorphysvypointEntity> list = hddcWyGeogeomorphysvypointService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("5".equals(type)) {
                List<HddcWyGeologicalsvylineEntity> list = hddcWyGeologicalsvylineService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("6".equals(type)) {
                List<HddcWyGeologicalsvypointEntity> list = hddcWyGeologicalsvypointService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("7".equals(type)) {
                List<HddcWyStratigraphysvypointEntity> list = hddcWyStratigraphysvypointService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("8".equals(type)) {
                List<HddcWyTrenchEntity> list = hddcWyTrenchService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("9".equals(type)) {
                List<HddcWyGeomorphysvylineEntity> list = hddcWyGeomorphysvylineService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("10".equals(type)) {
                List<HddcWyGeomorphysvypointEntity> list = hddcWyGeomorphysvypointService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("11".equals(type)) {
                List<HddcWyGeomorphysvyregionEntity> list = hddcWyGeomorphysvyregionService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("12".equals(type)) {
                List<HddcWyGeomorphysvyreprofEntity> list = hddcWyGeomorphysvyreprofService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("13".equals(type)) {
                List<HddcWyGeomorphysvysamplepointEntity> list = hddcWyGeomorphysvysamplepointService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("14".equals(type)) {
                List<HddcWyGeomorstationEntity> list = hddcWyGeomorstationService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("15".equals(type)) {
                List<HddcWyDrillholeEntity> list = hddcWyDrillholeService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("16".equals(type)) {
                List<HddcWySamplepointEntity> list = hddcWySamplepointService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("17".equals(type)) {
                List<HddcWyGeophysvylineEntity> list = hddcWyGeophysvylineService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("18".equals(type)) {
                List<HddcWyGeophysvypointEntity> list = hddcWyGeophysvypointService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("19".equals(type)) {
                List<HddcWyGeochemicalsvylineEntity> list = hddcWyGeochemicalsvylineService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("20".equals(type)) {
                List<HddcWyGeochemicalsvypointEntity> list = hddcWyGeochemicalsvypointService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("21".equals(type)) {
                List<HddcWyCraterEntity> list = hddcWyCraterService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("22".equals(type)) {
                List<HddcWyLavaEntity> list = hddcWyLavaService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("23".equals(type)) {
                List<HddcWyVolcanicsamplepointEntity> list = hddcWyVolcanicsamplepointService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            if ("24".equals(type)) {
                List<HddcWyVolcanicsvypointEntity> list = hddcWyVolcanicsvypointService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, "1");
                response = RestResponse.succeed(list);
            }
            return response;
        } catch (Exception e) {
            String errorMessage = "查询失败!";
            log.error(errorMessage, e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }


}
