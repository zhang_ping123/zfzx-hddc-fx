package com.css.zfzx.sjcj.modules.hddcA6StrongSeiHasFractBlt.repository;

import com.css.zfzx.sjcj.modules.hddcA6StrongSeiHasFractBlt.repository.entity.HddcA6StrongseihasfractbltEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-28
 */
public interface HddcA6StrongseihasfractbltRepository extends JpaRepository<HddcA6StrongseihasfractbltEntity, String> {
}
