package com.css.zfzx.sjcj.modules.hddcMaterialPolygonLayer.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-12-03
 */
@Data
public class HddcMaterialpolygonlayerQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String materialname;

}
