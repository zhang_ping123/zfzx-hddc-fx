package com.css.zfzx.sjcj.modules.hddcwyVolcanicSamplePoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyVolcanicSamplePoint.repository.entity.HddcWyVolcanicsamplepointEntity;
import com.css.zfzx.sjcj.modules.hddcwyVolcanicSamplePoint.viewobjects.HddcWyVolcanicsamplepointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-02
 */

public interface HddcWyVolcanicsamplepointService {

    public JSONObject queryHddcWyVolcanicsamplepoints(HddcWyVolcanicsamplepointQueryParams queryParams, int curPage, int pageSize);

    public HddcWyVolcanicsamplepointEntity getHddcWyVolcanicsamplepoint(String uuid);

    public HddcWyVolcanicsamplepointEntity saveHddcWyVolcanicsamplepoint(HddcWyVolcanicsamplepointEntity hddcWyVolcanicsamplepoint);

    public HddcWyVolcanicsamplepointEntity updateHddcWyVolcanicsamplepoint(HddcWyVolcanicsamplepointEntity hddcWyVolcanicsamplepoint);

    public void deleteHddcWyVolcanicsamplepoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    BigInteger queryHddcWyVolcanicsamplepoint(HddcAppZztCountVo queryParams);

    List<HddcWyVolcanicsamplepointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);

    List<HddcWyVolcanicsamplepointEntity> findAllByCreateUserAndIsValid(String userId,String isValid);



    /**
     * 逻辑删除-根据项目id删除数据
     * @param ids
     */
    void deleteByProjectId(List<String> ids);

    /**
     * 逻辑删除-根据任务id删除数据
     * @param ids
     */
    void deleteByTaskId(List<String> ids);
}
