package com.css.zfzx.sjcj.modules.hddcwyCrater.repository;

import com.css.zfzx.sjcj.modules.hddcwyCrater.repository.entity.HddcWyCraterEntity;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-02
 */
public interface HddcWyCraterRepository extends JpaRepository<HddcWyCraterEntity, String> {

    List<HddcWyCraterEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    List<HddcWyCraterEntity> findAllByCreateUserAndIsValid(String userId, String isValid);


    @Query(nativeQuery = true, value = "select * from hddc_wy_crater where is_valid!=0 and project_name in :projectIds")
    List<HddcWyCraterEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_crater where task_name in :projectIds")
    List<HddcWyCraterEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);


}
