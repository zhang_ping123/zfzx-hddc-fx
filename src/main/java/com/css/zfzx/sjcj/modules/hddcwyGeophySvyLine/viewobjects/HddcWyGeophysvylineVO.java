package com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-12-01
 */
@Data
public class HddcWyGeophysvylineVO implements Serializable {

    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 测线长度 [米]
     */
    @Excel(name = "测线长度 [米]", orderNum = "12")
    private Double length;
    /**
     * 终点桩号
     */
    @Excel(name = "终点桩号", orderNum = "20")
    private Integer endmilestonenum;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 测线所属探测工程编号
     */
    @Excel(name = "测线所属探测工程编号", orderNum = "19")
    private String projectId;
    /**
     * 项目名称
     */
    @Excel(name="项目名称",orderNum = "7")
    private String projectName;
    /**
     * 显示码（制图用）
     */
    @Excel(name = "显示码（制图用）", orderNum = "9")
    private Integer showcode;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 任务名称
     */
    @Excel(name="任务名称",orderNum = "8")
    private String taskName;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 断点解释剖面图图像编号
     */
    @Excel(name = "断点解释剖面图图像编号", orderNum = "26")
    private String faultptprofileAiid;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 测线名称
     */
    @Excel(name = "测线名称", orderNum = "22")
    private String name;
    /**
     * 测线来源与类型
     */
    @Excel(name = "测线来源与类型", orderNum = "16")
    private Integer svylinesource;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 综合解释剖面名称
     */
    @Excel(name = "综合解释剖面名称", orderNum = "23")
    private String resultname;
    /**
     * 项目ID
     */
    private String projectcode;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "3")
    private String city;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段5
     */
    @Excel(name="区域",orderNum = "6")
    private String extends5;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 综合解释剖面矢量图原始文件编号
     */
    @Excel(name = "综合解释剖面矢量图原始文件编号", orderNum = "14")
    private String resultmapArwid;
    /**
     * 收集地球物理测线来源补充说明来源
     */
    @Excel(name = "收集地球物理测线来源补充说明来源", orderNum = "10")
    private String collectedlinesource;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 起点桩号
     */
    @Excel(name = "起点桩号", orderNum = "13")
    private Integer startmilestonenum;
    /**
     * 探测方法
     */
    @Excel(name = "探测方法", orderNum = "15")
    private Integer svymethod;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 测线代码
     */
    @Excel(name = "测线代码", orderNum = "18")
    private String fieldid;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 乡
     */
    @Excel(name="详细地址",orderNum = "5")
    private String town;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 断点解释剖面图原始文件编号
     */
    @Excel(name = "断点解释剖面图原始文件编号", orderNum = "24")
    private String faultptprofileArwid;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备注
     */
    private String remark;
    /**
     * 综合解释剖面原始数据文件编号（sgy等）
     */
    @Excel(name = "综合解释剖面原始数据文件编号（sgy等）", orderNum = "25")
    private String expdataArwid;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "2")
    private String province;
    /**
     * 探测目的
     */
    @Excel(name = "探测目的", orderNum = "11")
    private String purpose;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 测线编号
     */
    @Excel(name = "测线编号",orderNum = "1")
    private String id;
    /**
     * 综合解释剖面栅格图原始文件编号
     */
    @Excel(name = "综合解释剖面栅格图原始文件编号", orderNum = "17")
    private String resultmapAiid;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "4")
    private String area;
    /**
     * 测线备注
     */
    @Excel(name = "测线备注", orderNum = "21")
    private String commentInfo;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 经度
     */
    private Double lon;
    /**
     * 维度
     */
    private Double lat;


    private String provinceName;
    private String cityName;
    private String areaName;
    private String rowNum;
    private String errorMsg;
}