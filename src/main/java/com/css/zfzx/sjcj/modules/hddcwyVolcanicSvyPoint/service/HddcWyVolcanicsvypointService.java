package com.css.zfzx.sjcj.modules.hddcwyVolcanicSvyPoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyVolcanicSvyPoint.repository.entity.HddcWyVolcanicsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyVolcanicSvyPoint.viewobjects.HddcWyVolcanicsvypointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-02
 */

public interface HddcWyVolcanicsvypointService {

    public JSONObject queryHddcWyVolcanicsvypoints(HddcWyVolcanicsvypointQueryParams queryParams, int curPage, int pageSize);

    public HddcWyVolcanicsvypointEntity getHddcWyVolcanicsvypoint(String uuid);

    public HddcWyVolcanicsvypointEntity saveHddcWyVolcanicsvypoint(HddcWyVolcanicsvypointEntity hddcWyVolcanicsvypoint);

    public HddcWyVolcanicsvypointEntity updateHddcWyVolcanicsvypoint(HddcWyVolcanicsvypointEntity hddcWyVolcanicsvypoint);

    public void deleteHddcWyVolcanicsvypoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    BigInteger queryHddcWyVolcanicsvypoint(HddcAppZztCountVo queryParams);

    List<HddcWyVolcanicsvypointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);

    List<HddcWyVolcanicsvypointEntity> findAllByCreateUserAndIsValid(String userId,  String isValid);

    /**
     * 逻辑删除-根据项目id删除数据
     * @param ids
     */
    void deleteByProjectId(List<String> ids);

    /**
     * 逻辑删除-根据任务id删除数据
     * @param ids
     */
    void deleteByTaskId(List<String> ids);

}
