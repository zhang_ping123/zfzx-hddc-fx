package com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyLine.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyLine.repository.entity.HddcWyGeochemicalsvylineEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyLine.service.HddcWyGeochemicalsvylineService;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyLine.viewobjects.HddcWyGeochemicalsvylineQueryParams;
import com.css.bpm.platform.utils.PlatformPageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-02
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcWyGeochemicalsvylines")
public class HddcWyGeochemicalsvylineController {
    @Autowired
    private HddcWyGeochemicalsvylineService hddcWyGeochemicalsvylineService;

    @GetMapping("/queryHddcWyGeochemicalsvylines")
    public RestResponse queryHddcWyGeochemicalsvylines(HttpServletRequest request, HddcWyGeochemicalsvylineQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcWyGeochemicalsvylineService.queryHddcWyGeochemicalsvylines(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/findAllByhddcWyGeochemicalsvylines")
    public RestResponse findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId,String taskId,String projectId,String isValid) {
        RestResponse response = null;
        try{
            List<HddcWyGeochemicalsvylineEntity> list = hddcWyGeochemicalsvylineService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, isValid);
            response = RestResponse.succeed(list);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{uuid}")
    public RestResponse getHddcWyGeochemicalsvyline(@PathVariable String uuid) {
        RestResponse response = null;
        try{
            HddcWyGeochemicalsvylineEntity hddcWyGeochemicalsvyline = hddcWyGeochemicalsvylineService.getHddcWyGeochemicalsvyline(uuid);
            response = RestResponse.succeed(hddcWyGeochemicalsvyline);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcWyGeochemicalsvyline(@RequestBody HddcWyGeochemicalsvylineEntity hddcWyGeochemicalsvyline) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcWyGeochemicalsvylineService.saveHddcWyGeochemicalsvyline(hddcWyGeochemicalsvyline);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcWyGeochemicalsvyline(@RequestBody HddcWyGeochemicalsvylineEntity hddcWyGeochemicalsvyline)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcWyGeochemicalsvylineService.updateHddcWyGeochemicalsvyline(hddcWyGeochemicalsvyline);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcWyGeochemicalsvylines(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcWyGeochemicalsvylineService.deleteHddcWyGeochemicalsvylines(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcWyGeochemicalsvylineService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }

    /***
     * 导出
     * @param response
     * @return
     */
    @GetMapping("/exportFile")
    public RestResponse exportFileYhDisasters(HttpServletResponse response,
                                              @RequestParam("projectName")String projectName, @RequestParam("taskName") String taskName,
                                              @RequestParam("startTime") String startTime, @RequestParam("endTime")String endTime) {
        RestResponse responseRest = null;
        JSONObject jsonObject = new JSONObject();
        try{
            HddcAppZztCountVo queryParams=new HddcAppZztCountVo();
            queryParams.setProjectName(projectName);
            queryParams.setTaskName(taskName);
            queryParams.setStartTime(startTime);
            queryParams.setEndTime(endTime);
            hddcWyGeochemicalsvylineService.exportFile(queryParams,response);
            jsonObject.put("message", "导出成功");
            responseRest = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            responseRest = RestResponse.fail(errorMessage);
        }
        return responseRest;
    }


    /**
     * 导入
     *
     * @param file
     * @param response
     * @return
     */
    @PostMapping("/importDisaster")
    public RestResponse export(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        RestResponse restResponse = null;
        try {
            String s = hddcWyGeochemicalsvylineService.exportExcel( file, response);
            restResponse = RestResponse.succeed(s);
        } catch (Exception e) {
            String errormessage = "导入失败";
            log.error(errormessage, e);
            restResponse = RestResponse.fail(errormessage);
        }
        return restResponse;
    }
}