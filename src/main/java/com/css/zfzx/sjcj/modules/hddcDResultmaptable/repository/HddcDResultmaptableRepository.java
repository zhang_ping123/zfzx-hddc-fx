package com.css.zfzx.sjcj.modules.hddcDResultmaptable.repository;

import com.css.zfzx.sjcj.modules.hddcDResultmaptable.repository.entity.HddcDResultmaptableEntity;
import com.css.zfzx.sjcj.modules.hddccjfilemanage.repository.entity.HddcFileManageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zyb
 * @date 2020-12-19
 */
public interface HddcDResultmaptableRepository extends JpaRepository<HddcDResultmaptableEntity, String> {

//    @Query(nativeQuery = true, value = "select f.* from hddc_d_resultmaptable f where f.file_id=?1")
//    List<HddcDResultmaptableEntity> findbyId(String filenumber);
}
