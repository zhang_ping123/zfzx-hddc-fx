package com.css.zfzx.sjcj.modules.hddcMaterialPolygonLayer.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-12-03
 */
@Data
public class HddcMaterialpolygonlayerVO implements Serializable {

    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 电子资料原始文件编号
     */
    @Excel(name = "电子资料原始文件编号", orderNum = "4")
    private String electronicmaterialArwid;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 比例尺（分母）
     */
    @Excel(name = "比例尺（分母）", orderNum = "5")
    private Integer scale;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 面状资料编号
     */
    private String id;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备注
     */
    private String remark;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 出版日期
     */
    @Excel(name = "出版日期", orderNum = "6")
    private String publishdate;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 资料图件名称
     */
    @Excel(name = "资料图件名称", orderNum = "7")
    private String materialname;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "8")
    private String commentInfo;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 村
     */
    private String village;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 出版单位
     */
    @Excel(name = "出版单位", orderNum = "9")
    private String publisher;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 乡
     */
    private String town;
    /**
     * 坐标系统
     */
    @Excel(name = "坐标系统", orderNum = "10")
    private String coordinatesystem;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 投影信息
     */
    @Excel(name = "投影信息", orderNum = "11")
    private String projection;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 是否为电子资料
     */
    @Excel(name = "是否为电子资料", orderNum = "12")
    private Integer iselectronicversion;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer iselectronicversionName;
    private String rowNum;
    private String errorMsg;
}