package com.css.zfzx.sjcj.modules.hddcB2GeomorphySvyProjectTable.repository;

import com.css.zfzx.sjcj.modules.hddcB2GeomorphySvyProjectTable.repository.entity.HddcB2GeomorphysvyprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB2GeomorphySvyProjectTable.viewobjects.HddcB2GeomorphysvyprojecttableQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-12-07
 */
public interface HddcB2GeomorphysvyprojecttableNativeRepository {

    Page<HddcB2GeomorphysvyprojecttableEntity> queryHddcB2Geomorphysvyprojecttables(HddcB2GeomorphysvyprojecttableQueryParams queryParams, int curPage, int pageSize);

    List<HddcB2GeomorphysvyprojecttableEntity> exportYhDisasters(HddcB2GeomorphysvyprojecttableQueryParams queryParams);
}
