package com.css.zfzx.sjcj.modules.hddcB5_GeophySvyDataTable.repository;

import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyDataTable.repository.entity.HddcB5GeophysvydatatableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
public interface HddcB5GeophysvydatatableRepository extends JpaRepository<HddcB5GeophysvydatatableEntity, String> {
}
