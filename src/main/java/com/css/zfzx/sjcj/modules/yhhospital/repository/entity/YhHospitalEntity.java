package com.css.zfzx.sjcj.modules.yhhospital.repository.entity;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author yyd
 * @date 2020-11-03
 */
@Data
@Entity
@Table(name="yh_hospital")
public class YhHospitalEntity implements Serializable {

    /**
     * 建筑等级
     */
    @Column(name="building_level")
    private String buildingLevel;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 区
     */
    @Column(name="area")
    private String area;
    /**
     * 状态
     */
    @Column(name="status")
    private String status;
    /**
     * 经度
     */
    @Column(name="longitude")
    private Double longitude;
    /**
     * 是否有效,0:无效,1:有效
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 隐患等级
     */
    @Column(name="yh_level")
    private String yhLevel;
    /**
     * 主键ID
     */
    @Id
    @Column(name="id")
    private String id;
    /**
     * 原因
     */
    @Column(name="reason")
    private String reason;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 纬度
     */
    @Column(name="latitude")
    private Double latitude;
    /**
     * 建筑高度
     */
    @Column(name="building_height")
    private Double buildingHeight;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 建筑面积
     */
    @Column(name="building_area")
    private Double buildingArea;
    /**
     * 承灾体名称
     */
    @Column(name="czt_name")
    private String cztName;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;

}

