package com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.repository.entity;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangping
 * @date 2020-11-23
 */
@Data
@Entity
@Table(name="hddc_c4_microtremortable")
public class HddcC4MicrotremortableEntity implements Serializable {

    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 数据编号
     */
    @Id
    @Column(name="ID")
    private String id;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 备注
     */
    @Column(name="CommentInfo")
    private String commentinfo;
    /**
     * 测量时间长度
     */
    @Column(name="TimeWindow")
    private Double timewindow;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 测点纬度
     */
    @Column(name="StationLatitude")
    private Double stationlatitude;
    /**
     * 测量单位
     */
    @Column(name="Unit")
    private String unit;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 测点经度
     */
    @Column(name="StationLongitude")
    private Double stationlongitude;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 采样率
     */
    @Column(name="Frequency")
    private Double frequency;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 三分量第脉动记录文件原始编号
     */
    @Column(name="Mircrotremor_ARWID")
    private String mircrotremorArwid;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 仪器类型
     */
    @Column(name="Instrument")
    private String instrument;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;

}

