package com.css.zfzx.sjcj.modules.hddcB1PaleoEQEvent.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Data
public class HddcB1PaleoeqeventVO implements Serializable {

    /**
     * 同震位移误差值
     */
    @Excel(name="同震位移误差值",orderNum = "9")
    private Double offseter;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 省
     */
    @Excel(name="省",orderNum = "1")
    private String province;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 识别标志
     */
    @Excel(name="识别标志",orderNum = "11")
    private String evidence;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 古地震发生时间区间
     */
    @Excel(name="古地震发生时间区间",orderNum = "7")
    private String occurdateestinterval;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 记录编号
     */
    private String id;
    /**
     * 乡
     */
    private String town;
    /**
     * 古地震发生时间
     */
    @Excel(name="古地震发生时间",orderNum = "5")
    private Double occurdateest;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 探槽编号
     */
    @Excel(name="探槽编号",orderNum = "4")
    private String trenchid;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 错动性质
     */
    @Excel(name="错动性质",orderNum = "10")
    private Integer slipfeature;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备注
     */
    @Excel(name="备注",orderNum = "12")
    private String commentInfo;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 同震位移 [ [米]]
     */
    @Excel(name="同震位移 [ [米]]",orderNum = "8")
    private Double offsetest;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 区（县）
     */
    @Excel(name="区（县）",orderNum = "3")
    private String area;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 市
     */
    @Excel(name="市",orderNum = "2")
    private String city;
    /**
     * 古地震发生时间误差
     */
    @Excel(name="古地震发生时间误差",orderNum = "6")
    private Double occurdateer;
    /**
     * 备选字段14
     */
    private String extends14;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer slipfeatureName;
    private String rowNum;
    private String errorMsg;

}