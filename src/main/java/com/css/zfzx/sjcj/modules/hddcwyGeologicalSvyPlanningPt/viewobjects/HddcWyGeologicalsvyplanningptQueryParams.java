package com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningPt.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
public class HddcWyGeologicalsvyplanningptQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String startTime;
    private String endTime;

    private String userId;
    private String taskId;
    private String projectId;
    private String isVaild;

}
