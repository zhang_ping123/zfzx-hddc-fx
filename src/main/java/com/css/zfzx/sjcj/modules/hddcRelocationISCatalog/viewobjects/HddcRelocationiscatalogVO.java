package com.css.zfzx.sjcj.modules.hddcRelocationISCatalog.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Data
public class HddcRelocationiscatalogVO implements Serializable {

    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 编号
     */
    private String id;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 地名
     */
    @Excel(name = "地名", orderNum = "4")
    private String locationname;
    /**
     * 所用P波到时数
     */
    @Excel(name = "所用P波到时数", orderNum = "5")
    private Integer nctp;
    /**
     * 秒（至少小数点后两位）
     */
    @Excel(name = "秒（至少小数点后两位）", orderNum = "6")
    private Double occurrencesecond;
    /**
     * 震级 [Ml]
     */
    @Excel(name = "震级 [Ml]", orderNum = "7")
    private Double magnitude;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 深度定位误差 [米]
     */
    @Excel(name = "深度定位误差 [米]", orderNum = "8")
    private Double locationerrorz;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 时（24小时制）
     */
    @Excel(name = "时（24小时制）", orderNum = "9")
    private String occurrencehour;
    /**
     * 震相数
     */
    @Excel(name = "震相数", orderNum = "10")
    private Integer seismicphasenum;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 分
     */
    @Excel(name = "分", orderNum = "11")
    private String occurrenceminute;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 所用S波到时数
     */
    @Excel(name = "所用S波到时数", orderNum = "12")
    private Integer ncts;
    /**
     * 东西向定位误差 [米]
     */
    @Excel(name = "东西向定位误差 [米]", orderNum = "13")
    private Double locationerrorx;
    /**
     * 南北向定位误差 [米]
     */
    @Excel(name = "南北向定位误差 [米]", orderNum = "14")
    private Double locationerrory;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 震源深度 [公里]
     */
    @Excel(name = "震源深度 [公里]", orderNum = "15")
    private Double depth;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 经度
     */
    @Excel(name = "经度", orderNum = "16")
    private Double lon;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 乡
     */
    private String town;
    /**
     * 日期
     */
    @Excel(name = "日期", orderNum = "40")
    private String occurrencedate;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 宏观震中烈度
     */
    @Excel(name = "宏观震中烈度", orderNum = "2")
    private Integer epicenter;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 纬度
     */
    @Excel(name = "纬度", orderNum = "3")
    private Double lat;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 定位残差 [秒]
     */
    @Excel(name = "定位残差 [秒]", orderNum = "4")
    private Double rms;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备注
     */
    private String commentInfo;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段20
     */
    private String extends20;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String rowNum;
    private String errorMsg;
}