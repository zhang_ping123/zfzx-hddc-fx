package com.css.zfzx.sjcj.modules.hddcStratigraphy25Pre.repository;

import com.css.zfzx.sjcj.modules.hddcStratigraphy25Pre.repository.entity.HddcStratigraphy25preEntity;
import com.css.zfzx.sjcj.modules.hddcStratigraphy25Pre.viewobjects.HddcStratigraphy25preQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcStratigraphy25preNativeRepository {

    Page<HddcStratigraphy25preEntity> queryHddcStratigraphy25pres(HddcStratigraphy25preQueryParams queryParams, int curPage, int pageSize);

    List<HddcStratigraphy25preEntity> exportYhDisasters(HddcStratigraphy25preQueryParams queryParams);
}
