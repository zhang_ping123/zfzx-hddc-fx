package com.css.zfzx.sjcj.modules.hddcTargetRegion.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
public class HddcTargetregionVO implements Serializable {

    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 地形变监测工程数
     */
    @Excel(name = "地形变监测工程数", orderNum = "4")
    private Integer crustaldfmprojectcount;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 是否开展地震危害性评价
     */
    @Excel(name = "是否开展地震危害性评价", orderNum = "5")
    private Integer sda;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 目标区研究断层总数
     */
    @Excel(name = "目标区研究断层总数", orderNum = "6")
    private Integer trstudiedfaultcount;
    /**
     * 地球物理探测工程数
     */
    @Excel(name = "地球物理探测工程数", orderNum = "7")
    private Integer geophysicalsvyprojectcount;
    /**
     * 乡
     */
    private String town;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "8")
    private String commentInfo;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 获得测试结果样品数
     */
    @Excel(name = "获得测试结果样品数", orderNum = "9")
    private Integer datingsamplecount;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 是否开展地震危险性评价
     */
    @Excel(name = "是否开展地震危险性评价", orderNum = "10")
    private Integer sra;
    /**
     * 探测总土方量 [m³]
     */
    @Excel(name = "探测总土方量 [m³]", orderNum = "11")
    private Double trenchvolume;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 地球化学探测工程数
     */
    @Excel(name = "地球化学探测工程数", orderNum = "12")
    private Integer geochemicalprojectcount;
    /**
     * 描述信息
     */
    @Excel(name = "描述信息", orderNum = "13")
    private String description;
    /**
     * 是否火山地质调查填图
     */
    @Excel(name = "是否火山地质调查填图", orderNum = "14")
    private Integer isvolcanic;
    /**
     * 遥感影像处理数目
     */
    @Excel(name = "遥感影像处理数目", orderNum = "15")
    private Integer rsprocess;
    /**
     * 采集样品总数
     */
    @Excel(name = "采集样品总数", orderNum = "16")
    private Integer collectedsamplecount;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 地质填图面积 [km²]
     */
    @Excel(name = "地质填图面积 [km²]", orderNum = "17")
    private Integer mappingarea;
    /**
     * 送样总数
     */
    @Excel(name = "送样总数", orderNum = "18")
    private Integer samplecount;
    /**
     * 目标区确定活动断层条数
     */
    @Excel(name = "目标区确定活动断层条数", orderNum = "19")
    private Integer afaultcount;
    /**
     * 目标区名称（课题名称）
     */
    @Excel(name = "目标区名称（课题名称）", orderNum = "20")
    private String targetname;
    /**
     * 工作区编号
     */
    @Excel(name = "工作区编号", orderNum = "21")
    private String workregionid;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 野外观测点数
     */
    @Excel(name = "野外观测点数", orderNum = "22")
    private Integer fieldsvyptcount;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 编号
     */
    private String id;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 微地貌测量工程总数
     */
    @Excel(name = "微地貌测量工程总数", orderNum = "23")
    private Integer geomorphysvyprojectcount;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 探槽数
     */
    @Excel(name = "探槽数", orderNum = "24")
    private Integer trenchcount;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备注
     */
    private String remark;
    /**
     * 地震监测工程数
     */
    @Excel(name = "地震监测工程数", orderNum = "25")
    private Integer seismicprojectcount;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 村
     */
    private String village;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 钻孔进尺 [米]
     */
    @Excel(name = "钻孔进尺 [米]", orderNum = "26")
    private Integer drilllength;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 目标区断层总数
     */
    @Excel(name = "目标区断层总数", orderNum = "27")
    private Integer trfaultcount;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 钻孔数
     */
    @Excel(name = "钻孔数", orderNum = "28")
    private Integer drillcount;
    /**
     * 项目名称（大项目名称）
     */
    @Excel(name = "项目名称（大项目名称）", orderNum = "29")
    private String name;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 显示码
     */
    @Excel(name = "显示码", orderNum = "30")
    private String showcode;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 断层三维数值模拟总数
     */
    @Excel(name = "断层三维数值模拟总数", orderNum = "31")
    private Integer numsimulationcount;
    /**
     * 地球物理测井数
     */
    @Excel(name = "地球物理测井数", orderNum = "32")
    private Integer gphwellcount;
    /**
     * 目标区面积 [km²]
     */
    @Excel(name = "目标区面积 [km²]", orderNum = "33")
    private Integer trarea;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer sdaName;
    private Integer sraName;
    private Integer isvolcanicName;
    private String rowNum;
    private String errorMsg;
}