package com.css.zfzx.sjcj.modules.hddcDResultmaptable.repository;

import com.css.zfzx.sjcj.modules.hddcDResultmaptable.repository.entity.HddcDResultmaptableEntity;
import com.css.zfzx.sjcj.modules.hddcDResultmaptable.viewobjects.HddcDResultmaptableQueryParams;
import org.springframework.data.domain.Page;

/**
 * @author zyb
 * @date 2020-12-19
 */
public interface HddcDResultmaptableNativeRepository {

    Page<HddcDResultmaptableEntity> queryHddcDResultmaptables(HddcDResultmaptableQueryParams queryParams, int curPage, int pageSize,String sort,String order);
}
