package com.css.zfzx.sjcj.modules.hddcB1GeomorPlyOnFractBlt.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.bpm.platform.utils.PlatformPageUtils;
import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyOnFractBlt.repository.entity.HddcB1GeomorplyonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyOnFractBlt.service.HddcB1GeomorplyonfractbltService;
import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyOnFractBlt.viewobjects.HddcB1GeomorplyonfractbltQueryParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcB1Geomorplyonfractblts")
public class HddcB1GeomorplyonfractbltController {
    @Autowired
    private HddcB1GeomorplyonfractbltService hddcB1GeomorplyonfractbltService;

    @GetMapping("/queryHddcB1Geomorplyonfractblts")
    public RestResponse queryHddcB1Geomorplyonfractblts(HttpServletRequest request, HddcB1GeomorplyonfractbltQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcB1GeomorplyonfractbltService.queryHddcB1Geomorplyonfractblts(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcB1Geomorplyonfractblt(@PathVariable String id) {
        RestResponse response = null;
        try{
            HddcB1GeomorplyonfractbltEntity hddcB1Geomorplyonfractblt = hddcB1GeomorplyonfractbltService.getHddcB1Geomorplyonfractblt(id);
            response = RestResponse.succeed(hddcB1Geomorplyonfractblt);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcB1Geomorplyonfractblt(@RequestBody HddcB1GeomorplyonfractbltEntity hddcB1Geomorplyonfractblt) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB1GeomorplyonfractbltService.saveHddcB1Geomorplyonfractblt(hddcB1Geomorplyonfractblt);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcB1Geomorplyonfractblt(@RequestBody HddcB1GeomorplyonfractbltEntity hddcB1Geomorplyonfractblt)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB1GeomorplyonfractbltService.updateHddcB1Geomorplyonfractblt(hddcB1Geomorplyonfractblt);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcB1Geomorplyonfractblts(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB1GeomorplyonfractbltService.deleteHddcB1Geomorplyonfractblts(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcB1GeomorplyonfractbltService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }
    /***
     * 导出
     * @param response
     * @return
     */
    @GetMapping("/exportFile")
    public RestResponse exportFileYhDisasters(HttpServletResponse response,
                                              @RequestParam("projectName")String projectName, @RequestParam("fractbltname") String fractbltname,
                                              @RequestParam("province") String province, @RequestParam("city")String city, @RequestParam("area")String area) {
        RestResponse responseRest = null;
        JSONObject jsonObject = new JSONObject();
        try{
            HddcB1GeomorplyonfractbltQueryParams queryParams=new HddcB1GeomorplyonfractbltQueryParams();
            queryParams.setArea(area);
            queryParams.setCity(city);
            queryParams.setProvince(province);
            queryParams.setProjectName(projectName);
            queryParams.setFractbltname(fractbltname);
            hddcB1GeomorplyonfractbltService.exportFile(queryParams,response);
            jsonObject.put("message", "导出成功");
            responseRest = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            responseRest = RestResponse.fail(errorMessage);
        }
        return responseRest;
    }

    /**
     * 导入
     *
     * @param file
     * @param response
     * @return
     */
    @PostMapping("/importDisaster")
    public RestResponse export(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        RestResponse restResponse = null;
        try {
            String s = hddcB1GeomorplyonfractbltService.exportExcel( file, response);
            restResponse = RestResponse.succeed(s);
        } catch (Exception e) {
            String errormessage = "导入失败";
            log.error(errormessage, e);
            restResponse = RestResponse.fail(errormessage);
        }
        return restResponse;
    }

}