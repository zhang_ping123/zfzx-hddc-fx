package com.css.zfzx.sjcj.modules.hddcB1GeologySvyProjectTable.repository;

import com.css.zfzx.sjcj.modules.hddcB1GeologySvyProjectTable.repository.entity.HddcB1GeologysvyprojecttableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcB1GeologysvyprojecttableRepository extends JpaRepository<HddcB1GeologysvyprojecttableEntity, String> {
}
