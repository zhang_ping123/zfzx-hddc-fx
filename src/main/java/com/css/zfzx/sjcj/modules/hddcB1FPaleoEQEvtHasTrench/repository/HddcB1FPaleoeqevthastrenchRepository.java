package com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtHasTrench.repository;

import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtHasTrench.repository.entity.HddcB1FPaleoeqevthastrenchEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcB1FPaleoeqevthastrenchRepository extends JpaRepository<HddcB1FPaleoeqevthastrenchEntity, String> {
}
