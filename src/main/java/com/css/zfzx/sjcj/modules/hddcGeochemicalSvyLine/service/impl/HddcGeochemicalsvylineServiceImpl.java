package com.css.zfzx.sjcj.modules.hddcGeochemicalSvyLine.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltVO;
import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyLine.repository.HddcGeochemicalsvylineNativeRepository;
import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyLine.repository.HddcGeochemicalsvylineRepository;
import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyLine.repository.entity.HddcGeochemicalsvylineEntity;
import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyLine.service.HddcGeochemicalsvylineService;
import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyLine.viewobjects.HddcGeochemicalsvylineQueryParams;
import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyLine.viewobjects.HddcGeochemicalsvylineVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Service
public class HddcGeochemicalsvylineServiceImpl implements HddcGeochemicalsvylineService {

	@Autowired
    private HddcGeochemicalsvylineRepository hddcGeochemicalsvylineRepository;
    @Autowired
    private HddcGeochemicalsvylineNativeRepository hddcGeochemicalsvylineNativeRepository;

    @Override
    public JSONObject queryHddcGeochemicalsvylines(HddcGeochemicalsvylineQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcGeochemicalsvylineEntity> hddcGeochemicalsvylinePage = this.hddcGeochemicalsvylineNativeRepository.queryHddcGeochemicalsvylines(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcGeochemicalsvylinePage);
        return jsonObject;
    }


    @Override
    public HddcGeochemicalsvylineEntity getHddcGeochemicalsvyline(String id) {
        HddcGeochemicalsvylineEntity hddcGeochemicalsvyline = this.hddcGeochemicalsvylineRepository.findById(id).orElse(null);
         return hddcGeochemicalsvyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeochemicalsvylineEntity saveHddcGeochemicalsvyline(HddcGeochemicalsvylineEntity hddcGeochemicalsvyline) {
        String uuid = UUIDGenerator.getUUID();
        hddcGeochemicalsvyline.setUuid(uuid);
        hddcGeochemicalsvyline.setCreateUser(PlatformSessionUtils.getUserId());
        hddcGeochemicalsvyline.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeochemicalsvylineRepository.save(hddcGeochemicalsvyline);
        return hddcGeochemicalsvyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeochemicalsvylineEntity updateHddcGeochemicalsvyline(HddcGeochemicalsvylineEntity hddcGeochemicalsvyline) {
        HddcGeochemicalsvylineEntity entity = hddcGeochemicalsvylineRepository.findById(hddcGeochemicalsvyline.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcGeochemicalsvyline);
        hddcGeochemicalsvyline.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcGeochemicalsvyline.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeochemicalsvylineRepository.save(hddcGeochemicalsvyline);
        return hddcGeochemicalsvyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcGeochemicalsvylines(List<String> ids) {
        List<HddcGeochemicalsvylineEntity> hddcGeochemicalsvylineList = this.hddcGeochemicalsvylineRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcGeochemicalsvylineList) && hddcGeochemicalsvylineList.size() > 0) {
            for(HddcGeochemicalsvylineEntity hddcGeochemicalsvyline : hddcGeochemicalsvylineList) {
                this.hddcGeochemicalsvylineRepository.delete(hddcGeochemicalsvyline);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcGeochemicalsvylineQueryParams queryParams, HttpServletResponse response) {
        List<HddcGeochemicalsvylineEntity> yhDisasterEntities = hddcGeochemicalsvylineNativeRepository.exportYhDisasters(queryParams);
        List<HddcGeochemicalsvylineVO> list=new ArrayList<>();
        for (HddcGeochemicalsvylineEntity entity:yhDisasterEntities) {
            HddcGeochemicalsvylineVO yhDisasterVO=new HddcGeochemicalsvylineVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"地球化学探测测线-线","地球化学探测测线-线",HddcGeochemicalsvylineVO.class,"地球化学探测测线-线.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcGeochemicalsvylineVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcGeochemicalsvylineVO.class, params);
            List<HddcGeochemicalsvylineVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcGeochemicalsvylineVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcGeochemicalsvylineVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcGeochemicalsvylineVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcGeochemicalsvylineEntity yhDisasterEntity = new HddcGeochemicalsvylineEntity();
            HddcGeochemicalsvylineVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcGeochemicalsvyline(yhDisasterEntity);
        }
    }

}
