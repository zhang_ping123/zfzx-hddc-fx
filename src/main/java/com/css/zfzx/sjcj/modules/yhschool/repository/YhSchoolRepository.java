package com.css.zfzx.sjcj.modules.yhschool.repository;

import com.css.zfzx.sjcj.modules.yhschool.repository.entity.YhSchoolEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author yyd
 * @date 2020-11-03
 */
public interface YhSchoolRepository extends JpaRepository<YhSchoolEntity, String> {
}
