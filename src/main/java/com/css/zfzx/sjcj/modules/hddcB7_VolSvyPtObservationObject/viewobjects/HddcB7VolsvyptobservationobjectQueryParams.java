package com.css.zfzx.sjcj.modules.hddcB7_VolSvyPtObservationObject.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
@Data
public class HddcB7VolsvyptobservationobjectQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

}
