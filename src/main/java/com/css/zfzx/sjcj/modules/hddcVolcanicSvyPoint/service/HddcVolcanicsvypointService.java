package com.css.zfzx.sjcj.modules.hddcVolcanicSvyPoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcVolcanicSvyPoint.repository.entity.HddcVolcanicsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcVolcanicSvyPoint.viewobjects.HddcVolcanicsvypointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-27
 */

public interface HddcVolcanicsvypointService {

    public JSONObject queryHddcVolcanicsvypoints(HddcVolcanicsvypointQueryParams queryParams, int curPage, int pageSize);

    public HddcVolcanicsvypointEntity getHddcVolcanicsvypoint(String id);

    public HddcVolcanicsvypointEntity saveHddcVolcanicsvypoint(HddcVolcanicsvypointEntity hddcVolcanicsvypoint);

    public HddcVolcanicsvypointEntity updateHddcVolcanicsvypoint(HddcVolcanicsvypointEntity hddcVolcanicsvypoint);

    public void deleteHddcVolcanicsvypoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcVolcanicsvypointQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
