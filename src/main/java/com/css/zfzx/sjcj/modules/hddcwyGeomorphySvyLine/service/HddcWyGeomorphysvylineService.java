package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyLine.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyLine.repository.entity.HddcWyGeomorphysvylineEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyLine.viewobjects.HddcWyGeomorphysvylineQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

/**
 * @author lihelei
 * @date 2020-12-01
 */

public interface HddcWyGeomorphysvylineService {

    public JSONObject queryHddcWyGeomorphysvylines(HddcWyGeomorphysvylineQueryParams queryParams, int curPage, int pageSize);

    public HddcWyGeomorphysvylineEntity getHddcWyGeomorphysvyline(String uuid);

    public HddcWyGeomorphysvylineEntity saveHddcWyGeomorphysvyline(HddcWyGeomorphysvylineEntity hddcWyGeomorphysvyline);

    public HddcWyGeomorphysvylineEntity updateHddcWyGeomorphysvyline(HddcWyGeomorphysvylineEntity hddcWyGeomorphysvyline);

    public void deleteHddcWyGeomorphysvylines(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    BigInteger queryHddcWyGeomorphysvyline(HddcAppZztCountVo queryParams);

    List<HddcWyGeomorphysvylineEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);

    List<HddcWyGeomorphysvylineEntity> findAllByCreateUserAndIsValid(String userId,String isValid);

    /**
     * 逻辑删除-根据项目id删除数据
     * @param ids
     */
    void deleteByProjectId(List<String> ids);

    /**
     * 逻辑删除-根据任务id删除数据
     * @param ids
     */
    void deleteByTaskId(List<String> ids);

    String judegeParams(String[] datas);

    void savehddcWyGeomorphysvylineServiceFromShpFiles(List<List<Object>> list, String provinceName, String cityName, String areaName);
}
