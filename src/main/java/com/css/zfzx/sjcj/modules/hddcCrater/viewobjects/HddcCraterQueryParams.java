package com.css.zfzx.sjcj.modules.hddcCrater.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
@Data
public class HddcCraterQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String conename;

}
