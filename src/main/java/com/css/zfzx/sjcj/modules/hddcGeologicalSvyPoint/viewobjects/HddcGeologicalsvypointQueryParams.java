package com.css.zfzx.sjcj.modules.hddcGeologicalSvyPoint.viewobjects;

import lombok.Data;

/**
 * @author lihelei
 * @date 2020-11-27
 */
@Data
public class HddcGeologicalsvypointQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String locationname;

}
