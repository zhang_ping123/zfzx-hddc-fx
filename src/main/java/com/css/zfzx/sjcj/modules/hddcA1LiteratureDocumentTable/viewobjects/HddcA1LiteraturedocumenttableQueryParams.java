package com.css.zfzx.sjcj.modules.hddcA1LiteratureDocumentTable.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
public class HddcA1LiteraturedocumenttableQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String publication;

}
