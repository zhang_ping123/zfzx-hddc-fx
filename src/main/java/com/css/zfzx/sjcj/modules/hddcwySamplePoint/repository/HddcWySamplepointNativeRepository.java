package com.css.zfzx.sjcj.modules.hddcwySamplePoint.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwySamplePoint.repository.entity.HddcWySamplepointEntity;
import com.css.zfzx.sjcj.modules.hddcwySamplePoint.viewobjects.HddcWySamplepointQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */
public interface HddcWySamplepointNativeRepository {

    Page<HddcWySamplepointEntity> queryHddcWySamplepoints(HddcWySamplepointQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWySamplepoint(HddcAppZztCountVo queryParams);

    List<HddcWySamplepointEntity> exportSamplePoint(HddcAppZztCountVo queryParams);
}
