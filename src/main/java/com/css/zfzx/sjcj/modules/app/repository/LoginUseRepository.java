package com.css.zfzx.sjcj.modules.app.repository;

import com.css.bpm.platform.org.user.repository.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginUseRepository extends JpaRepository<UserEntity, String> {

}
