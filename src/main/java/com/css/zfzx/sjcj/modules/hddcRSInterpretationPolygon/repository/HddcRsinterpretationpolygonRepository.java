package com.css.zfzx.sjcj.modules.hddcRSInterpretationPolygon.repository;

import com.css.zfzx.sjcj.modules.hddcRSInterpretationPolygon.repository.entity.HddcRsinterpretationpolygonEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangping
 * @date 2020-11-30
 */
public interface HddcRsinterpretationpolygonRepository extends JpaRepository<HddcRsinterpretationpolygonEntity, String> {
}
