package com.css.zfzx.sjcj.modules.hddcwyDrillHole.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcDrillHole.repository.HddcDrillholeRepository;
import com.css.zfzx.sjcj.modules.hddcDrillHole.repository.entity.HddcDrillholeEntity;
import com.css.zfzx.sjcj.modules.hddcwyDrillHole.repository.HddcWyDrillholeNativeRepository;
import com.css.zfzx.sjcj.modules.hddcwyDrillHole.repository.HddcWyDrillholeRepository;
import com.css.zfzx.sjcj.modules.hddcwyDrillHole.repository.entity.HddcWyDrillholeEntity;
import com.css.zfzx.sjcj.modules.hddcwyDrillHole.service.HddcWyDrillholeService;
import com.css.zfzx.sjcj.modules.hddcwyDrillHole.viewobjects.HddcWyDrillholeQueryParams;
import com.css.zfzx.sjcj.modules.hddcwyDrillHole.viewobjects.HddcWyDrillholeVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zyb
 * @date 2020-12-01
 */
@Service
public class HddcWyDrillholeServiceImpl implements HddcWyDrillholeService {

	@Autowired
    private HddcWyDrillholeRepository hddcWyDrillholeRepository;
    @Autowired
    private HddcWyDrillholeNativeRepository hddcWyDrillholeNativeRepository;
    @Autowired
    private HddcDrillholeRepository hddcDrillholeRepository;
    @Override
    public JSONObject queryHddcWyDrillholes(HddcWyDrillholeQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcWyDrillholeEntity> hddcWyDrillholePage = this.hddcWyDrillholeNativeRepository.queryHddcWyDrillholes(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcWyDrillholePage);
        return jsonObject;
    }


    @Override
    public HddcWyDrillholeEntity getHddcWyDrillhole(String uuid) {
        HddcWyDrillholeEntity hddcWyDrillhole = this.hddcWyDrillholeRepository.findById(uuid).orElse(null);
         return hddcWyDrillhole;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyDrillholeEntity saveHddcWyDrillhole(HddcWyDrillholeEntity hddcWyDrillhole) {
        String uuid = UUIDGenerator.getUUID();
        hddcWyDrillhole.setUuid(uuid);
        if(StringUtils.isEmpty(hddcWyDrillhole.getCreateUser())){
            hddcWyDrillhole.setCreateUser(PlatformSessionUtils.getUserId());
        }
        hddcWyDrillhole.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        hddcWyDrillhole.setIsValid("1");

        HddcDrillholeEntity hddcDrillholeEntity=new HddcDrillholeEntity();
        BeanUtils.copyProperties(hddcWyDrillhole,hddcDrillholeEntity);
        hddcDrillholeEntity.setExtends4(hddcWyDrillhole.getTown());
        hddcDrillholeEntity.setExtends5(String.valueOf(hddcWyDrillhole.getLon()));
        hddcDrillholeEntity.setExtends6(String.valueOf(hddcWyDrillhole.getLat()));
        this.hddcDrillholeRepository.save(hddcDrillholeEntity);

        this.hddcWyDrillholeRepository.save(hddcWyDrillhole);
        return hddcWyDrillhole;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyDrillholeEntity updateHddcWyDrillhole(HddcWyDrillholeEntity hddcWyDrillhole) {
        HddcWyDrillholeEntity entity = hddcWyDrillholeRepository.findById(hddcWyDrillhole.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcWyDrillhole);
        hddcWyDrillhole.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcWyDrillhole.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcWyDrillholeRepository.save(hddcWyDrillhole);
        return hddcWyDrillhole;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcWyDrillholes(List<String> ids) {
        List<HddcWyDrillholeEntity> hddcWyDrillholeList = this.hddcWyDrillholeRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcWyDrillholeList) && hddcWyDrillholeList.size() > 0) {
            for(HddcWyDrillholeEntity hddcWyDrillhole : hddcWyDrillholeList) {
                this.hddcWyDrillholeRepository.delete(hddcWyDrillhole);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public BigInteger queryHddcWyDrillhole(HddcAppZztCountVo queryParams) {
        BigInteger bigInteger = hddcWyDrillholeNativeRepository.queryHddcWyDrillhole(queryParams);
        return bigInteger;
    }

    @Override
    public List<HddcWyDrillholeEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid) {
        List<HddcWyDrillholeEntity> list = hddcWyDrillholeRepository.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, isValid);
        return list;
    }


    @Override
    public void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response) {
        List<HddcWyDrillholeEntity> hddcWyDrillholeEntity = hddcWyDrillholeNativeRepository.exportHole(queryParams);
        List<HddcWyDrillholeVO> list=new ArrayList<>();
        for (HddcWyDrillholeEntity entity:hddcWyDrillholeEntity) {
            HddcWyDrillholeVO hddcWyDrillholeVO=new HddcWyDrillholeVO();
            BeanUtils.copyProperties(entity,hddcWyDrillholeVO);
            list.add(hddcWyDrillholeVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"钻孔-点","钻孔-点", HddcWyDrillholeVO.class,"钻孔-点.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcWyDrillholeVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcWyDrillholeVO.class, params);
            List<HddcWyDrillholeVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcWyDrillholeVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcWyDrillholeVO hddcWyDrillholeVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + hddcWyDrillholeVO.getRowNum() + "行" + hddcWyDrillholeVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcWyDrillholeVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcWyDrillholeEntity hddcWyDrillholeEntity = new HddcWyDrillholeEntity();
            HddcWyDrillholeVO hddcWyDrillholeVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(hddcWyDrillholeVO, hddcWyDrillholeEntity);
            saveHddcWyDrillhole(hddcWyDrillholeEntity);
        }
    }

    @Override
    public List<HddcWyDrillholeEntity> findAllByCreateUserAndIsValid(String userId,String isValid) {
        List<HddcWyDrillholeEntity> list = hddcWyDrillholeRepository.findAllByCreateUserAndIsValid(userId,isValid);
        return list;
    }


    /**
     * 逻辑删除-根据项目id删除数据
     * @param projectIds
     */
    @Override
    public void deleteByProjectId(List<String> projectIds) {
        List<HddcWyDrillholeEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyDrillholeRepository.queryHddcA1InvrgnhasmaterialtablesByProjectId(projectIds);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyDrillholeEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyDrillholeRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }
    }

    /**
     * 逻辑删除-根据任务id删除数据
     * @param taskId
     */
    @Override
    public void deleteByTaskId(List<String> taskId) {
        List<HddcWyDrillholeEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyDrillholeRepository.queryHddcA1InvrgnhasmaterialtablesByTaskId(taskId);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyDrillholeEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyDrillholeRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }

    }
}
