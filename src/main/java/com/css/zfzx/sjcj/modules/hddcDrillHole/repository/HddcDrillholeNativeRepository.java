package com.css.zfzx.sjcj.modules.hddcDrillHole.repository;

import com.css.zfzx.sjcj.modules.hddcDrillHole.repository.entity.HddcDrillholeEntity;
import com.css.zfzx.sjcj.modules.hddcDrillHole.viewobjects.HddcDrillholeQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcDrillholeNativeRepository {

    Page<HddcDrillholeEntity> queryHddcDrillholes(HddcDrillholeQueryParams queryParams, int curPage, int pageSize);

    List<HddcDrillholeEntity> exportYhDisasters(HddcDrillholeQueryParams queryParams);
}
