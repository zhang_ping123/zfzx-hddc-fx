package com.css.zfzx.sjcj.modules.hddcB7_VolcanicSvyProjectTable.repository.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-26
 */
@Data
@Entity
@Table(name="hddc_b7_volcanicsvyprojecttable")
public class HddcB7VolcanicsvyprojecttableEntity implements Serializable {

    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 成果报告报告文件编号
     */
    @Column(name="resultreport_arid")
    private String resultreportArid;
    /**
     * 工程名称
     */
    @Column(name="name")
    private String name;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 送样报表原始文件编号
     */
    @Column(name="deliverreport_arwid")
    private String deliverreportArwid;
    /**
     * 测试单位
     */
    @Column(name="testinstitute")
    private String testinstitute;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 送样报告日期
     */
    @Column(name="reportdate")
    private String reportdate;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 工程编号
     */
    @Column(name="id")
    private String id;
    /**
     * 编号
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 测量仪器及型号
     */
    @Column(name="testapparatus")
    private String testapparatus;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 工作区编号
     */
    @Column(name="workregionid")
    private String workregionid;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 送样者
     */
    @Column(name="deliverperson")
    private String deliverperson;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 采集样品总数
     */
    @Column(name="collectedsamplecount")
    private Integer collectedsamplecount;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 送样单位
     */
    @Column(name="deliverinstitute")
    private String deliverinstitute;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 获得测量结果样品总数
     */
    @Column(name="measuringresultcount")
    private Integer measuringresultcount;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 成果报告原始文件编号
     */
    @Column(name="resultreport_arwid")
    private String resultreportArwid;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 备注
     */
    @Column(name="comment_info")
    private String commentinfo;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 测试人员
     */
    @Column(name="surveyor")
    private String surveyor;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 送样总数
     */
    @Column(name="samplecount")
    private Integer samplecount;
    /**
     * 目标区编号
     */
    @Column(name="targetregionid")
    private String targetregionid;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 送样报表编号
     */
    @Column(name="deliverreport_arid")
    private String deliverreportArid;
    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;
    /**
     * 测试报告原始文件编号
     */
    @Column(name="testreport_arwid")
    private String testreportArwid;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 地质填图区编号
     */
    @Column(name="mainafsregionid")
    private String mainafsregionid;
    /**
     * 测试报告文件编号
     */
    @Column(name="testreport_arid")
    private String testreportArid;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;

}

