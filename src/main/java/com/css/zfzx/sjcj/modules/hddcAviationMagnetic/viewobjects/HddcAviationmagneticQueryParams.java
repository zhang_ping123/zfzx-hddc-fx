package com.css.zfzx.sjcj.modules.hddcAviationMagnetic.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-26
 */
@Data
public class HddcAviationmagneticQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String magnitudename;

}
