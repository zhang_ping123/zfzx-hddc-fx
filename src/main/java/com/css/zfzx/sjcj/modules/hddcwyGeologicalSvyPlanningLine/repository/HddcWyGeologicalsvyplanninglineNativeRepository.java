package com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningLine.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningLine.repository.entity.HddcWyGeologicalsvyplanninglineEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningLine.viewobjects.HddcWyGeologicalsvyplanninglineQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;

/**
 * @author zhangping
 * @date 2020-12-01
 */
public interface HddcWyGeologicalsvyplanninglineNativeRepository {

    Page<HddcWyGeologicalsvyplanninglineEntity> queryHddcWyGeologicalsvyplanninglines(HddcWyGeologicalsvyplanninglineQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyGeologicalsvyplanningline(HddcAppZztCountVo queryParams);
}
