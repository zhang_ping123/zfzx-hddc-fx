package com.css.zfzx.sjcj.modules.hddcwyVolcanicSamplePoint.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyVolcanicSamplePoint.repository.entity.HddcWyVolcanicsamplepointEntity;
import com.css.zfzx.sjcj.modules.hddcwyVolcanicSamplePoint.viewobjects.HddcWyVolcanicsamplepointQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-02
 */
public interface HddcWyVolcanicsamplepointNativeRepository {

    Page<HddcWyVolcanicsamplepointEntity> queryHddcWyVolcanicsamplepoints(HddcWyVolcanicsamplepointQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyVolcanicsamplepoint(HddcAppZztCountVo queryParams);

    List<HddcWyVolcanicsamplepointEntity> exportVolcanPoint(HddcAppZztCountVo queryParams);
}
