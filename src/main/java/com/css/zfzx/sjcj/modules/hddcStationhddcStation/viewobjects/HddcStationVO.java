package com.css.zfzx.sjcj.modules.hddcStationhddcStation.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Data
public class HddcStationVO implements Serializable {

    /**
     * 台站纬度
     */
    @Excel(name = "台站纬度", orderNum = "4")
    private Double stationlatitude;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 起始时间
     */
    @Excel(name = "起始时间", orderNum = "5")
    private String starttime;
    /**
     * 台站经度
     */
    @Excel(name = "台站经度", orderNum = "6")
    private Double stationlongitude;
    /**
     * 所在城市
     */
    @Excel(name = "所在城市", orderNum = "17")
    private String city;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 仪器南北响应灵敏度 [count*s/μm]
     */
    @Excel(name = "仪器南北响应灵敏度 [count*s/μm]", orderNum = "7")
    private Integer sensitivityns;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 仪器类型
     */
    @Excel(name = "仪器类型", orderNum = "8")
    private String instrument;
    /**
     * 测定日期
     */
    @Excel(name = "测定日期", orderNum = "9")
    private String testtime;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 仪器东西响应灵敏度 [count*s/μm]
     */
    @Excel(name = "仪器东西响应灵敏度 [count*s/μm]", orderNum = "10")
    private Integer sensitivityew;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "11")
    private String commentInfo;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 台站类型
     */
    @Excel(name = "台站类型", orderNum = "12")
    private String stationtype;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 台基岩性
     */
    @Excel(name = "台基岩性", orderNum = "13")
    private String lithlogy;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 乡
     */
    private String town;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 台站编码
     */
    private String id;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 台站名称
     */
    @Excel(name = "台站名称", orderNum = "14")
    private String stationname;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备注
     */
    private String remark;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 台站海拔高度[米]
     */
    @Excel(name = "台站海拔高度[米]", orderNum = "15")
    private Double elevation;
    /**
     * 仪器垂直响应灵敏度 [count*s/μm]
     */
    @Excel(name = "仪器垂直响应灵敏度 [count*s/μm]", orderNum = "16")
    private Integer sensitivityv;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 市(编码需要)
     */
    @Excel(name = "市(编码需要)", orderNum = "2")
    private String cityCode;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段2
     */
    private String extends2;

    private String provinceName;
    private String cityCodeName;
    private String areaName;
    private String rowNum;
    private String errorMsg;
}