package com.css.zfzx.sjcj.modules.hddcSamplePoint.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcSamplePoint.repository.HddcSamplepointNativeRepository;
import com.css.zfzx.sjcj.modules.hddcSamplePoint.repository.HddcSamplepointRepository;
import com.css.zfzx.sjcj.modules.hddcSamplePoint.repository.entity.HddcSamplepointEntity;
import com.css.zfzx.sjcj.modules.hddcSamplePoint.service.HddcSamplepointService;
import com.css.zfzx.sjcj.modules.hddcSamplePoint.viewobjects.HddcSamplepointQueryParams;
import com.css.zfzx.sjcj.modules.hddcSamplePoint.viewobjects.HddcSamplepointVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Service
public class HddcSamplepointServiceImpl implements HddcSamplepointService {

	@Autowired
    private HddcSamplepointRepository hddcSamplepointRepository;
    @Autowired
    private HddcSamplepointNativeRepository hddcSamplepointNativeRepository;

    @Override
    public JSONObject queryHddcSamplepoints(HddcSamplepointQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcSamplepointEntity> hddcSamplepointPage = this.hddcSamplepointNativeRepository.queryHddcSamplepoints(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcSamplepointPage);
        return jsonObject;
    }


    @Override
    public HddcSamplepointEntity getHddcSamplepoint(String id) {
        HddcSamplepointEntity hddcSamplepoint = this.hddcSamplepointRepository.findById(id).orElse(null);
         return hddcSamplepoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcSamplepointEntity saveHddcSamplepoint(HddcSamplepointEntity hddcSamplepoint) {
        String uuid = UUIDGenerator.getUUID();
        hddcSamplepoint.setUuid(uuid);
        hddcSamplepoint.setCreateUser(PlatformSessionUtils.getUserId());
        hddcSamplepoint.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcSamplepointRepository.save(hddcSamplepoint);
        return hddcSamplepoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcSamplepointEntity updateHddcSamplepoint(HddcSamplepointEntity hddcSamplepoint) {
        HddcSamplepointEntity entity = hddcSamplepointRepository.findById(hddcSamplepoint.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcSamplepoint);
        hddcSamplepoint.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcSamplepoint.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcSamplepointRepository.save(hddcSamplepoint);
        return hddcSamplepoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcSamplepoints(List<String> ids) {
        List<HddcSamplepointEntity> hddcSamplepointList = this.hddcSamplepointRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcSamplepointList) && hddcSamplepointList.size() > 0) {
            for(HddcSamplepointEntity hddcSamplepoint : hddcSamplepointList) {
                this.hddcSamplepointRepository.delete(hddcSamplepoint);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcSamplepointQueryParams queryParams, HttpServletResponse response) {
        List<HddcSamplepointEntity> yhDisasterEntities = hddcSamplepointNativeRepository.exportYhDisasters(queryParams);
        List<HddcSamplepointVO> list=new ArrayList<>();
        for (HddcSamplepointEntity entity:yhDisasterEntities) {
            HddcSamplepointVO yhDisasterVO=new HddcSamplepointVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "采样点-点", "采样点-点", HddcSamplepointVO.class, "采样点-点.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcSamplepointVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcSamplepointVO.class, params);
            List<HddcSamplepointVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcSamplepointVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcSamplepointVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcSamplepointVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcSamplepointEntity yhDisasterEntity = new HddcSamplepointEntity();
            HddcSamplepointVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcSamplepoint(yhDisasterEntity);
        }
    }

}
