package com.css.zfzx.sjcj.modules.hddcB7_VolcanicSvyProjectTable.repository;

import com.css.zfzx.sjcj.modules.hddcB7_VolcanicSvyProjectTable.repository.entity.HddcB7VolcanicsvyprojecttableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-26
 */
public interface HddcB7VolcanicsvyprojecttableRepository extends JpaRepository<HddcB7VolcanicsvyprojecttableEntity, String> {
}
