package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.repository.impl;

import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.org.dept.repository.entity.DeptEntity;
import com.css.bpm.platform.org.division.repository.DivisionRepository;
import com.css.bpm.platform.org.division.repository.entity.DivisionEntity;
import com.css.bpm.platform.org.role.repository.entity.RoleEntity;
import com.css.bpm.platform.utils.PlatformSessionUtils;
import com.css.zfzx.sjcj.common.utils.ServerUtil;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.repository.HddcWyGeomorphysvypointNativeRepository;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.repository.entity.HddcWyGeomorphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.viewobjects.HddcWyGeomorphysvypointQueryParams;
import com.css.bpm.platform.utils.PlatformObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
/**
 * @author lihelei
 * @date 2020-12-01
 */
@Repository
public class HddcWyGeomorphysvypointNativeRepositoryImpl implements HddcWyGeomorphysvypointNativeRepository {
    @PersistenceContext
    private EntityManager em;

    @Value("${role.superCode}")
    private String superCode;
    @Value("${role.provinceCode}")
    private String provinceCode;
    @Value("${role.cityCode}")
    private String cityCode;
    @Value("${role.areaCode}")
    private String areaCode;
    @Autowired
    private DivisionRepository divisionRepository;

    private static final Logger log = LoggerFactory.getLogger(HddcWyGeomorphysvypointNativeRepositoryImpl.class);


    @Override
    public Page<HddcWyGeomorphysvypointEntity> queryHddcWyGeomorphysvypoints(HddcWyGeomorphysvypointQueryParams queryParams, int curPage, int pageSize) {
        StringBuilder sql = new StringBuilder("select * from hddc_wy_geomorphysvypoint ");
        StringBuilder whereSql = new StringBuilder(" where 1=1 ");
        if(!PlatformObjectUtils.isEmpty(queryParams.getProvince())) {
            whereSql.append(" and province = :province");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getCity())) {
            whereSql.append(" and city = :city");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getArea())) {
            whereSql.append(" and area = :area");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getProjectName())) {
            whereSql.append(" and project_name like :projectName");
        }

        String userId = PlatformSessionUtils.getUserId();
        List<RoleEntity> roles = PlatformAPI.getOrgAPI().getUserAPI().getRoles(userId);
        //是否为超级管理员
        boolean containtRole = ServerUtil.isContaintRole(roles, superCode);
        //是否为省级管理员
        boolean containtRolePro = ServerUtil.isContaintRole(roles, provinceCode);
        //是否为市级管理员
        boolean containtRoleCity = ServerUtil.isContaintRole(roles, cityCode);
        //是否为县级管理员
        boolean containtRoleArea= ServerUtil.isContaintRole(roles, areaCode);
        if(!containtRole){
            if (!containtRolePro && !containtRoleCity && !containtRoleArea) {
                whereSql.append(" and create_user =:userId");
            }
            if(containtRolePro){
                whereSql.append(" and province like :authProvince");
            }
            if(containtRoleCity){
                whereSql.append(" and city like :authCity");
            }
            if(containtRoleArea){
                whereSql.append(" and area like :authArea");
            }
        }

        Query query = this.em.createNativeQuery(sql.append(whereSql).toString(), HddcWyGeomorphysvypointEntity.class);
        StringBuilder countSql = new StringBuilder("select count(*) from hddc_wy_geomorphysvypoint ");
        Query countQuery = this.em.createNativeQuery(countSql.append(whereSql).toString());
        if(!PlatformObjectUtils.isEmpty(queryParams.getProvince())) {
            query.setParameter("province", queryParams.getProvince());
            countQuery.setParameter("province", queryParams.getProvince());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getCity())) {
            query.setParameter("city", queryParams.getCity());
            countQuery.setParameter("city", queryParams.getCity());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getArea())) {
            query.setParameter("area", queryParams.getArea());
            countQuery.setParameter("area", queryParams.getArea());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getProjectName())) {
            query.setParameter("projectName", "%" + queryParams.getProjectName() + "%");
            countQuery.setParameter("projectName", "%" + queryParams.getProjectName() + "%");
        }

        if(!containtRole){
            if (!containtRolePro && !containtRoleCity && !containtRoleArea) {
                query.setParameter("userId",PlatformSessionUtils.getUserId());
                countQuery.setParameter("userId", PlatformSessionUtils.getUserId());
            }
            DeptEntity mainDept = PlatformAPI.getOrgAPI().getUserAPI().getMainDept(userId);
            String divisionId = mainDept.getDivisionId();
            DivisionEntity validDivisionById = divisionRepository.findValidDivisionById(divisionId);
            String divisionName = validDivisionById.getDivisionName();
            if(containtRolePro){
                query.setParameter("authProvince","%"+divisionName+"%");
                countQuery.setParameter("authProvince","%"+divisionName+"%");
            }
            if(containtRoleCity){
                query.setParameter("authCity","%"+divisionName+"%");
                countQuery.setParameter("authCity","%"+divisionName+"%");
            }
            if(containtRoleArea){
                query.setParameter("authArea","%"+divisionName+"%");
                countQuery.setParameter("authArea","%"+divisionName+"%");
            }
        }

        Pageable pageable = PageRequest.of(curPage - 1, pageSize);
        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());
        List<HddcWyGeomorphysvypointEntity> list = query.getResultList();
        BigInteger count = (BigInteger) countQuery.getSingleResult();
        Page<HddcWyGeomorphysvypointEntity> hddcWyGeomorphysvypointPage = new PageImpl<>(list, pageable, count.longValue());
        return hddcWyGeomorphysvypointPage;
    }


    @Override
    public BigInteger queryHddcWyGeomorphysvypoint(HddcAppZztCountVo queryParams) {
        StringBuilder sql = new StringBuilder("select * from hddc_wy_geomorphysvypoint ");
        StringBuilder whereSql = new StringBuilder(" where 1=1 ");
        if(!PlatformObjectUtils.isEmpty(queryParams.getProvince())) {
            whereSql.append(" and province = :province");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getCity())) {
            whereSql.append(" and city = :city");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getArea())) {
            whereSql.append(" and area = :area");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getProjectName())) {
            whereSql.append(" and project_name like :projectName");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getTaskName())) {
            whereSql.append(" and task_name like :taskName");
        }

        String startDate=queryParams.getStartTime();
        String endDate=queryParams.getEndTime();

        // 时间段
        Date start = null;
        Date end = null;
        if (!StringUtils.isEmpty(startDate) || !StringUtils.isEmpty(endDate)){
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                // 结束时间为空
                if (StringUtils.isEmpty(endDate)) {
                    startDate += " 00:00:00";
                    start = sdf.parse(startDate);
                    whereSql.append(" and create_time >= :start");
                }
                // 开始时间为空
                else if (StringUtils.isEmpty(startDate)) {
                    endDate += " 23:59:59";
                    end = sdf.parse(endDate);
                    whereSql.append(" and create_time <= :end");
                }
                // 都不为空
                else {
                    startDate += " 00:00:00";
                    start = sdf.parse(startDate);
                    endDate += " 23:59:59";
                    end = sdf.parse(endDate);
                    whereSql.append(" and create_time between :start and :end");
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        String userId = PlatformSessionUtils.getUserId();
        List<RoleEntity> roles = PlatformAPI.getOrgAPI().getUserAPI().getRoles(userId);
        //是否为超级管理员
        boolean containtRole = ServerUtil.isContaintRole(roles, superCode);
        //是否为省级管理员
        boolean containtRolePro = ServerUtil.isContaintRole(roles, provinceCode);
        //是否为市级管理员
        boolean containtRoleCity = ServerUtil.isContaintRole(roles, cityCode);
        //是否为县级管理员
        boolean containtRoleArea= ServerUtil.isContaintRole(roles, areaCode);
        if(!containtRole){
            if (!containtRolePro && !containtRoleCity && !containtRoleArea) {
                whereSql.append(" and create_user =:userId");
            }
            if(containtRolePro){
                whereSql.append(" and province like :authProvince");
            }
            if(containtRoleCity){
                whereSql.append(" and city like :authCity");
            }
            if(containtRoleArea){
                whereSql.append(" and area like :authArea");
            }
        }

        Query query = this.em.createNativeQuery(sql.append(whereSql).toString(), HddcWyGeomorphysvypointEntity.class);
        StringBuilder countSql = new StringBuilder("select count(*) from hddc_wy_geomorphysvypoint ");
        Query countQuery = this.em.createNativeQuery(countSql.append(whereSql).toString());
        if(!PlatformObjectUtils.isEmpty(queryParams.getProvince())) {
            query.setParameter("province", queryParams.getProvince());
            countQuery.setParameter("province", queryParams.getProvince());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getCity())) {
            query.setParameter("city", queryParams.getCity());
            countQuery.setParameter("city", queryParams.getCity());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getArea())) {
            query.setParameter("area", queryParams.getArea());
            countQuery.setParameter("area", queryParams.getArea());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getProjectName())) {
            query.setParameter("projectName", "%" + queryParams.getProjectName() + "%");
            countQuery.setParameter("projectName", "%" + queryParams.getProjectName() + "%");
        }

        if(!PlatformObjectUtils.isEmpty(queryParams.getTaskName())) {
            query.setParameter("taskName", "%" + queryParams.getTaskName() + "%");
            countQuery.setParameter("taskName", "%" + queryParams.getTaskName() + "%");
        }

        // 时间
        if (!StringUtils.isEmpty(startDate) || !StringUtils.isEmpty(endDate)){
            // 结束时间为空
            if (StringUtils.isEmpty(endDate)) {
                countQuery.setParameter("start", start, TemporalType.DATE);
            }
            // 开始时间为空
            else if (StringUtils.isEmpty(startDate)) {
                countQuery.setParameter("end", end, TemporalType.DATE);
            }
            // 都不为空
            else {
                countQuery.setParameter("start", start, TemporalType.DATE);
                countQuery.setParameter("end", end, TemporalType.DATE);
            }
        }

        if(!containtRole){
            if (!containtRolePro && !containtRoleCity && !containtRoleArea) {
                query.setParameter("userId",PlatformSessionUtils.getUserId());
                countQuery.setParameter("userId", PlatformSessionUtils.getUserId());
            }
            DeptEntity mainDept = PlatformAPI.getOrgAPI().getUserAPI().getMainDept(userId);
            String divisionId = mainDept.getDivisionId();
            DivisionEntity validDivisionById = divisionRepository.findValidDivisionById(divisionId);
            String divisionName = validDivisionById.getDivisionName();
            if(containtRolePro){
                query.setParameter("authProvince","%"+divisionName+"%");
                countQuery.setParameter("authProvince","%"+divisionName+"%");
            }
            if(containtRoleCity){
                query.setParameter("authCity","%"+divisionName+"%");
                countQuery.setParameter("authCity","%"+divisionName+"%");
            }
            if(containtRoleArea){
                query.setParameter("authArea","%"+divisionName+"%");
                countQuery.setParameter("authArea","%"+divisionName+"%");
            }
        }

        BigInteger count = (BigInteger) countQuery.getSingleResult();
        return count;
    }

    @Override
    public List<HddcWyGeomorphysvypointEntity> exportGeomorPoint(HddcAppZztCountVo queryParams) {
        StringBuilder sql = new StringBuilder("select * from hddc_wy_geomorphysvypoint ");
        StringBuilder whereSql = new StringBuilder(" where 1=1 ");
        if(!PlatformObjectUtils.isEmpty(queryParams.getProvince())) {
            whereSql.append(" and province = :province");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getCity())) {
            whereSql.append(" and city = :city");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getArea())) {
            whereSql.append(" and area = :area");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getProjectName())) {
            whereSql.append(" and project_name like :projectName");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getTaskName())) {
            whereSql.append(" and task_name like :taskName");
        }

        String startDate=queryParams.getStartTime();
        String endDate=queryParams.getEndTime();

        // 时间段
        Date start = null;
        Date end = null;
        if (!StringUtils.isEmpty(startDate) || !StringUtils.isEmpty(endDate)){
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                // 结束时间为空
                if (StringUtils.isEmpty(endDate)) {
                    startDate += " 00:00:00";
                    start = sdf.parse(startDate);
                    whereSql.append(" and create_time >= :start");
                }
                // 开始时间为空
                else if (StringUtils.isEmpty(startDate)) {
                    endDate += " 23:59:59";
                    end = sdf.parse(endDate);
                    whereSql.append(" and create_time <= :end");
                }
                // 都不为空
                else {
                    startDate += " 00:00:00";
                    start = sdf.parse(startDate);
                    endDate += " 23:59:59";
                    end = sdf.parse(endDate);
                    whereSql.append(" and create_time between :start and :end");
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        String userId = PlatformSessionUtils.getUserId();
        List<RoleEntity> roles = PlatformAPI.getOrgAPI().getUserAPI().getRoles(userId);
        //是否为超级管理员
        boolean containtRole = ServerUtil.isContaintRole(roles, superCode);
        //是否为省级管理员
        boolean containtRolePro = ServerUtil.isContaintRole(roles, provinceCode);
        //是否为市级管理员
        boolean containtRoleCity = ServerUtil.isContaintRole(roles, cityCode);
        //是否为县级管理员
        boolean containtRoleArea= ServerUtil.isContaintRole(roles, areaCode);
        if(!containtRole){
            if (!containtRolePro && !containtRoleCity && !containtRoleArea) {
                whereSql.append(" and create_user =:userId");
            }
            if(containtRolePro){
                whereSql.append(" and province like :authProvince");
            }
            if(containtRoleCity){
                whereSql.append(" and city like :authCity");
            }
            if(containtRoleArea){
                whereSql.append(" and area like :authArea");
            }
        }

        Query query = this.em.createNativeQuery(sql.append(whereSql).toString(), HddcWyGeomorphysvypointEntity.class);

        if(!PlatformObjectUtils.isEmpty(queryParams.getProvince())) {
            query.setParameter("province", queryParams.getProvince());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getCity())) {
            query.setParameter("city", queryParams.getCity());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getArea())) {
            query.setParameter("area", queryParams.getArea());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getProjectName())) {
            query.setParameter("projectName", "%" + queryParams.getProjectName() + "%");
        }

        if(!PlatformObjectUtils.isEmpty(queryParams.getTaskName())) {
            query.setParameter("taskName", "%" + queryParams.getTaskName() + "%");
        }

        // 时间
        if (!StringUtils.isEmpty(startDate) || !StringUtils.isEmpty(endDate)){
            // 结束时间为空
            if (StringUtils.isEmpty(endDate)) {
                query.setParameter("start", start, TemporalType.DATE);
            }
            // 开始时间为空
            else if (StringUtils.isEmpty(startDate)) {
                query.setParameter("end", end, TemporalType.DATE);
            }
            // 都不为空
            else {
                query.setParameter("start", start, TemporalType.DATE);
                query.setParameter("end", end, TemporalType.DATE);
            }
        }

        if(!containtRole){
            if (!containtRolePro && !containtRoleCity && !containtRoleArea) {
                query.setParameter("userId",PlatformSessionUtils.getUserId());
                //countQuery.setParameter("userId", PlatformSessionUtils.getUserId());
            }
            DeptEntity mainDept = PlatformAPI.getOrgAPI().getUserAPI().getMainDept(userId);
            String divisionId = mainDept.getDivisionId();
            DivisionEntity validDivisionById = divisionRepository.findValidDivisionById(divisionId);
            String divisionName = validDivisionById.getDivisionName();
            if(containtRolePro){
                query.setParameter("authProvince","%"+divisionName+"%");
                //countQuery.setParameter("authProvince","%"+divisionName+"%");
            }
            if(containtRoleCity){
                query.setParameter("authCity","%"+divisionName+"%");
                //countQuery.setParameter("authCity","%"+divisionName+"%");
            }
            if(containtRoleArea){
                query.setParameter("authArea","%"+divisionName+"%");
                //countQuery.setParameter("authArea","%"+divisionName+"%");
            }
        }

        List<HddcWyGeomorphysvypointEntity> list = query.getResultList();
        return list;
    }
}
