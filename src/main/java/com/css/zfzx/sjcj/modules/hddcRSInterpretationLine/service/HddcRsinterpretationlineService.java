package com.css.zfzx.sjcj.modules.hddcRSInterpretationLine.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcRSInterpretationLine.repository.entity.HddcRsinterpretationlineEntity;
import com.css.zfzx.sjcj.modules.hddcRSInterpretationLine.viewobjects.HddcRsinterpretationlineQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangping
 * @date 2020-11-30
 */

public interface HddcRsinterpretationlineService {

    public JSONObject queryHddcRsinterpretationlines(HddcRsinterpretationlineQueryParams queryParams, int curPage, int pageSize);

    public HddcRsinterpretationlineEntity getHddcRsinterpretationline(String id);

    public HddcRsinterpretationlineEntity saveHddcRsinterpretationline(HddcRsinterpretationlineEntity hddcRsinterpretationline);

    public HddcRsinterpretationlineEntity updateHddcRsinterpretationline(HddcRsinterpretationlineEntity hddcRsinterpretationline);

    public void deleteHddcRsinterpretationlines(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcRsinterpretationlineQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
