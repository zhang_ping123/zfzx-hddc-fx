package com.css.zfzx.sjcj.modules.hddccjfilemanage.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.bpm.platform.utils.PlatformPageUtils;
import com.css.zfzx.sjcj.modules.hddccjfilemanage.repository.entity.HddcFileManageEntity;
import com.css.zfzx.sjcj.modules.hddccjfilemanage.service.HddcFileManageService;
import com.css.zfzx.sjcj.modules.hddccjfilemanage.viewobjects.HddcFileManageQueryParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-10
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcFileManages")
public class HddcFileManageController {
    @Autowired
    private HddcFileManageService hddcFileManageService;

    @GetMapping("/queryHddcFileManages")
    public RestResponse queryHddcFileManages(HttpServletRequest request, HddcFileManageQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcFileManageService.queryHddcFileManages(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcFileManage(@PathVariable String id) {
        RestResponse response = null;
        try{
            HddcFileManageEntity hddcFileManage = hddcFileManageService.getHddcFileManage(id);
            response = RestResponse.succeed(hddcFileManage);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcFileManage(@RequestBody HddcFileManageEntity hddcFileManage) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcFileManageService.saveHddcFileManage(hddcFileManage);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcFileManage(@RequestBody HddcFileManageEntity hddcFileManage)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcFileManageService.updateHddcFileManage(hddcFileManage);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcFileManages(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcFileManageService.deleteHddcFileManages(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcFileManageService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }
    @PostMapping("/upload")
    public RestResponse upload(@RequestParam("file") MultipartFile file,@RequestParam("filenumber")String filenumber) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            int mag=hddcFileManageService.upload(file,filenumber);
            if (mag==2) {
                json.put("message", "上传成功!");
            }else if (mag==0){
                json.put("message", "编号已存在!");
            }
            else if (mag==1){
                json.put("message", "上传失败!");
            }
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "上传失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/download/{name}")
    public RestResponse exportExcel(@PathVariable String name, HttpServletResponse response) {
        RestResponse restResponse = null;
        JSONObject json = new JSONObject();
        try {
            hddcFileManageService.download(name, response);
            json.put("message", "下载成功!");
            restResponse = RestResponse.succeed(json);
        } catch (Exception e) {
            String errorMsg = "下载失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }
}