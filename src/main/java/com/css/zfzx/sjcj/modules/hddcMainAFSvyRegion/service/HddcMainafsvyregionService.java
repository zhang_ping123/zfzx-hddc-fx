package com.css.zfzx.sjcj.modules.hddcMainAFSvyRegion.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcMainAFSvyRegion.repository.entity.HddcMainafsvyregionEntity;
import com.css.zfzx.sjcj.modules.hddcMainAFSvyRegion.viewobjects.HddcMainafsvyregionQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-14
 */

public interface HddcMainafsvyregionService {

    public JSONObject queryHddcMainafsvyregions(HddcMainafsvyregionQueryParams queryParams, int curPage, int pageSize);

    public HddcMainafsvyregionEntity getHddcMainafsvyregion(String id);

    public HddcMainafsvyregionEntity saveHddcMainafsvyregion(HddcMainafsvyregionEntity hddcMainafsvyregion);

    public HddcMainafsvyregionEntity updateHddcMainafsvyregion(HddcMainafsvyregionEntity hddcMainafsvyregion);

    public void deleteHddcMainafsvyregions(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcMainafsvyregionQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
