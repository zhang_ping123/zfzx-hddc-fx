package com.css.zfzx.sjcj.modules.hddcwyGeophysvypoint.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcGeophySvyLine.repository.entity.HddcGeophysvylineEntity;
import com.css.zfzx.sjcj.modules.hddcGeophySvyPoint.repository.HddcGeophysvypointRepository;
import com.css.zfzx.sjcj.modules.hddcGeophySvyPoint.repository.entity.HddcGeophysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeophysvypoint.repository.HddcWyGeophysvypointNativeRepository;
import com.css.zfzx.sjcj.modules.hddcwyGeophysvypoint.repository.HddcWyGeophysvypointRepository;
import com.css.zfzx.sjcj.modules.hddcwyGeophysvypoint.repository.entity.HddcWyGeophysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeophysvypoint.service.HddcWyGeophysvypointService;
import com.css.zfzx.sjcj.modules.hddcwyGeophysvypoint.viewobjects.HddcWyGeophysvypointQueryParams;
import com.css.zfzx.sjcj.modules.hddcwyGeophysvypoint.viewobjects.HddcWyGeophysvypointVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zyb
 * @date 2020-12-01
 */
@Service
public class HddcWyGeophysvypointServiceImpl implements HddcWyGeophysvypointService {

	@Autowired
    private HddcWyGeophysvypointRepository hddcWyGeophysvypointRepository;
    @Autowired
    private HddcWyGeophysvypointNativeRepository hddcWyGeophysvypointNativeRepository;
    @Autowired
    private HddcGeophysvypointRepository hddcGeophysvypointRepository;

    @Override
    public JSONObject queryHddcWyGeophysvypoints(HddcWyGeophysvypointQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcWyGeophysvypointEntity> hddcWyGeophysvypointPage = this.hddcWyGeophysvypointNativeRepository.queryHddcWyGeophysvypoints(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcWyGeophysvypointPage);
        return jsonObject;
    }


    @Override
    public HddcWyGeophysvypointEntity getHddcWyGeophysvypoint(String uuid) {
        HddcWyGeophysvypointEntity hddcWyGeophysvypoint = this.hddcWyGeophysvypointRepository.findById(uuid).orElse(null);
        return hddcWyGeophysvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyGeophysvypointEntity saveHddcWyGeophysvypoint(HddcWyGeophysvypointEntity hddcWyGeophysvypoint) {
        String uuid = UUIDGenerator.getUUID();
        hddcWyGeophysvypoint.setUuid(uuid);
        if(StringUtils.isEmpty(hddcWyGeophysvypoint.getCreateUser())){
            hddcWyGeophysvypoint.setCreateUser(PlatformSessionUtils.getUserId());
        }
        hddcWyGeophysvypoint.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        hddcWyGeophysvypoint.setIsValid("1");

        HddcGeophysvypointEntity hddcGeophysvypointEntity=new HddcGeophysvypointEntity();
        BeanUtils.copyProperties(hddcWyGeophysvypoint,hddcGeophysvypointEntity);
        hddcGeophysvypointEntity.setExtends4(hddcWyGeophysvypoint.getTown());
        hddcGeophysvypointEntity.setExtends5(String.valueOf(hddcWyGeophysvypoint.getLon()));
        hddcGeophysvypointEntity.setExtends6(String.valueOf(hddcWyGeophysvypoint.getLat()));
        this.hddcGeophysvypointRepository.save(hddcGeophysvypointEntity);

        this.hddcWyGeophysvypointRepository.save(hddcWyGeophysvypoint);
        return hddcWyGeophysvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyGeophysvypointEntity updateHddcWyGeophysvypoint(HddcWyGeophysvypointEntity hddcWyGeophysvypoint) {
        HddcWyGeophysvypointEntity entity = hddcWyGeophysvypointRepository.findById(hddcWyGeophysvypoint.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcWyGeophysvypoint);
        hddcWyGeophysvypoint.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcWyGeophysvypoint.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcWyGeophysvypointRepository.save(hddcWyGeophysvypoint);
        return hddcWyGeophysvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcWyGeophysvypoints(List<String> ids) {
        List<HddcWyGeophysvypointEntity> hddcWyGeophysvypointList = this.hddcWyGeophysvypointRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcWyGeophysvypointList) && hddcWyGeophysvypointList.size() > 0) {
            for(HddcWyGeophysvypointEntity hddcWyGeophysvypoint : hddcWyGeophysvypointList) {
                this.hddcWyGeophysvypointRepository.delete(hddcWyGeophysvypoint);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public BigInteger queryHddcWyGeophysvypoint(HddcAppZztCountVo queryParams) {
        BigInteger bigInteger = hddcWyGeophysvypointNativeRepository.queryHddcWyGeophysvypoint(queryParams);
        return bigInteger;
    }

    @Override
    public List<HddcWyGeophysvypointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid) {
        List<HddcWyGeophysvypointEntity> list = hddcWyGeophysvypointRepository.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, isValid);
        return list;
    }


    @Override
    public void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response) {
        List<HddcWyGeophysvypointEntity> hddcWyGeophysvypointEntity = hddcWyGeophysvypointNativeRepository.exportPhyPoint(queryParams);
        List<HddcWyGeophysvypointVO> list=new ArrayList<>();
        for (HddcWyGeophysvypointEntity entity:hddcWyGeophysvypointEntity) {
            HddcWyGeophysvypointVO hddcWyGeophysvypointVO=new HddcWyGeophysvypointVO();
            BeanUtils.copyProperties(entity,hddcWyGeophysvypointVO);
            list.add(hddcWyGeophysvypointVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"地球物理测点-点","地球物理测点-点", HddcWyGeophysvypointVO.class,"地球物理测点-点.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcWyGeophysvypointVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcWyGeophysvypointVO.class, params);
            List<HddcWyGeophysvypointVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcWyGeophysvypointVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcWyGeophysvypointVO hddcWyGeophysvypointVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + hddcWyGeophysvypointVO.getRowNum() + "行" + hddcWyGeophysvypointVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcWyGeophysvypointVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcWyGeophysvypointEntity hddcWyGeophysvypointEntity = new HddcWyGeophysvypointEntity();
            HddcWyGeophysvypointVO hddcWyGeophysvypointVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(hddcWyGeophysvypointVO, hddcWyGeophysvypointEntity);
            saveHddcWyGeophysvypoint(hddcWyGeophysvypointEntity);
        }
    }

    @Override
    public List<HddcWyGeophysvypointEntity> findAllByCreateUserAndIsValid(String userId, String isValid) {
        List<HddcWyGeophysvypointEntity> list = hddcWyGeophysvypointRepository.findAllByCreateUserAndIsValid(userId, isValid);
        return list;
    }


    /**
     * 逻辑删除-根据项目id删除数据
     * @param projectIds
     */
    @Override
    public void deleteByProjectId(List<String> projectIds) {
        List<HddcWyGeophysvypointEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyGeophysvypointRepository.queryHddcA1InvrgnhasmaterialtablesByProjectId(projectIds);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyGeophysvypointEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyGeophysvypointRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }
    }

    /**
     * 逻辑删除-根据任务id删除数据
     * @param taskId
     */
    @Override
    public void deleteByTaskId(List<String> taskId) {
        List<HddcWyGeophysvypointEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyGeophysvypointRepository.queryHddcA1InvrgnhasmaterialtablesByTaskId(taskId);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyGeophysvypointEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyGeophysvypointRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }

    }


}
