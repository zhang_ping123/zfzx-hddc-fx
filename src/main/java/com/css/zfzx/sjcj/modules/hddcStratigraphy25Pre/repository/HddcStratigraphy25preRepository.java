package com.css.zfzx.sjcj.modules.hddcStratigraphy25Pre.repository;

import com.css.zfzx.sjcj.modules.hddcStratigraphy25Pre.repository.entity.HddcStratigraphy25preEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcStratigraphy25preRepository extends JpaRepository<HddcStratigraphy25preEntity, String> {
}
