package com.css.zfzx.sjcj.modules.hddcB7_VolcanicSvyProjectTable.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-11-26
 */
@Data
public class HddcB7VolcanicsvyprojecttableQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String name;

}
