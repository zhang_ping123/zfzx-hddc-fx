package com.css.zfzx.sjcj.modules.hddcGeomorphySvySamplePoint.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
public class HddcGeomorphysvysamplepointQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

}
