package com.css.zfzx.sjcj.modules.app.vo;

import lombok.Data;

@Data
public class UpdatePwdVO {
    private String userId;
    private String oldPwd;
    private String newPwd;
}
