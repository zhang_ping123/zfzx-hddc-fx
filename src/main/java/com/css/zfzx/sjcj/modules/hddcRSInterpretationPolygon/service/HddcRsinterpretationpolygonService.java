package com.css.zfzx.sjcj.modules.hddcRSInterpretationPolygon.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcRSInterpretationPolygon.repository.entity.HddcRsinterpretationpolygonEntity;
import com.css.zfzx.sjcj.modules.hddcRSInterpretationPolygon.viewobjects.HddcRsinterpretationpolygonQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangping
 * @date 2020-11-30
 */

public interface HddcRsinterpretationpolygonService {

    public JSONObject queryHddcRsinterpretationpolygons(HddcRsinterpretationpolygonQueryParams queryParams, int curPage, int pageSize);

    public HddcRsinterpretationpolygonEntity getHddcRsinterpretationpolygon(String id);

    public HddcRsinterpretationpolygonEntity saveHddcRsinterpretationpolygon(HddcRsinterpretationpolygonEntity hddcRsinterpretationpolygon);

    public HddcRsinterpretationpolygonEntity updateHddcRsinterpretationpolygon(HddcRsinterpretationpolygonEntity hddcRsinterpretationpolygon);

    public void deleteHddcRsinterpretationpolygons(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcRsinterpretationpolygonQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
