package com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPoint.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPoint.repository.entity.HddcWyGeologicalsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPoint.viewobjects.HddcWyGeologicalsvypointQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */
public interface HddcWyGeologicalsvypointNativeRepository {

    Page<HddcWyGeologicalsvypointEntity> queryHddcWyGeologicalsvypoints(HddcWyGeologicalsvypointQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyGeologicalsvypoint(HddcAppZztCountVo queryParams);

    List<HddcWyGeologicalsvypointEntity> exportPoint(HddcAppZztCountVo queryParams);
}
