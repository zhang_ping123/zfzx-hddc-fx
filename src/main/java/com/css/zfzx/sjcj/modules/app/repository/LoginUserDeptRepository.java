package com.css.zfzx.sjcj.modules.app.repository;

import com.css.bpm.platform.org.user.repository.entity.DeptUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginUserDeptRepository extends JpaRepository<DeptUserEntity, String> {
}
