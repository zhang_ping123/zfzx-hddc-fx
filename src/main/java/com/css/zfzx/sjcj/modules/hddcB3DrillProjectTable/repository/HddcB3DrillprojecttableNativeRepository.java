package com.css.zfzx.sjcj.modules.hddcB3DrillProjectTable.repository;

import com.css.zfzx.sjcj.modules.hddcB3DrillProjectTable.repository.entity.HddcB3DrillprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB3DrillProjectTable.viewobjects.HddcB3DrillprojecttableQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcB3DrillprojecttableNativeRepository {

    Page<HddcB3DrillprojecttableEntity> queryHddcB3Drillprojecttables(HddcB3DrillprojecttableQueryParams queryParams, int curPage, int pageSize);

    List<HddcB3DrillprojecttableEntity> exportYhDisasters(HddcB3DrillprojecttableQueryParams queryParams);
}
