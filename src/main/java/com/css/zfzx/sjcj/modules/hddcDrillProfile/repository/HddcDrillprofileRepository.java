package com.css.zfzx.sjcj.modules.hddcDrillProfile.repository;

import com.css.zfzx.sjcj.modules.hddcDrillProfile.repository.entity.HddcDrillprofileEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcDrillprofileRepository extends JpaRepository<HddcDrillprofileEntity, String> {
}
