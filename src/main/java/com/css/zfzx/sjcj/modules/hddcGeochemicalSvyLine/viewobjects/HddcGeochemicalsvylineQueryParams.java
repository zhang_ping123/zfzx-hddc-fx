package com.css.zfzx.sjcj.modules.hddcGeochemicalSvyLine.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Data
public class HddcGeochemicalsvylineQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String name;

}
