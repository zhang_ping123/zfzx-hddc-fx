package com.css.zfzx.sjcj.modules.hddcRSInterpretationLine.viewobjects;

import lombok.Data;

/**
 * @author zhangping
 * @date 2020-11-30
 */
@Data
public class HddcRsinterpretationlineQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String geomorphyname;

}
