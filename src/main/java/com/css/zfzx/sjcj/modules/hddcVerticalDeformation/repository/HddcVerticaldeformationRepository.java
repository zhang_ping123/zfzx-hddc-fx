package com.css.zfzx.sjcj.modules.hddcVerticalDeformation.repository;

import com.css.zfzx.sjcj.modules.hddcVerticalDeformation.repository.entity.HddcVerticaldeformationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-26
 */
public interface HddcVerticaldeformationRepository extends JpaRepository<HddcVerticaldeformationEntity, String> {
}
