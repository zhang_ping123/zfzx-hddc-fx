package com.css.zfzx.sjcj.modules.yhchemical.repository;

import com.css.zfzx.sjcj.modules.yhchemical.repository.entity.YhChemicalEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author ly
 * @date 2020-11-04
 */
public interface YhChemicalRepository extends JpaRepository<YhChemicalEntity, String> {
}
