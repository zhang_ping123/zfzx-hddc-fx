package com.css.zfzx.sjcj.modules.hddcwyDrillHole.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyDrillHole.repository.entity.HddcWyDrillholeEntity;
import com.css.zfzx.sjcj.modules.hddcwyDrillHole.viewobjects.HddcWyDrillholeQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */

public interface HddcWyDrillholeService {

    public JSONObject queryHddcWyDrillholes(HddcWyDrillholeQueryParams queryParams, int curPage, int pageSize);

    public HddcWyDrillholeEntity getHddcWyDrillhole(String uuid);

    public HddcWyDrillholeEntity saveHddcWyDrillhole(HddcWyDrillholeEntity hddcWyDrillhole);

    public HddcWyDrillholeEntity updateHddcWyDrillhole(HddcWyDrillholeEntity hddcWyDrillhole);

    public void deleteHddcWyDrillholes(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    BigInteger queryHddcWyDrillhole(HddcAppZztCountVo queryParams);

    List<HddcWyDrillholeEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);

    List<HddcWyDrillholeEntity> findAllByCreateUserAndIsValid(String userId,String isValid);

    /**
     * 逻辑删除-根据项目id删除数据
     * @param ids
     */
    void deleteByProjectId(List<String> ids);

    /**
     * 逻辑删除-根据任务id删除数据
     * @param ids
     */
    void deleteByTaskId(List<String> ids);
}
