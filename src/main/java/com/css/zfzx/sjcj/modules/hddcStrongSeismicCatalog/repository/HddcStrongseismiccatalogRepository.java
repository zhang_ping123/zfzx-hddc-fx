package com.css.zfzx.sjcj.modules.hddcStrongSeismicCatalog.repository;

import com.css.zfzx.sjcj.modules.hddcStrongSeismicCatalog.repository.entity.HddcStrongseismiccatalogEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-28
 */
public interface HddcStrongseismiccatalogRepository extends JpaRepository<HddcStrongseismiccatalogEntity, String> {
}
