package com.css.zfzx.sjcj.modules.hddcGeomorStation.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author whj
 * @date 2020-11-30
 */
@Data
public class HddcGeomorstationVO implements Serializable {

    /**
     * 经度
     */
    @Excel(name = "经度", orderNum = "4")
    private Double lon;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 村
     */
    private String village;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 测点编号
     */
    private String id;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 微地貌测量工程编号
     */
    @Excel(name = "微地貌测量工程编号", orderNum = "5")
    private String geomorphysvyprjid;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备注-观测点说明
     */
    @Excel(name = "备注-观测点说明", orderNum = "6")
    private String commentInfo;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 高程 [米]
     */
    @Excel(name = "高程 [米]", orderNum = "7")
    private Double elevation;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 纬度
     */
    @Excel(name = "纬度", orderNum = "8")
    private Double lat;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 删除标识
     */
    private String isValid;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String rowNum;
    private String errorMsg;
}