package com.css.zfzx.sjcj.modules.hddcRock1LinePre.repository;

import com.css.zfzx.sjcj.modules.hddcRock1LinePre.repository.entity.HddcRock1linepreEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-26
 */
public interface HddcRock1linepreRepository extends JpaRepository<HddcRock1linepreEntity, String> {
}
