package com.css.zfzx.sjcj.modules.hddcWorkRegion.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
public class HddcWorkregionVO implements Serializable {

    /**
     * 送样总数
     */
    @Excel(name = "送样总数", orderNum = "4")
    private Integer samplecount;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 微地貌测量工程总数
     */
    @Excel(name = "微地貌测量工程总数", orderNum = "5")
    private Integer geomorphysvyprojectcount;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 目标区面积 [km²]
     */
    @Excel(name = "目标区面积 [km²]", orderNum = "6")
    private Integer trarea;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "7")
    private String commentInfo;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 采集样品总数
     */
    @Excel(name = "采集样品总数", orderNum = "8")
    private Integer collectedsamplecount;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 钻孔数
     */
    @Excel(name = "钻孔数", orderNum = "9")
    private Integer drillcount;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 村
     */
    private String village;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 遥感影像处理数目
     */
    @Excel(name = "遥感影像处理数目", orderNum = "10")
    private Integer rsprocess;
    /**
     * 钻孔进尺 [米]
     */
    @Excel(name = "钻孔进尺 [米]", orderNum = "11")
    private Integer drilllength;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 地球物理探测工程数
     */
    @Excel(name = "地球物理探测工程数", orderNum = "12")
    private Integer geophysicalsvyprojectcount;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 项目名称
     */
    @Excel(name = "项目名称", orderNum = "4")
    private String name;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 地球物理测井数
     */
    @Excel(name = "地球物理测井数", orderNum = "13")
    private Integer gphwellcount;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 探槽数
     */
    @Excel(name = "探槽数", orderNum = "14")
    private Integer trenchcount;
    /**
     * 地震危害性评价总数
     */
    @Excel(name = "地震危害性评价总数", orderNum = "15")
    private Integer sdacount;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 工作区断裂总数
     */
    @Excel(name = "工作区断裂总数", orderNum = "16")
    private Integer wrfaultcount;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 地震监测工程数
     */
    @Excel(name = "地震监测工程数", orderNum = "17")
    private Integer seismicprojectcount;
    /**
     * 地球化学探测工程数
     */
    @Excel(name = "地球化学探测工程数", orderNum = "18")
    private Integer geochemicalprojectcount;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 获得测试结果样品数
     */
    @Excel(name = "获得测试结果样品数", orderNum = "19")
    private Integer datingsamplecount;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 工作区面积 [km²]
     */
    @Excel(name = "工作区面积 [km²]", orderNum = "20")
    private Integer wrarea;
    /**
     * 编号
     */
    private String id;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 野外观测点数
     */
    @Excel(name = "野外观测点数", orderNum = "21")
    private Integer fieldsvyptcount;
    /**
     * 工作区研究断裂总数
     */
    @Excel(name = "工作区研究断裂总数", orderNum = "22")
    private Integer wrstudiedfaultcount;
    /**
     * 地质填图面积 [km²]
     */
    @Excel(name = "地质填图面积 [km²]", orderNum = "23")
    private Integer mappingarea;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 地形变监测工程数
     */
    @Excel(name = "地形变监测工程数", orderNum = "24")
    private Integer crustaldfmprojectcount;
    /**
     * 地震危险性评价总数
     */
    @Excel(name = "地震危险性评价总数", orderNum = "25")
    private Integer sracount;
    /**
     * 断层三维数值模拟总数
     */
    @Excel(name = "断层三维数值模拟总数", orderNum = "26")
    private Integer numsimulationcount;
    /**
     * 火山地质调查填图总数
     */
    @Excel(name = "火山地质调查填图总数", orderNum = "27")
    private Integer volcaniccount;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备注
     */
    private String remark;
    /**
     * 探测总土方量 [m³]
     */
    @Excel(name = "探测总土方量 [m³]", orderNum = "28")
    private Double trenchvolume;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 乡
     */
    private String town;
    /**
     * 显示码
     */
    @Excel(name = "显示码", orderNum = "29")
    private String showcode;
    /**
     * 工作区确定活动断裂条数
     */
    @Excel(name = "工作区确定活动断裂条数", orderNum = "30")
    private Integer afaultcount;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 描述信息
     */
    private String description;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String rowNum;
    private String errorMsg;
}