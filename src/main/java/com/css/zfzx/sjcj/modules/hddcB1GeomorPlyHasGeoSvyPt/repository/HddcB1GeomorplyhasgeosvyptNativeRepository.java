package com.css.zfzx.sjcj.modules.hddcB1GeomorPlyHasGeoSvyPt.repository;

import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyHasGeoSvyPt.repository.entity.HddcB1GeomorplyhasgeosvyptEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyHasGeoSvyPt.viewobjects.HddcB1GeomorplyhasgeosvyptQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcB1GeomorplyhasgeosvyptNativeRepository {

    Page<HddcB1GeomorplyhasgeosvyptEntity> queryHddcB1Geomorplyhasgeosvypts(HddcB1GeomorplyhasgeosvyptQueryParams queryParams, int curPage, int pageSize);

    List<HddcB1GeomorplyhasgeosvyptEntity> exportYhDisasters(HddcB1GeomorplyhasgeosvyptQueryParams queryParams);
}
