package com.css.zfzx.sjcj.modules.hddcwyLava.repository;

import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyLava.repository.entity.HddcWyLavaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-02
 */
public interface HddcWyLavaRepository extends JpaRepository<HddcWyLavaEntity, String> {

    List<HddcWyLavaEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    List<HddcWyLavaEntity> findAllByCreateUserAndIsValid(String userId, String isValid);

    @Query(nativeQuery = true, value = "select * from hddc_wy_lava where is_valid!=0 project_name in :projectIds")
    List<HddcWyLavaEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_lava where task_name in :projectIds")
    List<HddcWyLavaEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);
}
