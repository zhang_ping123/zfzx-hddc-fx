package com.css.zfzx.sjcj.modules.hddcFaultSvyPoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcFaultSvyPoint.repository.entity.HddcFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcFaultSvyPoint.viewobjects.HddcFaultsvypointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author lihelei
 * @date 2020-11-26
 */

public interface HddcFaultsvypointService {

  

    public JSONObject queryHddcFaultsvypoints(HddcFaultsvypointQueryParams queryParams, int curPage, int pageSize);

    public HddcFaultsvypointEntity getHddcFaultsvypoint(String id);

    public HddcFaultsvypointEntity saveHddcFaultsvypoint(HddcFaultsvypointEntity hddcFaultsvypoint);

    public HddcFaultsvypointEntity updateHddcFaultsvypoint(HddcFaultsvypointEntity hddcFaultsvypoint);

    public void deleteHddcFaultsvypoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcFaultsvypointQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
