package com.css.zfzx.sjcj.modules.hddcImageIndexLayer.repository.entity;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangping
 * @date 2020-11-30
 */
@Data
@Entity
@Table(name="hddc_imageindexlayer")
public class HddcImageindexlayerEntity implements Serializable {

    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 市(编码需要)
     */
    @Column(name="citycode")
    private String citycode;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 时相
     */
    @Column(name="imagedate")
    private String imagedate;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 编号
     */
    //@Id
    @Column(name="id")
    private String id;
    /**
     * 编号
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 投影信息
     */
    @Column(name="projection")
    private String projection;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 城市名称
     */
    @Column(name="city")
    private String city;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 传感器
     */
    @Column(name="sensor")
    private String sensor;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 处理过程
     */
    @Column(name="processinfo")
    private String processinfo;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 数据格式
     */
    @Column(name="format")
    private String format;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 备注
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 影像名称
     */
    @Column(name="imagename")
    private String imagename;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 波段信息
     */
    @Column(name="bandinfo")
    private String bandinfo;
    /**
     * 数据源
     */
    @Column(name="datasource")
    private String datasource;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;

}

