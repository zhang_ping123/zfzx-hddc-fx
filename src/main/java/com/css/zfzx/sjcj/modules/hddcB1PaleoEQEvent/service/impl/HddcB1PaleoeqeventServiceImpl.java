package com.css.zfzx.sjcj.modules.hddcB1PaleoEQEvent.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltVO;
import com.css.zfzx.sjcj.modules.hddcB1PaleoEQEvent.repository.HddcB1PaleoeqeventNativeRepository;
import com.css.zfzx.sjcj.modules.hddcB1PaleoEQEvent.repository.HddcB1PaleoeqeventRepository;
import com.css.zfzx.sjcj.modules.hddcB1PaleoEQEvent.repository.entity.HddcB1PaleoeqeventEntity;
import com.css.zfzx.sjcj.modules.hddcB1PaleoEQEvent.service.HddcB1PaleoeqeventService;
import com.css.zfzx.sjcj.modules.hddcB1PaleoEQEvent.viewobjects.HddcB1PaleoeqeventQueryParams;
import com.css.zfzx.sjcj.modules.hddcB1PaleoEQEvent.viewobjects.HddcB1PaleoeqeventVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Service
public class HddcB1PaleoeqeventServiceImpl implements HddcB1PaleoeqeventService {

	@Autowired
    private HddcB1PaleoeqeventRepository hddcB1PaleoeqeventRepository;
    @Autowired
    private HddcB1PaleoeqeventNativeRepository hddcB1PaleoeqeventNativeRepository;

    @Override
    public JSONObject queryHddcB1Paleoeqevents(HddcB1PaleoeqeventQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcB1PaleoeqeventEntity> hddcB1PaleoeqeventPage = this.hddcB1PaleoeqeventNativeRepository.queryHddcB1Paleoeqevents(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcB1PaleoeqeventPage);
        return jsonObject;
    }


    @Override
    public HddcB1PaleoeqeventEntity getHddcB1Paleoeqevent(String id) {
        HddcB1PaleoeqeventEntity hddcB1Paleoeqevent = this.hddcB1PaleoeqeventRepository.findById(id).orElse(null);
         return hddcB1Paleoeqevent;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB1PaleoeqeventEntity saveHddcB1Paleoeqevent(HddcB1PaleoeqeventEntity hddcB1Paleoeqevent) {
        String uuid = UUIDGenerator.getUUID();
        hddcB1Paleoeqevent.setUuid(uuid);
        hddcB1Paleoeqevent.setCreateUser(PlatformSessionUtils.getUserId());
        hddcB1Paleoeqevent.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB1PaleoeqeventRepository.save(hddcB1Paleoeqevent);
        return hddcB1Paleoeqevent;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB1PaleoeqeventEntity updateHddcB1Paleoeqevent(HddcB1PaleoeqeventEntity hddcB1Paleoeqevent) {
        HddcB1PaleoeqeventEntity entity = hddcB1PaleoeqeventRepository.findById(hddcB1Paleoeqevent.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcB1Paleoeqevent);
        hddcB1Paleoeqevent.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcB1Paleoeqevent.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB1PaleoeqeventRepository.save(hddcB1Paleoeqevent);
        return hddcB1Paleoeqevent;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcB1Paleoeqevents(List<String> ids) {
        List<HddcB1PaleoeqeventEntity> hddcB1PaleoeqeventList = this.hddcB1PaleoeqeventRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcB1PaleoeqeventList) && hddcB1PaleoeqeventList.size() > 0) {
            for(HddcB1PaleoeqeventEntity hddcB1Paleoeqevent : hddcB1PaleoeqeventList) {
                this.hddcB1PaleoeqeventRepository.delete(hddcB1Paleoeqevent);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcB1PaleoeqeventQueryParams queryParams, HttpServletResponse response) {
        List<HddcB1PaleoeqeventEntity> yhDisasterEntities = hddcB1PaleoeqeventNativeRepository.exportYhDisasters(queryParams);
        List<HddcB1PaleoeqeventVO> list=new ArrayList<>();
        for (HddcB1PaleoeqeventEntity entity:yhDisasterEntities) {
            HddcB1PaleoeqeventVO yhDisasterVO=new HddcB1PaleoeqeventVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"探槽古地震事件","探槽古地震事件",HddcB1PaleoeqeventVO.class,"探槽古地震事件.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcB1PaleoeqeventVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcB1PaleoeqeventVO.class, params);
            List<HddcB1PaleoeqeventVO> list = result.getList();
            // Excel条数据
           // int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcB1PaleoeqeventVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcB1PaleoeqeventVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcB1PaleoeqeventVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcB1PaleoeqeventEntity yhDisasterEntity = new HddcB1PaleoeqeventEntity();
            HddcB1PaleoeqeventVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcB1Paleoeqevent(yhDisasterEntity);
        }
    }

}
