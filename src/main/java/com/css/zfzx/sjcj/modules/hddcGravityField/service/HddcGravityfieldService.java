package com.css.zfzx.sjcj.modules.hddcGravityField.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcGravityField.repository.entity.HddcGravityfieldEntity;
import com.css.zfzx.sjcj.modules.hddcGravityField.viewobjects.HddcGravityfieldQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-26
 */

public interface HddcGravityfieldService {

    public JSONObject queryHddcGravityfields(HddcGravityfieldQueryParams queryParams, int curPage, int pageSize);

    public HddcGravityfieldEntity getHddcGravityfield(String id);

    public HddcGravityfieldEntity saveHddcGravityfield(HddcGravityfieldEntity hddcGravityfield);

    public HddcGravityfieldEntity updateHddcGravityfield(HddcGravityfieldEntity hddcGravityfield);

    public void deleteHddcGravityfields(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGravityfieldQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
