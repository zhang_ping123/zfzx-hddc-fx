package com.css.zfzx.sjcj.modules.yhhospital.repository;

import com.css.zfzx.sjcj.modules.yhhospital.repository.entity.YhHospitalEntity;
import com.css.zfzx.sjcj.modules.yhhospital.viewobjects.YhHospitalQueryParams;
import org.springframework.data.domain.Page;

/**
 * @author yyd
 * @date 2020-11-03
 */
public interface YhHospitalNativeRepository {

    Page<YhHospitalEntity> queryYhHospitals(YhHospitalQueryParams queryParams, int curPage, int pageSize);
}
