package com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtHasTrench.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtHasTrench.repository.entity.HddcB1FPaleoeqevthastrenchEntity;
import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtHasTrench.viewobjects.HddcB1FPaleoeqevthastrenchQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */

public interface HddcB1FPaleoeqevthastrenchService {

    public JSONObject queryHddcB1FPaleoeqevthastrenchs(HddcB1FPaleoeqevthastrenchQueryParams queryParams, int curPage, int pageSize);

    public HddcB1FPaleoeqevthastrenchEntity getHddcB1FPaleoeqevthastrench(String id);

    public HddcB1FPaleoeqevthastrenchEntity saveHddcB1FPaleoeqevthastrench(HddcB1FPaleoeqevthastrenchEntity hddcB1FPaleoeqevthastrench);

    public HddcB1FPaleoeqevthastrenchEntity updateHddcB1FPaleoeqevthastrench(HddcB1FPaleoeqevthastrenchEntity hddcB1FPaleoeqevthastrench);

    public void deleteHddcB1FPaleoeqevthastrenchs(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB1FPaleoeqevthastrenchQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
