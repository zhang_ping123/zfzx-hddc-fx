package com.css.zfzx.sjcj.modules.hddcMaterialPolygonLayer.repository;

import com.css.zfzx.sjcj.modules.hddcMaterialPolygonLayer.repository.entity.HddcMaterialpolygonlayerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-12-03
 */
public interface HddcMaterialpolygonlayerRepository extends JpaRepository<HddcMaterialpolygonlayerEntity, String> {
}
