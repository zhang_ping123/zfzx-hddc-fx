package com.css.zfzx.sjcj.modules.analysis.service.impl;

import com.css.zfzx.sjcj.modules.analysis.service.ZkAndYbService;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyDrillHole.service.HddcWyDrillholeService;
import com.css.zfzx.sjcj.modules.hddcwySamplePoint.service.HddcWySamplepointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO
 *
 * @author 王昊杰
 * @version 1.0
 * @date 2020/12/10  20:06
 */

@Service
public class ZkAndYbServiceImpl implements ZkAndYbService {
    @Autowired
    private HddcWyDrillholeService hddcWyDrillholeService;
    @Autowired
    private HddcWySamplepointService hddcWySamplepointService;
    
    public Map<String,Object> hddcZkAndYbNumData(HddcAppZztCountVo queryParams){
        //钻孔-点
        BigInteger holeCount = hddcWyDrillholeService.queryHddcWyDrillhole(queryParams);
        //采样点-点
        BigInteger pointCount = hddcWySamplepointService.queryHddcWySamplepoint(queryParams);
       //x轴
        ArrayList<String> x=new ArrayList<>();
        x.add("钻孔-点");
        x.add("采样点-点");
        //y轴
        ArrayList<BigInteger> y=new ArrayList<>();
        y.add(holeCount);
        y.add(pointCount);

        HashMap<String,Object>  map=new HashMap<>();
        map.put("x",x);
        map.put("y",y);
        return map;
    }
}
