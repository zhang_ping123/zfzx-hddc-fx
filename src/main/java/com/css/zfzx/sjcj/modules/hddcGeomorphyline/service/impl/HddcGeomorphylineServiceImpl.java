package com.css.zfzx.sjcj.modules.hddcGeomorphyline.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcGeomorphyline.repository.HddcGeomorphylineNativeRepository;
import com.css.zfzx.sjcj.modules.hddcGeomorphyline.repository.HddcGeomorphylineRepository;
import com.css.zfzx.sjcj.modules.hddcGeomorphyline.repository.entity.HddcGeomorphylineEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphyline.service.HddcGeomorphylineService;
import com.css.zfzx.sjcj.modules.hddcGeomorphyline.viewobjects.HddcGeomorphylineQueryParams;
import com.css.zfzx.sjcj.modules.hddcGeomorphyline.viewobjects.HddcGeomorphylineVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-09
 */
@Service
public class HddcGeomorphylineServiceImpl implements HddcGeomorphylineService {

	@Autowired
    private HddcGeomorphylineRepository hddcGeomorphylineRepository;
    @Autowired
    private HddcGeomorphylineNativeRepository hddcGeomorphylineNativeRepository;

    @Override
    public JSONObject queryHddcGeomorphylines(HddcGeomorphylineQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcGeomorphylineEntity> hddcGeomorphylinePage = this.hddcGeomorphylineNativeRepository.queryHddcGeomorphylines(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcGeomorphylinePage);
        return jsonObject;
    }


    @Override
    public HddcGeomorphylineEntity getHddcGeomorphyline(String id) {
        HddcGeomorphylineEntity hddcGeomorphyline = this.hddcGeomorphylineRepository.findById(id).orElse(null);
         return hddcGeomorphyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeomorphylineEntity saveHddcGeomorphyline(HddcGeomorphylineEntity hddcGeomorphyline) {
        String uuid = UUIDGenerator.getUUID();
        hddcGeomorphyline.setUuid(uuid);
        hddcGeomorphyline.setCreateUser(PlatformSessionUtils.getUserId());
        hddcGeomorphyline.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeomorphylineRepository.save(hddcGeomorphyline);
        return hddcGeomorphyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeomorphylineEntity updateHddcGeomorphyline(HddcGeomorphylineEntity hddcGeomorphyline) {
        HddcGeomorphylineEntity entity = hddcGeomorphylineRepository.findById(hddcGeomorphyline.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcGeomorphyline);
        hddcGeomorphyline.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcGeomorphyline.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeomorphylineRepository.save(hddcGeomorphyline);
        return hddcGeomorphyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcGeomorphylines(List<String> ids) {
        List<HddcGeomorphylineEntity> hddcGeomorphylineList = this.hddcGeomorphylineRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcGeomorphylineList) && hddcGeomorphylineList.size() > 0) {
            for(HddcGeomorphylineEntity hddcGeomorphyline : hddcGeomorphylineList) {
                this.hddcGeomorphylineRepository.delete(hddcGeomorphyline);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcGeomorphylineQueryParams queryParams, HttpServletResponse response) {
        List<HddcGeomorphylineEntity> yhDisasterEntities = hddcGeomorphylineNativeRepository.exportYhDisasters(queryParams);
        List<HddcGeomorphylineVO> list=new ArrayList<>();
        for (HddcGeomorphylineEntity entity:yhDisasterEntities) {
            HddcGeomorphylineVO yhDisasterVO=new HddcGeomorphylineVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"地貌线-线","地貌线-线",HddcGeomorphylineVO.class,"地貌线-线.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcGeomorphylineVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcGeomorphylineVO.class, params);
            List<HddcGeomorphylineVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcGeomorphylineVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcGeomorphylineVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcGeomorphylineVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcGeomorphylineEntity yhDisasterEntity = new HddcGeomorphylineEntity();
            HddcGeomorphylineVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcGeomorphyline(yhDisasterEntity);
        }
    }

}
