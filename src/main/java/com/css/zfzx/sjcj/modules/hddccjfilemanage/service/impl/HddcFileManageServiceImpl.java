package com.css.zfzx.sjcj.modules.hddccjfilemanage.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddccjfilemanage.repository.HddcFileManageNativeRepository;
import com.css.zfzx.sjcj.modules.hddccjfilemanage.repository.HddcFileManageRepository;
import com.css.zfzx.sjcj.modules.hddccjfilemanage.repository.entity.HddcFileManageEntity;
import com.css.zfzx.sjcj.modules.hddccjfilemanage.service.HddcFileManageService;
import com.css.zfzx.sjcj.modules.hddccjfilemanage.viewobjects.HddcFileManageQueryParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-10
 */
@Slf4j
@Service
@PropertySource(value = "classpath:platform-config.yml")
public class HddcFileManageServiceImpl implements HddcFileManageService {

    @Value("${file-upload.attachment-directory.config.defaultDirectory.fileDirectory}")
    private String localPath;
	@Autowired
    private HddcFileManageRepository hddcFileManageRepository;
    @Autowired
    private HddcFileManageNativeRepository hddcFileManageNativeRepository;

    @Override
    public JSONObject queryHddcFileManages(HddcFileManageQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcFileManageEntity> hddcFileManagePage = this.hddcFileManageNativeRepository.queryHddcFileManages(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcFileManagePage);
        return jsonObject;
    }


    @Override
    public HddcFileManageEntity getHddcFileManage(String id) {
        HddcFileManageEntity hddcFileManage = this.hddcFileManageRepository.findById(id).orElse(null);
         return hddcFileManage;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcFileManageEntity saveHddcFileManage(HddcFileManageEntity hddcFileManage) {
        String uuid = UUIDGenerator.getUUID();
        hddcFileManage.setUuid(uuid);
        hddcFileManage.setCreateUser(PlatformSessionUtils.getUserId());
        hddcFileManage.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcFileManageRepository.save(hddcFileManage);
        return hddcFileManage;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcFileManageEntity updateHddcFileManage(HddcFileManageEntity hddcFileManage) {
        HddcFileManageEntity entity = hddcFileManageRepository.findById(hddcFileManage.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcFileManage);
        hddcFileManage.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcFileManage.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcFileManageRepository.save(hddcFileManage);
        return hddcFileManage;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcFileManages(List<String> ids) {
        List<HddcFileManageEntity> hddcFileManageList = this.hddcFileManageRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcFileManageList) && hddcFileManageList.size() > 0) {
            for(HddcFileManageEntity hddcFileManage : hddcFileManageList) {
                this.hddcFileManageRepository.delete(hddcFileManage);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    private void addOrUpdateHddcFileManage(String name,String filenumber) {
        HddcFileManageEntity hddcFileManageEntity = new HddcFileManageEntity();
        hddcFileManageEntity.setFileName(name);
        hddcFileManageEntity.setFileId(filenumber);
        this.saveHddcFileManage(hddcFileManageEntity);
        /*HddcFileManageQueryParams params = new HddcFileManageQueryParams();
        params.setFileName(name);
        params.setFileId(filenumber);

        Page<HddcFileManageEntity> hddcFileManagePage = this.hddcFileManageNativeRepository.queryHddcFileManages(params, 1, 10);

        HddcFileManageEntity hddcFileManageEntity = new HddcFileManageEntity();
        hddcFileManageEntity.setFileName(name);
        hddcFileManageEntity.setFileId(filenumber);

        //update add
        if(hddcFileManagePage.getContent().size() > 0) {
            hddcFileManageEntity = hddcFileManagePage.getContent().get(0);
            this.updateHddcFileManage(hddcFileManageEntity);
        } else {
            this.saveHddcFileManage(hddcFileManageEntity);
        }*/
    }
    @Override
    public int upload(MultipartFile file,String filenumber) {
        List<HddcFileManageEntity> hddcFileManageEntity=hddcFileManageRepository.findbyId(filenumber);
        int count=0;
        if (!hddcFileManageEntity.isEmpty()&&hddcFileManageEntity.size()!=0){
            return count;//该编号已存在
        }
        try {
            String path = localPath + File.separator + "file";//上传的路径
            File dir = new File(path);
            if (!dir.exists()) {
                //路径不存在就创建路径
                dir.mkdir();
            }
            String fileName = file.getOriginalFilename();
            File file1 = new File(path, fileName);
            file.transferTo(file1);
            addOrUpdateHddcFileManage(fileName,filenumber);
            //addOrUpdateHddcFileManage(fileName.substring(0,fileName.lastIndexOf('.')),filenumber);
            count=2;//上传成功
        } catch (IOException e) {
            //throw new Exception(e.getMessage());
            log.error(e.getMessage());
            count=1;//上传失败
        }
        return count;
    }

    @Override
    public void download(String name, HttpServletResponse response) {
        try {
            String path = localPath + File.separator + "file";//文件的下载地址
            String fullName = name;
            //设置响应头
            response.reset();//设置页面不缓存
            response.setCharacterEncoding("UTF-8");//字符集编码
            response.setContentType("multipart/form-data");//二进制传输文件
            response.setHeader("Content-Disposition", "attachment;fileName=" + URLEncoder.encode(fullName, "UTF-8"));//设置响应头
            File file = new File(path, fullName);
            //输入流
            FileInputStream inputStream = new FileInputStream(file);
            //输出流
            ServletOutputStream outputStream = response.getOutputStream();

            byte[] bytes = new byte[10240000];
            int index = 0;
            while ((index = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, index);
                outputStream.flush();
            }
            outputStream.close();
            inputStream.close();
        } catch (Exception e) {

        }
    }

}
