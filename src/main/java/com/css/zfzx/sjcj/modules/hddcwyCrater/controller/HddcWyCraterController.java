package com.css.zfzx.sjcj.modules.hddcwyCrater.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcGravityField.viewobjects.HddcGravityfieldQueryParams;
import com.css.zfzx.sjcj.modules.hddcwyCrater.repository.entity.HddcWyCraterEntity;
import com.css.zfzx.sjcj.modules.hddcwyCrater.service.HddcWyCraterService;
import com.css.zfzx.sjcj.modules.hddcwyCrater.viewobjects.HddcWyCraterQueryParams;
import com.css.bpm.platform.utils.PlatformPageUtils;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-02
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcWyCraters")
public class HddcWyCraterController {
    @Autowired
    private HddcWyCraterService hddcWyCraterService;

    @GetMapping("/queryHddcWyCraters")
    public RestResponse queryHddcWyCraters(HttpServletRequest request, HddcWyCraterQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcWyCraterService.queryHddcWyCraters(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @GetMapping("/findAllByhddcWyCraters")
    public RestResponse findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid) {
        RestResponse response = null;
        try{
            List<HddcWyCraterEntity> list = hddcWyCraterService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, isValid);
            response = RestResponse.succeed(list);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @GetMapping("{uuid}")
    public RestResponse getHddcWyCrater(@PathVariable String uuid) {
        RestResponse response = null;
        try{
            HddcWyCraterEntity hddcWyCrater = hddcWyCraterService.getHddcWyCrater(uuid);
            response = RestResponse.succeed(hddcWyCrater);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcWyCrater(@RequestBody HddcWyCraterEntity hddcWyCrater) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcWyCraterService.saveHddcWyCrater(hddcWyCrater);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcWyCrater(@RequestBody HddcWyCraterEntity hddcWyCrater)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcWyCraterService.updateHddcWyCrater(hddcWyCrater);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcWyCraters(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcWyCraterService.deleteHddcWyCraters(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcWyCraterService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }


    /***
     * 导出
     * @param response
     * @return
     */
    @GetMapping("/exportFile")
    public RestResponse exportFileYhDisasters(HttpServletResponse response,
                                              @RequestParam("projectName")String projectName, @RequestParam("taskName") String taskName,
                                              @RequestParam("startTime") String startTime, @RequestParam("endTime")String endTime) {
        RestResponse responseRest = null;
        JSONObject jsonObject = new JSONObject();
        try{
            HddcAppZztCountVo queryParams=new HddcAppZztCountVo();
            queryParams.setProjectName(projectName);
            queryParams.setTaskName(taskName);
            queryParams.setStartTime(startTime);
            queryParams.setEndTime(endTime);
            hddcWyCraterService.exportFile(queryParams,response);
            jsonObject.put("message", "导出成功");
            responseRest = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            responseRest = RestResponse.fail(errorMessage);
        }
        return responseRest;
    }


    /**
     * 导入
     *
     * @param file
     * @param response
     * @return
     */
    @PostMapping("/importDisaster")
    public RestResponse export(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        RestResponse restResponse = null;
        try {
            String s = hddcWyCraterService.exportExcel( file, response);
            restResponse = RestResponse.succeed(s);
        } catch (Exception e) {
            String errormessage = "导入失败";
            log.error(errormessage, e);
            restResponse = RestResponse.fail(errormessage);
        }
        return restResponse;
    }
}