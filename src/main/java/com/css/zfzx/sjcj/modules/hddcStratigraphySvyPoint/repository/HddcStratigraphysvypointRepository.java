package com.css.zfzx.sjcj.modules.hddcStratigraphySvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcStratigraphySvyPoint.repository.entity.HddcStratigraphysvypointEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author lihelei
 * @date 2020-11-27
 */
public interface HddcStratigraphysvypointRepository extends JpaRepository<HddcStratigraphysvypointEntity, String> {
}
