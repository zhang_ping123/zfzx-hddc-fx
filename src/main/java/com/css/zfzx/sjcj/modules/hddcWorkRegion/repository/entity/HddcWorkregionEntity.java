package com.css.zfzx.sjcj.modules.hddcWorkRegion.repository.entity;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
@Entity
@Table(name="hddc_workregion")
public class HddcWorkregionEntity implements Serializable {

    /**
     * 送样总数
     */
    @Column(name="samplecount")
    private Integer samplecount;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 微地貌测量工程总数
     */
    @Column(name="geomorphysvyprojectcount")
    private Integer geomorphysvyprojectcount;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 目标区面积 [km²]
     */
    @Column(name="trarea")
    private Integer trarea;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 备注
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 采集样品总数
     */
    @Column(name="collectedsamplecount")
    private Integer collectedsamplecount;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 钻孔数
     */
    @Column(name="drillcount")
    private Integer drillcount;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 遥感影像处理数目
     */
    @Column(name="rsprocess")
    private Integer rsprocess;
    /**
     * 钻孔进尺 [米]
     */
    @Column(name="drilllength")
    private Integer drilllength;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 地球物理探测工程数
     */
    @Column(name="geophysicalsvyprojectcount")
    private Integer geophysicalsvyprojectcount;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 项目名称
     */
    @Column(name="name")
    private String name;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 地球物理测井数
     */
    @Column(name="gphwellcount")
    private Integer gphwellcount;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 探槽数
     */
    @Column(name="trenchcount")
    private Integer trenchcount;
    /**
     * 地震危害性评价总数
     */
    @Column(name="sdacount")
    private Integer sdacount;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 工作区断裂总数
     */
    @Column(name="wrfaultcount")
    private Integer wrfaultcount;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 地震监测工程数
     */
    @Column(name="seismicprojectcount")
    private Integer seismicprojectcount;
    /**
     * 地球化学探测工程数
     */
    @Column(name="geochemicalprojectcount")
    private Integer geochemicalprojectcount;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 获得测试结果样品数
     */
    @Column(name="datingsamplecount")
    private Integer datingsamplecount;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 工作区面积 [km²]
     */
    @Column(name="wrarea")
    private Integer wrarea;
    /**
     * 编号
     */
    //@Id
    @Column(name="id")
    private String id;

    /**
     * 编号
     */
    @Id
    @Column(name="uuid")
    private String uuid;


    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 野外观测点数
     */
    @Column(name="fieldsvyptcount")
    private Integer fieldsvyptcount;
    /**
     * 工作区研究断裂总数
     */
    @Column(name="wrstudiedfaultcount")
    private Integer wrstudiedfaultcount;
    /**
     * 地质填图面积 [km²]
     */
    @Column(name="mappingarea")
    private Integer mappingarea;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 地形变监测工程数
     */
    @Column(name="crustaldfmprojectcount")
    private Integer crustaldfmprojectcount;
    /**
     * 地震危险性评价总数
     */
    @Column(name="sracount")
    private Integer sracount;
    /**
     * 断层三维数值模拟总数
     */
    @Column(name="numsimulationcount")
    private Integer numsimulationcount;
    /**
     * 火山地质调查填图总数
     */
    @Column(name="volcaniccount")
    private Integer volcaniccount;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 探测总土方量 [m³]
     */
    @Column(name="trenchvolume")
    private Double trenchvolume;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 显示码
     */
    @Column(name="showcode")
    private String showcode;
    /**
     * 工作区确定活动断裂条数
     */
    @Column(name="afaultcount")
    private Integer afaultcount;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 描述信息
     */
    @Column(name="description")
    private String description;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;

}

