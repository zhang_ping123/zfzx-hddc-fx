package com.css.zfzx.sjcj.modules.hddcISCatalog.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcISCatalog.repository.HddcIscatalogNativeRepository;
import com.css.zfzx.sjcj.modules.hddcISCatalog.repository.HddcIscatalogRepository;
import com.css.zfzx.sjcj.modules.hddcISCatalog.repository.entity.HddcIscatalogEntity;
import com.css.zfzx.sjcj.modules.hddcISCatalog.service.HddcIscatalogService;
import com.css.zfzx.sjcj.modules.hddcISCatalog.viewobjects.HddcIscatalogQueryParams;
import com.css.zfzx.sjcj.modules.hddcISCatalog.viewobjects.HddcIscatalogVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Service
public class HddcIscatalogServiceImpl implements HddcIscatalogService {

	@Autowired
    private HddcIscatalogRepository hddcIscatalogRepository;
    @Autowired
    private HddcIscatalogNativeRepository hddcIscatalogNativeRepository;

    @Override
    public JSONObject queryHddcIscatalogs(HddcIscatalogQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcIscatalogEntity> hddcIscatalogPage = this.hddcIscatalogNativeRepository.queryHddcIscatalogs(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcIscatalogPage);
        return jsonObject;
    }


    @Override
    public HddcIscatalogEntity getHddcIscatalog(String id) {
        HddcIscatalogEntity hddcIscatalog = this.hddcIscatalogRepository.findById(id).orElse(null);
         return hddcIscatalog;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcIscatalogEntity saveHddcIscatalog(HddcIscatalogEntity hddcIscatalog) {
        String uuid = UUIDGenerator.getUUID();
        hddcIscatalog.setUuid(uuid);
        hddcIscatalog.setCreateUser(PlatformSessionUtils.getUserId());
        hddcIscatalog.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcIscatalogRepository.save(hddcIscatalog);
        return hddcIscatalog;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcIscatalogEntity updateHddcIscatalog(HddcIscatalogEntity hddcIscatalog) {
        HddcIscatalogEntity entity = hddcIscatalogRepository.findById(hddcIscatalog.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcIscatalog);
        hddcIscatalog.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcIscatalog.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcIscatalogRepository.save(hddcIscatalog);
        return hddcIscatalog;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcIscatalogs(List<String> ids) {
        List<HddcIscatalogEntity> hddcIscatalogList = this.hddcIscatalogRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcIscatalogList) && hddcIscatalogList.size() > 0) {
            for(HddcIscatalogEntity hddcIscatalog : hddcIscatalogList) {
                this.hddcIscatalogRepository.delete(hddcIscatalog);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcIscatalogQueryParams queryParams, HttpServletResponse response) {
        List<HddcIscatalogEntity> yhDisasterEntities = hddcIscatalogNativeRepository.exportYhDisasters(queryParams);
        List<HddcIscatalogVO> list=new ArrayList<>();
        for (HddcIscatalogEntity entity:yhDisasterEntities) {
            HddcIscatalogVO yhDisasterVO=new HddcIscatalogVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "1970年以来地震目录-点", "1970年以来地震目录-点", HddcIscatalogVO.class, "1970年以来地震目录-点.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcIscatalogVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcIscatalogVO.class, params);
            List<HddcIscatalogVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcIscatalogVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcIscatalogVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcIscatalogVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcIscatalogEntity yhDisasterEntity = new HddcIscatalogEntity();
            HddcIscatalogVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcIscatalog(yhDisasterEntity);
        }
    }

}
