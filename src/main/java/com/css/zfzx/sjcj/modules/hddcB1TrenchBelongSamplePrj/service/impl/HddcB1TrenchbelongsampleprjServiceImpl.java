package com.css.zfzx.sjcj.modules.hddcB1TrenchBelongSamplePrj.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1TrenchBelongSamplePrj.repository.HddcB1TrenchbelongsampleprjNativeRepository;
import com.css.zfzx.sjcj.modules.hddcB1TrenchBelongSamplePrj.repository.HddcB1TrenchbelongsampleprjRepository;
import com.css.zfzx.sjcj.modules.hddcB1TrenchBelongSamplePrj.repository.entity.HddcB1TrenchbelongsampleprjEntity;
import com.css.zfzx.sjcj.modules.hddcB1TrenchBelongSamplePrj.service.HddcB1TrenchbelongsampleprjService;
import com.css.zfzx.sjcj.modules.hddcB1TrenchBelongSamplePrj.viewobjects.HddcB1TrenchbelongsampleprjQueryParams;
import com.css.zfzx.sjcj.modules.hddcB1TrenchBelongSamplePrj.viewobjects.HddcB1TrenchbelongsampleprjVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Service
public class HddcB1TrenchbelongsampleprjServiceImpl implements HddcB1TrenchbelongsampleprjService {

	@Autowired
    private HddcB1TrenchbelongsampleprjRepository hddcB1TrenchbelongsampleprjRepository;
    @Autowired
    private HddcB1TrenchbelongsampleprjNativeRepository hddcB1TrenchbelongsampleprjNativeRepository;

    @Override
    public JSONObject queryHddcB1Trenchbelongsampleprjs(HddcB1TrenchbelongsampleprjQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcB1TrenchbelongsampleprjEntity> hddcB1TrenchbelongsampleprjPage = this.hddcB1TrenchbelongsampleprjNativeRepository.queryHddcB1Trenchbelongsampleprjs(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcB1TrenchbelongsampleprjPage);
        return jsonObject;
    }


    @Override
    public HddcB1TrenchbelongsampleprjEntity getHddcB1Trenchbelongsampleprj(String id) {
        HddcB1TrenchbelongsampleprjEntity hddcB1Trenchbelongsampleprj = this.hddcB1TrenchbelongsampleprjRepository.findById(id).orElse(null);
         return hddcB1Trenchbelongsampleprj;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB1TrenchbelongsampleprjEntity saveHddcB1Trenchbelongsampleprj(HddcB1TrenchbelongsampleprjEntity hddcB1Trenchbelongsampleprj) {
        String uuid = UUIDGenerator.getUUID();
        hddcB1Trenchbelongsampleprj.setUuid(uuid);
        hddcB1Trenchbelongsampleprj.setCreateUser(PlatformSessionUtils.getUserId());
        hddcB1Trenchbelongsampleprj.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB1TrenchbelongsampleprjRepository.save(hddcB1Trenchbelongsampleprj);
        return hddcB1Trenchbelongsampleprj;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB1TrenchbelongsampleprjEntity updateHddcB1Trenchbelongsampleprj(HddcB1TrenchbelongsampleprjEntity hddcB1Trenchbelongsampleprj) {
        HddcB1TrenchbelongsampleprjEntity entity = hddcB1TrenchbelongsampleprjRepository.findById(hddcB1Trenchbelongsampleprj.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcB1Trenchbelongsampleprj);
        hddcB1Trenchbelongsampleprj.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcB1Trenchbelongsampleprj.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB1TrenchbelongsampleprjRepository.save(hddcB1Trenchbelongsampleprj);
        return hddcB1Trenchbelongsampleprj;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcB1Trenchbelongsampleprjs(List<String> ids) {
        List<HddcB1TrenchbelongsampleprjEntity> hddcB1TrenchbelongsampleprjList = this.hddcB1TrenchbelongsampleprjRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcB1TrenchbelongsampleprjList) && hddcB1TrenchbelongsampleprjList.size() > 0) {
            for(HddcB1TrenchbelongsampleprjEntity hddcB1Trenchbelongsampleprj : hddcB1TrenchbelongsampleprjList) {
                this.hddcB1TrenchbelongsampleprjRepository.delete(hddcB1Trenchbelongsampleprj);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcB1TrenchbelongsampleprjQueryParams queryParams, HttpServletResponse response) {
        List<HddcB1TrenchbelongsampleprjEntity> yhDisasterEntities = hddcB1TrenchbelongsampleprjNativeRepository.exportYhDisasters(queryParams);
        List<HddcB1TrenchbelongsampleprjVO> list=new ArrayList<>();
        for (HddcB1TrenchbelongsampleprjEntity entity:yhDisasterEntities) {
            HddcB1TrenchbelongsampleprjVO yhDisasterVO=new HddcB1TrenchbelongsampleprjVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"探槽与采样工程关联表","探槽与采样工程关联表",HddcB1TrenchbelongsampleprjVO.class,"探槽与采样工程关联表.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcB1TrenchbelongsampleprjVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcB1TrenchbelongsampleprjVO.class, params);
            List<HddcB1TrenchbelongsampleprjVO> list = result.getList();
            // Excel条数据
          //  int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcB1TrenchbelongsampleprjVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcB1TrenchbelongsampleprjVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcB1TrenchbelongsampleprjVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcB1TrenchbelongsampleprjEntity yhDisasterEntity = new HddcB1TrenchbelongsampleprjEntity();
            HddcB1TrenchbelongsampleprjVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcB1Trenchbelongsampleprj(yhDisasterEntity);
        }
    }

}
