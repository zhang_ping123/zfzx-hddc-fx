package com.css.zfzx.sjcj.modules.hddcEpiMechanismResult.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.zfzx.sjcj.modules.hddcEpiMechanismResult.repository.entity.HddcEpimechanismresultEntity;
import com.css.zfzx.sjcj.modules.hddcEpiMechanismResult.service.HddcEpimechanismresultService;
import com.css.zfzx.sjcj.modules.hddcEpiMechanismResult.viewobjects.HddcEpimechanismresultQueryParams;
import com.css.bpm.platform.utils.PlatformPageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcEpimechanismresults")
public class HddcEpimechanismresultController {
    @Autowired
    private HddcEpimechanismresultService hddcEpimechanismresultService;

    @GetMapping("/queryHddcEpimechanismresults")
    public RestResponse queryHddcEpimechanismresults(HttpServletRequest request, HddcEpimechanismresultQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcEpimechanismresultService.queryHddcEpimechanismresults(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcEpimechanismresult(@PathVariable String id) {
        RestResponse response = null;
        try{
            HddcEpimechanismresultEntity hddcEpimechanismresult = hddcEpimechanismresultService.getHddcEpimechanismresult(id);
            response = RestResponse.succeed(hddcEpimechanismresult);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcEpimechanismresult(@RequestBody HddcEpimechanismresultEntity hddcEpimechanismresult) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcEpimechanismresultService.saveHddcEpimechanismresult(hddcEpimechanismresult);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcEpimechanismresult(@RequestBody HddcEpimechanismresultEntity hddcEpimechanismresult)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcEpimechanismresultService.updateHddcEpimechanismresult(hddcEpimechanismresult);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcEpimechanismresults(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcEpimechanismresultService.deleteHddcEpimechanismresults(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcEpimechanismresultService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }
    /***
     * 导出
     * @param response
     * @return
     */
    @GetMapping("/exportFile")
    public RestResponse exportFileYhDisasters(HttpServletResponse response,
                                              @RequestParam("projectName")String projectName,
                                              @RequestParam("province") String province,@RequestParam("city")String city,@RequestParam("area")String area) {
        RestResponse responseRest = null;
        JSONObject jsonObject = new JSONObject();
        try{
            HddcEpimechanismresultQueryParams queryParams=new HddcEpimechanismresultQueryParams();
            queryParams.setArea(area);
            queryParams.setCity(city);
            queryParams.setProvince(province);
            queryParams.setProjectName(projectName);
            hddcEpimechanismresultService.exportFile(queryParams,response);
            jsonObject.put("message", "导出成功");
            responseRest = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            responseRest = RestResponse.fail(errorMessage);
        }
        return responseRest;
    }

    /**
     * 导入
     *
     * @param file
     * @param response
     * @return
     */
    @PostMapping("/importDisaster")
    public RestResponse export(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        RestResponse restResponse = null;
        try {
            String s = hddcEpimechanismresultService.exportExcel( file, response);
            restResponse = RestResponse.succeed(s);
        } catch (Exception e) {
            String errormessage = "导入失败";
            log.error(errormessage, e);
            restResponse = RestResponse.fail(errormessage);
        }
        return restResponse;
    }
}