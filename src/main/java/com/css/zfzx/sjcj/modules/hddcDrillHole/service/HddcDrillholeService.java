package com.css.zfzx.sjcj.modules.hddcDrillHole.service;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.zfzx.sjcj.modules.hddcDrillHole.repository.entity.HddcDrillholeEntity;
import com.css.zfzx.sjcj.modules.hddcDrillHole.viewobjects.HddcDrillholeQueryParams;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */

public interface HddcDrillholeService {

    public JSONObject queryHddcDrillholes(HddcDrillholeQueryParams queryParams, int curPage, int pageSize);

    public HddcDrillholeEntity getHddcDrillhole(String id);

    public HddcDrillholeEntity saveHddcDrillhole(HddcDrillholeEntity hddcDrillhole);

    public HddcDrillholeEntity updateHddcDrillhole(HddcDrillholeEntity hddcDrillhole);

    public void deleteHddcDrillholes(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcDrillholeQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);

    /**
     * 逻辑删除-根据项目id删除数据
     * @param projectIds
     */
    void deleteByProjectId(List<String> projectIds);

    /**
     * 逻辑删除-根据任务id删除数据
     * @param taskIds
     */
    void deleteByTaskId(List<String> taskIds);
}
