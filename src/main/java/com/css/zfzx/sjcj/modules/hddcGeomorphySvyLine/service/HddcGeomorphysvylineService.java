package com.css.zfzx.sjcj.modules.hddcGeomorphySvyLine.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyLine.repository.entity.HddcGeomorphysvylineEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyLine.viewobjects.HddcGeomorphysvylineQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */

public interface HddcGeomorphysvylineService {

    public JSONObject queryHddcGeomorphysvylines(HddcGeomorphysvylineQueryParams queryParams, int curPage, int pageSize);

    public HddcGeomorphysvylineEntity getHddcGeomorphysvyline(String id);

    public HddcGeomorphysvylineEntity saveHddcGeomorphysvyline(HddcGeomorphysvylineEntity hddcGeomorphysvyline);

    public HddcGeomorphysvylineEntity updateHddcGeomorphysvyline(HddcGeomorphysvylineEntity hddcGeomorphysvyline);

    public void deleteHddcGeomorphysvylines(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGeomorphysvylineQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
