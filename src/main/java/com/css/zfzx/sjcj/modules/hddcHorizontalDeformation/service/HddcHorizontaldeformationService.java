package com.css.zfzx.sjcj.modules.hddcHorizontalDeformation.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcHorizontalDeformation.repository.entity.HddcHorizontaldeformationEntity;
import com.css.zfzx.sjcj.modules.hddcHorizontalDeformation.viewobjects.HddcHorizontaldeformationQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-26
 */

public interface HddcHorizontaldeformationService {

    public JSONObject queryHddcHorizontaldeformations(HddcHorizontaldeformationQueryParams queryParams, int curPage, int pageSize);

    public HddcHorizontaldeformationEntity getHddcHorizontaldeformation(String id);

    public HddcHorizontaldeformationEntity saveHddcHorizontaldeformation(HddcHorizontaldeformationEntity hddcHorizontaldeformation);

    public HddcHorizontaldeformationEntity updateHddcHorizontaldeformation(HddcHorizontaldeformationEntity hddcHorizontaldeformation);

    public void deleteHddcHorizontaldeformations(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcHorizontaldeformationQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
