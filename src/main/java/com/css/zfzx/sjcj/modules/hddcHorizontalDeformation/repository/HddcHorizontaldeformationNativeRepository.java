package com.css.zfzx.sjcj.modules.hddcHorizontalDeformation.repository;

import com.css.zfzx.sjcj.modules.hddcHorizontalDeformation.repository.entity.HddcHorizontaldeformationEntity;
import com.css.zfzx.sjcj.modules.hddcHorizontalDeformation.viewobjects.HddcHorizontaldeformationQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-26
 */
public interface HddcHorizontaldeformationNativeRepository {

    Page<HddcHorizontaldeformationEntity> queryHddcHorizontaldeformations(HddcHorizontaldeformationQueryParams queryParams, int curPage, int pageSize);

    List<HddcHorizontaldeformationEntity> exportYhDisasters(HddcHorizontaldeformationQueryParams queryParams);
}
