package com.css.zfzx.sjcj.modules.hddcGeochemicalSvyLine.repository;

import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyLine.repository.entity.HddcGeochemicalsvylineEntity;
import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyLine.viewobjects.HddcGeochemicalsvylineQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
public interface HddcGeochemicalsvylineNativeRepository {

    Page<HddcGeochemicalsvylineEntity> queryHddcGeochemicalsvylines(HddcGeochemicalsvylineQueryParams queryParams, int curPage, int pageSize);

    List<HddcGeochemicalsvylineEntity> exportYhDisasters(HddcGeochemicalsvylineQueryParams queryParams);
}
