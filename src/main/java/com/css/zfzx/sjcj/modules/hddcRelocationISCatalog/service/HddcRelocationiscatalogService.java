package com.css.zfzx.sjcj.modules.hddcRelocationISCatalog.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcRelocationISCatalog.repository.entity.HddcRelocationiscatalogEntity;
import com.css.zfzx.sjcj.modules.hddcRelocationISCatalog.viewobjects.HddcRelocationiscatalogQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */

public interface HddcRelocationiscatalogService {

    public JSONObject queryHddcRelocationiscatalogs(HddcRelocationiscatalogQueryParams queryParams, int curPage, int pageSize);

    public HddcRelocationiscatalogEntity getHddcRelocationiscatalog(String id);

    public HddcRelocationiscatalogEntity saveHddcRelocationiscatalog(HddcRelocationiscatalogEntity hddcRelocationiscatalog);

    public HddcRelocationiscatalogEntity updateHddcRelocationiscatalog(HddcRelocationiscatalogEntity hddcRelocationiscatalog);

    public void deleteHddcRelocationiscatalogs(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcRelocationiscatalogQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
