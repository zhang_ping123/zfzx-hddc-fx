package com.css.zfzx.sjcj.modules.hddcB7_VolSvyPtObservationObject.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB7_VolSvyPtObservationObject.repository.entity.HddcB7VolsvyptobservationobjectEntity;
import com.css.zfzx.sjcj.modules.hddcB7_VolSvyPtObservationObject.viewobjects.HddcB7VolsvyptobservationobjectQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-27
 */

public interface HddcB7VolsvyptobservationobjectService {

    public JSONObject queryHddcB7Volsvyptobservationobjects(HddcB7VolsvyptobservationobjectQueryParams queryParams, int curPage, int pageSize);

    public HddcB7VolsvyptobservationobjectEntity getHddcB7Volsvyptobservationobject(String id);

    public HddcB7VolsvyptobservationobjectEntity saveHddcB7Volsvyptobservationobject(HddcB7VolsvyptobservationobjectEntity hddcB7Volsvyptobservationobject);

    public HddcB7VolsvyptobservationobjectEntity updateHddcB7Volsvyptobservationobject(HddcB7VolsvyptobservationobjectEntity hddcB7Volsvyptobservationobject);

    public void deleteHddcB7Volsvyptobservationobjects(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB7VolsvyptobservationobjectQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
