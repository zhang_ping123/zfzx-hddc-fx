package com.css.zfzx.sjcj.modules.hddcwyTrench.repository.entity;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-12-02
 */
@Data
@Entity
@Table(name="hddc_wy_trench")
public class HddcWyTrenchEntity implements Serializable {

    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 探槽来源与类型
     */
    @Column(name="trenchsource")
    private Integer trenchsource;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 探槽名称
     */
    @Column(name="name")
    private String name;
    /**
     * 探槽方向角度 [度]
     */
    @Column(name="trenchdip")
    private Integer trenchdip;
    /**
     * 照片镜向
     */
    @Column(name="photoviewingto")
    private Integer photoviewingto;
    /**
     * 探槽长 [米]
     */
    @Column(name="length")
    private Double length;
    /**
     * 探槽深 [米]
     */
    @Column(name="depth")
    private Double depth;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 环境照片原始文件编号
     */
    @Column(name="photo_arwid")
    private String photoArwid;
    /**
     * 探槽剖面图2拼接照片图像文件编号
     */
    @Column(name="profile2photo_aiid")
    private String profile2photoAiid;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 揭露地层数
     */
    @Column(name="exposedstratumcount")
    private Integer exposedstratumcount;
    /**
     * 最晚古地震发震时代
     */
    @Column(name="latesteqperoidest")
    private Integer latesteqperoidest;
    /**
     * 探槽剖面地层描述1原始文件编号
     */
    @Column(name="profile1comment_arwid")
    private String profile1commentArwid;
    /**
     * 环境照片图像编号
     */
    @Column(name="photo_aiid")
    private String photoAiid;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 最晚古地震发震时代误差
     */
    @Column(name="latesteqperoider")
    private Integer latesteqperoider;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 拍摄者
     */
    @Column(name="photographer")
    private String photographer;
    /**
     * 探槽描述
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 纬度
     */
    @Column(name="lat")
    private Double lat;
    /**
     * 探槽剖面图1图像文件编号
     */
    @Column(name="profile1_aiid")
    private String profile1Aiid;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 探槽剖面地层描述2文件编号
     */
    @Column(name="profile2comment_arid")
    private String profile2commentArid;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;

    /**
     * 目标断层来源
     */
    @Column(name="targetfaultsource")
    private String targetfaultsource;
    /**
     * 目标断层编号
     */
    @Column(name="targetfaultid")
    private String targetfaultid;
    /**
     * 探槽剖面图2原始文件编号
     */
    @Column(name="profile2_arwid")
    private String profile2Arwid;
    /**
     * 探槽剖面图1拼接照片图像文件编号
     */
    @Column(name="profile1photo_aiid")
    private String profile1photoAiid;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 探槽剖面图1照片原始文件编号
     */
    @Column(name="profile1photo_arwid")
    private String profile1photoArwid;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 探槽剖面地层描述2原始文件编号
     */
    @Column(name="profile2comment_arwid")
    private String profile2commentArwid;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 探槽宽 [米]
     */
    @Column(name="width")
    private Double width;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 探槽剖面图1原始文件编号
     */
    @Column(name="profile1_arwid")
    private String profile1Arwid;
    /**
     * 所属地质调查工程编号
     */
    @Column(name="geologysvyprojectid")
    private String geologysvyprojectid;
    /**
     * 目标断层名称
     */
    @Column(name="targetfaultname")
    private String targetfaultname;
    /**
     * 高程 [米]
     */
    @Column(name="elevation")
    private Double elevation;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 探槽剖面地层描述1文件编号
     */
    @Column(name="profile1comment_arid")
    private String profile1commentArid;
    /**
     * 地貌环境
     */
    @Column(name="geomophenv")
    private String geomophenv;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 探槽剖面图2照片原始文件编号
     */
    @Column(name="profile2photo_arwid")
    private String profile2photoArwid;
    /**
     * 送样总数
     */
    @Column(name="samplecount")
    private Integer samplecount;
    /**
     * 参考位置
     */
    @Column(name="locationname")
    private String locationname;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 获得结果的样品数
     */
    @Column(name="datingsamplecount")
    private Integer datingsamplecount;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 探槽剖面图2图像文件编号
     */
    @Column(name="profile2_aiid")
    private String profile2Aiid;
    /**
     * 经度
     */
    @Column(name="lon")
    private Double lon;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 收集探槽来源补充说明
     */
    @Column(name="collectedtrenchsource")
    private String collectedtrenchsource;
    /**
     * 探槽编号
     */
    //@Id
    @Column(name="id")
    private String id;
    /**
     * 探槽编号
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 野外编号
     */
    @Column(name="fieldid")
    private String fieldid;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 采集样品总数
     */
    @Column(name="collectedsamplecount")
    private Integer collectedsamplecount;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 符号或标注旋转角度
     */
    @Column(name="lastangle")
    private Double lastangle;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 古地震事件次数
     */
    @Column(name="eqeventcount")
    private Integer eqeventcount;

    /**
     * 目标断层来源字典名称
     */
    @Column(name="targetfaultsource_name")
    private String targetfaultsourceName;
    /**
     * 探槽来源与类型字典名称
     */
    @Column(name="trenchsource_name")
    private String trenchsourceName;
    /**
     * 探槽方向角度 [度]字典名称
     */
    @Column(name="trenchdip_name")
    private String trenchdipName;
    /**
     * 照片镜向字典名称
     */
    @Column(name="photoviewingto_name")
    private String photoviewingtoName;
}

