package com.css.zfzx.sjcj.modules.hddcVerticalDeformation.repository;

import com.css.zfzx.sjcj.modules.hddcVerticalDeformation.repository.entity.HddcVerticaldeformationEntity;
import com.css.zfzx.sjcj.modules.hddcVerticalDeformation.viewobjects.HddcVerticaldeformationQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-26
 */
public interface HddcVerticaldeformationNativeRepository {

    Page<HddcVerticaldeformationEntity> queryHddcVerticaldeformations(HddcVerticaldeformationQueryParams queryParams, int curPage, int pageSize);

    List<HddcVerticaldeformationEntity> exportYhDisasters(HddcVerticaldeformationQueryParams queryParams);
}
