package com.css.zfzx.sjcj.modules.yhschool.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.yhschool.repository.YhSchoolNativeRepository;
import com.css.zfzx.sjcj.modules.yhschool.repository.YhSchoolRepository;
import com.css.zfzx.sjcj.modules.yhschool.repository.entity.YhSchoolEntity;
import com.css.zfzx.sjcj.modules.yhschool.service.YhSchoolService;
import com.css.zfzx.sjcj.modules.yhschool.viewobjects.YhSchoolQueryParams;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author yyd
 * @date 2020-11-03
 */
@Service
public class YhSchoolServiceImpl implements YhSchoolService {

	@Autowired
    private YhSchoolRepository yhSchoolRepository;
    @Autowired
    private YhSchoolNativeRepository yhSchoolNativeRepository;

    @Override
    public JSONObject queryYhSchools(YhSchoolQueryParams queryParams, int curPage, int pageSize) {
        Page<YhSchoolEntity> yhSchoolPage = this.yhSchoolNativeRepository.queryYhSchools(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(yhSchoolPage);
        return jsonObject;
    }


    @Override
    public YhSchoolEntity getYhSchool(String id) {
        YhSchoolEntity yhSchool = this.yhSchoolRepository.findById(id).orElse(null);
         return yhSchool;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public YhSchoolEntity saveYhSchool(YhSchoolEntity yhSchool) {
        String uuid = UUIDGenerator.getUUID();
        yhSchool.setId(uuid);
        yhSchool.setIsValid("0");
        yhSchool.setCreateUser(PlatformSessionUtils.getUserId());
        yhSchool.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.yhSchoolRepository.save(yhSchool);
        return yhSchool;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public YhSchoolEntity updateYhSchool(YhSchoolEntity yhSchool) {
        YhSchoolEntity entity = yhSchoolRepository.findById(yhSchool.getId()).get();
        UpdateUtil.copyNullProperties(entity,yhSchool);
        yhSchool.setUpdateUser(PlatformSessionUtils.getUserId());
        yhSchool.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.yhSchoolRepository.save(yhSchool);
        return yhSchool;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteYhSchools(List<String> ids) {
        List<YhSchoolEntity> yhSchoolList = this.yhSchoolRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(yhSchoolList) && yhSchoolList.size() > 0) {
            for(YhSchoolEntity yhSchool : yhSchoolList) {
                this.yhSchoolRepository.delete(yhSchool);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

}
