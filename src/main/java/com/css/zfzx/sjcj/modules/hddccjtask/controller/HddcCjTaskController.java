package com.css.zfzx.sjcj.modules.hddccjtask.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.zfzx.sjcj.modules.hddccjtask.repository.entity.HddcCjTaskEntity;
import com.css.zfzx.sjcj.modules.hddccjtask.service.HddcCjTaskService;
import com.css.zfzx.sjcj.modules.hddccjtask.viewobjects.HddcCjTaskQueryParams;
import com.css.zfzx.sjcj.modules.hddccjtask.viewobjects.HddcCjTaskVO;
import com.css.bpm.platform.utils.PlatformPageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangping
 * @date 2020-11-26
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcCjTasks")
public class HddcCjTaskController {
    @Autowired
    private HddcCjTaskService hddcCjTaskService;

    @GetMapping("/queryHddcCjTasks")
    public RestResponse queryHddcCjTasks(HttpServletRequest request, HddcCjTaskQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcCjTaskService.queryHddcCjTasks(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/queryHddcCjTasksByProjectId")
    public RestResponse queryHddcCjTasksByProjectId(String projectId,int curPage,int pageSize) {
        RestResponse response = null;
        try{
            JSONObject jsonObject = hddcCjTaskService.queryHddcCjTasksByProjectId(projectId,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/queryAllHddcCjTasks")
    public RestResponse queryAllHddcCjTasks() {
        RestResponse response = null;
        try{
            //int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            //int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            List<HddcCjTaskEntity> list = hddcCjTaskService.findAll();
            response = RestResponse.succeed(list);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcCjTask(@PathVariable String id) {
        RestResponse response = null;
        try{
            HddcCjTaskVO hddcCjTask = hddcCjTaskService.getHddcCjTask(id);
            response = RestResponse.succeed(hddcCjTask);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcCjTask(@RequestBody HddcCjTaskEntity hddcCjTask) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcCjTaskService.saveHddcCjTask(hddcCjTask);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcCjTask(@RequestBody HddcCjTaskEntity hddcCjTask)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcCjTaskService.updateHddcCjTask(hddcCjTask);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcCjTasks(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcCjTaskService.deleteHddcCjTasks(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcCjTaskService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }

    @GetMapping("/findHddcCjTaskEntityByProjectIds/{projectid}")
    public RestResponse findHddcCjTaskEntityByProjectIds(@PathVariable String projectid) {
        RestResponse response = null;
        try{
            List<HddcCjTaskEntity> list = hddcCjTaskService.findHddcCjTaskEntityByProjectIds(projectid);
            response = RestResponse.succeed(list);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

}