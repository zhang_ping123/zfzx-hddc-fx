package com.css.zfzx.sjcj.modules.hddcGeomorphyline.repository;

import com.css.zfzx.sjcj.modules.hddcGeomorphyline.repository.entity.HddcGeomorphylineEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-12-09
 */
public interface HddcGeomorphylineRepository extends JpaRepository<HddcGeomorphylineEntity, String> {
}
