package com.css.zfzx.sjcj.modules.hddcVolcanicSamplePoint.repository;

import com.css.zfzx.sjcj.modules.hddcVolcanicSamplePoint.repository.entity.HddcVolcanicsamplepointEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
public interface HddcVolcanicsamplepointRepository extends JpaRepository<HddcVolcanicsamplepointEntity, String> {
}
