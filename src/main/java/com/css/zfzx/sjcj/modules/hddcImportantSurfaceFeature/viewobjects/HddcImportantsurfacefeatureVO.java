package com.css.zfzx.sjcj.modules.hddcImportantSurfaceFeature.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author lihelei
 * @date 2020-11-30
 */
@Data
public class HddcImportantsurfacefeatureVO implements Serializable {

    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 村
     */
    private String village;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 名称
     */
    @Excel(name = "名称", orderNum = "4")
    private String name;
    /**
     * 备注
     */
    private String remark;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 编号
     */
    private String id;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 是否在图中显示
     */
    @Excel(name = "是否在图中显示", orderNum = "5")
    private Integer isinmap;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "6")
    private String commentInfo;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 国标值
     */
    @Excel(name = "国标值", orderNum = "7")
    private Integer gb;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 地物类型
     */
    @Excel(name = "地物类型", orderNum = "8")
    private String datatype;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer isinmapName;
    private String rowNum;
    private String errorMsg;
}