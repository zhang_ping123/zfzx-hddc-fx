package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyLine.repository;

import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyLine.repository.entity.HddcWyGeomorphysvylineEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author lihelei
 * @date 2020-12-01
 */
public interface HddcWyGeomorphysvylineRepository extends JpaRepository<HddcWyGeomorphysvylineEntity, String> {

    List<HddcWyGeomorphysvylineEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    List<HddcWyGeomorphysvylineEntity> findAllByCreateUserAndIsValid(String userId,String isValid);


    @Query(nativeQuery = true, value = "select * from hddc_wy_geomorphysvyline where is_valid!=0 project_name in :projectIds")
    List<HddcWyGeomorphysvylineEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_geomorphysvyline where task_name in :projectIds")
    List<HddcWyGeomorphysvylineEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);


    @Query(nativeQuery = true, value = "select * from hddc_wy_geomorphysvyline where create_user = :createID")
    List<HddcWyGeomorphysvylineEntity> queryHddcWyGeomorphysvylineByCreateuser(String createID);
}
