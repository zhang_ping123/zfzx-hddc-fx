package com.css.zfzx.sjcj.modules.hddcGeochemicalSvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyPoint.repository.entity.HddcGeochemicalsvypointEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
public interface HddcGeochemicalsvypointRepository extends JpaRepository<HddcGeochemicalsvypointEntity, String> {
}
