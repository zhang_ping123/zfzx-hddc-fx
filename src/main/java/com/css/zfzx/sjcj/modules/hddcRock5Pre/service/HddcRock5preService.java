package com.css.zfzx.sjcj.modules.hddcRock5Pre.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcRock5Pre.repository.entity.HddcRock5preEntity;
import com.css.zfzx.sjcj.modules.hddcRock5Pre.viewobjects.HddcRock5preQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author lhl
 * @date 2020-11-27
 */

public interface HddcRock5preService {

    public JSONObject queryHddcRock5pres(HddcRock5preQueryParams queryParams, int curPage, int pageSize);

    public HddcRock5preEntity getHddcRock5pre(String id);

    public HddcRock5preEntity saveHddcRock5pre(HddcRock5preEntity hddcRock5pre);

    public HddcRock5preEntity updateHddcRock5pre(HddcRock5preEntity hddcRock5pre);

    public void deleteHddcRock5pres(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    String findByDictCodeAndDictItemCode(String dictCode, String dictItemCode);

    void exportFile(HddcRock5preQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
