package com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningPt.repository;

import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningPt.repository.entity.HddcWyGeologicalsvyplanningptEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcWyGeologicalsvyplanningptRepository extends JpaRepository<HddcWyGeologicalsvyplanningptEntity, String> {

    List<HddcWyGeologicalsvyplanningptEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId,String taskId,String projectId,String isValid);

    List<HddcWyGeologicalsvyplanningptEntity> findAllByCreateUserAndIsValid(String userId,String isValid);



    @Query(nativeQuery = true, value = "select * from hddc_wy_geologicalsvyplanningpt where is_valid!=0 project_name in :projectIds")
    List<HddcWyGeologicalsvyplanningptEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_geologicalsvyplanningpt where task_name in :projectIds")
    List<HddcWyGeologicalsvyplanningptEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);


}
