package com.css.zfzx.sjcj.modules.hddcA6StationHasWaveforms.repository;

import com.css.zfzx.sjcj.modules.hddcA6StationHasWaveforms.repository.entity.HddcA6StationhaswaveformsEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-28
 */
public interface HddcA6StationhaswaveformsRepository extends JpaRepository<HddcA6StationhaswaveformsEntity, String> {
}
