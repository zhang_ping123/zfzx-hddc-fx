package com.css.zfzx.sjcj.modules.hddcwyGeomorStation.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcGeomorStation.repository.HddcGeomorstationRepository;
import com.css.zfzx.sjcj.modules.hddcGeomorStation.repository.entity.HddcGeomorstationEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvySamplePoint.repository.entity.HddcGeomorphysvysamplepointEntity;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorStation.repository.HddcWyGeomorstationNativeRepository;
import com.css.zfzx.sjcj.modules.hddcwyGeomorStation.repository.HddcWyGeomorstationRepository;
import com.css.zfzx.sjcj.modules.hddcwyGeomorStation.repository.entity.HddcWyGeomorstationEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorStation.service.HddcWyGeomorstationService;
import com.css.zfzx.sjcj.modules.hddcwyGeomorStation.viewobjects.HddcWyGeomorstationQueryParams;
import com.css.zfzx.sjcj.modules.hddcwyGeomorStation.viewobjects.HddcWyGeomorstationVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvySamplePoint.viewobjects.HddcWyGeomorphysvysamplepointVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zhangcong
 * @date 2020-12-01
 */
@Service
public class HddcWyGeomorstationServiceImpl implements HddcWyGeomorstationService {

	@Autowired
    private HddcWyGeomorstationRepository hddcWyGeomorstationRepository;
    @Autowired
    private HddcWyGeomorstationNativeRepository hddcWyGeomorstationNativeRepository;
    @Autowired
    private HddcGeomorstationRepository hddcGeomorstationRepository;

    @Override
    public JSONObject queryHddcWyGeomorstations(HddcWyGeomorstationQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcWyGeomorstationEntity> hddcWyGeomorstationPage = this.hddcWyGeomorstationNativeRepository.queryHddcWyGeomorstations(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcWyGeomorstationPage);
        return jsonObject;
    }


    @Override
    public HddcWyGeomorstationEntity getHddcWyGeomorstation(String uuid) {
        HddcWyGeomorstationEntity hddcWyGeomorstation = this.hddcWyGeomorstationRepository.findById(uuid).orElse(null);
         return hddcWyGeomorstation;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyGeomorstationEntity saveHddcWyGeomorstation(HddcWyGeomorstationEntity hddcWyGeomorstation) {
        String uuid = UUIDGenerator.getUUID();
        hddcWyGeomorstation.setUuid(uuid);
        if(StringUtils.isEmpty(hddcWyGeomorstation.getCreateUser())){
            hddcWyGeomorstation.setCreateUser(PlatformSessionUtils.getUserId());
        }
        hddcWyGeomorstation.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        hddcWyGeomorstation.setIsValid("1");

        HddcGeomorstationEntity hddcGeomorstationEntity=new HddcGeomorstationEntity();
        BeanUtils.copyProperties(hddcWyGeomorstation,hddcGeomorstationEntity);
        hddcGeomorstationEntity.setExtends4(hddcWyGeomorstation.getTown());
        hddcGeomorstationEntity.setExtends5(String.valueOf(hddcWyGeomorstation.getLon()));
        hddcGeomorstationEntity.setExtends6(String.valueOf(hddcWyGeomorstation.getLat()));
        this.hddcGeomorstationRepository.save(hddcGeomorstationEntity);


        this.hddcWyGeomorstationRepository.save(hddcWyGeomorstation);
        return hddcWyGeomorstation;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyGeomorstationEntity updateHddcWyGeomorstation(HddcWyGeomorstationEntity hddcWyGeomorstation) {
        HddcWyGeomorstationEntity entity = hddcWyGeomorstationRepository.findById(hddcWyGeomorstation.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcWyGeomorstation);
        hddcWyGeomorstation.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcWyGeomorstation.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcWyGeomorstationRepository.save(hddcWyGeomorstation);
        return hddcWyGeomorstation;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcWyGeomorstations(List<String> ids) {
        List<HddcWyGeomorstationEntity> hddcWyGeomorstationList = this.hddcWyGeomorstationRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcWyGeomorstationList) && hddcWyGeomorstationList.size() > 0) {
            for(HddcWyGeomorstationEntity hddcWyGeomorstation : hddcWyGeomorstationList) {
                this.hddcWyGeomorstationRepository.delete(hddcWyGeomorstation);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public BigInteger queryHddcWyGeomorstation(HddcAppZztCountVo queryParams) {
        BigInteger bigInteger = hddcWyGeomorstationNativeRepository.queryHddcWyGeomorstation(queryParams);
        return bigInteger;
    }

    @Override
    public List<HddcWyGeomorstationEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid) {
        List<HddcWyGeomorstationEntity> list = hddcWyGeomorstationRepository.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, isValid);
        return list;
    }

    @Override
    public void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response) {
        List<HddcWyGeomorstationEntity> hddcWyGeomorstationEntity = hddcWyGeomorstationNativeRepository.exportStation(queryParams);
        List<HddcWyGeomorstationVO> list=new ArrayList<>();
        for (HddcWyGeomorstationEntity entity:hddcWyGeomorstationEntity) {
            HddcWyGeomorstationVO hddcWyGeomorstationVO=new HddcWyGeomorstationVO();
            BeanUtils.copyProperties(entity,hddcWyGeomorstationVO);
            list.add(hddcWyGeomorstationVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"微地貌测量基站点-点","微地貌测量基站点-点", HddcWyGeomorstationVO.class,"微地貌测量基站点-点.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcWyGeomorstationVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcWyGeomorstationVO.class, params);
            List<HddcWyGeomorstationVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcWyGeomorstationVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcWyGeomorstationVO hddcWyGeomorstationVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + hddcWyGeomorstationVO.getRowNum() + "行" + hddcWyGeomorstationVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcWyGeomorstationVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcWyGeomorstationEntity hddcWyGeomorstationEntity = new HddcWyGeomorstationEntity();
            HddcWyGeomorstationVO hddcWyGeomorstationVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(hddcWyGeomorstationVO, hddcWyGeomorstationEntity);
            saveHddcWyGeomorstation(hddcWyGeomorstationEntity);
        }
    }

    @Override
    public List<HddcWyGeomorstationEntity> findAllByCreateUserAndIsValid(String userId,String isValid) {
        List<HddcWyGeomorstationEntity> list = hddcWyGeomorstationRepository.findAllByCreateUserAndIsValid(userId,isValid);
        return list;
    }


    /**
     * 逻辑删除-根据项目id删除数据
     * @param projectIds
     */
    @Override
    public void deleteByProjectId(List<String> projectIds) {
        List<HddcWyGeomorstationEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyGeomorstationRepository.queryHddcA1InvrgnhasmaterialtablesByProjectId(projectIds);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyGeomorstationEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyGeomorstationRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }
    }

    /**
     * 逻辑删除-根据任务id删除数据
     * @param taskId
     */
    @Override
    public void deleteByTaskId(List<String> taskId) {
        List<HddcWyGeomorstationEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyGeomorstationRepository.queryHddcA1InvrgnhasmaterialtablesByTaskId(taskId);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyGeomorstationEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyGeomorstationRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }

    }










}
