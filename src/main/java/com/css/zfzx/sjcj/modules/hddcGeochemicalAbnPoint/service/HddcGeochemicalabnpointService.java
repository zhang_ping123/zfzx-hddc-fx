package com.css.zfzx.sjcj.modules.hddcGeochemicalAbnPoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnPoint.repository.entity.HddcGeochemicalabnpointEntity;
import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnPoint.viewobjects.HddcGeochemicalabnpointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */

public interface HddcGeochemicalabnpointService {

    public JSONObject queryHddcGeochemicalabnpoints(HddcGeochemicalabnpointQueryParams queryParams, int curPage, int pageSize);

    public HddcGeochemicalabnpointEntity getHddcGeochemicalabnpoint(String id);

    public HddcGeochemicalabnpointEntity saveHddcGeochemicalabnpoint(HddcGeochemicalabnpointEntity hddcGeochemicalabnpoint);

    public HddcGeochemicalabnpointEntity updateHddcGeochemicalabnpoint(HddcGeochemicalabnpointEntity hddcGeochemicalabnpoint);

    public void deleteHddcGeochemicalabnpoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGeochemicalabnpointQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
