package com.css.zfzx.sjcj.modules.hddcB7_VolcanicSampleResultTable.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-11-26
 */
@Data
public class HddcB7VolcanicsampleresulttableQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

}
