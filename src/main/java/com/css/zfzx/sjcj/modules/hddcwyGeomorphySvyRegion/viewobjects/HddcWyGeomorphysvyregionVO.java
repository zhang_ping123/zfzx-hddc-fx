package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyRegion.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author lihelei
 * @date 2020-12-01
 */
@Data
public class HddcWyGeomorphysvyregionVO implements Serializable {

    /**
     * 地貌面结果图文件原始文件编号
     */
    @Excel(name="地貌面结果图文件原始文件编号",orderNum = "9")
    private String profileArwid;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 野外编号
     */
    @Excel(name="野外编号",orderNum = "15")
    private String fieldid;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * PhotoViewingTo
     */
    @Excel(name="PhotoViewingTo",orderNum = "16")
    private Integer photoviewingto;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 测量区域名称
     */
    @Excel(name="测量区域名称",orderNum = "18")
    private String name;
    /**
     * 典型照片文件编号
     */
    @Excel(name="典型照片文件编号",orderNum = "14")
    private String photoAiid;
    /**
     * 任务名称
     */
    @Excel(name="任务名称",orderNum = "8")
    private String taskName;
    /**
     * 备选字段5
     */
    @Excel(name="区域",orderNum = "6")
    private String extends5;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 省
     */
    @Excel(name="省",orderNum = "2")
    private String province;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 典型照片原始文件编号
     */
    @Excel(name="典型照片原始文件编号",orderNum = "17")
    private String photoArwid;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 区（县）
     */
    @Excel(name="区（县）",orderNum = "4")
    private String area;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 测量区域编号
     */
    @Excel(name="测量区域编号",orderNum = "1")
    private String id;
    /**
     * 备注-测量面描述及目的
     */
    @Excel(name="备注-测量面描述及目的",orderNum = "11")
    private String commentInfo;
    /**
     * 地貌面分析结果图图像文件编号
     */
    @Excel(name="地貌面分析结果图图像文件编号",orderNum = "13")
    private String profileAiid;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 乡
     */
    @Excel(name="详细地址",orderNum = "5")
    private String town;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备注
     */
    private String remark;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 项目名称
     */
    @Excel(name="项目名称",orderNum = "7")
    private String projectName;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 拍摄者
     */
    @Excel(name="拍摄者",orderNum = "10")
    private String photographer;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 微地貌测量工程编号
     */
    @Excel(name="微地貌测量工程编号",orderNum = "12")
    private String geomorphysvyprjid;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 市
     */
    @Excel(name="市",orderNum = "3")
    private String city;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 经度
     */
    @Column(name="lon")
    private Double lon;
    /**
     * 维度
     */
    @Column(name="lat")
    private Double lat;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String rowNum;
    private String errorMsg;
}