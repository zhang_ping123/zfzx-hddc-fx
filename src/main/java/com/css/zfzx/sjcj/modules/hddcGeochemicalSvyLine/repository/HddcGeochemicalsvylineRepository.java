package com.css.zfzx.sjcj.modules.hddcGeochemicalSvyLine.repository;

import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyLine.repository.entity.HddcGeochemicalsvylineEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
public interface HddcGeochemicalsvylineRepository extends JpaRepository<HddcGeochemicalsvylineEntity, String> {
}
