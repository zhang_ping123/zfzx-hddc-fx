package com.css.zfzx.sjcj.modules.hddcB3DrillProjectTable.repository;

import com.css.zfzx.sjcj.modules.hddcB3DrillProjectTable.repository.entity.HddcB3DrillprojecttableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcB3DrillprojecttableRepository extends JpaRepository<HddcB3DrillprojecttableEntity, String> {
}
