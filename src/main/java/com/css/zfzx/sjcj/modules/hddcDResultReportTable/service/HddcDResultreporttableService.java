package com.css.zfzx.sjcj.modules.hddcDResultReportTable.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcDResultReportTable.repository.entity.HddcDResultreporttableEntity;
import com.css.zfzx.sjcj.modules.hddcDResultReportTable.viewobjects.HddcDResultreporttableQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.hibernate.criterion.Order;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-19
 */

public interface HddcDResultreporttableService {

    public JSONObject queryHddcDResultreporttables(HddcDResultreporttableQueryParams queryParams, int curPage, int pageSize,String sort,String Order);

    public HddcDResultreporttableEntity getHddcDResultreporttable(String id);

    public HddcDResultreporttableEntity saveHddcDResultreporttable(HddcDResultreporttableEntity hddcDResultreporttable);

    public HddcDResultreporttableEntity updateHddcDResultreporttable(HddcDResultreporttableEntity hddcDResultreporttable);

    public void deleteHddcDResultreporttables(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

}
