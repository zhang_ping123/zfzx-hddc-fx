package com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyPoint.repository.entity.HddcWyGeochemicalsvypointEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-02
 */
public interface HddcWyGeochemicalsvypointRepository extends JpaRepository<HddcWyGeochemicalsvypointEntity, String> {

    List<HddcWyGeochemicalsvypointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    List<HddcWyGeochemicalsvypointEntity> findAllByCreateUserAndIsValid(String userId, String isValid);

    @Query(nativeQuery = true, value = "select * from hddc_wy_geochemicalsvypoint where is_valid!=0 project_name in :projectIds")
    List<HddcWyGeochemicalsvypointEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);

    @Query(nativeQuery = true, value = "select * from hddc_wy_geochemicalsvypoint where task_name in :projectIds")
    List<HddcWyGeochemicalsvypointEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);

}
