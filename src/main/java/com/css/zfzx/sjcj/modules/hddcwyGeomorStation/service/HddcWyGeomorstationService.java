package com.css.zfzx.sjcj.modules.hddcwyGeomorStation.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorStation.repository.entity.HddcWyGeomorstationEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorStation.viewobjects.HddcWyGeomorstationQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-01
 */

public interface HddcWyGeomorstationService {

    public JSONObject queryHddcWyGeomorstations(HddcWyGeomorstationQueryParams queryParams, int curPage, int pageSize);

    public HddcWyGeomorstationEntity getHddcWyGeomorstation(String uuid);

    public HddcWyGeomorstationEntity saveHddcWyGeomorstation(HddcWyGeomorstationEntity hddcWyGeomorstation);

    public HddcWyGeomorstationEntity updateHddcWyGeomorstation(HddcWyGeomorstationEntity hddcWyGeomorstation);

    public void deleteHddcWyGeomorstations(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    BigInteger queryHddcWyGeomorstation(HddcAppZztCountVo queryParams);

    List<HddcWyGeomorstationEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);

    List<HddcWyGeomorstationEntity> findAllByCreateUserAndIsValid(String userId,String isValid);



    /**
     * 逻辑删除-根据项目id删除数据
     * @param ids
     */
    void deleteByProjectId(List<String> ids);

    /**
     * 逻辑删除-根据任务id删除数据
     * @param ids
     */
    void deleteByTaskId(List<String> ids);


}
