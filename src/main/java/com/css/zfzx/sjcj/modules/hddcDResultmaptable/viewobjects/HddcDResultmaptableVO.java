package com.css.zfzx.sjcj.modules.hddcDResultmaptable.viewobjects;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-12-19
 */
@Data
public class HddcDResultmaptableVO implements Serializable {

    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 乡
     */
    private String town;
    /**
     * 编制时间
     */
    private String finishedtime;
    /**
     * 主键
     */
    private String uuid;
    /**
     * 资料/数据处理记录
     */
    private String processingrecord;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 投影信息
     */
    private String projection;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 图件比例尺（分母）
     */
    private Integer scale;
    /**
     * 备注
     */
    private String remark;
    /**
     * 项目id
     */
    private String projectCode;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 使用资料记录
     */
    private String materialrecord;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 区（县）
     */
    private String area;
    /**
     * 探测城市
     */
    private String cityname;
    /**
     * 村
     */
    private String village;
    /**
     * 作者
     */
    private String authors;
    /**
     * 工作区编号
     */
    private String workregionid;
    /**
     * 坐标系
     */
    private String coordinatesystem;
    /**
     * 附件id
     */
    private String attachid;
    /**
     * 附件路径
     */
    private String attachpath;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 工程编号
     */
    private String projectId;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 市
     */
    private String city;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 编号
     */
    private String id;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 目标区编号
     */
    private String targetregionid;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 图书在版编目（cip）数据
     */
    private String cip;
    /**
     * 出版日期
     */
    private String publishdate;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 任务id
     */
    private String taskId;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 出版单位
     */
    private String publisher;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 成果图原始档案文件编号
     */
    private String resultmapArwid;
    /**
     * 描述信息
     */
    private String commentInfo;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 地质填图区编号
     */
    private String mainafsregionid;
    /**
     * 成果图图像文件编号
     */
    private String resultmapAiid;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 名称
     */
    private String filename;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 省
     */
    private String province;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段5
     */
    private String extends5;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String projectNameName;
    private String taskNameName;
}