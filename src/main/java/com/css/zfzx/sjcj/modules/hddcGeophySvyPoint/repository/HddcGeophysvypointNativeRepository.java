package com.css.zfzx.sjcj.modules.hddcGeophySvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcGeophySvyPoint.repository.entity.HddcGeophysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcGeophySvyPoint.viewobjects.HddcGeophysvypointQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
public interface HddcGeophysvypointNativeRepository {

    Page<HddcGeophysvypointEntity> queryHddcGeophysvypoints(HddcGeophysvypointQueryParams queryParams, int curPage, int pageSize);

    List<HddcGeophysvypointEntity> exportYhDisasters(HddcGeophysvypointQueryParams queryParams);
}
