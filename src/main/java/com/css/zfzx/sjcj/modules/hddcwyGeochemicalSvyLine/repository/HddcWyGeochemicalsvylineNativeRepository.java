package com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyLine.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyLine.repository.entity.HddcWyGeochemicalsvylineEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyLine.viewobjects.HddcWyGeochemicalsvylineQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-02
 */
public interface HddcWyGeochemicalsvylineNativeRepository {

    Page<HddcWyGeochemicalsvylineEntity> queryHddcWyGeochemicalsvylines(HddcWyGeochemicalsvylineQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyGeochemicalsvyline(HddcAppZztCountVo queryParams);

    List<HddcWyGeochemicalsvylineEntity> exportChemLine(HddcAppZztCountVo queryParams);
}
