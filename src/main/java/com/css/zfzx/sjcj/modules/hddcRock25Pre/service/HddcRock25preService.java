package com.css.zfzx.sjcj.modules.hddcRock25Pre.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcRock25Pre.repository.entity.HddcRock25preEntity;
import com.css.zfzx.sjcj.modules.hddcRock25Pre.viewobjects.HddcRock25preQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */

public interface HddcRock25preService {

    public JSONObject queryHddcRock25pres(HddcRock25preQueryParams queryParams, int curPage, int pageSize);

    public HddcRock25preEntity getHddcRock25pre(String id);

    public HddcRock25preEntity saveHddcRock25pre(HddcRock25preEntity hddcRock25pre);

    public HddcRock25preEntity updateHddcRock25pre(HddcRock25preEntity hddcRock25pre);

    public void deleteHddcRock25pres(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    String findByDictCodeAndDictItemCode(String dictCode, String dictItemCode);

    void exportFile(HddcRock25preQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
