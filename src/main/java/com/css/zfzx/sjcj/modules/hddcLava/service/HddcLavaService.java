package com.css.zfzx.sjcj.modules.hddcLava.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcLava.repository.entity.HddcLavaEntity;
import com.css.zfzx.sjcj.modules.hddcLava.viewobjects.HddcLavaQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-27
 */

public interface HddcLavaService {

    public JSONObject queryHddcLavas(HddcLavaQueryParams queryParams, int curPage, int pageSize);

    public HddcLavaEntity getHddcLava(String id);

    public HddcLavaEntity saveHddcLava(HddcLavaEntity hddcLava);

    public HddcLavaEntity updateHddcLava(HddcLavaEntity hddcLava);

    public void deleteHddcLavas(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcLavaQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
