package com.css.zfzx.sjcj.modules.hddcGeomorphySvyPoint.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyPoint.repository.HddcGeomorphysvypointNativeRepository;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyPoint.repository.HddcGeomorphysvypointRepository;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyPoint.repository.entity.HddcGeomorphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyPoint.service.HddcGeomorphysvypointService;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyPoint.viewobjects.HddcGeomorphysvypointQueryParams;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyPoint.viewobjects.HddcGeomorphysvypointVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Service
public class HddcGeomorphysvypointServiceImpl implements HddcGeomorphysvypointService {

	@Autowired
    private HddcGeomorphysvypointRepository hddcGeomorphysvypointRepository;
    @Autowired
    private HddcGeomorphysvypointNativeRepository hddcGeomorphysvypointNativeRepository;

    @Override
    public JSONObject queryHddcGeomorphysvypoints(HddcGeomorphysvypointQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcGeomorphysvypointEntity> hddcGeomorphysvypointPage = this.hddcGeomorphysvypointNativeRepository.queryHddcGeomorphysvypoints(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcGeomorphysvypointPage);
        return jsonObject;
    }


    @Override
    public HddcGeomorphysvypointEntity getHddcGeomorphysvypoint(String id) {
        HddcGeomorphysvypointEntity hddcGeomorphysvypoint = this.hddcGeomorphysvypointRepository.findById(id).orElse(null);
         return hddcGeomorphysvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeomorphysvypointEntity saveHddcGeomorphysvypoint(HddcGeomorphysvypointEntity hddcGeomorphysvypoint) {
        String uuid = UUIDGenerator.getUUID();
        hddcGeomorphysvypoint.setUuid(uuid);
        hddcGeomorphysvypoint.setCreateUser(PlatformSessionUtils.getUserId());
        hddcGeomorphysvypoint.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeomorphysvypointRepository.save(hddcGeomorphysvypoint);
        return hddcGeomorphysvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeomorphysvypointEntity updateHddcGeomorphysvypoint(HddcGeomorphysvypointEntity hddcGeomorphysvypoint) {
        HddcGeomorphysvypointEntity entity = hddcGeomorphysvypointRepository.findById(hddcGeomorphysvypoint.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcGeomorphysvypoint);
        hddcGeomorphysvypoint.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcGeomorphysvypoint.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeomorphysvypointRepository.save(hddcGeomorphysvypoint);
        return hddcGeomorphysvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcGeomorphysvypoints(List<String> ids) {
        List<HddcGeomorphysvypointEntity> hddcGeomorphysvypointList = this.hddcGeomorphysvypointRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcGeomorphysvypointList) && hddcGeomorphysvypointList.size() > 0) {
            for(HddcGeomorphysvypointEntity hddcGeomorphysvypoint : hddcGeomorphysvypointList) {
                this.hddcGeomorphysvypointRepository.delete(hddcGeomorphysvypoint);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcGeomorphysvypointQueryParams queryParams, HttpServletResponse response) {
        List<HddcGeomorphysvypointEntity> yhDisasterEntities = hddcGeomorphysvypointNativeRepository.exportYhDisasters(queryParams);
        List<HddcGeomorphysvypointVO> list=new ArrayList<>();
        for (HddcGeomorphysvypointEntity entity:yhDisasterEntities) {
            HddcGeomorphysvypointVO yhDisasterVO=new HddcGeomorphysvypointVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "微地貌测量点-点", "微地貌测量点-点", HddcGeomorphysvypointVO.class, "微地貌测量点-点.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcGeomorphysvypointVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcGeomorphysvypointVO.class, params);
            List<HddcGeomorphysvypointVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcGeomorphysvypointVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcGeomorphysvypointVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcGeomorphysvypointVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcGeomorphysvypointEntity yhDisasterEntity = new HddcGeomorphysvypointEntity();
            HddcGeomorphysvypointVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcGeomorphysvypoint(yhDisasterEntity);
        }
    }

}
