package com.css.zfzx.sjcj.modules.hddcStratigraphy25LinePre.repository;

import com.css.zfzx.sjcj.modules.hddcStratigraphy25LinePre.repository.entity.HddcStratigraphy25linepreEntity;
import com.css.zfzx.sjcj.modules.hddcStratigraphy25LinePre.viewobjects.HddcStratigraphy25linepreQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcStratigraphy25linepreNativeRepository {

    Page<HddcStratigraphy25linepreEntity> queryHddcStratigraphy25linepres(HddcStratigraphy25linepreQueryParams queryParams, int curPage, int pageSize);

    List<HddcStratigraphy25linepreEntity> exportYhDisasters(HddcStratigraphy25linepreQueryParams queryParams);
}
