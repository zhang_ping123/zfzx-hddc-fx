package com.css.zfzx.sjcj.modules.hddcStationhddcStation.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcStationhddcStation.repository.HddcStationNativeRepository;
import com.css.zfzx.sjcj.modules.hddcStationhddcStation.repository.HddcStationRepository;
import com.css.zfzx.sjcj.modules.hddcStationhddcStation.repository.entity.HddcStationEntity;
import com.css.zfzx.sjcj.modules.hddcStationhddcStation.service.HddcStationService;
import com.css.zfzx.sjcj.modules.hddcStationhddcStation.viewobjects.HddcStationQueryParams;
import com.css.zfzx.sjcj.modules.hddcStationhddcStation.viewobjects.HddcStationVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Service
public class HddcStationServiceImpl implements HddcStationService {

	@Autowired
    private HddcStationRepository hddcStationRepository;
    @Autowired
    private HddcStationNativeRepository hddcStationNativeRepository;

    @Override
    public JSONObject queryHddcStations(HddcStationQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcStationEntity> hddcStationPage = this.hddcStationNativeRepository.queryHddcStations(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcStationPage);
        return jsonObject;
    }


    @Override
    public HddcStationEntity getHddcStation(String id) {
        HddcStationEntity hddcStation = this.hddcStationRepository.findById(id).orElse(null);
         return hddcStation;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcStationEntity saveHddcStation(HddcStationEntity hddcStation) {
        String uuid = UUIDGenerator.getUUID();
        hddcStation.setUuid(uuid);
        hddcStation.setCreateUser(PlatformSessionUtils.getUserId());
        hddcStation.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcStationRepository.save(hddcStation);
        return hddcStation;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcStationEntity updateHddcStation(HddcStationEntity hddcStation) {
        HddcStationEntity entity = hddcStationRepository.findById(hddcStation.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcStation);
        hddcStation.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcStation.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcStationRepository.save(hddcStation);
        return hddcStation;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcStations(List<String> ids) {
        List<HddcStationEntity> hddcStationList = this.hddcStationRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcStationList) && hddcStationList.size() > 0) {
            for(HddcStationEntity hddcStation : hddcStationList) {
                this.hddcStationRepository.delete(hddcStation);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcStationQueryParams queryParams, HttpServletResponse response) {
        List<HddcStationEntity> yhDisasterEntities = hddcStationNativeRepository.exportYhDisasters(queryParams);
        List<HddcStationVO> list=new ArrayList<>();
        for (HddcStationEntity entity:yhDisasterEntities) {
            HddcStationVO yhDisasterVO=new HddcStationVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "地震台站-点", "地震台站-点", HddcStationVO.class, "地震台站-点.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcStationVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcStationVO.class, params);
            List<HddcStationVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcStationVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcStationVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcStationVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcStationEntity yhDisasterEntity = new HddcStationEntity();
            HddcStationVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcStation(yhDisasterEntity);
        }
    }

}
