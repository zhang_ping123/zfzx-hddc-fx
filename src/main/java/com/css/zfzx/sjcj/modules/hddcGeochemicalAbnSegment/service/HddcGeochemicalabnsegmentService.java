package com.css.zfzx.sjcj.modules.hddcGeochemicalAbnSegment.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnSegment.repository.entity.HddcGeochemicalabnsegmentEntity;
import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnSegment.viewobjects.HddcGeochemicalabnsegmentQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */

public interface HddcGeochemicalabnsegmentService {

    public JSONObject queryHddcGeochemicalabnsegments(HddcGeochemicalabnsegmentQueryParams queryParams, int curPage, int pageSize);

    public HddcGeochemicalabnsegmentEntity getHddcGeochemicalabnsegment(String id);

    public HddcGeochemicalabnsegmentEntity saveHddcGeochemicalabnsegment(HddcGeochemicalabnsegmentEntity hddcGeochemicalabnsegment);

    public HddcGeochemicalabnsegmentEntity updateHddcGeochemicalabnsegment(HddcGeochemicalabnsegmentEntity hddcGeochemicalabnsegment);

    public void deleteHddcGeochemicalabnsegments(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGeochemicalabnsegmentQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
