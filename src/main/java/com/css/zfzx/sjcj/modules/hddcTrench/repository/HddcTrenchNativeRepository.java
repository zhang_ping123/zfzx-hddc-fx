package com.css.zfzx.sjcj.modules.hddcTrench.repository;

import com.css.zfzx.sjcj.modules.hddcTrench.repository.entity.HddcTrenchEntity;
import com.css.zfzx.sjcj.modules.hddcTrench.viewobjects.HddcTrenchQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcTrenchNativeRepository {

    Page<HddcTrenchEntity> queryHddcTrenchs(HddcTrenchQueryParams queryParams, int curPage, int pageSize);

    List<HddcTrenchEntity> exportYhDisasters(HddcTrenchQueryParams queryParams);
}
