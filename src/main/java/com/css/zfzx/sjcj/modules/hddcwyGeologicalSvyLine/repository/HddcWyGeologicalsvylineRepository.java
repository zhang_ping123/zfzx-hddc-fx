package com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyLine.repository;

import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyLine.repository.entity.HddcWyGeologicalsvylineEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */
public interface HddcWyGeologicalsvylineRepository extends JpaRepository<HddcWyGeologicalsvylineEntity, String> {

    List<HddcWyGeologicalsvylineEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    List<HddcWyGeologicalsvylineEntity> findAllByCreateUserAndIsValid(String userId, String isValid);

    @Query(nativeQuery = true, value = "select * from hddc_wy_geologicalsvyline where is_valid!=0 project_name in :projectIds")
    List<HddcWyGeologicalsvylineEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_geologicalsvyline where task_name in :projectIds")
    List<HddcWyGeologicalsvylineEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);

}
