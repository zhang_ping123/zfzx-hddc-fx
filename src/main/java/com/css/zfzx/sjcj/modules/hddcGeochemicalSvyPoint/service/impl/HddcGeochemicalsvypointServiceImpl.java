package com.css.zfzx.sjcj.modules.hddcGeochemicalSvyPoint.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltVO;
import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyPoint.repository.HddcGeochemicalsvypointNativeRepository;
import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyPoint.repository.HddcGeochemicalsvypointRepository;
import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyPoint.repository.entity.HddcGeochemicalsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyPoint.service.HddcGeochemicalsvypointService;
import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyPoint.viewobjects.HddcGeochemicalsvypointQueryParams;
import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyPoint.viewobjects.HddcGeochemicalsvypointVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Service
public class HddcGeochemicalsvypointServiceImpl implements HddcGeochemicalsvypointService {

	@Autowired
    private HddcGeochemicalsvypointRepository hddcGeochemicalsvypointRepository;
    @Autowired
    private HddcGeochemicalsvypointNativeRepository hddcGeochemicalsvypointNativeRepository;

    @Override
    public JSONObject queryHddcGeochemicalsvypoints(HddcGeochemicalsvypointQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcGeochemicalsvypointEntity> hddcGeochemicalsvypointPage = this.hddcGeochemicalsvypointNativeRepository.queryHddcGeochemicalsvypoints(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcGeochemicalsvypointPage);
        return jsonObject;
    }


    @Override
    public HddcGeochemicalsvypointEntity getHddcGeochemicalsvypoint(String id) {
        HddcGeochemicalsvypointEntity hddcGeochemicalsvypoint = this.hddcGeochemicalsvypointRepository.findById(id).orElse(null);
         return hddcGeochemicalsvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeochemicalsvypointEntity saveHddcGeochemicalsvypoint(HddcGeochemicalsvypointEntity hddcGeochemicalsvypoint) {
        String uuid = UUIDGenerator.getUUID();
        hddcGeochemicalsvypoint.setUuid(uuid);
        hddcGeochemicalsvypoint.setCreateUser(PlatformSessionUtils.getUserId());
        hddcGeochemicalsvypoint.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeochemicalsvypointRepository.save(hddcGeochemicalsvypoint);
        return hddcGeochemicalsvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeochemicalsvypointEntity updateHddcGeochemicalsvypoint(HddcGeochemicalsvypointEntity hddcGeochemicalsvypoint) {
        HddcGeochemicalsvypointEntity entity = hddcGeochemicalsvypointRepository.findById(hddcGeochemicalsvypoint.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcGeochemicalsvypoint);
        hddcGeochemicalsvypoint.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcGeochemicalsvypoint.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeochemicalsvypointRepository.save(hddcGeochemicalsvypoint);
        return hddcGeochemicalsvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcGeochemicalsvypoints(List<String> ids) {
        List<HddcGeochemicalsvypointEntity> hddcGeochemicalsvypointList = this.hddcGeochemicalsvypointRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcGeochemicalsvypointList) && hddcGeochemicalsvypointList.size() > 0) {
            for(HddcGeochemicalsvypointEntity hddcGeochemicalsvypoint : hddcGeochemicalsvypointList) {
                this.hddcGeochemicalsvypointRepository.delete(hddcGeochemicalsvypoint);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcGeochemicalsvypointQueryParams queryParams, HttpServletResponse response) {
        List<HddcGeochemicalsvypointEntity> yhDisasterEntities = hddcGeochemicalsvypointNativeRepository.exportYhDisasters(queryParams);
        List<HddcGeochemicalsvypointVO> list=new ArrayList<>();
        for (HddcGeochemicalsvypointEntity entity:yhDisasterEntities) {
            HddcGeochemicalsvypointVO yhDisasterVO=new HddcGeochemicalsvypointVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"地球化学探测测点-点","地球化学探测测点-点",HddcGeochemicalsvypointVO.class,"地球化学探测测点-点.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcGeochemicalsvypointVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcGeochemicalsvypointVO.class, params);
            List<HddcGeochemicalsvypointVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcGeochemicalsvypointVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcGeochemicalsvypointVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcGeochemicalsvypointVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcGeochemicalsvypointEntity yhDisasterEntity = new HddcGeochemicalsvypointEntity();
            HddcGeochemicalsvypointVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcGeochemicalsvypoint(yhDisasterEntity);
        }
    }
}
