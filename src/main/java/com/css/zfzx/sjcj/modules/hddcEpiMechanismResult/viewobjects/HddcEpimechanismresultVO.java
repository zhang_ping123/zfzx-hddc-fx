package com.css.zfzx.sjcj.modules.hddcEpiMechanismResult.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Data
public class HddcEpimechanismresultVO implements Serializable {

    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "4")
    private String commentInfo;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * T轴方位
     */
    @Excel(name = "T轴方位", orderNum = "5")
    private Integer tazimuth;
    /**
     * 震级
     */
    @Excel(name = "震级", orderNum = "6")
    private Double magnitude;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 水平最大主应力方位
     */
    @Excel(name = "水平最大主应力方位", orderNum = "7")
    private Integer shazimuth;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 震源深度 [公里]
     */
    @Excel(name = "震源深度 [公里]", orderNum = "8")
    private Double depth;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * Ⅰ节面走向
     */
    @Excel(name = "Ⅰ节面走向", orderNum = "9")
    private Integer plane1strike;
    /**
     * Ⅱ节面走向
     */
    @Excel(name = "Ⅱ节面走向", orderNum = "10")
    private Integer plane2strike;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * Ⅰ节面滑动角
     */
    @Excel(name = "Ⅰ节面滑动角", orderNum = "11")
    private Integer plane1slip;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 资料来源
     */
    @Excel(name = "资料来源", orderNum = "12")
    private String reference;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 发震时间
     */
    @Excel(name = "发震时间", orderNum = "13")
    private String occurrencetime;
    /**
     * 乡
     */
    private String town;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * B轴方位
     */
    @Excel(name = "B轴方位", orderNum = "14")
    private Integer bazimuth;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 编号
     */
    private String id;
    /**
     * Ⅱ节面滑动角
     */
    @Excel(name = "Ⅱ节面滑动角", orderNum = "15")
    private Integer plane2slip;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 震源机制解的求解方法
     */
    @Excel(name = "震源机制解的求解方法", orderNum = "16")
    private String method;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 震级单位 [Ms、Ml、Mw等]
     */
    @Excel(name = "震级单位 [Ms、Ml、Mw等]", orderNum = "17")
    private String unit;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 地名
     */
    @Excel(name = "地名", orderNum = "18")
    private String locationname;
    /**
     * 发震日期
     */
    @Excel(name = "发震日期", orderNum = "19")
    private String occurrencedate;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * T轴倾角
     */
    @Excel(name = "T轴倾角", orderNum = "20")
    private Integer tplunge;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * P轴倾角
     */
    @Excel(name = "P轴倾角", orderNum = "21")
    private Integer pplunge;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * Ⅱ节面倾角
     */
    @Excel(name = "Ⅱ节面倾角", orderNum = "22")
    private Integer plane2dip;
    /**
     * B轴倾角
     */
    @Excel(name = "B轴倾角", orderNum = "23")
    private Integer bplunge;
    /**
     * 震中纬度
     */
    @Excel(name = "震中纬度", orderNum = "24")
    private Double lat;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备注
     */
    private String remark;
    /**
     * 震中经度
     */
    @Excel(name = "震中经度", orderNum = "25")
    private Double lon;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * Ⅰ节面倾角
     */
    @Excel(name = "Ⅰ节面倾角", orderNum = "26")
    private Integer plane1dip;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 应力状态
     */
    @Excel(name = "应力状态", orderNum = "27")
    private Integer stressregime;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * P轴方位
     */
    @Excel(name = "P轴方位", orderNum = "28")
    private Integer pazimuth;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段2
     */
    private String extends2;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer stressregimeName;
    private String rowNum;
    private String errorMsg;
}