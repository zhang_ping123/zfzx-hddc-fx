package com.css.zfzx.sjcj.modules.hddcB6_GeochemicalProjectTable.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
@Data
public class HddcB6GeochemicalprojecttableVO implements Serializable {

    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 成果报告文件报告编号
     */
    @Excel(name = "成果报告文件报告编号")
    private String reportArid;
    /**
     * 市
     */
    @Excel(name = "市",orderNum = "2")
    private String city;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 探测设计原始文件编号
     */
    @Excel(name = "探测设计原始文件编号")
    private String dbArwid;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 探测原始数据原始表索引号
     */
    @Excel(name = "探测原始数据原始表索引号")
    private String dataArwid;
    /**
     * 备注
     */
    private String commentInfo;
    /**
     * 成果报告原始文件编号
     */
    @Excel(name = "成果报告原始文件编号")
    private String reportArwid;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 测点总数
     */
    @Excel(name = "测点总数")
    private Integer svypointcount;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 地质填图区编号
     */
    @Excel(name = "地质填图区编号")
    private String mainafsregionid;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 变更情况报告编号
     */
    @Excel(name = "变更情况报告编号")
    private String csArid;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备注
     */
    @Excel(name = "备注")
    private String remark;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 测线总长度 [米]
     */
    @Excel(name = "测线总长度 [米]")
    private Double svylinelength;
    /**
     * 工程编号
     */
    private String id;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）",orderNum = "3")
    private String area;
    /**
     * 探测手段
     */
    @Excel(name = "探测手段")
    private Integer svymethod;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 探测设计报告编号
     */
    @Excel(name = "探测设计报告编号")
    private String dbArid;
    /**
     * 工程名称
     */
    @Excel(name = "工程名称")
    private String name;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 省
     */
    @Excel(name = "省",orderNum = "1")
    private String province;
    /**
     * 工作区编号
     */
    @Excel(name = "工作区编号")
    private String workregionid;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 村
     */
    private String village;
    /**
     * 探测单位
     */
    @Excel(name = "探测单位")
    private String svyinstitute;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 变更情况原始编号
     */
    @Excel(name = "变更情况原始编号")
    private String csArwid;
    /**
     * 测线条数
     */
    @Excel(name = "测线条数")
    private Integer svylinecount;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 目标区编号
     */
    @Excel(name = "目标区编号")
    private String targetregionid;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer svymethodName;

    private String rowNum;
    private String errorMsg;
}