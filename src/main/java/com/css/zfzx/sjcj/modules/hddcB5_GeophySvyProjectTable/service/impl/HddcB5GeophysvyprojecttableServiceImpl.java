package com.css.zfzx.sjcj.modules.hddcB5_GeophySvyProjectTable.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyProjectTable.repository.HddcB5GeophysvyprojecttableNativeRepository;
import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyProjectTable.repository.HddcB5GeophysvyprojecttableRepository;
import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyProjectTable.repository.entity.HddcB5GeophysvyprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyProjectTable.service.HddcB5GeophysvyprojecttableService;
import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyProjectTable.viewobjects.HddcB5GeophysvyprojecttableQueryParams;
import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyProjectTable.viewobjects.HddcB5GeophysvyprojecttableVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Service
public class HddcB5GeophysvyprojecttableServiceImpl implements HddcB5GeophysvyprojecttableService {

	@Autowired
    private HddcB5GeophysvyprojecttableRepository hddcB5GeophysvyprojecttableRepository;
    @Autowired
    private HddcB5GeophysvyprojecttableNativeRepository hddcB5GeophysvyprojecttableNativeRepository;

    @Override
    public JSONObject queryHddcB5Geophysvyprojecttables(HddcB5GeophysvyprojecttableQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcB5GeophysvyprojecttableEntity> hddcB5GeophysvyprojecttablePage = this.hddcB5GeophysvyprojecttableNativeRepository.queryHddcB5Geophysvyprojecttables(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcB5GeophysvyprojecttablePage);
        return jsonObject;
    }


    @Override
    public HddcB5GeophysvyprojecttableEntity getHddcB5Geophysvyprojecttable(String id) {
        HddcB5GeophysvyprojecttableEntity hddcB5Geophysvyprojecttable = this.hddcB5GeophysvyprojecttableRepository.findById(id).orElse(null);
         return hddcB5Geophysvyprojecttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB5GeophysvyprojecttableEntity saveHddcB5Geophysvyprojecttable(HddcB5GeophysvyprojecttableEntity hddcB5Geophysvyprojecttable) {
        String uuid = UUIDGenerator.getUUID();
        hddcB5Geophysvyprojecttable.setUuid(uuid);
        hddcB5Geophysvyprojecttable.setCreateUser(PlatformSessionUtils.getUserId());
        hddcB5Geophysvyprojecttable.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB5GeophysvyprojecttableRepository.save(hddcB5Geophysvyprojecttable);
        return hddcB5Geophysvyprojecttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB5GeophysvyprojecttableEntity updateHddcB5Geophysvyprojecttable(HddcB5GeophysvyprojecttableEntity hddcB5Geophysvyprojecttable) {
        HddcB5GeophysvyprojecttableEntity entity = hddcB5GeophysvyprojecttableRepository.findById(hddcB5Geophysvyprojecttable.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcB5Geophysvyprojecttable);
        hddcB5Geophysvyprojecttable.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcB5Geophysvyprojecttable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB5GeophysvyprojecttableRepository.save(hddcB5Geophysvyprojecttable);
        return hddcB5Geophysvyprojecttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcB5Geophysvyprojecttables(List<String> ids) {
        List<HddcB5GeophysvyprojecttableEntity> hddcB5GeophysvyprojecttableList = this.hddcB5GeophysvyprojecttableRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcB5GeophysvyprojecttableList) && hddcB5GeophysvyprojecttableList.size() > 0) {
            for(HddcB5GeophysvyprojecttableEntity hddcB5Geophysvyprojecttable : hddcB5GeophysvyprojecttableList) {
                this.hddcB5GeophysvyprojecttableRepository.delete(hddcB5Geophysvyprojecttable);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcB5GeophysvyprojecttableQueryParams queryParams, HttpServletResponse response) {
        List<HddcB5GeophysvyprojecttableEntity> yhDisasterEntities = hddcB5GeophysvyprojecttableNativeRepository.exportYhDisasters(queryParams);
        List<HddcB5GeophysvyprojecttableVO> list=new ArrayList<>();
        for (HddcB5GeophysvyprojecttableEntity entity:yhDisasterEntities) {
            HddcB5GeophysvyprojecttableVO yhDisasterVO=new HddcB5GeophysvyprojecttableVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "地球物理探测工程表", "地球物理探测工程表", HddcB5GeophysvyprojecttableVO.class, "地球物理探测工程表.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcB5GeophysvyprojecttableVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcB5GeophysvyprojecttableVO.class, params);
            List<HddcB5GeophysvyprojecttableVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcB5GeophysvyprojecttableVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcB5GeophysvyprojecttableVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcB5GeophysvyprojecttableVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcB5GeophysvyprojecttableEntity yhDisasterEntity = new HddcB5GeophysvyprojecttableEntity();
            HddcB5GeophysvyprojecttableVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcB5Geophysvyprojecttable(yhDisasterEntity);
        }
    }
}
