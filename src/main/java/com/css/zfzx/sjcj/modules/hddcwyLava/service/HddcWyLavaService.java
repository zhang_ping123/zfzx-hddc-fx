package com.css.zfzx.sjcj.modules.hddcwyLava.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyLava.repository.entity.HddcWyLavaEntity;
import com.css.zfzx.sjcj.modules.hddcwyLava.viewobjects.HddcWyLavaQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-02
 */

public interface HddcWyLavaService {

    public JSONObject queryHddcWyLavas(HddcWyLavaQueryParams queryParams, int curPage, int pageSize);

    public HddcWyLavaEntity getHddcWyLava(String uuid);

    public HddcWyLavaEntity saveHddcWyLava(HddcWyLavaEntity hddcWyLava);

    public HddcWyLavaEntity updateHddcWyLava(HddcWyLavaEntity hddcWyLava);

    public void deleteHddcWyLavas(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    BigInteger queryHddcWyLava(HddcAppZztCountVo queryParams);

    List<HddcWyLavaEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);

    List<HddcWyLavaEntity> findAllByCreateUserAndIsValid(String userId, String isValid);

    /**
     * 逻辑删除-根据项目id删除数据
     * @param ids
     */
    void deleteByProjectId(List<String> ids);

    /**
     * 逻辑删除-根据任务id删除数据
     * @param ids
     */
    void deleteByTaskId(List<String> ids);

}
