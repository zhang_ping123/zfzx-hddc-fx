package com.css.zfzx.sjcj.modules.hddcTargetRegion.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcTargetRegion.repository.entity.HddcTargetregionEntity;
import com.css.zfzx.sjcj.modules.hddcTargetRegion.viewobjects.HddcTargetregionQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */

public interface HddcTargetregionService {

    public JSONObject queryHddcTargetregions(HddcTargetregionQueryParams queryParams, int curPage, int pageSize);

    public HddcTargetregionEntity getHddcTargetregion(String id);

    public HddcTargetregionEntity saveHddcTargetregion(HddcTargetregionEntity hddcTargetregion);

    public HddcTargetregionEntity updateHddcTargetregion(HddcTargetregionEntity hddcTargetregion);

    public void deleteHddcTargetregions(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcTargetregionQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
