package com.css.zfzx.sjcj.modules.hddcB2GeomorphySvyProjectTable.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB2GeomorphySvyProjectTable.repository.entity.HddcB2GeomorphysvyprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB2GeomorphySvyProjectTable.viewobjects.HddcB2GeomorphysvyprojecttableQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-07
 */

public interface HddcB2GeomorphysvyprojecttableService {

    public JSONObject queryHddcB2Geomorphysvyprojecttables(HddcB2GeomorphysvyprojecttableQueryParams queryParams, int curPage, int pageSize);

    public HddcB2GeomorphysvyprojecttableEntity getHddcB2Geomorphysvyprojecttable(String id);

    public HddcB2GeomorphysvyprojecttableEntity saveHddcB2Geomorphysvyprojecttable(HddcB2GeomorphysvyprojecttableEntity hddcB2Geomorphysvyprojecttable);

    public HddcB2GeomorphysvyprojecttableEntity updateHddcB2Geomorphysvyprojecttable(HddcB2GeomorphysvyprojecttableEntity hddcB2Geomorphysvyprojecttable);

    public void deleteHddcB2Geomorphysvyprojecttables(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB2GeomorphysvyprojecttableQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
