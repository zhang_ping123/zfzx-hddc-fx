package com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPoint.repository.entity.HddcWyGeologicalsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPoint.viewobjects.HddcWyGeologicalsvypointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */

public interface HddcWyGeologicalsvypointService {

    public JSONObject queryHddcWyGeologicalsvypoints(HddcWyGeologicalsvypointQueryParams queryParams, int curPage, int pageSize);

    public HddcWyGeologicalsvypointEntity getHddcWyGeologicalsvypoint(String uuid);

    public HddcWyGeologicalsvypointEntity saveHddcWyGeologicalsvypoint(HddcWyGeologicalsvypointEntity hddcWyGeologicalsvypoint);

    public HddcWyGeologicalsvypointEntity updateHddcWyGeologicalsvypoint(HddcWyGeologicalsvypointEntity hddcWyGeologicalsvypoint);

    public void deleteHddcWyGeologicalsvypoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    BigInteger queryHddcWyGeologicalsvypoint(HddcAppZztCountVo queryParams);

    List<HddcWyGeologicalsvypointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);

    List<HddcWyGeologicalsvypointEntity> findAllByCreateUserAndIsValid(String userId,String isValid);

    /**
     * 逻辑删除-根据项目id删除数据
     * @param ids
     */
    void deleteByProjectId(List<String> ids);

    /**
     * 逻辑删除-根据任务id删除数据
     * @param ids
     */
    void deleteByTaskId(List<String> ids);

}
