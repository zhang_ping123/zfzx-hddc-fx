package com.css.zfzx.sjcj.modules.hddcGeophySvyLine.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Data
public class HddcGeophysvylineVO implements Serializable {

    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 综合解释剖面名称
     */
    @Excel(name = "综合解释剖面名称", orderNum = "4")
    private String resultname;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 断点解释剖面图原始文件编号
     */
    @Excel(name = "断点解释剖面图原始文件编号", orderNum = "5")
    private String faultptprofileArwid;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 测线编号
     */
    private String id;
    /**
     * 综合解释剖面原始数据文件编号（sgy等）
     */
    @Excel(name = "综合解释剖面原始数据文件编号（sgy等）", orderNum = "6")
    private String expdataArwid;
    /**
     * 断点解释剖面图图像编号
     */
    @Excel(name = "断点解释剖面图图像编号", orderNum = "7")
    private String faultptprofileAiid;
    /**
     * 显示码（制图用）
     */
    @Excel(name = "显示码（制图用）", orderNum = "8")
    private Integer showcode;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 村
     */
    private String village;
    /**
     * 收集地球物理测线来源补充说明来源
     */
    @Excel(name = "收集地球物理测线来源补充说明来源", orderNum = "9")
    private String collectedlinesource;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 探测目的
     */
    @Excel(name = "探测目的", orderNum = "10")
    private String purpose;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 测线长度 [米]
     */
    @Excel(name = "测线长度 [米]", orderNum = "11")
    private Double length;
    /**
     * 起点桩号
     */
    @Excel(name = "起点桩号", orderNum = "12")
    private Integer startmilestonenum;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 综合解释剖面矢量图原始文件编号
     */
    @Excel(name = "综合解释剖面矢量图原始文件编号", orderNum = "13")
    private String resultmapArwid;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 探测方法
     */
    @Excel(name = "探测方法", orderNum = "14")
    private Integer svymethod;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 测线来源与类型
     */
    @Excel(name = "测线来源与类型", orderNum = "15")
    private Integer svylinesource;
    /**
     * 综合解释剖面栅格图原始文件编号
     */
    @Excel(name = "综合解释剖面栅格图原始文件编号", orderNum = "16")
    private String resultmapAiid;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 测线代码
     */
    @Excel(name = "测线代码", orderNum = "17")
    private String fieldid;
    /**
     * 测线所属探测工程编号
     */
    @Excel(name = "测线所属探测工程编号", orderNum = "18")
    private String projectid;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 终点桩号
     */
    @Excel(name = "终点桩号", orderNum = "19")
    private Integer endmilestonenum;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 测线备注
     */
    @Excel(name = "测线备注", orderNum = "20")
    private String commentInfo;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备注
     */
    private String remark;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 测线名称
     */
    @Excel(name = "测线名称", orderNum = "21")
    private String name;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段2
     */
    private String extends2;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer svylinesourceName;
    private Integer svymethodName;
    private String rowNum;
    private String errorMsg;
}