package com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningPt.viewobjects;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
public class HddcWyGeologicalsvyplanningptVO implements Serializable {

    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 观测点编号
     */
    private String id;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 观测方法
     */
    private String svymethods;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 省
     */
    private String province;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 地质剖面线编号
     */
    private String svylineid;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 区（县）
     */
    private String area;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 观测目的
     */
    private String purpose;
    /**
     * 市
     */
    private String city;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 经度
     */
    private Double lon;
    /**
     * 维度
     */
    private Double lat;


    private String provinceName;
    private String cityName;
    private String areaName;
}