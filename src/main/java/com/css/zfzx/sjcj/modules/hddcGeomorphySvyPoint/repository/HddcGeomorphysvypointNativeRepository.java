package com.css.zfzx.sjcj.modules.hddcGeomorphySvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcGeomorphySvyPoint.repository.entity.HddcGeomorphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyPoint.viewobjects.HddcGeomorphysvypointQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcGeomorphysvypointNativeRepository {

    Page<HddcGeomorphysvypointEntity> queryHddcGeomorphysvypoints(HddcGeomorphysvypointQueryParams queryParams, int curPage, int pageSize);

    List<HddcGeomorphysvypointEntity> exportYhDisasters(HddcGeomorphysvypointQueryParams queryParams);
}
