package com.css.zfzx.sjcj.modules.hddcCollectedDrillHole.repository;

import com.css.zfzx.sjcj.modules.hddcCollectedDrillHole.repository.entity.HddcCollecteddrillholeEntity;
import com.css.zfzx.sjcj.modules.hddcCollectedDrillHole.viewobjects.HddcCollecteddrillholeQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcCollecteddrillholeNativeRepository {

    Page<HddcCollecteddrillholeEntity> queryHddcCollecteddrillholes(HddcCollecteddrillholeQueryParams queryParams, int curPage, int pageSize);

    List<HddcCollecteddrillholeEntity> exportYhDisasters(HddcCollecteddrillholeQueryParams queryParams);
}
