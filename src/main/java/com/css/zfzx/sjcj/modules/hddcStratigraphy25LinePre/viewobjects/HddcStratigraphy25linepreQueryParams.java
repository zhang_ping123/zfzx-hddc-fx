package com.css.zfzx.sjcj.modules.hddcStratigraphy25LinePre.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Data
public class HddcStratigraphy25linepreQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

}
