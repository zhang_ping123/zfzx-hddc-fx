package com.css.zfzx.sjcj.modules.analysis.service.impl;

import com.css.zfzx.sjcj.modules.analysis.service.HsService;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyCrater.service.HddcWyCraterService;
import com.css.zfzx.sjcj.modules.hddcwyLava.service.HddcWyLavaService;
import com.css.zfzx.sjcj.modules.hddcwyVolcanicSamplePoint.service.HddcWyVolcanicsamplepointService;
import com.css.zfzx.sjcj.modules.hddcwyVolcanicSvyPoint.service.HddcWyVolcanicsvypointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO
 *
 * @author 王昊杰
 * @version 1.0
 * @date 2020/12/10  20:14
 */

@Service
public class HsServiceImpl implements HsService {
    @Autowired
    private HddcWyCraterService hddcWyCraterService;
    @Autowired
    private HddcWyLavaService hddcWyLavaService;
    @Autowired
    private HddcWyVolcanicsamplepointService hddcWyVolcanicsamplepointService;
    @Autowired
    private HddcWyVolcanicsvypointService hddcWyVolcanicsvypointService;

    public Map<String,Object> hddcHsNumData(HddcAppZztCountVo queryParams){
        //火山口-点
        BigInteger craterCount = hddcWyCraterService.queryHddcWyCrater(queryParams);
        //熔岩流-面
        //BigInteger lavaCount = hddcWyLavaService.queryHddcWyLava(queryParams);
        //火山采样点-点
        BigInteger samplePointCount = hddcWyVolcanicsamplepointService.queryHddcWyVolcanicsamplepoint(queryParams);
        //火山调查观测点-点
        BigInteger pointCount = hddcWyVolcanicsvypointService.queryHddcWyVolcanicsvypoint(queryParams);

        //X轴
        ArrayList<String> x=new ArrayList<>();
        x.add("火山口-点");
        //x.add("熔岩流-面");
        x.add("火山采样点-点");
        x.add("火山调查观测点-点");
        //y轴
        ArrayList<BigInteger> y=new ArrayList<>();
        y.add(craterCount);
        //y.add(lavaCount);
        y.add(samplePointCount);
        y.add(pointCount);
        HashMap<String,Object> map=new HashMap<>();
        map.put("x",x);
        map.put("y",y);
        return map;
    }
}

