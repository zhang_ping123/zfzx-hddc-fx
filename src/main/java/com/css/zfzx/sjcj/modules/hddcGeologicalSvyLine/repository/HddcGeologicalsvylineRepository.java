package com.css.zfzx.sjcj.modules.hddcGeologicalSvyLine.repository;

import com.css.zfzx.sjcj.modules.hddcGeologicalSvyLine.repository.entity.HddcGeologicalsvylineEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author lihelei
 * @date 2020-11-27
 */
public interface HddcGeologicalsvylineRepository extends JpaRepository<HddcGeologicalsvylineEntity, String> {
}
