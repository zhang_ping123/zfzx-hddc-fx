package com.css.zfzx.sjcj.modules.hddcGeomorphySvyLine.repository;

import com.css.zfzx.sjcj.modules.hddcGeomorphySvyLine.repository.entity.HddcGeomorphysvylineEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyLine.viewobjects.HddcGeomorphysvylineQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcGeomorphysvylineNativeRepository {

    Page<HddcGeomorphysvylineEntity> queryHddcGeomorphysvylines(HddcGeomorphysvylineQueryParams queryParams, int curPage, int pageSize);

    List<HddcGeomorphysvylineEntity> exportYhDisasters(HddcGeomorphysvylineQueryParams queryParams);
}
