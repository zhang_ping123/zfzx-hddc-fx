package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvySamplePoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvySamplePoint.repository.entity.HddcWyGeomorphysvysamplepointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvySamplePoint.viewobjects.HddcWyGeomorphysvysamplepointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.List;

/**
 * @author lihelei
 * @date 2020-12-01
 */

public interface HddcWyGeomorphysvysamplepointService {

    public JSONObject queryHddcWyGeomorphysvysamplepoints(HddcWyGeomorphysvysamplepointQueryParams queryParams, int curPage, int pageSize);

    public HddcWyGeomorphysvysamplepointEntity getHddcWyGeomorphysvysamplepoint(String uuid);

    public HddcWyGeomorphysvysamplepointEntity saveHddcWyGeomorphysvysamplepoint(HddcWyGeomorphysvysamplepointEntity hddcWyGeomorphysvysamplepoint);

    public HddcWyGeomorphysvysamplepointEntity updateHddcWyGeomorphysvysamplepoint(HddcWyGeomorphysvysamplepointEntity hddcWyGeomorphysvysamplepoint);

    public void deleteHddcWyGeomorphysvysamplepoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    BigInteger queryHddcWyGeomorphysvysamplepoint(HddcAppZztCountVo queryParams);

    List<HddcWyGeomorphysvysamplepointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);

    List<HddcWyGeomorphysvysamplepointEntity> findAllByCreateUserAndIsValid(String userId,String isValid);


    /**
     * 逻辑删除-根据项目id删除数据
     * @param ids
     */
    void deleteByProjectId(List<String> ids);

    /**
     * 逻辑删除-根据任务id删除数据
     * @param ids
     */
    void deleteByTaskId(List<String> ids);

}
