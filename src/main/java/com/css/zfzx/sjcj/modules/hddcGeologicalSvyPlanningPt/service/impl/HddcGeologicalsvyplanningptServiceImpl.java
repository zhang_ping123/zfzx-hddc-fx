package com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningPt.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningPt.repository.HddcGeologicalsvyplanningptNativeRepository;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningPt.repository.HddcGeologicalsvyplanningptRepository;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningPt.repository.entity.HddcGeologicalsvyplanningptEntity;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningPt.service.HddcGeologicalsvyplanningptService;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningPt.viewobjects.HddcGeologicalsvyplanningptQueryParams;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningPt.viewobjects.HddcGeologicalsvyplanningptVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-07
 */
@Service
public class HddcGeologicalsvyplanningptServiceImpl implements HddcGeologicalsvyplanningptService {

	@Autowired
    private HddcGeologicalsvyplanningptRepository hddcGeologicalsvyplanningptRepository;
    @Autowired
    private HddcGeologicalsvyplanningptNativeRepository hddcGeologicalsvyplanningptNativeRepository;

    @Override
    public JSONObject queryHddcGeologicalsvyplanningpts(HddcGeologicalsvyplanningptQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcGeologicalsvyplanningptEntity> hddcGeologicalsvyplanningptPage = this.hddcGeologicalsvyplanningptNativeRepository.queryHddcGeologicalsvyplanningpts(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcGeologicalsvyplanningptPage);
        return jsonObject;
    }


    @Override
    public HddcGeologicalsvyplanningptEntity getHddcGeologicalsvyplanningpt(String id) {
        HddcGeologicalsvyplanningptEntity hddcGeologicalsvyplanningpt = this.hddcGeologicalsvyplanningptRepository.findById(id).orElse(null);
         return hddcGeologicalsvyplanningpt;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeologicalsvyplanningptEntity saveHddcGeologicalsvyplanningpt(HddcGeologicalsvyplanningptEntity hddcGeologicalsvyplanningpt) {
        String uuid = UUIDGenerator.getUUID();
        hddcGeologicalsvyplanningpt.setUuid(uuid);
        hddcGeologicalsvyplanningpt.setCreateUser(PlatformSessionUtils.getUserId());
        hddcGeologicalsvyplanningpt.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeologicalsvyplanningptRepository.save(hddcGeologicalsvyplanningpt);
        return hddcGeologicalsvyplanningpt;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeologicalsvyplanningptEntity updateHddcGeologicalsvyplanningpt(HddcGeologicalsvyplanningptEntity hddcGeologicalsvyplanningpt) {
        HddcGeologicalsvyplanningptEntity entity = hddcGeologicalsvyplanningptRepository.findById(hddcGeologicalsvyplanningpt.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcGeologicalsvyplanningpt);
        hddcGeologicalsvyplanningpt.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcGeologicalsvyplanningpt.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeologicalsvyplanningptRepository.save(hddcGeologicalsvyplanningpt);
        return hddcGeologicalsvyplanningpt;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcGeologicalsvyplanningpts(List<String> ids) {
        List<HddcGeologicalsvyplanningptEntity> hddcGeologicalsvyplanningptList = this.hddcGeologicalsvyplanningptRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcGeologicalsvyplanningptList) && hddcGeologicalsvyplanningptList.size() > 0) {
            for(HddcGeologicalsvyplanningptEntity hddcGeologicalsvyplanningpt : hddcGeologicalsvyplanningptList) {
                this.hddcGeologicalsvyplanningptRepository.delete(hddcGeologicalsvyplanningpt);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcGeologicalsvyplanningptQueryParams queryParams, HttpServletResponse response) {
        List<HddcGeologicalsvyplanningptEntity> yhDisasterEntities = hddcGeologicalsvyplanningptNativeRepository.exportYhDisasters(queryParams);
        List<HddcGeologicalsvyplanningptVO> list=new ArrayList<>();
        for (HddcGeologicalsvyplanningptEntity entity:yhDisasterEntities) {
            HddcGeologicalsvyplanningptVO yhDisasterVO=new HddcGeologicalsvyplanningptVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"地质调查规划点","地质调查规划点",HddcGeologicalsvyplanningptVO.class,"地质调查规划点-点.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcGeologicalsvyplanningptVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcGeologicalsvyplanningptVO.class, params);
            List<HddcGeologicalsvyplanningptVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcGeologicalsvyplanningptVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcGeologicalsvyplanningptVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcGeologicalsvyplanningptVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcGeologicalsvyplanningptEntity yhDisasterEntity = new HddcGeologicalsvyplanningptEntity();
            HddcGeologicalsvyplanningptVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcGeologicalsvyplanningpt(yhDisasterEntity);
        }
    }

}
