package com.css.zfzx.sjcj.modules.hddccjfilemanage.repository.impl;

import com.css.bpm.platform.utils.PlatformObjectUtils;
import com.css.zfzx.sjcj.modules.hddccjfilemanage.repository.HddcFileManageNativeRepository;
import com.css.zfzx.sjcj.modules.hddccjfilemanage.repository.entity.HddcFileManageEntity;
import com.css.zfzx.sjcj.modules.hddccjfilemanage.viewobjects.HddcFileManageQueryParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;
/**
 * @author zyb
 * @date 2020-12-10
 */
@Repository
public class HddcFileManageNativeRepositoryImpl implements HddcFileManageNativeRepository {
    @PersistenceContext
    private EntityManager em;

    private static final Logger log = LoggerFactory.getLogger(HddcFileManageNativeRepositoryImpl.class);


    @Override
    public Page<HddcFileManageEntity> queryHddcFileManages(HddcFileManageQueryParams queryParams, int curPage, int pageSize) {
        if(PlatformObjectUtils.isEmpty(queryParams.getIsValid())) {
            queryParams.setIsValid("1");
        }
        StringBuilder sql = new StringBuilder("select * from hddc_file_manage ");
        StringBuilder whereSql = new StringBuilder(" where 1=1 ");
        if(!PlatformObjectUtils.isEmpty(queryParams.getFileId())) {
            whereSql.append(" and file_id = :fileId");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getFileName())) {
            whereSql.append(" and file_name = :fileName");
        }
        Query query = this.em.createNativeQuery(sql.append(whereSql).toString(), HddcFileManageEntity.class);
        StringBuilder countSql = new StringBuilder("select count(*) from hddc_file_manage ");
        Query countQuery = this.em.createNativeQuery(countSql.append(whereSql).toString());
        if(!PlatformObjectUtils.isEmpty(queryParams.getFileId())) {
            query.setParameter("fileId", queryParams.getFileId());
            countQuery.setParameter("fileId", queryParams.getFileId());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getFileName())) {
            query.setParameter("fileName", queryParams.getFileName());
            countQuery.setParameter("fileName", queryParams.getFileName());
        }
        Pageable pageable = PageRequest.of(curPage - 1, pageSize);
        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());
        List<HddcFileManageEntity> list = query.getResultList();
        BigInteger count = (BigInteger) countQuery.getSingleResult();
        Page<HddcFileManageEntity> hddcFileManagePage = new PageImpl<>(list, pageable, count.longValue());
        return hddcFileManagePage;
    }
}
