package com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyLine.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-12-01
 */
@Data
public class HddcWyGeologicalsvylineVO implements Serializable {

    /**
     * 备注
     */
    private String commentInfo;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 项目名称
     */
    @Excel(name="项目名称",orderNum = "7")
    private String projectName;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 野外编号
     */
    @Excel(name="野外编号",orderNum = "9")
    private String fieldid;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 观测目的
     */
    @Excel(name="观测目的",orderNum = "10")
    private String purpose;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 测线名称
     */
    @Excel(name="测线名称",orderNum = "11")
    private String svylinename;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 编号
     */
    @Excel(name="编号",orderNum = "1")
    private String id;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备注
     */
    @Excel(name="备注",orderNum = "12")
    private String remark;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 乡
     */
    @Excel(name="详细地址",orderNum = "5")
    private String town;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 任务名称
     */
    @Excel(name="任务名称",orderNum = "8")
    private String taskName;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段5
     */
    @Excel(name="区域",orderNum = "6")
    private String extends5;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 市
     */
    @Excel(name="市",orderNum = "3")
    private String city;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 省
     */
    @Excel(name="省",orderNum = "2")
    private String province;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 村
     */
    private String village;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 区（县）
     */
    @Excel(name="区（县）",orderNum = "4")
    private String area;
    /**
     * 经度
     */
    private Double lon;
    /**
     * 维度
     */
    private Double lat;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String rowNum;
    private String errorMsg;
}