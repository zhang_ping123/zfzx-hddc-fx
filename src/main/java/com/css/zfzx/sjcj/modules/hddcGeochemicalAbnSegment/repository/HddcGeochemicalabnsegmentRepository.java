package com.css.zfzx.sjcj.modules.hddcGeochemicalAbnSegment.repository;

import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnSegment.repository.entity.HddcGeochemicalabnsegmentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
public interface HddcGeochemicalabnsegmentRepository extends JpaRepository<HddcGeochemicalabnsegmentEntity, String> {
}
