package com.css.zfzx.sjcj.modules.app.repository;

import com.css.bpm.platform.org.dept.repository.entity.DeptEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BaseDeptRepository extends JpaRepository<DeptEntity,String> {

    DeptEntity findByDivisionIdAndExtend1(String divisionId, String extend1);


}
