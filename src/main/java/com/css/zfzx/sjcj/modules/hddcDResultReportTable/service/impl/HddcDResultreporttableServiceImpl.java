package com.css.zfzx.sjcj.modules.hddcDResultReportTable.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.CheckObjFields;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcDResultReportTable.repository.HddcDResultreporttableNativeRepository;
import com.css.zfzx.sjcj.modules.hddcDResultReportTable.repository.HddcDResultreporttableRepository;
import com.css.zfzx.sjcj.modules.hddcDResultReportTable.repository.entity.HddcDResultreporttableEntity;
import com.css.zfzx.sjcj.modules.hddcDResultReportTable.service.HddcDResultreporttableService;
import com.css.zfzx.sjcj.modules.hddcDResultReportTable.viewobjects.HddcDResultreporttableQueryParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-19
 */
@Service
public class HddcDResultreporttableServiceImpl implements HddcDResultreporttableService {

    @Autowired
    private HddcDResultreporttableRepository hddcDResultreporttableRepository;
    @Autowired
    private HddcDResultreporttableNativeRepository hddcDResultreporttableNativeRepository;

    @Override
    public JSONObject queryHddcDResultreporttables(HddcDResultreporttableQueryParams queryParams, int curPage, int pageSize,String sort,String order) {
        if (!PlatformObjectUtils.isEmpty(sort) && sort.length() > 0){
            sort= CheckObjFields.propertyChange(sort);
        }
        Page<HddcDResultreporttableEntity> hddcDResultreporttablePage = this.hddcDResultreporttableNativeRepository.queryHddcDResultreporttables(queryParams, curPage, pageSize,sort,order);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcDResultreporttablePage);
        return jsonObject;
    }


    @Override
    public HddcDResultreporttableEntity getHddcDResultreporttable(String id) {
        HddcDResultreporttableEntity hddcDResultreporttable = this.hddcDResultreporttableRepository.findById(id).orElse(null);
        return hddcDResultreporttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcDResultreporttableEntity saveHddcDResultreporttable(HddcDResultreporttableEntity hddcDResultreporttable) {
        String uuid = UUIDGenerator.getUUID();
        hddcDResultreporttable.setUuid(uuid);
        hddcDResultreporttable.setCreateUser(PlatformSessionUtils.getUserId());
        hddcDResultreporttable.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        hddcDResultreporttable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        hddcDResultreporttable.setIsValid("1");
        this.hddcDResultreporttableRepository.save(hddcDResultreporttable);
        return hddcDResultreporttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcDResultreporttableEntity updateHddcDResultreporttable(HddcDResultreporttableEntity hddcDResultreporttable) {
        HddcDResultreporttableEntity entity = hddcDResultreporttableRepository.findById(hddcDResultreporttable.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcDResultreporttable);
        hddcDResultreporttable.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcDResultreporttable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcDResultreporttableRepository.save(hddcDResultreporttable);
        return hddcDResultreporttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcDResultreporttables(List<String> ids) {
        List<HddcDResultreporttableEntity> hddcDResultreporttableList = this.hddcDResultreporttableRepository.findAllById(ids);
        if (!PlatformObjectUtils.isEmpty(hddcDResultreporttableList) && hddcDResultreporttableList.size() > 0) {
            for (HddcDResultreporttableEntity hddcDResultreporttable : hddcDResultreporttableList) {
//                this.hddcDResultreporttableRepository.delete(hddcDResultreporttable);
                hddcDResultreporttable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcDResultreporttable.setIsValid("0");
                hddcDResultreporttable.setUpdateTime(new Date());
                this.hddcDResultreporttableRepository.save(hddcDResultreporttable);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

}
