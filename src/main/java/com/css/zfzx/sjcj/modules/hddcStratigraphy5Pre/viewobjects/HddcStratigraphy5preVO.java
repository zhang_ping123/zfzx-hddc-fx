package com.css.zfzx.sjcj.modules.hddcStratigraphy5Pre.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Data
public class HddcStratigraphy5preVO implements Serializable {

    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 地层描述
     */
    @Excel(name = "地层描述", orderNum = "4")
    private String description;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 地层编号
     */
    private String id;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 符号下标
     */
    @Excel(name = "符号下标", orderNum = "5")
    private String symbollow;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 合并地层名称
     */
    @Excel(name = "合并地层名称", orderNum = "6")
    private String unionname;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 地层名称
     */
    @Excel(name = "地层名称", orderNum = "7")
    private String stratigraphyname;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "8")
    private String commentInfo;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 符号上标
     */
    @Excel(name = "符号上标", orderNum = "9")
    private String symbolup;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 地层厚度 [米]
     */
    @Excel(name = "地层厚度 [米]", orderNum = "10")
    private String thickness;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 沉积相（符号）
     */
    @Excel(name = "沉积相（符号）", orderNum = "11")
    private String sedimentaryfacies;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 地层年代
     */
    @Excel(name = "地层年代", orderNum = "12")
    private Integer symbol;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备注
     */
    private String remark;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 地层符号基础位
     */
    @Excel(name = "地层符号基础位", orderNum = "13")
    private String symbolmain;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段23
     */
    private String extends23;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String projectNameName;
    private Integer symbolName;
    private String rowNum;
    private String errorMsg;
}