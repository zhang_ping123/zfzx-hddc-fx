package com.css.zfzx.sjcj.modules.hddcGeochemicalSvyLine.repository.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Data
@Entity
@Table(name="hddc_geochemicalsvyline")
public class HddcGeochemicalsvylineEntity implements Serializable {

    /**
     * 显示码
     */
    @Column(name="showcode")
    private Integer showcode;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 分析结果图图像文件编号
     */
    @Column(name="rm_aiid")
    private String rmAiid;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 异常点总数
     */
    @Column(name="abnpointnum")
    private Integer abnpointnum;
    /**
     * 测线名称
     */
    @Column(name="name")
    private String name;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 项目ID
     */
    @Column(name="project_num")
    private String projectnum;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 测线起点经度
     */
    @Column(name="startlongitude")
    private Double startlongitude;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 探测方法
     */
    @Column(name="svymethod")
    private Integer svymethod;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 均值
     */
    @Column(name="meanvalue")
    private Double meanvalue;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 备注
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 测线终点经度
     */
    @Column(name="endlongitutde")
    private Double endlongitutde;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 滑动
     */
    @Column(name="slippagevalue")
    private Double slippagevalue;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 测点数
     */
    @Column(name="svypointnum")
    private Integer svypointnum;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 分析结果图原始表索引号
     */
    @Column(name="rm_arwid")
    private String rmArwid;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 内插点
     */
    @Column(name="interpolatenum")
    private Integer interpolatenum;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 测线起点纬度
     */
    @Column(name="startlatitude")
    private Double startlatitude;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 测线所属工程编号
     */
    @Column(name="projectid")
    private String projectid;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 测线长度 [米]
     */
    @Column(name="length")
    private Double length;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 测线编号
     */
    @Column(name="id")
    private String id;
    /**
     * 编号
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 测线终点纬度
     */
    @Column(name="endlatitude")
    private Double endlatitude;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 异常下限值
     */
    @Column(name="abnormalbottomvalue")
    private Double abnormalbottomvalue;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;

}

