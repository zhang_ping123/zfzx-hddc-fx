package com.css.zfzx.sjcj.modules.hddcB7_VolSvyPtObservationObject.repository;

import com.css.zfzx.sjcj.modules.hddcB7_VolSvyPtObservationObject.repository.entity.HddcB7VolsvyptobservationobjectEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
public interface HddcB7VolsvyptobservationobjectRepository extends JpaRepository<HddcB7VolsvyptobservationobjectEntity, String> {
}
