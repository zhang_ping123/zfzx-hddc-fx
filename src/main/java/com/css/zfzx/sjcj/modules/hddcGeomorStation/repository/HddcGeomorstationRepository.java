package com.css.zfzx.sjcj.modules.hddcGeomorStation.repository;

import com.css.zfzx.sjcj.modules.hddcGeomorStation.repository.entity.HddcGeomorstationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author whj
 * @date 2020-11-30
 */
public interface HddcGeomorstationRepository extends JpaRepository<HddcGeomorstationEntity, String> {
}
