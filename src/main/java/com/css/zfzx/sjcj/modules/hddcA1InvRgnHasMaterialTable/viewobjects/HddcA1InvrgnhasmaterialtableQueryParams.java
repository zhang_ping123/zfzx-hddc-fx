package com.css.zfzx.sjcj.modules.hddcA1InvRgnHasMaterialTable.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
public class HddcA1InvrgnhasmaterialtableQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

}
