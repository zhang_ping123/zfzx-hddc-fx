package com.css.zfzx.sjcj.modules.hddcB4_SampleDataTable.repository;

import com.css.zfzx.sjcj.modules.hddcB4_SampleDataTable.repository.entity.HddcB4SampledatatableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcB4SampledatatableRepository extends JpaRepository<HddcB4SampledatatableEntity, String> {
}
