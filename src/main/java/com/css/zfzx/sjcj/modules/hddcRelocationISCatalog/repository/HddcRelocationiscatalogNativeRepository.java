package com.css.zfzx.sjcj.modules.hddcRelocationISCatalog.repository;

import com.css.zfzx.sjcj.modules.hddcRelocationISCatalog.repository.entity.HddcRelocationiscatalogEntity;
import com.css.zfzx.sjcj.modules.hddcRelocationISCatalog.viewobjects.HddcRelocationiscatalogQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */
public interface HddcRelocationiscatalogNativeRepository {

    Page<HddcRelocationiscatalogEntity> queryHddcRelocationiscatalogs(HddcRelocationiscatalogQueryParams queryParams, int curPage, int pageSize);

    List<HddcRelocationiscatalogEntity> exportYhDisasters(HddcRelocationiscatalogQueryParams queryParams);
}
