package com.css.zfzx.sjcj.modules.hddcwySamplePoint.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-12-01
 */
@Data
public class HddcWySamplepointVO implements Serializable {

    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 采样情况
     */
    @Excel(name = "采样情况", orderNum = "10")
    private String samplevariety;
    /**
     * 是否联合采样出单一测试结果
     */
    @Excel(name = "是否联合采样出单一测试结果", orderNum = "12")
    private Integer isunitedresult;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 数据来源
     */
    @Excel(name = "数据来源", orderNum = "6")
    private String samplesource;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 采样点符号
     */
    @Excel(name = "采样点符号", orderNum = "17")
    private Integer symbolinfo;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "15")
    private String commentInfo;
    /**
     * 项目名称
     */
    @Excel(name="项目名称",orderNum = "8")
    private String projectName;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 野外编号
     */
    @Excel(name = "野外编号", orderNum = "13")
    private String fieldid;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "3")
    private String city;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 采样数据来源点编号
     */
    @Excel(name = "采样数据来源点编号", orderNum = "16")
    private String samplesourceid;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 乡
     */
    @Excel(name="详细地址",orderNum = "5")
    private String town;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "4")
    private String area;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "2")
    private String province;
    /**
     * 村
     */
    private String village;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 任务名称
     */
    @Excel(name="任务名称",orderNum = "9")
    private String taskName;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 采样点编号
     */
    @Excel(name = "采样点编号",orderNum = "1")
    private String id;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 标注名称
     */
    @Excel(name = "标注名称", orderNum = "11")
    private String labelinfo;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备注
     */
    private String remark;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 经度
     */
    @Excel(name="经度",orderNum = "6")
    private Double lon;
    /**
     * 维度
     */
    @Excel(name="纬度",orderNum = "7")
    private Double lat;

    private String provinceName;
    private String cityName;
    private String areaName;

    private String rowNum;
    private String errorMsg;
}