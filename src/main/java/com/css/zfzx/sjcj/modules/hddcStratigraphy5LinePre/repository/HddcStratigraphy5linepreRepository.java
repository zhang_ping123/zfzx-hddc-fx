package com.css.zfzx.sjcj.modules.hddcStratigraphy5LinePre.repository;

import com.css.zfzx.sjcj.modules.hddcStratigraphy5LinePre.repository.entity.HddcStratigraphy5linepreEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcStratigraphy5linepreRepository extends JpaRepository<HddcStratigraphy5linepreEntity, String> {
}
