package com.css.zfzx.sjcj.modules.hddcRock1Pre.repository;

import com.css.zfzx.sjcj.modules.hddcRock1Pre.repository.entity.HddcRock1preEntity;
import com.css.zfzx.sjcj.modules.hddcRock1Pre.viewobjects.HddcRock1preQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcRock1preNativeRepository {

    Page<HddcRock1preEntity> queryHddcRock1pres(HddcRock1preQueryParams queryParams, int curPage, int pageSize);

    List<HddcRock1preEntity> exportYhDisasters(HddcRock1preQueryParams queryParams);
}
