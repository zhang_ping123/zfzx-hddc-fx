package com.css.zfzx.sjcj.modules.hddcwyCrater.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyCrater.repository.entity.HddcWyCraterEntity;
import com.css.zfzx.sjcj.modules.hddcwyCrater.viewobjects.HddcWyCraterQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-02
 */
public interface HddcWyCraterNativeRepository {

    Page<HddcWyCraterEntity> queryHddcWyCraters(HddcWyCraterQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyCrater(HddcAppZztCountVo queryParams);

    List<HddcWyCraterEntity> exportCrater(HddcAppZztCountVo queryParams);
}
