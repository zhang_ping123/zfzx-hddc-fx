package com.css.zfzx.sjcj.modules.hddcB4_SampleProjectTable.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB4_SampleProjectTable.repository.entity.HddcB4SampleprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB4_SampleProjectTable.viewobjects.HddcB4SampleprojecttableQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */

public interface HddcB4SampleprojecttableService {

    public JSONObject queryHddcB4Sampleprojecttables(HddcB4SampleprojecttableQueryParams queryParams, int curPage, int pageSize);

    public HddcB4SampleprojecttableEntity getHddcB4Sampleprojecttable(String id);

    public HddcB4SampleprojecttableEntity saveHddcB4Sampleprojecttable(HddcB4SampleprojecttableEntity hddcB4Sampleprojecttable);

    public HddcB4SampleprojecttableEntity updateHddcB4Sampleprojecttable(HddcB4SampleprojecttableEntity hddcB4Sampleprojecttable);

    public void deleteHddcB4Sampleprojecttables(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB4SampleprojecttableQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
