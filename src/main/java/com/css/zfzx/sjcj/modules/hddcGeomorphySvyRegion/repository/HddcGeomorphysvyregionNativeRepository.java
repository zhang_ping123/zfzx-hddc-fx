package com.css.zfzx.sjcj.modules.hddcGeomorphySvyRegion.repository;

import com.css.zfzx.sjcj.modules.hddcGeomorphySvyRegion.repository.entity.HddcGeomorphysvyregionEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyRegion.viewobjects.HddcGeomorphysvyregionQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcGeomorphysvyregionNativeRepository {

    Page<HddcGeomorphysvyregionEntity> queryHddcGeomorphysvyregions(HddcGeomorphysvyregionQueryParams queryParams, int curPage, int pageSize);

    List<HddcGeomorphysvyregionEntity> exportYhDisasters(HddcGeomorphysvyregionQueryParams queryParams);
}
