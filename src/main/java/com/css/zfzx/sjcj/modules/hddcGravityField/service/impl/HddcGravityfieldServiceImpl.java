package com.css.zfzx.sjcj.modules.hddcGravityField.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcGravityField.repository.HddcGravityfieldNativeRepository;
import com.css.zfzx.sjcj.modules.hddcGravityField.repository.HddcGravityfieldRepository;
import com.css.zfzx.sjcj.modules.hddcGravityField.repository.entity.HddcGravityfieldEntity;
import com.css.zfzx.sjcj.modules.hddcGravityField.service.HddcGravityfieldService;
import com.css.zfzx.sjcj.modules.hddcGravityField.viewobjects.HddcGravityfieldQueryParams;
import com.css.zfzx.sjcj.modules.hddcGravityField.viewobjects.HddcGravityfieldVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-26
 */
@Service
public class HddcGravityfieldServiceImpl implements HddcGravityfieldService {

	@Autowired
    private HddcGravityfieldRepository hddcGravityfieldRepository;
    @Autowired
    private HddcGravityfieldNativeRepository hddcGravityfieldNativeRepository;

    @Override
    public JSONObject queryHddcGravityfields(HddcGravityfieldQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcGravityfieldEntity> hddcGravityfieldPage = this.hddcGravityfieldNativeRepository.queryHddcGravityfields(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcGravityfieldPage);
        return jsonObject;
    }


    @Override
    public HddcGravityfieldEntity getHddcGravityfield(String id) {
        HddcGravityfieldEntity hddcGravityfield = this.hddcGravityfieldRepository.findById(id).orElse(null);
         return hddcGravityfield;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGravityfieldEntity saveHddcGravityfield(HddcGravityfieldEntity hddcGravityfield) {
        String uuid = UUIDGenerator.getUUID();
        hddcGravityfield.setUuid(uuid);
        hddcGravityfield.setCreateUser(PlatformSessionUtils.getUserId());
        hddcGravityfield.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGravityfieldRepository.save(hddcGravityfield);
        return hddcGravityfield;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGravityfieldEntity updateHddcGravityfield(HddcGravityfieldEntity hddcGravityfield) {
        HddcGravityfieldEntity entity = hddcGravityfieldRepository.findById(hddcGravityfield.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcGravityfield);
        hddcGravityfield.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcGravityfield.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGravityfieldRepository.save(hddcGravityfield);
        return hddcGravityfield;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcGravityfields(List<String> ids) {
        List<HddcGravityfieldEntity> hddcGravityfieldList = this.hddcGravityfieldRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcGravityfieldList) && hddcGravityfieldList.size() > 0) {
            for(HddcGravityfieldEntity hddcGravityfield : hddcGravityfieldList) {
                this.hddcGravityfieldRepository.delete(hddcGravityfield);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcGravityfieldQueryParams queryParams, HttpServletResponse response) {
        List<HddcGravityfieldEntity> yhDisasterEntities = hddcGravityfieldNativeRepository.exportYhDisasters(queryParams);
        List<HddcGravityfieldVO> list=new ArrayList<>();
        for (HddcGravityfieldEntity entity:yhDisasterEntities) {
            HddcGravityfieldVO yhDisasterVO=new HddcGravityfieldVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"重力场-线","重力场-线",HddcGravityfieldVO.class,"重力场-线.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcGravityfieldVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcGravityfieldVO.class, params);
            List<HddcGravityfieldVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcGravityfieldVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcGravityfieldVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcGravityfieldVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcGravityfieldEntity yhDisasterEntity = new HddcGravityfieldEntity();
            HddcGravityfieldVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcGravityfield(yhDisasterEntity);
        }
    }

}
