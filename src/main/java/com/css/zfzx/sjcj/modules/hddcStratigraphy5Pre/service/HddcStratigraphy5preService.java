package com.css.zfzx.sjcj.modules.hddcStratigraphy5Pre.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcStratigraphy5Pre.repository.entity.HddcStratigraphy5preEntity;
import com.css.zfzx.sjcj.modules.hddcStratigraphy5Pre.viewobjects.HddcStratigraphy5preQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */

public interface HddcStratigraphy5preService {

    public JSONObject queryHddcStratigraphy5pres(HddcStratigraphy5preQueryParams queryParams, int curPage, int pageSize);

    public HddcStratigraphy5preEntity getHddcStratigraphy5pre(String id);

    public HddcStratigraphy5preEntity saveHddcStratigraphy5pre(HddcStratigraphy5preEntity hddcStratigraphy5pre);

    public HddcStratigraphy5preEntity updateHddcStratigraphy5pre(HddcStratigraphy5preEntity hddcStratigraphy5pre);

    public void deleteHddcStratigraphy5pres(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcStratigraphy5preQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
