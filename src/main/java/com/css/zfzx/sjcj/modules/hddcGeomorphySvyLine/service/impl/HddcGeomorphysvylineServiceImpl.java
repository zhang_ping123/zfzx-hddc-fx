package com.css.zfzx.sjcj.modules.hddcGeomorphySvyLine.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyLine.repository.HddcGeomorphysvylineNativeRepository;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyLine.repository.HddcGeomorphysvylineRepository;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyLine.repository.entity.HddcGeomorphysvylineEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyLine.service.HddcGeomorphysvylineService;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyLine.viewobjects.HddcGeomorphysvylineQueryParams;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyLine.viewobjects.HddcGeomorphysvylineVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Service
public class HddcGeomorphysvylineServiceImpl implements HddcGeomorphysvylineService {

	@Autowired
    private HddcGeomorphysvylineRepository hddcGeomorphysvylineRepository;
    @Autowired
    private HddcGeomorphysvylineNativeRepository hddcGeomorphysvylineNativeRepository;

    @Override
    public JSONObject queryHddcGeomorphysvylines(HddcGeomorphysvylineQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcGeomorphysvylineEntity> hddcGeomorphysvylinePage = this.hddcGeomorphysvylineNativeRepository.queryHddcGeomorphysvylines(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcGeomorphysvylinePage);
        return jsonObject;
    }


    @Override
    public HddcGeomorphysvylineEntity getHddcGeomorphysvyline(String id) {
        HddcGeomorphysvylineEntity hddcGeomorphysvyline = this.hddcGeomorphysvylineRepository.findById(id).orElse(null);
         return hddcGeomorphysvyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeomorphysvylineEntity saveHddcGeomorphysvyline(HddcGeomorphysvylineEntity hddcGeomorphysvyline) {
        String uuid = UUIDGenerator.getUUID();
        hddcGeomorphysvyline.setUuid(uuid);
        hddcGeomorphysvyline.setCreateUser(PlatformSessionUtils.getUserId());
        hddcGeomorphysvyline.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeomorphysvylineRepository.save(hddcGeomorphysvyline);
        return hddcGeomorphysvyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeomorphysvylineEntity updateHddcGeomorphysvyline(HddcGeomorphysvylineEntity hddcGeomorphysvyline) {
        HddcGeomorphysvylineEntity entity = hddcGeomorphysvylineRepository.findById(hddcGeomorphysvyline.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcGeomorphysvyline);
        hddcGeomorphysvyline.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcGeomorphysvyline.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeomorphysvylineRepository.save(hddcGeomorphysvyline);
        return hddcGeomorphysvyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcGeomorphysvylines(List<String> ids) {
        List<HddcGeomorphysvylineEntity> hddcGeomorphysvylineList = this.hddcGeomorphysvylineRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcGeomorphysvylineList) && hddcGeomorphysvylineList.size() > 0) {
            for(HddcGeomorphysvylineEntity hddcGeomorphysvyline : hddcGeomorphysvylineList) {
                this.hddcGeomorphysvylineRepository.delete(hddcGeomorphysvyline);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcGeomorphysvylineQueryParams queryParams, HttpServletResponse response) {
        List<HddcGeomorphysvylineEntity> yhDisasterEntities = hddcGeomorphysvylineNativeRepository.exportYhDisasters(queryParams);
        List<HddcGeomorphysvylineVO> list=new ArrayList<>();
        for (HddcGeomorphysvylineEntity entity:yhDisasterEntities) {
            HddcGeomorphysvylineVO yhDisasterVO=new HddcGeomorphysvylineVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "微地貌测量线-线", "微地貌测量线-线", HddcGeomorphysvylineVO.class, "微地貌测量线-线.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcGeomorphysvylineVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcGeomorphysvylineVO.class, params);
            List<HddcGeomorphysvylineVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcGeomorphysvylineVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcGeomorphysvylineVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcGeomorphysvylineVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcGeomorphysvylineEntity yhDisasterEntity = new HddcGeomorphysvylineEntity();
            HddcGeomorphysvylineVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcGeomorphysvyline(yhDisasterEntity);
        }
    }

}
