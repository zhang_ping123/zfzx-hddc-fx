package com.css.zfzx.sjcj.modules.hddcTrench.repository;

import com.css.zfzx.sjcj.modules.hddcTrench.repository.entity.HddcTrenchEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcTrenchRepository extends JpaRepository<HddcTrenchEntity, String> {
}
