package com.css.zfzx.sjcj.modules.hddccjproject.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.bpm.platform.utils.PlatformPageUtils;
import com.css.zfzx.sjcj.modules.hddccjproject.repository.entity.HddcCjProjectEntity;
import com.css.zfzx.sjcj.modules.hddccjproject.service.HddcCjProjectService;
import com.css.zfzx.sjcj.modules.hddccjproject.viewobjects.HddcCjProjectQueryParams;
import com.css.zfzx.sjcj.modules.hddccjproject.viewobjects.HddcCjProjectVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author zhangping
 * @date 2020-11-26
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcCjProjects")
public class HddcCjProjectController {
    @Autowired
    private HddcCjProjectService hddcCjProjectService;

    @GetMapping("/queryHddcCjProjects")
    public RestResponse queryHddcCjProjects(HttpServletRequest request, HddcCjProjectQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcCjProjectService.queryHddcCjProjects(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/queryHddcCjProjectsByPersonId")
    public RestResponse queryHddcCjProjectsByPersonId(String personId,int curPage,int pageSize) {
        RestResponse response = null;
        try{
            //int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            //int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcCjProjectService.queryHddcCjProjectsByPersonId(personId,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/queryAllHddcCjProjects")
    public RestResponse queryAllHddcCjProjects() {
        RestResponse response = null;
        try{
            //int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            //int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            List<HddcCjProjectEntity> list = hddcCjProjectService.findAll();
            response = RestResponse.succeed(list);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcCjProject(@PathVariable String id) {
        RestResponse response = null;
        try{
            HddcCjProjectVO hddcCjProject = hddcCjProjectService.getHddcCjProject(id);
            response = RestResponse.succeed(hddcCjProject);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcCjProject(@RequestBody HddcCjProjectEntity hddcCjProject) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcCjProjectService.saveHddcCjProject(hddcCjProject);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcCjProject(@RequestBody HddcCjProjectEntity hddcCjProject)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcCjProjectService.updateHddcCjProject(hddcCjProject);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcCjProjects(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcCjProjectService.deleteHddcCjProjects(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcCjProjectService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }

    /**
     * 根据人员ID获取对应的项目集合
     * @param personid
     * @return
     */
    @GetMapping("/findHddcCjProjectEntityByPersonId/{personid}")
    public RestResponse findHddcCjProjectEntityByPersonId(@PathVariable String personid){
        RestResponse restResponse=null;
        try{
            List<HddcCjProjectEntity> list = hddcCjProjectService.findHddcCjProjectEntityByPersonId(personid);
            restResponse = RestResponse.succeed(list);
        }catch (Exception e){
            String messsageerror="查询失败";
            restResponse=RestResponse.fail(messsageerror);
        }
        return restResponse;
    }

}