package com.css.zfzx.sjcj.modules.hddcwyGeomorStation.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-12-01
 */
@Data
public class HddcWyGeomorstationQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

    private String startTime;
    private String endTime;

    private String userId;
    private String taskId;
    private String projectId;
    private String isVaild;
}
