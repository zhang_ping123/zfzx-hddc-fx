package com.css.zfzx.sjcj.modules.hddcGeochemicalSvyLine.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyLine.repository.entity.HddcGeochemicalsvylineEntity;
import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyLine.viewobjects.HddcGeochemicalsvylineQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */

public interface HddcGeochemicalsvylineService {

    public JSONObject queryHddcGeochemicalsvylines(HddcGeochemicalsvylineQueryParams queryParams, int curPage, int pageSize);

    public HddcGeochemicalsvylineEntity getHddcGeochemicalsvyline(String id);

    public HddcGeochemicalsvylineEntity saveHddcGeochemicalsvyline(HddcGeochemicalsvylineEntity hddcGeochemicalsvyline);

    public HddcGeochemicalsvylineEntity updateHddcGeochemicalsvyline(HddcGeochemicalsvylineEntity hddcGeochemicalsvyline);

    public void deleteHddcGeochemicalsvylines(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGeochemicalsvylineQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
