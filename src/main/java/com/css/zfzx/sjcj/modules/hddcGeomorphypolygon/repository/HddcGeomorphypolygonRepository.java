package com.css.zfzx.sjcj.modules.hddcGeomorphypolygon.repository;

import com.css.zfzx.sjcj.modules.hddcGeomorphypolygon.repository.entity.HddcGeomorphypolygonEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-12-09
 */
public interface HddcGeomorphypolygonRepository extends JpaRepository<HddcGeomorphypolygonEntity, String> {
}
