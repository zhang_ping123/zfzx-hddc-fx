package com.css.zfzx.sjcj.modules.hddcGeomorphypolygon.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcGeomorphypolygon.repository.entity.HddcGeomorphypolygonEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphypolygon.viewobjects.HddcGeomorphypolygonQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-09
 */

public interface HddcGeomorphypolygonService {

    public JSONObject queryHddcGeomorphypolygons(HddcGeomorphypolygonQueryParams queryParams, int curPage, int pageSize);

    public HddcGeomorphypolygonEntity getHddcGeomorphypolygon(String id);

    public HddcGeomorphypolygonEntity saveHddcGeomorphypolygon(HddcGeomorphypolygonEntity hddcGeomorphypolygon);

    public HddcGeomorphypolygonEntity updateHddcGeomorphypolygon(HddcGeomorphypolygonEntity hddcGeomorphypolygon);

    public void deleteHddcGeomorphypolygons(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGeomorphypolygonQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
