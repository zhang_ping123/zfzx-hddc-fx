package com.css.zfzx.sjcj.modules.hddcDResultmaptable.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.bpm.platform.utils.PlatformPageUtils;
import com.css.zfzx.sjcj.modules.hddcDResultmaptable.repository.entity.HddcDResultmaptableEntity;
import com.css.zfzx.sjcj.modules.hddcDResultmaptable.service.HddcDResultmaptableService;
import com.css.zfzx.sjcj.modules.hddcDResultmaptable.viewobjects.HddcDResultmaptableQueryParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-19
 */
@Slf4j
@RestController
@RequestMapping("hddc/hddcDResultmaptables")
public class HddcDResultmaptableController {
    @Autowired
    private HddcDResultmaptableService hddcDResultmaptableService;

    @GetMapping("/queryHddcDResultmaptables")
    public RestResponse queryHddcDResultmaptables(HttpServletRequest request, HddcDResultmaptableQueryParams queryParams) {
        RestResponse response = null;
        try {
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            String sort=request.getParameter("sort");
            String order=request.getParameter("order");
            JSONObject jsonObject = hddcDResultmaptableService.queryHddcDResultmaptables(queryParams, curPage, pageSize,sort,order);
            response = RestResponse.succeed(jsonObject);
        } catch (Exception e) {
            String errorMessage = "查询失败!";
            log.error(errorMessage, e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcDResultmaptable(@PathVariable String id) {
        RestResponse response = null;
        try {
            HddcDResultmaptableEntity hddcDResultmaptable = hddcDResultmaptableService.getHddcDResultmaptable(id);
            response = RestResponse.succeed(hddcDResultmaptable);
        } catch (Exception e) {
            String errorMessage = "获取失败!";
            log.error(errorMessage, e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @PostMapping
    public RestResponse saveHddcDResultmaptable(@RequestBody HddcDResultmaptableEntity hddcDResultmaptable) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try {
            hddcDResultmaptableService.saveHddcDResultmaptable(hddcDResultmaptable);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        } catch (Exception e) {
            String errorMessage = "新增失败!";
            log.error(errorMessage, e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }

    @PutMapping
    public RestResponse updateHddcDResultmaptable(@RequestBody HddcDResultmaptableEntity hddcDResultmaptable) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try {
            hddcDResultmaptableService.updateHddcDResultmaptable(hddcDResultmaptable);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        } catch (Exception e) {
            String errorMessage = "修改失败!";
            log.error(errorMessage, e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @DeleteMapping
    public RestResponse deleteHddcDResultmaptables(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try {
            hddcDResultmaptableService.deleteHddcDResultmaptables(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        } catch (Exception e) {
            String errorMessage = "删除失败!";
            log.error(errorMessage, e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcDResultmaptableService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }

    /**
     * 上传附件
     * @param file
     * @param filenumber
     * @return
     */
//    @PostMapping("/upload")
//    public RestResponse upload(@RequestParam("file") MultipartFile file, @RequestParam("filenumber")String filenumber) {
//        RestResponse response = null;
//        JSONObject json = new JSONObject();
//        try{
//            int mag=hddcDResultmaptableService.upload(file,filenumber);
//            if (mag==2) {
//                json.put("message", "上传成功!");
//            }else if (mag==0){
//                json.put("message", "编号已存在!");
//            }
//            else if (mag==1){
//                json.put("message", "上传失败!");
//            }
//            response = RestResponse.succeed(json);
//        }catch (Exception e){
//            String errorMessage = "上传失败!";
//            log.error(errorMessage,e);
//            response = RestResponse.fail(errorMessage);
//        }
//        return response;
//    }

}