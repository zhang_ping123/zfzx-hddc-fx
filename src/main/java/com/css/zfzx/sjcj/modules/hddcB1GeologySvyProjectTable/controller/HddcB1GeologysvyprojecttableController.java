package com.css.zfzx.sjcj.modules.hddcB1GeologySvyProjectTable.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.zfzx.sjcj.modules.hddcB1GeologySvyProjectTable.repository.entity.HddcB1GeologysvyprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeologySvyProjectTable.service.HddcB1GeologysvyprojecttableService;
import com.css.zfzx.sjcj.modules.hddcB1GeologySvyProjectTable.viewobjects.HddcB1GeologysvyprojecttableQueryParams;
import com.css.bpm.platform.utils.PlatformPageUtils;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltQueryParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcB1Geologysvyprojecttables")
public class HddcB1GeologysvyprojecttableController {
    @Autowired
    private HddcB1GeologysvyprojecttableService hddcB1GeologysvyprojecttableService;

    @GetMapping("/queryHddcB1Geologysvyprojecttables")
    public RestResponse queryHddcB1Geologysvyprojecttables(HttpServletRequest request, HddcB1GeologysvyprojecttableQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcB1GeologysvyprojecttableService.queryHddcB1Geologysvyprojecttables(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcB1Geologysvyprojecttable(@PathVariable String id) {
        RestResponse response = null;
        try{
            HddcB1GeologysvyprojecttableEntity hddcB1Geologysvyprojecttable = hddcB1GeologysvyprojecttableService.getHddcB1Geologysvyprojecttable(id);
            response = RestResponse.succeed(hddcB1Geologysvyprojecttable);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcB1Geologysvyprojecttable(@RequestBody HddcB1GeologysvyprojecttableEntity hddcB1Geologysvyprojecttable) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB1GeologysvyprojecttableService.saveHddcB1Geologysvyprojecttable(hddcB1Geologysvyprojecttable);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcB1Geologysvyprojecttable(@RequestBody HddcB1GeologysvyprojecttableEntity hddcB1Geologysvyprojecttable)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB1GeologysvyprojecttableService.updateHddcB1Geologysvyprojecttable(hddcB1Geologysvyprojecttable);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcB1Geologysvyprojecttables(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB1GeologysvyprojecttableService.deleteHddcB1Geologysvyprojecttables(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcB1GeologysvyprojecttableService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }
    /***
     * 导出
     * @param response
     * @return
     */
    @GetMapping("/exportFile")
    public RestResponse exportFileYhDisasters(HttpServletResponse response,
                                              @RequestParam("projectName")String projectName,@RequestParam("name") String name,
                                              @RequestParam("province") String province,@RequestParam("city")String city,@RequestParam("area")String area) {
        RestResponse responseRest = null;
        JSONObject jsonObject = new JSONObject();
        try{
            HddcB1GeologysvyprojecttableQueryParams queryParams=new HddcB1GeologysvyprojecttableQueryParams();
            queryParams.setArea(area);
            queryParams.setCity(city);
            queryParams.setProvince(province);
            queryParams.setProjectName(projectName);
            queryParams.setName(name);
            hddcB1GeologysvyprojecttableService.exportFile(queryParams,response);
            jsonObject.put("message", "导出成功");
            responseRest = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            responseRest = RestResponse.fail(errorMessage);
        }
        return responseRest;
    }

    /**
     * 导入
     *
     * @param file
     * @param response
     * @return
     */
    @PostMapping("/importDisaster")
    public RestResponse export(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        RestResponse restResponse = null;
        try {
            String s = hddcB1GeologysvyprojecttableService.exportExcel( file, response);
            restResponse = RestResponse.succeed(s);
        } catch (Exception e) {
            String errormessage = "导入失败";
            log.error(errormessage, e);
            restResponse = RestResponse.fail(errormessage);
        }
        return restResponse;
    }
}