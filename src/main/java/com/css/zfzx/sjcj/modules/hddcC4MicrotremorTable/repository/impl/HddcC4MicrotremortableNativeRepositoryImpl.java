package com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.repository.impl;

import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.org.dept.repository.entity.DeptEntity;
import com.css.bpm.platform.org.division.repository.DivisionRepository;
import com.css.bpm.platform.org.division.repository.entity.DivisionEntity;
import com.css.bpm.platform.org.role.repository.entity.RoleEntity;
import com.css.bpm.platform.utils.PlatformSessionUtils;
import com.css.zfzx.sjcj.common.utils.ServerUtil;
import com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.repository.HddcC4MicrotremortableNativeRepository;
import com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.repository.entity.HddcC4MicrotremortableEntity;
import com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.viewobjects.HddcC4MicrotremortableQueryParams;
import com.css.bpm.platform.utils.PlatformObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
/**
 * @author zhangping
 * @date 2020-11-23
 */
@Repository
@PropertySource("classpath:platform-config.yml")
public class HddcC4MicrotremortableNativeRepositoryImpl implements HddcC4MicrotremortableNativeRepository {
    @PersistenceContext
    private EntityManager em;
    @Value("${role.superCode}")
    private String superCode;
    @Value("${role.provinceCode}")
    private String provinceCode;
    @Value("${role.cityCode}")
    private String cityCode;
    @Value("${role.areaCode}")
    private String areaCode;
    @Autowired
    private DivisionRepository divisionRepository;
    private static final Logger log = LoggerFactory.getLogger(HddcC4MicrotremortableNativeRepositoryImpl.class);


    @Override
    public Page<HddcC4MicrotremortableEntity> queryHddcC4Microtremortables(HddcC4MicrotremortableQueryParams queryParams, int curPage, int pageSize) {
        StringBuilder sql = new StringBuilder("select * from hddc_c4_microtremortable ");
        StringBuilder whereSql = new StringBuilder(" where 1=1 ");
        if(!PlatformObjectUtils.isEmpty(queryParams.getProjectId())) {
            whereSql.append(" and project_id = :projectId");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getUnit())) {
            whereSql.append(" and Unit = :unit");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getCreateTime())) {
            whereSql.append(" and create_time = :createTime");
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getInstrument())) {
            whereSql.append(" and Instrument = :instrument");
        }
        String userId = PlatformSessionUtils.getUserId();
        List<RoleEntity> roles = PlatformAPI.getOrgAPI().getUserAPI().getRoles(userId);
        //是否为超级管理员
        boolean containtRole = ServerUtil.isContaintRole(roles, superCode);
        //是否为省级管理员
        boolean containtRolePro = ServerUtil.isContaintRole(roles, provinceCode);
        //是否为市级管理员
        boolean containtRoleCity = ServerUtil.isContaintRole(roles, cityCode);
        //是否为县级管理员
        boolean containtRoleArea= ServerUtil.isContaintRole(roles, areaCode);
        if(!containtRole){
            if (!containtRolePro && !containtRoleCity && !containtRoleArea) {
                whereSql.append(" and create_user =:userId");
            }
            if(containtRolePro){
                whereSql.append(" and province like :authProvince");
            }
            if(containtRoleCity){
                whereSql.append(" and city like :authCity");
            }
            if(containtRoleArea){
                whereSql.append(" and area like :authArea");
            }
        }
        Query query = this.em.createNativeQuery(sql.append(whereSql).toString(), HddcC4MicrotremortableEntity.class);
        StringBuilder countSql = new StringBuilder("select count(*) from hddc_c4_microtremortable ");
        Query countQuery = this.em.createNativeQuery(countSql.append(whereSql).toString());
        if(!PlatformObjectUtils.isEmpty(queryParams.getProjectId())) {
            query.setParameter("projectId", queryParams.getProjectId());
            countQuery.setParameter("projectId", queryParams.getProjectId());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getUnit())) {
            query.setParameter("unit", queryParams.getUnit());
            countQuery.setParameter("unit", queryParams.getUnit());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getCreateTime())) {
            query.setParameter("createTime", queryParams.getCreateTime());
            countQuery.setParameter("createTime", queryParams.getCreateTime());
        }
        if(!PlatformObjectUtils.isEmpty(queryParams.getInstrument())) {
            query.setParameter("instrument", queryParams.getInstrument());
            countQuery.setParameter("instrument", queryParams.getInstrument());
        }
        if(!containtRole){
            if (!containtRolePro && !containtRoleCity && !containtRoleArea) {
                query.setParameter("userId",PlatformSessionUtils.getUserId());
                countQuery.setParameter("userId", PlatformSessionUtils.getUserId());
            }
            DeptEntity mainDept = PlatformAPI.getOrgAPI().getUserAPI().getMainDept(userId);
            String divisionId = mainDept.getDivisionId();
            DivisionEntity validDivisionById = divisionRepository.findValidDivisionById(divisionId);
            String divisionName = validDivisionById.getDivisionName();
            if(containtRolePro){
                query.setParameter("authProvince","%"+divisionName+"%");
                countQuery.setParameter("authProvince","%"+divisionName+"%");
            }
            if(containtRoleCity){
                query.setParameter("authCity","%"+divisionName+"%");
                countQuery.setParameter("authCity","%"+divisionName+"%");
            }
            if(containtRoleArea){
                query.setParameter("authArea","%"+divisionName+"%");
                countQuery.setParameter("authArea","%"+divisionName+"%");
            }
        }
        Pageable pageable = PageRequest.of(curPage - 1, pageSize);
        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());
        List<HddcC4MicrotremortableEntity> list = query.getResultList();
        BigInteger count = (BigInteger) countQuery.getSingleResult();
        Page<HddcC4MicrotremortableEntity> hddcC4MicrotremortablePage = new PageImpl<>(list, pageable, count.longValue());
        return hddcC4MicrotremortablePage;
    }
}
