package com.css.zfzx.sjcj.modules.hddcStratigraphySvyPoint.repository.entity;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author lihelei
 * @date 2020-11-27
 */
@Data
@Entity
@Table(name="hddc_stratigraphysvypoint")
public class HddcStratigraphysvypointEntity implements Serializable {

    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 走向 [°]
     */
    @Column(name="strike")
    private Integer strike;
    /**
     * 接触关系描述
     */
    @Column(name="touchredescription")
    private String touchredescription;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 地层描述
     */
    @Column(name="stratigraphydescription")
    private String stratigraphydescription;
    /**
     * 是否修改工作底图
     */
    @Column(name="ismodifyworkmap")
    private Integer ismodifyworkmap;
    /**
     * 照片镜向
     */
    @Column(name="photoviewingto")
    private Integer photoviewingto;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 地层点编号
     */
    //@Id
    @Column(name="id")
    private String id;
    /**
     * 地层点编号
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 是否在图中显示
     */
    @Column(name="isinmap")
    private Integer isinmap;
    /**
     * 地质调查观测点编号
     */
    @Column(name="geologicalsvypointid")
    private String geologicalsvypointid;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 平面图原始文件编号
     */
    @Column(name="sketch_arwid")
    private String sketchArwid;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 地层厚度 [米]
     */
    @Column(name="thickness")
    private String thickness;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 备注
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 典型剖面图原始文件编号
     */
    @Column(name="typicalprofile_arwid")
    private String typicalprofileArwid;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 野外编号
     */
    @Column(name="fieldid")
    private String fieldid;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 平面图文件编号
     */
    @Column(name="sketch_aiid")
    private String sketchAiid;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 倾角 [度]
     */
    @Column(name="clination")
    private Integer clination;
    /**
     * 典型照片文件编号
     */
    @Column(name="photo_aiid")
    private String photoAiid;
    /**
     * 地层名称
     */
    @Column(name="stratigraphyname")
    private String stratigraphyname;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 数据来源
     */
    @Column(name="datasource")
    private String datasource;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 实测倾向 [度]
     */
    @Column(name="dip")
    private Integer dip;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 典型剖面图图像文件编号
     */
    @Column(name="typicalprofile_aiid")
    private String typicalprofileAiid;
    /**
     * 拍摄者
     */
    @Column(name="photographer")
    private String photographer;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 典型照片原始文件编号
     */
    @Column(name="photo_arwid")
    private String photoArwid;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 符号或标注旋转角度
     */
    @Column(name="lastangle")
    private Double lastangle;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 接触关系
     */
    @Column(name="touchrelation")
    private Integer touchrelation;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 是否倒转地层产状
     */
    @Column(name="isreversed")
    private Integer isreversed;

}

