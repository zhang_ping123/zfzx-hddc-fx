package com.css.zfzx.sjcj.modules.hddcB1GeomorLnHasGeoSvyPt.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnHasGeoSvyPt.repository.entity.HddcB1GeomorlnhasgeosvyptEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnHasGeoSvyPt.viewobjects.HddcB1GeomorlnhasgeosvyptQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */

public interface HddcB1GeomorlnhasgeosvyptService {

    public JSONObject queryHddcB1Geomorlnhasgeosvypts(HddcB1GeomorlnhasgeosvyptQueryParams queryParams, int curPage, int pageSize);

    public HddcB1GeomorlnhasgeosvyptEntity getHddcB1Geomorlnhasgeosvypt(String id);

    public HddcB1GeomorlnhasgeosvyptEntity saveHddcB1Geomorlnhasgeosvypt(HddcB1GeomorlnhasgeosvyptEntity hddcB1Geomorlnhasgeosvypt);

    public HddcB1GeomorlnhasgeosvyptEntity updateHddcB1Geomorlnhasgeosvypt(HddcB1GeomorlnhasgeosvyptEntity hddcB1Geomorlnhasgeosvypt);

    public void deleteHddcB1Geomorlnhasgeosvypts(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB1GeomorlnhasgeosvyptQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
