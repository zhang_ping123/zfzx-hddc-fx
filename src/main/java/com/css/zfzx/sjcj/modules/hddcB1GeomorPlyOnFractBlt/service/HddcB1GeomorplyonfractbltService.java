package com.css.zfzx.sjcj.modules.hddcB1GeomorPlyOnFractBlt.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyOnFractBlt.repository.entity.HddcB1GeomorplyonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyOnFractBlt.viewobjects.HddcB1GeomorplyonfractbltQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */

public interface HddcB1GeomorplyonfractbltService {

    public JSONObject queryHddcB1Geomorplyonfractblts(HddcB1GeomorplyonfractbltQueryParams queryParams, int curPage, int pageSize);

    public HddcB1GeomorplyonfractbltEntity getHddcB1Geomorplyonfractblt(String id);

    public HddcB1GeomorplyonfractbltEntity saveHddcB1Geomorplyonfractblt(HddcB1GeomorplyonfractbltEntity hddcB1Geomorplyonfractblt);

    public HddcB1GeomorplyonfractbltEntity updateHddcB1Geomorplyonfractblt(HddcB1GeomorplyonfractbltEntity hddcB1Geomorplyonfractblt);

    public void deleteHddcB1Geomorplyonfractblts(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB1GeomorplyonfractbltQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
