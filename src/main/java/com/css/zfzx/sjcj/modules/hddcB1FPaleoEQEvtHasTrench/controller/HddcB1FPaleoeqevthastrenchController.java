package com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtHasTrench.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtHasTrench.repository.entity.HddcB1FPaleoeqevthastrenchEntity;
import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtHasTrench.service.HddcB1FPaleoeqevthastrenchService;
import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtHasTrench.viewobjects.HddcB1FPaleoeqevthastrenchQueryParams;
import com.css.bpm.platform.utils.PlatformPageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcB1FPaleoeqevthastrenchs")
public class HddcB1FPaleoeqevthastrenchController {
    @Autowired
    private HddcB1FPaleoeqevthastrenchService hddcB1FPaleoeqevthastrenchService;

    @GetMapping("/queryHddcB1FPaleoeqevthastrenchs")
    public RestResponse queryHddcB1FPaleoeqevthastrenchs(HttpServletRequest request, HddcB1FPaleoeqevthastrenchQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcB1FPaleoeqevthastrenchService.queryHddcB1FPaleoeqevthastrenchs(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcB1FPaleoeqevthastrench(@PathVariable String id) {
        RestResponse response = null;
        try{
            HddcB1FPaleoeqevthastrenchEntity hddcB1FPaleoeqevthastrench = hddcB1FPaleoeqevthastrenchService.getHddcB1FPaleoeqevthastrench(id);
            response = RestResponse.succeed(hddcB1FPaleoeqevthastrench);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcB1FPaleoeqevthastrench(@RequestBody HddcB1FPaleoeqevthastrenchEntity hddcB1FPaleoeqevthastrench) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB1FPaleoeqevthastrenchService.saveHddcB1FPaleoeqevthastrench(hddcB1FPaleoeqevthastrench);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcB1FPaleoeqevthastrench(@RequestBody HddcB1FPaleoeqevthastrenchEntity hddcB1FPaleoeqevthastrench)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB1FPaleoeqevthastrenchService.updateHddcB1FPaleoeqevthastrench(hddcB1FPaleoeqevthastrench);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcB1FPaleoeqevthastrenchs(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB1FPaleoeqevthastrenchService.deleteHddcB1FPaleoeqevthastrenchs(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcB1FPaleoeqevthastrenchService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }

    /***
     * 导出
     * @param response
     * @return
     */
    @GetMapping("/exportFile")
    public RestResponse exportFileYhDisasters(HttpServletResponse response,
                                              @RequestParam("projectName")String projectName,
                                              @RequestParam("province") String province,@RequestParam("city")String city,@RequestParam("area")String area) {
        RestResponse responseRest = null;
        JSONObject jsonObject = new JSONObject();
        try{
            HddcB1FPaleoeqevthastrenchQueryParams queryParams=new HddcB1FPaleoeqevthastrenchQueryParams();
            queryParams.setArea(area);
            queryParams.setCity(city);
            queryParams.setProvince(province);
            queryParams.setProjectName(projectName);
            hddcB1FPaleoeqevthastrenchService.exportFile(queryParams,response);
            jsonObject.put("message", "导出成功");
            responseRest = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            responseRest = RestResponse.fail(errorMessage);
        }
        return responseRest;
    }

    /**
     * 导入
     *
     * @param file
     * @param response
     * @return
     */
    @PostMapping("/importDisaster")
    public RestResponse export(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        RestResponse restResponse = null;
        try {
            String s = hddcB1FPaleoeqevthastrenchService.exportExcel( file, response);
            restResponse = RestResponse.succeed(s);
        } catch (Exception e) {
            String errormessage = "导入失败";
            log.error(errormessage, e);
            restResponse = RestResponse.fail(errormessage);
        }
        return restResponse;
    }
}