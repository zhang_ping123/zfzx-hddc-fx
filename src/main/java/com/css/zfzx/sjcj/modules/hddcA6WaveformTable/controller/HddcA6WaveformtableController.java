package com.css.zfzx.sjcj.modules.hddcA6WaveformTable.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.bpm.platform.utils.PlatformPageUtils;
import com.css.zfzx.sjcj.modules.hddcA6WaveformTable.repository.entity.HddcA6WaveformtableEntity;
import com.css.zfzx.sjcj.modules.hddcA6WaveformTable.service.HddcA6WaveformtableService;
import com.css.zfzx.sjcj.modules.hddcA6WaveformTable.viewobjects.HddcA6WaveformtableQueryParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcA6Waveformtables")
public class HddcA6WaveformtableController {
    @Autowired
    private HddcA6WaveformtableService hddcA6WaveformtableService;

    @GetMapping("/queryHddcA6Waveformtables")
    public RestResponse queryHddcA6Waveformtables(HttpServletRequest request, HddcA6WaveformtableQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcA6WaveformtableService.queryHddcA6Waveformtables(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcA6Waveformtable(@PathVariable String id) {
        RestResponse response = null;
        try{
            HddcA6WaveformtableEntity hddcA6Waveformtable = hddcA6WaveformtableService.getHddcA6Waveformtable(id);
            response = RestResponse.succeed(hddcA6Waveformtable);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcA6Waveformtable(@RequestBody HddcA6WaveformtableEntity hddcA6Waveformtable) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcA6WaveformtableService.saveHddcA6Waveformtable(hddcA6Waveformtable);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcA6Waveformtable(@RequestBody HddcA6WaveformtableEntity hddcA6Waveformtable)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcA6WaveformtableService.updateHddcA6Waveformtable(hddcA6Waveformtable);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcA6Waveformtables(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcA6WaveformtableService.deleteHddcA6Waveformtables(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcA6WaveformtableService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }
    /***
     * 导出
     * @param response
     * @return
     */
    @GetMapping("/exportFile")
    public RestResponse exportFileYhDisasters(HttpServletResponse response,
                                              @RequestParam("projectName")String projectName,
                                              @RequestParam("province") String province, @RequestParam("city")String city, @RequestParam("area")String area) {
        RestResponse responseRest = null;
        JSONObject jsonObject = new JSONObject();
        try{
            HddcA6WaveformtableQueryParams queryParams=new HddcA6WaveformtableQueryParams();
            queryParams.setArea(area);
            queryParams.setCity(city);
            queryParams.setProvince(province);
            queryParams.setProjectName(projectName);
            hddcA6WaveformtableService.exportFile(queryParams,response);
            jsonObject.put("message", "导出成功");
            responseRest = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            responseRest = RestResponse.fail(errorMessage);
        }
        return responseRest;
    }
    /**
     * 导入
     *
     * @param file
     * @param response
     * @return
     */
    @PostMapping("/importDisaster")
    public RestResponse export(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        RestResponse restResponse = null;
        try {
            String s = hddcA6WaveformtableService.exportExcel( file, response);
            restResponse = RestResponse.succeed(s);
        } catch (Exception e) {
            String errormessage = "导入失败";
            log.error(errormessage, e);
            restResponse = RestResponse.fail(errormessage);
        }
        return restResponse;
    }

}