package com.css.zfzx.sjcj.modules.hddcGeomorphySvyPoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyPoint.repository.entity.HddcGeomorphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyPoint.viewobjects.HddcGeomorphysvypointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */

public interface HddcGeomorphysvypointService {

    public JSONObject queryHddcGeomorphysvypoints(HddcGeomorphysvypointQueryParams queryParams, int curPage, int pageSize);

    public HddcGeomorphysvypointEntity getHddcGeomorphysvypoint(String id);

    public HddcGeomorphysvypointEntity saveHddcGeomorphysvypoint(HddcGeomorphysvypointEntity hddcGeomorphysvypoint);

    public HddcGeomorphysvypointEntity updateHddcGeomorphysvypoint(HddcGeomorphysvypointEntity hddcGeomorphysvypoint);

    public void deleteHddcGeomorphysvypoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGeomorphysvypointQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
