package com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyLine.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-12-02
 */
@Data
public class HddcWyGeochemicalsvylineVO implements Serializable {

    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 测线终点经度
     */
    @Excel(name = "测线终点经度", orderNum = "13")
    private Double endlongitutde;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 异常点总数
     */
    @Excel(name = "异常点总数", orderNum = "23")
    private Integer abnpointnum;
    /**
     * 任务名称
     */
    @Excel(name="任务名称",orderNum = "8")
    private String taskName;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 测线名称
     */
    @Excel(name = "测线名称", orderNum = "9")
    private String name;
    /**
     * 项目名称
     */
    @Excel(name="项目名称",orderNum = "7")
    private String projectName;
    /**
     * 测线起点纬度
     */
    @Excel(name = "测线起点纬度", orderNum = "18")
    private Double startlatitude;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 显示码
     */
    @Excel(name = "显示码", orderNum = "25")
    private Integer showcode;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 滑动
     */
    @Excel(name = "滑动", orderNum = "14")
    private Double slippagevalue;

    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 测线起点经度
     */
    @Excel(name = "测线起点经度", orderNum = "10")
    private Double startlongitude;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 分析结果图图像文件编号
     */
    @Excel(name = "分析结果图图像文件编号", orderNum = "26")
    private String rmAiid;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "4")
    private String area;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 测线编号
     */
    @Excel(name = "测线编号", orderNum = "1")
    private String id;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 探测方法
     */
    @Excel(name = "探测方法", orderNum = "24")
    private Integer svymethod;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 异常下限值
     */
    @Excel(name = "异常下限值", orderNum = "22")
    private Double abnormalbottomvalue;
    /**
     * 内插点
     */
    @Excel(name = "内插点", orderNum = "17")
    private Integer interpolatenum;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 均值
     */
    @Excel(name = "均值", orderNum = "11")
    private Double meanvalue;
    /**
     * 测线终点纬度
     */
    @Excel(name = "测线终点纬度", orderNum = "21")
    private Double endlatitude;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "12")
    private String commentInfo;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 项目ID
     */
    private String projectcode;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 测线长度 [米]
     */
    @Excel(name = "测线长度 [米]", orderNum = "20")
    private Double length;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 测点数
     */
    @Excel(name = "测点数", orderNum = "14")
    private Integer svypointnum;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "3")
    private String city;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 乡
     */
    @Excel(name="详细地址",orderNum = "5")
    private String town;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 分析结果图原始表索引号
     */
    @Excel(name = "分析结果图原始表索引号", orderNum = "16")
    private String rmArwid;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段5
     */
    @Excel(name="区域",orderNum = "6")
    private String extends5;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "2")
    private String province;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备注
     */
    private String remark;
    /**
     * 测线所属工程编号
     */
    @Excel(name = "测线所属工程编号", orderNum = "19")
    private String projectId;
    /**
     * 经度
     */
    private Double lon;
    /**
     * 维度
     */
    private Double lat;


    private String provinceName;
    private String cityName;
    private String areaName;

    private String rowNum;
    private String errorMsg;
}