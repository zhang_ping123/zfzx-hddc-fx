package com.css.zfzx.sjcj.modules.hddcGeomorStation.viewobjects;

import lombok.Data;

/**
 * @author whj
 * @date 2020-11-30
 */
@Data
public class HddcGeomorstationQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

}
