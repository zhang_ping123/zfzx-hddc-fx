package com.css.zfzx.sjcj.modules.hddcGeomorphySvyReProf.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
public class HddcGeomorphysvyreprofVO implements Serializable {

    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段5
     */
    @Excel(name = "区域", orderNum = "5")
    private String extends5;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 备注-测量线描述及目的
     */
    @Excel(name = "备注-测量线描述及目的", orderNum = "31")
    private String commentInfo;
    /**
     * 剖面线分析结果图原始文件编号
     */
    @Excel(name = "剖面线分析结果图原始文件编号", orderNum = "32")
    private String profileArwid;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 测线名称
     */
    @Excel(name = "测线名称", orderNum = "16")
    private String name;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 乡
     */
    private String town;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 所属微地貌面编号
     */
    @Excel(name = "所属微地貌面编号", orderNum = "17")
    private String regionid;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 图切剖面线编号
     */
    @Excel(name = "图切剖面线编号", orderNum = "8")
    private String sliceselfid;
    /**
     * 测线编号
     */
    @Excel(name = "测线编号", orderNum = "0")
    private String id;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备注
     */
    private String remark;
    /**
     * 村
     */
    private String village;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段4
     */
    @Excel(name = "详细地址", orderNum = "4")
    private String extends4;
    /**
     * 项目名称
     */
    @Excel(name = "项目名称", orderNum = "6")
    private String projectName;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 剖面线分析结果图图像文件编号
     */
    @Excel(name = "剖面线分析结果图图像文件编号", orderNum = "9")
    private String profileAiid;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 微地貌测量工程编号
     */
    @Excel(name = "微地貌测量工程编号", orderNum = "10")
    private String geomorphysvyprjid;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 任务名称
     */
    @Excel(name = "任务名称", orderNum = "7")
    private String taskName;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String rowNum;
    private String errorMsg;
}