package com.css.zfzx.sjcj.modules.hddcRock1LinePre.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcRock1LinePre.repository.entity.HddcRock1linepreEntity;
import com.css.zfzx.sjcj.modules.hddcRock1LinePre.viewobjects.HddcRock1linepreQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-26
 */

public interface HddcRock1linepreService {

    public JSONObject queryHddcRock1linepres(HddcRock1linepreQueryParams queryParams, int curPage, int pageSize);

    public HddcRock1linepreEntity getHddcRock1linepre(String id);

    public HddcRock1linepreEntity saveHddcRock1linepre(HddcRock1linepreEntity hddcRock1linepre);

    public HddcRock1linepreEntity updateHddcRock1linepre(HddcRock1linepreEntity hddcRock1linepre);

    public void deleteHddcRock1linepres(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcRock1linepreQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
