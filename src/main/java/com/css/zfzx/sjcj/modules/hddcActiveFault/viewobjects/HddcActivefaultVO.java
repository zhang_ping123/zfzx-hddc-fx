package com.css.zfzx.sjcj.modules.hddcActiveFault.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
public class HddcActivefaultVO implements Serializable {

    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 平均垂直速率 [毫米/年]
     */
    @Excel(name = "平均垂直速率 [毫米/年]")
    private Double avevrate;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 走向 [16方位]
     */
    @Excel(name="走向 [16方位]")
    private Integer strikedirection;
    /**
     * 平均破裂长度
     */
    @Excel(name = "平均破裂长度")
    private Integer averagerupturelength;
    /**
     * 最新垂直速率 [毫米/年]
     */
    @Excel(name = "最新垂直速率 [毫米/年]")
    private Double newvrate;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 断层显示码
     */
    @Excel(name = "断层显示码")
    private Integer showcode;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 最新活动时代
     */
    @Excel(name = "最新活动时代")
    private Integer latestactiveperiod;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 破碎带宽度 [米]
     */
    @Excel(name = "破碎带宽度 [米]")
    private Double width;
    /**
     * 断层编号
     */
    private String id;
    /**
     * 误差
     */
    @Excel(name = "最大水平速率误差")
    private Double maxhrateer;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 位移与平均速率起算时间
     */
    @Excel(name = "位移与平均速率起算时间")
    private String starttimeest;
    /**
     * 蠕动速率误差
     */
    @Excel(name = "蠕动速率误差")
    private Double creeprateer;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备注
     */
    @Excel(name = "备注")
    private String commentInfo;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 断层符号上标位
     */
    @Excel(name = "断层符号上标位")
    private Integer nsb3;
    /**
     * 最大水平速率 [毫米/年]
     */
    @Excel(name = "最大水平速率 [毫米/年]")
    private Double maxhrate;
    /**
     * 垂直位移 [米]
     */
    @Excel(name = "垂直位移 [米]")
    private Double vdisplaceest;
    /**
     * 变形带宽度 [米]
     */
    @Excel(name = "变形带宽度 [米]")
    private Double fracturebeltwidth;
    /**
     * 长度 [公里]
     */
    @Excel(name = "长度 [公里]")
    private Double length;
    /**
     * 误差
     */
    @Excel(name = "最新垂直速率误差")
    private Double newvrateer;
    /**
     * 误差
     */
    @Excel(name = "平均垂直速率误差")
    private Double avevrateer;
    /**
     * 平均同震位移 [米]
     */
    @Excel(name = "平均同震位移 [米]")
    private Double coseismicaverageslipest;
    /**
     * 平均水平速率 [毫米/年]
     */
    @Excel(name = "平均水平速率 [毫米/年]")
    private Double avehrate;
    /**
     * 错动深度
     */
    @Excel(name = "错动深度")
    private Integer slipdepthest;
    /**
     * 确定手段
     */
    @Excel(name = "确定手段")
    private String method;
    /**
     * 误差
     */
    @Excel(name = "垂直位移误差")
    private Double vdisplaceer;
    /**
     * 最大同震位移 [米]
     */
    @Excel(name = "最大同震位移 [米]")
    private Double coseismicmaxslipest;
    /**
     * 误差
     */
    @Excel(name = "最新水平速率误差")
    private Double newhrateer;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 误差
     */
    @Excel(name = "水平张缩位移误差")
    private Double tdisplaceer;
    /**
     * 平均滑动速率 [毫米/年]
     */
    @Excel(name = "平均滑动速率 [毫米/年]")
    private Double averagesliprateest;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 误差
     */
    @Excel(name = "走向水平位移误差")
    private Double hdisplaceer;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 蠕动速率 [毫米/年]
     */
    @Excel(name = "蠕动速率 [毫米/年]")
    private Double creeprateest;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 错动深度误差
     */
    @Excel(name = "错动深度误差")
    private Integer slipdepther;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 古地震复发间隔上限
     */
    @Excel(name = "古地震复发间隔上限")
    private Integer eqeventritop;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）",orderNum = "3")
    private String area;
    /**
     * 最大垂直速率 [米]
     */
    @Excel(name = "最大垂直速率 [米]")
    private Double maxvrate;
    /**
     * 平均同震位移误差
     */
    @Excel(name = "平均同震位移误差")
    private Double coseismicaveragesliper;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 误差
     */
    @Excel(name = "最大垂直速率误差")
    private Double maxvrateer;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 最新地震平均位移
     */
    @Excel(name = "最新地震平均位移")
    private Double latestcoseismicslipest;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 断层段名称
     */
    @Excel(name="断层段名称")
    private String faultsegmentname;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 市
     */
    @Excel(name = "市",orderNum = "2")
    private String city;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 最新地震平均位移误差
     */
    @Excel(name = "最新地震平均位移误差")
    private Double latestcoseismicsliper;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 最大同震位移误差
     */
    @Excel(name = "最大同震位移误差")
    private Double coseismicmaxsliper;
    /**
     * 断层性质
     */
    @Excel(name = "断层性质")
    private Integer feature;
    /**
     * 断层符号基础位
     */
    @Excel(name = "断层符号基础位")
    private String nsb1;
    /**
     * 最新水平速率 [毫米/年]
     */
    @Excel(name = "最新水平速率 [毫米/年]")
    private Double newhrate;
    /**
     * 误差
     */
    @Excel(name = "平均水平速率误差")
    private Double avehrateer;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 水平//张缩位移 [米]
     */
    @Excel(name = "水平张缩位移 [米]")
    private Double tdisplaceest;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 平均滑动速率误差
     */
    @Excel(name = "平均滑动速率误差")
    private Double averagesliprateer;
    /**
     * 省
     */
    @Excel(name="省",orderNum = "1")
    private String province;
    /**
     * 最新速率起算时间
     */
    @Excel(name = "最新速率起算时间")
    private Double starttimenewest;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 断层符号下标位
     */
    @Excel(name = "断层符号下标位")
    private Integer nsb2;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 工作底图比例尺（分母）
     */
    @Excel(name="工作底图比例尺（分母）")
    private Integer scale;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 最大破裂长度
     */
    @Excel(name = "最大破裂长度")
    private Integer maxrupturelength;
    /**
     * 上断点埋深 [米]
     */
    @Excel(name = "上断点埋深 [米]")
    private String topdepth;
    /**
     * 断层名称
     */
    @Excel(name="断层名称")
    private String name;
    /**
     * 断裂带名称
     */
    @Excel(name="断裂带名称")
    private String fracturezonename;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 乡
     */
    private String town;
    /**
     * 倾角 [度]
     */
    @Excel(name = "倾角 [度]")
    private Integer clination;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 古地震复发间隔
     */
    @Excel(name = "古地震复发间隔")
    private Integer eqeventribottom;
    /**
     * 古地震事件次数
     */
    @Excel(name = "古地震事件次数")
    private Integer eqeventcount;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 断层走向 [度]
     */
    @Excel(name="断层走向 [度]")
    private Integer strike;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 走向水平位移 [米]
     */
    @Excel(name="走向水平位移 [米]")
    private Double hdisplaceest;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 倾向 [16方位]
     */
    @Excel(name="倾向 [16方位]")
    private Integer direction;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 最晚地震离逝时间
     */
    @Excel(name = "最晚地震离逝时间")
    private Integer elapsetimeforlatesteq;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer strikedirectionName;
    private Integer directionName;
    private Integer featureName;
    private Integer latestactiveperiodName;

    private String rowNum;
    private String errorMsg;
}