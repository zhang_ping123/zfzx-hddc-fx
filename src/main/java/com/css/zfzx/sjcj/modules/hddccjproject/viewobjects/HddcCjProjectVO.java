package com.css.zfzx.sjcj.modules.hddccjproject.viewobjects;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangping
 * @date 2020-11-26
 */
@Data
public class HddcCjProjectVO implements Serializable {

    /**
     * 创建人
     */
    private String createUser;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 区（县）
     */
    private String area;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备注
     */
    private String remark;
    /**
     * 市
     */
    private String city;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 编号
     */
    private String id;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 项目描述
     */
    private String projectInfo;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 人员信息
     */
    private String personIds;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 村
     */
    private String village;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 省
     */
    private String province;
    /**
     * 编号
     */
    private String objectCode;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String personIdsName;
}