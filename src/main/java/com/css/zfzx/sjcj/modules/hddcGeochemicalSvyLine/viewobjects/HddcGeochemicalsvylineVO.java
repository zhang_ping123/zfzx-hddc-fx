package com.css.zfzx.sjcj.modules.hddcGeochemicalSvyLine.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Data
public class HddcGeochemicalsvylineVO implements Serializable {

    /**
     * 显示码
     */
    @Excel(name = "显示码", orderNum = "4")
    private Integer showcode;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 村
     */
    private String village;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 分析结果图图像文件编号
     */
    @Excel(name = "分析结果图图像文件编号", orderNum = "5")
    private String rmAiid;
    /**
     * 备注
     */
    private String remark;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 异常点总数
     */
    @Excel(name = "异常点总数", orderNum = "6")
    private Integer abnpointnum;
    /**
     * 测线名称
     */
    @Excel(name = "测线名称", orderNum = "7")
    private String name;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 项目ID
     */
    private String projectnum;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 测线起点经度
     */
    @Excel(name = "测线起点经度", orderNum = "8")
    private Double startlongitude;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 探测方法
     */
    @Excel(name = "探测方法", orderNum = "9")
    private Integer svymethod;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 均值
     */
    @Excel(name = "均值", orderNum = "10")
    private Double meanvalue;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 乡
     */
    private String town;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "11")
    private String commentInfo;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 测线终点经度
     */
    @Excel(name = "测线终点经度", orderNum = "12")
    private Double endlongitutde;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 滑动
     */
    @Excel(name = "滑动", orderNum = "13")
    private Double slippagevalue;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 测点数
     */
    @Excel(name = "测点数", orderNum = "14")
    private Integer svypointnum;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 分析结果图原始表索引号
     */
    @Excel(name = "分析结果图原始表索引号", orderNum = "15")
    private String rmArwid;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 内插点
     */
    @Excel(name = "内插点", orderNum = "16")
    private Integer interpolatenum;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 测线起点纬度
     */
    @Excel(name = "测线起点纬度", orderNum = "17")
    private Double startlatitude;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 测线所属工程编号
     */
    @Excel(name = "测线所属工程编号", orderNum = "18")
    private String projectid;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 测线长度 [米]
     */
    @Excel(name = "测线长度 [米]", orderNum = "19")
    private Double length;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 测线编号
     */
    @Excel(name = "测线编号", orderNum = "20")
    private String id;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 测线终点纬度
     */
    @Excel(name = "测线终点纬度", orderNum = "21")
    private Double endlatitude;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 异常下限值
     */
    @Excel(name = "异常下限值", orderNum = "22")
    private Double abnormalbottomvalue;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer svymethodName;
    private String rowNum;
    private String errorMsg;
}