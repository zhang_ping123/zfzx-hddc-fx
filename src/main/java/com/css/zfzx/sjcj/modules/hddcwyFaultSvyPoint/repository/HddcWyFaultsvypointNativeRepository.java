package com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.viewobjects.HddcWyFaultsvypointQueryParams;
import com.fasterxml.jackson.databind.node.BigIntegerNode;
import org.springframework.data.domain.Page;

import javax.management.Query;
import java.math.BigInteger;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcWyFaultsvypointNativeRepository {

    Page<HddcWyFaultsvypointEntity> queryHddcWyFaultsvypoints(HddcWyFaultsvypointQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyFaultsvypoint(HddcAppZztCountVo queryParams);

    List<HddcWyFaultsvypointEntity> exportpoint(HddcAppZztCountVo queryParams);
}
