package com.css.zfzx.sjcj.modules.hddcVerticalDeformation.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcVerticalDeformation.repository.HddcVerticaldeformationNativeRepository;
import com.css.zfzx.sjcj.modules.hddcVerticalDeformation.repository.HddcVerticaldeformationRepository;
import com.css.zfzx.sjcj.modules.hddcVerticalDeformation.repository.entity.HddcVerticaldeformationEntity;
import com.css.zfzx.sjcj.modules.hddcVerticalDeformation.service.HddcVerticaldeformationService;
import com.css.zfzx.sjcj.modules.hddcVerticalDeformation.viewobjects.HddcVerticaldeformationQueryParams;
import com.css.zfzx.sjcj.modules.hddcVerticalDeformation.viewobjects.HddcVerticaldeformationVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zyb
 * @date 2020-11-26
 */
@Service
public class HddcVerticaldeformationServiceImpl implements HddcVerticaldeformationService {

	@Autowired
    private HddcVerticaldeformationRepository hddcVerticaldeformationRepository;
    @Autowired
    private HddcVerticaldeformationNativeRepository hddcVerticaldeformationNativeRepository;

    @Override
    public JSONObject queryHddcVerticaldeformations(HddcVerticaldeformationQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcVerticaldeformationEntity> hddcVerticaldeformationPage = this.hddcVerticaldeformationNativeRepository.queryHddcVerticaldeformations(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcVerticaldeformationPage);
        return jsonObject;
    }


    @Override
    public HddcVerticaldeformationEntity getHddcVerticaldeformation(String id) {
        HddcVerticaldeformationEntity hddcVerticaldeformation = this.hddcVerticaldeformationRepository.findById(id).orElse(null);
         return hddcVerticaldeformation;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcVerticaldeformationEntity saveHddcVerticaldeformation(HddcVerticaldeformationEntity hddcVerticaldeformation) {
        String uuid = UUIDGenerator.getUUID();
        hddcVerticaldeformation.setUuid(uuid);
        hddcVerticaldeformation.setCreateUser(PlatformSessionUtils.getUserId());
        hddcVerticaldeformation.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcVerticaldeformationRepository.save(hddcVerticaldeformation);
        return hddcVerticaldeformation;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcVerticaldeformationEntity updateHddcVerticaldeformation(HddcVerticaldeformationEntity hddcVerticaldeformation) {
        HddcVerticaldeformationEntity entity = hddcVerticaldeformationRepository.findById(hddcVerticaldeformation.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcVerticaldeformation);
        hddcVerticaldeformation.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcVerticaldeformation.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcVerticaldeformationRepository.save(hddcVerticaldeformation);
        return hddcVerticaldeformation;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcVerticaldeformations(List<String> ids) {
        List<HddcVerticaldeformationEntity> hddcVerticaldeformationList = this.hddcVerticaldeformationRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcVerticaldeformationList) && hddcVerticaldeformationList.size() > 0) {
            for(HddcVerticaldeformationEntity hddcVerticaldeformation : hddcVerticaldeformationList) {
                this.hddcVerticaldeformationRepository.delete(hddcVerticaldeformation);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcVerticaldeformationQueryParams queryParams, HttpServletResponse response) {
        List<HddcVerticaldeformationEntity> yhDisasterEntities = hddcVerticaldeformationNativeRepository.exportYhDisasters(queryParams);
        List<HddcVerticaldeformationVO> list=new ArrayList<>();
        for (HddcVerticaldeformationEntity entity:yhDisasterEntities) {
            HddcVerticaldeformationVO yhDisasterVO=new HddcVerticaldeformationVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "垂直形变-线", "垂直形变-线", HddcVerticaldeformationVO.class, "垂直形变-线.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcVerticaldeformationVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcVerticaldeformationVO.class, params);
            List<HddcVerticaldeformationVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcVerticaldeformationVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcVerticaldeformationVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcVerticaldeformationVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcVerticaldeformationEntity yhDisasterEntity = new HddcVerticaldeformationEntity();
            HddcVerticaldeformationVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcVerticaldeformation(yhDisasterEntity);
        }
    }

}
