package com.css.zfzx.sjcj.modules.hddcB2GeomorphySvyProjectTable.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.bpm.platform.utils.PlatformPageUtils;
import com.css.zfzx.sjcj.modules.hddcB2GeomorphySvyProjectTable.repository.entity.HddcB2GeomorphysvyprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB2GeomorphySvyProjectTable.service.HddcB2GeomorphysvyprojecttableService;
import com.css.zfzx.sjcj.modules.hddcB2GeomorphySvyProjectTable.viewobjects.HddcB2GeomorphysvyprojecttableQueryParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-07
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcB2Geomorphysvyprojecttables")
public class HddcB2GeomorphysvyprojecttableController {
    @Autowired
    private HddcB2GeomorphysvyprojecttableService hddcB2GeomorphysvyprojecttableService;

    @GetMapping("/queryHddcB2Geomorphysvyprojecttables")
    public RestResponse queryHddcB2Geomorphysvyprojecttables(HttpServletRequest request, HddcB2GeomorphysvyprojecttableQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcB2GeomorphysvyprojecttableService.queryHddcB2Geomorphysvyprojecttables(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcB2Geomorphysvyprojecttable(@PathVariable String id) {
        RestResponse response = null;
        try{
            HddcB2GeomorphysvyprojecttableEntity hddcB2Geomorphysvyprojecttable = hddcB2GeomorphysvyprojecttableService.getHddcB2Geomorphysvyprojecttable(id);
            response = RestResponse.succeed(hddcB2Geomorphysvyprojecttable);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcB2Geomorphysvyprojecttable(@RequestBody HddcB2GeomorphysvyprojecttableEntity hddcB2Geomorphysvyprojecttable) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB2GeomorphysvyprojecttableService.saveHddcB2Geomorphysvyprojecttable(hddcB2Geomorphysvyprojecttable);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcB2Geomorphysvyprojecttable(@RequestBody HddcB2GeomorphysvyprojecttableEntity hddcB2Geomorphysvyprojecttable)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB2GeomorphysvyprojecttableService.updateHddcB2Geomorphysvyprojecttable(hddcB2Geomorphysvyprojecttable);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcB2Geomorphysvyprojecttables(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB2GeomorphysvyprojecttableService.deleteHddcB2Geomorphysvyprojecttables(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcB2GeomorphysvyprojecttableService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }
    /***
     * 导出
     * @param response
     * @return
     */
    @GetMapping("/exportFile")
    public RestResponse exportFileYhDisasters(HttpServletResponse response,
                                              @RequestParam("projectName")String projectName, @RequestParam("name") String name,
                                              @RequestParam("province") String province, @RequestParam("city")String city, @RequestParam("area")String area) {
        RestResponse responseRest = null;
        JSONObject jsonObject = new JSONObject();
        try{
            HddcB2GeomorphysvyprojecttableQueryParams queryParams=new HddcB2GeomorphysvyprojecttableQueryParams();
            queryParams.setArea(area);
            queryParams.setCity(city);
            queryParams.setProvince(province);
            queryParams.setProjectName(projectName);
            queryParams.setName(name);
            hddcB2GeomorphysvyprojecttableService.exportFile(queryParams,response);
            jsonObject.put("message", "导出成功");
            responseRest = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            responseRest = RestResponse.fail(errorMessage);
        }
        return responseRest;
    }
    /**
     * 导入
     *
     * @param file
     * @param response
     * @return
     */
    @PostMapping("/importDisaster")
    public RestResponse export(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        RestResponse restResponse = null;
        try {
            String s = hddcB2GeomorphysvyprojecttableService.exportExcel( file, response);
            restResponse = RestResponse.succeed(s);
        } catch (Exception e) {
            String errormessage = "导入失败";
            log.error(errormessage, e);
            restResponse = RestResponse.fail(errormessage);
        }
        return restResponse;
    }

}