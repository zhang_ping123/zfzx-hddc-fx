package com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.viewobjects;

import lombok.Data;
import java.util.Date;

/**
 * @author zhangping
 * @date 2020-11-23
 */
@Data
public class HddcC4MicrotremortableQueryParams {


    private String projectId;
    private String unit;
    private String createTime;
    private String instrument;

}
