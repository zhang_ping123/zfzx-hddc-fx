package com.css.zfzx.sjcj.modules.hddcRock5Pre.repository;

import com.css.zfzx.sjcj.modules.hddcRock5Pre.repository.entity.HddcRock5preEntity;
import com.css.zfzx.sjcj.modules.hddcRock5Pre.viewobjects.HddcRock5preQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author lhl
 * @date 2020-11-27
 */
public interface HddcRock5preNativeRepository {

    Page<HddcRock5preEntity> queryHddcRock5pres(HddcRock5preQueryParams queryParams, int curPage, int pageSize);

    List<HddcRock5preEntity> exportYhDisasters(HddcRock5preQueryParams queryParams);
}
