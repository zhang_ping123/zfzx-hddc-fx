package com.css.zfzx.sjcj.modules.hddcwyDrillHole.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-12-01
 */
@Data
public class HddcWyDrillholeVO implements Serializable {

    /**
     * 钻探地点
     */
    @Excel(name = "钻探地点", orderNum = "39")
    private String locationname;
    /**
     * 钻探日期
     */
    @Excel(name = "钻探日期", orderNum = "25")
    private String drilldate;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 钻孔孔深检查编号
     */
    @Excel(name = "钻孔孔深检查编号", orderNum = "46")
    private String depthcheckAiid;
    /**
     * 井斜测量结果登记表原始文件编号
     */
    @Excel(name = "井斜测量结果登记表原始文件编号", orderNum = "30")
    private String wellclinationArwid;
    /**
     * 封孔设计及封孔报告书原始文件编号
     */
    @Excel(name = "封孔设计及封孔报告书原始文件编号", orderNum = "52")
    private String sealdesignreportArwid;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 钻孔柱状图图像文件编号
     */
    @Excel(name = "钻孔柱状图图像文件编号", orderNum = "35")
    private String columnchartAiid;
    /**
     * 野外编号
     */
    @Excel(name = "野外编号", orderNum = "38")
    private String fieldid;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 孔位经度
     */
    @Excel(name = "孔位经度", orderNum = "6")
    private Double lon;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 钻孔班报原始文件编号
     */
    @Excel(name = "钻孔班报原始文件编号", orderNum = "51")
    private String drillinglogArwid;
    /**
     * 是否开展地球物理测井
     */
    @Excel(name = "是否开展地球物理测井", orderNum = "48")
    private Integer isgeophywell;
    /**
     * 中更新统厚度 [米]
     */
    @Excel(name = "中更新统厚度 [米]", orderNum = "20")
    private Double midpleithickness;
    /**
     * 项目名称
     */
    @Excel(name="项目名称",orderNum = "8")
    private String projectName;
    /**
     * 钻孔编号
     */
    @Excel(name = "钻孔编号", orderNum = "1")
    private String id;
    /**
     * 备注
     */
    private String remark;
    /**
     * 任务名称
     */
    @Excel(name="任务名称",orderNum = "9")
    private String taskName;
    /**
     * 孔深 [米]
     */
    @Excel(name = "孔深 [米]", orderNum = "40")
    private Double depth;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 项目ID
     */
    private String projectCode;
    /**
     * 采样记录表编号
     */
    @Excel(name = "采样记录表编号", orderNum = "10")
    private String geologysmplrecAiid;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 全新统厚度 [米]
     */
    @Excel(name = "全新统厚度 [米]", orderNum = "24")
    private Double holocenethickness;
    /**
     * 钻孔质量验收报告原始文件编号
     */
    @Excel(name = "钻孔质量验收报告原始文件编号", orderNum = "17")
    private String sealcheckArwid;
    /**
     * 工程编号
     */
    @Excel(name = "工程编号", orderNum = "31")
    private String projectId;
    /**
     * 采集环境与工程样品数
     */
    @Excel(name = "采集环境与工程样品数", orderNum = "42")
    private Integer collectedenviromentsamplecount;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 村
     */
    private String village;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 采集样品总数
     */
    @Excel(name = "采集样品总数", orderNum = "18")
    private Integer collectedsamplecount;
    /**
     * 前第四纪厚度 [米]
     */
    @Excel(name = "前第四纪厚度 [米]", orderNum = "47")
    private Double prepleithickness;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 岩芯总长 [米]
     */
    @Excel(name = "岩芯总长 [米]", orderNum = "34")
    private Double coretotalthickness;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 原始岩芯编录表原始
     */
    @Excel(name = "原始岩芯编录表原始", orderNum = "14")
    private String corecatalogArwid;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 孔口标高 [米]
     */
    @Excel(name = "孔口标高 [米]", orderNum = "33")
    private Double elevation;
    /**
     * 井斜测量结果登记表文件编号
     */
    @Excel(name = "井斜测量结果登记表文件编号", orderNum = "11")
    private String wellclinationAiid;
    /**
     * 获得结果样品总数
     */
    @Excel(name = "获得结果样品总数", orderNum = "21")
    private Integer datingsamplecount;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "3")
    private String city;
    /**
     * 钻孔质量验收报告文件编号
     */
    @Excel(name = "钻孔质量验收报告文件编号", orderNum = "16")
    private String sealcheckArid;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "4")
    private String area;
    /**
     * 环境与工程样品送样总数
     */
    @Excel(name = "环境与工程样品送样总数", orderNum = "44")
    private Integer enviromentsamplecount;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 钻孔描述
     */
    @Excel(name = "钻孔描述", orderNum = "27")
    private String commentInfo;
    /**
     * 乡
     */
    @Excel(name="详细地址",orderNum = "5")
    private String town;
    /**
     * 钻孔孔深检查原始文件编号
     */
    @Excel(name = "钻孔孔深检查原始文件编号", orderNum = "28")
    private String depthcheckArwid;
    /**
     * 孔位纬度
     */
    @Excel(name = "孔位纬度", orderNum = "7")
    private Double lat;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 上更新统厚度 [米]
     */
    @Excel(name = "上更新统厚度 [米]", orderNum = "45")
    private Double uppleithickness;
    /**
     * 钻探目的
     */
    @Excel(name = "钻探目的", orderNum = "23")
    private String purpose;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 钻孔剖面编号
     */
    @Excel(name = "钻孔剖面编号", orderNum = "49")
    private String profileid;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 钻孔班报编号
     */
    @Excel(name = "钻孔班报编号", orderNum = "43")
    private String drillinglogAiid;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 下更新统厚度 [米]
     */
    @Excel(name = "下更新统厚度 [米]", orderNum = "32")
    private Double lowpleithickness;
    /**
     * 简易水文观测记录表原始
     */
    @Excel(name = "简易水文观测记录表原始", orderNum = "15")
    private String hydrorecordArwid;
    /**
     * 原始岩芯编录表图像文件编号
     */
    @Excel(name = "原始岩芯编录表图像文件编号", orderNum = "22")
    private String corecatalogAiid;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "2")
    private String province;
    /**
     * 钻孔柱状图原始档案编号
     */
    @Excel(name = "钻孔柱状图原始档案编号", orderNum = "37")
    private String columnchartArwid;
    /**
     * 封孔设计及封孔报告书文件编号
     */
    @Excel(name = "封孔设计及封孔报告书文件编号", orderNum = "13")
    private String sealdesignreportArid;
    /**
     * 岩芯照片图像档案编号
     */
    @Excel(name = "岩芯照片图像档案编号", orderNum = "50")
    private String corephotoAiid;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 钻孔来源与类型
     */
    @Excel(name = "钻孔来源与类型", orderNum = "19")
    private Integer drillsource;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 采样记录表原始文件编号
     */
    @Excel(name = "采样记录表原始文件编号", orderNum = "36")
    private String geologysmplrecArwid;
    /**
     * 简易水文观测记录表
     */
    @Excel(name = "简易水文观测记录表", orderNum = "29")
    private String hydrorecordAiid;
    /**
     * 获得测试结果的环境与工程样品数
     */
    @Excel(name = "获得测试结果的环境与工程样品数", orderNum = "41")
    private Integer testedenviromentsamplecount;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 送样总数
     */
    @Excel(name = "送样总数", orderNum = "12")
    private Integer samplecount;
    /**
     * 岩芯照片原始档案编号
     */
    @Excel(name = "岩芯照片原始档案编号", orderNum = "26")
    private String corephotoArwid;
    /**
     * 审查意见
     */
    private String examineComments;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String rowNum;
    private String errorMsg;
}