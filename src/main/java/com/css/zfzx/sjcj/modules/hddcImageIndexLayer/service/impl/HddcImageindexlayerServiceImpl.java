package com.css.zfzx.sjcj.modules.hddcImageIndexLayer.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcImageIndexLayer.repository.HddcImageindexlayerNativeRepository;
import com.css.zfzx.sjcj.modules.hddcImageIndexLayer.repository.HddcImageindexlayerRepository;
import com.css.zfzx.sjcj.modules.hddcImageIndexLayer.repository.entity.HddcImageindexlayerEntity;
import com.css.zfzx.sjcj.modules.hddcImageIndexLayer.service.HddcImageindexlayerService;
import com.css.zfzx.sjcj.modules.hddcImageIndexLayer.viewobjects.HddcImageindexlayerQueryParams;
import com.css.zfzx.sjcj.modules.hddcImageIndexLayer.viewobjects.HddcImageindexlayerVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zhangping
 * @date 2020-11-30
 */
@Service
public class HddcImageindexlayerServiceImpl implements HddcImageindexlayerService {

	@Autowired
    private HddcImageindexlayerRepository hddcImageindexlayerRepository;
    @Autowired
    private HddcImageindexlayerNativeRepository hddcImageindexlayerNativeRepository;

    @Override
    public JSONObject queryHddcImageindexlayers(HddcImageindexlayerQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcImageindexlayerEntity> hddcImageindexlayerPage = this.hddcImageindexlayerNativeRepository.queryHddcImageindexlayers(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcImageindexlayerPage);
        return jsonObject;
    }


    @Override
    public HddcImageindexlayerEntity getHddcImageindexlayer(String id) {
        HddcImageindexlayerEntity hddcImageindexlayer = this.hddcImageindexlayerRepository.findById(id).orElse(null);
         return hddcImageindexlayer;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcImageindexlayerEntity saveHddcImageindexlayer(HddcImageindexlayerEntity hddcImageindexlayer) {
        String uuid = UUIDGenerator.getUUID();
        hddcImageindexlayer.setUuid(uuid);
        hddcImageindexlayer.setCreateUser(PlatformSessionUtils.getUserId());
        hddcImageindexlayer.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcImageindexlayerRepository.save(hddcImageindexlayer);
        return hddcImageindexlayer;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcImageindexlayerEntity updateHddcImageindexlayer(HddcImageindexlayerEntity hddcImageindexlayer) {
        HddcImageindexlayerEntity entity = hddcImageindexlayerRepository.findById(hddcImageindexlayer.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcImageindexlayer);
        hddcImageindexlayer.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcImageindexlayer.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcImageindexlayerRepository.save(hddcImageindexlayer);
        return hddcImageindexlayer;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcImageindexlayers(List<String> ids) {
        List<HddcImageindexlayerEntity> hddcImageindexlayerList = this.hddcImageindexlayerRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcImageindexlayerList) && hddcImageindexlayerList.size() > 0) {
            for(HddcImageindexlayerEntity hddcImageindexlayer : hddcImageindexlayerList) {
                this.hddcImageindexlayerRepository.delete(hddcImageindexlayer);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcImageindexlayerQueryParams queryParams, HttpServletResponse response) {
        List<HddcImageindexlayerEntity> yhDisasterEntities = hddcImageindexlayerNativeRepository.exportYhDisasters(queryParams);
        List<HddcImageindexlayerVO> list=new ArrayList<>();
        for (HddcImageindexlayerEntity entity:yhDisasterEntities) {
            HddcImageindexlayerVO yhDisasterVO=new HddcImageindexlayerVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "影像索引表-面", "影像索引表-面", HddcImageindexlayerVO.class, "影像索引表-面.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcImageindexlayerVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcImageindexlayerVO.class, params);
            List<HddcImageindexlayerVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcImageindexlayerVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcImageindexlayerVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcImageindexlayerVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcImageindexlayerEntity yhDisasterEntity = new HddcImageindexlayerEntity();
            HddcImageindexlayerVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcImageindexlayer(yhDisasterEntity);
        }
    }

}
