package com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyLine.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyLine.repository.entity.HddcWyGeologicalsvylineEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyLine.viewobjects.HddcWyGeologicalsvylineQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */

public interface HddcWyGeologicalsvylineService {

    public JSONObject queryHddcWyGeologicalsvylines(HddcWyGeologicalsvylineQueryParams queryParams, int curPage, int pageSize);

    public HddcWyGeologicalsvylineEntity getHddcWyGeologicalsvyline(String uuid);

    public HddcWyGeologicalsvylineEntity saveHddcWyGeologicalsvyline(HddcWyGeologicalsvylineEntity hddcWyGeologicalsvyline);

    public HddcWyGeologicalsvylineEntity updateHddcWyGeologicalsvyline(HddcWyGeologicalsvylineEntity hddcWyGeologicalsvyline);

    public void deleteHddcWyGeologicalsvylines(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    BigInteger queryHddcWyGeologicalsvyline(HddcAppZztCountVo queryParams);

    List<HddcWyGeologicalsvylineEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);

    List<HddcWyGeologicalsvylineEntity> findAllByCreateUserAndIsValid(String userId, String isValid);


    /**
     * 逻辑删除-根据项目id删除数据
     * @param ids
     */
    void deleteByProjectId(List<String> ids);

    /**
     * 逻辑删除-根据任务id删除数据
     * @param ids
     */
    void deleteByTaskId(List<String> ids);

}
