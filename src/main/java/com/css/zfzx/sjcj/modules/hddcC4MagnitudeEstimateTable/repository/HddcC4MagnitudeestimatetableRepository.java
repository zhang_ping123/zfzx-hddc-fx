package com.css.zfzx.sjcj.modules.hddcC4MagnitudeEstimateTable.repository;

import com.css.zfzx.sjcj.modules.hddcC4MagnitudeEstimateTable.repository.entity.HddcC4MagnitudeestimatetableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangping
 * @date 2020-11-23
 */
public interface HddcC4MagnitudeestimatetableRepository extends JpaRepository<HddcC4MagnitudeestimatetableEntity, String> {
}
