package com.css.zfzx.sjcj.modules.analysis.service;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;

import java.util.Map;

public interface ZkAndYbService {

    Map<String,Object> hddcZkAndYbNumData(HddcAppZztCountVo queryParams);
}
