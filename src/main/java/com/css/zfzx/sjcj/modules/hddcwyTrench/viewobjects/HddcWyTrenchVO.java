package com.css.zfzx.sjcj.modules.hddcwyTrench.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Date;

/**
 * @author zyb
 * @date 2020-12-02
 */
@Data
public class HddcWyTrenchVO implements Serializable {

    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 探槽来源与类型
     */
    @Excel(name="探槽来源与类型",orderNum = "19")
    private Integer trenchsource;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 探槽名称
     */
    @Excel(name="探槽名称",orderNum = "30")
    private String name;
    /**
     * 探槽方向角度 [度]
     */
    @Excel(name="探槽方向角度 [度]",orderNum = "44")
    private Integer trenchdip;
    /**
     * 照片镜向
     */
    @Excel(name="照片镜向",orderNum = "21")
    private Integer photoviewingto;
    /**
     * 探槽长 [米]
     */
    @Excel(name="探槽长 [米]",orderNum = "36")
    private Double length;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 探槽深 [米]
     */
    @Excel(name = "探槽深 [米]",orderNum = "17")
    private Double depth;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 环境照片原始文件编号
     */
    @Excel(name="环境照片原始文件编号",orderNum = "26")
    private String photoArwid;
    /**
     * 探槽剖面图2拼接照片图像文件编号
     */
    @Excel(name="探槽剖面图2拼接照片图像文件编号",orderNum = "32")
    private String profile2photoAiid;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 揭露地层数
     */
    @Excel(name="揭露地层数",orderNum = "33")
    private Integer exposedstratumcount;
    /**
     * 最晚古地震发震时代
     */
    @Excel(name="最晚古地震发震时代",orderNum = "39")
    private Integer latesteqperoidest;
    /**
     * 探槽剖面地层描述1原始文件编号
     */
    @Excel(name="探槽剖面地层描述1原始文件编号",orderNum = "34")
    private String profile1commentArwid;
    /**
     * 环境照片图像编号
     */
    @Excel(name="环境照片图像编号",orderNum = "25")
    private String photoAiid;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 村
     */
    private String village;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 最晚古地震发震时代误差
     */
    @Excel(name = "最晚古地震发震时代误差",orderNum = "37")
    private Integer latesteqperoider;
    /**
     * 市
     */
    @Excel(name="市",orderNum = "3")
    private String city;
    /**
     * 拍摄者
     */
    @Excel(name="拍摄者",orderNum = "14")
    private String photographer;
    /**
     * 探槽描述
     */
    @Excel(name="探槽描述",orderNum = "21")
    private String commentInfo;
    /**
     * 纬度
     */
    @Excel(name="纬度",orderNum = "7")
    private Double lat;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 探槽剖面图1图像文件编号
     */
    @Excel(name="探槽剖面图1图像文件编号",orderNum = "12")
    private String profile1Aiid;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 探槽剖面地层描述2文件编号
     */
    @Excel(name="探槽剖面地层描述2文件编号",orderNum = "42")
    private String profile2commentArid;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 区（县）
     */
    @Excel(name="区（县）",orderNum = "4")
    private String area;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 目标断层来源
     */
    @Excel(name="目标断层来源",orderNum = "46")
    private String targetfaultsource;
    /**
     * 目标断层编号
     */
    @Excel(name="目标断层编号",orderNum = "10")
    private String targetfaultid;
    /**
     * 探槽剖面图2原始文件编号
     */
    @Excel(name="探槽剖面图2原始文件编号",orderNum = "35")
    private String profile2Arwid;
    /**
     * 探槽剖面图1拼接照片图像文件编号
     */
    @Excel(name="探槽剖面图1拼接照片图像文件编号",orderNum = "20")
    private String profile1photoAiid;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 探槽剖面图1照片原始文件编号
     */
    @Excel(name="探槽剖面图1照片原始文件编号",orderNum = "40")
    private String profile1photoArwid;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 探槽剖面地层描述2原始文件编号
     */
    @Excel(name="探槽剖面地层描述2原始文件编号",orderNum = "47")
    private String profile2commentArwid;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 探槽宽 [米]
     */
    @Excel(name = "探槽宽 [米]",orderNum = "29")
    private Double width;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 探槽剖面图1原始文件编号
     */
    @Excel(name="探槽剖面图1原始文件编号",orderNum = "27")
    private String profile1Arwid;
    /**
     * 所属地质调查工程编号
     */
    @Excel(name="所属地质调查工程编号",orderNum = "38")
    private String geologysvyprojectid;
    /**
     * 目标断层名称
     */
    @Excel(name="目标断层名称",orderNum = "41")
    private String targetfaultname;
    /**
     * 高程 [米]
     */
    @Excel(name="高程 [米]",orderNum = "43")
    private Double elevation;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 乡
     */
    @Excel(name="详细地址",orderNum = "5")
    private String town;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 探槽剖面地层描述1文件编号
     */
    @Excel(name="探槽剖面地层描述1文件编号",orderNum = "24")
    private String profile1commentArid;
    /**
     * 地貌环境
     */
    @Excel(name="地貌环境",orderNum = "31")
    private String geomophenv;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 探槽剖面图2照片原始文件编号
     */
    @Excel(name="探槽剖面图2照片原始文件编号",orderNum = "45")
    private String profile2photoArwid;
    /**
     * 送样总数
     */
    @Excel(name="送样总数",orderNum = "13")
    private Integer samplecount;
    /**
     * 参考位置
     */
    @Excel(name="参考位置",orderNum = "15")
    private String locationname;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 获得结果的样品数
     */
    @Excel(name = "获得结果的样品数",orderNum = "28")
    private Integer datingsamplecount;
    /**
     * 项目名称
     */
    @Excel(name="项目名称",orderNum = "8")
    private String projectName;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 任务名称
     */
    @Excel(name="任务名称",orderNum = "9")
    private String taskName;
    /**
     * 探槽剖面图2图像文件编号
     */
    @Excel(name="探槽剖面图2图像文件编号",orderNum = "16")
    private String profile2Aiid;
    /**
     * 经度
     */
    @Excel(name="经度",orderNum = "6")
    private Double lon;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 收集探槽来源补充说明
     */
    @Excel(name="收集探槽来源补充说明",orderNum = "11")
    private String collectedtrenchsource;
    /**
     * 探槽编号
     */
    @Excel(name="探槽编号",orderNum = "1")
    private String id;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 野外编号
     */
    @Excel(name="野外编号",orderNum = "22")
    private String fieldid;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 采集样品总数
     */
    @Excel(name = "采集样品总数",orderNum = "23")
    private Integer collectedsamplecount;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 省
     */
    @Excel(name="省",orderNum = "2")
    private String province;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 符号或标注旋转角度
     */
    @Excel(name="符号或标注旋转角度",orderNum = "18")
    private Double lastangle;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 古地震事件次数
     */
    @Excel(name = "古地震事件次数",orderNum = "48")
    private Integer eqeventcount;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String rowNum;
    private String errorMsg;
}