package com.css.zfzx.sjcj.modules.hddcISCatalog.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Data
public class HddcIscatalogVO implements Serializable {

    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 地名
     */
    @Excel(name = "地名", orderNum = "5")
    private String locationname;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 符号或标注旋转角度
     */
    @Excel(name = "符号或标注旋转角度", orderNum = "6")
    private Double lastangle;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 经度
     */
    @Excel(name = "经度", orderNum = "7")
    private Double lon;
    /**
     * 震级 [Ml]
     */
    @Excel(name = "震级 [Ml]", orderNum = "8")
    private Double magnitudeml;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 震级 [Ms]（大于4.7必填）
     */
    @Excel(name = "震级 [Ms]（大于4.7必填）", orderNum = "9")
    private String magnitudems;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 乡
     */
    private String town;
    /**
     * 震源深度 [公里]
     */
    @Excel(name = "震源深度 [公里]", orderNum = "10")
    private Double depth;
    /**
     * 地震编号
     */
    private String id;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 日期
     */
    @Excel(name = "日期", orderNum = "11")
    private String occurrencedate;
    /**
     * 纬度
     */
    @Excel(name = "纬度", orderNum = "12")
    private Double lat;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "13")
    private String commentInfo;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 宏观震中烈度
     */
    @Excel(name = "宏观震中烈度", orderNum = "14")
    private Integer epicenter;
    /**
     * 时间
     */
    @Excel(name = "时间", orderNum = "4")
    private String occurrencetime;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String rowNum;
    private String errorMsg;
}