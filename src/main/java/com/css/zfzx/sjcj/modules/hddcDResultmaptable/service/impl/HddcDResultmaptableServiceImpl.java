package com.css.zfzx.sjcj.modules.hddcDResultmaptable.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.CheckObjFields;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcDResultmaptable.repository.HddcDResultmaptableNativeRepository;
import com.css.zfzx.sjcj.modules.hddcDResultmaptable.repository.HddcDResultmaptableRepository;
import com.css.zfzx.sjcj.modules.hddcDResultmaptable.repository.entity.HddcDResultmaptableEntity;
import com.css.zfzx.sjcj.modules.hddcDResultmaptable.service.HddcDResultmaptableService;
import com.css.zfzx.sjcj.modules.hddcDResultmaptable.viewobjects.HddcDResultmaptableQueryParams;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-19
 */
@Service
@Log4j
public class HddcDResultmaptableServiceImpl implements HddcDResultmaptableService {

    @Value("${file-upload.attachment-directory.config.defaultDirectory.fileDirectory}")
    private String localPath;
    @Autowired
    private HddcDResultmaptableRepository hddcDResultmaptableRepository;
    @Autowired
    private HddcDResultmaptableNativeRepository hddcDResultmaptableNativeRepository;

    @Override
    public JSONObject queryHddcDResultmaptables(HddcDResultmaptableQueryParams queryParams, int curPage, int pageSize,String sort,String order) {
        if (!PlatformObjectUtils.isEmpty(sort) && sort.length() > 0){
            sort= CheckObjFields.propertyChange(sort);
        }
        Page<HddcDResultmaptableEntity> hddcDResultmaptablePage = this.hddcDResultmaptableNativeRepository.queryHddcDResultmaptables(queryParams, curPage, pageSize,sort,order);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcDResultmaptablePage);
        return jsonObject;
    }


    @Override
    public HddcDResultmaptableEntity getHddcDResultmaptable(String id) {
        HddcDResultmaptableEntity hddcDResultmaptable = this.hddcDResultmaptableRepository.findById(id).orElse(null);
        return hddcDResultmaptable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcDResultmaptableEntity saveHddcDResultmaptable(HddcDResultmaptableEntity hddcDResultmaptable) {
        String uuid = UUIDGenerator.getUUID();
        hddcDResultmaptable.setUuid(uuid);
        hddcDResultmaptable.setCreateUser(PlatformSessionUtils.getUserId());
        hddcDResultmaptable.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        hddcDResultmaptable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        hddcDResultmaptable.setIsValid("1");
        this.hddcDResultmaptableRepository.save(hddcDResultmaptable);
        return hddcDResultmaptable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcDResultmaptableEntity updateHddcDResultmaptable(HddcDResultmaptableEntity hddcDResultmaptable) {
        HddcDResultmaptableEntity entity = hddcDResultmaptableRepository.findById(hddcDResultmaptable.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcDResultmaptable);
        hddcDResultmaptable.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcDResultmaptable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcDResultmaptableRepository.save(hddcDResultmaptable);
        return hddcDResultmaptable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcDResultmaptables(List<String> ids) {
        List<HddcDResultmaptableEntity> hddcDResultmaptableList = this.hddcDResultmaptableRepository.findAllById(ids);
        if (!PlatformObjectUtils.isEmpty(hddcDResultmaptableList) && hddcDResultmaptableList.size() > 0) {
            for (HddcDResultmaptableEntity hddcDResultmaptable : hddcDResultmaptableList) {
//                this.hddcDResultmaptableRepository.delete(hddcDResultmaptable);
                hddcDResultmaptable.setIsValid("0");
                hddcDResultmaptable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcDResultmaptable.setUpdateTime(new Date());
                this.hddcDResultmaptableRepository.save(hddcDResultmaptable);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    /**
     * 上传附件
     * @param file
     * @param filenumber
     * @return
     */
//    @Override
//    public int upload(MultipartFile file, String filenumber) {
//        List<HddcDResultmaptableEntity> hddcDResultmaptableEntity=hddcDResultmaptableRepository.findbyId(filenumber);
//        int count=0;
//        if (!hddcFileManageEntity.isEmpty()&&hddcFileManageEntity.size()!=0){
//            return count;//该编号已存在
//        }
//        try {
//            String path = localPath + "/file";//上传的路径
//            File dir = new File(path);
//            if (!dir.exists()) {
//                //路径不存在就创建路径
//                dir.mkdir();
//            }
//            String fileName = file.getOriginalFilename();
//            File file1 = new File(path, fileName);
//            file.transferTo(file1);
//            addOrUpdateHddcFileManage(fileName,filenumber);
//            //addOrUpdateHddcFileManage(fileName.substring(0,fileName.lastIndexOf('.')),filenumber);
//            count=2;//上传成功
//        } catch (IOException e) {
//            //throw new Exception(e.getMessage());
//            log.error(e.getMessage());
//            count=1;//上传失败
//        }
//        return count;
//    }
//
//    private void addOrUpdateHddcFileManage(String name,String filenumber) {
//        HddcFileManageEntity hddcFileManageEntity = new HddcFileManageEntity();
//
//        hddcFileManageEntity.setFileName(name);
//        hddcFileManageEntity.setFileId(filenumber);
//        this.saveHddcDResultmaptable(hddcFileManageEntity);
//        /*HddcFileManageQueryParams params = new HddcFileManageQueryParams();
//        params.setFileName(name);
//        params.setFileId(filenumber);
//
//        Page<HddcFileManageEntity> hddcFileManagePage = this.hddcFileManageNativeRepository.queryHddcFileManages(params, 1, 10);
//
//        HddcFileManageEntity hddcFileManageEntity = new HddcFileManageEntity();
//        hddcFileManageEntity.setFileName(name);
//        hddcFileManageEntity.setFileId(filenumber);
//
//        //update add
//        if(hddcFileManagePage.getContent().size() > 0) {
//            hddcFileManageEntity = hddcFileManagePage.getContent().get(0);
//            this.updateHddcFileManage(hddcFileManageEntity);
//        } else {
//            this.saveHddcFileManage(hddcFileManageEntity);
//        }*/
//    }

}
