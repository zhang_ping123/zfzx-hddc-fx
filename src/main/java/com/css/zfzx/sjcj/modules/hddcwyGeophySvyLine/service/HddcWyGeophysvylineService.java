package com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.repository.entity.HddcWyGeophysvylineEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.viewobjects.HddcWyGeophysvylineQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */

public interface HddcWyGeophysvylineService {

    public JSONObject queryHddcWyGeophysvylines(HddcWyGeophysvylineQueryParams queryParams, int curPage, int pageSize);

    public HddcWyGeophysvylineEntity getHddcWyGeophysvyline(String uuid);

    public HddcWyGeophysvylineEntity saveHddcWyGeophysvyline(HddcWyGeophysvylineEntity hddcWyGeophysvyline);

    public HddcWyGeophysvylineEntity updateHddcWyGeophysvyline(HddcWyGeophysvylineEntity hddcWyGeophysvyline);

    public void deleteHddcWyGeophysvylines(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    BigInteger queryHddcWyGeophysvyline(HddcAppZztCountVo queryParams);

    List<HddcWyGeophysvylineEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);

    List<HddcWyGeophysvylineEntity> findAllByCreateUserAndIsValid(String userId, String isValid);

    /**
     * 逻辑删除-根据项目id删除数据
     * @param ids
     */
    void deleteByProjectId(List<String> ids);

    /**
     * 逻辑删除-根据任务id删除数据
     * @param ids
     */
    void deleteByTaskId(List<String> ids);
}
