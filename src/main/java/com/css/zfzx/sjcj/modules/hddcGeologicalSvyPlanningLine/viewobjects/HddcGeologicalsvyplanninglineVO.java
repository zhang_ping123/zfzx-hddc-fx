package com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningLine.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-12-07
 */
@Data
public class HddcGeologicalsvyplanninglineVO implements Serializable {

    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 编号
     */
    private String id;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备注
     */
    private String remark;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 规划路线名称
     */
    @Excel(name = "规划路线名称", orderNum = "4")
    private String svylinename;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备注
     */
    private String commentInfo;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 观测方法
     */
    @Excel(name = "观测方法", orderNum = "6")
    private String svymethods;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 村
     */
    private String village;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 观测目的
     */
    @Excel(name = "观测目的", orderNum = "5")
    private String purpose;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String projectNameName;
    private String taskNameName;
    private String rowNum;
    private String errorMsg;
}