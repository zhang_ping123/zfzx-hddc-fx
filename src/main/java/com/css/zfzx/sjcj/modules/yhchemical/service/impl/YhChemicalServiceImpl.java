package com.css.zfzx.sjcj.modules.yhchemical.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.yhchemical.repository.YhChemicalNativeRepository;
import com.css.zfzx.sjcj.modules.yhchemical.repository.YhChemicalRepository;
import com.css.zfzx.sjcj.modules.yhchemical.repository.entity.YhChemicalEntity;
import com.css.zfzx.sjcj.modules.yhchemical.service.YhChemicalService;
import com.css.zfzx.sjcj.modules.yhchemical.viewobjects.YhChemicalQueryParams;
import com.css.zfzx.sjcj.modules.yhchemical.viewobjects.YhChemicalVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author ly
 * @date 2020-11-04
 */
@Service
public class YhChemicalServiceImpl implements YhChemicalService {

	@Autowired
    private YhChemicalRepository yhChemicalRepository;
    @Autowired
    private YhChemicalNativeRepository yhChemicalNativeRepository;

    @Override
    public JSONObject queryYhChemicals(YhChemicalQueryParams queryParams, int curPage, int pageSize) {
        Page<YhChemicalEntity> yhChemicalPage = this.yhChemicalNativeRepository.queryYhChemicals(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(yhChemicalPage);
        return jsonObject;
    }


    @Override
    public YhChemicalEntity getYhChemical(String id) {
        YhChemicalEntity yhChemical = this.yhChemicalRepository.findById(id).orElse(null);
         return yhChemical;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public YhChemicalEntity saveYhChemical(YhChemicalEntity yhChemical) {
        String uuid = UUIDGenerator.getUUID();
        yhChemical.setId(uuid);
        yhChemical.setCreateUser(PlatformSessionUtils.getUserId());
        yhChemical.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.yhChemicalRepository.save(yhChemical);
        return yhChemical;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public YhChemicalEntity updateYhChemical(YhChemicalEntity yhChemical) {
        YhChemicalEntity entity = yhChemicalRepository.findById(yhChemical.getId()).get();
        UpdateUtil.copyNullProperties(entity,yhChemical);
        yhChemical.setUpdateUser(PlatformSessionUtils.getUserId());
        yhChemical.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.yhChemicalRepository.save(yhChemical);
        return yhChemical;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteYhChemicals(List<String> ids) {
        List<YhChemicalEntity> yhChemicalList = this.yhChemicalRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(yhChemicalList) && yhChemicalList.size() > 0) {
            for(YhChemicalEntity yhChemical : yhChemicalList) {
                this.yhChemicalRepository.delete(yhChemical);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

}
