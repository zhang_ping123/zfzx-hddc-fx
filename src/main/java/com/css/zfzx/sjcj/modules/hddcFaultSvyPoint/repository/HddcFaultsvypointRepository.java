package com.css.zfzx.sjcj.modules.hddcFaultSvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcFaultSvyPoint.repository.entity.HddcFaultsvypointEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author lihelei
 * @date 2020-11-26
 */
public interface HddcFaultsvypointRepository extends JpaRepository<HddcFaultsvypointEntity, String> {
}
