package com.css.zfzx.sjcj.modules.hddcStratigraphySvyPoint.viewobjects;

import lombok.Data;

/**
 * @author lihelei
 * @date 2020-11-27
 */
@Data
public class HddcStratigraphysvypointQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String stratigraphyname;

}
