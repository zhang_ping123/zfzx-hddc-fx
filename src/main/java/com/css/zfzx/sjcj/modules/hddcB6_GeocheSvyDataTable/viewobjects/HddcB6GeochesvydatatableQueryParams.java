package com.css.zfzx.sjcj.modules.hddcB6_GeocheSvyDataTable.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
@Data
public class HddcB6GeochesvydatatableQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String resultname;

}
