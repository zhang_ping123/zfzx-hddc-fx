package com.css.zfzx.sjcj.modules.hddcB5_GeophySvyDataTable.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyDataTable.repository.HddcB5GeophysvydatatableNativeRepository;
import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyDataTable.repository.HddcB5GeophysvydatatableRepository;
import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyDataTable.repository.entity.HddcB5GeophysvydatatableEntity;
import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyDataTable.service.HddcB5GeophysvydatatableService;
import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyDataTable.viewobjects.HddcB5GeophysvydatatableQueryParams;
import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyDataTable.viewobjects.HddcB5GeophysvydatatableVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Service
public class HddcB5GeophysvydatatableServiceImpl implements HddcB5GeophysvydatatableService {

	@Autowired
    private HddcB5GeophysvydatatableRepository hddcB5GeophysvydatatableRepository;
    @Autowired
    private HddcB5GeophysvydatatableNativeRepository hddcB5GeophysvydatatableNativeRepository;

    @Override
    public JSONObject queryHddcB5Geophysvydatatables(HddcB5GeophysvydatatableQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcB5GeophysvydatatableEntity> hddcB5GeophysvydatatablePage = this.hddcB5GeophysvydatatableNativeRepository.queryHddcB5Geophysvydatatables(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcB5GeophysvydatatablePage);
        return jsonObject;
    }


    @Override
    public HddcB5GeophysvydatatableEntity getHddcB5Geophysvydatatable(String id) {
        HddcB5GeophysvydatatableEntity hddcB5Geophysvydatatable = this.hddcB5GeophysvydatatableRepository.findById(id).orElse(null);
         return hddcB5Geophysvydatatable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB5GeophysvydatatableEntity saveHddcB5Geophysvydatatable(HddcB5GeophysvydatatableEntity hddcB5Geophysvydatatable) {
        String uuid = UUIDGenerator.getUUID();
        hddcB5Geophysvydatatable.setUuid(uuid);
        hddcB5Geophysvydatatable.setCreateUser(PlatformSessionUtils.getUserId());
        hddcB5Geophysvydatatable.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB5GeophysvydatatableRepository.save(hddcB5Geophysvydatatable);
        return hddcB5Geophysvydatatable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB5GeophysvydatatableEntity updateHddcB5Geophysvydatatable(HddcB5GeophysvydatatableEntity hddcB5Geophysvydatatable) {
        HddcB5GeophysvydatatableEntity entity = hddcB5GeophysvydatatableRepository.findById(hddcB5Geophysvydatatable.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcB5Geophysvydatatable);
        hddcB5Geophysvydatatable.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcB5Geophysvydatatable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB5GeophysvydatatableRepository.save(hddcB5Geophysvydatatable);
        return hddcB5Geophysvydatatable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcB5Geophysvydatatables(List<String> ids) {
        List<HddcB5GeophysvydatatableEntity> hddcB5GeophysvydatatableList = this.hddcB5GeophysvydatatableRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcB5GeophysvydatatableList) && hddcB5GeophysvydatatableList.size() > 0) {
            for(HddcB5GeophysvydatatableEntity hddcB5Geophysvydatatable : hddcB5GeophysvydatatableList) {
                this.hddcB5GeophysvydatatableRepository.delete(hddcB5Geophysvydatatable);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcB5GeophysvydatatableQueryParams queryParams, HttpServletResponse response) {
        List<HddcB5GeophysvydatatableEntity> yhDisasterEntities = hddcB5GeophysvydatatableNativeRepository.exportYhDisasters(queryParams);
        List<HddcB5GeophysvydatatableVO> list=new ArrayList<>();
        for (HddcB5GeophysvydatatableEntity entity:yhDisasterEntities) {
            HddcB5GeophysvydatatableVO yhDisasterVO=new HddcB5GeophysvydatatableVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "地球物理探测数据表", "地球物理探测数据表", HddcB5GeophysvydatatableVO.class, "地球物理探测数据表.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcB5GeophysvydatatableVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcB5GeophysvydatatableVO.class, params);
            List<HddcB5GeophysvydatatableVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcB5GeophysvydatatableVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcB5GeophysvydatatableVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcB5GeophysvydatatableVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcB5GeophysvydatatableEntity yhDisasterEntity = new HddcB5GeophysvydatatableEntity();
            HddcB5GeophysvydatatableVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcB5Geophysvydatatable(yhDisasterEntity);
        }
    }

}
