package com.css.zfzx.sjcj.modules.hddcwyStratigraphySvyPoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyStratigraphySvyPoint.repository.entity.HddcWyStratigraphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyStratigraphySvyPoint.viewobjects.HddcWyStratigraphysvypointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */

public interface HddcWyStratigraphysvypointService {

    public JSONObject queryHddcWyStratigraphysvypoints(HddcWyStratigraphysvypointQueryParams queryParams, int curPage, int pageSize);

    public HddcWyStratigraphysvypointEntity getHddcWyStratigraphysvypoint(String uuid);

    public HddcWyStratigraphysvypointEntity saveHddcWyStratigraphysvypoint(HddcWyStratigraphysvypointEntity hddcWyStratigraphysvypoint);

    public HddcWyStratigraphysvypointEntity updateHddcWyStratigraphysvypoint(HddcWyStratigraphysvypointEntity hddcWyStratigraphysvypoint);

    public void deleteHddcWyStratigraphysvypoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    BigInteger queryHddcWyStratigraphysvypoint(HddcAppZztCountVo queryParams);

    List<HddcWyStratigraphysvypointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);

    List<HddcWyStratigraphysvypointEntity> findAllByCreateUserAndIsValid(String userId, String isValid);


    /**
     * 逻辑删除-根据项目id删除数据
     * @param ids
     */
    void deleteByProjectId(List<String> ids);

    /**
     * 逻辑删除-根据任务id删除数据
     * @param ids
     */
    void deleteByTaskId(List<String> ids);
}
