package com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningLine.repository;

import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningLine.repository.entity.HddcGeologicalsvyplanninglineEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-12-07
 */
public interface HddcGeologicalsvyplanninglineRepository extends JpaRepository<HddcGeologicalsvyplanninglineEntity, String> {
}
