package com.css.zfzx.sjcj.modules.hddcwyStratigraphySvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyStratigraphySvyPoint.repository.entity.HddcWyStratigraphysvypointEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */
public interface HddcWyStratigraphysvypointRepository extends JpaRepository<HddcWyStratigraphysvypointEntity, String> {

    List<HddcWyStratigraphysvypointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    List<HddcWyStratigraphysvypointEntity> findAllByCreateUserAndIsValid(String userId, String isValid);


    @Query(nativeQuery = true, value = "select * from hddc_wy_stratigraphysvypoint where is_valid!=0 project_name in :projectIds")
    List<HddcWyStratigraphysvypointEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_stratigraphysvypoint where task_name in :projectIds")
    List<HddcWyStratigraphysvypointEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);

}
