package com.css.zfzx.sjcj.modules.hddcRock25Pre.repository;

import com.css.zfzx.sjcj.modules.hddcRock25Pre.repository.entity.HddcRock25preEntity;
import com.css.zfzx.sjcj.modules.hddcRock25Pre.viewobjects.HddcRock25preQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcRock25preNativeRepository {

    Page<HddcRock25preEntity> queryHddcRock25pres(HddcRock25preQueryParams queryParams, int curPage, int pageSize);

    List<HddcRock25preEntity> exportYhDisasters(HddcRock25preQueryParams queryParams);
}
