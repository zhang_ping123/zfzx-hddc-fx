package com.css.zfzx.sjcj.modules.hddcGeomorphySvyReProf.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyReProf.repository.HddcGeomorphysvyreprofNativeRepository;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyReProf.repository.HddcGeomorphysvyreprofRepository;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyReProf.repository.entity.HddcGeomorphysvyreprofEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyReProf.service.HddcGeomorphysvyreprofService;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyReProf.viewobjects.HddcGeomorphysvyreprofQueryParams;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyReProf.viewobjects.HddcGeomorphysvyreprofVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Service
public class HddcGeomorphysvyreprofServiceImpl implements HddcGeomorphysvyreprofService {

	@Autowired
    private HddcGeomorphysvyreprofRepository hddcGeomorphysvyreprofRepository;
    @Autowired
    private HddcGeomorphysvyreprofNativeRepository hddcGeomorphysvyreprofNativeRepository;

    @Override
    public JSONObject queryHddcGeomorphysvyreprofs(HddcGeomorphysvyreprofQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcGeomorphysvyreprofEntity> hddcGeomorphysvyreprofPage = this.hddcGeomorphysvyreprofNativeRepository.queryHddcGeomorphysvyreprofs(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcGeomorphysvyreprofPage);
        return jsonObject;
    }


    @Override
    public HddcGeomorphysvyreprofEntity getHddcGeomorphysvyreprof(String id) {
        HddcGeomorphysvyreprofEntity hddcGeomorphysvyreprof = this.hddcGeomorphysvyreprofRepository.findById(id).orElse(null);
         return hddcGeomorphysvyreprof;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeomorphysvyreprofEntity saveHddcGeomorphysvyreprof(HddcGeomorphysvyreprofEntity hddcGeomorphysvyreprof) {
        String uuid = UUIDGenerator.getUUID();
        hddcGeomorphysvyreprof.setUuid(uuid);
        hddcGeomorphysvyreprof.setCreateUser(PlatformSessionUtils.getUserId());
        hddcGeomorphysvyreprof.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        hddcGeomorphysvyreprof.setIsValid("1");
        hddcGeomorphysvyreprof.setQualityinspectionStatus("0");
        hddcGeomorphysvyreprof.setExtends10("1");
        this.hddcGeomorphysvyreprofRepository.save(hddcGeomorphysvyreprof);
        return hddcGeomorphysvyreprof;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeomorphysvyreprofEntity updateHddcGeomorphysvyreprof(HddcGeomorphysvyreprofEntity hddcGeomorphysvyreprof) {
        HddcGeomorphysvyreprofEntity entity = hddcGeomorphysvyreprofRepository.findById(hddcGeomorphysvyreprof.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcGeomorphysvyreprof);
        hddcGeomorphysvyreprof.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcGeomorphysvyreprof.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeomorphysvyreprofRepository.save(hddcGeomorphysvyreprof);
        return hddcGeomorphysvyreprof;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcGeomorphysvyreprofs(List<String> ids) {
        List<HddcGeomorphysvyreprofEntity> hddcGeomorphysvyreprofList = this.hddcGeomorphysvyreprofRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcGeomorphysvyreprofList) && hddcGeomorphysvyreprofList.size() > 0) {
            for(HddcGeomorphysvyreprofEntity hddcGeomorphysvyreprof : hddcGeomorphysvyreprofList) {
                hddcGeomorphysvyreprof.setIsValid("0");
                hddcGeomorphysvyreprof.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcGeomorphysvyreprof.setUpdateTime(new Date());
                this.hddcGeomorphysvyreprofRepository.save(hddcGeomorphysvyreprof);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcGeomorphysvyreprofQueryParams queryParams, HttpServletResponse response) {
        List<HddcGeomorphysvyreprofEntity> yhDisasterEntities = hddcGeomorphysvyreprofNativeRepository.exportYhDisasters(queryParams);
        List<HddcGeomorphysvyreprofVO> list=new ArrayList<>();
        for (HddcGeomorphysvyreprofEntity entity:yhDisasterEntities) {
            HddcGeomorphysvyreprofVO yhDisasterVO=new HddcGeomorphysvyreprofVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "微地貌面测量切线-线", "微地貌面测量切线-线", HddcGeomorphysvyreprofVO.class, "微地貌面测量切线-线.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcGeomorphysvyreprofVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcGeomorphysvyreprofVO.class, params);
            List<HddcGeomorphysvyreprofVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcGeomorphysvyreprofVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcGeomorphysvyreprofVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcGeomorphysvyreprofVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcGeomorphysvyreprofEntity yhDisasterEntity = new HddcGeomorphysvyreprofEntity();
            HddcGeomorphysvyreprofVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcGeomorphysvyreprof(yhDisasterEntity);
        }
    }
    @Override
    public void deleteByProjectId(List<String> projectIds) {
        List<HddcGeomorphysvyreprofEntity> hddcGeomorphysvyreprofList = this.hddcGeomorphysvyreprofRepository.queryHddcGeomorphysvyreprofsByProjectId(projectIds);
        if(!PlatformObjectUtils.isEmpty(hddcGeomorphysvyreprofList) && hddcGeomorphysvyreprofList.size() > 0) {
            for(HddcGeomorphysvyreprofEntity hddcGeomorphysvyreprof : hddcGeomorphysvyreprofList) {
                hddcGeomorphysvyreprof.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcGeomorphysvyreprof.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcGeomorphysvyreprof.setIsValid("0");
                this.hddcGeomorphysvyreprofRepository.save(hddcGeomorphysvyreprof);
            }
        }
    }

    @Override
    public void deleteByTaskId(List<String> taskIds) {
        List<HddcGeomorphysvyreprofEntity> hddcGeomorphysvyreprofList = this.hddcGeomorphysvyreprofRepository.queryHddcGeomorphysvyreprofsByTaskId(taskIds);
        if(!PlatformObjectUtils.isEmpty(hddcGeomorphysvyreprofList) && hddcGeomorphysvyreprofList.size() > 0) {
            for(HddcGeomorphysvyreprofEntity hddcGeomorphysvyreprof : hddcGeomorphysvyreprofList) {
                hddcGeomorphysvyreprof.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcGeomorphysvyreprof.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcGeomorphysvyreprof.setIsValid("0");
                this.hddcGeomorphysvyreprofRepository.save(hddcGeomorphysvyreprof);
            }
        }
    }

    @Override
    public String judegeParams(String[] datas) {
        String str = "";
        Set<String> set = new HashSet<String>(Arrays.asList(datas));
        if(!set.contains("ID")){
            str+="ID,";
        }
        if(!set.contains("GeomorphySvyPrjID")){
            str+="GeomorphySvyPrjID,";
        }
        if(!set.contains("RegionID")){
            str+="RegionID,";
        }
        if(!set.contains("SliceSelfID")){
            str+="SliceSelfID,";
        }
        if(!set.contains("Name")){
            str+="Name,";
        }
        if(!set.contains("Profile_AIID")){
            str+="Profile_AIID,";
        }
        if(!set.contains("Profile_ARWID")){
            str+="Profile_ARWID,";
        }
        if(!set.contains("CommentInfo")){
            str+="CommentInfo,";
        }
        return str;
    }

    @Override
    public void savehddcGeomorphysvyreprofServiceFromShpFiles(List<List<Object>> list, String provinceName, String cityName, String areaName,String savefilename) {
        if(list!=null&&list.size()>0){
            List<HddcGeomorphysvyreprofVO> datas= new ArrayList<>();
            for(int i=0;i<list.size();i++){
                List<Object> obs=list.get(i);
                HddcGeomorphysvyreprofVO data=new HddcGeomorphysvyreprofVO();
                data.setArea(areaName);
                data.setProvince(provinceName);
                data.setCity(cityName);
                data.setExtends8(savefilename);
                data.setId(obs.get(1).toString());
                data.setGeomorphysvyprjid(obs.get(2).toString());
                data.setRegionid(obs.get(3).toString());
                data.setSliceselfid(obs.get(4).toString());
                data.setName(obs.get(5).toString());
                data.setProfileAiid(obs.get(6).toString());
                data.setProfileArwid(obs.get(7).toString());
                data.setCommentInfo(obs.get(8).toString());
                String pstr=obs.get(0).toString();
                String[] ss1=pstr.split("\\(");
                if(ss1.length==2){
                    String[] ss2=ss1[1].split("\\)");
                    if(ss2.length<=2){
                        String spacedata=ss2[0].replace(",",";").replace(" ",",");
                        data.setExtends5(spacedata);
                    }
                }
                datas.add(data);
                /*for(int j=0;j<obs.size();j++){
                    Object value=obs.
                }*/
            }
            deleteByCreateUser();
            saveDisasterList(datas,null);
        }

    }


    public void deleteByCreateUser(){
        String createID=PlatformSessionUtils.getUserId();
        List<HddcGeomorphysvyreprofEntity> hddcGeomorphysvyreprofList = this.hddcGeomorphysvyreprofRepository.queryHddcGeomorphysvyreprofByCreateuser(createID);
        if(!PlatformObjectUtils.isEmpty(hddcGeomorphysvyreprofList) && hddcGeomorphysvyreprofList.size() > 0) {
            for(HddcGeomorphysvyreprofEntity hddcGeologicalsvyplanningline : hddcGeomorphysvyreprofList) {
                this.hddcGeomorphysvyreprofRepository.deleteById(hddcGeologicalsvyplanningline.getUuid());
            }
        }
    }

    @Override
    public List<HddcGeomorphysvyreprofEntity> findAll() {
        List<HddcGeomorphysvyreprofEntity> list=this.hddcGeomorphysvyreprofRepository.findAll();
        return list;
    }


}
