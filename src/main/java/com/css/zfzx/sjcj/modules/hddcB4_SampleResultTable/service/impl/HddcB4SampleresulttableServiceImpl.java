package com.css.zfzx.sjcj.modules.hddcB4_SampleResultTable.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB4_SampleResultTable.repository.HddcB4SampleresulttableNativeRepository;
import com.css.zfzx.sjcj.modules.hddcB4_SampleResultTable.repository.HddcB4SampleresulttableRepository;
import com.css.zfzx.sjcj.modules.hddcB4_SampleResultTable.repository.entity.HddcB4SampleresulttableEntity;
import com.css.zfzx.sjcj.modules.hddcB4_SampleResultTable.service.HddcB4SampleresulttableService;
import com.css.zfzx.sjcj.modules.hddcB4_SampleResultTable.viewobjects.HddcB4SampleresulttableQueryParams;
import com.css.zfzx.sjcj.modules.hddcB4_SampleResultTable.viewobjects.HddcB4SampleresulttableVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Service
public class HddcB4SampleresulttableServiceImpl implements HddcB4SampleresulttableService {

	@Autowired
    private HddcB4SampleresulttableRepository hddcB4SampleresulttableRepository;
    @Autowired
    private HddcB4SampleresulttableNativeRepository hddcB4SampleresulttableNativeRepository;

    @Override
    public JSONObject queryHddcB4Sampleresulttables(HddcB4SampleresulttableQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcB4SampleresulttableEntity> hddcB4SampleresulttablePage = this.hddcB4SampleresulttableNativeRepository.queryHddcB4Sampleresulttables(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcB4SampleresulttablePage);
        return jsonObject;
    }


    @Override
    public HddcB4SampleresulttableEntity getHddcB4Sampleresulttable(String id) {
        HddcB4SampleresulttableEntity hddcB4Sampleresulttable = this.hddcB4SampleresulttableRepository.findById(id).orElse(null);
         return hddcB4Sampleresulttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB4SampleresulttableEntity saveHddcB4Sampleresulttable(HddcB4SampleresulttableEntity hddcB4Sampleresulttable) {
        String uuid = UUIDGenerator.getUUID();
        hddcB4Sampleresulttable.setUuid(uuid);
        hddcB4Sampleresulttable.setCreateUser(PlatformSessionUtils.getUserId());
        hddcB4Sampleresulttable.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB4SampleresulttableRepository.save(hddcB4Sampleresulttable);
        return hddcB4Sampleresulttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB4SampleresulttableEntity updateHddcB4Sampleresulttable(HddcB4SampleresulttableEntity hddcB4Sampleresulttable) {
        HddcB4SampleresulttableEntity entity = hddcB4SampleresulttableRepository.findById(hddcB4Sampleresulttable.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcB4Sampleresulttable);
        hddcB4Sampleresulttable.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcB4Sampleresulttable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB4SampleresulttableRepository.save(hddcB4Sampleresulttable);
        return hddcB4Sampleresulttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcB4Sampleresulttables(List<String> ids) {
        List<HddcB4SampleresulttableEntity> hddcB4SampleresulttableList = this.hddcB4SampleresulttableRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcB4SampleresulttableList) && hddcB4SampleresulttableList.size() > 0) {
            for(HddcB4SampleresulttableEntity hddcB4Sampleresulttable : hddcB4SampleresulttableList) {
                this.hddcB4SampleresulttableRepository.delete(hddcB4Sampleresulttable);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcB4SampleresulttableQueryParams queryParams, HttpServletResponse response) {
        List<HddcB4SampleresulttableEntity> yhDisasterEntities = hddcB4SampleresulttableNativeRepository.exportYhDisasters(queryParams);
        List<HddcB4SampleresulttableVO> list=new ArrayList<>();
        for (HddcB4SampleresulttableEntity entity:yhDisasterEntities) {
            HddcB4SampleresulttableVO yhDisasterVO=new HddcB4SampleresulttableVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"样品测试结果表","样品测试结果表",HddcB4SampleresulttableVO.class,"样品测试结果表.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcB4SampleresulttableVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcB4SampleresulttableVO.class, params);
            List<HddcB4SampleresulttableVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcB4SampleresulttableVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcB4SampleresulttableVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcB4SampleresulttableVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcB4SampleresulttableEntity yhDisasterEntity = new HddcB4SampleresulttableEntity();
            HddcB4SampleresulttableVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcB4Sampleresulttable(yhDisasterEntity);
        }
    }
}
