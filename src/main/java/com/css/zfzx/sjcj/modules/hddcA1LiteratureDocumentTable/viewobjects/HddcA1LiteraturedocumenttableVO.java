package com.css.zfzx.sjcj.modules.hddcA1LiteratureDocumentTable.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
public class HddcA1LiteraturedocumenttableVO implements Serializable {

    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 出版日期
     */
    @Excel(name="出版日期",orderNum = "10")
    private String publishdate;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 刊物名称
     */
    @Excel(name="刊物名称",orderNum = "6")
    private String publication;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 作者
     */
    @Excel(name="作者",orderNum = "7")
    private String author;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 市
     */
    @Excel(name="市",orderNum = "2")
    private String city;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备注
     */
    private String remark;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 原始资料编号
     */
    @Excel(name="原始资料编号",orderNum = "11")
    private String rawdataArwid;
    /**
     * 文献类型
     */
    @Excel(name="文献类型",orderNum = "5")
    private String literaturetype;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 文献编号
     */
    private String id;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 文献名称
     */
    @Excel(name="文献名称",orderNum = "4")
    private String literaturename;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 出版单位
     */
    @Excel(name="出版单位",orderNum = "9")
    private String publisher;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 省
     */
    @Excel(name="省",orderNum = "1")
    private String province;
    /**
     * 备注
     */
    @Excel(name="备注",orderNum = "12")
    private String commentInfo;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 第一作者单位
     */
    @Excel(name="第一作者单位",orderNum = "8")
    private String authorinstitute;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 区（县）
     */
    @Excel(name="区（县）",orderNum = "3")
    private String area;

    private String provinceName;
    private String cityName;
    private String areaName;

    private String rowNum;
    private String errorMsg;
}