package com.css.zfzx.sjcj.modules.hddcB4_SampleProjectTable.repository;

import com.css.zfzx.sjcj.modules.hddcB4_SampleProjectTable.repository.entity.HddcB4SampleprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB4_SampleProjectTable.viewobjects.HddcB4SampleprojecttableQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcB4SampleprojecttableNativeRepository {

    Page<HddcB4SampleprojecttableEntity> queryHddcB4Sampleprojecttables(HddcB4SampleprojecttableQueryParams queryParams, int curPage, int pageSize);

    List<HddcB4SampleprojecttableEntity> exportYhDisasters(HddcB4SampleprojecttableQueryParams queryParams);
}
