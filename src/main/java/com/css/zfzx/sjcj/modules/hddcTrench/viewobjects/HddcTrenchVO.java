package com.css.zfzx.sjcj.modules.hddcTrench.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Data
public class HddcTrenchVO implements Serializable {

    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 审查用户
     */
    private String examineUser;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 古地震事件次数
     */
    @Excel(name = "古地震事件次数", orderNum = "4")
    private Integer eqeventcount;
    /**
     * 揭露地层数
     */
    @Excel(name = "揭露地层数", orderNum = "5")
    private Integer exposedstratumcount;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 目标断层编号
     */
    @Excel(name = "目标断层编号", orderNum = "6")
    private String targetfaultid;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 质检数据状态值
     */
    @Excel(name = "质检数据状态值", orderNum = "7")
    private String qualityinspectionStatus;
    /**
     * 质检意见
     */
    private String qualityinspectionComments;
    /**
     * 审核数据状态值 0：未提交；1：已提交，2：审核通过；3：审核未通过
     */
    @Excel(name = "审核数据状态值", orderNum = "8")
    private String reviewStatus;
    /**
     * 收集探槽来源补充说明
     */
    @Excel(name = "收集探槽来源补充说明", orderNum = "9")
    private String collectedtrenchsource;
    /**
     * 探槽剖面图1图像文件编号
     */
    @Excel(name = "探槽剖面图1图像文件编号", orderNum = "10")
    private String profile1Aiid;
    /**
     * 送样总数
     */
    @Excel(name = "送样总数", orderNum = "11")
    private Integer samplecount;
    /**
     * 拍摄者
     */
    @Excel(name = "拍摄者", orderNum = "12")
    private String photographer;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 参考位置
     */
    @Excel(name = "参考位置", orderNum = "13")
    private String locationname;
    /**
     * 探槽剖面图2图像文件编号
     */
    @Excel(name = "探槽剖面图2图像文件编号", orderNum = "14")
    private String profile2Aiid;
    /**
     * 探槽深 [米]
     */
    @Excel(name = "探槽深 [米]", orderNum = "15")
    private Double depth;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 符号或标注旋转角度
     */
    @Excel(name = "符号或标注旋转角度", orderNum = "16")
    private Double lastangle;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 探槽来源与类型
     */
    @Excel(name = "探槽来源与类型", orderNum = "17")
    private Integer trenchsource;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 探槽剖面图1拼接照片图像文件编号
     */
    @Excel(name = "探槽剖面图1拼接照片图像文件编号", orderNum = "18")
    private String profile1photoAiid;
    /**
     * 探槽描述
     */
    @Excel(name = "探槽描述", orderNum = "19")
    private String commentInfo;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 野外编号
     */
    @Excel(name = "野外编号", orderNum = "20")
    private String fieldid;
    /**
     * 质检者
     */
    private String qualityinspectionUser;
    /**
     * 采集样品总数
     */
    @Excel(name = "采集样品总数", orderNum = "21")
    private Integer collectedsamplecount;
    /**
     * 探槽剖面地层描述1文件编号
     */
    @Excel(name = "探槽剖面地层描述1文件编号", orderNum = "22")
    private String profile1commentArid;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 环境照片图像编号
     */
    @Excel(name = "环境照片图像编号", orderNum = "23")
    private String photoAiid;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 照片镜向
     */
    private Integer photoviewingto;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 纬度
     */
    @Excel(name = "纬度", orderNum = "24")
    private Double lat;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 环境照片原始文件编号
     */
    @Excel(name = "环境照片原始文件编号", orderNum = "25")
    private String photoArwid;
    /**
     * 探槽剖面图1原始文件编号
     */
    @Excel(name = "探槽剖面图1原始文件编号", orderNum = "26")
    private String profile1Arwid;
    /**
     * 获得结果的样品数
     */
    @Excel(name = "获得结果的样品数", orderNum = "27")
    private Integer datingsamplecount;
    /**
     * 探槽宽 [米]
     */
    @Excel(name = "探槽宽 [米]", orderNum = "28")
    private Double width;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 探槽名称
     */
    @Excel(name = "探槽名称", orderNum = "29")
    private String name;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 地貌环境
     */
    @Excel(name = "地貌环境", orderNum = "30")
    private String geomophenv;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 探槽剖面图2拼接照片图像文件编号
     */
    @Excel(name = "探槽剖面图2拼接照片图像文件编号", orderNum = "31")
    private String profile2photoAiid;
    /**
     * 经度
     */
    @Excel(name = "经度", orderNum = "32")
    private Double lon;
    /**
     * 探槽剖面地层描述1原始文件编号
     */
    @Excel(name = "探槽剖面地层描述1原始文件编号", orderNum = "33")
    private String profile1commentArwid;
    /**
     * 探槽剖面图2原始文件编号
     */
    @Excel(name = "探槽剖面图2原始文件编号", orderNum = "34")
    private String profile2Arwid;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 探槽长 [米]
     */
    @Excel(name = "探槽长 [米]", orderNum = "35")
    private Double length;
    /**
     * 最晚古地震发震时代误差
     */
    @Excel(name = "最晚古地震发震时代误差", orderNum = "36")
    private Integer latesteqperoider;
    /**
     * 所属地质调查工程编号
     */
    @Excel(name = "所属地质调查工程编号", orderNum = "37")
    private String geologysvyprojectid;
    /**
     * 最晚古地震发震时代
     */
    @Excel(name = "最晚古地震发震时代", orderNum = "38")
    private Integer latesteqperoidest;
    /**
     * 类别码+行政区划组合编码
     */
    @Excel(name = "类别码+行政区划组合编码", orderNum = "39")
    private String objectCode;
    /**
     * 探槽剖面图1照片原始文件编号
     */
    @Excel(name = "探槽剖面图1照片原始文件编号", orderNum = "40")
    private String profile1photoArwid;
    /**
     * 目标断层名称
     */
    @Excel(name = "目标断层名称", orderNum = "41")
    private String targetfaultname;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 探槽编号
     */
    private String id;
    /**
     * 探槽剖面地层描述2文件编号
     */
    @Excel(name = "探槽剖面地层描述2文件编号", orderNum = "42")
    private String profile2commentArid;
    /**
     * 高程 [米]
     */
    @Excel(name = "高程 [米]", orderNum = "43")
    private Double elevation;
    /**
     * 探槽方向角度 [度]
     */
    @Excel(name = "探槽方向角度 [度]", orderNum = "44")
    private Integer trenchdip;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 探槽剖面图2照片原始文件编号
     */
    @Excel(name = "探槽剖面图2照片原始文件编号", orderNum = "45")
    private String profile2photoArwid;
    /**
     * 备注
     */
    private String remark;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 目标断层来源
     */
    @Excel(name = "目标断层来源", orderNum = "46")
    private String targetfaultsource;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 探槽剖面地层描述2原始文件编号
     */
    @Excel(name = "探槽剖面地层描述2原始文件编号", orderNum = "47")
    private String profile2commentArwid;
    /**
     * 是否有效,0:无效,1:有效
     */
    @Excel(name = "是否有效", orderNum = "48")
    private String isValid;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer trenchsourceName;
    private Integer photoviewingtoName;
    private String targetfaultsourceName;
    private String rowNum;
    private String errorMsg;
}