package com.css.zfzx.sjcj.modules.hddcFaultSvyPoint.repository.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author lihelei
 * @date 2020-11-26
 */
@Data
@Entity
@Table(name="hddc_faultsvypoint")
public class HddcFaultsvypointEntity implements Serializable {

    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 水平//张缩位移 [米]
     */
    @Column(name="tensionaldisplacement")
    private Double tensionaldisplacement;
    /**
     * 目标断层编号
     */
    @Column(name="targetfaultid")
    private String targetfaultid;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 平面图原始文件编号
     */
    @Column(name="sketch_arwid")
    private String sketchArwid;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 地质调查观测点编号
     */
    @Column(name="geologicalsvypointid")
    private String geologicalsvypointid;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 是否修改工作底图
     */
    @Column(name="ismodifyworkmap")
    private Integer ismodifyworkmap;
    /**
     * 数据来源
     */
    @Column(name="datasource")
    private String datasource;
    /**
     * 典型剖面图图像文件编号
     */
    @Column(name="typicalprofile_aiid")
    private String typicalprofileAiid;
    /**
     * 获得测试结果样品数
     */
    @Column(name="datingsamplecount")
    private Integer datingsamplecount;
    /**
     * 走向水平位移 [米]
     */
    @Column(name="horizentaloffset")
    private Double horizentaloffset;
    /**
     * 断层倾角 [度]
     */
    @Column(name="faultclination")
    private Integer faultclination;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 拍摄者
     */
    @Column(name="photographer")
    private String photographer;
    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 观测点野外编号
     */
    @Column(name="fieldid")
    private String fieldid;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 水平/张缩位移误差
     */
    @Column(name="tensionaldisplacementerror")
    private Double tensionaldisplacementerror;
    /**
     * 产状点编号
     */
    @Column(name="id")
    private String id;
    /**
     * 编号
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 比例尺（分母）
     */
    @Column(name="scale")
    private Integer scale;
    /**
     * 符号或标注旋转角度
     */
    @Column(name="lastangle")
    private Double lastangle;
    /**
     * 典型照片文件编号
     */
    @Column(name="photo_aiid")
    private String photoAiid;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 断层性质
     */
    @Column(name="feature")
    private Integer feature;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 是否在图中显示
     */
    @Column(name="isinmap")
    private Integer isinmap;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 目标断层名称
     */
    @Column(name="targetfaultname")
    private String targetfaultname;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 断层倾向 [度]
     */
    @Column(name="measurefaultdip")
    private Integer measurefaultdip;
    /**
     * 送样数
     */
    @Column(name="samplecount")
    private Integer samplecount;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 平面图文件编号
     */
    @Column(name="sketch_aiid")
    private String sketchAiid;
    /**
     * 走向水平位移误差
     */
    @Column(name="horizentaloffseterror")
    private Double horizentaloffseterror;
    /**
     * 典型剖面图原始文件编号
     */
    @Column(name="typicalprofile_arwid")
    private String typicalprofileArwid;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 垂直位移 [米]
     */
    @Column(name="verticaldisplacement")
    private Double verticaldisplacement;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 备注
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 典型照片原始文件编号
     */
    @Column(name="photo_arwid")
    private String photoArwid;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 目标断层来源
     */
    @Column(name="targetfaultsource")
    private String targetfaultsource;
    /**
     * 断层走向 [度]
     */
    @Column(name="faultstrike")
    private Integer faultstrike;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 垂直位移误差
     */
    @Column(name="verticaldisplacementerror")
    private Double verticaldisplacementerror;
    /**
     * 照片镜向
     */
    @Column(name="photoviewingto")
    private Integer photoviewingto;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;

}

