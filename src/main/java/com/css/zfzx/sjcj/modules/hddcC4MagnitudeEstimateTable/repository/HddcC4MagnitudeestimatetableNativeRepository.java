package com.css.zfzx.sjcj.modules.hddcC4MagnitudeEstimateTable.repository;

import com.css.zfzx.sjcj.modules.hddcC4MagnitudeEstimateTable.repository.entity.HddcC4MagnitudeestimatetableEntity;
import com.css.zfzx.sjcj.modules.hddcC4MagnitudeEstimateTable.viewobjects.HddcC4MagnitudeestimatetableQueryParams;
import org.springframework.data.domain.Page;

/**
 * @author zhangping
 * @date 2020-11-23
 */
public interface HddcC4MagnitudeestimatetableNativeRepository {

    Page<HddcC4MagnitudeestimatetableEntity> queryHddcC4Magnitudeestimatetables(HddcC4MagnitudeestimatetableQueryParams queryParams, int curPage, int pageSize);
}
