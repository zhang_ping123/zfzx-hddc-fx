package com.css.zfzx.sjcj.modules.hddcTargetRegion.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
public class HddcTargetregionQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String targetname;

}
