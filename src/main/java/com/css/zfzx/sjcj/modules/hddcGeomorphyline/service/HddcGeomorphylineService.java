package com.css.zfzx.sjcj.modules.hddcGeomorphyline.service;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphyline.repository.entity.HddcGeomorphylineEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphyline.viewobjects.HddcGeomorphylineQueryParams;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-09
 */

public interface HddcGeomorphylineService {

    public JSONObject queryHddcGeomorphylines(HddcGeomorphylineQueryParams queryParams, int curPage, int pageSize);

    public HddcGeomorphylineEntity getHddcGeomorphyline(String id);

    public HddcGeomorphylineEntity saveHddcGeomorphyline(HddcGeomorphylineEntity hddcGeomorphyline);

    public HddcGeomorphylineEntity updateHddcGeomorphyline(HddcGeomorphylineEntity hddcGeomorphyline);

    public void deleteHddcGeomorphylines(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGeomorphylineQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
