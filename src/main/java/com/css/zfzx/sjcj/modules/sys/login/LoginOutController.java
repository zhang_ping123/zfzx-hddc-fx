package com.css.zfzx.sjcj.modules.sys.login;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author leon
 * @date 2021/2/5 17:03
 */
@Controller
@RequestMapping("/sys")
public class LoginOutController {
    @Value("${sso.server-outUrl}")
    private String ssoOutUrl;
    @GetMapping("/logOut")
    public String loginOut(HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.getSession().invalidate();
        if (request.getParameter("logOutRedirectUrl") != null && !"".equals(request.getParameter("logOutRedirectUrl"))) {
            return "redirect:" + ssoOutUrl + "/logout?service=" + request.getParameter("logOutRedirectUrl");
        } else {
            return "redirect:" + ssoOutUrl + "/logout?service=";
        }
    }
}
