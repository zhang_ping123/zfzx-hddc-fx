package com.css.zfzx.sjcj.modules.hddcB6_GeochemicalProjectTable.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.zfzx.sjcj.modules.hddcB6_GeochemicalProjectTable.repository.entity.HddcB6GeochemicalprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB6_GeochemicalProjectTable.service.HddcB6GeochemicalprojecttableService;
import com.css.zfzx.sjcj.modules.hddcB6_GeochemicalProjectTable.viewobjects.HddcB6GeochemicalprojecttableQueryParams;
import com.css.bpm.platform.utils.PlatformPageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcB6Geochemicalprojecttables")
public class HddcB6GeochemicalprojecttableController {
    @Autowired
    private HddcB6GeochemicalprojecttableService hddcB6GeochemicalprojecttableService;

    @GetMapping("/queryHddcB6Geochemicalprojecttables")
    public RestResponse queryHddcB6Geochemicalprojecttables(HttpServletRequest request, HddcB6GeochemicalprojecttableQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcB6GeochemicalprojecttableService.queryHddcB6Geochemicalprojecttables(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcB6Geochemicalprojecttable(@PathVariable String id) {
        RestResponse response = null;
        try{
            HddcB6GeochemicalprojecttableEntity hddcB6Geochemicalprojecttable = hddcB6GeochemicalprojecttableService.getHddcB6Geochemicalprojecttable(id);
            response = RestResponse.succeed(hddcB6Geochemicalprojecttable);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcB6Geochemicalprojecttable(@RequestBody HddcB6GeochemicalprojecttableEntity hddcB6Geochemicalprojecttable) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB6GeochemicalprojecttableService.saveHddcB6Geochemicalprojecttable(hddcB6Geochemicalprojecttable);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcB6Geochemicalprojecttable(@RequestBody HddcB6GeochemicalprojecttableEntity hddcB6Geochemicalprojecttable)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB6GeochemicalprojecttableService.updateHddcB6Geochemicalprojecttable(hddcB6Geochemicalprojecttable);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcB6Geochemicalprojecttables(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB6GeochemicalprojecttableService.deleteHddcB6Geochemicalprojecttables(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcB6GeochemicalprojecttableService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }
    /***
     * 导出
     * @param response
     * @return
     */
    @GetMapping("/exportFile")
    public RestResponse exportFileYhDisasters(HttpServletResponse response,
                                              @RequestParam("projectName")String projectName,@RequestParam("name") String name,
                                              @RequestParam("province") String province,@RequestParam("city")String city,@RequestParam("area")String area) {
        RestResponse responseRest = null;
        JSONObject jsonObject = new JSONObject();
        try{
            HddcB6GeochemicalprojecttableQueryParams queryParams=new HddcB6GeochemicalprojecttableQueryParams();
            queryParams.setArea(area);
            queryParams.setCity(city);
            queryParams.setProvince(province);
            queryParams.setProjectName(projectName);
            queryParams.setName(name);
            hddcB6GeochemicalprojecttableService.exportFile(queryParams,response);
            jsonObject.put("message", "导出成功");
            responseRest = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            responseRest = RestResponse.fail(errorMessage);
        }
        return responseRest;
    }

    /**
     * 导入
     *
     * @param file
     * @param response
     * @return
     */
    @PostMapping("/importDisaster")
    public RestResponse export(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        RestResponse restResponse = null;
        try {
            String s = hddcB6GeochemicalprojecttableService.exportExcel( file, response);
            restResponse = RestResponse.succeed(s);
        } catch (Exception e) {
            String errormessage = "导入失败";
            log.error(errormessage, e);
            restResponse = RestResponse.fail(errormessage);
        }
        return restResponse;
    }
}