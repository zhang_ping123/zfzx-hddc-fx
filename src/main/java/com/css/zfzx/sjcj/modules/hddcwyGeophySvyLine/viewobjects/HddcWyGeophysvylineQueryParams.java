package com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-12-01
 */
@Data
public class HddcWyGeophysvylineQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String name;

    private String startTime;
    private String endTime;


    private String userId;
    private String taskId;
    private String projectId;
    private String isValid;
}
