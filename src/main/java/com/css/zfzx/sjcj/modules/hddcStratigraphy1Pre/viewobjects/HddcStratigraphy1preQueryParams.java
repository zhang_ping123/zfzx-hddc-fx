package com.css.zfzx.sjcj.modules.hddcStratigraphy1Pre.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Data
public class HddcStratigraphy1preQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String stratigraphyname;

}
