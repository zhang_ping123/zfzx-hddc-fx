package com.css.zfzx.sjcj.modules.hddcB6_GeocheSvyDataTable.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB6_GeocheSvyDataTable.repository.entity.HddcB6GeochesvydatatableEntity;
import com.css.zfzx.sjcj.modules.hddcB6_GeocheSvyDataTable.viewobjects.HddcB6GeochesvydatatableQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-27
 */

public interface HddcB6GeochesvydatatableService {

    public JSONObject queryHddcB6Geochesvydatatables(HddcB6GeochesvydatatableQueryParams queryParams, int curPage, int pageSize);

    public HddcB6GeochesvydatatableEntity getHddcB6Geochesvydatatable(String id);

    public HddcB6GeochesvydatatableEntity saveHddcB6Geochesvydatatable(HddcB6GeochesvydatatableEntity hddcB6Geochesvydatatable);

    public HddcB6GeochesvydatatableEntity updateHddcB6Geochesvydatatable(HddcB6GeochesvydatatableEntity hddcB6Geochesvydatatable);

    public void deleteHddcB6Geochesvydatatables(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB6GeochesvydatatableQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
