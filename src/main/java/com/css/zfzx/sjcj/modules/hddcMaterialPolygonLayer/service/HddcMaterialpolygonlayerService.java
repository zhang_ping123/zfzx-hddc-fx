package com.css.zfzx.sjcj.modules.hddcMaterialPolygonLayer.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcMaterialPolygonLayer.repository.entity.HddcMaterialpolygonlayerEntity;
import com.css.zfzx.sjcj.modules.hddcMaterialPolygonLayer.viewobjects.HddcMaterialpolygonlayerQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-03
 */

public interface HddcMaterialpolygonlayerService {

    public JSONObject queryHddcMaterialpolygonlayers(HddcMaterialpolygonlayerQueryParams queryParams, int curPage, int pageSize);

    public HddcMaterialpolygonlayerEntity getHddcMaterialpolygonlayer(String id);

    public HddcMaterialpolygonlayerEntity saveHddcMaterialpolygonlayer(HddcMaterialpolygonlayerEntity hddcMaterialpolygonlayer);

    public HddcMaterialpolygonlayerEntity updateHddcMaterialpolygonlayer(HddcMaterialpolygonlayerEntity hddcMaterialpolygonlayer);

    public void deleteHddcMaterialpolygonlayers(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcMaterialpolygonlayerQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
