package com.css.zfzx.sjcj.modules.hddcB4_SampleResultTable.repository.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Data
@Entity
@Table(name="hddc_b4_sampleresulttable")
public class HddcB4SampleresulttableEntity implements Serializable {

    /**
     * 采样点图原始文件编号
     */
    @Column(name="samplelayout_arwid")
    private String samplelayoutArwid;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 采样点编号
     */
    @Column(name="samplepointid")
    private String samplepointid;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 照片集镜向及拍摄者说明文档
     */
    @Column(name="photodesc_arwid")
    private String photodescArwid;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 采样深度
     */
    @Column(name="depth")
    private Double depth;
    /**
     * 可靠性评估
     */
    @Column(name="reliability")
    private Integer reliability;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 采样照片原始文件编号
     */
    @Column(name="photo_arwid")
    private String photoArwid;
    /**
     * 采样照片文件编号
     */
    @Column(name="photo_aiid")
    private String photoAiid;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 测试结果数据表编号
     */
    @Column(name="testeddata_arwid")
    private String testeddataArwid;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 记录编号
     */
    @Column(name="id")
    private String id;
    /**
     * 编号
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 采样类型
     */
    @Column(name="sampletype")
    private Integer sampletype;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 样品野外编号
     */
    @Column(name="fieldid")
    private String fieldid;
    /**
     * 测年结果[年龄±误差(单位)]
     */
    @Column(name="dateage")
    private String dateage;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 备注（对测年结果说明）
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 所属测年工程编号
     */
    @Column(name="project_num")
    private String projectnum;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 测试方法
     */
    @Column(name="sampletestedmethod")
    private Integer sampletestedmethod;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 采样点图图像文件编号
     */
    @Column(name="samplelayout_aiid")
    private String samplelayoutAiid;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;

}

