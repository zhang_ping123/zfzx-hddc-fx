package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyLine.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyLine.repository.HddcGeomorphysvylineRepository;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyLine.repository.entity.HddcGeomorphysvylineEntity;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyLine.repository.HddcWyGeomorphysvylineNativeRepository;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyLine.repository.HddcWyGeomorphysvylineRepository;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyLine.repository.entity.HddcWyGeomorphysvylineEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyLine.service.HddcWyGeomorphysvylineService;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyLine.viewobjects.HddcWyGeomorphysvylineQueryParams;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyLine.viewobjects.HddcWyGeomorphysvylineVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author lihelei
 * @date 2020-12-01
 */
@Service
public class HddcWyGeomorphysvylineServiceImpl implements HddcWyGeomorphysvylineService {

	@Autowired
    private HddcWyGeomorphysvylineRepository hddcWyGeomorphysvylineRepository;
    @Autowired
    private HddcWyGeomorphysvylineNativeRepository hddcWyGeomorphysvylineNativeRepository;
    @Autowired
    private HddcGeomorphysvylineRepository hddcGeomorphysvylineRepository;

    @Override
    public JSONObject queryHddcWyGeomorphysvylines(HddcWyGeomorphysvylineQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcWyGeomorphysvylineEntity> hddcWyGeomorphysvylinePage = this.hddcWyGeomorphysvylineNativeRepository.queryHddcWyGeomorphysvylines(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcWyGeomorphysvylinePage);
        return jsonObject;
    }


    @Override
    public HddcWyGeomorphysvylineEntity getHddcWyGeomorphysvyline(String uuid) {
        HddcWyGeomorphysvylineEntity hddcWyGeomorphysvyline = this.hddcWyGeomorphysvylineRepository.findById(uuid).orElse(null);
         return hddcWyGeomorphysvyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyGeomorphysvylineEntity saveHddcWyGeomorphysvyline(HddcWyGeomorphysvylineEntity hddcWyGeomorphysvyline) {
        String uuid = UUIDGenerator.getUUID();
        hddcWyGeomorphysvyline.setUuid(uuid);
        if(StringUtils.isEmpty(hddcWyGeomorphysvyline.getCreateUser())){
            hddcWyGeomorphysvyline.setCreateUser(PlatformSessionUtils.getUserId());
        }
        hddcWyGeomorphysvyline.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        hddcWyGeomorphysvyline.setIsValid("1");

        HddcGeomorphysvylineEntity hddcGeomorphysvylineEntity=new HddcGeomorphysvylineEntity();
        BeanUtils.copyProperties(hddcWyGeomorphysvyline,hddcGeomorphysvylineEntity);
        hddcGeomorphysvylineEntity.setExtends4(hddcWyGeomorphysvyline.getTown());
        hddcGeomorphysvylineEntity.setExtends5(String.valueOf(hddcWyGeomorphysvyline.getLon()));
        hddcGeomorphysvylineEntity.setExtends6(String.valueOf(hddcWyGeomorphysvyline.getLat()));
        this.hddcGeomorphysvylineRepository.save(hddcGeomorphysvylineEntity);

        this.hddcWyGeomorphysvylineRepository.save(hddcWyGeomorphysvyline);
        return hddcWyGeomorphysvyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyGeomorphysvylineEntity updateHddcWyGeomorphysvyline(HddcWyGeomorphysvylineEntity hddcWyGeomorphysvyline) {
        HddcWyGeomorphysvylineEntity entity = hddcWyGeomorphysvylineRepository.findById(hddcWyGeomorphysvyline.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcWyGeomorphysvyline);
        hddcWyGeomorphysvyline.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcWyGeomorphysvyline.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcWyGeomorphysvylineRepository.save(hddcWyGeomorphysvyline);
        return hddcWyGeomorphysvyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcWyGeomorphysvylines(List<String> ids) {
        List<HddcWyGeomorphysvylineEntity> hddcWyGeomorphysvylineList = this.hddcWyGeomorphysvylineRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcWyGeomorphysvylineList) && hddcWyGeomorphysvylineList.size() > 0) {
            for(HddcWyGeomorphysvylineEntity hddcWyGeomorphysvyline : hddcWyGeomorphysvylineList) {
                this.hddcWyGeomorphysvylineRepository.delete(hddcWyGeomorphysvyline);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public BigInteger queryHddcWyGeomorphysvyline(HddcAppZztCountVo queryParams) {
        BigInteger bigInteger = hddcWyGeomorphysvylineNativeRepository.queryHddcWyGeomorphysvyline(queryParams);
        return bigInteger;
    }

    @Override
    public List<HddcWyGeomorphysvylineEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid) {
        List<HddcWyGeomorphysvylineEntity> list = hddcWyGeomorphysvylineRepository.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, isValid);
        return list;
    }

    @Override
    public void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response) {
        List<HddcWyGeomorphysvylineEntity> hddcWyGeomorphysvylineEntity = hddcWyGeomorphysvylineNativeRepository.exportLine(queryParams);
        List<HddcWyGeomorphysvylineVO> list=new ArrayList<>();
        for (HddcWyGeomorphysvylineEntity entity:hddcWyGeomorphysvylineEntity) {
            HddcWyGeomorphysvylineVO hddcWyGeomorphysvylineVO=new HddcWyGeomorphysvylineVO();
            BeanUtils.copyProperties(entity,hddcWyGeomorphysvylineVO);
            list.add(hddcWyGeomorphysvylineVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"微地貌测量线-线","微地貌测量线-线", HddcWyGeomorphysvylineVO.class,"微地貌测量线-线.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcWyGeomorphysvylineVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcWyGeomorphysvylineVO.class, params);
            List<HddcWyGeomorphysvylineVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcWyGeomorphysvylineVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcWyGeomorphysvylineVO hddcWyTrenchVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + hddcWyTrenchVO.getRowNum() + "行" + hddcWyTrenchVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcWyGeomorphysvylineVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcWyGeomorphysvylineEntity hddcWyGeomorphysvylineEntity = new HddcWyGeomorphysvylineEntity();
            HddcWyGeomorphysvylineVO hddcWyGeomorphysvylineVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(hddcWyGeomorphysvylineVO, hddcWyGeomorphysvylineEntity);
            saveHddcWyGeomorphysvyline(hddcWyGeomorphysvylineEntity);
        }
    }


    @Override
    public List<HddcWyGeomorphysvylineEntity> findAllByCreateUserAndIsValid(String userId,String isValid) {
        List<HddcWyGeomorphysvylineEntity> list = hddcWyGeomorphysvylineRepository.findAllByCreateUserAndIsValid(userId,isValid);
        return list;
    }


    /**
     * 逻辑删除-根据项目id删除数据
     * @param projectIds
     */
    @Override
    public void deleteByProjectId(List<String> projectIds) {
        List<HddcWyGeomorphysvylineEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyGeomorphysvylineRepository.queryHddcA1InvrgnhasmaterialtablesByProjectId(projectIds);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyGeomorphysvylineEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyGeomorphysvylineRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }
    }

    /**
     * 逻辑删除-根据任务id删除数据
     * @param taskId
     */
    @Override
    public void deleteByTaskId(List<String> taskId) {
        List<HddcWyGeomorphysvylineEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyGeomorphysvylineRepository.queryHddcA1InvrgnhasmaterialtablesByTaskId(taskId);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyGeomorphysvylineEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyGeomorphysvylineRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }

    }

    @Override
    public String judegeParams(String[] datas) {
        String str = "";
        Set<String> set = new HashSet<String>(Arrays.asList(datas));
        if(!set.contains("ID")){
            str+="ID,";
        }
        if(!set.contains("GeomorphySvyPrjID")){
            str+="GeomorphySvyPrjID,";
        }
        if(!set.contains("FieldID")){
            str+="FieldID,";
        }
        if(!set.contains("Name")){
            str+="Name,";
        }
        if(!set.contains("Profile_AIID")){
            str+="Profile_AIID,";
        }
        if(!set.contains("Profile_ARWID")){
            str+="Profile_ARWID,";
        }
        if(!set.contains("Photo_AIID")){
            str+="Photo_AIID,";
        }
        if(!set.contains("Photo_ARWID")){
            str+="Photo_ARWID,";
        }
        if(!set.contains("PhotoViewingTo")){
            str+="PhotoViewingTo,";
        }
        if(!set.contains("Photographer")){
            str+="Photographer,";
        }
        if(!set.contains("CommentInfo")){
            str+="CommentInfo,";
        }

        return str;
    }

    @Override
    public void savehddcWyGeomorphysvylineServiceFromShpFiles(List<List<Object>> list, String provinceName, String cityName, String areaName) {

        if(list!=null&&list.size()>0){
            List<HddcWyGeomorphysvylineVO> datas= new ArrayList<>();
            for(int i=0;i<list.size();i++){
                List<Object> obs=list.get(i);
                HddcWyGeomorphysvylineVO data=new HddcWyGeomorphysvylineVO();
                data.setArea(areaName);
                data.setProvince(provinceName);
                data.setCity(cityName);
                data.setId(obs.get(1).toString());
                data.setGeomorphysvyprjid(obs.get(2).toString());
                data.setFieldid(obs.get(3).toString());
                data.setName(obs.get(4).toString());
                data.setProfileAiid(obs.get(4).toString());
                data.setProfileArwid(obs.get(5).toString());
                data.setPhotoAiid(obs.get(6).toString());
                data.setPhotoArwid(obs.get(7).toString());
                data.setPhotoviewingto(Integer.valueOf(obs.get(8).toString()));
                data.setPhotographer(obs.get(9).toString());
                data.setCommentInfo(obs.get(10).toString());

                String pstr=obs.get(0).toString();
                String[] ss1=pstr.split("\\(");
                if(ss1.length==2){
                    String[] ss2=ss1[1].split("\\)");
                    if(ss2.length<=2){
                        String spacedata=ss2[0].replace(",",";").replace(" ",",");
                        data.setExtends5(spacedata);
                    }
                }
                datas.add(data);
                /*for(int j=0;j<obs.size();j++){
                    Object value=obs.
                }*/
            }
            deleteByCreateUser();
            saveDisasterList(datas,null);
        }

    }

    public void deleteByCreateUser(){
        String createID=PlatformSessionUtils.getUserId();
        List<HddcWyGeomorphysvylineEntity> hddcWyGeomorphysvylineList = this.hddcWyGeomorphysvylineRepository.queryHddcWyGeomorphysvylineByCreateuser(createID);
        if(!PlatformObjectUtils.isEmpty(hddcWyGeomorphysvylineList) && hddcWyGeomorphysvylineList.size() > 0) {
            for(HddcWyGeomorphysvylineEntity hddcGeologicalsvyplanningline : hddcWyGeomorphysvylineList) {
                this.hddcWyGeomorphysvylineRepository.deleteById(hddcGeologicalsvyplanningline.getUuid());
            }
        }
    }
}
