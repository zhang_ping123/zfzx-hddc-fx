package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyPoint.repository.HddcGeomorphysvypointRepository;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyPoint.repository.entity.HddcGeomorphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.repository.HddcWyGeomorphysvypointNativeRepository;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.repository.HddcWyGeomorphysvypointRepository;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.repository.entity.HddcWyGeomorphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.service.HddcWyGeomorphysvypointService;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.viewobjects.HddcWyGeomorphysvypointQueryParams;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.viewobjects.HddcWyGeomorphysvypointVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author lihelei
 * @date 2020-12-01
 */
@Service
public class HddcWyGeomorphysvypointServiceImpl implements HddcWyGeomorphysvypointService {

	@Autowired
    private HddcWyGeomorphysvypointRepository hddcWyGeomorphysvypointRepository;
    @Autowired
    private HddcWyGeomorphysvypointNativeRepository hddcWyGeomorphysvypointNativeRepository;
    @Autowired
    private HddcGeomorphysvypointRepository hddcGeomorphysvypointRepository;

    @Override
    public JSONObject queryHddcWyGeomorphysvypoints(HddcWyGeomorphysvypointQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcWyGeomorphysvypointEntity> hddcWyGeomorphysvypointPage = this.hddcWyGeomorphysvypointNativeRepository.queryHddcWyGeomorphysvypoints(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcWyGeomorphysvypointPage);
        return jsonObject;
    }


    @Override
    public HddcWyGeomorphysvypointEntity getHddcWyGeomorphysvypoint(String uuid) {
        HddcWyGeomorphysvypointEntity hddcWyGeomorphysvypoint = this.hddcWyGeomorphysvypointRepository.findById(uuid).orElse(null);
         return hddcWyGeomorphysvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyGeomorphysvypointEntity saveHddcWyGeomorphysvypoint(HddcWyGeomorphysvypointEntity hddcWyGeomorphysvypoint) {
        String uuid = UUIDGenerator.getUUID();
        hddcWyGeomorphysvypoint.setUuid(uuid);
        if(StringUtils.isEmpty(hddcWyGeomorphysvypoint.getCreateUser())){
            hddcWyGeomorphysvypoint.setCreateUser(PlatformSessionUtils.getUserId());
        }
        hddcWyGeomorphysvypoint.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        hddcWyGeomorphysvypoint.setIsValid("1");

        HddcGeomorphysvypointEntity hddcGeomorphysvypointEntity=new HddcGeomorphysvypointEntity();
        BeanUtils.copyProperties(hddcWyGeomorphysvypoint,hddcGeomorphysvypointEntity);
        hddcGeomorphysvypointEntity.setExtends4(hddcWyGeomorphysvypoint.getTown());
        hddcGeomorphysvypointEntity.setExtends5(String.valueOf(hddcWyGeomorphysvypoint.getLon()));
        hddcGeomorphysvypointEntity.setExtends6(String.valueOf(hddcWyGeomorphysvypoint.getLat()));
        this.hddcGeomorphysvypointRepository.save(hddcGeomorphysvypointEntity);

        this.hddcWyGeomorphysvypointRepository.save(hddcWyGeomorphysvypoint);
        return hddcWyGeomorphysvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyGeomorphysvypointEntity updateHddcWyGeomorphysvypoint(HddcWyGeomorphysvypointEntity hddcWyGeomorphysvypoint) {
        HddcWyGeomorphysvypointEntity entity = hddcWyGeomorphysvypointRepository.findById(hddcWyGeomorphysvypoint.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcWyGeomorphysvypoint);
        hddcWyGeomorphysvypoint.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcWyGeomorphysvypoint.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcWyGeomorphysvypointRepository.save(hddcWyGeomorphysvypoint);
        return hddcWyGeomorphysvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcWyGeomorphysvypoints(List<String> ids) {
        List<HddcWyGeomorphysvypointEntity> hddcWyGeomorphysvypointList = this.hddcWyGeomorphysvypointRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcWyGeomorphysvypointList) && hddcWyGeomorphysvypointList.size() > 0) {
            for(HddcWyGeomorphysvypointEntity hddcWyGeomorphysvypoint : hddcWyGeomorphysvypointList) {
                this.hddcWyGeomorphysvypointRepository.delete(hddcWyGeomorphysvypoint);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public BigInteger queryHddcWyGeomorphysvypoint(HddcAppZztCountVo queryParams) {
        BigInteger bigInteger = hddcWyGeomorphysvypointNativeRepository.queryHddcWyGeomorphysvypoint(queryParams);
        return bigInteger;
    }

    @Override
    public List<HddcWyGeomorphysvypointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid) {
        List<HddcWyGeomorphysvypointEntity> list = hddcWyGeomorphysvypointRepository.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, isValid);
        return list;
    }

    @Override
    public void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response) {
        List<HddcWyGeomorphysvypointEntity> hddcWyGeomorphysvypointEntity = hddcWyGeomorphysvypointNativeRepository.exportGeomorPoint(queryParams);
        List<HddcWyGeomorphysvypointVO> list=new ArrayList<>();
        for (HddcWyGeomorphysvypointEntity entity:hddcWyGeomorphysvypointEntity) {
            HddcWyGeomorphysvypointVO hddcWyGeomorphysvypointVO=new HddcWyGeomorphysvypointVO();
            BeanUtils.copyProperties(entity,hddcWyGeomorphysvypointVO);
            list.add(hddcWyGeomorphysvypointVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"微地貌测量点-点","微地貌测量点-点", HddcWyGeomorphysvypointVO.class,"微地貌测量点-点.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcWyGeomorphysvypointVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcWyGeomorphysvypointVO.class, params);
            List<HddcWyGeomorphysvypointVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcWyGeomorphysvypointVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcWyGeomorphysvypointVO hddcWyGeomorphysvypointVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + hddcWyGeomorphysvypointVO.getRowNum() + "行" + hddcWyGeomorphysvypointVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcWyGeomorphysvypointVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcWyGeomorphysvypointEntity hddcWyGeomorphysvypointEntity = new HddcWyGeomorphysvypointEntity();
            HddcWyGeomorphysvypointVO hddcWyGeomorphysvypointVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(hddcWyGeomorphysvypointVO, hddcWyGeomorphysvypointEntity);
            saveHddcWyGeomorphysvypoint(hddcWyGeomorphysvypointEntity);
        }
    }

    @Override
    public List<HddcWyGeomorphysvypointEntity> findAllByCreateUserAndIsValid(String userId,String isValid) {
        List<HddcWyGeomorphysvypointEntity> list = hddcWyGeomorphysvypointRepository.findAllByCreateUserAndIsValid(userId,isValid);
        return list;
    }


    /**
     * 逻辑删除-根据项目id删除数据
     * @param projectIds
     */
    @Override
    public void deleteByProjectId(List<String> projectIds) {
        List<HddcWyGeomorphysvypointEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyGeomorphysvypointRepository.queryHddcA1InvrgnhasmaterialtablesByProjectId(projectIds);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyGeomorphysvypointEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyGeomorphysvypointRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }
    }

    /**
     * 逻辑删除-根据任务id删除数据
     * @param taskId
     */
    @Override
    public void deleteByTaskId(List<String> taskId) {
        List<HddcWyGeomorphysvypointEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyGeomorphysvypointRepository.queryHddcA1InvrgnhasmaterialtablesByTaskId(taskId);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyGeomorphysvypointEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyGeomorphysvypointRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }

    }












}
