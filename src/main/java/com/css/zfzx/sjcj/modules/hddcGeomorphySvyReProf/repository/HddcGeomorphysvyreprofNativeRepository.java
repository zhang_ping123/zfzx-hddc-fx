package com.css.zfzx.sjcj.modules.hddcGeomorphySvyReProf.repository;

import com.css.zfzx.sjcj.modules.hddcGeomorphySvyReProf.repository.entity.HddcGeomorphysvyreprofEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyReProf.viewobjects.HddcGeomorphysvyreprofQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcGeomorphysvyreprofNativeRepository {

    Page<HddcGeomorphysvyreprofEntity> queryHddcGeomorphysvyreprofs(HddcGeomorphysvyreprofQueryParams queryParams, int curPage, int pageSize);

    List<HddcGeomorphysvyreprofEntity> exportYhDisasters(HddcGeomorphysvyreprofQueryParams queryParams);
}
