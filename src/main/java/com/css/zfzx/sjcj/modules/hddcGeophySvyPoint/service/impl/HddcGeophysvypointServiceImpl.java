package com.css.zfzx.sjcj.modules.hddcGeophySvyPoint.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcGeophySvyPoint.repository.HddcGeophysvypointNativeRepository;
import com.css.zfzx.sjcj.modules.hddcGeophySvyPoint.repository.HddcGeophysvypointRepository;
import com.css.zfzx.sjcj.modules.hddcGeophySvyPoint.repository.entity.HddcGeophysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcGeophySvyPoint.service.HddcGeophysvypointService;
import com.css.zfzx.sjcj.modules.hddcGeophySvyPoint.viewobjects.HddcGeophysvypointQueryParams;
import com.css.zfzx.sjcj.modules.hddcGeophySvyPoint.viewobjects.HddcGeophysvypointVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Service
public class HddcGeophysvypointServiceImpl implements HddcGeophysvypointService {

	@Autowired
    private HddcGeophysvypointRepository hddcGeophysvypointRepository;
    @Autowired
    private HddcGeophysvypointNativeRepository hddcGeophysvypointNativeRepository;

    @Override
    public JSONObject queryHddcGeophysvypoints(HddcGeophysvypointQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcGeophysvypointEntity> hddcGeophysvypointPage = this.hddcGeophysvypointNativeRepository.queryHddcGeophysvypoints(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcGeophysvypointPage);
        return jsonObject;
    }


    @Override
    public HddcGeophysvypointEntity getHddcGeophysvypoint(String id) {
        HddcGeophysvypointEntity hddcGeophysvypoint = this.hddcGeophysvypointRepository.findById(id).orElse(null);
         return hddcGeophysvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeophysvypointEntity saveHddcGeophysvypoint(HddcGeophysvypointEntity hddcGeophysvypoint) {
        String uuid = UUIDGenerator.getUUID();
        hddcGeophysvypoint.setUuid(uuid);
        hddcGeophysvypoint.setCreateUser(PlatformSessionUtils.getUserId());
        hddcGeophysvypoint.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeophysvypointRepository.save(hddcGeophysvypoint);
        return hddcGeophysvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeophysvypointEntity updateHddcGeophysvypoint(HddcGeophysvypointEntity hddcGeophysvypoint) {
        HddcGeophysvypointEntity entity = hddcGeophysvypointRepository.findById(hddcGeophysvypoint.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcGeophysvypoint);
        hddcGeophysvypoint.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcGeophysvypoint.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeophysvypointRepository.save(hddcGeophysvypoint);
        return hddcGeophysvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcGeophysvypoints(List<String> ids) {
        List<HddcGeophysvypointEntity> hddcGeophysvypointList = this.hddcGeophysvypointRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcGeophysvypointList) && hddcGeophysvypointList.size() > 0) {
            for(HddcGeophysvypointEntity hddcGeophysvypoint : hddcGeophysvypointList) {
                this.hddcGeophysvypointRepository.delete(hddcGeophysvypoint);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcGeophysvypointQueryParams queryParams, HttpServletResponse response) {
        List<HddcGeophysvypointEntity> yhDisasterEntities = hddcGeophysvypointNativeRepository.exportYhDisasters(queryParams);
        List<HddcGeophysvypointVO> list=new ArrayList<>();
        for (HddcGeophysvypointEntity entity:yhDisasterEntities) {
            HddcGeophysvypointVO yhDisasterVO=new HddcGeophysvypointVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"地球物理测点-点","地球物理测点-点",HddcGeophysvypointVO.class,"地球物理测点-点.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcGeophysvypointVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcGeophysvypointVO.class, params);
            List<HddcGeophysvypointVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcGeophysvypointVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcGeophysvypointVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcGeophysvypointVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcGeophysvypointEntity yhDisasterEntity = new HddcGeophysvypointEntity();
            HddcGeophysvypointVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcGeophysvypoint(yhDisasterEntity);
        }
    }

}
