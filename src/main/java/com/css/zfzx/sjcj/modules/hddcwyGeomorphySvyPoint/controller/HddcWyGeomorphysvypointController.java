package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.repository.entity.HddcWyGeomorphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.service.HddcWyGeomorphysvypointService;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.viewobjects.HddcWyGeomorphysvypointQueryParams;
import com.css.bpm.platform.utils.PlatformPageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author lihelei
 * @date 2020-12-01
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcWyGeomorphysvypoints")
public class HddcWyGeomorphysvypointController {
    @Autowired
    private HddcWyGeomorphysvypointService hddcWyGeomorphysvypointService;

    @GetMapping("/queryHddcWyGeomorphysvypoints")
    public RestResponse queryHddcWyGeomorphysvypoints(HttpServletRequest request, HddcWyGeomorphysvypointQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcWyGeomorphysvypointService.queryHddcWyGeomorphysvypoints(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/findAllByhddcWyGeomorphysvypoints")
    public RestResponse findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid) {
        RestResponse response = null;
        try{
            List<HddcWyGeomorphysvypointEntity> list = hddcWyGeomorphysvypointService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, isValid);
            response = RestResponse.succeed(list);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @GetMapping("{uuid}")
    public RestResponse getHddcWyGeomorphysvypoint(@PathVariable String uuid) {
        RestResponse response = null;
        try{
            HddcWyGeomorphysvypointEntity hddcWyGeomorphysvypoint = hddcWyGeomorphysvypointService.getHddcWyGeomorphysvypoint(uuid);
            response = RestResponse.succeed(hddcWyGeomorphysvypoint);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcWyGeomorphysvypoint(@RequestBody HddcWyGeomorphysvypointEntity hddcWyGeomorphysvypoint) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcWyGeomorphysvypointService.saveHddcWyGeomorphysvypoint(hddcWyGeomorphysvypoint);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcWyGeomorphysvypoint(@RequestBody HddcWyGeomorphysvypointEntity hddcWyGeomorphysvypoint)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcWyGeomorphysvypointService.updateHddcWyGeomorphysvypoint(hddcWyGeomorphysvypoint);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcWyGeomorphysvypoints(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcWyGeomorphysvypointService.deleteHddcWyGeomorphysvypoints(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcWyGeomorphysvypointService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }

    /***
     * 导出
     * @param response
     * @return
     */
    @GetMapping("/exportFile")
    public RestResponse exportFileYhDisasters(HttpServletResponse response,
                                              @RequestParam("projectName")String projectName, @RequestParam("taskName") String taskName,
                                              @RequestParam("startTime") String startTime, @RequestParam("endTime")String endTime) {
        RestResponse responseRest = null;
        JSONObject jsonObject = new JSONObject();
        try{
            HddcAppZztCountVo queryParams=new HddcAppZztCountVo();
            queryParams.setProjectName(projectName);
            queryParams.setTaskName(taskName);
            queryParams.setStartTime(startTime);
            queryParams.setEndTime(endTime);
            hddcWyGeomorphysvypointService.exportFile(queryParams,response);
            jsonObject.put("message", "导出成功");
            responseRest = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            responseRest = RestResponse.fail(errorMessage);
        }
        return responseRest;
    }


    /**
     * 导入
     *
     * @param file
     * @param response
     * @return
     */
    @PostMapping("/importDisaster")
    public RestResponse export(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        RestResponse restResponse = null;
        try {
            String s = hddcWyGeomorphysvypointService.exportExcel( file, response);
            restResponse = RestResponse.succeed(s);
        } catch (Exception e) {
            String errormessage = "导入失败";
            log.error(errormessage, e);
            restResponse = RestResponse.fail(errormessage);
        }
        return restResponse;
    }


}