package com.css.zfzx.sjcj.modules.hddcRock25Pre.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Data
public class HddcRock25preVO implements Serializable {

    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 侵入时代
     */
    @Excel(name = "侵入时代", orderNum = "4")
    private Integer qdho;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 岩体描述
     */
    @Excel(name = "岩体描述", orderNum = "5")
    private String description;
    /**
     * 岩体名称
     */
    @Excel(name = "岩体名称", orderNum = "6")
    private String rockname;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 岩体合并
     */
    @Excel(name = "岩体合并", orderNum = "7")
    private String rockunion;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 岩体符号上标位
     */
    @Excel(name = "岩体符号上标位", orderNum = "8")
    private String nsb3;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 岩体类别
     */
    @Excel(name = "岩体类别", orderNum = "9")
    private Integer symbol;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备注
     */
    private String remark;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备注
     */
    private String commentInfo;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 乡
     */
    private String town;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 岩体编号
     */
    private String id;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 岩体符号下标位
     */
    @Excel(name = "岩体符号下标位", orderNum = "10")
    private String nsb2;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 岩体符号基础位
     */
    @Excel(name = "岩体符号基础位", orderNum = "11")
    private String nsb1;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段26
     */
    private String extends26;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String symbolName;
    private Integer qdhoName;
    private String rowNum;
    private String errorMsg;
}