package com.css.zfzx.sjcj.modules.hddcGeoProfileLine.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcGeoProfileLine.repository.entity.HddcGeoprofilelineEntity;
import com.css.zfzx.sjcj.modules.hddcGeoProfileLine.viewobjects.HddcGeoprofilelineQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */

public interface HddcGeoprofilelineService {

    public JSONObject queryHddcGeoprofilelines(HddcGeoprofilelineQueryParams queryParams, int curPage, int pageSize);

    public HddcGeoprofilelineEntity getHddcGeoprofileline(String id);

    public HddcGeoprofilelineEntity saveHddcGeoprofileline(HddcGeoprofilelineEntity hddcGeoprofileline);

    public HddcGeoprofilelineEntity updateHddcGeoprofileline(HddcGeoprofilelineEntity hddcGeoprofileline);

    public void deleteHddcGeoprofilelines(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGeoprofilelineQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
