package com.css.zfzx.sjcj.modules.yhschool.viewobjects;

import lombok.Data;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author yyd
 * @date 2020-11-03
 */
@Data
public class YhSchoolVO implements Serializable {

    /**
     * 状态
     */
    private String status;
    /**
     * 区
     */
    private String area;
    /**
     * 建筑面积
     */
    private Double buildingArea;
    /**
     * 纬度
     */
    private Double latitude;
    /**
     * 主键ID
     */
    private String id;
    /**
     * 原因
     */
    private String reason;
    /**
     * 隐患等级
     */
    private String yhLevel;
    /**
     * 承灾体名称
     */
    private String cztName;
    /**
     * 是否有效,0:无效,1:有效
     */
    private String isValid;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 建筑等级
     */
    private String buildingLevel;
    /**
     * 省
     */
    private String province;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 市
     */
    private String city;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 建筑高度
     */
    private Double buildingHeight;
    /**
     * 经度
     */
    private Double longitude;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String buildingLevelName;
    private String yhLevelName;
}