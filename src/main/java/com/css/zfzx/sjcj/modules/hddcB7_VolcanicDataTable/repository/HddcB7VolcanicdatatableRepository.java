package com.css.zfzx.sjcj.modules.hddcB7_VolcanicDataTable.repository;

import com.css.zfzx.sjcj.modules.hddcB7_VolcanicDataTable.repository.entity.HddcB7VolcanicdatatableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-26
 */
public interface HddcB7VolcanicdatatableRepository extends JpaRepository<HddcB7VolcanicdatatableEntity, String> {
}
