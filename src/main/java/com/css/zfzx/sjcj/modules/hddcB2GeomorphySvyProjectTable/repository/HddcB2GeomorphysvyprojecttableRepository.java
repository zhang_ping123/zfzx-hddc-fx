package com.css.zfzx.sjcj.modules.hddcB2GeomorphySvyProjectTable.repository;

import com.css.zfzx.sjcj.modules.hddcB2GeomorphySvyProjectTable.repository.entity.HddcB2GeomorphysvyprojecttableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-12-07
 */
public interface HddcB2GeomorphysvyprojecttableRepository extends JpaRepository<HddcB2GeomorphysvyprojecttableEntity, String> {
}
