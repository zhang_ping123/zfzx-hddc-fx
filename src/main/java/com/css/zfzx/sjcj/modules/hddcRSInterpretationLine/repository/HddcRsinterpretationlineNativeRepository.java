package com.css.zfzx.sjcj.modules.hddcRSInterpretationLine.repository;

import com.css.zfzx.sjcj.modules.hddcRSInterpretationLine.repository.entity.HddcRsinterpretationlineEntity;
import com.css.zfzx.sjcj.modules.hddcRSInterpretationLine.viewobjects.HddcRsinterpretationlineQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangping
 * @date 2020-11-30
 */
public interface HddcRsinterpretationlineNativeRepository {

    Page<HddcRsinterpretationlineEntity> queryHddcRsinterpretationlines(HddcRsinterpretationlineQueryParams queryParams, int curPage, int pageSize);

    List<HddcRsinterpretationlineEntity> exportYhDisasters(HddcRsinterpretationlineQueryParams queryParams);
}
