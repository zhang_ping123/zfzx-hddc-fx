package com.css.zfzx.sjcj.modules.hddcB4_SampleProjectTable.repository;

import com.css.zfzx.sjcj.modules.hddcB4_SampleProjectTable.repository.entity.HddcB4SampleprojecttableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcB4SampleprojecttableRepository extends JpaRepository<HddcB4SampleprojecttableEntity, String> {
}
