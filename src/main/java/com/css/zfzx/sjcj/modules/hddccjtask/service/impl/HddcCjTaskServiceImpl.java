package com.css.zfzx.sjcj.modules.hddccjtask.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddccjtask.repository.HddcCjTaskNativeRepository;
import com.css.zfzx.sjcj.modules.hddccjtask.repository.HddcCjTaskRepository;
import com.css.zfzx.sjcj.modules.hddccjtask.repository.entity.HddcCjTaskEntity;
import com.css.zfzx.sjcj.modules.hddccjtask.service.HddcCjTaskService;
import com.css.zfzx.sjcj.modules.hddccjtask.viewobjects.HddcCjTaskQueryParams;
import com.css.zfzx.sjcj.modules.hddccjtask.viewobjects.HddcCjTaskVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.org.user.repository.entity.UserEntity;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author zhangping
 * @date 2020-11-26
 */
@Service
public class HddcCjTaskServiceImpl implements HddcCjTaskService {

	@Autowired
    private HddcCjTaskRepository hddcCjTaskRepository;
    @Autowired
    private HddcCjTaskNativeRepository hddcCjTaskNativeRepository;

    @Override
    public JSONObject queryHddcCjTasks(HddcCjTaskQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcCjTaskEntity> hddcCjTaskPage = this.hddcCjTaskNativeRepository.queryHddcCjTasks(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcCjTaskPage);
        return jsonObject;
    }


    @Override
    public HddcCjTaskVO getHddcCjTask(String id) {
        HddcCjTaskEntity hddcCjTask = this.hddcCjTaskRepository.findById(id).orElse(null);
        HddcCjTaskVO hddcCjTaskVO = new HddcCjTaskVO();
        if(hddcCjTask!=null){
            BeanUtils.copyProperties(hddcCjTask, hddcCjTaskVO);
            UserEntity userOfPersonIds = PlatformAPI.getOrgAPI().getUserAPI().getUser(hddcCjTask.getPersonIds());
            String valuePersonIds = "";
            if(PlatformObjectUtils.isNotEmpty(userOfPersonIds)) {
                valuePersonIds = userOfPersonIds.getUserName();
            }
            hddcCjTaskVO.setPersonIdsName(valuePersonIds);
        }
        return hddcCjTaskVO;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcCjTaskEntity saveHddcCjTask(HddcCjTaskEntity hddcCjTask) {
        String uuid = UUIDGenerator.getUUID();
        hddcCjTask.setId(uuid);
        hddcCjTask.setCreateUser(PlatformSessionUtils.getUserId());
        hddcCjTask.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcCjTaskRepository.save(hddcCjTask);
        return hddcCjTask;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcCjTaskEntity updateHddcCjTask(HddcCjTaskEntity hddcCjTask) {
        HddcCjTaskEntity entity = hddcCjTaskRepository.findById(hddcCjTask.getId()).get();
        UpdateUtil.copyNullProperties(entity,hddcCjTask);
        hddcCjTask.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcCjTask.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcCjTaskRepository.save(hddcCjTask);
        return hddcCjTask;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcCjTasks(List<String> ids) {
        List<HddcCjTaskEntity> hddcCjTaskList = this.hddcCjTaskRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcCjTaskList) && hddcCjTaskList.size() > 0) {
            for(HddcCjTaskEntity hddcCjTask : hddcCjTaskList) {
                this.hddcCjTaskRepository.delete(hddcCjTask);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public List<HddcCjTaskEntity> findAll() {
        List<HddcCjTaskEntity> hddcCjTaskList = this.hddcCjTaskRepository.findAll();
        return hddcCjTaskList;
    }

    @Override
    public List<HddcCjTaskEntity> findHddcCjTaskEntityByProjectIds(String projectid) {
        List<HddcCjTaskEntity> hddcCjTaskList = this.hddcCjTaskRepository.findHddcCjTaskEntityByProjectIds(projectid);
        return hddcCjTaskList;
    }

    @Override
    public JSONObject queryHddcCjTasksByProjectId(String projectId, int curPage, int pageSize) {
        Page<HddcCjTaskEntity> hddcCjTaskPage = this.hddcCjTaskNativeRepository.queryHddcCjTasksByProjectId(projectId, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcCjTaskPage);
        return jsonObject;
    }

}
