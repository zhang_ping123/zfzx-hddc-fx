package com.css.zfzx.sjcj.modules.hddcAviationMagnetic.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcAviationMagnetic.repository.entity.HddcAviationmagneticEntity;
import com.css.zfzx.sjcj.modules.hddcAviationMagnetic.viewobjects.HddcAviationmagneticQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-26
 */

public interface HddcAviationmagneticService {

    public JSONObject queryHddcAviationmagnetics(HddcAviationmagneticQueryParams queryParams, int curPage, int pageSize);

    public HddcAviationmagneticEntity getHddcAviationmagnetic(String id);

    public HddcAviationmagneticEntity saveHddcAviationmagnetic(HddcAviationmagneticEntity hddcAviationmagnetic);

    public HddcAviationmagneticEntity updateHddcAviationmagnetic(HddcAviationmagneticEntity hddcAviationmagnetic);

    public void deleteHddcAviationmagnetics(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcAviationmagneticQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
