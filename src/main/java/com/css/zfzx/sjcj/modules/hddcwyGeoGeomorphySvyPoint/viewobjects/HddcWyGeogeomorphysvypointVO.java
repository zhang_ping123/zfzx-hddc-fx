package com.css.zfzx.sjcj.modules.hddcwyGeoGeomorphySvyPoint.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-12-01
 */
@Data
public class HddcWyGeogeomorphysvypointVO implements Serializable {

    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 垂直位移 [米]
     */
    @Excel(name="垂直位移 [米]",orderNum = "22")
    private Double verticaldisplacement;
    /**
     * 性质
     */
    @Excel(name="性质",orderNum = "12")
    private String feature;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 典型照片原始文件编号
     */
    @Excel(name="典型照片原始文件编号",orderNum = "28")
    private String photoArwid;
    /**
     * 市
     */
    @Excel(name="市",orderNum = "3")
    private String city;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 地质调查观测点编号
     */
    @Excel(name="地质调查观测点编号",orderNum = "10")
    private String geologicalsvypointid;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 地震地表破裂类型
     */
    @Excel(name="地震地表破裂类型",orderNum = "15")
    private Integer fracturetype;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 水平//张缩位移 [米]
     */
    @Excel(name="水平//张缩位移 [米]",orderNum = "33")
    private Double tensiondisplacement;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备注
     */
    private String remark;
    /**
     * 典型照片文件编号
     */
    @Excel(name="典型照片文件编号",orderNum = "21")
    private String photoAiid;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 地表破裂（断塞塘等）宽 [米]
     */
    @Excel(name="地表破裂（断塞塘等）宽 [米]",orderNum = "37")
    private Double width;
    /**
     * 照片镜向
     */
    @Excel(name="照片镜向",orderNum = "17")
    private Integer photoviewingto;
    /**
     * 符号或标注旋转角度
     */
    @Excel(name="符号或标注旋转角度",orderNum = "34")
    private Double lastangle;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 观测点野外编号
     */
    @Excel(name="观测点野外编号",orderNum = "35")
    private String fieldid;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 典型剖面图原始文件编号
     */
    @Excel(name="典型剖面图原始文件编号",orderNum = "30")
    private String typicalprofileArwid;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 地表破裂（断塞塘等）高/深 [米]
     */
    @Excel(name="地表破裂（断塞塘等）高/深 [米]",orderNum = "31")
    private Double height;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 描述
     */
    @Excel(name="描述",orderNum = "19")
    private String commentInfo;
    /**
     * 乡
     */
    @Excel(name="详细地址",orderNum = "5")
    private String town;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 地貌点类型
     */
    @Excel(name="地貌点类型",orderNum = "23")
    private String geomorphytype;
    /**
     * 走向水平位移 [米]
     */
    @Excel(name="走向水平位移 [米]",orderNum = "26")
    private Double horizenoffset;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 拍摄者
     */
    @Excel(name="拍摄者",orderNum = "32")
    private String photographer;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 任务名称
     */
    @Excel(name="任务名称",orderNum = "9")
    private String taskName;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 是否为已知地震的地表破裂
     */
    @Excel(name="是否为已知地震的地表破裂",orderNum = "18")
    private Integer issurfacerupturebelt;
    /**
     * 区（县）
     */
    @Excel(name="区（县）",orderNum = "4")
    private String area;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 项目名称
     */
    @Excel(name="项目名称",orderNum = "8")
    private String projectName;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 地表破裂（断塞塘等）长 [米]
     */
    @Excel(name="地表破裂（断塞塘等）长 [米]",orderNum = "27")
    private Double length;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 地貌线走向（制图用）
     */
    @Excel(name="地貌线走向（制图用）",orderNum = "29")
    private Integer geomorphylndirection;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 编号
     */
    @Excel(name="编号",orderNum = "1")
    private String id;
    /**
     * 平面图文件编号
     */
    @Excel(name="平面图文件编号",orderNum = "14")
    private String sketchAiid;
    /**
     * 省
     */
    @Excel(name="省",orderNum = "2")
    private String province;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 地貌点名称
     */
    @Excel(name="地貌点名称",orderNum = "11")
    private String geomorphyname;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 地貌代码
     */
    @Excel(name="地貌代码",orderNum = "25")
    private String geomorphycode;
    /**
     * 是否在图中显示
     */
    @Excel(name="是否在图中显示",orderNum = "24")
    private Integer isinmap;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 是否修改工作底图
     */
    @Excel(name="是否修改工作底图",orderNum = "20")
    private Integer ismodifyworkmap;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 形成时代
     */
    @Excel(name="形成时代",orderNum = "13")
    private Integer createdate;
    /**
     * 典型剖面图图表文件编号
     */
    @Excel(name="典型剖面图图表文件编号",orderNum = "36")
    private String typicalprofileAiid;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 平面图原始文件编号
     */
    @Excel(name="平面图原始文件编号",orderNum = "16")
    private String sketchArwid;
    /**
     * 经度
     */
    @Excel(name="经度",orderNum = "6")
    private Double lon;
    /**
     * 维度
     */
    @Excel(name="纬度",orderNum = "7")
    private Double lat;


    private String provinceName;
    private String cityName;
    private String areaName;


    private String rowNum;
    private String errorMsg;
}