package com.css.zfzx.sjcj.modules.hddcC4MagnitudeEstimateTable.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcC4MagnitudeEstimateTable.repository.HddcC4MagnitudeestimatetableNativeRepository;
import com.css.zfzx.sjcj.modules.hddcC4MagnitudeEstimateTable.repository.HddcC4MagnitudeestimatetableRepository;
import com.css.zfzx.sjcj.modules.hddcC4MagnitudeEstimateTable.repository.entity.HddcC4MagnitudeestimatetableEntity;
import com.css.zfzx.sjcj.modules.hddcC4MagnitudeEstimateTable.service.HddcC4MagnitudeestimatetableService;
import com.css.zfzx.sjcj.modules.hddcC4MagnitudeEstimateTable.viewobjects.HddcC4MagnitudeestimatetableQueryParams;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author zhangping
 * @date 2020-11-23
 */
@Service
public class HddcC4MagnitudeestimatetableServiceImpl implements HddcC4MagnitudeestimatetableService {

	@Autowired
    private HddcC4MagnitudeestimatetableRepository hddcC4MagnitudeestimatetableRepository;
    @Autowired
    private HddcC4MagnitudeestimatetableNativeRepository hddcC4MagnitudeestimatetableNativeRepository;

    @Override
    public JSONObject queryHddcC4Magnitudeestimatetables(HddcC4MagnitudeestimatetableQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcC4MagnitudeestimatetableEntity> hddcC4MagnitudeestimatetablePage = this.hddcC4MagnitudeestimatetableNativeRepository.queryHddcC4Magnitudeestimatetables(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcC4MagnitudeestimatetablePage);
        return jsonObject;
    }


    @Override
    public HddcC4MagnitudeestimatetableEntity getHddcC4Magnitudeestimatetable(String id) {
        HddcC4MagnitudeestimatetableEntity hddcC4Magnitudeestimatetable = this.hddcC4MagnitudeestimatetableRepository.findById(id).orElse(null);
         return hddcC4Magnitudeestimatetable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcC4MagnitudeestimatetableEntity saveHddcC4Magnitudeestimatetable(HddcC4MagnitudeestimatetableEntity hddcC4Magnitudeestimatetable) {
        String uuid = UUIDGenerator.getUUID();
        hddcC4Magnitudeestimatetable.setFaultid(uuid);
//        hddcC4Magnitudeestimatetable.setCreateUser(PlatformSessionUtils.getUserId());
        hddcC4Magnitudeestimatetable.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcC4MagnitudeestimatetableRepository.save(hddcC4Magnitudeestimatetable);
        return hddcC4Magnitudeestimatetable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcC4MagnitudeestimatetableEntity updateHddcC4Magnitudeestimatetable(HddcC4MagnitudeestimatetableEntity hddcC4Magnitudeestimatetable) {
        HddcC4MagnitudeestimatetableEntity entity = hddcC4MagnitudeestimatetableRepository.findById(hddcC4Magnitudeestimatetable.getFaultid()).get();
        UpdateUtil.copyNullProperties(entity,hddcC4Magnitudeestimatetable);
        hddcC4Magnitudeestimatetable.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcC4Magnitudeestimatetable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcC4MagnitudeestimatetableRepository.save(hddcC4Magnitudeestimatetable);
        return hddcC4Magnitudeestimatetable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcC4Magnitudeestimatetables(List<String> ids) {
        List<HddcC4MagnitudeestimatetableEntity> hddcC4MagnitudeestimatetableList = this.hddcC4MagnitudeestimatetableRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcC4MagnitudeestimatetableList) && hddcC4MagnitudeestimatetableList.size() > 0) {
            for(HddcC4MagnitudeestimatetableEntity hddcC4Magnitudeestimatetable : hddcC4MagnitudeestimatetableList) {
                this.hddcC4MagnitudeestimatetableRepository.delete(hddcC4Magnitudeestimatetable);
            }
        }
    }


}
