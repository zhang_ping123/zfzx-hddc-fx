package com.css.zfzx.sjcj.modules.hddcISCatalog.repository;

import com.css.zfzx.sjcj.modules.hddcISCatalog.repository.entity.HddcIscatalogEntity;
import com.css.zfzx.sjcj.modules.hddcISCatalog.viewobjects.HddcIscatalogQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */
public interface HddcIscatalogNativeRepository {

    Page<HddcIscatalogEntity> queryHddcIscatalogs(HddcIscatalogQueryParams queryParams, int curPage, int pageSize);

    List<HddcIscatalogEntity> exportYhDisasters(HddcIscatalogQueryParams queryParams);
}
