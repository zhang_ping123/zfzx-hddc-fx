package com.css.zfzx.sjcj.modules.hddcGeologicalSvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPoint.repository.entity.HddcGeologicalsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPoint.viewobjects.HddcGeologicalsvypointQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author lihelei
 * @date 2020-11-27
 */
public interface HddcGeologicalsvypointNativeRepository {

    Page<HddcGeologicalsvypointEntity> queryHddcGeologicalsvypoints(HddcGeologicalsvypointQueryParams queryParams, int curPage, int pageSize);

    List<HddcGeologicalsvypointEntity> exportYhDisasters(HddcGeologicalsvypointQueryParams queryParams);
}
