package com.css.zfzx.sjcj.modules.analysis.service.impl;

import com.css.zfzx.sjcj.modules.analysis.service.DzjcService;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.service.HddcWyFaultsvypointService;
import com.css.zfzx.sjcj.modules.hddcwyGeoGeomorphySvyPoint.service.HddcWyGeogeomorphysvypointService;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyLine.service.HddcWyGeologicalsvylineService;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPoint.service.HddcWyGeologicalsvypointService;
import com.css.zfzx.sjcj.modules.hddcwyStratigraphySvyPoint.service.HddcWyStratigraphysvypointService;
import com.css.zfzx.sjcj.modules.hddcwyTrench.service.HddcWyTrenchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO
 *
 * @author 王昊杰
 * @version 1.0
 * @date 2020/12/10  19:45
 */

@Service
public class DzjcServiceImpl implements DzjcService {
    @Autowired
    private HddcWyFaultsvypointService hddcWyFaultsvypointService;
    @Autowired
    private HddcWyGeogeomorphysvypointService hddcWyGeogeomorphysvypointService;
    @Autowired
    private HddcWyGeologicalsvylineService hddcWyGeologicalsvylineService;
    @Autowired
    private HddcWyGeologicalsvypointService hddcWyGeologicalsvypointService;
    @Autowired
    private HddcWyStratigraphysvypointService hddcWyStratigraphysvypointService;
    @Autowired
    private HddcWyTrenchService hddcWyTrenchService;

    public Map<String,Object> hddcDzjcNumData(HddcAppZztCountVo queryParams){
        //断层观测点-点
        BigInteger pointCount = hddcWyFaultsvypointService.queryHddcWyFaultsvypoint(queryParams);
        //地质地貌调查观测点-点
        BigInteger geoPointCount = hddcWyGeogeomorphysvypointService.queryHddcWyGeogeomorphysvypoint(queryParams);
        //地质调查路线-线
        //BigInteger lineCount = hddcWyGeologicalsvylineService.queryHddcWyGeologicalsvyline(queryParams);
        //地质调查观测点-点
        BigInteger logPointCount = hddcWyGeologicalsvypointService.queryHddcWyGeologicalsvypoint(queryParams);
        //地层观测点-点
        BigInteger strpointCount = hddcWyStratigraphysvypointService.queryHddcWyStratigraphysvypoint(queryParams);
        //探槽-点
        BigInteger trenchCount = hddcWyTrenchService.queryHddcWyTrench(queryParams);
        //X轴
        ArrayList<String> x=new ArrayList<>();
        x.add("断层观测点-点");
        x.add("地质地貌调查观测点-点");
        //x.add("地质调查路线-线");
        x.add("地质调查观测点-点");
        x.add("地层观测点-点");
        x.add("探槽-点");
        //Y轴
        ArrayList<BigInteger> y=new ArrayList<>();
        y.add(pointCount);
        y.add(geoPointCount);
        //y.add(lineCount);
        y.add(logPointCount);
        y.add(strpointCount);
        y.add(trenchCount);

        HashMap<String,Object> map=new HashMap<>();
        map.put("x",x);
        map.put("y",y);
        return map;
    }

}
