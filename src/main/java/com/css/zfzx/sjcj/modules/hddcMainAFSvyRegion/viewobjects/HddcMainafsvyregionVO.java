package com.css.zfzx.sjcj.modules.hddcMainAFSvyRegion.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-12-14
 */
@Data
public class HddcMainafsvyregionVO implements Serializable {

    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 送样总数
     */
    @Excel(name = "送样总数",orderNum = "18")
    private Integer samplecount;
    /**
     * 描述信息
     */
    @Excel(name = "描述信息",orderNum = "29")
    private String description;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 研究断层总数
     */
    @Excel(name = "研究断层总数",orderNum = "8")
    private Integer studiedfaultcount;
    /**
     * 显示码
     */
    @Excel(name = "显示码",orderNum = "30")
    private String showcode;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 地质填图面积 [km²]
     */
    @Excel(name = "地质填图面积",orderNum = "7")
    private Integer mappingarea;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 主键
     */
    private String uuid;
    /**
     * 遥感影像处理数目
     */
    @Excel(name = "遥感影像处理数目",orderNum = "10")
    private Integer rsprocess;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 获得测试结果样品数
     */
    @Excel(name = "获得测试结果样品数",orderNum = "19")
    private Integer datingsamplecount;
    /**
     * 是否开展地震危害性评价
     */
    @Excel(name = "是否开展地震危害性评价",orderNum = "27")
    private Integer sda;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 探槽数
     */
    @Excel(name = "探槽数",orderNum = "12")
    private Integer trenchcount;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）",orderNum = "3")
    private String area;
    /**
     * 项目名称
     */
    @Excel(name = "项目名称",orderNum = "6")
    private String projectname;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 地球物理探测工程数
     */
    @Excel(name = "地球物理探测工程数",orderNum = "23")
    private Integer geophysicalsvyprojectcount;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 市
     */
    @Excel(name = "市",orderNum = "3")
    private String city;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 省
     */
    @Excel(name = "省",orderNum = "1")
    private String province;
    /**
     * 是否火山地质调查填图
     */
    @Excel(name = "是否火山地质调查填图",orderNum = "25")
    private Integer isvolcanic;
    /**
     * 地震监测工程数
     */
    @Excel(name = "地震监测工程数",orderNum = "21")
    private Integer seismicprojectcount;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 编号
     */

    private String objectCode;
    /**
     * 备注
     */
    @Excel(name = "备注",orderNum = "31")
    private String remark;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 村
     */
    private String village;
    /**
     * 钻孔数
     */
    @Excel(name = "钻孔数",orderNum = "14")
    private Integer drillcount;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 微地貌测量工程总数
     */
    @Excel(name = "微地貌测量工程总数",orderNum = "20")
    private Integer geomorphysvyprojectcount;
    /**
     * 备注
     */
    private String commentInfo;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 钻孔进尺 [米]
     */
    @Excel(name = "钻孔进尺 [米]",orderNum = "15")
    private Integer drilllength;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 项目名称
     */
    private String proName;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 野外观测点数
     */
    @Excel(name = "野外观测点数",orderNum = "11")
    private Integer fieldsvyptcount;
    /**
     * 地形变监测工程数
     */
    @Excel(name = "地形变监测工程数",orderNum = "22")
    private Integer crustaldfmprojectcount;
    /**
     * 地球化学探测工程数
     */
    @Excel(name = "地球化学探测工程数",orderNum = "24")
    private Integer geochemicalprojectcount;
    /**
     * 地球物理测井数
     */
    @Excel(name = "地球物理测井数",orderNum = "16")
    private Integer gphwellcount;
    /**
     * 活动断层条数
     */
    @Excel(name = "活动断层条数",orderNum = "9")
    private Integer afaultcount;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 是否开展断层三维数值模拟
     */
    @Excel(name = "是否开展断层三维数值模拟",orderNum = "28")
    private Integer isnumsimulation;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 是否开展地震危险性评价
     */
    @Excel(name = "是否开展地震危险性评价",orderNum = "26")
    private Integer sra;
    /**
     * 探测总土方量 [m³]
     */
    @Excel(name = "探测总土方量 [m³]",orderNum = "13")
    private Double trenchvolume;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 编号
     */
    @Excel(name = "编号",orderNum = "4")
    private String id;
    /**
     * 目标区编号
     */
    @Excel(name = "目标区编号",orderNum = "5")
    private String targetregionid;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 采集样品总数
     */
    @Excel(name = "采集样品总数",orderNum = "17")
    private Integer collectedsamplecount;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段30
     */
    private String extends30;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String proNameName;
    private String taskNameName;
    private Integer isvolcanicName;
    private Integer sraName;
    private Integer sdaName;
    private Integer isnumsimulationName;

    private String rowNum;
    private String errorMsg;
}