package com.css.zfzx.sjcj.modules.hddcGMInterpretationLine.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcGMInterpretationLine.repository.entity.HddcGminterpretationlineEntity;
import com.css.zfzx.sjcj.modules.hddcGMInterpretationLine.viewobjects.HddcGminterpretationlineQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-26
 */

public interface HddcGminterpretationlineService {

    public JSONObject queryHddcGminterpretationlines(HddcGminterpretationlineQueryParams queryParams, int curPage, int pageSize);

    public HddcGminterpretationlineEntity getHddcGminterpretationline(String id);

    public HddcGminterpretationlineEntity saveHddcGminterpretationline(HddcGminterpretationlineEntity hddcGminterpretationline);

    public HddcGminterpretationlineEntity updateHddcGminterpretationline(HddcGminterpretationlineEntity hddcGminterpretationline);

    public void deleteHddcGminterpretationlines(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGminterpretationlineQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
