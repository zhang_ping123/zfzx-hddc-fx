package com.css.zfzx.sjcj.modules.hddcGeoProfileLine.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Data
public class HddcGeoprofilelineVO implements Serializable {

    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 所属成果图比例尺（分母）
     */
    @Excel(name = "所属成果图比例尺（分母）", orderNum = "4")
    private Integer scale;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 野外编号（图切不填）
     */
    @Excel(name = "野外编号（图切不填）", orderNum = "5")
    private String fieldid;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 编号
     */
    private String id;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 村
     */
    private String village;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 剖面观测目的
     */
    @Excel(name = "剖面观测目的", orderNum = "6")
    private String purpose;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备注
     */
    private String remark;
    /**
     * 剖面线类型
     */
    @Excel(name = "剖面线类型", orderNum = "7")
    private Integer profiletype;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 乡
     */
    private String town;
    /**
     * 剖面图原始文件编号
     */
    @Excel(name = "剖面图原始文件编号", orderNum = "8")
    private String geoprofileArwid;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "9")
    private String commentInfo;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 剖面线名称
     */
    @Excel(name = "剖面线名称", orderNum = "10")
    private String name;
    /**
     * 剖面图文件编号
     */
    @Excel(name = "剖面图文件编号", orderNum = "11")
    private String geoprofileAiid;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段18
     */
    private String extends18;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer profiletypeName;
    private String rowNum;
    private String errorMsg;
}