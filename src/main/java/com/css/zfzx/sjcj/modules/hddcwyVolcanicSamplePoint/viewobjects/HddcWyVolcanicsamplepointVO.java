package com.css.zfzx.sjcj.modules.hddcwyVolcanicSamplePoint.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-12-02
 */
@Data
public class HddcWyVolcanicsamplepointVO implements Serializable {


    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 野外编号
     */
    @Excel(name = "野外编号", orderNum = "10")
    private String fieldid;
    /**
     * 村
     */
    private String village;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "12")
    private String remark;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "3")
    private String city;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 任务名称
     */
    @Excel(name="任务名称",orderNum = "9")
    private String taskName;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "4")
    private String area;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 采样点符号
     */
    @Excel(name="采样点符号",orderNum = "14")
    private Integer symbolinfo;
    /**
     * 乡
     */
    @Excel(name="详细地址",orderNum = "5")
    private String town;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备注
     */
    private String commentInfo;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 火山调查观测点编号
     */
    @Excel(name = "火山调查观测点编号", orderNum = "13")
    private String volcanicsvypointid;
    /**
     * 项目名称
     */
    @Excel(name="项目名称",orderNum = "8")
    private String projectName;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "2")
    private String province;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 是否单次采样
     */
    @Excel(name = "是否单次采样", orderNum = "15")
    private Integer issinglesample;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 采样点编号
     */
    @Excel(name = "采样点符号", orderNum = "1")
    private String id;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 标注名称
     */
    @Excel(name = "标注名称", orderNum = "11")
    private String labelinfo;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 经度
     */
    @Excel(name="经度",orderNum = "6")
    private Double lon;
    /**
     * 维度
     */
    @Excel(name="维度",orderNum = "7")
    private Double lat;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String rowNum;
    private String errorMsg;

}