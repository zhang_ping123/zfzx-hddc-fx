package com.css.zfzx.sjcj.modules.hddcGeophySvyPoint.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Data
public class HddcGeophysvypointVO implements Serializable {

    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 测点桩号
     */
    @Excel(name = "测点桩号", orderNum = "4")
    private Integer milestonenum;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 测点经度
     */
    @Excel(name = "测点经度", orderNum = "5")
    private Double lon;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 村
     */
    private String village;
    /**
     * 探测方法
     */
    @Excel(name = "探测方法", orderNum = "6")
    private Integer svymethod;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 拐点标注名称
     */
    @Excel(name = "拐点标注名称", orderNum = "7")
    private String labelinfo;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 测点编号
     */
    private String id;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 野外编号
     */
    @Excel(name = "野外编号", orderNum = "8")
    private String fieldid;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 高程 [米]
     */
    @Excel(name = "高程 [米]", orderNum = "9")
    private Double elevation;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 测点描述
     */
    @Excel(name = "测点描述", orderNum = "10")
    private String svypointdescription;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 测线编号
     */
    @Excel(name = "测线编号", orderNum = "11")
    private String svylineid;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "12")
    private String commentInfo;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 测点纬度
     */
    @Excel(name = "测点纬度", orderNum = "13")
    private Double lat;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String svypointdescriptionName;
    private Integer svymethodName;
    private String rowNum;
    private String errorMsg;
}