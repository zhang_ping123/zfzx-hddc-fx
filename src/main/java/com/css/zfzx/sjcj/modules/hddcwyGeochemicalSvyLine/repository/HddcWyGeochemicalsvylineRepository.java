package com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyLine.repository;

import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyLine.repository.entity.HddcWyGeochemicalsvylineEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-02
 */
public interface HddcWyGeochemicalsvylineRepository extends JpaRepository<HddcWyGeochemicalsvylineEntity, String> {

    List<HddcWyGeochemicalsvylineEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    List<HddcWyGeochemicalsvylineEntity> findAllByCreateUserAndIsValid(String userId, String isValid);



    @Query(nativeQuery = true, value = "select * from hddc_wy_geochemicalsvyline where is_valid!=0 project_name in :projectIds")
    List<HddcWyGeochemicalsvylineEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_geochemicalsvyline where task_name in :projectIds")
    List<HddcWyGeochemicalsvylineEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);
}
