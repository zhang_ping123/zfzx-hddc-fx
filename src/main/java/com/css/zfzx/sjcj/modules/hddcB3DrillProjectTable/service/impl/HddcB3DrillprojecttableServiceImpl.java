package com.css.zfzx.sjcj.modules.hddcB3DrillProjectTable.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltVO;
import com.css.zfzx.sjcj.modules.hddcB3DrillProjectTable.repository.HddcB3DrillprojecttableNativeRepository;
import com.css.zfzx.sjcj.modules.hddcB3DrillProjectTable.repository.HddcB3DrillprojecttableRepository;
import com.css.zfzx.sjcj.modules.hddcB3DrillProjectTable.repository.entity.HddcB3DrillprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB3DrillProjectTable.service.HddcB3DrillprojecttableService;
import com.css.zfzx.sjcj.modules.hddcB3DrillProjectTable.viewobjects.HddcB3DrillprojecttableQueryParams;
import com.css.zfzx.sjcj.modules.hddcB3DrillProjectTable.viewobjects.HddcB3DrillprojecttableVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Service
public class HddcB3DrillprojecttableServiceImpl implements HddcB3DrillprojecttableService {

	@Autowired
    private HddcB3DrillprojecttableRepository hddcB3DrillprojecttableRepository;
    @Autowired
    private HddcB3DrillprojecttableNativeRepository hddcB3DrillprojecttableNativeRepository;

    @Override
    public JSONObject queryHddcB3Drillprojecttables(HddcB3DrillprojecttableQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcB3DrillprojecttableEntity> hddcB3DrillprojecttablePage = this.hddcB3DrillprojecttableNativeRepository.queryHddcB3Drillprojecttables(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcB3DrillprojecttablePage);
        return jsonObject;
    }


    @Override
    public HddcB3DrillprojecttableEntity getHddcB3Drillprojecttable(String id) {
        HddcB3DrillprojecttableEntity hddcB3Drillprojecttable = this.hddcB3DrillprojecttableRepository.findById(id).orElse(null);
         return hddcB3Drillprojecttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB3DrillprojecttableEntity saveHddcB3Drillprojecttable(HddcB3DrillprojecttableEntity hddcB3Drillprojecttable) {
        String uuid = UUIDGenerator.getUUID();
        hddcB3Drillprojecttable.setUuid(uuid);
        hddcB3Drillprojecttable.setCreateUser(PlatformSessionUtils.getUserId());
        hddcB3Drillprojecttable.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB3DrillprojecttableRepository.save(hddcB3Drillprojecttable);
        return hddcB3Drillprojecttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB3DrillprojecttableEntity updateHddcB3Drillprojecttable(HddcB3DrillprojecttableEntity hddcB3Drillprojecttable) {
        HddcB3DrillprojecttableEntity entity = hddcB3DrillprojecttableRepository.findById(hddcB3Drillprojecttable.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcB3Drillprojecttable);
        hddcB3Drillprojecttable.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcB3Drillprojecttable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB3DrillprojecttableRepository.save(hddcB3Drillprojecttable);
        return hddcB3Drillprojecttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcB3Drillprojecttables(List<String> ids) {
        List<HddcB3DrillprojecttableEntity> hddcB3DrillprojecttableList = this.hddcB3DrillprojecttableRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcB3DrillprojecttableList) && hddcB3DrillprojecttableList.size() > 0) {
            for(HddcB3DrillprojecttableEntity hddcB3Drillprojecttable : hddcB3DrillprojecttableList) {
                this.hddcB3DrillprojecttableRepository.delete(hddcB3Drillprojecttable);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcB3DrillprojecttableQueryParams queryParams, HttpServletResponse response) {
        List<HddcB3DrillprojecttableEntity> yhDisasterEntities = hddcB3DrillprojecttableNativeRepository.exportYhDisasters(queryParams);
        List<HddcB3DrillprojecttableVO> list=new ArrayList<>();
        for (HddcB3DrillprojecttableEntity entity:yhDisasterEntities) {
            HddcB3DrillprojecttableVO yhDisasterVO=new HddcB3DrillprojecttableVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"钻探工程表","钻探工程表",HddcB3DrillprojecttableVO.class,"钻探工程表.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcB3DrillprojecttableVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcB3DrillprojecttableVO.class, params);
            List<HddcB3DrillprojecttableVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcB3DrillprojecttableVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcB3DrillprojecttableVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcB3DrillprojecttableVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcB3DrillprojecttableEntity yhDisasterEntity = new HddcB3DrillprojecttableEntity();
            HddcB3DrillprojecttableVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcB3Drillprojecttable(yhDisasterEntity);
        }
    }

}
