package com.css.zfzx.sjcj.modules.hddcStratigraphy25LinePre.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcStratigraphy25LinePre.repository.entity.HddcStratigraphy25linepreEntity;
import com.css.zfzx.sjcj.modules.hddcStratigraphy25LinePre.viewobjects.HddcStratigraphy25linepreQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */

public interface HddcStratigraphy25linepreService {

    public JSONObject queryHddcStratigraphy25linepres(HddcStratigraphy25linepreQueryParams queryParams, int curPage, int pageSize);

    public HddcStratigraphy25linepreEntity getHddcStratigraphy25linepre(String id);

    public HddcStratigraphy25linepreEntity saveHddcStratigraphy25linepre(HddcStratigraphy25linepreEntity hddcStratigraphy25linepre);

    public HddcStratigraphy25linepreEntity updateHddcStratigraphy25linepre(HddcStratigraphy25linepreEntity hddcStratigraphy25linepre);

    public void deleteHddcStratigraphy25linepres(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    String findByDictCodeAndDictItemCode(String dictCode, String dictItemCode);

    void exportFile(HddcStratigraphy25linepreQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
