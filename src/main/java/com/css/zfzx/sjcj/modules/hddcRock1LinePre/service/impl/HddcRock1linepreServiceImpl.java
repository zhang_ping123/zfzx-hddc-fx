package com.css.zfzx.sjcj.modules.hddcRock1LinePre.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltVO;
import com.css.zfzx.sjcj.modules.hddcRock1LinePre.repository.HddcRock1linepreNativeRepository;
import com.css.zfzx.sjcj.modules.hddcRock1LinePre.repository.HddcRock1linepreRepository;
import com.css.zfzx.sjcj.modules.hddcRock1LinePre.repository.entity.HddcRock1linepreEntity;
import com.css.zfzx.sjcj.modules.hddcRock1LinePre.service.HddcRock1linepreService;
import com.css.zfzx.sjcj.modules.hddcRock1LinePre.viewobjects.HddcRock1linepreQueryParams;
import com.css.zfzx.sjcj.modules.hddcRock1LinePre.viewobjects.HddcRock1linepreVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-26
 */
@Service
public class HddcRock1linepreServiceImpl implements HddcRock1linepreService {

	@Autowired
    private HddcRock1linepreRepository hddcRock1linepreRepository;
    @Autowired
    private HddcRock1linepreNativeRepository hddcRock1linepreNativeRepository;

    @Override
    public JSONObject queryHddcRock1linepres(HddcRock1linepreQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcRock1linepreEntity> hddcRock1lineprePage = this.hddcRock1linepreNativeRepository.queryHddcRock1linepres(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcRock1lineprePage);
        return jsonObject;
    }


    @Override
    public HddcRock1linepreEntity getHddcRock1linepre(String id) {
        HddcRock1linepreEntity hddcRock1linepre = this.hddcRock1linepreRepository.findById(id).orElse(null);
         return hddcRock1linepre;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcRock1linepreEntity saveHddcRock1linepre(HddcRock1linepreEntity hddcRock1linepre) {
        String uuid = UUIDGenerator.getUUID();
        hddcRock1linepre.setUuid(uuid);
        hddcRock1linepre.setCreateUser(PlatformSessionUtils.getUserId());
        hddcRock1linepre.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcRock1linepreRepository.save(hddcRock1linepre);
        return hddcRock1linepre;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcRock1linepreEntity updateHddcRock1linepre(HddcRock1linepreEntity hddcRock1linepre) {
        HddcRock1linepreEntity entity = hddcRock1linepreRepository.findById(hddcRock1linepre.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcRock1linepre);
        hddcRock1linepre.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcRock1linepre.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcRock1linepreRepository.save(hddcRock1linepre);
        return hddcRock1linepre;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcRock1linepres(List<String> ids) {
        List<HddcRock1linepreEntity> hddcRock1linepreList = this.hddcRock1linepreRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcRock1linepreList) && hddcRock1linepreList.size() > 0) {
            for(HddcRock1linepreEntity hddcRock1linepre : hddcRock1linepreList) {
                this.hddcRock1linepreRepository.delete(hddcRock1linepre);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcRock1linepreQueryParams queryParams, HttpServletResponse response) {
        List<HddcRock1linepreEntity> yhDisasterEntities = hddcRock1linepreNativeRepository.exportYhDisasters(queryParams);
        List<HddcRock1linepreVO> list=new ArrayList<>();
        for (HddcRock1linepreEntity entity:yhDisasterEntities) {
            HddcRock1linepreVO yhDisasterVO=new HddcRock1linepreVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"1：1万底图岩体线-线","1：1万底图岩体线-线",HddcRock1linepreVO.class,"1：1万底图岩体线-线.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcRock1linepreVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcRock1linepreVO.class, params);
            List<HddcRock1linepreVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcRock1linepreVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcRock1linepreVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcRock1linepreVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcRock1linepreEntity yhDisasterEntity = new HddcRock1linepreEntity();
            HddcRock1linepreVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcRock1linepre(yhDisasterEntity);
        }
    }

}
