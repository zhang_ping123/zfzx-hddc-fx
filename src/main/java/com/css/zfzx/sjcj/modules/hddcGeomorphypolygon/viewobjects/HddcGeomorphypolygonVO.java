package com.css.zfzx.sjcj.modules.hddcGeomorphypolygon.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-12-09
 */
@Data
public class HddcGeomorphypolygonVO implements Serializable {

    /**
     * 地表破裂（断塞塘等）宽 [米]
     */
    private Double width;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 地表破裂（断塞塘等）长 [米]
     */
    @Excel(name = "地表破裂（断塞塘等）长 [米]",orderNum = "7")
    private Double length;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 最大垂直位移 [米]
     */
    @Excel(name = "最大垂直位移 [米]",orderNum = "10")
    private Double maxverticaldisplacement;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 地貌符号基础位
     */
    @Excel(name = "地貌符号基础位",orderNum = "21")
    private String nsb1;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 照片镜向
     */
    @Excel(name = "照片镜向",orderNum = "19")
    private Integer photoviewingto;
    /**
     * 照片原始文件编号
     */
    @Excel(name = "照片原始文件编号",orderNum = "18")
    private String photoArwid;
    /**
     * 任务id
     */
    private String taskId;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 地貌符号下标位
     */
    @Excel(name = "地貌符号下标位",orderNum = "22")
    private String nsb2;
    /**
     * 照片文件编号
     */
    @Excel(name = "照片文件编号",orderNum = "17")
    private String photoAiid;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 地貌符号上标位
     */
    @Excel(name = "地貌符号上标位",orderNum = "23")
    private String nsb3;
    /**
     * 项目id
     */
    private String projectId;
    /**
     * 地表破裂（断塞塘等）高/深 [米]
     */
    @Excel(name = "地表破裂（断塞塘等）高/深 [米]",orderNum = "8")
    private Double height;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 形成时代
     */
    @Excel(name = "形成时代",orderNum = "14")
    private Integer createdate;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 最大水平位移 [米]
     */
    @Excel(name = "最大水平位移 [米]", orderNum = "11")
    private Double maxhorizenoffset;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 描述
     */
    @Excel(name = "描述",orderNum = "24")
    private String commentInfo;
    /**
     * 地貌描述
     */
    @Excel(name = "地貌描述",orderNum = "6")
    private String geomorphydescription;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 地貌类型
     */
    @Excel(name = "地貌类型",orderNum = "5")
    private String geomorphytype;
    /**
     * 是否为已知地震的地表破裂
     */
    @Excel(name = "是否为已知地震的地表破裂",orderNum = "15")
    private Integer issurfacerupturebelt;
    /**
     * 编号
     */
    private String id;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 地貌代码
     */
    private String geomorphycode;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 性质
     */
    @Excel(name = "性质",orderNum = "13")
    private String feature;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 地震地表破裂类型
     */
    @Excel(name = "地震地表破裂类型",orderNum = "16")
    private Integer fracturetype;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 最大水平/张缩位移 [米]
     */
    @Excel(name = "最大水平/张缩位移 [米]",orderNum = "12")
    private Double maxtdisplacement;
    /**
     * 地貌名称
     */
    @Excel(name = "地貌名称",orderNum = "4")
    private String geomorphyname;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 拍摄者
     */
    @Excel(name = "拍摄者",orderNum = "20")
    private String photographer;
    /**
     * 备选字段17
     */
    private String extends17;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String projectNameName;
    private String taskNameName;
    private Integer issurfacerupturebeltName;
    private Integer photoviewingtoName;
    private Integer createdateName;
    private Integer fracturetypeName;
    private String rowNum;
    private String errorMsg;
}