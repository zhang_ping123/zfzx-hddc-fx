package com.css.zfzx.sjcj.modules.hddcStratigraphy5LinePre.repository;

import com.css.zfzx.sjcj.modules.hddcStratigraphy5LinePre.repository.entity.HddcStratigraphy5linepreEntity;
import com.css.zfzx.sjcj.modules.hddcStratigraphy5LinePre.viewobjects.HddcStratigraphy5linepreQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcStratigraphy5linepreNativeRepository {

    Page<HddcStratigraphy5linepreEntity> queryHddcStratigraphy5linepres(HddcStratigraphy5linepreQueryParams queryParams, int curPage, int pageSize);

    List<HddcStratigraphy5linepreEntity> exportYhDisasters(HddcStratigraphy5linepreQueryParams queryParams);
}
