package com.css.zfzx.sjcj.modules.hddcVolcanicSvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcVolcanicSvyPoint.repository.entity.HddcVolcanicsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcVolcanicSvyPoint.viewobjects.HddcVolcanicsvypointQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
public interface HddcVolcanicsvypointNativeRepository {

    Page<HddcVolcanicsvypointEntity> queryHddcVolcanicsvypoints(HddcVolcanicsvypointQueryParams queryParams, int curPage, int pageSize);

    List<HddcVolcanicsvypointEntity> exportYhDisasters(HddcVolcanicsvypointQueryParams queryParams);
}
