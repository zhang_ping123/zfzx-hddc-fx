package com.css.zfzx.sjcj.modules.hddcB1GeomorLnHasGeoSvyPt.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.bpm.platform.utils.PlatformPageUtils;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnHasGeoSvyPt.repository.entity.HddcB1GeomorlnhasgeosvyptEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnHasGeoSvyPt.service.HddcB1GeomorlnhasgeosvyptService;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnHasGeoSvyPt.viewobjects.HddcB1GeomorlnhasgeosvyptQueryParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcB1Geomorlnhasgeosvypts")
public class HddcB1GeomorlnhasgeosvyptController {
    @Autowired
    private HddcB1GeomorlnhasgeosvyptService hddcB1GeomorlnhasgeosvyptService;

    @GetMapping("/queryHddcB1Geomorlnhasgeosvypts")
    public RestResponse queryHddcB1Geomorlnhasgeosvypts(HttpServletRequest request, HddcB1GeomorlnhasgeosvyptQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcB1GeomorlnhasgeosvyptService.queryHddcB1Geomorlnhasgeosvypts(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcB1Geomorlnhasgeosvypt(@PathVariable String id) {
        RestResponse response = null;
        try{
            HddcB1GeomorlnhasgeosvyptEntity hddcB1Geomorlnhasgeosvypt = hddcB1GeomorlnhasgeosvyptService.getHddcB1Geomorlnhasgeosvypt(id);
            response = RestResponse.succeed(hddcB1Geomorlnhasgeosvypt);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcB1Geomorlnhasgeosvypt(@RequestBody HddcB1GeomorlnhasgeosvyptEntity hddcB1Geomorlnhasgeosvypt) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB1GeomorlnhasgeosvyptService.saveHddcB1Geomorlnhasgeosvypt(hddcB1Geomorlnhasgeosvypt);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcB1Geomorlnhasgeosvypt(@RequestBody HddcB1GeomorlnhasgeosvyptEntity hddcB1Geomorlnhasgeosvypt)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB1GeomorlnhasgeosvyptService.updateHddcB1Geomorlnhasgeosvypt(hddcB1Geomorlnhasgeosvypt);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcB1Geomorlnhasgeosvypts(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB1GeomorlnhasgeosvyptService.deleteHddcB1Geomorlnhasgeosvypts(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcB1GeomorlnhasgeosvyptService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }
    /***
     * 导出
     * @param response
     * @return
     */
    @GetMapping("/exportFile")
    public RestResponse exportFileYhDisasters(HttpServletResponse response,
                                              @RequestParam("projectName")String projectName,
                                              @RequestParam("province") String province, @RequestParam("city")String city, @RequestParam("area")String area) {
        RestResponse responseRest = null;
        JSONObject jsonObject = new JSONObject();
        try{
            HddcB1GeomorlnhasgeosvyptQueryParams queryParams=new HddcB1GeomorlnhasgeosvyptQueryParams();
            queryParams.setArea(area);
            queryParams.setCity(city);
            queryParams.setProvince(province);
            queryParams.setProjectName(projectName);
            hddcB1GeomorlnhasgeosvyptService.exportFile(queryParams,response);
            jsonObject.put("message", "导出成功");
            responseRest = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            responseRest = RestResponse.fail(errorMessage);
        }
        return responseRest;
    }
    /**
     * 导入
     *
     * @param file
     * @param response
     * @return
     */
    @PostMapping("/importDisaster")
    public RestResponse export(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        RestResponse restResponse = null;
        try {
            String s = hddcB1GeomorlnhasgeosvyptService.exportExcel( file, response);
            restResponse = RestResponse.succeed(s);
        } catch (Exception e) {
            String errormessage = "导入失败";
            log.error(errormessage, e);
            restResponse = RestResponse.fail(errormessage);
        }
        return restResponse;
    }
}