package com.css.zfzx.sjcj.modules.hddcVolcanicSamplePoint.repository;

import com.css.zfzx.sjcj.modules.hddcVolcanicSamplePoint.repository.entity.HddcVolcanicsamplepointEntity;
import com.css.zfzx.sjcj.modules.hddcVolcanicSamplePoint.viewobjects.HddcVolcanicsamplepointQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
public interface HddcVolcanicsamplepointNativeRepository {

    Page<HddcVolcanicsamplepointEntity> queryHddcVolcanicsamplepoints(HddcVolcanicsamplepointQueryParams queryParams, int curPage, int pageSize);

    List<HddcVolcanicsamplepointEntity> exportYhDisasters(HddcVolcanicsamplepointQueryParams queryParams);
}
