package com.css.zfzx.sjcj.modules.hddcB4_SampleDataTable.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB4_SampleDataTable.repository.entity.HddcB4SampledatatableEntity;
import com.css.zfzx.sjcj.modules.hddcB4_SampleDataTable.viewobjects.HddcB4SampledatatableQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */

public interface HddcB4SampledatatableService {

    public JSONObject queryHddcB4Sampledatatables(HddcB4SampledatatableQueryParams queryParams, int curPage, int pageSize);

    public HddcB4SampledatatableEntity getHddcB4Sampledatatable(String id);

    public HddcB4SampledatatableEntity saveHddcB4Sampledatatable(HddcB4SampledatatableEntity hddcB4Sampledatatable);

    public HddcB4SampledatatableEntity updateHddcB4Sampledatatable(HddcB4SampledatatableEntity hddcB4Sampledatatable);

    public void deleteHddcB4Sampledatatables(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB4SampledatatableQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
