package com.css.zfzx.sjcj.modules.yhchemical.viewobjects;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author ly
 * @date 2020-11-04
 */
@Data
public class YhChemicalVO implements Serializable {

    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 主键ID
     */
    private String id;
    /**
     * 状态
     */
    private String status;
    /**
     * 纬度
     */
    private Double latitude;
    /**
     * 市
     */
    private String city;
    /**
     * 区
     */
    private String area;
    /**
     * 建筑等级
     */
    private String buildingLevel;
    /**
     * 原因
     */
    private String reason;
    /**
     * 省
     */
    private String province;
    /**
     * 建筑高度
     */
    private Double buildingHeight;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 经度
     */
    private Double longitude;
    /**
     * 建筑面积
     */
    private Double buildingArea;
    /**
     * 承灾体名称
     */
    private String cztName;
    /**
     * 是否有效,0:无效,1:有效
     */
    private String isValid;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 隐患等级
     */
    private String yhLevel;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String buildingLevelName;
    private String yhLevelName;
}