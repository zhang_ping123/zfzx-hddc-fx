package com.css.zfzx.sjcj.modules.yhhospital.viewobjects;

import lombok.Data;
import java.util.Date;

/**
 * @author yyd
 * @date 2020-11-03
 */
@Data
public class YhHospitalQueryParams {


    private String cztName;
    private String province;
    private String city;
    private String area;
    private String buildingLevel;
    private String yhLevel;
    private String status;
    private String createTime;
    private String createUser;

}
