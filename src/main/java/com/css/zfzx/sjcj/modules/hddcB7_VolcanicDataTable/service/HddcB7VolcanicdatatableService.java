package com.css.zfzx.sjcj.modules.hddcB7_VolcanicDataTable.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB7_VolcanicDataTable.repository.entity.HddcB7VolcanicdatatableEntity;
import com.css.zfzx.sjcj.modules.hddcB7_VolcanicDataTable.viewobjects.HddcB7VolcanicdatatableQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-26
 */

public interface HddcB7VolcanicdatatableService {

    public JSONObject queryHddcB7Volcanicdatatables(HddcB7VolcanicdatatableQueryParams queryParams, int curPage, int pageSize);

    public HddcB7VolcanicdatatableEntity getHddcB7Volcanicdatatable(String id);

    public HddcB7VolcanicdatatableEntity saveHddcB7Volcanicdatatable(HddcB7VolcanicdatatableEntity hddcB7Volcanicdatatable);

    public HddcB7VolcanicdatatableEntity updateHddcB7Volcanicdatatable(HddcB7VolcanicdatatableEntity hddcB7Volcanicdatatable);

    public void deleteHddcB7Volcanicdatatables(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB7VolcanicdatatableQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
