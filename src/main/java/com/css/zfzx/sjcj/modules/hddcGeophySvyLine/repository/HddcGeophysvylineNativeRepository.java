package com.css.zfzx.sjcj.modules.hddcGeophySvyLine.repository;

import com.css.zfzx.sjcj.modules.hddcGeophySvyLine.repository.entity.HddcGeophysvylineEntity;
import com.css.zfzx.sjcj.modules.hddcGeophySvyLine.viewobjects.HddcGeophysvylineQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
public interface HddcGeophysvylineNativeRepository {

    Page<HddcGeophysvylineEntity> queryHddcGeophysvylines(HddcGeophysvylineQueryParams queryParams, int curPage, int pageSize);

    List<HddcGeophysvylineEntity> exportYhDisasters(HddcGeophysvylineQueryParams queryParams);
}
