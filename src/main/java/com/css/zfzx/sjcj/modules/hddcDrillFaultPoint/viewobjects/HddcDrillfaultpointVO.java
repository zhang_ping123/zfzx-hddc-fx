package com.css.zfzx.sjcj.modules.hddcDrillFaultPoint.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
public class HddcDrillfaultpointVO implements Serializable {

    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 跨断层钻探剖面编号
     */
    @Excel(name = "跨断层钻探剖面编号", orderNum = "4")
    private String drillprofileid;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 错断断距
     */
    @Excel(name = "错断断距", orderNum = "5")
    private Double lastinterfacedisplace;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "6")
    private String commentInfo;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 乡
     */
    private String town;
    /**
     * 断点编号
     */
    @Excel(name = "断点编号", orderNum = "7")
    private String id;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备注
     */
    private String remark;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 探测手段
     */
    @Excel(name = "探测手段", orderNum = "8")
    private String svymethod;
    /**
     * 上覆地层年代 [ka]
     */
    @Excel(name = "上覆地层年代 [ka]", orderNum = "9")
    private String topage;
    /**
     * 上断点埋深 [米]
     */
    @Excel(name = "上断点埋深 [米]", orderNum = "10")
    private String topdepth;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 野外编号
     */
    @Excel(name = "野外编号", orderNum = "11")
    private String fieldid;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 村
     */
    private String village;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 视倾角 [度]
     */
    @Excel(name = "视倾角 [度]", orderNum = "12")
    private Integer viewclination;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 断层性质
     */
    @Excel(name = "断层性质", orderNum = "13")
    private Integer faulttype;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 断点描述
     */
    @Excel(name = "断点描述", orderNum = "14")
    private String faultpointdescription;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 视倾向 [度]
     */
    @Excel(name = "视倾向 [度]", orderNum = "15")
    private Integer viewdirection;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 断错最新地层年代 [ka]
     */
    @Excel(name = "断错最新地层年代 [ka]", orderNum = "16")
    private String lastage;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 任务ID
     */
    private String taskId;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer faulttypeName;
    private String rowNum;
    private String errorMsg;
}