package com.css.zfzx.sjcj.modules.hddcwyTrench.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyTrench.repository.entity.HddcWyTrenchEntity;
import com.css.zfzx.sjcj.modules.hddcwyTrench.viewobjects.HddcWyTrenchQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-02
 */
public interface HddcWyTrenchNativeRepository {

    Page<HddcWyTrenchEntity> queryHddcWyTrenchs(HddcWyTrenchQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyTrench(HddcAppZztCountVo queryParams);

    List<HddcWyTrenchEntity> exportTrench(HddcAppZztCountVo queryParams);
}
