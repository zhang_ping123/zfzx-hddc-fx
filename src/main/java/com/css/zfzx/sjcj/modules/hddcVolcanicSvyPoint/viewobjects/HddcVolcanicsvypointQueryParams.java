package com.css.zfzx.sjcj.modules.hddcVolcanicSvyPoint.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
@Data
public class HddcVolcanicsvypointQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String locationname;

}
