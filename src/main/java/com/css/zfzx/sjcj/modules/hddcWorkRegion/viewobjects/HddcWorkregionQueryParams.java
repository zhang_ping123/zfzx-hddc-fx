package com.css.zfzx.sjcj.modules.hddcWorkRegion.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
public class HddcWorkregionQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

}
