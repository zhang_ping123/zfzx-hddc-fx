package com.css.zfzx.sjcj.modules.hddcB7_VolcanicSvyProjectTable.viewobjects;

import com.fasterxml.jackson.annotation.JsonFormat;
import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-26
 */
@Data
public class HddcB7VolcanicsvyprojecttableVO implements Serializable {

    /**
     * 省
     */
    @Excel(name = "省",orderNum = "1")
    private String province;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 成果报告报告文件编号
     */
    @Excel(name = "成果报告报告文件编号",orderNum = "4")
    private String resultreportArid;
    /**
     * 工程名称
     */
    @Excel(name = "工程名称",orderNum = "5")
    private String name;
    /**
     * 市
     */
    @Excel(name = "市",orderNum = "2")
    private String city;
    /**
     * 送样报表原始文件编号
     */
    @Excel(name = "送样报表原始文件编号",orderNum = "6")
    private String deliverreportArwid;
    /**
     * 测试单位
     */
    @Excel(name = "测试单位",orderNum = "7")
    private String testinstitute;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 送样报告日期
     */
    @Excel(name = "送样报告日期",orderNum = "8")
    private String reportdate;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 工程编号
     */
    private String id;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 测量仪器及型号
     */
    @Excel(name = "测量仪器及型号",orderNum = "9")
    private String testapparatus;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 工作区编号
     */
    @Excel(name = "工作区编号",orderNum = "10")
    private String workregionid;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 送样者
     */
    @Excel(name = "送样者",orderNum = "11")
    private String deliverperson;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 采集样品总数
     */
    @Excel(name = "采集样品总数",orderNum = "12")
    private Integer collectedsamplecount;
    /**
     * 备注
     */
    private String remark;
    /**
     * 乡
     */
    private String town;
    /**
     * 送样单位
     */
    @Excel(name = "送样单位",orderNum = "13")
    private String deliverinstitute;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 获得测量结果样品总数
     */
    @Excel(name = "获得测量结果样品总数",orderNum = "14")
    private Integer measuringresultcount;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）",orderNum = "3")
    private String area;
    /**
     * 成果报告原始文件编号
     */
    @Excel(name = "成果报告原始文件编号",orderNum = "15")
    private String resultreportArwid;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备注
     */
    @Excel(name = "备注",orderNum = "16")
    private String commentinfo;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 测试人员
     */
    @Excel(name = "测试人员",orderNum = "17")
    private String surveyor;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 送样总数
     */
    @Excel(name = "送样总数",orderNum = "18")
    private Integer samplecount;
    /**
     * 目标区编号
     */
    @Excel(name = "目标区编号",orderNum = "19")
    private String targetregionid;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 送样报表编号
     */
    @Excel(name = "送样报表编号",orderNum = "20")
    private String deliverreportArid;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 村
     */
    private String village;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 测试报告原始文件编号
     */
    @Excel(name = "测试报告原始文件编号",orderNum = "21")
    private String testreportArwid;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 地质填图区编号
     */
    @Excel(name = "地质填图区编号",orderNum = "22")
    private String mainafsregionid;
    /**
     * 测试报告文件编号
     */
    @Excel(name = "测试报告文件编号",orderNum = "23")
    private String testreportArid;
    /**
     * 备选字段19
     */
    private String extends19;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String projectNameName;
}