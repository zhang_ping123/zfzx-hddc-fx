package com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.bpm.platform.utils.PlatformPageUtils;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.service.HddcB1GeomorlnonfractbltService;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltQueryParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcB1Geomorlnonfractblts")
public class HddcB1GeomorlnonfractbltController {
    @Autowired
    private HddcB1GeomorlnonfractbltService hddcB1GeomorlnonfractbltService;

    @GetMapping("/queryHddcB1Geomorlnonfractblts")
    public RestResponse queryHddcB1Geomorlnonfractblts(HttpServletRequest request, HddcB1GeomorlnonfractbltQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcB1GeomorlnonfractbltService.queryHddcB1Geomorlnonfractblts(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcB1Geomorlnonfractblt(@PathVariable String id) {
        RestResponse response = null;
        try{
            HddcB1GeomorlnonfractbltEntity hddcB1Geomorlnonfractblt = hddcB1GeomorlnonfractbltService.getHddcB1Geomorlnonfractblt(id);
            response = RestResponse.succeed(hddcB1Geomorlnonfractblt);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcB1Geomorlnonfractblt(@RequestBody HddcB1GeomorlnonfractbltEntity hddcB1Geomorlnonfractblt) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB1GeomorlnonfractbltService.saveHddcB1Geomorlnonfractblt(hddcB1Geomorlnonfractblt);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcB1Geomorlnonfractblt(@RequestBody HddcB1GeomorlnonfractbltEntity hddcB1Geomorlnonfractblt)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB1GeomorlnonfractbltService.updateHddcB1Geomorlnonfractblt(hddcB1Geomorlnonfractblt);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcB1Geomorlnonfractblts(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcB1GeomorlnonfractbltService.deleteHddcB1Geomorlnonfractblts(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcB1GeomorlnonfractbltService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }

    /***
     * 导出
     * @param response
     * @return
     */
    @GetMapping("/exportFile")
    public RestResponse exportFileYhDisasters(HttpServletResponse response,
                                              @RequestParam("projectName")String projectName,@RequestParam("fractbltname") String fractbltname,
                                              @RequestParam("province") String province,@RequestParam("city")String city,@RequestParam("area")String area) {
        RestResponse responseRest = null;
        JSONObject jsonObject = new JSONObject();
        try{
            HddcB1GeomorlnonfractbltQueryParams queryParams=new HddcB1GeomorlnonfractbltQueryParams();
            queryParams.setArea(area);
            queryParams.setCity(city);
            queryParams.setProvince(province);
            queryParams.setProjectName(projectName);
            queryParams.setFractbltname(fractbltname);
            hddcB1GeomorlnonfractbltService.exportFile(queryParams,response);
            jsonObject.put("message", "导出成功");
            responseRest = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            responseRest = RestResponse.fail(errorMessage);
        }
        return responseRest;
    }

    /**
     * 导入
     *
     * @param file
     * @param response
     * @return
     */
    @PostMapping("/importDisaster")
    public RestResponse export(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        RestResponse restResponse = null;
        try {
            String s = hddcB1GeomorlnonfractbltService.exportExcel( file, response);
            restResponse = RestResponse.succeed(s);
        } catch (Exception e) {
            String errormessage = "导入失败";
            log.error(errormessage, e);
            restResponse = RestResponse.fail(errormessage);
        }
        return restResponse;
    }

}