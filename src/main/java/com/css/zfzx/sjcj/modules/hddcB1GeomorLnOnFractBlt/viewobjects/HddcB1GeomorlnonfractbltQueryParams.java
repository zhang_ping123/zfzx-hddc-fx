package com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Data
public class HddcB1GeomorlnonfractbltQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String fractbltname;

}
