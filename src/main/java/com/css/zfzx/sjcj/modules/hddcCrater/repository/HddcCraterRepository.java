package com.css.zfzx.sjcj.modules.hddcCrater.repository;

import com.css.zfzx.sjcj.modules.hddcCrater.repository.entity.HddcCraterEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
public interface HddcCraterRepository extends JpaRepository<HddcCraterEntity, String> {
}
