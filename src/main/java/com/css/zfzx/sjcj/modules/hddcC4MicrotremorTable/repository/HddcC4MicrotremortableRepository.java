package com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.repository;

import com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.repository.entity.HddcC4MicrotremortableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangping
 * @date 2020-11-23
 */
public interface HddcC4MicrotremortableRepository extends JpaRepository<HddcC4MicrotremortableEntity, String> {
}
