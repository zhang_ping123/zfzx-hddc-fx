package com.css.zfzx.sjcj.modules.yhschool.viewobjects;

import lombok.Data;

/**
 * @author yyd
 * @date 2020-11-03
 */
@Data
public class YhSchoolQueryParams {


    private String cztName;
    private String province;
    private String city;
    private String area;
    private String buildingLevel;
    private String yhLevel;
    private String status;
    private String createTime;
    private String createUser;

}
