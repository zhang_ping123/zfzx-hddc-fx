package com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPoint.repository.entity.HddcWyGeologicalsvypointEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */
public interface HddcWyGeologicalsvypointRepository extends JpaRepository<HddcWyGeologicalsvypointEntity, String> {

    List<HddcWyGeologicalsvypointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    List<HddcWyGeologicalsvypointEntity> findAllByCreateUserAndIsValid(String userId,String isValid);


    @Query(nativeQuery = true, value = "select * from hddc_wy_geologicalsvypoint where is_valid!=0 project_name in :projectIds")
    List<HddcWyGeologicalsvypointEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_geologicalsvypoint where task_name in :projectIds")
    List<HddcWyGeologicalsvypointEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);


}
