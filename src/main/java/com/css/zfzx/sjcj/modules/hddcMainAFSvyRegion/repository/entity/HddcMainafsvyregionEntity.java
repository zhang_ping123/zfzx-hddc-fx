package com.css.zfzx.sjcj.modules.hddcMainAFSvyRegion.repository.entity;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-12-14
 */
@Data
@Entity
@Table(name="hddc_mainafsvyregion")
public class HddcMainafsvyregionEntity implements Serializable {

    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 送样总数
     */
    @Column(name="samplecount")
    private Integer samplecount;
    /**
     * 描述信息
     */
    @Column(name="description")
    private String description;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 研究断层总数
     */
    @Column(name="studiedfaultcount")
    private Integer studiedfaultcount;
    /**
     * 显示码
     */
    @Column(name="showcode")
    private String showcode;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 地质填图面积 [km²]
     */
    @Column(name="mappingarea")
    private Integer mappingarea;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 主键
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 遥感影像处理数目
     */
    @Column(name="rsprocess")
    private Integer rsprocess;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 获得测试结果样品数
     */
    @Column(name="datingsamplecount")
    private Integer datingsamplecount;
    /**
     * 是否开展地震危害性评价
     */
    @Column(name="sda")
    private Integer sda;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 探槽数
     */
    @Column(name="trenchcount")
    private Integer trenchcount;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 项目名称
     */
    @Column(name="projectname")
    private String projectname;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 地球物理探测工程数
     */
    @Column(name="geophysicalsvyprojectcount")
    private Integer geophysicalsvyprojectcount;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 是否火山地质调查填图
     */
    @Column(name="isvolcanic")
    private Integer isvolcanic;
    /**
     * 地震监测工程数
     */
    @Column(name="seismicprojectcount")
    private Integer seismicprojectcount;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 钻孔数
     */
    @Column(name="drillcount")
    private Integer drillcount;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 微地貌测量工程总数
     */
    @Column(name="geomorphysvyprojectcount")
    private Integer geomorphysvyprojectcount;
    /**
     * 备注
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 钻孔进尺 [米]
     */
    @Column(name="drilllength")
    private Integer drilllength;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 项目名称
     */
    @Column(name="pro_name")
    private String proName;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 野外观测点数
     */
    @Column(name="fieldsvyptcount")
    private Integer fieldsvyptcount;
    /**
     * 地形变监测工程数
     */
    @Column(name="crustaldfmprojectcount")
    private Integer crustaldfmprojectcount;
    /**
     * 地球化学探测工程数
     */
    @Column(name="geochemicalprojectcount")
    private Integer geochemicalprojectcount;
    /**
     * 地球物理测井数
     */
    @Column(name="gphwellcount")
    private Integer gphwellcount;
    /**
     * 活动断层条数
     */
    @Column(name="afaultcount")
    private Integer afaultcount;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 是否开展断层三维数值模拟
     */
    @Column(name="isnumsimulation")
    private Integer isnumsimulation;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 是否开展地震危险性评价
     */
    @Column(name="sra")
    private Integer sra;
    /**
     * 探测总土方量 [m³]
     */
    @Column(name="trenchvolume")
    private Double trenchvolume;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 编号
     */
    @Column(name="id")
    private String id;
    /**
     * 目标区编号
     */
    @Column(name="targetregionid")
    private String targetregionid;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 采集样品总数
     */
    @Column(name="collectedsamplecount")
    private Integer collectedsamplecount;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;

}

