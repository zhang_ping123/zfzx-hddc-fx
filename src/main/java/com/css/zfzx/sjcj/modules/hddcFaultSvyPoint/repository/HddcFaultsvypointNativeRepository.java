package com.css.zfzx.sjcj.modules.hddcFaultSvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcFaultSvyPoint.repository.entity.HddcFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcFaultSvyPoint.viewobjects.HddcFaultsvypointQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author lihelei
 * @date 2020-11-26
 */
public interface HddcFaultsvypointNativeRepository {

    Page<HddcFaultsvypointEntity> queryHddcFaultsvypoints(HddcFaultsvypointQueryParams queryParams, int curPage, int pageSize);

    List<HddcFaultsvypointEntity> exportYhDisasters(HddcFaultsvypointQueryParams queryParams);
}
