package com.css.zfzx.sjcj.modules.hddccjfilemanage.repository;

import com.css.zfzx.sjcj.modules.hddccjfilemanage.repository.entity.HddcFileManageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zyb
 * @date 2020-12-10
 */
public interface HddcFileManageRepository extends JpaRepository<HddcFileManageEntity, String> {
    @Query(nativeQuery = true,value="select f.* from hddc_file_manage f where f.file_id=?1")
    List<HddcFileManageEntity> findbyId(String filenumber);
}
