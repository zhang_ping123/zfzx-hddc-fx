package com.css.zfzx.sjcj.modules.hddcwyCrater.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyCrater.repository.entity.HddcWyCraterEntity;
import com.css.zfzx.sjcj.modules.hddcwyCrater.viewobjects.HddcWyCraterQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-02
 */

public interface HddcWyCraterService {

    public JSONObject queryHddcWyCraters(HddcWyCraterQueryParams queryParams, int curPage, int pageSize);

    public HddcWyCraterEntity getHddcWyCrater(String uuid);

    public HddcWyCraterEntity saveHddcWyCrater(HddcWyCraterEntity hddcWyCrater);

    public HddcWyCraterEntity updateHddcWyCrater(HddcWyCraterEntity hddcWyCrater);

    public void deleteHddcWyCraters(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    BigInteger queryHddcWyCrater(HddcAppZztCountVo queryParams);

    List<HddcWyCraterEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);

    List<HddcWyCraterEntity> findAllByCreateUserAndIsValid(String userId, String isValid);

    /**
     * 逻辑删除-根据项目id删除数据
     * @param ids
     */
    void deleteByProjectId(List<String> ids);

    /**
     * 逻辑删除-根据任务id删除数据
     * @param ids
     */
    void deleteByTaskId(List<String> ids);
}
