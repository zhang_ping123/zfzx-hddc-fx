package com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.repository.entity.HddcC4MicrotremortableEntity;
import com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.viewobjects.HddcC4MicrotremortableQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import java.util.List;

/**
 * @author zhangping
 * @date 2020-11-23
 */

public interface HddcC4MicrotremortableService {

    public JSONObject queryHddcC4Microtremortables(HddcC4MicrotremortableQueryParams queryParams, int curPage, int pageSize);

    public HddcC4MicrotremortableEntity getHddcC4Microtremortable(String id);

    public HddcC4MicrotremortableEntity saveHddcC4Microtremortable(HddcC4MicrotremortableEntity hddcC4Microtremortable);

    public HddcC4MicrotremortableEntity updateHddcC4Microtremortable(HddcC4MicrotremortableEntity hddcC4Microtremortable);

    public void deleteHddcC4Microtremortables(List<String> ids);


}
