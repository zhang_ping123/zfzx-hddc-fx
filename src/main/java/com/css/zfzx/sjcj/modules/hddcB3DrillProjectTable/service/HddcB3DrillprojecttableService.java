package com.css.zfzx.sjcj.modules.hddcB3DrillProjectTable.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB3DrillProjectTable.repository.entity.HddcB3DrillprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB3DrillProjectTable.viewobjects.HddcB3DrillprojecttableQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */

public interface HddcB3DrillprojecttableService {

    public JSONObject queryHddcB3Drillprojecttables(HddcB3DrillprojecttableQueryParams queryParams, int curPage, int pageSize);

    public HddcB3DrillprojecttableEntity getHddcB3Drillprojecttable(String id);

    public HddcB3DrillprojecttableEntity saveHddcB3Drillprojecttable(HddcB3DrillprojecttableEntity hddcB3Drillprojecttable);

    public HddcB3DrillprojecttableEntity updateHddcB3Drillprojecttable(HddcB3DrillprojecttableEntity hddcB3Drillprojecttable);

    public void deleteHddcB3Drillprojecttables(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB3DrillprojecttableQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
