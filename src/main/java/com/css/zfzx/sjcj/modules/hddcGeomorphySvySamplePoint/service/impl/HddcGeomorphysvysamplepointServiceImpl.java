package com.css.zfzx.sjcj.modules.hddcGeomorphySvySamplePoint.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvySamplePoint.repository.HddcGeomorphysvysamplepointNativeRepository;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvySamplePoint.repository.HddcGeomorphysvysamplepointRepository;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvySamplePoint.repository.entity.HddcGeomorphysvysamplepointEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvySamplePoint.service.HddcGeomorphysvysamplepointService;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvySamplePoint.viewobjects.HddcGeomorphysvysamplepointQueryParams;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvySamplePoint.viewobjects.HddcGeomorphysvysamplepointVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Service
public class HddcGeomorphysvysamplepointServiceImpl implements HddcGeomorphysvysamplepointService {

	@Autowired
    private HddcGeomorphysvysamplepointRepository hddcGeomorphysvysamplepointRepository;
    @Autowired
    private HddcGeomorphysvysamplepointNativeRepository hddcGeomorphysvysamplepointNativeRepository;

    @Override
    public JSONObject queryHddcGeomorphysvysamplepoints(HddcGeomorphysvysamplepointQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcGeomorphysvysamplepointEntity> hddcGeomorphysvysamplepointPage = this.hddcGeomorphysvysamplepointNativeRepository.queryHddcGeomorphysvysamplepoints(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcGeomorphysvysamplepointPage);
        return jsonObject;
    }


    @Override
    public HddcGeomorphysvysamplepointEntity getHddcGeomorphysvysamplepoint(String id) {
        HddcGeomorphysvysamplepointEntity hddcGeomorphysvysamplepoint = this.hddcGeomorphysvysamplepointRepository.findById(id).orElse(null);
         return hddcGeomorphysvysamplepoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeomorphysvysamplepointEntity saveHddcGeomorphysvysamplepoint(HddcGeomorphysvysamplepointEntity hddcGeomorphysvysamplepoint) {
        String uuid = UUIDGenerator.getUUID();
        hddcGeomorphysvysamplepoint.setUuid(uuid);
        hddcGeomorphysvysamplepoint.setCreateUser(PlatformSessionUtils.getUserId());
        hddcGeomorphysvysamplepoint.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeomorphysvysamplepointRepository.save(hddcGeomorphysvysamplepoint);
        return hddcGeomorphysvysamplepoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeomorphysvysamplepointEntity updateHddcGeomorphysvysamplepoint(HddcGeomorphysvysamplepointEntity hddcGeomorphysvysamplepoint) {
        HddcGeomorphysvysamplepointEntity entity = hddcGeomorphysvysamplepointRepository.findById(hddcGeomorphysvysamplepoint.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcGeomorphysvysamplepoint);
        hddcGeomorphysvysamplepoint.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcGeomorphysvysamplepoint.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeomorphysvysamplepointRepository.save(hddcGeomorphysvysamplepoint);
        return hddcGeomorphysvysamplepoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcGeomorphysvysamplepoints(List<String> ids) {
        List<HddcGeomorphysvysamplepointEntity> hddcGeomorphysvysamplepointList = this.hddcGeomorphysvysamplepointRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcGeomorphysvysamplepointList) && hddcGeomorphysvysamplepointList.size() > 0) {
            for(HddcGeomorphysvysamplepointEntity hddcGeomorphysvysamplepoint : hddcGeomorphysvysamplepointList) {
                this.hddcGeomorphysvysamplepointRepository.delete(hddcGeomorphysvysamplepoint);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcGeomorphysvysamplepointQueryParams queryParams, HttpServletResponse response) {
        List<HddcGeomorphysvysamplepointEntity> yhDisasterEntities = hddcGeomorphysvysamplepointNativeRepository.exportYhDisasters(queryParams);
        List<HddcGeomorphysvysamplepointVO> list=new ArrayList<>();
        for (HddcGeomorphysvysamplepointEntity entity:yhDisasterEntities) {
            HddcGeomorphysvysamplepointVO yhDisasterVO=new HddcGeomorphysvysamplepointVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "微地貌测量采样点-点", "微地貌测量采样点-点", HddcGeomorphysvysamplepointVO.class, "微地貌测量采样点-点.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcGeomorphysvysamplepointVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcGeomorphysvysamplepointVO.class, params);
            List<HddcGeomorphysvysamplepointVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcGeomorphysvysamplepointVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcGeomorphysvysamplepointVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcGeomorphysvysamplepointVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcGeomorphysvysamplepointEntity yhDisasterEntity = new HddcGeomorphysvysamplepointEntity();
            HddcGeomorphysvysamplepointVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcGeomorphysvysamplepoint(yhDisasterEntity);
        }
    }

}
