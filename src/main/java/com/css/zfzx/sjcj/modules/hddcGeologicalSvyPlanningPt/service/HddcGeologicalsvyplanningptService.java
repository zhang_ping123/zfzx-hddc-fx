package com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningPt.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningPt.repository.entity.HddcGeologicalsvyplanningptEntity;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningPt.viewobjects.HddcGeologicalsvyplanningptQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-07
 */

public interface HddcGeologicalsvyplanningptService {

    public JSONObject queryHddcGeologicalsvyplanningpts(HddcGeologicalsvyplanningptQueryParams queryParams, int curPage, int pageSize);

    public HddcGeologicalsvyplanningptEntity getHddcGeologicalsvyplanningpt(String id);

    public HddcGeologicalsvyplanningptEntity saveHddcGeologicalsvyplanningpt(HddcGeologicalsvyplanningptEntity hddcGeologicalsvyplanningpt);

    public HddcGeologicalsvyplanningptEntity updateHddcGeologicalsvyplanningpt(HddcGeologicalsvyplanningptEntity hddcGeologicalsvyplanningpt);

    public void deleteHddcGeologicalsvyplanningpts(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGeologicalsvyplanningptQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
