package com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.repository;

import com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.repository.entity.HddcC4MicrotremortableEntity;
import com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.viewobjects.HddcC4MicrotremortableQueryParams;
import org.springframework.data.domain.Page;

/**
 * @author zhangping
 * @date 2020-11-23
 */
public interface HddcC4MicrotremortableNativeRepository {

    Page<HddcC4MicrotremortableEntity> queryHddcC4Microtremortables(HddcC4MicrotremortableQueryParams queryParams, int curPage, int pageSize);
}
