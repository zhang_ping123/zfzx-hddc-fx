package com.css.zfzx.sjcj.modules.hddcB1GeomorPlyOnFractBlt.repository;

import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyOnFractBlt.repository.entity.HddcB1GeomorplyonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyOnFractBlt.viewobjects.HddcB1GeomorplyonfractbltQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcB1GeomorplyonfractbltNativeRepository {

    Page<HddcB1GeomorplyonfractbltEntity> queryHddcB1Geomorplyonfractblts(HddcB1GeomorplyonfractbltQueryParams queryParams, int curPage, int pageSize);

    List<HddcB1GeomorplyonfractbltEntity> exportYhDisasters(HddcB1GeomorplyonfractbltQueryParams queryParams);
}
