package com.css.zfzx.sjcj.modules.hddccjtask.viewobjects;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangping
 * @date 2020-11-26
 */
@Data
public class HddcCjTaskVO implements Serializable {

    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 人员信息
     */
    private String personIds;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 地图范围
     */
    private String mapInfos;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 市
     */
    private String city;
    /**
     * 所属项目
     */
    private String belongProject;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 区（县）
     */
    private String area;
    /**
     * 备注
     */
    private String remark;
    /**
     * 省
     */
    private String province;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 编号
     */
    private String id;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 村
     */
    private String village;
    /**
     * 乡
     */
    private String town;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String belongProjectName;
    private String personIdsName;
}