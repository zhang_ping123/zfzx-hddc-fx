package com.css.zfzx.sjcj.modules.hddcVolcanicSamplePoint.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
@Data
public class HddcVolcanicsamplepointQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String labelinfo;

}
