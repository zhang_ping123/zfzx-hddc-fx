package com.css.zfzx.sjcj.modules.hddcGeomorphySvyRegion.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyRegion.repository.HddcGeomorphysvyregionNativeRepository;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyRegion.repository.HddcGeomorphysvyregionRepository;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyRegion.repository.entity.HddcGeomorphysvyregionEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyRegion.service.HddcGeomorphysvyregionService;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyRegion.viewobjects.HddcGeomorphysvyregionQueryParams;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyRegion.viewobjects.HddcGeomorphysvyregionVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Service
public class HddcGeomorphysvyregionServiceImpl implements HddcGeomorphysvyregionService {

	@Autowired
    private HddcGeomorphysvyregionRepository hddcGeomorphysvyregionRepository;
    @Autowired
    private HddcGeomorphysvyregionNativeRepository hddcGeomorphysvyregionNativeRepository;

    @Override
    public JSONObject queryHddcGeomorphysvyregions(HddcGeomorphysvyregionQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcGeomorphysvyregionEntity> hddcGeomorphysvyregionPage = this.hddcGeomorphysvyregionNativeRepository.queryHddcGeomorphysvyregions(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcGeomorphysvyregionPage);
        return jsonObject;
    }


    @Override
    public HddcGeomorphysvyregionEntity getHddcGeomorphysvyregion(String id) {
        HddcGeomorphysvyregionEntity hddcGeomorphysvyregion = this.hddcGeomorphysvyregionRepository.findById(id).orElse(null);
         return hddcGeomorphysvyregion;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeomorphysvyregionEntity saveHddcGeomorphysvyregion(HddcGeomorphysvyregionEntity hddcGeomorphysvyregion) {
        String uuid = UUIDGenerator.getUUID();
        hddcGeomorphysvyregion.setUuid(uuid);
        hddcGeomorphysvyregion.setCreateUser(PlatformSessionUtils.getUserId());
        hddcGeomorphysvyregion.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeomorphysvyregionRepository.save(hddcGeomorphysvyregion);
        return hddcGeomorphysvyregion;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeomorphysvyregionEntity updateHddcGeomorphysvyregion(HddcGeomorphysvyregionEntity hddcGeomorphysvyregion) {
        HddcGeomorphysvyregionEntity entity = hddcGeomorphysvyregionRepository.findById(hddcGeomorphysvyregion.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcGeomorphysvyregion);
        hddcGeomorphysvyregion.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcGeomorphysvyregion.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeomorphysvyregionRepository.save(hddcGeomorphysvyregion);
        return hddcGeomorphysvyregion;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcGeomorphysvyregions(List<String> ids) {
        List<HddcGeomorphysvyregionEntity> hddcGeomorphysvyregionList = this.hddcGeomorphysvyregionRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcGeomorphysvyregionList) && hddcGeomorphysvyregionList.size() > 0) {
            for(HddcGeomorphysvyregionEntity hddcGeomorphysvyregion : hddcGeomorphysvyregionList) {
                this.hddcGeomorphysvyregionRepository.delete(hddcGeomorphysvyregion);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcGeomorphysvyregionQueryParams queryParams, HttpServletResponse response) {
        List<HddcGeomorphysvyregionEntity> yhDisasterEntities = hddcGeomorphysvyregionNativeRepository.exportYhDisasters(queryParams);
        List<HddcGeomorphysvyregionVO> list=new ArrayList<>();
        for (HddcGeomorphysvyregionEntity entity:yhDisasterEntities) {
            HddcGeomorphysvyregionVO yhDisasterVO=new HddcGeomorphysvyregionVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "微地貌测量面-面", "微地貌测量面-面", HddcGeomorphysvyregionVO.class, "微地貌测量面-面.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcGeomorphysvyregionVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcGeomorphysvyregionVO.class, params);
            List<HddcGeomorphysvyregionVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcGeomorphysvyregionVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcGeomorphysvyregionVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcGeomorphysvyregionVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcGeomorphysvyregionEntity yhDisasterEntity = new HddcGeomorphysvyregionEntity();
            HddcGeomorphysvyregionVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcGeomorphysvyregion(yhDisasterEntity);
        }
    }
}
