package com.css.zfzx.sjcj.modules.hddcFaultSvyPoint.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author lihelei
 * @date 2020-11-26
 */
@Data
public class HddcFaultsvypointVO implements Serializable {

    /**
     * 村
     */
    private String village;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 水平//张缩位移 [米]
     */
    @Excel(name = "水平//张缩位移 [米]", orderNum = "4")
    private Double tensionaldisplacement;
    /**
     * 目标断层编号
     */
    @Excel(name = "目标断层编号", orderNum = "5")
    private String targetfaultid;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 平面图原始文件编号
     */
    @Excel(name = "平面图原始文件编号", orderNum = "6")
    private String sketchArwid;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 地质调查观测点编号
     */
    @Excel(name = "地质调查观测点编号", orderNum = "7")
    private String geologicalsvypointid;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 是否修改工作底图
     */
    @Excel(name = "是否修改工作底图", orderNum = "8")
    private Integer ismodifyworkmap;
    /**
     * 数据来源
     */
    @Excel(name = "数据来源", orderNum = "9")
    private String datasource;
    /**
     * 典型剖面图图像文件编号
     */
    @Excel(name = "典型剖面图图像文件编号", orderNum = "10")
    private String typicalprofileAiid;
    /**
     * 获得测试结果样品数
     */
    @Excel(name = "获得测试结果样品数", orderNum = "11")
    private Integer datingsamplecount;
    /**
     * 走向水平位移 [米]
     */
    @Excel(name = "走向水平位移 [米]", orderNum = "12")
    private Double horizentaloffset;
    /**
     * 断层倾角 [度]
     */
    @Excel(name = "断层倾角 [度]", orderNum = "13")
    private Integer faultclination;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 拍摄者
     */
    @Excel(name = "拍摄者", orderNum = "14")
    private String photographer;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 观测点野外编号
     */
    @Excel(name = "观测点野外编号", orderNum = "15")
    private String fieldid;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 水平/张缩位移误差
     */
    @Excel(name = "水平/张缩位移误差", orderNum = "16")
    private Double tensionaldisplacementerror;
    /**
     * 产状点编号
     */
    private String id;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 比例尺（分母）
     */
    @Excel(name = "比例尺（分母）", orderNum = "17")
    private Integer scale;
    /**
     * 符号或标注旋转角度
     */
    @Excel(name = "符号或标注旋转角度", orderNum = "18")
    private Double lastangle;
    /**
     * 典型照片文件编号
     */
    @Excel(name = "典型照片文件编号", orderNum = "19")
    private String photoAiid;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 断层性质
     */
    @Excel(name = "断层性质", orderNum = "20")
    private Integer feature;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 是否在图中显示
     */
    @Excel(name = "是否在图中显示", orderNum = "21")
    private Integer isinmap;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 目标断层名称
     */
    @Excel(name = "目标断层名称", orderNum = "22")
    private String targetfaultname;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 断层倾向 [度]
     */
    @Excel(name = "断层倾向 [度]", orderNum = "23")
    private Integer measurefaultdip;
    /**
     * 送样数
     */
    @Excel(name = "送样数", orderNum = "24")
    private Integer samplecount;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 平面图文件编号
     */
    @Excel(name = "平面图文件编号", orderNum = "25")
    private String sketchAiid;
    /**
     * 走向水平位移误差
     */
    @Excel(name = "走向水平位移误差", orderNum = "26")
    private Double horizentaloffseterror;
    /**
     * 典型剖面图原始文件编号
     */
    @Excel(name = "典型剖面图原始文件编号", orderNum = "27")
    private String typicalprofileArwid;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 垂直位移 [米]
     */
    @Excel(name = "垂直位移 [米]", orderNum = "28")
    private Double verticaldisplacement;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "29")
    private String commentInfo;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 典型照片原始文件编号
     */
    @Excel(name = "典型照片原始文件编号", orderNum = "30")
    private String photoArwid;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 目标断层来源
     */
    @Excel(name = "目标断层来源", orderNum = "31")
    private String targetfaultsource;
    /**
     * 断层走向 [度]
     */
    @Excel(name = "断层走向 [度]", orderNum = "32")
    private Integer faultstrike;
    /**
     * 乡
     */
    private String town;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 垂直位移误差
     */
    @Excel(name = "垂直位移误差", orderNum = "33")
    private Double verticaldisplacementerror;
    /**
     * 照片镜向
     */
    @Excel(name = "照片镜向", orderNum = "34")
    private Integer photoviewingto;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段24
     */
    private String extends24;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer featureName;
    private String targetfaultsourceName;
    private Integer photoviewingtoName;
    private String rowNum;
    private String errorMsg;
}