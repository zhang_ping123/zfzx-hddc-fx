package com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningPt.service.impl;


import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningPt.repository.HddcGeologicalsvyplanningptRepository;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningPt.repository.entity.HddcGeologicalsvyplanningptEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningPt.repository.HddcWyGeologicalsvyplanningptNativeRepository;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningPt.repository.HddcWyGeologicalsvyplanningptRepository;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningPt.repository.entity.HddcWyGeologicalsvyplanningptEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningPt.service.HddcWyGeologicalsvyplanningptService;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningPt.viewobjects.HddcWyGeologicalsvyplanningptQueryParams;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigInteger;
import java.util.*;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Service
public class HddcWyGeologicalsvyplanningptServiceImpl implements HddcWyGeologicalsvyplanningptService {

	@Autowired
    private HddcWyGeologicalsvyplanningptRepository hddcWyGeologicalsvyplanningptRepository;
    @Autowired
    private HddcWyGeologicalsvyplanningptNativeRepository hddcWyGeologicalsvyplanningptNativeRepository;
    @Autowired
    private HddcGeologicalsvyplanningptRepository hddcGeologicalsvyplanningptRepository;

    @Override
    public JSONObject queryHddcWyGeologicalsvyplanningpts(HddcWyGeologicalsvyplanningptQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcWyGeologicalsvyplanningptEntity> hddcWyGeologicalsvyplanningptPage = this.hddcWyGeologicalsvyplanningptNativeRepository.queryHddcWyGeologicalsvyplanningpts(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcWyGeologicalsvyplanningptPage);
        return jsonObject;
    }


    @Override
    public HddcWyGeologicalsvyplanningptEntity getHddcWyGeologicalsvyplanningpt(String uuid) {
        HddcWyGeologicalsvyplanningptEntity hddcWyGeologicalsvyplanningpt = this.hddcWyGeologicalsvyplanningptRepository.findById(uuid).orElse(null);
         return hddcWyGeologicalsvyplanningpt;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyGeologicalsvyplanningptEntity saveHddcWyGeologicalsvyplanningpt(HddcWyGeologicalsvyplanningptEntity hddcWyGeologicalsvyplanningpt) {
        String uuid = UUIDGenerator.getUUID();
        hddcWyGeologicalsvyplanningpt.setUuid(uuid);
        if(StringUtils.isEmpty(hddcWyGeologicalsvyplanningpt.getCreateUser())){
            hddcWyGeologicalsvyplanningpt.setCreateUser(PlatformSessionUtils.getUserId());
        }
        hddcWyGeologicalsvyplanningpt.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        hddcWyGeologicalsvyplanningpt.setIsValid("1");

        HddcGeologicalsvyplanningptEntity hddcGeologicalsvyplanningptEntity=new HddcGeologicalsvyplanningptEntity();
        BeanUtils.copyProperties(hddcWyGeologicalsvyplanningpt,hddcGeologicalsvyplanningptEntity);
        hddcGeologicalsvyplanningptEntity.setExtends4(hddcWyGeologicalsvyplanningpt.getTown());
        hddcGeologicalsvyplanningptEntity.setExtends5(String.valueOf(hddcWyGeologicalsvyplanningpt.getLon()));
        hddcGeologicalsvyplanningptEntity.setExtends6(String.valueOf(hddcWyGeologicalsvyplanningpt.getLat()));
        this.hddcGeologicalsvyplanningptRepository.save(hddcGeologicalsvyplanningptEntity);


        this.hddcWyGeologicalsvyplanningptRepository.save(hddcWyGeologicalsvyplanningpt);
        return hddcWyGeologicalsvyplanningpt;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWyGeologicalsvyplanningptEntity updateHddcWyGeologicalsvyplanningpt(HddcWyGeologicalsvyplanningptEntity hddcWyGeologicalsvyplanningpt) {
        HddcWyGeologicalsvyplanningptEntity entity = hddcWyGeologicalsvyplanningptRepository.findById(hddcWyGeologicalsvyplanningpt.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcWyGeologicalsvyplanningpt);
        hddcWyGeologicalsvyplanningpt.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcWyGeologicalsvyplanningpt.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcWyGeologicalsvyplanningptRepository.save(hddcWyGeologicalsvyplanningpt);
        return hddcWyGeologicalsvyplanningpt;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcWyGeologicalsvyplanningpts(List<String> ids) {
        List<HddcWyGeologicalsvyplanningptEntity> hddcWyGeologicalsvyplanningptList = this.hddcWyGeologicalsvyplanningptRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcWyGeologicalsvyplanningptList) && hddcWyGeologicalsvyplanningptList.size() > 0) {
            for(HddcWyGeologicalsvyplanningptEntity hddcWyGeologicalsvyplanningpt : hddcWyGeologicalsvyplanningptList) {
                this.hddcWyGeologicalsvyplanningptRepository.delete(hddcWyGeologicalsvyplanningpt);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public BigInteger queryHddcWyGeologicalsvyplanningpt(HddcAppZztCountVo queryParams) {
        BigInteger bigInteger = hddcWyGeologicalsvyplanningptNativeRepository.queryHddcWyGeologicalsvyplanningpt(queryParams);
        return bigInteger;
    }

    @Override
    public List<HddcWyGeologicalsvyplanningptEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid) {
        List<HddcWyGeologicalsvyplanningptEntity> list = hddcWyGeologicalsvyplanningptRepository.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, isValid);
        return list;
    }
    @Override
    public List<HddcWyGeologicalsvyplanningptEntity> findAllByCreateUserAndIsValid(String userId,String isValid) {
        List<HddcWyGeologicalsvyplanningptEntity> list = hddcWyGeologicalsvyplanningptRepository.findAllByCreateUserAndIsValid(userId,isValid);
        return list;
    }


    /**
     * 逻辑删除-根据项目id删除数据
     * @param projectIds
     */
    @Override
    public void deleteByProjectId(List<String> projectIds) {
        List<HddcWyGeologicalsvyplanningptEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyGeologicalsvyplanningptRepository.queryHddcA1InvrgnhasmaterialtablesByProjectId(projectIds);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyGeologicalsvyplanningptEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyGeologicalsvyplanningptRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }
    }

    /**
     * 逻辑删除-根据任务id删除数据
     * @param taskId
     */
    @Override
    public void deleteByTaskId(List<String> taskId) {
        List<HddcWyGeologicalsvyplanningptEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWyGeologicalsvyplanningptRepository.queryHddcA1InvrgnhasmaterialtablesByTaskId(taskId);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWyGeologicalsvyplanningptEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWyGeologicalsvyplanningptRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }

    }





}
