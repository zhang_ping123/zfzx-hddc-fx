package com.css.zfzx.sjcj.modules.hddcGeochemicalAbnPoint.repository;

import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnPoint.repository.entity.HddcGeochemicalabnpointEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
public interface HddcGeochemicalabnpointRepository extends JpaRepository<HddcGeochemicalabnpointEntity, String> {
}
