package com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtTable.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtTable.repository.HddcB1FPaleoeqevttableNativeRepository;
import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtTable.repository.HddcB1FPaleoeqevttableRepository;
import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtTable.repository.entity.HddcB1FPaleoeqevttableEntity;
import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtTable.service.HddcB1FPaleoeqevttableService;
import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtTable.viewobjects.HddcB1FPaleoeqevttableQueryParams;
import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtTable.viewobjects.HddcB1FPaleoeqevttableVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Service
public class HddcB1FPaleoeqevttableServiceImpl implements HddcB1FPaleoeqevttableService {

	@Autowired
    private HddcB1FPaleoeqevttableRepository hddcB1FPaleoeqevttableRepository;
    @Autowired
    private HddcB1FPaleoeqevttableNativeRepository hddcB1FPaleoeqevttableNativeRepository;

    @Override
    public JSONObject queryHddcB1FPaleoeqevttables(HddcB1FPaleoeqevttableQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcB1FPaleoeqevttableEntity> hddcB1FPaleoeqevttablePage = this.hddcB1FPaleoeqevttableNativeRepository.queryHddcB1FPaleoeqevttables(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcB1FPaleoeqevttablePage);
        return jsonObject;
    }


    @Override
    public HddcB1FPaleoeqevttableEntity getHddcB1FPaleoeqevttable(String id) {
        HddcB1FPaleoeqevttableEntity hddcB1FPaleoeqevttable = this.hddcB1FPaleoeqevttableRepository.findById(id).orElse(null);
         return hddcB1FPaleoeqevttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB1FPaleoeqevttableEntity saveHddcB1FPaleoeqevttable(HddcB1FPaleoeqevttableEntity hddcB1FPaleoeqevttable) {
        String uuid = UUIDGenerator.getUUID();
        hddcB1FPaleoeqevttable.setUuid(uuid);
        hddcB1FPaleoeqevttable.setCreateUser(PlatformSessionUtils.getUserId());
        hddcB1FPaleoeqevttable.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB1FPaleoeqevttableRepository.save(hddcB1FPaleoeqevttable);
        return hddcB1FPaleoeqevttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB1FPaleoeqevttableEntity updateHddcB1FPaleoeqevttable(HddcB1FPaleoeqevttableEntity hddcB1FPaleoeqevttable) {
        HddcB1FPaleoeqevttableEntity entity = hddcB1FPaleoeqevttableRepository.findById(hddcB1FPaleoeqevttable.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcB1FPaleoeqevttable);
        hddcB1FPaleoeqevttable.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcB1FPaleoeqevttable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB1FPaleoeqevttableRepository.save(hddcB1FPaleoeqevttable);
        return hddcB1FPaleoeqevttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcB1FPaleoeqevttables(List<String> ids) {
        List<HddcB1FPaleoeqevttableEntity> hddcB1FPaleoeqevttableList = this.hddcB1FPaleoeqevttableRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcB1FPaleoeqevttableList) && hddcB1FPaleoeqevttableList.size() > 0) {
            for(HddcB1FPaleoeqevttableEntity hddcB1FPaleoeqevttable : hddcB1FPaleoeqevttableList) {
                this.hddcB1FPaleoeqevttableRepository.delete(hddcB1FPaleoeqevttable);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcB1FPaleoeqevttableQueryParams queryParams, HttpServletResponse response) {
        List<HddcB1FPaleoeqevttableEntity> yhDisasterEntities = hddcB1FPaleoeqevttableNativeRepository.exportYhDisasters(queryParams);
        List<HddcB1FPaleoeqevttableVO> list=new ArrayList<>();
        for (HddcB1FPaleoeqevttableEntity entity:yhDisasterEntities) {
            HddcB1FPaleoeqevttableVO yhDisasterVO=new HddcB1FPaleoeqevttableVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "断层段古地震事件表", "断层段古地震事件表", HddcB1FPaleoeqevttableVO.class, "断层段古地震事件表.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcB1FPaleoeqevttableVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcB1FPaleoeqevttableVO.class, params);
            List<HddcB1FPaleoeqevttableVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcB1FPaleoeqevttableVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcB1FPaleoeqevttableVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcB1FPaleoeqevttableVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcB1FPaleoeqevttableEntity yhDisasterEntity = new HddcB1FPaleoeqevttableEntity();
            HddcB1FPaleoeqevttableVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcB1FPaleoeqevttable(yhDisasterEntity);
        }
    }

}
