package com.css.zfzx.sjcj.modules.hddcStratigraphy1Pre.repository;

import com.css.zfzx.sjcj.modules.hddcStratigraphy1Pre.repository.entity.HddcStratigraphy1preEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-28
 */
public interface HddcStratigraphy1preRepository extends JpaRepository<HddcStratigraphy1preEntity, String> {
}
