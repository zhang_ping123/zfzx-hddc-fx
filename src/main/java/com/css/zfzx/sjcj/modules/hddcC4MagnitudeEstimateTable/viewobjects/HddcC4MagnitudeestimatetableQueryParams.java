package com.css.zfzx.sjcj.modules.hddcC4MagnitudeEstimateTable.viewobjects;

import lombok.Data;
import java.util.Date;

/**
 * @author zhangping
 * @date 2020-11-23
 */
@Data
public class HddcC4MagnitudeestimatetableQueryParams {


    private String projectId;
    private String examineDate;

}
