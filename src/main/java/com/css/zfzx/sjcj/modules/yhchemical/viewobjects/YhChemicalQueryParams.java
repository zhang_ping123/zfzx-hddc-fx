package com.css.zfzx.sjcj.modules.yhchemical.viewobjects;

import lombok.Data;
import java.util.Date;

/**
 * @author ly
 * @date 2020-11-04
 */
@Data
public class YhChemicalQueryParams {


    private String cztName;
    private String province;
    private String city;
    private String area;
    private String buildingLevel;
    private String yhLevel;
    private String status;
    private String createTime;
    private String createUser;

}
