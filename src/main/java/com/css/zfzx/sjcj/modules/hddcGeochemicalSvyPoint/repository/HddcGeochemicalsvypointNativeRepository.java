package com.css.zfzx.sjcj.modules.hddcGeochemicalSvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyPoint.repository.entity.HddcGeochemicalsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcGeochemicalSvyPoint.viewobjects.HddcGeochemicalsvypointQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
public interface HddcGeochemicalsvypointNativeRepository {

    Page<HddcGeochemicalsvypointEntity> queryHddcGeochemicalsvypoints(HddcGeochemicalsvypointQueryParams queryParams, int curPage, int pageSize);

    List<HddcGeochemicalsvypointEntity> exportYhDisasters(HddcGeochemicalsvypointQueryParams queryParams);
}
