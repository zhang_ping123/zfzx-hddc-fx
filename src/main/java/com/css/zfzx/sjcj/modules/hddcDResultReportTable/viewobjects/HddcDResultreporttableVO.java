package com.css.zfzx.sjcj.modules.hddcDResultReportTable.viewobjects;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-12-19
 */
@Data
public class HddcDResultreporttableVO implements Serializable {

    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 附件id
     */
    private String attachid;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 附件路径
     */
    private String attachpath;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 乡
     */
    private String town;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 文件名称
     */
    private String filename;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 探测城市
     */
    private String cityname;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 村
     */
    private String village;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 描述信息
     */
    private String commentInfo;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 省
     */
    private String province;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 任务id
     */
    private String taskId;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 地质填图区编号
     */
    private String mainafsregionid;
    /**
     * 工作区编号
     */
    private String workregionid;
    /**
     * 市
     */
    private String city;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 编号
     */
    private String id;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 成果报告原始文件编号
     */
    private String resultreportArwid;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 区（县）
     */
    private String area;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 目标区编号
     */
    private String targetregionid;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 项目id
     */
    private String projectId;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 主键
     */
    private String uuid;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 成果报告报告文件编号
     */
    private String resultreportArid;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String projectNameName;
    private String taskNameName;
}