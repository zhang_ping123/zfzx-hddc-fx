package com.css.zfzx.sjcj.modules.hddcStrongSeismicCatalog.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcStrongSeismicCatalog.repository.entity.HddcStrongseismiccatalogEntity;
import com.css.zfzx.sjcj.modules.hddcStrongSeismicCatalog.viewobjects.HddcStrongseismiccatalogQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */

public interface HddcStrongseismiccatalogService {

    public JSONObject queryHddcStrongseismiccatalogs(HddcStrongseismiccatalogQueryParams queryParams, int curPage, int pageSize);

    public HddcStrongseismiccatalogEntity getHddcStrongseismiccatalog(String id);

    public HddcStrongseismiccatalogEntity saveHddcStrongseismiccatalog(HddcStrongseismiccatalogEntity hddcStrongseismiccatalog);

    public HddcStrongseismiccatalogEntity updateHddcStrongseismiccatalog(HddcStrongseismiccatalogEntity hddcStrongseismiccatalog);

    public void deleteHddcStrongseismiccatalogs(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcStrongseismiccatalogQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
