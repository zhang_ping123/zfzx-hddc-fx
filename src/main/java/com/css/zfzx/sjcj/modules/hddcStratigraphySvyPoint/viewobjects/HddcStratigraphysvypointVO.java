package com.css.zfzx.sjcj.modules.hddcStratigraphySvyPoint.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author lihelei
 * @date 2020-11-27
 */
@Data
public class HddcStratigraphysvypointVO implements Serializable {

    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备注
     */
    private String remark;
    /**
     * 走向 [°]
     */
    @Excel(name = "走向 [°]", orderNum = "4")
    private Integer strike;
    /**
     * 接触关系描述
     */
    @Excel(name = "接触关系描述", orderNum = "5")
    private String touchredescription;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 地层描述
     */
    @Excel(name = "地层描述", orderNum = "6")
    private String stratigraphydescription;
    /**
     * 是否修改工作底图
     */
    @Excel(name = "是否修改工作底图", orderNum = "7")
    private Integer ismodifyworkmap;
    /**
     * 照片镜向
     */
    @Excel(name = "照片镜向", orderNum = "8")
    private Integer photoviewingto;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 地层点编号
     */
    private String id;
    /**
     * 是否在图中显示
     */
    @Excel(name = "是否在图中显示", orderNum = "9")
    private Integer isinmap;
    /**
     * 地质调查观测点编号
     */
    @Excel(name = "地质调查观测点编号", orderNum = "10")
    private String geologicalsvypointid;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 平面图原始文件编号
     */
    @Excel(name = "平面图原始文件编号", orderNum = "11")
    private String sketchArwid;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 地层厚度 [米]
     */
    @Excel(name = "地层厚度 [米]", orderNum = "12")
    private String thickness;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "13")
    private String commentInfo;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 典型剖面图原始文件编号
     */
    @Excel(name = "典型剖面图原始文件编号", orderNum = "14")
    private String typicalprofileArwid;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 野外编号
     */
    @Excel(name = "野外编号", orderNum = "15")
    private String fieldid;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 村
     */
    private String village;
    /**
     * 平面图文件编号
     */
    @Excel(name = "平面图文件编号", orderNum = "16")
    private String sketchAiid;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 倾角 [度]
     */
    @Excel(name = "倾角 [度]", orderNum = "17")
    private Integer clination;
    /**
     * 典型照片文件编号
     */
    @Excel(name = "典型照片文件编号", orderNum = "18")
    private String photoAiid;
    /**
     * 地层名称
     */
    @Excel(name = "地层名称", orderNum = "19")
    private String stratigraphyname;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 乡
     */
    private String town;
    /**
     * 数据来源
     */
    @Excel(name = "数据来源", orderNum = "20")
    private String datasource;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 实测倾向 [度]
     */
    @Excel(name = "实测倾向 [度]", orderNum = "21")
    private Integer dip;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 典型剖面图图像文件编号
     */
    @Excel(name = "典型剖面图图像文件编号", orderNum = "22")
    private String typicalprofileAiid;
    /**
     * 拍摄者
     */
    @Excel(name = "拍摄者", orderNum = "23")
    private String photographer;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 典型照片原始文件编号
     */
    @Excel(name = "典型照片原始文件编号", orderNum = "24")
    private String photoArwid;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 符号或标注旋转角度
     */
    @Excel(name = "符号或标注旋转角度", orderNum = "25")
    private Double lastangle;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 接触关系
     */
    @Excel(name = "接触关系", orderNum = "26")
    private Integer touchrelation;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 是否倒转地层产状
     */
    @Excel(name = "是否倒转地层产状", orderNum = "27")
    private Integer isreversed;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer photoviewingtoName;
    private Integer touchrelationName;
    private String rowNum;
    private String errorMsg;
}