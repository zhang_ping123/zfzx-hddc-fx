package com.css.zfzx.sjcj.modules.yhhospital.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.zfzx.sjcj.modules.yhhospital.repository.entity.YhHospitalEntity;
import com.css.zfzx.sjcj.modules.yhhospital.service.YhHospitalService;
import com.css.zfzx.sjcj.modules.yhhospital.viewobjects.YhHospitalQueryParams;
import com.css.bpm.platform.utils.PlatformPageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yyd
 * @date 2020-11-03
 */
@Slf4j
@RestController
@RequestMapping("/yhHospitals")
public class YhHospitalController {
    @Autowired
    private YhHospitalService yhHospitalService;

    @GetMapping("/queryYhHospitals")
    public RestResponse queryYhHospitals(HttpServletRequest request, YhHospitalQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = yhHospitalService.queryYhHospitals(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getYhHospital(@PathVariable String id) {
        RestResponse response = null;
        try{
            YhHospitalEntity yhHospital = yhHospitalService.getYhHospital(id);
            response = RestResponse.succeed(yhHospital);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveYhHospital(@RequestBody YhHospitalEntity yhHospital) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            yhHospitalService.saveYhHospital(yhHospital);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateYhHospital(@RequestBody YhHospitalEntity yhHospital)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            yhHospitalService.updateYhHospital(yhHospital);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteYhHospitals(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            yhHospitalService.deleteYhHospitals(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(yhHospitalService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }

}