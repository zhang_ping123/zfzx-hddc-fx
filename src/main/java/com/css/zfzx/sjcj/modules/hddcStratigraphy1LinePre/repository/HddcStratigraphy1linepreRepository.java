package com.css.zfzx.sjcj.modules.hddcStratigraphy1LinePre.repository;

import com.css.zfzx.sjcj.modules.hddcStratigraphy1LinePre.repository.entity.HddcStratigraphy1linepreEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcStratigraphy1linepreRepository extends JpaRepository<HddcStratigraphy1linepreEntity, String> {
}
