package com.css.zfzx.sjcj.modules.hddcB1GeomorPlyHasGeoSvyPt.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltVO;
import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyHasGeoSvyPt.repository.HddcB1GeomorplyhasgeosvyptNativeRepository;
import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyHasGeoSvyPt.repository.HddcB1GeomorplyhasgeosvyptRepository;
import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyHasGeoSvyPt.repository.entity.HddcB1GeomorplyhasgeosvyptEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyHasGeoSvyPt.service.HddcB1GeomorplyhasgeosvyptService;
import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyHasGeoSvyPt.viewobjects.HddcB1GeomorplyhasgeosvyptQueryParams;
import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyHasGeoSvyPt.viewobjects.HddcB1GeomorplyhasgeosvyptVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Service
public class HddcB1GeomorplyhasgeosvyptServiceImpl implements HddcB1GeomorplyhasgeosvyptService {

	@Autowired
    private HddcB1GeomorplyhasgeosvyptRepository hddcB1GeomorplyhasgeosvyptRepository;
    @Autowired
    private HddcB1GeomorplyhasgeosvyptNativeRepository hddcB1GeomorplyhasgeosvyptNativeRepository;

    @Override
    public JSONObject queryHddcB1Geomorplyhasgeosvypts(HddcB1GeomorplyhasgeosvyptQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcB1GeomorplyhasgeosvyptEntity> hddcB1GeomorplyhasgeosvyptPage = this.hddcB1GeomorplyhasgeosvyptNativeRepository.queryHddcB1Geomorplyhasgeosvypts(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcB1GeomorplyhasgeosvyptPage);
        return jsonObject;
    }


    @Override
    public HddcB1GeomorplyhasgeosvyptEntity getHddcB1Geomorplyhasgeosvypt(String id) {
        HddcB1GeomorplyhasgeosvyptEntity hddcB1Geomorplyhasgeosvypt = this.hddcB1GeomorplyhasgeosvyptRepository.findById(id).orElse(null);
         return hddcB1Geomorplyhasgeosvypt;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB1GeomorplyhasgeosvyptEntity saveHddcB1Geomorplyhasgeosvypt(HddcB1GeomorplyhasgeosvyptEntity hddcB1Geomorplyhasgeosvypt) {
        String uuid = UUIDGenerator.getUUID();
        hddcB1Geomorplyhasgeosvypt.setUuid(uuid);
        hddcB1Geomorplyhasgeosvypt.setCreateUser(PlatformSessionUtils.getUserId());
        hddcB1Geomorplyhasgeosvypt.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB1GeomorplyhasgeosvyptRepository.save(hddcB1Geomorplyhasgeosvypt);
        return hddcB1Geomorplyhasgeosvypt;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB1GeomorplyhasgeosvyptEntity updateHddcB1Geomorplyhasgeosvypt(HddcB1GeomorplyhasgeosvyptEntity hddcB1Geomorplyhasgeosvypt) {
        HddcB1GeomorplyhasgeosvyptEntity entity = hddcB1GeomorplyhasgeosvyptRepository.findById(hddcB1Geomorplyhasgeosvypt.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcB1Geomorplyhasgeosvypt);
        hddcB1Geomorplyhasgeosvypt.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcB1Geomorplyhasgeosvypt.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB1GeomorplyhasgeosvyptRepository.save(hddcB1Geomorplyhasgeosvypt);
        return hddcB1Geomorplyhasgeosvypt;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcB1Geomorplyhasgeosvypts(List<String> ids) {
        List<HddcB1GeomorplyhasgeosvyptEntity> hddcB1GeomorplyhasgeosvyptList = this.hddcB1GeomorplyhasgeosvyptRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcB1GeomorplyhasgeosvyptList) && hddcB1GeomorplyhasgeosvyptList.size() > 0) {
            for(HddcB1GeomorplyhasgeosvyptEntity hddcB1Geomorplyhasgeosvypt : hddcB1GeomorplyhasgeosvyptList) {
                this.hddcB1GeomorplyhasgeosvyptRepository.delete(hddcB1Geomorplyhasgeosvypt);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcB1GeomorplyhasgeosvyptQueryParams queryParams, HttpServletResponse response) {
        List<HddcB1GeomorplyhasgeosvyptEntity> yhDisasterEntities = hddcB1GeomorplyhasgeosvyptNativeRepository.exportYhDisasters(queryParams);
        List<HddcB1GeomorplyhasgeosvyptVO> list=new ArrayList<>();
        for (HddcB1GeomorplyhasgeosvyptEntity entity:yhDisasterEntities) {
            HddcB1GeomorplyhasgeosvyptVO yhDisasterVO=new HddcB1GeomorplyhasgeosvyptVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"地貌面与地质地貌点关联表","地貌面与地质地貌点关联表",HddcB1GeomorplyhasgeosvyptVO.class,"地貌面与地质地貌点关联表.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcB1GeomorplyhasgeosvyptVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcB1GeomorplyhasgeosvyptVO.class, params);
            List<HddcB1GeomorplyhasgeosvyptVO> list = result.getList();
            // Excel条数据
           // int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcB1GeomorplyhasgeosvyptVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcB1GeomorplyhasgeosvyptVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcB1GeomorplyhasgeosvyptVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcB1GeomorplyhasgeosvyptEntity yhDisasterEntity = new HddcB1GeomorplyhasgeosvyptEntity();
            HddcB1GeomorplyhasgeosvyptVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcB1Geomorplyhasgeosvypt(yhDisasterEntity);
        }
    }

}
