package com.css.zfzx.sjcj.modules.hddcStratigraphy1LinePre.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcStratigraphy1LinePre.repository.entity.HddcStratigraphy1linepreEntity;
import com.css.zfzx.sjcj.modules.hddcStratigraphy1LinePre.viewobjects.HddcStratigraphy1linepreQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */

public interface HddcStratigraphy1linepreService {

    public JSONObject queryHddcStratigraphy1linepres(HddcStratigraphy1linepreQueryParams queryParams, int curPage, int pageSize);

    public HddcStratigraphy1linepreEntity getHddcStratigraphy1linepre(String id);

    public HddcStratigraphy1linepreEntity saveHddcStratigraphy1linepre(HddcStratigraphy1linepreEntity hddcStratigraphy1linepre);

    public HddcStratigraphy1linepreEntity updateHddcStratigraphy1linepre(HddcStratigraphy1linepreEntity hddcStratigraphy1linepre);

    public void deleteHddcStratigraphy1linepres(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    String findByDictCodeAndDictItemCode(String dictCode, String dictItemCode);

    void exportFile(HddcStratigraphy1linepreQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
