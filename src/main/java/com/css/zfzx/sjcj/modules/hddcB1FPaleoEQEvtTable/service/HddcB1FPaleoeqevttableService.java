package com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtTable.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtTable.repository.entity.HddcB1FPaleoeqevttableEntity;
import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtTable.viewobjects.HddcB1FPaleoeqevttableQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */

public interface HddcB1FPaleoeqevttableService {

    public JSONObject queryHddcB1FPaleoeqevttables(HddcB1FPaleoeqevttableQueryParams queryParams, int curPage, int pageSize);

    public HddcB1FPaleoeqevttableEntity getHddcB1FPaleoeqevttable(String id);

    public HddcB1FPaleoeqevttableEntity saveHddcB1FPaleoeqevttable(HddcB1FPaleoeqevttableEntity hddcB1FPaleoeqevttable);

    public HddcB1FPaleoeqevttableEntity updateHddcB1FPaleoeqevttable(HddcB1FPaleoeqevttableEntity hddcB1FPaleoeqevttable);

    public void deleteHddcB1FPaleoeqevttables(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB1FPaleoeqevttableQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
