package com.css.zfzx.sjcj.modules.hddcB3DrillProjectTable.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Data
public class HddcB3DrillprojecttableVO implements Serializable {

    /**
     * 变更情况
     */
    @Excel(name="变更情况",orderNum = "16")
    private String csArid;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 变更情况原始
     */
    @Excel(name="变更情况原始",orderNum = "17")
    private String csArwid;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 设计书原始文件编号
     */
    @Excel(name="设计书原始文件编号",orderNum = "15")
    private String dbArwid;
    /**
     * 目标区编号
     */
    @Excel(name="目标区编号",orderNum = "5")
    private String targetregionid;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 工程设计报告编号
     */
    @Excel(name="工程设计报告编号",orderNum = "14")
    private String dbArid;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 乡
     */
    private String town;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 钻孔数
     */
    @Excel(name="钻孔数",orderNum = "8")
    private Integer drillcount;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 钻孔总进尺 [米]
     */
    @Excel(name="钻孔总进尺 [米]",orderNum = "9")
    private Double drilllength;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备注
     */
    @Excel(name="备注",orderNum = "20")
    private String commentInfo;
    /**
     * 省
     */
    @Excel(name="省",orderNum = "1")
    private String province;
    /**
     * 工程名称
     */
    @Excel(name="工程名称",orderNum = "7")
    private String name;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 地质填图区编号
     */
    @Excel(name="地质填图区编号",orderNum = "4")
    private String mainafsregionid;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 区（县）
     */
    @Excel(name="区（县）",orderNum = "3")
    private String area;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 钻探工程获得测试结果样品总数
     */
    @Excel(name="钻探工程获得测试结果样品总数",orderNum = "12")
    private Integer datingsamplecount;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 采集样品总数
     */
    @Excel(name="采集样品总数",orderNum = "10")
    private Integer collectedsamplecount;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 成果报告文件报告编号
     */
    @Excel(name="工作区编号",orderNum = "18")
    private String reportArid;
    /**
     * 工程编号
     */
    private String id;
    /**
     * 钻探单位
     */
    @Excel(name="钻探单位",orderNum = "13")
    private String drillinstitute;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 成果报告原始文件编号
     */
    @Excel(name="成果报告原始文件编号",orderNum = "19")
    private String reportArwid;
    /**
     * 工作区编号
     */
    @Excel(name="工作区编号",orderNum = "6")
    private String workregionid;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 钻探工程测年送样总数
     */
    @Excel(name="钻探工程测年送样总数",orderNum = "11")
    private Integer samplecount;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 市
     */
    @Excel(name="市",orderNum = "2")
    private String city;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String rowNum;
    private String errorMsg;

}