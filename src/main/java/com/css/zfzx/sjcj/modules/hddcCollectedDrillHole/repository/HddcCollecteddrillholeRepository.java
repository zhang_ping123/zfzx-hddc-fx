package com.css.zfzx.sjcj.modules.hddcCollectedDrillHole.repository;

import com.css.zfzx.sjcj.modules.hddcCollectedDrillHole.repository.entity.HddcCollecteddrillholeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcCollecteddrillholeRepository extends JpaRepository<HddcCollecteddrillholeEntity, String> {
}
