package com.css.zfzx.sjcj.modules.hddcB6_GeochemicalProjectTable.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
@Data
public class HddcB6GeochemicalprojecttableQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String name;

}
