package com.css.zfzx.sjcj.modules.hddcwyGeoGeomorphySvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeoGeomorphySvyPoint.repository.entity.HddcWyGeogeomorphysvypointEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */
public interface HddcWyGeogeomorphysvypointRepository extends JpaRepository<HddcWyGeogeomorphysvypointEntity, String> {

    List<HddcWyGeogeomorphysvypointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);


    List<HddcWyGeogeomorphysvypointEntity> findAllByCreateUserAndIsValid(String userId,String isValid);

    @Query(nativeQuery = true, value = "select * from hddc_wy_geogeomorphysvypoint where is_valid!=0 project_name in :projectIds")
    List<HddcWyGeogeomorphysvypointEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_geogeomorphysvypoint where task_name in :projectIds")
    List<HddcWyGeogeomorphysvypointEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);

}
