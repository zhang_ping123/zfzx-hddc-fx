package com.css.zfzx.sjcj.modules.hddcwyVolcanicSvyPoint.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-12-02
 */
@Data
public class HddcWyVolcanicsvypointQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String locationname;

    private String startTime;
    private String endTime;
}
