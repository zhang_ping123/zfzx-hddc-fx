package com.css.zfzx.sjcj.modules.hddcGeoProfileLine.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltVO;
import com.css.zfzx.sjcj.modules.hddcGeoProfileLine.repository.HddcGeoprofilelineNativeRepository;
import com.css.zfzx.sjcj.modules.hddcGeoProfileLine.repository.HddcGeoprofilelineRepository;
import com.css.zfzx.sjcj.modules.hddcGeoProfileLine.repository.entity.HddcGeoprofilelineEntity;
import com.css.zfzx.sjcj.modules.hddcGeoProfileLine.service.HddcGeoprofilelineService;
import com.css.zfzx.sjcj.modules.hddcGeoProfileLine.viewobjects.HddcGeoprofilelineQueryParams;
import com.css.zfzx.sjcj.modules.hddcGeoProfileLine.viewobjects.HddcGeoprofilelineVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Service
public class HddcGeoprofilelineServiceImpl implements HddcGeoprofilelineService {

	@Autowired
    private HddcGeoprofilelineRepository hddcGeoprofilelineRepository;
    @Autowired
    private HddcGeoprofilelineNativeRepository hddcGeoprofilelineNativeRepository;

    @Override
    public JSONObject queryHddcGeoprofilelines(HddcGeoprofilelineQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcGeoprofilelineEntity> hddcGeoprofilelinePage = this.hddcGeoprofilelineNativeRepository.queryHddcGeoprofilelines(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcGeoprofilelinePage);
        return jsonObject;
    }


    @Override
    public HddcGeoprofilelineEntity getHddcGeoprofileline(String id) {
        HddcGeoprofilelineEntity hddcGeoprofileline = this.hddcGeoprofilelineRepository.findById(id).orElse(null);
         return hddcGeoprofileline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeoprofilelineEntity saveHddcGeoprofileline(HddcGeoprofilelineEntity hddcGeoprofileline) {
        String uuid = UUIDGenerator.getUUID();
        hddcGeoprofileline.setUuid(uuid);
        hddcGeoprofileline.setCreateUser(PlatformSessionUtils.getUserId());
        hddcGeoprofileline.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeoprofilelineRepository.save(hddcGeoprofileline);
        return hddcGeoprofileline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeoprofilelineEntity updateHddcGeoprofileline(HddcGeoprofilelineEntity hddcGeoprofileline) {
        HddcGeoprofilelineEntity entity = hddcGeoprofilelineRepository.findById(hddcGeoprofileline.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcGeoprofileline);
        hddcGeoprofileline.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcGeoprofileline.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeoprofilelineRepository.save(hddcGeoprofileline);
        return hddcGeoprofileline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcGeoprofilelines(List<String> ids) {
        List<HddcGeoprofilelineEntity> hddcGeoprofilelineList = this.hddcGeoprofilelineRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcGeoprofilelineList) && hddcGeoprofilelineList.size() > 0) {
            for(HddcGeoprofilelineEntity hddcGeoprofileline : hddcGeoprofilelineList) {
                this.hddcGeoprofilelineRepository.delete(hddcGeoprofileline);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcGeoprofilelineQueryParams queryParams, HttpServletResponse response) {
        List<HddcGeoprofilelineEntity> yhDisasterEntities = hddcGeoprofilelineNativeRepository.exportYhDisasters(queryParams);
        List<HddcGeoprofilelineVO> list=new ArrayList<>();
        for (HddcGeoprofilelineEntity entity:yhDisasterEntities) {
            HddcGeoprofilelineVO yhDisasterVO=new HddcGeoprofilelineVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"地质剖面线-线","地质剖面线-线",HddcGeoprofilelineVO.class,"地质剖面线-线.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcGeoprofilelineVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcGeoprofilelineVO.class, params);
            List<HddcGeoprofilelineVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcGeoprofilelineVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcGeoprofilelineVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcGeoprofilelineVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcGeoprofilelineEntity yhDisasterEntity = new HddcGeoprofilelineEntity();
            HddcGeoprofilelineVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcGeoprofileline(yhDisasterEntity);
        }
    }
}
