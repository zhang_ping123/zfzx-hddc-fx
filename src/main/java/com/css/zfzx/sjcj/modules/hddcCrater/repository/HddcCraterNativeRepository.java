package com.css.zfzx.sjcj.modules.hddcCrater.repository;

import com.css.zfzx.sjcj.modules.hddcCrater.repository.entity.HddcCraterEntity;
import com.css.zfzx.sjcj.modules.hddcCrater.viewobjects.HddcCraterQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
public interface HddcCraterNativeRepository {

    Page<HddcCraterEntity> queryHddcCraters(HddcCraterQueryParams queryParams, int curPage, int pageSize);

    List<HddcCraterEntity> exportYhDisasters(HddcCraterQueryParams queryParams);
}
