package com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyPoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyPoint.repository.entity.HddcWyGeochemicalsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyPoint.viewobjects.HddcWyGeochemicalsvypointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-02
 */

public interface HddcWyGeochemicalsvypointService {

    public JSONObject queryHddcWyGeochemicalsvypoints(HddcWyGeochemicalsvypointQueryParams queryParams, int curPage, int pageSize);

    public HddcWyGeochemicalsvypointEntity getHddcWyGeochemicalsvypoint(String uuid);

    public HddcWyGeochemicalsvypointEntity saveHddcWyGeochemicalsvypoint(HddcWyGeochemicalsvypointEntity hddcWyGeochemicalsvypoint);

    public HddcWyGeochemicalsvypointEntity updateHddcWyGeochemicalsvypoint(HddcWyGeochemicalsvypointEntity hddcWyGeochemicalsvypoint);

    public void deleteHddcWyGeochemicalsvypoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    BigInteger queryHddcWyGeochemicalsvypoint(HddcAppZztCountVo queryParams);

    List<HddcWyGeochemicalsvypointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);

    List<HddcWyGeochemicalsvypointEntity> findAllByCreateUserAndIsValid(String userId, String isValid);



    /**
     * 逻辑删除-根据项目id删除数据
     * @param ids
     */
    void deleteByProjectId(List<String> ids);

    /**
     * 逻辑删除-根据任务id删除数据
     * @param ids
     */
    void deleteByTaskId(List<String> ids);
}
