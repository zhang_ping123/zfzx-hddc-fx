package com.css.zfzx.sjcj.modules.hddcStratigraphySvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcStratigraphySvyPoint.repository.entity.HddcStratigraphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcStratigraphySvyPoint.viewobjects.HddcStratigraphysvypointQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author lihelei
 * @date 2020-11-27
 */
public interface HddcStratigraphysvypointNativeRepository {

    Page<HddcStratigraphysvypointEntity> queryHddcStratigraphysvypoints(HddcStratigraphysvypointQueryParams queryParams, int curPage, int pageSize);

    List<HddcStratigraphysvypointEntity> exportYhDisasters(HddcStratigraphysvypointQueryParams queryParams);
}
