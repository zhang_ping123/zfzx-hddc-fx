package com.css.zfzx.sjcj.modules.hddcB1GeologySvyProjectTable.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB1GeologySvyProjectTable.repository.entity.HddcB1GeologysvyprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeologySvyProjectTable.viewobjects.HddcB1GeologysvyprojecttableQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */

public interface HddcB1GeologysvyprojecttableService {

    public JSONObject queryHddcB1Geologysvyprojecttables(HddcB1GeologysvyprojecttableQueryParams queryParams, int curPage, int pageSize);

    public HddcB1GeologysvyprojecttableEntity getHddcB1Geologysvyprojecttable(String id);

    public HddcB1GeologysvyprojecttableEntity saveHddcB1Geologysvyprojecttable(HddcB1GeologysvyprojecttableEntity hddcB1Geologysvyprojecttable);

    public HddcB1GeologysvyprojecttableEntity updateHddcB1Geologysvyprojecttable(HddcB1GeologysvyprojecttableEntity hddcB1Geologysvyprojecttable);

    public void deleteHddcB1Geologysvyprojecttables(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB1GeologysvyprojecttableQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
