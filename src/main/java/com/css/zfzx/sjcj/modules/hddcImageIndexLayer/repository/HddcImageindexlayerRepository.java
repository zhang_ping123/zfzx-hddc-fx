package com.css.zfzx.sjcj.modules.hddcImageIndexLayer.repository;

import com.css.zfzx.sjcj.modules.hddcImageIndexLayer.repository.entity.HddcImageindexlayerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangping
 * @date 2020-11-30
 */
public interface HddcImageindexlayerRepository extends JpaRepository<HddcImageindexlayerEntity, String> {
}
