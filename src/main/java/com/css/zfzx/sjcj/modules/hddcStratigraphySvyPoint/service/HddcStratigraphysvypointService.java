package com.css.zfzx.sjcj.modules.hddcStratigraphySvyPoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcStratigraphySvyPoint.repository.entity.HddcStratigraphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcStratigraphySvyPoint.viewobjects.HddcStratigraphysvypointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author lihelei
 * @date 2020-11-27
 */

public interface HddcStratigraphysvypointService {

    public JSONObject queryHddcStratigraphysvypoints(HddcStratigraphysvypointQueryParams queryParams, int curPage, int pageSize);

    public HddcStratigraphysvypointEntity getHddcStratigraphysvypoint(String id);

    public HddcStratigraphysvypointEntity saveHddcStratigraphysvypoint(HddcStratigraphysvypointEntity hddcStratigraphysvypoint);

    public HddcStratigraphysvypointEntity updateHddcStratigraphysvypoint(HddcStratigraphysvypointEntity hddcStratigraphysvypoint);

    public void deleteHddcStratigraphysvypoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcStratigraphysvypointQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
