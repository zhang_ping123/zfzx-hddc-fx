package com.css.zfzx.sjcj.modules.hddcRSInterpretationPolygon.repository;

import com.css.zfzx.sjcj.modules.hddcRSInterpretationPolygon.repository.entity.HddcRsinterpretationpolygonEntity;
import com.css.zfzx.sjcj.modules.hddcRSInterpretationPolygon.viewobjects.HddcRsinterpretationpolygonQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangping
 * @date 2020-11-30
 */
public interface HddcRsinterpretationpolygonNativeRepository {

    Page<HddcRsinterpretationpolygonEntity> queryHddcRsinterpretationpolygons(HddcRsinterpretationpolygonQueryParams queryParams, int curPage, int pageSize);

    List<HddcRsinterpretationpolygonEntity> exportYhDisasters(HddcRsinterpretationpolygonQueryParams queryParams);
}
