package com.css.zfzx.sjcj.modules.analysis.service.impl;

import com.css.zfzx.sjcj.modules.analysis.service.WlAndHxService;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyLine.service.HddcWyGeochemicalsvylineService;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyPoint.service.HddcWyGeochemicalsvypointService;
import com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.service.HddcWyGeophysvylineService;
import com.css.zfzx.sjcj.modules.hddcwyGeophysvypoint.service.HddcWyGeophysvypointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO
 *
 * @author 王昊杰
 * @version 1.0
 * @date 2020/12/10  20:11
 */

@Service
public class WlAndHxServiceImpl implements WlAndHxService {
    @Autowired
    private HddcWyGeophysvylineService hddcWyGeophysvylineService;
    @Autowired
    private HddcWyGeophysvypointService hddcWyGeophysvypointService;
    @Autowired
    private HddcWyGeochemicalsvylineService hddcWyGeochemicalsvylineService;
    @Autowired
    private HddcWyGeochemicalsvypointService hddcWyGeochemicalsvypointService;

    public Map<String,Object> hddcWlAndHxNumData(HddcAppZztCountVo queryParams){
        //地球物理测线-线
        //BigInteger lineCount = hddcWyGeophysvylineService.queryHddcWyGeophysvyline(queryParams);
        //地球物理测点-点
        BigInteger pointCount = hddcWyGeophysvypointService.queryHddcWyGeophysvypoint(queryParams);
        //地球化学探测测线-线
        //BigInteger geoLineCount = hddcWyGeochemicalsvylineService.queryHddcWyGeochemicalsvyline(queryParams);
        //地球化学探测测点-点
        BigInteger geoPointCount = hddcWyGeochemicalsvypointService.queryHddcWyGeochemicalsvypoint(queryParams);
        //x轴
        ArrayList<String> x=new ArrayList<>();
        //x.add("地球物理测线-线");
        x.add("地球物理测点-点");
        //x.add("地球化学探测测线-线");
        x.add("地球化学探测测点-点");
        //y轴
        ArrayList<BigInteger> y=new ArrayList<>();
        //y.add(lineCount);
        y.add(pointCount);
        //y.add(geoLineCount);
        y.add(geoPointCount);

        HashMap<String,Object> map=new HashMap<>();
        map.put("x",x);
        map.put("y",y);

        return map;
    }
}
