package com.css.zfzx.sjcj.modules.hddcStrongSeismicCatalog.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Data
public class HddcStrongseismiccatalogQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

}
