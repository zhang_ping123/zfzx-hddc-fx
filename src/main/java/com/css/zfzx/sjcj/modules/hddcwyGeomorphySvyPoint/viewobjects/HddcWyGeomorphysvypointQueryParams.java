package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.viewobjects;

import lombok.Data;

/**
 * @author lihelei
 * @date 2020-12-01
 */
@Data
public class HddcWyGeomorphysvypointQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

    private String startTime;
    private String endTime;


    private String userId;
    private String taskId;
    private String projectId;
    private String isVaild;
}
