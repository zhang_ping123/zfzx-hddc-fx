package com.css.zfzx.sjcj.modules.hddcB1GeomorPlyOnFractBlt.repository;

import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyOnFractBlt.repository.entity.HddcB1GeomorplyonfractbltEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcB1GeomorplyonfractbltRepository extends JpaRepository<HddcB1GeomorplyonfractbltEntity, String> {
}
