package com.css.zfzx.sjcj.modules.hddcB1GeomorLnHasGeoSvyPt.repository;

import com.css.zfzx.sjcj.modules.hddcB1GeomorLnHasGeoSvyPt.repository.entity.HddcB1GeomorlnhasgeosvyptEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcB1GeomorlnhasgeosvyptRepository extends JpaRepository<HddcB1GeomorlnhasgeosvyptEntity, String> {
}
