package com.css.zfzx.sjcj.modules.hddcCollectedDrillHole.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcCollectedDrillHole.repository.HddcCollecteddrillholeNativeRepository;
import com.css.zfzx.sjcj.modules.hddcCollectedDrillHole.repository.HddcCollecteddrillholeRepository;
import com.css.zfzx.sjcj.modules.hddcCollectedDrillHole.repository.entity.HddcCollecteddrillholeEntity;
import com.css.zfzx.sjcj.modules.hddcCollectedDrillHole.service.HddcCollecteddrillholeService;
import com.css.zfzx.sjcj.modules.hddcCollectedDrillHole.viewobjects.HddcCollecteddrillholeQueryParams;
import com.css.zfzx.sjcj.modules.hddcCollectedDrillHole.viewobjects.HddcCollecteddrillholeVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Service
public class HddcCollecteddrillholeServiceImpl implements HddcCollecteddrillholeService {

	@Autowired
    private HddcCollecteddrillholeRepository hddcCollecteddrillholeRepository;
    @Autowired
    private HddcCollecteddrillholeNativeRepository hddcCollecteddrillholeNativeRepository;

    @Override
    public JSONObject queryHddcCollecteddrillholes(HddcCollecteddrillholeQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcCollecteddrillholeEntity> hddcCollecteddrillholePage = this.hddcCollecteddrillholeNativeRepository.queryHddcCollecteddrillholes(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcCollecteddrillholePage);
        return jsonObject;
    }


    @Override
    public HddcCollecteddrillholeEntity getHddcCollecteddrillhole(String id) {
        HddcCollecteddrillholeEntity hddcCollecteddrillhole = this.hddcCollecteddrillholeRepository.findById(id).orElse(null);
         return hddcCollecteddrillhole;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcCollecteddrillholeEntity saveHddcCollecteddrillhole(HddcCollecteddrillholeEntity hddcCollecteddrillhole) {
        String uuid = UUIDGenerator.getUUID();
        hddcCollecteddrillhole.setUuid(uuid);
        hddcCollecteddrillhole.setCreateUser(PlatformSessionUtils.getUserId());
        hddcCollecteddrillhole.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcCollecteddrillholeRepository.save(hddcCollecteddrillhole);
        return hddcCollecteddrillhole;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcCollecteddrillholeEntity updateHddcCollecteddrillhole(HddcCollecteddrillholeEntity hddcCollecteddrillhole) {
        HddcCollecteddrillholeEntity entity = hddcCollecteddrillholeRepository.findById(hddcCollecteddrillhole.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcCollecteddrillhole);
        hddcCollecteddrillhole.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcCollecteddrillhole.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcCollecteddrillholeRepository.save(hddcCollecteddrillhole);
        return hddcCollecteddrillhole;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcCollecteddrillholes(List<String> ids) {
        List<HddcCollecteddrillholeEntity> hddcCollecteddrillholeList = this.hddcCollecteddrillholeRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcCollecteddrillholeList) && hddcCollecteddrillholeList.size() > 0) {
            for(HddcCollecteddrillholeEntity hddcCollecteddrillhole : hddcCollecteddrillholeList) {
                this.hddcCollecteddrillholeRepository.delete(hddcCollecteddrillhole);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcCollecteddrillholeQueryParams queryParams, HttpServletResponse response) {
        List<HddcCollecteddrillholeEntity> yhDisasterEntities = hddcCollecteddrillholeNativeRepository.exportYhDisasters(queryParams);
        List<HddcCollecteddrillholeVO> list=new ArrayList<>();
        for (HddcCollecteddrillholeEntity entity:yhDisasterEntities) {
            HddcCollecteddrillholeVO yhDisasterVO=new HddcCollecteddrillholeVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"收集钻孔","收集钻孔",HddcCollecteddrillholeVO.class,"收集钻孔.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcCollecteddrillholeVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcCollecteddrillholeVO.class, params);
            List<HddcCollecteddrillholeVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcCollecteddrillholeVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcCollecteddrillholeVO yhDisasterVO = iterator.next();
                    String error = "";
                    /*returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");*/
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcCollecteddrillholeVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcCollecteddrillholeEntity yhDisasterEntity = new HddcCollecteddrillholeEntity();
            HddcCollecteddrillholeVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcCollecteddrillhole(yhDisasterEntity);
        }
    }

}
