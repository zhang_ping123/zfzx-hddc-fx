package com.css.zfzx.sjcj.modules.hddcRelocationISCatalog.repository;

import com.css.zfzx.sjcj.modules.hddcRelocationISCatalog.repository.entity.HddcRelocationiscatalogEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-28
 */
public interface HddcRelocationiscatalogRepository extends JpaRepository<HddcRelocationiscatalogEntity, String> {
}
