package com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningLine.viewobjects;

import lombok.Data;

/**
 * @author zhangping
 * @date 2020-12-01
 */
@Data
public class HddcWyGeologicalsvyplanninglineQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String svylinename;
    private String startTime;
    private String endTime;

    private String userId;
    private String taskId;
    private String projectId;
    private String isVaild;
}
