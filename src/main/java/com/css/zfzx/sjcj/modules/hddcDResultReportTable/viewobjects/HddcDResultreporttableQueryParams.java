package com.css.zfzx.sjcj.modules.hddcDResultReportTable.viewobjects;

import lombok.Data;

/**
 * @author zhangcong
 * @date 2020-12-19
 */
@Data
public class HddcDResultreporttableQueryParams {


    private String province;
    private String city;
    private String area;
    private String id;
    private String cityname;
    private String filename;
    private String resultreportArid;

}
