package com.css.zfzx.sjcj.modules.hddcRock5Pre.repository;

import com.css.zfzx.sjcj.modules.hddcRock5Pre.repository.entity.HddcRock5preEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author lhl
 * @date 2020-11-27
 */
public interface HddcRock5preRepository extends JpaRepository<HddcRock5preEntity, String> {
}
