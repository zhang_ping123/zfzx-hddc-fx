package com.css.zfzx.sjcj.modules.yhhospital.viewobjects;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author yyd
 * @date 2020-11-03
 */
@Data
public class YhHospitalVO implements Serializable {

    /**
     * 建筑等级
     */
    private String buildingLevel;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 区
     */
    private String area;
    /**
     * 状态
     */
    private String status;
    /**
     * 经度
     */
    private Double longitude;
    /**
     * 是否有效,0:无效,1:有效
     */
    private String isValid;
    /**
     * 省
     */
    private String province;
    /**
     * 隐患等级
     */
    private String yhLevel;
    /**
     * 主键ID
     */
    private String id;
    /**
     * 原因
     */
    private String reason;
    /**
     * 市
     */
    private String city;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 纬度
     */
    private Double latitude;
    /**
     * 建筑高度
     */
    private Double buildingHeight;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 建筑面积
     */
    private Double buildingArea;
    /**
     * 承灾体名称
     */
    private String cztName;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String buildingLevelName;
    private String yhLevelName;
}