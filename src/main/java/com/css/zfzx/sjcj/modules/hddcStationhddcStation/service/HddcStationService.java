package com.css.zfzx.sjcj.modules.hddcStationhddcStation.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcStationhddcStation.repository.entity.HddcStationEntity;
import com.css.zfzx.sjcj.modules.hddcStationhddcStation.viewobjects.HddcStationQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */

public interface HddcStationService {

    public JSONObject queryHddcStations(HddcStationQueryParams queryParams, int curPage, int pageSize);

    public HddcStationEntity getHddcStation(String id);

    public HddcStationEntity saveHddcStation(HddcStationEntity hddcStation);

    public HddcStationEntity updateHddcStation(HddcStationEntity hddcStation);

    public void deleteHddcStations(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcStationQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
