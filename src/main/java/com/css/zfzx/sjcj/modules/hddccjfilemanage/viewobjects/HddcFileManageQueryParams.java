package com.css.zfzx.sjcj.modules.hddccjfilemanage.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-12-10
 */
@Data
public class HddcFileManageQueryParams {


    private String fileId;
    private String fileName;
    private String isValid;
}
