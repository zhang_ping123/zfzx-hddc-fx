package com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningLine.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningLine.repository.entity.HddcWyGeologicalsvyplanninglineEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningLine.viewobjects.HddcWyGeologicalsvyplanninglineQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

/**
 * @author zhangping
 * @date 2020-12-01
 */

public interface HddcWyGeologicalsvyplanninglineService {

    public JSONObject queryHddcWyGeologicalsvyplanninglines(HddcWyGeologicalsvyplanninglineQueryParams queryParams, int curPage, int pageSize);

    public HddcWyGeologicalsvyplanninglineEntity getHddcWyGeologicalsvyplanningline(String uuid);

    public HddcWyGeologicalsvyplanninglineEntity saveHddcWyGeologicalsvyplanningline(HddcWyGeologicalsvyplanninglineEntity hddcWyGeologicalsvyplanningline);

    public HddcWyGeologicalsvyplanninglineEntity updateHddcWyGeologicalsvyplanningline(HddcWyGeologicalsvyplanninglineEntity hddcWyGeologicalsvyplanningline);

    public void deleteHddcWyGeologicalsvyplanninglines(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    BigInteger queryHddcWyGeologicalsvyplanningline(HddcAppZztCountVo queryParams);

    List<HddcWyGeologicalsvyplanninglineEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId,String taskId,String projectId,String isValid);

    String uploadPic(MultipartFile[] multipartFiles,String type) throws IOException;

    List<HddcWyGeologicalsvyplanninglineEntity> findAllByCreateUserAndIsValid(String userId,String isValid);


    /**
     * 逻辑删除-根据项目id删除数据
     * @param ids
     */
    void deleteByProjectId(List<String> ids);

    /**
     * 逻辑删除-根据任务id删除数据
     * @param ids
     */
    void deleteByTaskId(List<String> ids);
}
