package com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.HddcB1GeomorlnonfractbltNativeRepository;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.HddcB1GeomorlnonfractbltRepository;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.service.HddcB1GeomorlnonfractbltService;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltQueryParams;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Service
public class HddcB1GeomorlnonfractbltServiceImpl implements HddcB1GeomorlnonfractbltService {

	@Autowired
    private HddcB1GeomorlnonfractbltRepository hddcB1GeomorlnonfractbltRepository;
    @Autowired
    private HddcB1GeomorlnonfractbltNativeRepository hddcB1GeomorlnonfractbltNativeRepository;

    @Override
    public JSONObject queryHddcB1Geomorlnonfractblts(HddcB1GeomorlnonfractbltQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcB1GeomorlnonfractbltEntity> hddcB1GeomorlnonfractbltPage = this.hddcB1GeomorlnonfractbltNativeRepository.queryHddcB1Geomorlnonfractblts(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcB1GeomorlnonfractbltPage);
        return jsonObject;
    }


    @Override
    public HddcB1GeomorlnonfractbltEntity getHddcB1Geomorlnonfractblt(String id) {
        HddcB1GeomorlnonfractbltEntity hddcB1Geomorlnonfractblt = this.hddcB1GeomorlnonfractbltRepository.findById(id).orElse(null);
         return hddcB1Geomorlnonfractblt;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB1GeomorlnonfractbltEntity saveHddcB1Geomorlnonfractblt(HddcB1GeomorlnonfractbltEntity hddcB1Geomorlnonfractblt) {
        String uuid = UUIDGenerator.getUUID();
        hddcB1Geomorlnonfractblt.setUuid(uuid);
        hddcB1Geomorlnonfractblt.setCreateUser(PlatformSessionUtils.getUserId());
        hddcB1Geomorlnonfractblt.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB1GeomorlnonfractbltRepository.save(hddcB1Geomorlnonfractblt);
        return hddcB1Geomorlnonfractblt;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB1GeomorlnonfractbltEntity updateHddcB1Geomorlnonfractblt(HddcB1GeomorlnonfractbltEntity hddcB1Geomorlnonfractblt) {
        HddcB1GeomorlnonfractbltEntity entity = hddcB1GeomorlnonfractbltRepository.findById(hddcB1Geomorlnonfractblt.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcB1Geomorlnonfractblt);
        hddcB1Geomorlnonfractblt.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcB1Geomorlnonfractblt.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB1GeomorlnonfractbltRepository.save(hddcB1Geomorlnonfractblt);
        return hddcB1Geomorlnonfractblt;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcB1Geomorlnonfractblts(List<String> ids) {
        List<HddcB1GeomorlnonfractbltEntity> hddcB1GeomorlnonfractbltList = this.hddcB1GeomorlnonfractbltRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcB1GeomorlnonfractbltList) && hddcB1GeomorlnonfractbltList.size() > 0) {
            for(HddcB1GeomorlnonfractbltEntity hddcB1Geomorlnonfractblt : hddcB1GeomorlnonfractbltList) {
                this.hddcB1GeomorlnonfractbltRepository.delete(hddcB1Geomorlnonfractblt);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcB1GeomorlnonfractbltQueryParams queryParams, HttpServletResponse response) {
        List<HddcB1GeomorlnonfractbltEntity> yhDisasterEntities = hddcB1GeomorlnonfractbltNativeRepository.exportYhDisasters(queryParams);
        List<HddcB1GeomorlnonfractbltVO> list=new ArrayList<>();
        for (HddcB1GeomorlnonfractbltEntity entity:yhDisasterEntities) {
            HddcB1GeomorlnonfractbltVO yhDisasterVO=new HddcB1GeomorlnonfractbltVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"地貌线与地表破裂带关联表","地貌线与地表破裂带关联表",HddcB1GeomorlnonfractbltVO.class,"地貌线与地表破裂带关联表.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcB1GeomorlnonfractbltVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcB1GeomorlnonfractbltVO.class, params);
            List<HddcB1GeomorlnonfractbltVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
           saveDisasterList(list, sb);
           returnMsg.append("成功导入" + (result.getList().size() ));
           returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcB1GeomorlnonfractbltVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcB1GeomorlnonfractbltVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcB1GeomorlnonfractbltVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcB1GeomorlnonfractbltEntity yhDisasterEntity = new HddcB1GeomorlnonfractbltEntity();
            HddcB1GeomorlnonfractbltVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcB1Geomorlnonfractblt(yhDisasterEntity);
        }
    }
}
