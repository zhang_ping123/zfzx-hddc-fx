package com.css.zfzx.sjcj.modules.hddcC4MagnitudeEstimateTable.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcC4MagnitudeEstimateTable.repository.entity.HddcC4MagnitudeestimatetableEntity;
import com.css.zfzx.sjcj.modules.hddcC4MagnitudeEstimateTable.viewobjects.HddcC4MagnitudeestimatetableQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import java.util.List;

/**
 * @author zhangping
 * @date 2020-11-23
 */

public interface HddcC4MagnitudeestimatetableService {

    public JSONObject queryHddcC4Magnitudeestimatetables(HddcC4MagnitudeestimatetableQueryParams queryParams, int curPage, int pageSize);

    public HddcC4MagnitudeestimatetableEntity getHddcC4Magnitudeestimatetable(String id);

    public HddcC4MagnitudeestimatetableEntity saveHddcC4Magnitudeestimatetable(HddcC4MagnitudeestimatetableEntity hddcC4Magnitudeestimatetable);

    public HddcC4MagnitudeestimatetableEntity updateHddcC4Magnitudeestimatetable(HddcC4MagnitudeestimatetableEntity hddcC4Magnitudeestimatetable);

    public void deleteHddcC4Magnitudeestimatetables(List<String> ids);


}
