package com.css.zfzx.sjcj.modules.hddcEpiMechanismResult.repository;

import com.css.zfzx.sjcj.modules.hddcEpiMechanismResult.repository.entity.HddcEpimechanismresultEntity;
import com.css.zfzx.sjcj.modules.hddcEpiMechanismResult.viewobjects.HddcEpimechanismresultQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */
public interface HddcEpimechanismresultNativeRepository {

    Page<HddcEpimechanismresultEntity> queryHddcEpimechanismresults(HddcEpimechanismresultQueryParams queryParams, int curPage, int pageSize);

    List<HddcEpimechanismresultEntity> exportYhDisasters(HddcEpimechanismresultQueryParams queryParams);
}
