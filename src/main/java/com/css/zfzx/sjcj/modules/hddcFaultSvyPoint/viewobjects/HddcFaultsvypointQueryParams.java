package com.css.zfzx.sjcj.modules.hddcFaultSvyPoint.viewobjects;

import lombok.Data;

/**
 * @author lihelei
 * @date 2020-11-26
 */
@Data
public class HddcFaultsvypointQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String targetfaultname;

}
