package com.css.zfzx.sjcj.modules.hddcB6_GeochemicalProjectTable.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB6_GeochemicalProjectTable.repository.entity.HddcB6GeochemicalprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB6_GeochemicalProjectTable.viewobjects.HddcB6GeochemicalprojecttableQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-27
 */

public interface HddcB6GeochemicalprojecttableService {

    public JSONObject queryHddcB6Geochemicalprojecttables(HddcB6GeochemicalprojecttableQueryParams queryParams, int curPage, int pageSize);

    public HddcB6GeochemicalprojecttableEntity getHddcB6Geochemicalprojecttable(String id);

    public HddcB6GeochemicalprojecttableEntity saveHddcB6Geochemicalprojecttable(HddcB6GeochemicalprojecttableEntity hddcB6Geochemicalprojecttable);

    public HddcB6GeochemicalprojecttableEntity updateHddcB6Geochemicalprojecttable(HddcB6GeochemicalprojecttableEntity hddcB6Geochemicalprojecttable);

    public void deleteHddcB6Geochemicalprojecttables(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB6GeochemicalprojecttableQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
