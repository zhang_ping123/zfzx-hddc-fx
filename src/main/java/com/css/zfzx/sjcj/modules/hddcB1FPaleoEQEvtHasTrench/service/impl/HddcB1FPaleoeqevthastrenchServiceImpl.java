package com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtHasTrench.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtHasTrench.repository.HddcB1FPaleoeqevthastrenchNativeRepository;
import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtHasTrench.repository.HddcB1FPaleoeqevthastrenchRepository;
import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtHasTrench.repository.entity.HddcB1FPaleoeqevthastrenchEntity;
import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtHasTrench.service.HddcB1FPaleoeqevthastrenchService;
import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtHasTrench.viewobjects.HddcB1FPaleoeqevthastrenchQueryParams;
import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtHasTrench.viewobjects.HddcB1FPaleoeqevthastrenchVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Service
public class HddcB1FPaleoeqevthastrenchServiceImpl implements HddcB1FPaleoeqevthastrenchService {

	@Autowired
    private HddcB1FPaleoeqevthastrenchRepository hddcB1FPaleoeqevthastrenchRepository;
    @Autowired
    private HddcB1FPaleoeqevthastrenchNativeRepository hddcB1FPaleoeqevthastrenchNativeRepository;

    @Override
    public JSONObject queryHddcB1FPaleoeqevthastrenchs(HddcB1FPaleoeqevthastrenchQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcB1FPaleoeqevthastrenchEntity> hddcB1FPaleoeqevthastrenchPage = this.hddcB1FPaleoeqevthastrenchNativeRepository.queryHddcB1FPaleoeqevthastrenchs(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcB1FPaleoeqevthastrenchPage);
        return jsonObject;
    }


    @Override
    public HddcB1FPaleoeqevthastrenchEntity getHddcB1FPaleoeqevthastrench(String id) {
        HddcB1FPaleoeqevthastrenchEntity hddcB1FPaleoeqevthastrench = this.hddcB1FPaleoeqevthastrenchRepository.findById(id).orElse(null);
         return hddcB1FPaleoeqevthastrench;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB1FPaleoeqevthastrenchEntity saveHddcB1FPaleoeqevthastrench(HddcB1FPaleoeqevthastrenchEntity hddcB1FPaleoeqevthastrench) {
        String uuid = UUIDGenerator.getUUID();
        hddcB1FPaleoeqevthastrench.setUuid(uuid);
        hddcB1FPaleoeqevthastrench.setCreateUser(PlatformSessionUtils.getUserId());
        hddcB1FPaleoeqevthastrench.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB1FPaleoeqevthastrenchRepository.save(hddcB1FPaleoeqevthastrench);
        return hddcB1FPaleoeqevthastrench;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB1FPaleoeqevthastrenchEntity updateHddcB1FPaleoeqevthastrench(HddcB1FPaleoeqevthastrenchEntity hddcB1FPaleoeqevthastrench) {
        HddcB1FPaleoeqevthastrenchEntity entity = hddcB1FPaleoeqevthastrenchRepository.findById(hddcB1FPaleoeqevthastrench.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcB1FPaleoeqevthastrench);
        hddcB1FPaleoeqevthastrench.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcB1FPaleoeqevthastrench.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB1FPaleoeqevthastrenchRepository.save(hddcB1FPaleoeqevthastrench);
        return hddcB1FPaleoeqevthastrench;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcB1FPaleoeqevthastrenchs(List<String> ids) {
        List<HddcB1FPaleoeqevthastrenchEntity> hddcB1FPaleoeqevthastrenchList = this.hddcB1FPaleoeqevthastrenchRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcB1FPaleoeqevthastrenchList) && hddcB1FPaleoeqevthastrenchList.size() > 0) {
            for(HddcB1FPaleoeqevthastrenchEntity hddcB1FPaleoeqevthastrench : hddcB1FPaleoeqevthastrenchList) {
                this.hddcB1FPaleoeqevthastrenchRepository.delete(hddcB1FPaleoeqevthastrench);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcB1FPaleoeqevthastrenchQueryParams queryParams, HttpServletResponse response) {
        List<HddcB1FPaleoeqevthastrenchEntity> yhDisasterEntities = hddcB1FPaleoeqevthastrenchNativeRepository.exportYhDisasters(queryParams);
        List<HddcB1FPaleoeqevthastrenchVO> list=new ArrayList<>();
        for (HddcB1FPaleoeqevthastrenchEntity entity:yhDisasterEntities) {
            HddcB1FPaleoeqevthastrenchVO yhDisasterVO=new HddcB1FPaleoeqevthastrenchVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "断层段古地震事件与探槽关联表", "断层段古地震事件与探槽关联表", HddcB1FPaleoeqevthastrenchVO.class, "断层段古地震事件与探槽关联表.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcB1FPaleoeqevthastrenchVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcB1FPaleoeqevthastrenchVO.class, params);
            List<HddcB1FPaleoeqevthastrenchVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcB1FPaleoeqevthastrenchVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcB1FPaleoeqevthastrenchVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcB1FPaleoeqevthastrenchVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcB1FPaleoeqevthastrenchEntity yhDisasterEntity = new HddcB1FPaleoeqevthastrenchEntity();
            HddcB1FPaleoeqevthastrenchVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcB1FPaleoeqevthastrench(yhDisasterEntity);
        }
    }

}
