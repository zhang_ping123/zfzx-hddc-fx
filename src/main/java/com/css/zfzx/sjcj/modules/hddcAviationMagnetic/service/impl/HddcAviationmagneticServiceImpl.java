package com.css.zfzx.sjcj.modules.hddcAviationMagnetic.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcActiveFault.viewobjects.HddcActivefaultVO;
import com.css.zfzx.sjcj.modules.hddcAviationMagnetic.repository.HddcAviationmagneticNativeRepository;
import com.css.zfzx.sjcj.modules.hddcAviationMagnetic.repository.HddcAviationmagneticRepository;
import com.css.zfzx.sjcj.modules.hddcAviationMagnetic.repository.entity.HddcAviationmagneticEntity;
import com.css.zfzx.sjcj.modules.hddcAviationMagnetic.service.HddcAviationmagneticService;
import com.css.zfzx.sjcj.modules.hddcAviationMagnetic.viewobjects.HddcAviationmagneticQueryParams;
import com.css.zfzx.sjcj.modules.hddcAviationMagnetic.viewobjects.HddcAviationmagneticVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zyb
 * @date 2020-11-26
 */
@Service
public class HddcAviationmagneticServiceImpl implements HddcAviationmagneticService {

	@Autowired
    private HddcAviationmagneticRepository hddcAviationmagneticRepository;
    @Autowired
    private HddcAviationmagneticNativeRepository hddcAviationmagneticNativeRepository;

    @Override
    public JSONObject queryHddcAviationmagnetics(HddcAviationmagneticQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcAviationmagneticEntity> hddcAviationmagneticPage = this.hddcAviationmagneticNativeRepository.queryHddcAviationmagnetics(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcAviationmagneticPage);
        return jsonObject;
    }


    @Override
    public HddcAviationmagneticEntity getHddcAviationmagnetic(String id) {
        HddcAviationmagneticEntity hddcAviationmagnetic = this.hddcAviationmagneticRepository.findById(id).orElse(null);
         return hddcAviationmagnetic;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcAviationmagneticEntity saveHddcAviationmagnetic(HddcAviationmagneticEntity hddcAviationmagnetic) {
        String uuid = UUIDGenerator.getUUID();
        hddcAviationmagnetic.setUuid(uuid);
        hddcAviationmagnetic.setCreateUser(PlatformSessionUtils.getUserId());
        hddcAviationmagnetic.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcAviationmagneticRepository.save(hddcAviationmagnetic);
        return hddcAviationmagnetic;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcAviationmagneticEntity updateHddcAviationmagnetic(HddcAviationmagneticEntity hddcAviationmagnetic) {
        HddcAviationmagneticEntity entity = hddcAviationmagneticRepository.findById(hddcAviationmagnetic.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcAviationmagnetic);
        hddcAviationmagnetic.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcAviationmagnetic.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcAviationmagneticRepository.save(hddcAviationmagnetic);
        return hddcAviationmagnetic;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcAviationmagnetics(List<String> ids) {
        List<HddcAviationmagneticEntity> hddcAviationmagneticList = this.hddcAviationmagneticRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcAviationmagneticList) && hddcAviationmagneticList.size() > 0) {
            for(HddcAviationmagneticEntity hddcAviationmagnetic : hddcAviationmagneticList) {
                this.hddcAviationmagneticRepository.delete(hddcAviationmagnetic);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcAviationmagneticQueryParams queryParams, HttpServletResponse response) {
        List<HddcAviationmagneticEntity> yhDisasterEntities = hddcAviationmagneticNativeRepository.exportYhDisasters(queryParams);
        List<HddcAviationmagneticVO> list=new ArrayList<>();
        for (HddcAviationmagneticEntity entity:yhDisasterEntities) {
            HddcAviationmagneticVO yhDisasterVO=new HddcAviationmagneticVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "航磁-线", "航磁-线", HddcAviationmagneticVO.class, "航磁-线.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcAviationmagneticVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcAviationmagneticVO.class, params);
            List<HddcAviationmagneticVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcAviationmagneticVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcAviationmagneticVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    public void saveDisasterList( List<HddcAviationmagneticVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcAviationmagneticEntity yhDisasterEntity = new HddcAviationmagneticEntity();
            HddcAviationmagneticVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcAviationmagnetic(yhDisasterEntity);
        }
    }


}
