package com.css.zfzx.sjcj.modules.hddcGeochemicalAbnSegment.repository;

import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnSegment.repository.entity.HddcGeochemicalabnsegmentEntity;
import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnSegment.viewobjects.HddcGeochemicalabnsegmentQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
public interface HddcGeochemicalabnsegmentNativeRepository {

    Page<HddcGeochemicalabnsegmentEntity> queryHddcGeochemicalabnsegments(HddcGeochemicalabnsegmentQueryParams queryParams, int curPage, int pageSize);

    List<HddcGeochemicalabnsegmentEntity> exportYhDisasters(HddcGeochemicalabnsegmentQueryParams queryParams);
}
