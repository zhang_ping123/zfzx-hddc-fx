package com.css.zfzx.sjcj.modules.hddccjproject.repository.entity;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangping
 * @date 2020-11-26
 */
@Data
@Entity
@Table(name="hddc_cj_project")
public class HddcCjProjectEntity implements Serializable {

    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 编号
     */
    @Id
    @Column(name="ID")
    private String id;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 项目描述
     */
    @Column(name="project_info")
    private String projectInfo;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 人员信息
     */
    @Column(name="person_ids")
    private String personIds;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;

}

