package com.css.zfzx.sjcj.modules.hddcGeoGeomorphySvyPoint.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author lihelei
 * @date 2020-11-26
 */
@Data
public class HddcGeogeomorphysvypointVO implements Serializable {

    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 照片镜向
     */
    @Excel(name = "照片镜向", orderNum = "4")
    private Integer photoviewingto;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 地貌点名称
     */
    @Excel(name = "地貌点名称", orderNum = "5")
    private String geomorphyname;
    /**
     * 地震地表破裂类型
     */
    @Excel(name = "地震地表破裂类型", orderNum = "6")
    private Integer fracturetype;
    /**
     * 乡
     */
    private String town;
    /**
     * 地质调查观测点编号
     */
    @Excel(name = "地质调查观测点编号", orderNum = "7")
    private String geologicalsvypointid;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 性质
     */
    @Excel(name = "性质", orderNum = "8")
    private String feature;
    /**
     * 形成时代
     */
    @Excel(name = "形成时代", orderNum = "9")
    private Integer createdate;
    /**
     * 备注
     */
    private String remark;
    /**
     * 平面图文件编号
     */
    @Excel(name = "平面图文件编号", orderNum = "10")
    private String sketchAiid;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 平面图原始文件编号
     */
    @Excel(name = "平面图原始文件编号", orderNum = "11")
    private String sketchArwid;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 是否为已知地震的地表破裂
     */
    @Excel(name = "是否为已知地震的地表破裂", orderNum = "12")
    private Integer issurfacerupturebelt;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 描述
     */
    @Excel(name = "描述", orderNum = "13")
    private String commentInfo;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 编号
     */
    private String id;
    /**
     * 是否修改工作底图
     */
    @Excel(name = "是否修改工作底图", orderNum = "14")
    private Integer ismodifyworkmap;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 典型照片文件编号
     */
    @Excel(name = "典型照片文件编号", orderNum = "15")
    private String photoAiid;
    /**
     * 垂直位移 [米]
     */
    @Excel(name = "垂直位移 [米]", orderNum = "16")
    private Double verticaldisplacement;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 地貌点类型
     */
    @Excel(name = "地貌点类型", orderNum = "17")
    private String geomorphytype;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 是否在图中显示
     */
    @Excel(name = "是否在图中显示", orderNum = "18")
    private Integer isinmap;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 地貌代码
     */
    @Excel(name = "地貌代码", orderNum = "19")
    private String geomorphycode;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 走向水平位移 [米]
     */
    @Excel(name = "走向水平位移 [米]", orderNum = "20")
    private Double horizenoffset;
    /**
     * 地表破裂（断塞塘等）长 [米]
     */
    @Excel(name = "地表破裂（断塞塘等）长 [米]", orderNum = "21")
    private Double length;
    /**
     * 典型照片原始文件编号
     */
    @Excel(name = "典型照片原始文件编号", orderNum = "22")
    private String photoArwid;
    /**
     * 地貌线走向（制图用）
     */
    @Excel(name = "地貌线走向（制图用）", orderNum = "23")
    private Integer geomorphylndirection;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 典型剖面图原始文件编号
     */
    @Excel(name = "典型剖面图原始文件编号", orderNum = "24")
    private String typicalprofileArwid;
    /**
     * 村
     */
    private String village;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 地表破裂（断塞塘等）高/深 [米]
     */
    @Excel(name = "地表破裂（断塞塘等）高/深 [米]", orderNum = "25")
    private Double height;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 拍摄者
     */
    @Excel(name = "拍摄者", orderNum = "26")
    private String photographer;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 水平//张缩位移 [米]
     */
    @Excel(name = "水平//张缩位移 [米]", orderNum = "27")
    private Double tensiondisplacement;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 符号或标注旋转角度
     */
    @Excel(name = "符号或标注旋转角度", orderNum = "28")
    private Double lastangle;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 观测点野外编号
     */
    @Excel(name = "观测点野外编号", orderNum = "29")
    private String fieldid;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 典型剖面图图表文件编号
     */
    @Excel(name = "典型剖面图图表文件编号", orderNum = "30")
    private String typicalprofileAiid;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 地表破裂（断塞塘等）宽 [米]
     */
    @Excel(name = "地表破裂（断塞塘等）宽 [米]", orderNum = "31")
    private Double width;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer photoviewingtoName;
    private Integer createdateName;
    private String rowNum;
    private String errorMsg;
}