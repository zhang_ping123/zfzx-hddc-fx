package com.css.zfzx.sjcj.modules.hddcwyDrillHole.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyDrillHole.repository.entity.HddcWyDrillholeEntity;
import com.css.zfzx.sjcj.modules.hddcwyDrillHole.service.HddcWyDrillholeService;
import com.css.zfzx.sjcj.modules.hddcwyDrillHole.viewobjects.HddcWyDrillholeQueryParams;
import com.css.bpm.platform.utils.PlatformPageUtils;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcWyDrillholes")
public class HddcWyDrillholeController {
    @Autowired
    private HddcWyDrillholeService hddcWyDrillholeService;

    @GetMapping("/queryHddcWyDrillholes")
    public RestResponse queryHddcWyDrillholes(HttpServletRequest request, HddcWyDrillholeQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcWyDrillholeService.queryHddcWyDrillholes(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/findAllByhddcWyDrillholes")
    public RestResponse findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid) {
        RestResponse response = null;
        try{
            List<HddcWyDrillholeEntity> list = hddcWyDrillholeService.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, isValid);
            response = RestResponse.succeed(list);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @GetMapping("{uuid}")
    public RestResponse getHddcWyDrillhole(@PathVariable String uuid) {
        RestResponse response = null;
        try{
            HddcWyDrillholeEntity hddcWyDrillhole = hddcWyDrillholeService.getHddcWyDrillhole(uuid);
            response = RestResponse.succeed(hddcWyDrillhole);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcWyDrillhole(@RequestBody HddcWyDrillholeEntity hddcWyDrillhole) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcWyDrillholeService.saveHddcWyDrillhole(hddcWyDrillhole);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcWyDrillhole(@RequestBody HddcWyDrillholeEntity hddcWyDrillhole)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcWyDrillholeService.updateHddcWyDrillhole(hddcWyDrillhole);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcWyDrillholes(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcWyDrillholeService.deleteHddcWyDrillholes(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcWyDrillholeService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }

    /***
     * 导出
     * @param response
     * @return
     */
    @GetMapping("/exportFile")
    public RestResponse exportFileYhDisasters(HttpServletResponse response,
                                              @RequestParam("projectName")String projectName, @RequestParam("taskName") String taskName,
                                              @RequestParam("startTime") String startTime, @RequestParam("endTime")String endTime) {
        RestResponse responseRest = null;
        JSONObject jsonObject = new JSONObject();
        try{
            HddcAppZztCountVo queryParams=new HddcAppZztCountVo();
            queryParams.setProjectName(projectName);
            queryParams.setTaskName(taskName);
            queryParams.setStartTime(startTime);
            queryParams.setEndTime(endTime);
            hddcWyDrillholeService.exportFile(queryParams,response);
            jsonObject.put("message", "导出成功");
            responseRest = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            responseRest = RestResponse.fail(errorMessage);
        }
        return responseRest;
    }


    /**
     * 导入
     *
     * @param file
     * @param response
     * @return
     */
    @PostMapping("/importDisaster")
    public RestResponse export(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        RestResponse restResponse = null;
        try {
            String s = hddcWyDrillholeService.exportExcel( file, response);
            restResponse = RestResponse.succeed(s);
        } catch (Exception e) {
            String errormessage = "导入失败";
            log.error(errormessage, e);
            restResponse = RestResponse.fail(errormessage);
        }
        return restResponse;
    }
}