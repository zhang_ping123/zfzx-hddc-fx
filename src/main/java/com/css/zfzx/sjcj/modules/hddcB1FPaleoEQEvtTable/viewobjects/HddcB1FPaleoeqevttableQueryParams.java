package com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtTable.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
public class HddcB1FPaleoeqevttableQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String faultname;

}
