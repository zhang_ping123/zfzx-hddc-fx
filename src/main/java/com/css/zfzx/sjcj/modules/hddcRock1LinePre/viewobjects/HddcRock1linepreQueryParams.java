package com.css.zfzx.sjcj.modules.hddcRock1LinePre.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-26
 */
@Data
public class HddcRock1linepreQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

}
