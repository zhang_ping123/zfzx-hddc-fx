package com.css.zfzx.sjcj.modules.hddcStationhddcStation.repository;

import com.css.zfzx.sjcj.modules.hddcStationhddcStation.repository.entity.HddcStationEntity;
import com.css.zfzx.sjcj.modules.hddcStationhddcStation.viewobjects.HddcStationQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */
public interface HddcStationNativeRepository {

    Page<HddcStationEntity> queryHddcStations(HddcStationQueryParams queryParams, int curPage, int pageSize);

    List<HddcStationEntity> exportYhDisasters(HddcStationQueryParams queryParams);
}
