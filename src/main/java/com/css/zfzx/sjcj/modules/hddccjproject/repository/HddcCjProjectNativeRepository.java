package com.css.zfzx.sjcj.modules.hddccjproject.repository;

import com.css.zfzx.sjcj.modules.hddccjproject.repository.entity.HddcCjProjectEntity;
import com.css.zfzx.sjcj.modules.hddccjproject.viewobjects.HddcCjProjectQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangping
 * @date 2020-11-26
 */
public interface HddcCjProjectNativeRepository {

    Page<HddcCjProjectEntity> queryHddcCjProjects(HddcCjProjectQueryParams queryParams, int curPage, int pageSize);

    Page<HddcCjProjectEntity> queryHddcCjProjectsByPersonId(String personId, int curPage, int pageSize);

    Integer queryHddcCjTasksByProjectId(String projectId);

    List<HddcCjProjectEntity> queryHddcCjProjectName();
}
