package com.css.zfzx.sjcj.modules.hddcRock1Pre.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Data
public class HddcRock1preQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String rockname;

}
