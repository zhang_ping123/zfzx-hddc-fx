package com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningLine.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningLine.repository.entity.HddcGeologicalsvyplanninglineEntity;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningLine.viewobjects.HddcGeologicalsvyplanninglineQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-07
 */

public interface HddcGeologicalsvyplanninglineService {

    public JSONObject queryHddcGeologicalsvyplanninglines(HddcGeologicalsvyplanninglineQueryParams queryParams, int curPage, int pageSize);

    public HddcGeologicalsvyplanninglineEntity getHddcGeologicalsvyplanningline(String id);

    public HddcGeologicalsvyplanninglineEntity saveHddcGeologicalsvyplanningline(HddcGeologicalsvyplanninglineEntity hddcGeologicalsvyplanningline);

    public HddcGeologicalsvyplanninglineEntity updateHddcGeologicalsvyplanningline(HddcGeologicalsvyplanninglineEntity hddcGeologicalsvyplanningline);

    public void deleteHddcGeologicalsvyplanninglines(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGeologicalsvyplanninglineQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
