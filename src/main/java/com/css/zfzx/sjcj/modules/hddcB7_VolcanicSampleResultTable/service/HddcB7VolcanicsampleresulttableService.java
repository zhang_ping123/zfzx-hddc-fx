package com.css.zfzx.sjcj.modules.hddcB7_VolcanicSampleResultTable.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB7_VolcanicSampleResultTable.repository.entity.HddcB7VolcanicsampleresulttableEntity;
import com.css.zfzx.sjcj.modules.hddcB7_VolcanicSampleResultTable.viewobjects.HddcB7VolcanicsampleresulttableQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-26
 */

public interface HddcB7VolcanicsampleresulttableService {

    public JSONObject queryHddcB7Volcanicsampleresulttables(HddcB7VolcanicsampleresulttableQueryParams queryParams, int curPage, int pageSize);

    public HddcB7VolcanicsampleresulttableEntity getHddcB7Volcanicsampleresulttable(String id);

    public HddcB7VolcanicsampleresulttableEntity saveHddcB7Volcanicsampleresulttable(HddcB7VolcanicsampleresulttableEntity hddcB7Volcanicsampleresulttable);

    public HddcB7VolcanicsampleresulttableEntity updateHddcB7Volcanicsampleresulttable(HddcB7VolcanicsampleresulttableEntity hddcB7Volcanicsampleresulttable);

    public void deleteHddcB7Volcanicsampleresulttables(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB7VolcanicsampleresulttableQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
