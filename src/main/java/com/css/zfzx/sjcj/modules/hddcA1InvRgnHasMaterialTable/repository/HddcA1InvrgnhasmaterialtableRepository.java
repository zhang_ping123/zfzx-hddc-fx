package com.css.zfzx.sjcj.modules.hddcA1InvRgnHasMaterialTable.repository;

import com.css.zfzx.sjcj.modules.hddcA1InvRgnHasMaterialTable.repository.entity.HddcA1InvrgnhasmaterialtableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcA1InvrgnhasmaterialtableRepository extends JpaRepository<HddcA1InvrgnhasmaterialtableEntity, String> {
}
