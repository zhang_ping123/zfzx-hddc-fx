package com.css.zfzx.sjcj.modules.hddcTargetRegion.repository;

import com.css.zfzx.sjcj.modules.hddcTargetRegion.repository.entity.HddcTargetregionEntity;
import com.css.zfzx.sjcj.modules.hddcTargetRegion.viewobjects.HddcTargetregionQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcTargetregionNativeRepository {

    Page<HddcTargetregionEntity> queryHddcTargetregions(HddcTargetregionQueryParams queryParams, int curPage, int pageSize);

    List<HddcTargetregionEntity> exportYhDisasters(HddcTargetregionQueryParams queryParams);
}
