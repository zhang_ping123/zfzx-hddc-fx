package com.css.zfzx.sjcj.modules.hddcA1LiteratureDocumentTable.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.bpm.platform.utils.PlatformPageUtils;
import com.css.zfzx.sjcj.modules.hddcA1LiteratureDocumentTable.repository.entity.HddcA1LiteraturedocumenttableEntity;
import com.css.zfzx.sjcj.modules.hddcA1LiteratureDocumentTable.service.HddcA1LiteraturedocumenttableService;
import com.css.zfzx.sjcj.modules.hddcA1LiteratureDocumentTable.viewobjects.HddcA1LiteraturedocumenttableQueryParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcA1Literaturedocumenttables")
public class HddcA1LiteraturedocumenttableController {
    @Autowired
    private HddcA1LiteraturedocumenttableService hddcA1LiteraturedocumenttableService;

    @GetMapping("/queryHddcA1Literaturedocumenttables")
    public RestResponse queryHddcA1Literaturedocumenttables(HttpServletRequest request, HddcA1LiteraturedocumenttableQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcA1LiteraturedocumenttableService.queryHddcA1Literaturedocumenttables(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcA1Literaturedocumenttable(@PathVariable String id) {
        RestResponse response = null;
        try{
            HddcA1LiteraturedocumenttableEntity hddcA1Literaturedocumenttable = hddcA1LiteraturedocumenttableService.getHddcA1Literaturedocumenttable(id);
            response = RestResponse.succeed(hddcA1Literaturedocumenttable);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcA1Literaturedocumenttable(@RequestBody HddcA1LiteraturedocumenttableEntity hddcA1Literaturedocumenttable) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcA1LiteraturedocumenttableService.saveHddcA1Literaturedocumenttable(hddcA1Literaturedocumenttable);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcA1Literaturedocumenttable(@RequestBody HddcA1LiteraturedocumenttableEntity hddcA1Literaturedocumenttable)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcA1LiteraturedocumenttableService.updateHddcA1Literaturedocumenttable(hddcA1Literaturedocumenttable);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcA1Literaturedocumenttables(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcA1LiteraturedocumenttableService.deleteHddcA1Literaturedocumenttables(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcA1LiteraturedocumenttableService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }
    /***
     * 导出
     * @param response
     * @return
     */
    @GetMapping("/exportFile")
    public RestResponse exportFileYhDisasters(HttpServletResponse response,
                                              @RequestParam("projectName")String projectName,@RequestParam("publication") String publication,
                                              @RequestParam("province") String province,@RequestParam("city")String city,@RequestParam("area")String area) {
        RestResponse responseRest = null;
        JSONObject jsonObject = new JSONObject();
        try{
            HddcA1LiteraturedocumenttableQueryParams queryParams=new HddcA1LiteraturedocumenttableQueryParams();
            queryParams.setArea(area);
            queryParams.setCity(city);
            queryParams.setProvince(province);
            queryParams.setProjectName(projectName);
            queryParams.setPublication(publication);
            hddcA1LiteraturedocumenttableService.exportFile(queryParams,response);
            jsonObject.put("message", "导出成功");
            responseRest = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            responseRest = RestResponse.fail(errorMessage);
        }
        return responseRest;
    }
    /**
     * 导入
     *
     * @param file
     * @param response
     * @return
     */
    @PostMapping("/importDisaster")
    public RestResponse export(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        RestResponse restResponse = null;
        try {
            String s = hddcA1LiteraturedocumenttableService.exportExcel( file, response);
            restResponse = RestResponse.succeed(s);
        } catch (Exception e) {
            String errormessage = "导入失败";
            log.error(errormessage, e);
            restResponse = RestResponse.fail(errormessage);
        }
        return restResponse;
    }
}