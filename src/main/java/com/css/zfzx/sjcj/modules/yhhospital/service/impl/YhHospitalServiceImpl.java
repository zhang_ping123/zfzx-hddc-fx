package com.css.zfzx.sjcj.modules.yhhospital.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.yhhospital.repository.YhHospitalNativeRepository;
import com.css.zfzx.sjcj.modules.yhhospital.repository.YhHospitalRepository;
import com.css.zfzx.sjcj.modules.yhhospital.repository.entity.YhHospitalEntity;
import com.css.zfzx.sjcj.modules.yhhospital.service.YhHospitalService;
import com.css.zfzx.sjcj.modules.yhhospital.viewobjects.YhHospitalQueryParams;
import com.css.zfzx.sjcj.modules.yhhospital.viewobjects.YhHospitalVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author yyd
 * @date 2020-11-03
 */
@Service
public class YhHospitalServiceImpl implements YhHospitalService {

	@Autowired
    private YhHospitalRepository yhHospitalRepository;
    @Autowired
    private YhHospitalNativeRepository yhHospitalNativeRepository;

    @Override
    public JSONObject queryYhHospitals(YhHospitalQueryParams queryParams, int curPage, int pageSize) {
        Page<YhHospitalEntity> yhHospitalPage = this.yhHospitalNativeRepository.queryYhHospitals(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(yhHospitalPage);
        return jsonObject;
    }


    @Override
    public YhHospitalEntity getYhHospital(String id) {
        YhHospitalEntity yhHospital = this.yhHospitalRepository.findById(id).orElse(null);
         return yhHospital;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public YhHospitalEntity saveYhHospital(YhHospitalEntity yhHospital) {
        String uuid = UUIDGenerator.getUUID();
        yhHospital.setId(uuid);
        yhHospital.setCreateUser(PlatformSessionUtils.getUserId());
        yhHospital.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.yhHospitalRepository.save(yhHospital);
        return yhHospital;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public YhHospitalEntity updateYhHospital(YhHospitalEntity yhHospital) {
        YhHospitalEntity entity = yhHospitalRepository.findById(yhHospital.getId()).get();
        UpdateUtil.copyNullProperties(entity,yhHospital);
        yhHospital.setUpdateUser(PlatformSessionUtils.getUserId());
        yhHospital.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.yhHospitalRepository.save(yhHospital);
        return yhHospital;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteYhHospitals(List<String> ids) {
        List<YhHospitalEntity> yhHospitalList = this.yhHospitalRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(yhHospitalList) && yhHospitalList.size() > 0) {
            for(YhHospitalEntity yhHospital : yhHospitalList) {
                this.yhHospitalRepository.delete(yhHospital);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

}
