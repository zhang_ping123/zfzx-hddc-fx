package com.css.zfzx.sjcj.modules.hddcB1TrenchBelongSamplePrj.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB1TrenchBelongSamplePrj.repository.entity.HddcB1TrenchbelongsampleprjEntity;
import com.css.zfzx.sjcj.modules.hddcB1TrenchBelongSamplePrj.viewobjects.HddcB1TrenchbelongsampleprjQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */

public interface HddcB1TrenchbelongsampleprjService {

    public JSONObject queryHddcB1Trenchbelongsampleprjs(HddcB1TrenchbelongsampleprjQueryParams queryParams, int curPage, int pageSize);

    public HddcB1TrenchbelongsampleprjEntity getHddcB1Trenchbelongsampleprj(String id);

    public HddcB1TrenchbelongsampleprjEntity saveHddcB1Trenchbelongsampleprj(HddcB1TrenchbelongsampleprjEntity hddcB1Trenchbelongsampleprj);

    public HddcB1TrenchbelongsampleprjEntity updateHddcB1Trenchbelongsampleprj(HddcB1TrenchbelongsampleprjEntity hddcB1Trenchbelongsampleprj);

    public void deleteHddcB1Trenchbelongsampleprjs(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB1TrenchbelongsampleprjQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
