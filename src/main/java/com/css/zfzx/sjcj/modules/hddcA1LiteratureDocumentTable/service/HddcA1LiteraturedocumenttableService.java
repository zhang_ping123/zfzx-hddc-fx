package com.css.zfzx.sjcj.modules.hddcA1LiteratureDocumentTable.service;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.zfzx.sjcj.modules.hddcA1LiteratureDocumentTable.repository.entity.HddcA1LiteraturedocumenttableEntity;
import com.css.zfzx.sjcj.modules.hddcA1LiteratureDocumentTable.viewobjects.HddcA1LiteraturedocumenttableQueryParams;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */

public interface HddcA1LiteraturedocumenttableService {

    public JSONObject queryHddcA1Literaturedocumenttables(HddcA1LiteraturedocumenttableQueryParams queryParams, int curPage, int pageSize);

    public HddcA1LiteraturedocumenttableEntity getHddcA1Literaturedocumenttable(String id);

    public HddcA1LiteraturedocumenttableEntity saveHddcA1Literaturedocumenttable(HddcA1LiteraturedocumenttableEntity hddcA1Literaturedocumenttable);

    public HddcA1LiteraturedocumenttableEntity updateHddcA1Literaturedocumenttable(HddcA1LiteraturedocumenttableEntity hddcA1Literaturedocumenttable);

    public void deleteHddcA1Literaturedocumenttables(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcA1LiteraturedocumenttableQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
