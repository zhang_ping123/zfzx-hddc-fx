package com.css.zfzx.sjcj.modules.hddcB7_VolcanicDataTable.repository;

import com.css.zfzx.sjcj.modules.hddcB7_VolcanicDataTable.repository.entity.HddcB7VolcanicdatatableEntity;
import com.css.zfzx.sjcj.modules.hddcB7_VolcanicDataTable.viewobjects.HddcB7VolcanicdatatableQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-26
 */
public interface HddcB7VolcanicdatatableNativeRepository {

    Page<HddcB7VolcanicdatatableEntity> queryHddcB7Volcanicdatatables(HddcB7VolcanicdatatableQueryParams queryParams, int curPage, int pageSize);

    List<HddcB7VolcanicdatatableEntity> exportYhDisasters(HddcB7VolcanicdatatableQueryParams queryParams);
}
