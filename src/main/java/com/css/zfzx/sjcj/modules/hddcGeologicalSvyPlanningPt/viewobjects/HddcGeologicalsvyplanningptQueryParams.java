package com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningPt.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-12-07
 */
@Data
public class HddcGeologicalsvyplanningptQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String taskName;

}
