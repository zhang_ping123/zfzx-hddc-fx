package com.css.zfzx.sjcj.modules.hddcDResultmaptable.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcDResultmaptable.repository.entity.HddcDResultmaptableEntity;
import com.css.zfzx.sjcj.modules.hddcDResultmaptable.viewobjects.HddcDResultmaptableQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author zyb
 * @date 2020-12-19
 */

public interface HddcDResultmaptableService {

    public JSONObject queryHddcDResultmaptables(HddcDResultmaptableQueryParams queryParams, int curPage, int pageSize,String sort,String order);

    public HddcDResultmaptableEntity getHddcDResultmaptable(String id);

    public HddcDResultmaptableEntity saveHddcDResultmaptable(HddcDResultmaptableEntity hddcDResultmaptable);

    public HddcDResultmaptableEntity updateHddcDResultmaptable(HddcDResultmaptableEntity hddcDResultmaptable);

    public void deleteHddcDResultmaptables(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

//    int upload(MultipartFile file, String filenumber);


}
