package com.css.zfzx.sjcj.modules.hddcGeoGeomorphySvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcGeoGeomorphySvyPoint.repository.entity.HddcGeogeomorphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcGeoGeomorphySvyPoint.viewobjects.HddcGeogeomorphysvypointQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author lihelei
 * @date 2020-11-26
 */
public interface HddcGeogeomorphysvypointNativeRepository {

    Page<HddcGeogeomorphysvypointEntity> queryHddcGeogeomorphysvypoints(HddcGeogeomorphysvypointQueryParams queryParams, int curPage, int pageSize);

    List<HddcGeogeomorphysvypointEntity> exportYhDisasters(HddcGeogeomorphysvypointQueryParams queryParams);
}
