package com.css.zfzx.sjcj.modules.hddcImageIndexLayer.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcImageIndexLayer.repository.entity.HddcImageindexlayerEntity;
import com.css.zfzx.sjcj.modules.hddcImageIndexLayer.viewobjects.HddcImageindexlayerQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangping
 * @date 2020-11-30
 */

public interface HddcImageindexlayerService {

    public JSONObject queryHddcImageindexlayers(HddcImageindexlayerQueryParams queryParams, int curPage, int pageSize);

    public HddcImageindexlayerEntity getHddcImageindexlayer(String id);

    public HddcImageindexlayerEntity saveHddcImageindexlayer(HddcImageindexlayerEntity hddcImageindexlayer);

    public HddcImageindexlayerEntity updateHddcImageindexlayer(HddcImageindexlayerEntity hddcImageindexlayer);

    public void deleteHddcImageindexlayers(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcImageindexlayerQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
