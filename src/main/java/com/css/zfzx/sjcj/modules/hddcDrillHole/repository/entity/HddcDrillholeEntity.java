package com.css.zfzx.sjcj.modules.hddcDrillHole.repository.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Data
@Entity
@Table(name="hddc_drillhole")
public class HddcDrillholeEntity implements Serializable {

    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 钻孔剖面编号
     */
    @Column(name="profileid")
    private String profileid;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 岩芯照片图像档案编号
     */
    @Column(name="corephoto_aiid")
    private String corephotoAiid;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 孔位经度
     */
    @Column(name="lon")
    private Double lon;
    /**
     * 钻孔班报原始文件编号
     */
    @Column(name="drillinglog_arwid")
    private String drillinglogArwid;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 封孔设计及封孔报告书原始文件编号
     */
    @Column(name="sealdesignreport_arwid")
    private String sealdesignreportArwid;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 采样记录表编号
     */
    @Column(name="geologysmplrec_aiid")
    private String geologysmplrecAiid;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 井斜测量结果登记表文件编号
     */
    @Column(name="wellclination_aiid")
    private String wellclinationAiid;
    /**
     * 送样总数
     */
    @Column(name="samplecount")
    private Integer samplecount;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 封孔设计及封孔报告书文件编号
     */
    @Column(name="sealdesignreport_arid")
    private String sealdesignreportArid;
    /**
     * 原始岩芯编录表原始
     */
    @Column(name="corecatalog_arwid")
    private String corecatalogArwid;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 简易水文观测记录表原始
     */
    @Column(name="hydrorecord_arwid")
    private String hydrorecordArwid;
    /**
     * 钻孔质量验收报告文件编号
     */
    @Column(name="sealcheck_arid")
    private String sealcheckArid;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 钻孔质量验收报告原始文件编号
     */
    @Column(name="sealcheck_arwid")
    private String sealcheckArwid;
    /**
     * 采集样品总数
     */
    @Column(name="collectedsamplecount")
    private Integer collectedsamplecount;
    /**
     * 钻孔来源与类型
     */
    @Column(name="drillsource")
    private Integer drillsource;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;
    /**
     * 中更新统厚度 [米]
     */
    @Column(name="midpleithickness")
    private Double midpleithickness;
    /**
     * 获得结果样品总数
     */
    @Column(name="datingsamplecount")
    private Integer datingsamplecount;
    /**
     * 原始岩芯编录表图像文件编号
     */
    @Column(name="corecatalog_aiid")
    private String corecatalogAiid;
    /**
     * 钻探目的
     */
    @Column(name="purpose")
    private String purpose;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 全新统厚度 [米]
     */
    @Column(name="holocenethickness")
    private Double holocenethickness;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 钻探日期
     */
    @Column(name="drilldate")
    private String drilldate;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 岩芯照片原始档案编号
     */
    @Column(name="corephoto_arwid")
    private String corephotoArwid;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 钻孔描述
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 钻孔孔深检查原始文件编号
     */
    @Column(name="depthcheck_arwid")
    private String depthcheckArwid;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 简易水文观测记录表
     */
    @Column(name="hydrorecord_aiid")
    private String hydrorecordAiid;
    /**
     * 井斜测量结果登记表原始文件编号
     */
    @Column(name="wellclination_arwid")
    private String wellclinationArwid;
    /**
     * 工程编号
     */
    @Column(name="projectid")
    private String projectId;
    /**
     * 项目ID
     */
    @Column(name="project_code")
    private String projectCode;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 下更新统厚度 [米]
     */
    @Column(name="lowpleithickness")
    private Double lowpleithickness;
    /**
     * 钻孔编号
     */
    @Column(name="id")
    private String id;
    /**
     * 编号
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 孔口标高 [米]
     */
    @Column(name="elevation")
    private Double elevation;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 岩芯总长 [米]
     */
    @Column(name="coretotalthickness")
    private Double coretotalthickness;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 钻孔柱状图图像文件编号
     */
    @Column(name="columnchart_aiid")
    private String columnchartAiid;
    /**
     * 采样记录表原始文件编号
     */
    @Column(name="geologysmplrec_arwid")
    private String geologysmplrecArwid;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 钻孔柱状图原始档案编号
     */
    @Column(name="columnchart_arwid")
    private String columnchartArwid;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 野外编号
     */
    @Column(name="fieldid")
    private String fieldid;
    /**
     * 钻探地点
     */
    @Column(name="locationname")
    private String locationname;
    /**
     * 孔深 [米]
     */
    @Column(name="depth")
    private Double depth;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 获得测试结果的环境与工程样品数
     */
    @Column(name="testedenviromentsamplecount")
    private Integer testedenviromentsamplecount;
    /**
     * 采集环境与工程样品数
     */
    @Column(name="collectedenviromentsamplecount")
    private Integer collectedenviromentsamplecount;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 钻孔班报编号
     */
    @Column(name="drillinglog_aiid")
    private String drillinglogAiid;
    /**
     * 孔位纬度
     */
    @Column(name="lat")
    private Double lat;
    /**
     * 环境与工程样品送样总数
     */
    @Column(name="enviromentsamplecount")
    private Integer enviromentsamplecount;
    /**
     * 上更新统厚度 [米]
     */
    @Column(name="uppleithickness")
    private Double uppleithickness;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 钻孔孔深检查编号
     */
    @Column(name="depthcheck_aiid")
    private String depthcheckAiid;
    /**
     * 前第四纪厚度 [米]
     */
    @Column(name="prepleithickness")
    private Double prepleithickness;
    /**
     * 是否开展地球物理测井
     */
    @Column(name="isgeophywell")
    private Integer isgeophywell;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;

}

