package com.css.zfzx.sjcj.modules.hddcRelocationISCatalog.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Data
public class HddcRelocationiscatalogQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String locationname;

}
