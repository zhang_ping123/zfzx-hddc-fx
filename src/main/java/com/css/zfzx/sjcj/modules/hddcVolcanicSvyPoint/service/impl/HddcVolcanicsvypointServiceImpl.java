package com.css.zfzx.sjcj.modules.hddcVolcanicSvyPoint.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltVO;
import com.css.zfzx.sjcj.modules.hddcVolcanicSvyPoint.repository.HddcVolcanicsvypointNativeRepository;
import com.css.zfzx.sjcj.modules.hddcVolcanicSvyPoint.repository.HddcVolcanicsvypointRepository;
import com.css.zfzx.sjcj.modules.hddcVolcanicSvyPoint.repository.entity.HddcVolcanicsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcVolcanicSvyPoint.service.HddcVolcanicsvypointService;
import com.css.zfzx.sjcj.modules.hddcVolcanicSvyPoint.viewobjects.HddcVolcanicsvypointQueryParams;
import com.css.zfzx.sjcj.modules.hddcVolcanicSvyPoint.viewobjects.HddcVolcanicsvypointVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
@Service
public class HddcVolcanicsvypointServiceImpl implements HddcVolcanicsvypointService {

	@Autowired
    private HddcVolcanicsvypointRepository hddcVolcanicsvypointRepository;
    @Autowired
    private HddcVolcanicsvypointNativeRepository hddcVolcanicsvypointNativeRepository;

    @Override
    public JSONObject queryHddcVolcanicsvypoints(HddcVolcanicsvypointQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcVolcanicsvypointEntity> hddcVolcanicsvypointPage = this.hddcVolcanicsvypointNativeRepository.queryHddcVolcanicsvypoints(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcVolcanicsvypointPage);
        return jsonObject;
    }


    @Override
    public HddcVolcanicsvypointEntity getHddcVolcanicsvypoint(String id) {
        HddcVolcanicsvypointEntity hddcVolcanicsvypoint = this.hddcVolcanicsvypointRepository.findById(id).orElse(null);
         return hddcVolcanicsvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcVolcanicsvypointEntity saveHddcVolcanicsvypoint(HddcVolcanicsvypointEntity hddcVolcanicsvypoint) {
        String uuid = UUIDGenerator.getUUID();
        hddcVolcanicsvypoint.setUuid(uuid);
        hddcVolcanicsvypoint.setCreateUser(PlatformSessionUtils.getUserId());
        hddcVolcanicsvypoint.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcVolcanicsvypointRepository.save(hddcVolcanicsvypoint);
        return hddcVolcanicsvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcVolcanicsvypointEntity updateHddcVolcanicsvypoint(HddcVolcanicsvypointEntity hddcVolcanicsvypoint) {
        HddcVolcanicsvypointEntity entity = hddcVolcanicsvypointRepository.findById(hddcVolcanicsvypoint.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcVolcanicsvypoint);
        hddcVolcanicsvypoint.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcVolcanicsvypoint.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcVolcanicsvypointRepository.save(hddcVolcanicsvypoint);
        return hddcVolcanicsvypoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcVolcanicsvypoints(List<String> ids) {
        List<HddcVolcanicsvypointEntity> hddcVolcanicsvypointList = this.hddcVolcanicsvypointRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcVolcanicsvypointList) && hddcVolcanicsvypointList.size() > 0) {
            for(HddcVolcanicsvypointEntity hddcVolcanicsvypoint : hddcVolcanicsvypointList) {
                this.hddcVolcanicsvypointRepository.delete(hddcVolcanicsvypoint);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcVolcanicsvypointQueryParams queryParams, HttpServletResponse response) {
        List<HddcVolcanicsvypointEntity> yhDisasterEntities = hddcVolcanicsvypointNativeRepository.exportYhDisasters(queryParams);
        List<HddcVolcanicsvypointVO> list=new ArrayList<>();
        for (HddcVolcanicsvypointEntity entity:yhDisasterEntities) {
            HddcVolcanicsvypointVO yhDisasterVO=new HddcVolcanicsvypointVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"火山调查观测点-点","火山调查观测点-点",HddcVolcanicsvypointVO.class,"火山调查观测点-点.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcVolcanicsvypointVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcVolcanicsvypointVO.class, params);
            List<HddcVolcanicsvypointVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcVolcanicsvypointVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcVolcanicsvypointVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcVolcanicsvypointVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcVolcanicsvypointEntity yhDisasterEntity = new HddcVolcanicsvypointEntity();
            HddcVolcanicsvypointVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcVolcanicsvypoint(yhDisasterEntity);
        }
    }
}
