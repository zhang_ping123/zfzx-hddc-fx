package com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyPoint.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyPoint.repository.entity.HddcWyGeochemicalsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeochemicalSvyPoint.viewobjects.HddcWyGeochemicalsvypointQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-02
 */
public interface HddcWyGeochemicalsvypointNativeRepository {

    Page<HddcWyGeochemicalsvypointEntity> queryHddcWyGeochemicalsvypoints(HddcWyGeochemicalsvypointQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyGeochemicalsvypoint(HddcAppZztCountVo queryParams);

    List<HddcWyGeochemicalsvypointEntity> exportChemPoint(HddcAppZztCountVo queryParams);
}
