package com.css.zfzx.sjcj.modules.hddcDResultmaptable.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-12-19
 */
@Data
public class HddcDResultmaptableQueryParams {


    private String province;
    private String city;
    private String area;
    private String cityname;
    private String filename;
    private String id;
    private String authors;
    private String pageType;

}
