package com.css.zfzx.sjcj.modules.hddcGravityField.repository;

import com.css.zfzx.sjcj.modules.hddcGravityField.repository.entity.HddcGravityfieldEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-26
 */
public interface HddcGravityfieldRepository extends JpaRepository<HddcGravityfieldEntity, String> {
}
