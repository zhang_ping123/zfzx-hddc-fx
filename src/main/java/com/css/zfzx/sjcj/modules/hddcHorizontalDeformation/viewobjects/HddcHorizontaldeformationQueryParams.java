package com.css.zfzx.sjcj.modules.hddcHorizontalDeformation.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-26
 */
@Data
public class HddcHorizontaldeformationQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

}
