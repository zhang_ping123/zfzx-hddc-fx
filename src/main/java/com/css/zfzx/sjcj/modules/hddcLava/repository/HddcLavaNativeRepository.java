package com.css.zfzx.sjcj.modules.hddcLava.repository;

import com.css.zfzx.sjcj.modules.hddcLava.repository.entity.HddcLavaEntity;
import com.css.zfzx.sjcj.modules.hddcLava.viewobjects.HddcLavaQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
public interface HddcLavaNativeRepository {

    Page<HddcLavaEntity> queryHddcLavas(HddcLavaQueryParams queryParams, int curPage, int pageSize);

    List<HddcLavaEntity> exportYhDisasters(HddcLavaQueryParams queryParams);
}
