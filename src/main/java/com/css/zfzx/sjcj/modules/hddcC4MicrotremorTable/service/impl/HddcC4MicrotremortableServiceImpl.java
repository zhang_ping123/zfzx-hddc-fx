package com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.repository.HddcC4MicrotremortableNativeRepository;
import com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.repository.HddcC4MicrotremortableRepository;
import com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.repository.entity.HddcC4MicrotremortableEntity;
import com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.service.HddcC4MicrotremortableService;
import com.css.zfzx.sjcj.modules.hddcC4MicrotremorTable.viewobjects.HddcC4MicrotremortableQueryParams;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author zhangping
 * @date 2020-11-23
 */
@Service
public class HddcC4MicrotremortableServiceImpl implements HddcC4MicrotremortableService {

	@Autowired
    private HddcC4MicrotremortableRepository hddcC4MicrotremortableRepository;
    @Autowired
    private HddcC4MicrotremortableNativeRepository hddcC4MicrotremortableNativeRepository;

    @Override
    public JSONObject queryHddcC4Microtremortables(HddcC4MicrotremortableQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcC4MicrotremortableEntity> hddcC4MicrotremortablePage = this.hddcC4MicrotremortableNativeRepository.queryHddcC4Microtremortables(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcC4MicrotremortablePage);
        return jsonObject;
    }


    @Override
    public HddcC4MicrotremortableEntity getHddcC4Microtremortable(String id) {
        HddcC4MicrotremortableEntity hddcC4Microtremortable = this.hddcC4MicrotremortableRepository.findById(id).orElse(null);
         return hddcC4Microtremortable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcC4MicrotremortableEntity saveHddcC4Microtremortable(HddcC4MicrotremortableEntity hddcC4Microtremortable) {
        String uuid = UUIDGenerator.getUUID();
        hddcC4Microtremortable.setId(uuid);
//        hddcC4Microtremortable.setCreateUser(PlatformSessionUtils.getUserId());
        hddcC4Microtremortable.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcC4MicrotremortableRepository.save(hddcC4Microtremortable);
        return hddcC4Microtremortable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcC4MicrotremortableEntity updateHddcC4Microtremortable(HddcC4MicrotremortableEntity hddcC4Microtremortable) {
        HddcC4MicrotremortableEntity entity = hddcC4MicrotremortableRepository.findById(hddcC4Microtremortable.getId()).get();
        UpdateUtil.copyNullProperties(entity,hddcC4Microtremortable);
        hddcC4Microtremortable.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcC4Microtremortable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcC4MicrotremortableRepository.save(hddcC4Microtremortable);
        return hddcC4Microtremortable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcC4Microtremortables(List<String> ids) {
        List<HddcC4MicrotremortableEntity> hddcC4MicrotremortableList = this.hddcC4MicrotremortableRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcC4MicrotremortableList) && hddcC4MicrotremortableList.size() > 0) {
            for(HddcC4MicrotremortableEntity hddcC4Microtremortable : hddcC4MicrotremortableList) {
                this.hddcC4MicrotremortableRepository.delete(hddcC4Microtremortable);
            }
        }
    }


}
