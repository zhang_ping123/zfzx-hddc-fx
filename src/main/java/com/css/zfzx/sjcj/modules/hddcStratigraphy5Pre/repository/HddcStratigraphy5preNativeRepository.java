package com.css.zfzx.sjcj.modules.hddcStratigraphy5Pre.repository;

import com.css.zfzx.sjcj.modules.hddcStratigraphy5Pre.repository.entity.HddcStratigraphy5preEntity;
import com.css.zfzx.sjcj.modules.hddcStratigraphy5Pre.viewobjects.HddcStratigraphy5preQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcStratigraphy5preNativeRepository {

    Page<HddcStratigraphy5preEntity> queryHddcStratigraphy5pres(HddcStratigraphy5preQueryParams queryParams, int curPage, int pageSize);

    List<HddcStratigraphy5preEntity> exportYhDisasters(HddcStratigraphy5preQueryParams queryParams);
}
