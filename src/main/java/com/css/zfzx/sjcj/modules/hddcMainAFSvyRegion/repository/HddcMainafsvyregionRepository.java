package com.css.zfzx.sjcj.modules.hddcMainAFSvyRegion.repository;

import com.css.zfzx.sjcj.modules.hddcMainAFSvyRegion.repository.entity.HddcMainafsvyregionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-12-14
 */
public interface HddcMainafsvyregionRepository extends JpaRepository<HddcMainafsvyregionEntity, String> {
}
