package com.css.zfzx.sjcj.modules.hddcGeochemicalAbnSegment.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnSegment.repository.HddcGeochemicalabnsegmentNativeRepository;
import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnSegment.repository.HddcGeochemicalabnsegmentRepository;
import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnSegment.repository.entity.HddcGeochemicalabnsegmentEntity;
import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnSegment.service.HddcGeochemicalabnsegmentService;
import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnSegment.viewobjects.HddcGeochemicalabnsegmentQueryParams;
import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnSegment.viewobjects.HddcGeochemicalabnsegmentVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Service
public class HddcGeochemicalabnsegmentServiceImpl implements HddcGeochemicalabnsegmentService {

	@Autowired
    private HddcGeochemicalabnsegmentRepository hddcGeochemicalabnsegmentRepository;
    @Autowired
    private HddcGeochemicalabnsegmentNativeRepository hddcGeochemicalabnsegmentNativeRepository;

    @Override
    public JSONObject queryHddcGeochemicalabnsegments(HddcGeochemicalabnsegmentQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcGeochemicalabnsegmentEntity> hddcGeochemicalabnsegmentPage = this.hddcGeochemicalabnsegmentNativeRepository.queryHddcGeochemicalabnsegments(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcGeochemicalabnsegmentPage);
        return jsonObject;
    }


    @Override
    public HddcGeochemicalabnsegmentEntity getHddcGeochemicalabnsegment(String id) {
        HddcGeochemicalabnsegmentEntity hddcGeochemicalabnsegment = this.hddcGeochemicalabnsegmentRepository.findById(id).orElse(null);
         return hddcGeochemicalabnsegment;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeochemicalabnsegmentEntity saveHddcGeochemicalabnsegment(HddcGeochemicalabnsegmentEntity hddcGeochemicalabnsegment) {
        String uuid = UUIDGenerator.getUUID();
        hddcGeochemicalabnsegment.setUuid(uuid);
        hddcGeochemicalabnsegment.setCreateUser(PlatformSessionUtils.getUserId());
        hddcGeochemicalabnsegment.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeochemicalabnsegmentRepository.save(hddcGeochemicalabnsegment);
        return hddcGeochemicalabnsegment;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeochemicalabnsegmentEntity updateHddcGeochemicalabnsegment(HddcGeochemicalabnsegmentEntity hddcGeochemicalabnsegment) {
        HddcGeochemicalabnsegmentEntity entity = hddcGeochemicalabnsegmentRepository.findById(hddcGeochemicalabnsegment.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcGeochemicalabnsegment);
        hddcGeochemicalabnsegment.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcGeochemicalabnsegment.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeochemicalabnsegmentRepository.save(hddcGeochemicalabnsegment);
        return hddcGeochemicalabnsegment;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcGeochemicalabnsegments(List<String> ids) {
        List<HddcGeochemicalabnsegmentEntity> hddcGeochemicalabnsegmentList = this.hddcGeochemicalabnsegmentRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcGeochemicalabnsegmentList) && hddcGeochemicalabnsegmentList.size() > 0) {
            for(HddcGeochemicalabnsegmentEntity hddcGeochemicalabnsegment : hddcGeochemicalabnsegmentList) {
                this.hddcGeochemicalabnsegmentRepository.delete(hddcGeochemicalabnsegment);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcGeochemicalabnsegmentQueryParams queryParams, HttpServletResponse response) {
        List<HddcGeochemicalabnsegmentEntity> yhDisasterEntities = hddcGeochemicalabnsegmentNativeRepository.exportYhDisasters(queryParams);
        List<HddcGeochemicalabnsegmentVO> list=new ArrayList<>();
        for (HddcGeochemicalabnsegmentEntity entity:yhDisasterEntities) {
            HddcGeochemicalabnsegmentVO yhDisasterVO=new HddcGeochemicalabnsegmentVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"地球化学异常区段-线","地球化学异常区段-线",HddcGeochemicalabnsegmentVO.class,"地球化学异常区段-线.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcGeochemicalabnsegmentVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcGeochemicalabnsegmentVO.class, params);
            List<HddcGeochemicalabnsegmentVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcGeochemicalabnsegmentVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcGeochemicalabnsegmentVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcGeochemicalabnsegmentVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcGeochemicalabnsegmentEntity yhDisasterEntity = new HddcGeochemicalabnsegmentEntity();
            HddcGeochemicalabnsegmentVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcGeochemicalabnsegment(yhDisasterEntity);
        }
    }
}
