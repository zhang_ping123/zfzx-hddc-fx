package com.css.zfzx.sjcj.modules.hddcB1PaleoEQEvent.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcB1PaleoEQEvent.repository.entity.HddcB1PaleoeqeventEntity;
import com.css.zfzx.sjcj.modules.hddcB1PaleoEQEvent.viewobjects.HddcB1PaleoeqeventQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */

public interface HddcB1PaleoeqeventService {

    public JSONObject queryHddcB1Paleoeqevents(HddcB1PaleoeqeventQueryParams queryParams, int curPage, int pageSize);

    public HddcB1PaleoeqeventEntity getHddcB1Paleoeqevent(String id);

    public HddcB1PaleoeqeventEntity saveHddcB1Paleoeqevent(HddcB1PaleoeqeventEntity hddcB1Paleoeqevent);

    public HddcB1PaleoeqeventEntity updateHddcB1Paleoeqevent(HddcB1PaleoeqeventEntity hddcB1Paleoeqevent);

    public void deleteHddcB1Paleoeqevents(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcB1PaleoeqeventQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
