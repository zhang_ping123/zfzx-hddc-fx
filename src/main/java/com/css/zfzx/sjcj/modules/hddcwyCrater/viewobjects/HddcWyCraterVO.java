package com.css.zfzx.sjcj.modules.hddcwyCrater.viewobjects;


import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-12-02
 */
@Data
public class HddcWyCraterVO implements Serializable {

    /**
     * 项目名称
     */
    @Excel(name="项目名称",orderNum = "8")
    private String projectName;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 塑性熔岩饼单体尺寸
     */
    @Excel(name="塑性熔岩饼单体尺寸",orderNum = "14")
    private Double lavadribletsize;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 火口直径
     */
    @Excel(name="火口直径",orderNum = "16")
    private Double craterdiameter;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 锥体结构组成剖面图原始文件
     */
    @Excel(name="锥体结构组成剖面图原始文件",orderNum = "33")
    private String conestructureprofileArwid;
    /**
     * 锥体形态
     */
    @Excel(name="锥体形态",orderNum = "23")
    private String conemorphology;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 乡
     */
    @Excel(name="详细地址",orderNum = "5")
    private String town;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "3")
    private String city;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 溢出口方向
     */
    @Excel(name="溢出口方向",orderNum = "19")
    private Integer overfalldirection;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 外坡度
     */
    @Excel(name="外坡度",orderNum = "36")
    private Double outsideslopeangle;
    /**
     * 锥体底部直径
     */
    @Excel(name="锥体底部直径",orderNum = "18")
    private Double bottomdiameter;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备注
     */
    @Excel(name="备注",orderNum = "31")
    private String remark;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备注
     */
    private String commentInfo;
    /**
     * 岩石包体粒度
     */
    @Excel(name="岩石包体粒度",orderNum = "30")
    private String rockinclusiongranularity;
    /**
     * 区（县）
     */
    @Excel(name="区（县）",orderNum = "4")
    private String area;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 任务名称
     */
    @Excel(name="任务名称",orderNum = "9")
    private String taskName;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 岩石包体产出状态
     */
    @Excel(name="岩石包体产出状态",orderNum = "28")
    private String rockinclusionoutputstate;
    /**
     * 堆积物类型
     */
    @Excel(name="堆积物类型",orderNum = "37")
    private String deposittype;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 素描图图像
     */
    @Excel(name="素描图图像",orderNum = "24")
    private String sketchAiid;
    /**
     * 锥体名称
     */
    @Excel(name="锥体名称",orderNum = "10")
    private String conename;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 照片集镜向及拍摄者说明文档
     */
    @Excel(name="照片集镜向及拍摄者说明文档",orderNum = "13")
    private String photodescArwid;
    /**
     * 岩石包体形状
     */
    @Excel(name="岩石包体形状",orderNum = "22")
    private String rockinclusionshape;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 锥体结构组成剖面图图片
     */
    @Excel(name="锥体结构组成剖面图图片",orderNum = "35")
    private String conestructureprofileAiid;
    /**
     * 岩石包体数量
     */
    @Excel(name="岩石包体数量",orderNum = "34")
    private Integer rockinclusionnum;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 锥体类型
     */
    @Excel(name="锥体类型",orderNum = "29")
    private Integer conetype;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 照片文件编号
     */
    @Excel(name="照片文件编号",orderNum = "32")
    private String photoAiid;
    /**
     * DepositThickness
     */
    @Excel(name="DepositThickness",orderNum = "26")
    private Double depositthickness;
    /**
     * 照片原始文件编号
     */
    @Excel(name="照片原始文件编号",orderNum = "21")
    private String photoArwid;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 火口深度[米]
     */
    @Excel(name="火口深度[米]",orderNum = "20")
    private Double craterdepth;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 岩石包体类型
     */
    @Excel(name="岩石包体类型",orderNum = "25")
    private String rockinclusiontype;
    /**
     * 火口垣直径
     */
    @Excel(name="火口垣直径",orderNum = "11")
    private Double craterwallsdiameter;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 拍摄者
     */
    @Excel(name="拍摄者",orderNum = "38")
    private String photographer;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 素描图原始文件
     */
    @Excel(name="素描图原始文件",orderNum = "27")
    private String sketchArwid;
    /**
     * 堆积物粒度
     */
    @Excel(name="堆积物粒度",orderNum = "15")
    private String depositgranularity;
    /**
     * 火山口编号
     */
    @Excel(name="火山口编号",orderNum = "1")
    private String id;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 省
     */
    @Excel(name="省",orderNum = "2")
    private String province;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 内坡度
     */
    @Excel(name="内坡度",orderNum = "12")
    private Double insideslopeangle;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 锥体高度[米]
     */
    @Excel(name="锥体高度[米]",orderNum = "17")
    private Double coneheight;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 经度
     */
    @Excel(name="经度",orderNum = "6")
    private Double lon;
    /**
     * 维度
     */
    @Excel(name="纬度",orderNum = "7")
    private Double lat;


    private String provinceName;
    private String cityName;
    private String areaName;

    private String rowNum;
    private String errorMsg;
}