package com.css.zfzx.sjcj.modules.hddcStationhddcStation.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Data
public class HddcStationQueryParams {


    private String province;
    private String cityCode;
    private String area;
    private String projectName;
    private String stationname;

}
