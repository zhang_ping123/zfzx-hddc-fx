package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.repository.entity.HddcWyGeomorphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyPoint.viewobjects.HddcWyGeomorphysvypointQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;
import java.util.List;

/**
 * @author lihelei
 * @date 2020-12-01
 */
public interface HddcWyGeomorphysvypointNativeRepository {

    Page<HddcWyGeomorphysvypointEntity> queryHddcWyGeomorphysvypoints(HddcWyGeomorphysvypointQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyGeomorphysvypoint(HddcAppZztCountVo queryParams);

    List<HddcWyGeomorphysvypointEntity> exportGeomorPoint(HddcAppZztCountVo queryParams);
}
