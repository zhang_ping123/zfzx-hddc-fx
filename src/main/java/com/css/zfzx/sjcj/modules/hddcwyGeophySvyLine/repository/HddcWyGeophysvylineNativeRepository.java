package com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.repository.entity.HddcWyGeophysvylineEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeophySvyLine.viewobjects.HddcWyGeophysvylineQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */
public interface HddcWyGeophysvylineNativeRepository {

    Page<HddcWyGeophysvylineEntity> queryHddcWyGeophysvylines(HddcWyGeophysvylineQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyGeophysvyline(HddcAppZztCountVo queryParams);

    List<HddcWyGeophysvylineEntity> exportPhyLinr(HddcAppZztCountVo queryParams);
}
