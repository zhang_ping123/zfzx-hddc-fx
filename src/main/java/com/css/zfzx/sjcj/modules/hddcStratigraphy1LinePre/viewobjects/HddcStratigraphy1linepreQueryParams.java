package com.css.zfzx.sjcj.modules.hddcStratigraphy1LinePre.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Data
public class HddcStratigraphy1linepreQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;

}
