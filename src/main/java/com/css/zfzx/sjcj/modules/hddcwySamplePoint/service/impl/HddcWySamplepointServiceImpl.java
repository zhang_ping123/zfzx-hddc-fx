package com.css.zfzx.sjcj.modules.hddcwySamplePoint.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcSamplePoint.repository.HddcSamplepointRepository;
import com.css.zfzx.sjcj.modules.hddcSamplePoint.repository.entity.HddcSamplepointEntity;
import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwySamplePoint.repository.HddcWySamplepointNativeRepository;
import com.css.zfzx.sjcj.modules.hddcwySamplePoint.repository.HddcWySamplepointRepository;
import com.css.zfzx.sjcj.modules.hddcwySamplePoint.repository.entity.HddcWySamplepointEntity;
import com.css.zfzx.sjcj.modules.hddcwySamplePoint.service.HddcWySamplepointService;
import com.css.zfzx.sjcj.modules.hddcwySamplePoint.viewobjects.HddcWySamplepointQueryParams;
import com.css.zfzx.sjcj.modules.hddcwySamplePoint.viewobjects.HddcWySamplepointVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zyb
 * @date 2020-12-01
 */
@Service
public class HddcWySamplepointServiceImpl implements HddcWySamplepointService {

	@Autowired
    private HddcWySamplepointRepository hddcWySamplepointRepository;
    @Autowired
    private HddcWySamplepointNativeRepository hddcWySamplepointNativeRepository;
    @Autowired
    private HddcSamplepointRepository hddcSamplepointRepository;
    @Override
    public JSONObject queryHddcWySamplepoints(HddcWySamplepointQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcWySamplepointEntity> hddcWySamplepointPage = this.hddcWySamplepointNativeRepository.queryHddcWySamplepoints(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcWySamplepointPage);
        return jsonObject;
    }


    @Override
    public HddcWySamplepointEntity getHddcWySamplepoint(String uuid) {
        HddcWySamplepointEntity hddcWySamplepoint = this.hddcWySamplepointRepository.findById(uuid).orElse(null);
         return hddcWySamplepoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWySamplepointEntity saveHddcWySamplepoint(HddcWySamplepointEntity hddcWySamplepoint) {
        String uuid = UUIDGenerator.getUUID();
        hddcWySamplepoint.setUuid(uuid);
        if(StringUtils.isEmpty(hddcWySamplepoint.getCreateUser())){
            hddcWySamplepoint.setCreateUser(PlatformSessionUtils.getUserId());
        }
        hddcWySamplepoint.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        hddcWySamplepoint.setIsValid("1");

        HddcSamplepointEntity hddcSamplepointEntity=new HddcSamplepointEntity();
        BeanUtils.copyProperties(hddcWySamplepoint,hddcSamplepointEntity);
        hddcSamplepointEntity.setExtends4(hddcWySamplepoint.getTown());
        hddcSamplepointEntity.setExtends5(String.valueOf(hddcWySamplepoint.getLon()));
        hddcSamplepointEntity.setExtends6(String.valueOf(hddcWySamplepoint.getLat()));
        this.hddcSamplepointRepository.save(hddcSamplepointEntity);

        this.hddcWySamplepointRepository.save(hddcWySamplepoint);
        return hddcWySamplepoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcWySamplepointEntity updateHddcWySamplepoint(HddcWySamplepointEntity hddcWySamplepoint) {
        HddcWySamplepointEntity entity = hddcWySamplepointRepository.findById(hddcWySamplepoint.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcWySamplepoint);
        hddcWySamplepoint.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcWySamplepoint.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcWySamplepointRepository.save(hddcWySamplepoint);
        return hddcWySamplepoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcWySamplepoints(List<String> ids) {
        List<HddcWySamplepointEntity> hddcWySamplepointList = this.hddcWySamplepointRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcWySamplepointList) && hddcWySamplepointList.size() > 0) {
            for(HddcWySamplepointEntity hddcWySamplepoint : hddcWySamplepointList) {
                this.hddcWySamplepointRepository.delete(hddcWySamplepoint);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public BigInteger queryHddcWySamplepoint(HddcAppZztCountVo queryParams) {
        BigInteger bigInteger = hddcWySamplepointNativeRepository.queryHddcWySamplepoint(queryParams);
        return bigInteger;
    }

    @Override
    public List<HddcWySamplepointEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid) {
        List<HddcWySamplepointEntity> list = hddcWySamplepointRepository.findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(userId, taskId, projectId, isValid);
        return list;
    }

    @Override
    public void exportFile(HddcAppZztCountVo queryParams, HttpServletResponse response) {
        List<HddcWySamplepointEntity> hddcWySamplepointEntity = hddcWySamplepointNativeRepository.exportSamplePoint(queryParams);
        List<HddcWySamplepointVO> list=new ArrayList<>();
        for (HddcWySamplepointEntity entity:hddcWySamplepointEntity) {
            HddcWySamplepointVO hddcWySamplepointVO=new HddcWySamplepointVO();
            BeanUtils.copyProperties(entity,hddcWySamplepointVO);
            list.add(hddcWySamplepointVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"采样点-点","采样点-点", HddcWySamplepointVO.class,"采样点-点.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcWySamplepointVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcWySamplepointVO.class, params);
            List<HddcWySamplepointVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcWySamplepointVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcWySamplepointVO hddcWySamplepointVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + hddcWySamplepointVO.getRowNum() + "行" + hddcWySamplepointVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }

    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcWySamplepointVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcWySamplepointEntity hddcWySamplepointEntity = new HddcWySamplepointEntity();
            HddcWySamplepointVO hddcWySamplepointVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(hddcWySamplepointVO, hddcWySamplepointEntity);
            saveHddcWySamplepoint(hddcWySamplepointEntity);
        }
    }

    @Override
    public List<HddcWySamplepointEntity> findAllByCreateUserAndIsValid(String userId, String isValid) {
        List<HddcWySamplepointEntity> list = hddcWySamplepointRepository.findAllByCreateUserAndIsValid(userId,isValid);
        return list;
    }

    /**
     * 逻辑删除-根据项目id删除数据
     * @param projectIds
     */
    @Override
    public void deleteByProjectId(List<String> projectIds) {
        List<HddcWySamplepointEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWySamplepointRepository.queryHddcA1InvrgnhasmaterialtablesByProjectId(projectIds);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWySamplepointEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWySamplepointRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }
    }

    /**
     * 逻辑删除-根据任务id删除数据
     * @param taskId
     */
    @Override
    public void deleteByTaskId(List<String> taskId) {
        List<HddcWySamplepointEntity> hddcA1InvrgnhasmaterialtableList = this.hddcWySamplepointRepository.queryHddcA1InvrgnhasmaterialtablesByTaskId(taskId);
        if(!PlatformObjectUtils.isEmpty(hddcA1InvrgnhasmaterialtableList) && hddcA1InvrgnhasmaterialtableList.size() > 0) {
            for(HddcWySamplepointEntity hddcA1Invrgnhasmaterialtable : hddcA1InvrgnhasmaterialtableList) {
                hddcA1Invrgnhasmaterialtable.setUpdateUser(PlatformSessionUtils.getUserId());
                hddcA1Invrgnhasmaterialtable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
                hddcA1Invrgnhasmaterialtable.setIsValid("0");
                this.hddcWySamplepointRepository.save(hddcA1Invrgnhasmaterialtable);
            }
        }

    }
}
