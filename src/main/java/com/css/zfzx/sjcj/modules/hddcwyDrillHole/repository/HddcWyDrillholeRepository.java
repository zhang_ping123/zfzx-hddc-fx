package com.css.zfzx.sjcj.modules.hddcwyDrillHole.repository;


import com.css.zfzx.sjcj.modules.hddcwyDrillHole.repository.entity.HddcWyDrillholeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */
public interface HddcWyDrillholeRepository extends JpaRepository<HddcWyDrillholeEntity, String> {

    List<HddcWyDrillholeEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId,String projectId, String isValid);

    List<HddcWyDrillholeEntity> findAllByCreateUserAndIsValid(String userId,String isValid);

    @Query(nativeQuery = true, value = "select * from hddc_wy_drillhole where is_valid!=0 project_name in :projectIds")
    List<HddcWyDrillholeEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_drillhole where task_name in :projectIds")
    List<HddcWyDrillholeEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);

}
