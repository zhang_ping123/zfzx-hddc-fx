package com.css.zfzx.sjcj.modules.hddcGeoGeomorphySvyPoint.repository.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author lihelei
 * @date 2020-11-26
 */
@Data
@Entity
@Table(name="hddc_geogeomorphysvypoint")
public class HddcGeogeomorphysvypointEntity implements Serializable {

    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 照片镜向
     */
    @Column(name="photoviewingto")
    private Integer photoviewingto;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 地貌点名称
     */
    @Column(name="geomorphyname")
    private String geomorphyname;
    /**
     * 地震地表破裂类型
     */
    @Column(name="fracturetype")
    private Integer fracturetype;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 地质调查观测点编号
     */
    @Column(name="geologicalsvypointid")
    private String geologicalsvypointid;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 性质
     */
    @Column(name="feature")
    private String feature;
    /**
     * 形成时代
     */
    @Column(name="create_date")
    private Integer createdate;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 平面图文件编号
     */
    @Column(name="sketch_aiid")
    private String sketchAiid;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 平面图原始文件编号
     */
    @Column(name="sketch_arwid")
    private String sketchArwid;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 是否为已知地震的地表破裂
     */
    @Column(name="issurfacerupturebelt")
    private Integer issurfacerupturebelt;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 描述
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 编号
     */
    @Column(name="id")
    private String id;
    /**
     * 编号
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 是否修改工作底图
     */
    @Column(name="ismodifyworkmap")
    private Integer ismodifyworkmap;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 典型照片文件编号
     */
    @Column(name="photo_aiid")
    private String photoAiid;
    /**
     * 垂直位移 [米]
     */
    @Column(name="verticaldisplacement")
    private Double verticaldisplacement;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 地貌点类型
     */
    @Column(name="geomorphytype")
    private String geomorphytype;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 是否在图中显示
     */
    @Column(name="isinmap")
    private Integer isinmap;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 地貌代码
     */
    @Column(name="geomorphycode")
    private String geomorphycode;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 走向水平位移 [米]
     */
    @Column(name="horizenoffset")
    private Double horizenoffset;
    /**
     * 地表破裂（断塞塘等）长 [米]
     */
    @Column(name="length")
    private Double length;
    /**
     * 典型照片原始文件编号
     */
    @Column(name="photo_arwid")
    private String photoArwid;
    /**
     * 地貌线走向（制图用）
     */
    @Column(name="geomorphylndirection")
    private Integer geomorphylndirection;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 典型剖面图原始文件编号
     */
    @Column(name="typicalprofile_arwid")
    private String typicalprofileArwid;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 地表破裂（断塞塘等）高/深 [米]
     */
    @Column(name="height")
    private Double height;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 拍摄者
     */
    @Column(name="photographer")
    private String photographer;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 水平//张缩位移 [米]
     */
    @Column(name="tensiondisplacement")
    private Double tensiondisplacement;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 符号或标注旋转角度
     */
    @Column(name="lastangle")
    private Double lastangle;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 项目ID
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 观测点野外编号
     */
    @Column(name="fieldid")
    private String fieldid;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 典型剖面图图表文件编号
     */
    @Column(name="typicalprofile_aiid")
    private String typicalprofileAiid;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 地表破裂（断塞塘等）宽 [米]
     */
    @Column(name="width")
    private Double width;

}

