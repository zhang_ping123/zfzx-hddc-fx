package com.css.zfzx.sjcj.modules.hddcActiveFault.repository;

import com.css.zfzx.sjcj.modules.hddcActiveFault.repository.entity.HddcActivefaultEntity;
import com.css.zfzx.sjcj.modules.hddcActiveFault.viewobjects.HddcActivefaultQueryParams;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcActivefaultRepository extends JpaRepository<HddcActivefaultEntity, String> {
}
