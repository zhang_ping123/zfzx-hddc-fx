package com.css.zfzx.sjcj.modules.hddcB5_GeophySvyProjectTable.repository;

import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyProjectTable.repository.entity.HddcB5GeophysvyprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB5_GeophySvyProjectTable.viewobjects.HddcB5GeophysvyprojecttableQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
public interface HddcB5GeophysvyprojecttableNativeRepository {

    Page<HddcB5GeophysvyprojecttableEntity> queryHddcB5Geophysvyprojecttables(HddcB5GeophysvyprojecttableQueryParams queryParams, int curPage, int pageSize);

    List<HddcB5GeophysvyprojecttableEntity> exportYhDisasters(HddcB5GeophysvyprojecttableQueryParams queryParams);
}
