package com.css.zfzx.sjcj.modules.hddcVerticalDeformation.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-26
 */
@Data
public class HddcVerticaldeformationVO implements Serializable {

    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 村
     */
    private String village;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "4")
    private String commentinfo;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 编号
     */
    private String id;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 数值
     */
    @Excel(name = "数值", orderNum = "5")
    private Double displace;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 显示码
     */
    @Excel(name = "显示码", orderNum = "6")
    private Integer showcode;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 乡
     */
    private String town;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 备选字段6
     */
    private String extends6;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String rowNum;
    private String errorMsg;
}