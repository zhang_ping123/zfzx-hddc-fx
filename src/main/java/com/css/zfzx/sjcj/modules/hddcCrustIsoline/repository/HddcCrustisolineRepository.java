package com.css.zfzx.sjcj.modules.hddcCrustIsoline.repository;

import com.css.zfzx.sjcj.modules.hddcCrustIsoline.repository.entity.HddcCrustisolineEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-26
 */
public interface HddcCrustisolineRepository extends JpaRepository<HddcCrustisolineEntity, String> {
}
