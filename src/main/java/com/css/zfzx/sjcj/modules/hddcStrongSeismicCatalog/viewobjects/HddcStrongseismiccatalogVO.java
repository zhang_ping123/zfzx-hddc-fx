package com.css.zfzx.sjcj.modules.hddcStrongSeismicCatalog.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Data
public class HddcStrongseismiccatalogVO implements Serializable {

    /**
     * 地名
     */
    @Excel(name = "地名", orderNum = "4")
    private String locationname;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 宏观震中烈度
     */
    @Excel(name = "宏观震中烈度", orderNum = "5")
    private String epicenter;
    /**
     * 震级 [M]
     */
    @Excel(name = "震级 [M]", orderNum = "6")
    private Double magnitude;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 经度
     */
    @Excel(name = "经度", orderNum = "7")
    private Double lon;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 纬度
     */
    @Excel(name = "纬度", orderNum = "8")
    private Double lat;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "9")
    private String commentInfo;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 发震时间
     */
    @Excel(name = "发震时间", orderNum = "10")
    private String occurrencetime;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 等震线图图表编号
     */
    @Excel(name = "等震线图图表编号", orderNum = "11")
    private String ilgraphAiid;
    /**
     * 备注
     */
    private String remark;
    /**
     * 强震事件编号
     */
    private String id;
    /**
     * 等震线图原始图表编号
     */
    @Excel(name = "等震线图原始图表编号", orderNum = "12")
    private String ilgraphArwid;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 日期
     */
    @Excel(name = "日期", orderNum = "13")
    private String occurrencedate;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 精度
     */
    @Excel(name = "精度", orderNum = "14")
    private Integer accuracy;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 符号或标注旋转角度
     */
    @Excel(name = "符号或标注旋转角度", orderNum = "15")
    private Double lastangle;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 乡
     */
    private String town;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 震源深度 [公里]
     */
    @Excel(name = "震源深度 [公里]", orderNum = "16")
    private Double depth;
    /**
     * 备选字段16
     */
    private String extends16;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String rowNum;
    private String errorMsg;
}