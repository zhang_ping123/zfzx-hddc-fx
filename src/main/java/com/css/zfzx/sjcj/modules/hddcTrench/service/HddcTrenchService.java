package com.css.zfzx.sjcj.modules.hddcTrench.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcTrench.repository.entity.HddcTrenchEntity;
import com.css.zfzx.sjcj.modules.hddcTrench.viewobjects.HddcTrenchQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-27
 */

public interface HddcTrenchService {

    public JSONObject queryHddcTrenchs(HddcTrenchQueryParams queryParams, int curPage, int pageSize);

    public HddcTrenchEntity getHddcTrench(String id);

    public HddcTrenchEntity saveHddcTrench(HddcTrenchEntity hddcTrench);

    public HddcTrenchEntity updateHddcTrench(HddcTrenchEntity hddcTrench);

    public void deleteHddcTrenchs(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcTrenchQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
