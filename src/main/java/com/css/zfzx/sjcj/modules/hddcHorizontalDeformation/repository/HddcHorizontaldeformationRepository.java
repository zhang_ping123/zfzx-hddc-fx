package com.css.zfzx.sjcj.modules.hddcHorizontalDeformation.repository;

import com.css.zfzx.sjcj.modules.hddcHorizontalDeformation.repository.entity.HddcHorizontaldeformationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-26
 */
public interface HddcHorizontaldeformationRepository extends JpaRepository<HddcHorizontaldeformationEntity, String> {
}
