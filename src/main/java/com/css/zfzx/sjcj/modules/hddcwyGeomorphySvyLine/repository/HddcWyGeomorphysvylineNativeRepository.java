package com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyLine.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyLine.repository.entity.HddcWyGeomorphysvylineEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorphySvyLine.viewobjects.HddcWyGeomorphysvylineQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;
import java.util.List;

/**
 * @author lihelei
 * @date 2020-12-01
 */
public interface HddcWyGeomorphysvylineNativeRepository {

    Page<HddcWyGeomorphysvylineEntity> queryHddcWyGeomorphysvylines(HddcWyGeomorphysvylineQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyGeomorphysvyline(HddcAppZztCountVo queryParams);

    List<HddcWyGeomorphysvylineEntity> exportLine(HddcAppZztCountVo queryParams);
}
