package com.css.zfzx.sjcj.modules.hddcGeomorphyline.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-12-09
 */
@Data
public class HddcGeomorphylineVO implements Serializable {

    /**
     * 拍摄者
     */
    @Excel(name = "拍摄者",orderNum = "20")
    private String photographer;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 项目id
     */
    private String projectId;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 地震地表破裂类型
     */
    @Excel(name = "地震地表破裂类型",orderNum = "16")
    private Integer fracturetype;
    /**
     * 地表破裂（断塞塘等）长 [米]
     */
    @Excel(name = "地表破裂（断塞塘等）长 [米]",orderNum = "7")
    private Double length;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 地貌类型
     */
    @Excel(name = "地貌类型",orderNum = "5")
    private String geomorphytype;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 照片镜向
     */
    @Excel(name = "照片镜向",orderNum = "19")
    private Integer photoviewingto;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 地貌符号基础位
     */
    @Excel(name = "地貌符号基础位",orderNum = "21")
    private String nsb1;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 地貌符号下标位
     */
    @Excel(name = "地貌符号下标位",orderNum = "22")
    private String nsb2;
    /**
     * 地貌名称
     */
    @Excel(name = "地貌名称", orderNum = "4")
    private String geomorphyname;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 照片文件编号
     */
    @Excel(name = "照片文件编号",orderNum = "17")
    private String photoAiid;
    /**
     * 地表破裂（断塞塘等）宽 [米]
     */
    @Excel(name = "地表破裂（断塞塘等）宽 [米]",orderNum = "8")
    private Double width;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 地貌代码
     */
    private String geomorphycode;
    /**
     * 备注
     */
    private String commentInfo;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 地貌符号上标位
     */
    @Excel(name = "地貌符号上标位",orderNum = "23")
    private String nsb3;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 村
     */
    private String village;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 地貌线编号
     */
    private String id;
    /**
     * 任务id
     */
    private String taskId;
    /**
     * 照片原始文件编号
     */
    @Excel(name = "照片原始文件编号",orderNum = "18")
    private String photoArwid;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 形成时代
     */
    @Excel(name = "形成时代",orderNum = "14")
    private Integer createdate;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 最大垂直位移 [米]
     */
    @Excel(name = "最大垂直位移 [米]",orderNum = "10")
    private Double maxverticaldisplacement;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 地表破裂（断塞塘等）高/深 [米]
     */
    @Excel(name = "地表破裂（断塞塘等）高/深 [米]",orderNum = "9")
    private Double height;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 乡
     */
    private String town;
    /**
     * 最大水平/张缩位移 [米]
     */
    @Excel(name = "最大水平/张缩位移 [米]",orderNum = "12")
    private Double maxtdisplacement;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 性质
     */
    @Excel(name = "性质",orderNum = "13")
    private String feature;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 地貌描述
     */
    @Excel(name = "地貌描述",orderNum = "6")
    private String geomorphydescription;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 最大水平位移 [米]
     */
    @Excel(name = "最大水平位移 [米]",orderNum = "11")
    private Double maxhorizenoffset;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 是否为已知地震的地表破裂
     */
    @Excel(name = "是否为已知地震的地表破裂",orderNum = "15")
    private Integer issurfacerupturebelt;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段30
     */
    private String extends30;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String projectNameName;
    private String taskNameName;
    private Integer photoviewingtoName;
    private Integer issurfacerupturebeltName;
    private Integer createdateName;

    private String rowNum;
    private String errorMsg;

}