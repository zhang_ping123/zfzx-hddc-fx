package com.css.zfzx.sjcj.modules.hddcA1InvRgnHasMaterialTable.repository;

import com.css.zfzx.sjcj.modules.hddcA1InvRgnHasMaterialTable.repository.entity.HddcA1InvrgnhasmaterialtableEntity;
import com.css.zfzx.sjcj.modules.hddcA1InvRgnHasMaterialTable.viewobjects.HddcA1InvrgnhasmaterialtableQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcA1InvrgnhasmaterialtableNativeRepository {

    List<HddcA1InvrgnhasmaterialtableEntity> exportYhDisasters(HddcA1InvrgnhasmaterialtableQueryParams queryParams);

    Page<HddcA1InvrgnhasmaterialtableEntity> queryHddcA1Invrgnhasmaterialtables(HddcA1InvrgnhasmaterialtableQueryParams queryParams, int curPage, int pageSize);
}
