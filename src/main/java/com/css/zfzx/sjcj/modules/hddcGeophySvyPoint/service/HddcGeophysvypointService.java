package com.css.zfzx.sjcj.modules.hddcGeophySvyPoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcGeophySvyPoint.repository.entity.HddcGeophysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcGeophySvyPoint.viewobjects.HddcGeophysvypointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */

public interface HddcGeophysvypointService {

    public JSONObject queryHddcGeophysvypoints(HddcGeophysvypointQueryParams queryParams, int curPage, int pageSize);

    public HddcGeophysvypointEntity getHddcGeophysvypoint(String id);

    public HddcGeophysvypointEntity saveHddcGeophysvypoint(HddcGeophysvypointEntity hddcGeophysvypoint);

    public HddcGeophysvypointEntity updateHddcGeophysvypoint(HddcGeophysvypointEntity hddcGeophysvypoint);

    public void deleteHddcGeophysvypoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGeophysvypointQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
