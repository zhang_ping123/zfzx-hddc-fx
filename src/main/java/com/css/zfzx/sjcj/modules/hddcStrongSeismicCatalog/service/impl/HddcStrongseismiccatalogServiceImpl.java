package com.css.zfzx.sjcj.modules.hddcStrongSeismicCatalog.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltVO;
import com.css.zfzx.sjcj.modules.hddcStrongSeismicCatalog.repository.HddcStrongseismiccatalogNativeRepository;
import com.css.zfzx.sjcj.modules.hddcStrongSeismicCatalog.repository.HddcStrongseismiccatalogRepository;
import com.css.zfzx.sjcj.modules.hddcStrongSeismicCatalog.repository.entity.HddcStrongseismiccatalogEntity;
import com.css.zfzx.sjcj.modules.hddcStrongSeismicCatalog.service.HddcStrongseismiccatalogService;
import com.css.zfzx.sjcj.modules.hddcStrongSeismicCatalog.viewobjects.HddcStrongseismiccatalogQueryParams;
import com.css.zfzx.sjcj.modules.hddcStrongSeismicCatalog.viewobjects.HddcStrongseismiccatalogVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Service
public class HddcStrongseismiccatalogServiceImpl implements HddcStrongseismiccatalogService {

	@Autowired
    private HddcStrongseismiccatalogRepository hddcStrongseismiccatalogRepository;
    @Autowired
    private HddcStrongseismiccatalogNativeRepository hddcStrongseismiccatalogNativeRepository;

    @Override
    public JSONObject queryHddcStrongseismiccatalogs(HddcStrongseismiccatalogQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcStrongseismiccatalogEntity> hddcStrongseismiccatalogPage = this.hddcStrongseismiccatalogNativeRepository.queryHddcStrongseismiccatalogs(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcStrongseismiccatalogPage);
        return jsonObject;
    }


    @Override
    public HddcStrongseismiccatalogEntity getHddcStrongseismiccatalog(String id) {
        HddcStrongseismiccatalogEntity hddcStrongseismiccatalog = this.hddcStrongseismiccatalogRepository.findById(id).orElse(null);
         return hddcStrongseismiccatalog;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcStrongseismiccatalogEntity saveHddcStrongseismiccatalog(HddcStrongseismiccatalogEntity hddcStrongseismiccatalog) {
        String uuid = UUIDGenerator.getUUID();
        hddcStrongseismiccatalog.setUuid(uuid);
        hddcStrongseismiccatalog.setCreateUser(PlatformSessionUtils.getUserId());
        hddcStrongseismiccatalog.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcStrongseismiccatalogRepository.save(hddcStrongseismiccatalog);
        return hddcStrongseismiccatalog;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcStrongseismiccatalogEntity updateHddcStrongseismiccatalog(HddcStrongseismiccatalogEntity hddcStrongseismiccatalog) {
        HddcStrongseismiccatalogEntity entity = hddcStrongseismiccatalogRepository.findById(hddcStrongseismiccatalog.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcStrongseismiccatalog);
        hddcStrongseismiccatalog.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcStrongseismiccatalog.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcStrongseismiccatalogRepository.save(hddcStrongseismiccatalog);
        return hddcStrongseismiccatalog;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcStrongseismiccatalogs(List<String> ids) {
        List<HddcStrongseismiccatalogEntity> hddcStrongseismiccatalogList = this.hddcStrongseismiccatalogRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcStrongseismiccatalogList) && hddcStrongseismiccatalogList.size() > 0) {
            for(HddcStrongseismiccatalogEntity hddcStrongseismiccatalog : hddcStrongseismiccatalogList) {
                this.hddcStrongseismiccatalogRepository.delete(hddcStrongseismiccatalog);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcStrongseismiccatalogQueryParams queryParams, HttpServletResponse response) {
        List<HddcStrongseismiccatalogEntity> yhDisasterEntities = hddcStrongseismiccatalogNativeRepository.exportYhDisasters(queryParams);
        List<HddcStrongseismiccatalogVO> list=new ArrayList<>();
        for (HddcStrongseismiccatalogEntity entity:yhDisasterEntities) {
            HddcStrongseismiccatalogVO yhDisasterVO=new HddcStrongseismiccatalogVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"1970年前4 3_4以上强震目录-点","1970年前4 3_4以上强震目录-点",HddcStrongseismiccatalogVO.class,"1970年前4 3_4以上强震目录-点.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcStrongseismiccatalogVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcStrongseismiccatalogVO.class, params);
            List<HddcStrongseismiccatalogVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcStrongseismiccatalogVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcStrongseismiccatalogVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcStrongseismiccatalogVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcStrongseismiccatalogEntity yhDisasterEntity = new HddcStrongseismiccatalogEntity();
            HddcStrongseismiccatalogVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcStrongseismiccatalog(yhDisasterEntity);
        }
    }

}
