package com.css.zfzx.sjcj.modules.hddcSamplePoint.repository;

import com.css.zfzx.sjcj.modules.hddcSamplePoint.repository.entity.HddcSamplepointEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
public interface HddcSamplepointRepository extends JpaRepository<HddcSamplepointEntity, String> {
}
