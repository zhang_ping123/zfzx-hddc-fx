package com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository;

import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.repository.entity.HddcB1GeomorlnonfractbltEntity;
import com.css.zfzx.sjcj.modules.hddcB1GeomorLnOnFractBlt.viewobjects.HddcB1GeomorlnonfractbltQueryParams;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcB1GeomorlnonfractbltRepository extends JpaRepository<HddcB1GeomorlnonfractbltEntity, String> {

}
