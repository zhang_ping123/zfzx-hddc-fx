package com.css.zfzx.sjcj.modules.hddcA6StationHasWaveforms.repository;

import com.css.zfzx.sjcj.modules.hddcA6StationHasWaveforms.repository.entity.HddcA6StationhaswaveformsEntity;
import com.css.zfzx.sjcj.modules.hddcA6StationHasWaveforms.viewobjects.HddcA6StationhaswaveformsQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */
public interface HddcA6StationhaswaveformsNativeRepository {

    Page<HddcA6StationhaswaveformsEntity> queryHddcA6Stationhaswaveformss(HddcA6StationhaswaveformsQueryParams queryParams, int curPage, int pageSize);

    List<HddcA6StationhaswaveformsEntity> exportYhDisasters(HddcA6StationhaswaveformsQueryParams queryParams);
}
