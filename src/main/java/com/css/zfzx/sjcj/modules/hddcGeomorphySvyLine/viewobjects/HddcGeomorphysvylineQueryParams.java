package com.css.zfzx.sjcj.modules.hddcGeomorphySvyLine.viewobjects;

import lombok.Data;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Data
public class HddcGeomorphysvylineQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String name;

}
