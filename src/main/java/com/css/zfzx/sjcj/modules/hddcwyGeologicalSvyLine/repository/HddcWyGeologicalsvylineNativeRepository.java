package com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyLine.repository;

import com.css.zfzx.sjcj.modules.analysis.vo.HddcAppZztCountVo;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyLine.repository.entity.HddcWyGeologicalsvylineEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyLine.viewobjects.HddcWyGeologicalsvylineQueryParams;
import org.springframework.data.domain.Page;

import java.math.BigInteger;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-01
 */
public interface HddcWyGeologicalsvylineNativeRepository {

    Page<HddcWyGeologicalsvylineEntity> queryHddcWyGeologicalsvylines(HddcWyGeologicalsvylineQueryParams queryParams, int curPage, int pageSize);

    BigInteger queryHddcWyGeologicalsvyline(HddcAppZztCountVo queryParams);

    List<HddcWyGeologicalsvylineEntity> exportLine(HddcAppZztCountVo queryParams);

}
