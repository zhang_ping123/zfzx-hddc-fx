package com.css.zfzx.sjcj.modules.hddccjtask.repository.entity;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangping
 * @date 2020-11-26
 */
@Data
@Entity
@Table(name="hddc_cj_task")
public class HddcCjTaskEntity implements Serializable {

    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 人员信息
     */
    @Column(name="person_ids")
    private String personIds;
    /**
     * 任务ID
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 地图范围
     */
    @Column(name="map_infos")
    private String mapInfos;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 所属项目ID
     */
    @Column(name="belong_project_id")
    private String belongProjectID;
    /**
     * 所属项目
     */
    @Column(name="belong_project")
    private String belongProject;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 编号
     */
    @Id
    @Column(name="ID")
    private String id;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;

}

