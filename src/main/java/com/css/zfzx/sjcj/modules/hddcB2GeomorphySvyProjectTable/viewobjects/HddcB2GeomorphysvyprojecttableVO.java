package com.css.zfzx.sjcj.modules.hddcB2GeomorphySvyProjectTable.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Id;
import java.util.Date;

@Data
public class HddcB2GeomorphysvyprojecttableVO {
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 工程名称
     */
    @Excel(name = "工程名称",orderNum = "4")
    private String name;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 省
     */
    @Excel(name = "省",orderNum = "1")
    private String province;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 数据处理说明文档
     */
    @Excel(name = "数据处理说明文档",orderNum = "17")
    private String dataprocessArwid;
    /**
     * 测量说明文档
     */
    @Excel(name = "测量说明文档",orderNum = "16")
    private String measuredescArwid;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 照片原始文件编号
     */
    @Excel(name = "照片原始文件编号",orderNum = "18")
    private String photoArwid;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 目标区编号
     */
    private String targetregionid;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 测量数据坐标系
     */
    @Excel(name ="测量数据坐标系",orderNum = "7")
    private String coordination;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 区（县）
     */
    @Excel(name="区（县）",orderNum = "3")
    private String area;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备注
     */
    private String commentInfo;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 测量工程编码
     */
    @Id
    private String id;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 测量地点
     */
    @Excel(name = "测量地点",orderNum = "6")
    private String location;
    /**
     * 工作区编号
     */
    private String workregionid;
    /**
     * 观测数据文件编号
     */
    @Excel(name="观测数据文件编号",orderNum = "15")
    private String surveydataArwid;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 采集样品总数
     */
    @Excel(name = "采集样品总数",orderNum = "12")
    private Integer collectedsamplecount;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 送样总数
     */
    @Excel(name = "送样总数",orderNum = "13")
    private Integer samplecount;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 照片集镜向及拍摄者说明文档
     */
    @Excel(name = "照片集镜向及拍摄者说明文档",orderNum = "19")
    private String photodescArwid;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 市
     */
    @Excel(name="市",orderNum = "2")
    private String city;
    /**
     * 观测日期
     */
    @Excel(name="观测日期",orderNum = "5")
    private String surveydate;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 坐标系统假北方向
     */
    @Excel(name = "坐标系统假北方向",orderNum = "10")
    private Integer datandirection;
    /**
     * 测量者
     */
    @Excel(name = "测量者",orderNum = "11")
    private String measurer;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 展示照片文件编号
     */
    @Excel(name = "展示照片文件编号",orderNum = "17")
    private String photoAiid;
    /**
     * 测量目的
     */
    @Excel(name="测量目的",orderNum = "8")
    private String purpose;
    /**
     * 乡
     */
    private String town;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 地质填图区编号
     */
    private String mainafsregionid;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 获得测年结果样品数
     */
    @Excel(name = "获得测年结果样品数",orderNum = "14")
    private Integer datingsamplecount;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 测量仪器
     */
    @Excel(name = "测量仪器",orderNum = "9")
    private String measuringapparatus;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段14
     */
    private String extends14;

    private String provinceName;
    private String cityName;
    private String areaName;

    private String rowNum;
    private String errorMsg;
}
