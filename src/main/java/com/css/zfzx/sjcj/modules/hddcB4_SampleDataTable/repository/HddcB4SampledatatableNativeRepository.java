package com.css.zfzx.sjcj.modules.hddcB4_SampleDataTable.repository;

import com.css.zfzx.sjcj.modules.hddcB4_SampleDataTable.repository.entity.HddcB4SampledatatableEntity;
import com.css.zfzx.sjcj.modules.hddcB4_SampleDataTable.viewobjects.HddcB4SampledatatableQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcB4SampledatatableNativeRepository {

    Page<HddcB4SampledatatableEntity> queryHddcB4Sampledatatables(HddcB4SampledatatableQueryParams queryParams, int curPage, int pageSize);

    List<HddcB4SampledatatableEntity> exportYhDisasters(HddcB4SampledatatableQueryParams queryParams);
}
