package com.css.zfzx.sjcj.modules.hddcA1InvRgnHasMaterialTable.controller;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.bpm.platform.utils.PlatformPageUtils;
import com.css.zfzx.sjcj.modules.hddcA1InvRgnHasMaterialTable.repository.entity.HddcA1InvrgnhasmaterialtableEntity;
import com.css.zfzx.sjcj.modules.hddcA1InvRgnHasMaterialTable.service.HddcA1InvrgnhasmaterialtableService;
import com.css.zfzx.sjcj.modules.hddcA1InvRgnHasMaterialTable.viewobjects.HddcA1InvrgnhasmaterialtableQueryParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
@Slf4j
@RestController
@RequestMapping("/hddc/hddcA1Invrgnhasmaterialtables")
public class HddcA1InvrgnhasmaterialtableController {
    @Autowired
    private HddcA1InvrgnhasmaterialtableService hddcA1InvrgnhasmaterialtableService;

    @GetMapping("/queryHddcA1Invrgnhasmaterialtables")
    public RestResponse queryHddcA1Invrgnhasmaterialtables(HttpServletRequest request, HddcA1InvrgnhasmaterialtableQueryParams queryParams) {
        RestResponse response = null;
        try{
            int curPage = Integer.valueOf(request.getParameter(PlatformPageUtils.CUR_PAGE));
            int pageSize = Integer.valueOf(request.getParameter(PlatformPageUtils.PAGE_SIZE));
            JSONObject jsonObject = hddcA1InvrgnhasmaterialtableService.queryHddcA1Invrgnhasmaterialtables(queryParams,curPage,pageSize);
            response = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("{id}")
    public RestResponse getHddcA1Invrgnhasmaterialtable(@PathVariable String id) {
        RestResponse response = null;
        try{
            HddcA1InvrgnhasmaterialtableEntity hddcA1Invrgnhasmaterialtable = hddcA1InvrgnhasmaterialtableService.getHddcA1Invrgnhasmaterialtable(id);
            response = RestResponse.succeed(hddcA1Invrgnhasmaterialtable);
        }catch (Exception e){
            String errorMessage = "获取失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @PostMapping
    public RestResponse saveHddcA1Invrgnhasmaterialtable(@RequestBody HddcA1InvrgnhasmaterialtableEntity hddcA1Invrgnhasmaterialtable) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcA1InvrgnhasmaterialtableService.saveHddcA1Invrgnhasmaterialtable(hddcA1Invrgnhasmaterialtable);
            json.put("message", "新增成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "新增失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;

    }
    @PutMapping
    public RestResponse updateHddcA1Invrgnhasmaterialtable(@RequestBody HddcA1InvrgnhasmaterialtableEntity hddcA1Invrgnhasmaterialtable)  {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcA1InvrgnhasmaterialtableService.updateHddcA1Invrgnhasmaterialtable(hddcA1Invrgnhasmaterialtable);
            json.put("message", "修改成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "修改失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }
    @DeleteMapping
    public RestResponse deleteHddcA1Invrgnhasmaterialtables(@RequestParam List<String> ids) {
        RestResponse response = null;
        JSONObject json = new JSONObject();
        try{
            hddcA1InvrgnhasmaterialtableService.deleteHddcA1Invrgnhasmaterialtables(ids);
            json.put("message", "删除成功!");
            response = RestResponse.succeed(json);
        }catch (Exception e){
            String errorMessage = "删除失败!";
            log.error(errorMessage,e);
            response = RestResponse.fail(errorMessage);
        }
        return response;
    }

    @GetMapping("/getValidDictItemsByDictCode/{dictCode}")
    public RestResponse getValidDictItemsByDictCode(@PathVariable String dictCode) {
        RestResponse restResponse = null;
        try {
            restResponse = RestResponse.succeed(hddcA1InvrgnhasmaterialtableService.getValidDictItemsByDictCode(dictCode));
        } catch (Exception e) {
            String errorMsg = "字典项获取失败!";
            log.error(errorMsg, e);
            restResponse = RestResponse.fail(errorMsg);
        }
        return restResponse;
    }
    /***
     * 导出
     * @param response
     * @return
     */
    @GetMapping("/exportFile")
    public RestResponse exportFileYhDisasters(HttpServletResponse response,
                                              @RequestParam("projectName")String projectName,
                                              @RequestParam("province") String province, @RequestParam("city")String city, @RequestParam("area")String area) {
        RestResponse responseRest = null;
        JSONObject jsonObject = new JSONObject();
        try{
            HddcA1InvrgnhasmaterialtableQueryParams queryParams=new HddcA1InvrgnhasmaterialtableQueryParams();
            queryParams.setArea(area);
            queryParams.setCity(city);
            queryParams.setProvince(province);
            queryParams.setProjectName(projectName);
            hddcA1InvrgnhasmaterialtableService.exportFile(queryParams,response);
            jsonObject.put("message", "导出成功");
            responseRest = RestResponse.succeed(jsonObject);
        }catch (Exception e){
            String errorMessage = "查询失败!";
            log.error(errorMessage,e);
            responseRest = RestResponse.fail(errorMessage);
        }
        return responseRest;
    }
    /**
     * 导入
     *
     * @param file
     * @param response
     * @return
     */
    @PostMapping("/importDisaster")
    public RestResponse export(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        RestResponse restResponse = null;
        try {
            String s = hddcA1InvrgnhasmaterialtableService.exportExcel( file, response);
            restResponse = RestResponse.succeed(s);
        } catch (Exception e) {
            String errormessage = "导入失败";
            log.error(errormessage, e);
            restResponse = RestResponse.fail(errormessage);
        }
        return restResponse;
    }
}