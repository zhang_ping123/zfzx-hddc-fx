package com.css.zfzx.sjcj.modules.hddcISCatalog.repository;

import com.css.zfzx.sjcj.modules.hddcISCatalog.repository.entity.HddcIscatalogEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-28
 */
public interface HddcIscatalogRepository extends JpaRepository<HddcIscatalogEntity, String> {
}
