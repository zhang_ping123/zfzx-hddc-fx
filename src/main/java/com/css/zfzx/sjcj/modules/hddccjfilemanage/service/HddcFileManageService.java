package com.css.zfzx.sjcj.modules.hddccjfilemanage.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddccjfilemanage.repository.entity.HddcFileManageEntity;
import com.css.zfzx.sjcj.modules.hddccjfilemanage.viewobjects.HddcFileManageQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-10
 */

public interface HddcFileManageService {

    public JSONObject queryHddcFileManages(HddcFileManageQueryParams queryParams, int curPage, int pageSize);

    public HddcFileManageEntity getHddcFileManage(String id);

    public HddcFileManageEntity saveHddcFileManage(HddcFileManageEntity hddcFileManage);

    public HddcFileManageEntity updateHddcFileManage(HddcFileManageEntity hddcFileManage);

    public void deleteHddcFileManages(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    int upload(MultipartFile file,String filenumber);

    void download(String name, HttpServletResponse response);
}
