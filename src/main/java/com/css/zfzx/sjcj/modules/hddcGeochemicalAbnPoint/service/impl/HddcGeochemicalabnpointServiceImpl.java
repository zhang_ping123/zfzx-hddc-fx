package com.css.zfzx.sjcj.modules.hddcGeochemicalAbnPoint.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnPoint.repository.HddcGeochemicalabnpointNativeRepository;
import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnPoint.repository.HddcGeochemicalabnpointRepository;
import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnPoint.repository.entity.HddcGeochemicalabnpointEntity;
import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnPoint.service.HddcGeochemicalabnpointService;
import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnPoint.viewobjects.HddcGeochemicalabnpointQueryParams;
import com.css.zfzx.sjcj.modules.hddcGeochemicalAbnPoint.viewobjects.HddcGeochemicalabnpointVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Service
public class HddcGeochemicalabnpointServiceImpl implements HddcGeochemicalabnpointService {

	@Autowired
    private HddcGeochemicalabnpointRepository hddcGeochemicalabnpointRepository;
    @Autowired
    private HddcGeochemicalabnpointNativeRepository hddcGeochemicalabnpointNativeRepository;

    @Override
    public JSONObject queryHddcGeochemicalabnpoints(HddcGeochemicalabnpointQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcGeochemicalabnpointEntity> hddcGeochemicalabnpointPage = this.hddcGeochemicalabnpointNativeRepository.queryHddcGeochemicalabnpoints(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcGeochemicalabnpointPage);
        return jsonObject;
    }


    @Override
    public HddcGeochemicalabnpointEntity getHddcGeochemicalabnpoint(String id) {
        HddcGeochemicalabnpointEntity hddcGeochemicalabnpoint = this.hddcGeochemicalabnpointRepository.findById(id).orElse(null);
         return hddcGeochemicalabnpoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeochemicalabnpointEntity saveHddcGeochemicalabnpoint(HddcGeochemicalabnpointEntity hddcGeochemicalabnpoint) {
        String uuid = UUIDGenerator.getUUID();
        hddcGeochemicalabnpoint.setUuid(uuid);
        hddcGeochemicalabnpoint.setCreateUser(PlatformSessionUtils.getUserId());
        hddcGeochemicalabnpoint.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeochemicalabnpointRepository.save(hddcGeochemicalabnpoint);
        return hddcGeochemicalabnpoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeochemicalabnpointEntity updateHddcGeochemicalabnpoint(HddcGeochemicalabnpointEntity hddcGeochemicalabnpoint) {
        HddcGeochemicalabnpointEntity entity = hddcGeochemicalabnpointRepository.findById(hddcGeochemicalabnpoint.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcGeochemicalabnpoint);
        hddcGeochemicalabnpoint.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcGeochemicalabnpoint.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeochemicalabnpointRepository.save(hddcGeochemicalabnpoint);
        return hddcGeochemicalabnpoint;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcGeochemicalabnpoints(List<String> ids) {
        List<HddcGeochemicalabnpointEntity> hddcGeochemicalabnpointList = this.hddcGeochemicalabnpointRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcGeochemicalabnpointList) && hddcGeochemicalabnpointList.size() > 0) {
            for(HddcGeochemicalabnpointEntity hddcGeochemicalabnpoint : hddcGeochemicalabnpointList) {
                this.hddcGeochemicalabnpointRepository.delete(hddcGeochemicalabnpoint);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcGeochemicalabnpointQueryParams queryParams, HttpServletResponse response) {
        List<HddcGeochemicalabnpointEntity> yhDisasterEntities = hddcGeochemicalabnpointNativeRepository.exportYhDisasters(queryParams);
        List<HddcGeochemicalabnpointVO> list=new ArrayList<>();
        for (HddcGeochemicalabnpointEntity entity:yhDisasterEntities) {
            HddcGeochemicalabnpointVO yhDisasterVO=new HddcGeochemicalabnpointVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"地球化学探测异常点-点","地球化学探测异常点-点",HddcGeochemicalabnpointVO.class,"地球化学探测异常点-点.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcGeochemicalabnpointVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcGeochemicalabnpointVO.class, params);
            List<HddcGeochemicalabnpointVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcGeochemicalabnpointVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcGeochemicalabnpointVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcGeochemicalabnpointVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcGeochemicalabnpointEntity yhDisasterEntity = new HddcGeochemicalabnpointEntity();
            HddcGeochemicalabnpointVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcGeochemicalabnpoint(yhDisasterEntity);
        }
    }
}
