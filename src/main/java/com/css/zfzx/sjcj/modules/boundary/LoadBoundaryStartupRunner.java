package com.css.zfzx.sjcj.modules.boundary;

import com.css.zfzx.sjcj.common.utils.AreaBoundaryUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
public class LoadBoundaryStartupRunner implements CommandLineRunner {

    //获取配置文件的上传文件路径
    @Value("${file.boundary.pcaPath}")
    private String pcaPath;

    @Value("${file.boundary.pPath}")
    private String pPath;

    @Value("${file.boundary.pcPath}")
    private String pcPath;

    @Override
    public void run(String... args) throws Exception {
        /**********部署服务时行政边界数据从jar包拉取出，放在与jar包同级目录下********start**/
        String rootPath = System.getProperty("user.dir").replace("\\", "/");
        pcaPath = rootPath + pcaPath;
        pPath = rootPath + pPath;
        pcPath = rootPath + pcPath;
        /**********部署服务时, 打开注释, 行政边界数据从jar包拉取出，放在与jar包同级目录下********end**/
        AreaBoundaryUtil.initialize(pcaPath, pPath, pcPath);
    }
}
