package com.css.zfzx.sjcj.modules.hddcGeomorphyline.repository.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-12-09
 */
@Data
@Entity
@Table(name="hddc_geomorphyline")
public class HddcGeomorphylineEntity implements Serializable {

    /**
     * 拍摄者
     */
    @Column(name="photographer")
    private String photographer;
    /**
     * 备选字段10
     */
    @Column(name="extends10")
    private String extends10;
    /**
     * 市
     */
    @Column(name="city")
    private String city;
    /**
     * 备选字段26
     */
    @Column(name="extends26")
    private String extends26;
    /**
     * 备选字段1
     */
    @Column(name="extends1")
    private String extends1;
    /**
     * 备选字段8
     */
    @Column(name="extends8")
    private String extends8;
    /**
     * 备选字段14
     */
    @Column(name="extends14")
    private String extends14;
    /**
     * 编号
     */
    @Column(name="object_code")
    private String objectCode;
    /**
     * 质检状态
     */
    @Column(name="qualityinspection_status")
    private String qualityinspectionStatus;
    /**
     * 项目id
     */
    @Column(name="project_id")
    private String projectId;
    /**
     * 审查时间
     */
    @Column(name="examine_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 地震地表破裂类型
     */
    @Column(name="fracturetype")
    private Integer fracturetype;
    /**
     * 地表破裂（断塞塘等）长 [米]
     */
    @Column(name="length")
    private Double length;
    /**
     * 备选字段29
     */
    @Column(name="extends29")
    private String extends29;
    /**
     * 备注
     */
    @Column(name="remark")
    private String remark;
    /**
     * 备选字段28
     */
    @Column(name="extends28")
    private String extends28;
    /**
     * 备选字段24
     */
    @Column(name="extends24")
    private String extends24;
    /**
     * 地貌类型
     */
    @Column(name="geomorphytype")
    private String geomorphytype;
    /**
     * 修改人
     */
    @Column(name="update_user")
    private String updateUser;
    /**
     * 任务名称
     */
    @Column(name="task_name")
    private String taskName;
    /**
     * 备选字段12
     */
    @Column(name="extends12")
    private String extends12;
    /**
     * 照片镜向
     */
    @Column(name="photoviewingto")
    private Integer photoviewingto;
    /**
     * 备选字段16
     */
    @Column(name="extends16")
    private String extends16;
    /**
     * 地貌符号基础位
     */
    @Column(name="nsb1")
    private String nsb1;
    /**
     * 备选字段9
     */
    @Column(name="extends9")
    private String extends9;
    /**
     * 地貌符号下标位
     */
    @Column(name="nsb2")
    private String nsb2;
    /**
     * 地貌名称
     */
    @Column(name="geomorphyname")
    private String geomorphyname;
    /**
     * 备选字段20
     */
    @Column(name="extends20")
    private String extends20;
    /**
     * 备选字段2
     */
    @Column(name="extends2")
    private String extends2;
    /**
     * 备选字段7
     */
    @Column(name="extends7")
    private String extends7;
    /**
     * 创建人
     */
    @Column(name="create_user")
    private String createUser;
    /**
     * 备选字段15
     */
    @Column(name="extends15")
    private String extends15;
    /**
     * 质检原因
     */
    @Column(name="qualityinspection_comments")
    private String qualityinspectionComments;
    /**
     * 照片文件编号
     */
    @Column(name="photo_aiid")
    private String photoAiid;
    /**
     * 地表破裂（断塞塘等）宽 [米]
     */
    @Column(name="width")
    private Double width;
    /**
     * 质检人
     */
    @Column(name="qualityinspection_user")
    private String qualityinspectionUser;
    /**
     * 地貌代码
     */
    @Column(name="geomorphycode")
    private String geomorphycode;
    /**
     * 备注
     */
    @Column(name="comment_info")
    private String commentInfo;
    /**
     * 备选字段27
     */
    @Column(name="extends27")
    private String extends27;
    /**
     * 地貌符号上标位
     */
    @Column(name="nsb3")
    private String nsb3;
    /**
     * 备选字段18
     */
    @Column(name="extends18")
    private String extends18;
    /**
     * 村
     */
    @Column(name="village")
    private String village;
    /**
     * 创建时间
     */
    @Column(name="create_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 地貌线编号
     */
    @Column(name="id")
    private String id;
    /**
     * 编号
     */
    @Id
    @Column(name="uuid")
    private String uuid;
    /**
     * 任务id
     */
    @Column(name="task_id")
    private String taskId;
    /**
     * 照片原始文件编号
     */
    @Column(name="photo_arwid")
    private String photoArwid;
    /**
     * 修改时间
     */
    @Column(name="update_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段23
     */
    @Column(name="extends23")
    private String extends23;
    /**
     * 备选字段5
     */
    @Column(name="extends5")
    private String extends5;
    /**
     * 审核状态（保存）
     */
    @Column(name="review_status")
    private String reviewStatus;
    /**
     * 形成时代
     */
    @Column(name="createdate")
    private Integer createdate;
    /**
     * 审查意见
     */
    @Column(name="examine_comments")
    private String examineComments;
    /**
     * 最大垂直位移 [米]
     */
    @Column(name="maxverticaldisplacement")
    private Double maxverticaldisplacement;
    /**
     * 审查人
     */
    @Column(name="examine_user")
    private String examineUser;
    /**
     * 备选字段13
     */
    @Column(name="extends13")
    private String extends13;
    /**
     * 备选字段4
     */
    @Column(name="extends4")
    private String extends4;
    /**
     * 地表破裂（断塞塘等）高/深 [米]
     */
    @Column(name="height")
    private Double height;
    /**
     * 备选字段21
     */
    @Column(name="extends21")
    private String extends21;
    /**
     * 区（县）
     */
    @Column(name="area")
    private String area;
    /**
     * 备选字段22
     */
    @Column(name="extends22")
    private String extends22;
    /**
     * 分区标识
     */
    @Column(name="partion_flag")
    private Integer partionFlag;
    /**
     * 乡
     */
    @Column(name="town")
    private String town;
    /**
     * 最大水平/张缩位移 [米]
     */
    @Column(name="maxtdisplacement")
    private Double maxtdisplacement;
    /**
     * 删除标识
     */
    @Column(name="is_valid")
    private String isValid;
    /**
     * 性质
     */
    @Column(name="feature")
    private String feature;
    /**
     * 备选字段25
     */
    @Column(name="extends25")
    private String extends25;
    /**
     * 备选字段3
     */
    @Column(name="extends3")
    private String extends3;
    /**
     * 备选字段6
     */
    @Column(name="extends6")
    private String extends6;
    /**
     * 质检时间
     */
    @Column(name="qualityinspection_date")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 地貌描述
     */
    @Column(name="geomorphydescription")
    private String geomorphydescription;
    /**
     * 项目名称
     */
    @Column(name="project_name")
    private String projectName;
    /**
     * 最大水平位移 [米]
     */
    @Column(name="maxhorizenoffset")
    private Double maxhorizenoffset;
    /**
     * 备选字段11
     */
    @Column(name="extends11")
    private String extends11;
    /**
     * 备选字段19
     */
    @Column(name="extends19")
    private String extends19;
    /**
     * 省
     */
    @Column(name="province")
    private String province;
    /**
     * 是否为已知地震的地表破裂
     */
    @Column(name="issurfacerupturebelt")
    private Integer issurfacerupturebelt;
    /**
     * 备选字段17
     */
    @Column(name="extends17")
    private String extends17;
    /**
     * 备选字段30
     */
    @Column(name="extends30")
    private String extends30;

}

