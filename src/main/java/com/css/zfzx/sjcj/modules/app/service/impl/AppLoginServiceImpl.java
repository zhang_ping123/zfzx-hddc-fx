package com.css.zfzx.sjcj.modules.app.service.impl;

import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.constants.YNEnum;
import com.css.bpm.platform.org.dept.repository.entity.DeptEntity;
import com.css.bpm.platform.org.user.repository.UserRepository;
import com.css.bpm.platform.org.user.repository.entity.DeptUserEntity;
import com.css.bpm.platform.org.user.repository.entity.UserEntity;
import com.css.bpm.platform.page.login.viewobjects.PwdCheck;
import com.css.bpm.platform.utils.PlatformDateUtils;
import com.css.bpm.platform.utils.PlatformSessionUtils;
import com.css.bpm.platform.utils.UUIDGenerator;
import com.css.zfzx.sjcj.modules.app.repository.BaseDeptRepository;
import com.css.zfzx.sjcj.modules.app.repository.LoginUseRepository;
import com.css.zfzx.sjcj.modules.app.repository.LoginUserDeptRepository;
import com.css.zfzx.sjcj.modules.app.service.AppLoginService;
import com.css.zfzx.sjcj.modules.app.vo.RegistUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class AppLoginServiceImpl implements AppLoginService {
    @Autowired
    BaseDeptRepository baseDeptRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    LoginUseRepository loginUseRepository;
    @Autowired
    LoginUserDeptRepository loginUserDeptRepository;
    @Override
    public PwdCheck doLogin(String userName, String password, String privateKey) throws Exception {
        PwdCheck pwdCheck= PlatformAPI.getOrgAPI().getUserAPI().checkLoginPassword(userName,password,privateKey);
        return pwdCheck;
    }

    @Override
    public void registUser(RegistUserVo registUserVo) {
        String division = registUserVo.getDivision();
        UserEntity userEntity=new UserEntity();
        userEntity.setUserName(registUserVo.getRelName());
        userEntity.setIsValid(registUserVo.getIsValid());
        userEntity.setMobilePhone(registUserVo.getUserName());
        userEntity.setUserCode(registUserVo.getUserName());
        userEntity.setIdCard(registUserVo.getIdNumber().toString());
        userEntity.setCreateTime(new Date());
        userEntity.setExtend1(registUserVo.getDivision());
        userEntity.setExtend2(registUserVo.getUnit());
        //userEntity.setCreateUser(fwUserEntity.getCreateUser());
        String uuid = UUIDGenerator.getUUID();
        userEntity.setUserId(uuid);
        userEntity.setIsValid(YNEnum.Y.toString());
        userEntity.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        userEntity.setCreateUser(registUserVo.getRelName());
        loginUseRepository.save(userEntity);
        String baseUserId=uuid;
        PlatformAPI.getOrgAPI().getUserAPI().saveCertification(baseUserId,registUserVo.getPwd());
        List<String> list=new ArrayList<>();
        list.add(baseUserId);
        PlatformAPI.getOrgAPI().getUserAPI().activeUsers(list);
        DeptEntity deptEntity = baseDeptRepository.findByDivisionIdAndExtend1(division,"1");
        String deptId = deptEntity.getDeptId();
        DeptUserEntity deptUserEntity = new DeptUserEntity();
        deptUserEntity.setDeptId(deptId);
        deptUserEntity.setUserId(baseUserId);
        deptUserEntity.setIsMain("1");
        String uuidDept = UUIDGenerator.getUUID();
        deptUserEntity.setId(uuidDept);
        deptUserEntity.setIsValid("1");
        deptUserEntity.setCreateUser(registUserVo.getRelName());
        deptUserEntity.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        loginUserDeptRepository.save(deptUserEntity);
    }

    @Override
    public List<UserEntity> checkIdCard(String idCard) {
        List<UserEntity> cardAndIsValid = userRepository.findByIdCardAndIsValid(idCard, "1");
        return cardAndIsValid;
    }
}
