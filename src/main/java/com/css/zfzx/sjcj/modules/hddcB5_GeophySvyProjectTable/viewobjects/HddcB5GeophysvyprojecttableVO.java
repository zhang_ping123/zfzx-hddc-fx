package com.css.zfzx.sjcj.modules.hddcB5_GeophySvyProjectTable.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Data
public class HddcB5GeophysvyprojecttableVO implements Serializable {

    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 工程编号
     */
    private String id;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备注
     */
    @Excel(name = "备注")
    private String commentInfo;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 工程名称
     */
    @Excel(name = "工程名称")
    private String name;
    /**
     * 成果报告原始文件编号
     */
    @Excel(name = "成果报告原始文件编号")
    private String reportArwid;
    /**
     * 测线总长度 [米]
     */
    @Excel(name = "测线总长度 [米]")
    private Double svylinelength;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 省
     */
    @Excel(name = "省",orderNum = "1")
    private String province;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 变更情况原始编号
     */
    @Excel(name = "变更情况原始编号")
    private String csArwid;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 目标区编号
     */
    @Excel(name = "目标区编号")
    private String targetregionid;
    /**
     * 村
     */
    private String village;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 市
     */
    @Excel(name = "市",orderNum = "2")
    private String city;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 探测设计报告编号
     */
    @Excel(name = "探测设计报告编号")
    private String dbArid;
    /**
     * 地质填图区编号
     */
    @Excel(name = "地质填图区编号")
    private String mainafsregionid;
    /**
     * 工作区编号
     */
    @Excel(name = "工作区编号")
    private String workregionid;
    /**
     * 探测手段
     */
    @Excel(name = "探测手段")
    private Integer svymethod;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 探测原始数据说明
     */
    @Excel(name = "探测原始数据说明")
    private String surveycommentInfo;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 大数据文件编号
     */
    @Excel(name = "大数据文件编号")
    private String hugevolumedataid;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 探测设计原始文件编号
     */
    @Excel(name = "探测设计原始文件编号")
    private String dbArwid;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）",orderNum = "3")
    private String area;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 乡
     */
    private String town;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备注
     */
    private String remark;
    /**
     * 成果报告文件报告编号
     */
    @Excel(name = "成果报告文件报告编号")
    private String reportArid;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 变更情况报告编号
     */
    @Excel(name = "变更情况报告编号")
    private String csArid;
    /**
     * 测线条数
     */
    @Excel(name = "测线条数")
    private Integer svylinecount;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 探测单位
     */
    @Excel(name = "探测单位")
    private String svyinstitute;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 测点总数
     */
    @Excel(name = "测点总数")
    private Integer svypointcount;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer svymethodName;

    private String rowNum;
    private String errorMsg;
}