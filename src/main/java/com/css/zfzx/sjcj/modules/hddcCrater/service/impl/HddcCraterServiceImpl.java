package com.css.zfzx.sjcj.modules.hddcCrater.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcCrater.repository.HddcCraterNativeRepository;
import com.css.zfzx.sjcj.modules.hddcCrater.repository.HddcCraterRepository;
import com.css.zfzx.sjcj.modules.hddcCrater.repository.entity.HddcCraterEntity;
import com.css.zfzx.sjcj.modules.hddcCrater.service.HddcCraterService;
import com.css.zfzx.sjcj.modules.hddcCrater.viewobjects.HddcCraterQueryParams;
import com.css.zfzx.sjcj.modules.hddcCrater.viewobjects.HddcCraterVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
@Service
public class HddcCraterServiceImpl implements HddcCraterService {

	@Autowired
    private HddcCraterRepository hddcCraterRepository;
    @Autowired
    private HddcCraterNativeRepository hddcCraterNativeRepository;

    @Override
    public JSONObject queryHddcCraters(HddcCraterQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcCraterEntity> hddcCraterPage = this.hddcCraterNativeRepository.queryHddcCraters(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcCraterPage);
        return jsonObject;
    }


    @Override
    public HddcCraterEntity getHddcCrater(String id) {
        HddcCraterEntity hddcCrater = this.hddcCraterRepository.findById(id).orElse(null);
         return hddcCrater;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcCraterEntity saveHddcCrater(HddcCraterEntity hddcCrater) {
        String uuid = UUIDGenerator.getUUID();
        hddcCrater.setUuid(uuid);
        hddcCrater.setCreateUser(PlatformSessionUtils.getUserId());
        hddcCrater.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcCraterRepository.save(hddcCrater);
        return hddcCrater;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcCraterEntity updateHddcCrater(HddcCraterEntity hddcCrater) {
        HddcCraterEntity entity = hddcCraterRepository.findById(hddcCrater.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcCrater);
        hddcCrater.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcCrater.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcCraterRepository.save(hddcCrater);
        return hddcCrater;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcCraters(List<String> ids) {
        List<HddcCraterEntity> hddcCraterList = this.hddcCraterRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcCraterList) && hddcCraterList.size() > 0) {
            for(HddcCraterEntity hddcCrater : hddcCraterList) {
                this.hddcCraterRepository.delete(hddcCrater);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcCraterQueryParams queryParams, HttpServletResponse response) {
        List<HddcCraterEntity> yhDisasterEntities = hddcCraterNativeRepository.exportYhDisasters(queryParams);
        List<HddcCraterVO> list=new ArrayList<>();
        for (HddcCraterEntity entity:yhDisasterEntities) {
            HddcCraterVO yhDisasterVO=new HddcCraterVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"火山口-点","火山口-点",HddcCraterVO.class,"火山口-点.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcCraterVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcCraterVO.class, params);
            List<HddcCraterVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcCraterVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcCraterVO yhDisasterVO = iterator.next();
                    String error = "";
                    /*returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");*/
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcCraterVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcCraterEntity yhDisasterEntity = new HddcCraterEntity();
            HddcCraterVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcCrater(yhDisasterEntity);
        }
    }

}
