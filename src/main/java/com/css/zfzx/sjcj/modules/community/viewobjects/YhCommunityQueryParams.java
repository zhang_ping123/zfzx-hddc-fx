package com.css.zfzx.sjcj.modules.community.viewobjects;

import lombok.Data;
import java.util.Date;

/**
 * @author yyd
 * @date 2020-11-04
 */
@Data
public class YhCommunityQueryParams {


    private String cztName;
    private String province;
    private String area;
    private String city;
    private String buildingLevel;
    private String yhLevel;
    private String status;
    private String createTime;
    private String createUser;

}
