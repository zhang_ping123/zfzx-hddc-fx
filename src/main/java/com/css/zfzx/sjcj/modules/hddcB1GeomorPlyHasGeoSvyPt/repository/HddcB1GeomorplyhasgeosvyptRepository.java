package com.css.zfzx.sjcj.modules.hddcB1GeomorPlyHasGeoSvyPt.repository;

import com.css.zfzx.sjcj.modules.hddcB1GeomorPlyHasGeoSvyPt.repository.entity.HddcB1GeomorplyhasgeosvyptEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcB1GeomorplyhasgeosvyptRepository extends JpaRepository<HddcB1GeomorplyhasgeosvyptEntity, String> {
}
