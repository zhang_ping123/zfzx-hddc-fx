package com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningLine.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningLine.repository.HddcGeologicalsvyplanninglineNativeRepository;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningLine.repository.HddcGeologicalsvyplanninglineRepository;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningLine.repository.entity.HddcGeologicalsvyplanninglineEntity;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningLine.service.HddcGeologicalsvyplanninglineService;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningLine.viewobjects.HddcGeologicalsvyplanninglineQueryParams;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPlanningLine.viewobjects.HddcGeologicalsvyplanninglineVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zyb
 * @date 2020-12-07
 */
@Service
public class HddcGeologicalsvyplanninglineServiceImpl implements HddcGeologicalsvyplanninglineService {

	@Autowired
    private HddcGeologicalsvyplanninglineRepository hddcGeologicalsvyplanninglineRepository;
    @Autowired
    private HddcGeologicalsvyplanninglineNativeRepository hddcGeologicalsvyplanninglineNativeRepository;

    @Override
    public JSONObject queryHddcGeologicalsvyplanninglines(HddcGeologicalsvyplanninglineQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcGeologicalsvyplanninglineEntity> hddcGeologicalsvyplanninglinePage = this.hddcGeologicalsvyplanninglineNativeRepository.queryHddcGeologicalsvyplanninglines(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcGeologicalsvyplanninglinePage);
        return jsonObject;
    }


    @Override
    public HddcGeologicalsvyplanninglineEntity getHddcGeologicalsvyplanningline(String id) {
        HddcGeologicalsvyplanninglineEntity hddcGeologicalsvyplanningline = this.hddcGeologicalsvyplanninglineRepository.findById(id).orElse(null);
         return hddcGeologicalsvyplanningline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeologicalsvyplanninglineEntity saveHddcGeologicalsvyplanningline(HddcGeologicalsvyplanninglineEntity hddcGeologicalsvyplanningline) {
        String uuid = UUIDGenerator.getUUID();
        hddcGeologicalsvyplanningline.setUuid(uuid);
        hddcGeologicalsvyplanningline.setCreateUser(PlatformSessionUtils.getUserId());
        hddcGeologicalsvyplanningline.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeologicalsvyplanninglineRepository.save(hddcGeologicalsvyplanningline);
        return hddcGeologicalsvyplanningline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeologicalsvyplanninglineEntity updateHddcGeologicalsvyplanningline(HddcGeologicalsvyplanninglineEntity hddcGeologicalsvyplanningline) {
        HddcGeologicalsvyplanninglineEntity entity = hddcGeologicalsvyplanninglineRepository.findById(hddcGeologicalsvyplanningline.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcGeologicalsvyplanningline);
        hddcGeologicalsvyplanningline.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcGeologicalsvyplanningline.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeologicalsvyplanninglineRepository.save(hddcGeologicalsvyplanningline);
        return hddcGeologicalsvyplanningline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcGeologicalsvyplanninglines(List<String> ids) {
        List<HddcGeologicalsvyplanninglineEntity> hddcGeologicalsvyplanninglineList = this.hddcGeologicalsvyplanninglineRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcGeologicalsvyplanninglineList) && hddcGeologicalsvyplanninglineList.size() > 0) {
            for(HddcGeologicalsvyplanninglineEntity hddcGeologicalsvyplanningline : hddcGeologicalsvyplanninglineList) {
                this.hddcGeologicalsvyplanninglineRepository.delete(hddcGeologicalsvyplanningline);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcGeologicalsvyplanninglineQueryParams queryParams, HttpServletResponse response) {
        List<HddcGeologicalsvyplanninglineEntity> yhDisasterEntities = hddcGeologicalsvyplanninglineNativeRepository.exportYhDisasters(queryParams);
        List<HddcGeologicalsvyplanninglineVO> list=new ArrayList<>();
        for (HddcGeologicalsvyplanninglineEntity entity:yhDisasterEntities) {
            HddcGeologicalsvyplanninglineVO yhDisasterVO=new HddcGeologicalsvyplanninglineVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"地质调查规划路线","地质调查规划路线",HddcGeologicalsvyplanninglineVO.class,"地质调查规划路线-线.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcGeologicalsvyplanninglineVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcGeologicalsvyplanninglineVO.class, params);
            List<HddcGeologicalsvyplanninglineVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcGeologicalsvyplanninglineVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcGeologicalsvyplanninglineVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcGeologicalsvyplanninglineVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcGeologicalsvyplanninglineEntity yhDisasterEntity = new HddcGeologicalsvyplanninglineEntity();
            HddcGeologicalsvyplanninglineVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcGeologicalsvyplanningline(yhDisasterEntity);
        }
    }

}
