package com.css.zfzx.sjcj.modules.community.repository;

import com.css.zfzx.sjcj.modules.community.repository.entity.YhCommunityEntity;
import com.css.zfzx.sjcj.modules.community.viewobjects.YhCommunityQueryParams;
import org.springframework.data.domain.Page;

/**
 * @author yyd
 * @date 2020-11-04
 */
public interface YhCommunityNativeRepository {

    Page<YhCommunityEntity> queryYhCommunitys(YhCommunityQueryParams queryParams, int curPage, int pageSize);
}
