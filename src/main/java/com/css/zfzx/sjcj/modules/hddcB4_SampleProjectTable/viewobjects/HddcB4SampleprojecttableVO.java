package com.css.zfzx.sjcj.modules.hddcB4_SampleProjectTable.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Data
public class HddcB4SampleprojecttableVO implements Serializable {

    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 区（县）
     */
    @Excel(name="区（县）",orderNum = "3")
    private String area;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 村
     */
    private String village;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 工程编号
     */
    private String id;
    /**
     * 工作区编号
     */
    @Excel(name="工作区编号",orderNum = "6")
    private String workregionid;
    /**
     * 省
     */
    @Excel(name="省",orderNum = "1")
    private String province;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 送样报表编号
     */
    @Excel(name="送样报表编号",orderNum = "17")
    private String deliverreportArid;
    /**
     * 采集样品总数
     */
    @Excel(name="送样者",orderNum = "14")
    private Integer collectedsamplecount;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 成果报告报告文件编号
     */
    @Excel(name="送样者",orderNum = "23")
    private String resultreportArid;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备注
     */
    private String remark;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 测试报告原始文件编号
     */
    @Excel(name="测试报告原始文件编号",orderNum = "20")
    private String testreportArwid;
    /**
     * 测试方法
     */
    @Excel(name="测试方法",orderNum = "13")
    private Integer sampletestedmethod;
    /**
     * 送样者
     */
    @Excel(name="送样者",orderNum = "8")
    private String deliverperson;
    /**
     * 备注
     */
    @Excel(name="备注",orderNum = "25")
    private String commentInfo;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 获得结果样品总数
     */
    @Excel(name="获得结果样品总数",orderNum = "16")
    private Integer datingsamplecount;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 成果报告原始文件编号
     */
    @Excel(name="成果报告原始文件编号",orderNum = "24")
    private String resultreportArwid;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 测试人员
     */
    @Excel(name="测试人员",orderNum = "21")
    private String surveyor;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 送样总数
     */
    @Excel(name="送样总数",orderNum = "15")
    private Integer samplecount;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 测量仪器
     */
    @Excel(name="测量仪器",orderNum = "11")
    private String testapparatus;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 送样单位
     */
    @Excel(name="送样单位",orderNum = "9")
    private String deliverinstitute;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 市
     */
    @Excel(name="市",orderNum = "2")
    private String city;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 采样类型
     */
    @Excel(name="采样类型",orderNum = "12")
    private Integer sampletype;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 送样报表原始文件编号
     */
    @Excel(name="送样报表原始文件编号",orderNum = "18")
    private String deliverreportArwid;
    /**
     * 测试单位
     */
    @Excel(name="测试单位",orderNum = "22")
    private String testinstitute;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 送样报告日期
     */
    @Excel(name="送样报告日期",orderNum = "10")
    private String reportdate;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 测试报告文件编号
     */
    @Excel(name="测试报告文件编号",orderNum = "19")
    private String testreportArid;
    /**
     * 乡
     */
    private String town;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 地质填图区编号
     */
    @Excel(name="地质填图区编号",orderNum = "4")
    private String mainafsregionid;
    /**
     * 目标区编号
     */
    @Excel(name="目标区编号",orderNum = "5")
    private String targetregionid;
    /**
     * 工程名称
     */
    @Excel(name="工程名称",orderNum = "7")
    private String name;

    private String provinceName;
    private String cityName;
    private String areaName;
    private Integer sampletypeName;
    private Integer sampletestedmethodName;
    private String rowNum;
    private String errorMsg;

}