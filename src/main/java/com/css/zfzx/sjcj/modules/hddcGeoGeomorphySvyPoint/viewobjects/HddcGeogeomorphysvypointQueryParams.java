package com.css.zfzx.sjcj.modules.hddcGeoGeomorphySvyPoint.viewobjects;

import lombok.Data;

/**
 * @author lihelei
 * @date 2020-11-26
 */
@Data
public class HddcGeogeomorphysvypointQueryParams {


    private String province;
    private String city;
    private String area;
    private String projectName;
    private String geomorphyname;

}
