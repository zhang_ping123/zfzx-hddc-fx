package com.css.zfzx.sjcj.modules.community.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.community.repository.YhCommunityNativeRepository;
import com.css.zfzx.sjcj.modules.community.repository.YhCommunityRepository;
import com.css.zfzx.sjcj.modules.community.repository.entity.YhCommunityEntity;
import com.css.zfzx.sjcj.modules.community.service.YhCommunityService;
import com.css.zfzx.sjcj.modules.community.viewobjects.YhCommunityQueryParams;
import com.css.zfzx.sjcj.modules.community.viewobjects.YhCommunityVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author yyd
 * @date 2020-11-04
 */
@Service
public class YhCommunityServiceImpl implements YhCommunityService {

	@Autowired
    private YhCommunityRepository yhCommunityRepository;
    @Autowired
    private YhCommunityNativeRepository yhCommunityNativeRepository;

    @Override
    public JSONObject queryYhCommunitys(YhCommunityQueryParams queryParams, int curPage, int pageSize) {
        Page<YhCommunityEntity> yhCommunityPage = this.yhCommunityNativeRepository.queryYhCommunitys(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(yhCommunityPage);
        return jsonObject;
    }


    @Override
    public YhCommunityEntity getYhCommunity(String id) {
        YhCommunityEntity yhCommunity = this.yhCommunityRepository.findById(id).orElse(null);
         return yhCommunity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public YhCommunityEntity saveYhCommunity(YhCommunityEntity yhCommunity) {
        String uuid = UUIDGenerator.getUUID();
        yhCommunity.setId(uuid);
        yhCommunity.setCreateUser(PlatformSessionUtils.getUserId());
        yhCommunity.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.yhCommunityRepository.save(yhCommunity);
        return yhCommunity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public YhCommunityEntity updateYhCommunity(YhCommunityEntity yhCommunity) {
        YhCommunityEntity entity = yhCommunityRepository.findById(yhCommunity.getId()).get();
        UpdateUtil.copyNullProperties(entity,yhCommunity);
        yhCommunity.setUpdateUser(PlatformSessionUtils.getUserId());
        yhCommunity.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.yhCommunityRepository.save(yhCommunity);
        return yhCommunity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteYhCommunitys(List<String> ids) {
        List<YhCommunityEntity> yhCommunityList = this.yhCommunityRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(yhCommunityList) && yhCommunityList.size() > 0) {
            for(YhCommunityEntity yhCommunity : yhCommunityList) {
                this.yhCommunityRepository.delete(yhCommunity);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

}
