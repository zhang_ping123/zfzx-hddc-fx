package com.css.zfzx.sjcj.modules.hddcB7_VolcanicSvyProjectTable.repository;

import com.css.zfzx.sjcj.modules.hddcB7_VolcanicSvyProjectTable.repository.entity.HddcB7VolcanicsvyprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB7_VolcanicSvyProjectTable.viewobjects.HddcB7VolcanicsvyprojecttableQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-26
 */
public interface HddcB7VolcanicsvyprojecttableNativeRepository {

    Page<HddcB7VolcanicsvyprojecttableEntity> queryHddcB7Volcanicsvyprojecttables(HddcB7VolcanicsvyprojecttableQueryParams queryParams, int curPage, int pageSize);

    List<HddcB7VolcanicsvyprojecttableEntity> exportYhDisasters(HddcB7VolcanicsvyprojecttableQueryParams queryParams);
}
