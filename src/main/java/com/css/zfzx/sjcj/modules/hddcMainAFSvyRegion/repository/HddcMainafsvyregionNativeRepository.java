package com.css.zfzx.sjcj.modules.hddcMainAFSvyRegion.repository;

import com.css.zfzx.sjcj.modules.hddcMainAFSvyRegion.repository.entity.HddcMainafsvyregionEntity;
import com.css.zfzx.sjcj.modules.hddcMainAFSvyRegion.viewobjects.HddcMainafsvyregionQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-12-14
 */
public interface HddcMainafsvyregionNativeRepository {

    Page<HddcMainafsvyregionEntity> queryHddcMainafsvyregions(HddcMainafsvyregionQueryParams queryParams, int curPage, int pageSize);

    List<HddcMainafsvyregionEntity> exportYhDisasters(HddcMainafsvyregionQueryParams queryParams);
}
