package com.css.zfzx.sjcj.modules.hddcGeoGeomorphySvyPoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcGeoGeomorphySvyPoint.repository.entity.HddcGeogeomorphysvypointEntity;
import com.css.zfzx.sjcj.modules.hddcGeoGeomorphySvyPoint.viewobjects.HddcGeogeomorphysvypointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author lihelei
 * @date 2020-11-26
 */

public interface HddcGeogeomorphysvypointService {

    public JSONObject queryHddcGeogeomorphysvypoints(HddcGeogeomorphysvypointQueryParams queryParams, int curPage, int pageSize);

    public HddcGeogeomorphysvypointEntity getHddcGeogeomorphysvypoint(String id);

    public HddcGeogeomorphysvypointEntity saveHddcGeogeomorphysvypoint(HddcGeogeomorphysvypointEntity hddcGeogeomorphysvypoint);

    public HddcGeogeomorphysvypointEntity updateHddcGeogeomorphysvypoint(HddcGeogeomorphysvypointEntity hddcGeogeomorphysvypoint);

    public void deleteHddcGeogeomorphysvypoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGeogeomorphysvypointQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
