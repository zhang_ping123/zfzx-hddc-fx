package com.css.zfzx.sjcj.modules.hddcDrillFaultPoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcDrillFaultPoint.repository.entity.HddcDrillfaultpointEntity;
import com.css.zfzx.sjcj.modules.hddcDrillFaultPoint.viewobjects.HddcDrillfaultpointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */

public interface HddcDrillfaultpointService {

    public JSONObject queryHddcDrillfaultpoints(HddcDrillfaultpointQueryParams queryParams, int curPage, int pageSize);

    public HddcDrillfaultpointEntity getHddcDrillfaultpoint(String id);

    public HddcDrillfaultpointEntity saveHddcDrillfaultpoint(HddcDrillfaultpointEntity hddcDrillfaultpoint);

    public HddcDrillfaultpointEntity updateHddcDrillfaultpoint(HddcDrillfaultpointEntity hddcDrillfaultpoint);

    public void deleteHddcDrillfaultpoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcDrillfaultpointQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
