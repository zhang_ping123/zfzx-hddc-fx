package com.css.zfzx.sjcj.modules.hddcWorkRegion.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcWorkRegion.repository.entity.HddcWorkregionEntity;
import com.css.zfzx.sjcj.modules.hddcWorkRegion.viewobjects.HddcWorkregionQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */

public interface HddcWorkregionService {

    public JSONObject queryHddcWorkregions(HddcWorkregionQueryParams queryParams, int curPage, int pageSize);

    public HddcWorkregionEntity getHddcWorkregion(String id);

    public HddcWorkregionEntity saveHddcWorkregion(HddcWorkregionEntity hddcWorkregion);

    public HddcWorkregionEntity updateHddcWorkregion(HddcWorkregionEntity hddcWorkregion);

    public void deleteHddcWorkregions(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcWorkregionQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
