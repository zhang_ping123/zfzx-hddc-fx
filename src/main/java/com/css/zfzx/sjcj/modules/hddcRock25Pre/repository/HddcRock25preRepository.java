package com.css.zfzx.sjcj.modules.hddcRock25Pre.repository;

import com.css.zfzx.sjcj.modules.hddcRock25Pre.repository.entity.HddcRock25preEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcRock25preRepository extends JpaRepository<HddcRock25preEntity, String> {
}
