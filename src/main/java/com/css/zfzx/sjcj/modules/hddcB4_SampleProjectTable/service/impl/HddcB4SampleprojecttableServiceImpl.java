package com.css.zfzx.sjcj.modules.hddcB4_SampleProjectTable.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB4_SampleProjectTable.repository.HddcB4SampleprojecttableNativeRepository;
import com.css.zfzx.sjcj.modules.hddcB4_SampleProjectTable.repository.HddcB4SampleprojecttableRepository;
import com.css.zfzx.sjcj.modules.hddcB4_SampleProjectTable.repository.entity.HddcB4SampleprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB4_SampleProjectTable.service.HddcB4SampleprojecttableService;
import com.css.zfzx.sjcj.modules.hddcB4_SampleProjectTable.viewobjects.HddcB4SampleprojecttableQueryParams;
import com.css.zfzx.sjcj.modules.hddcB4_SampleProjectTable.viewobjects.HddcB4SampleprojecttableVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
@Service
public class HddcB4SampleprojecttableServiceImpl implements HddcB4SampleprojecttableService {

	@Autowired
    private HddcB4SampleprojecttableRepository hddcB4SampleprojecttableRepository;
    @Autowired
    private HddcB4SampleprojecttableNativeRepository hddcB4SampleprojecttableNativeRepository;

    @Override
    public JSONObject queryHddcB4Sampleprojecttables(HddcB4SampleprojecttableQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcB4SampleprojecttableEntity> hddcB4SampleprojecttablePage = this.hddcB4SampleprojecttableNativeRepository.queryHddcB4Sampleprojecttables(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcB4SampleprojecttablePage);
        return jsonObject;
    }


    @Override
    public HddcB4SampleprojecttableEntity getHddcB4Sampleprojecttable(String id) {
        HddcB4SampleprojecttableEntity hddcB4Sampleprojecttable = this.hddcB4SampleprojecttableRepository.findById(id).orElse(null);
         return hddcB4Sampleprojecttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB4SampleprojecttableEntity saveHddcB4Sampleprojecttable(HddcB4SampleprojecttableEntity hddcB4Sampleprojecttable) {
        String uuid = UUIDGenerator.getUUID();
        hddcB4Sampleprojecttable.setUuid(uuid);
        hddcB4Sampleprojecttable.setCreateUser(PlatformSessionUtils.getUserId());
        hddcB4Sampleprojecttable.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB4SampleprojecttableRepository.save(hddcB4Sampleprojecttable);
        return hddcB4Sampleprojecttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB4SampleprojecttableEntity updateHddcB4Sampleprojecttable(HddcB4SampleprojecttableEntity hddcB4Sampleprojecttable) {
        HddcB4SampleprojecttableEntity entity = hddcB4SampleprojecttableRepository.findById(hddcB4Sampleprojecttable.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcB4Sampleprojecttable);
        hddcB4Sampleprojecttable.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcB4Sampleprojecttable.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB4SampleprojecttableRepository.save(hddcB4Sampleprojecttable);
        return hddcB4Sampleprojecttable;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcB4Sampleprojecttables(List<String> ids) {
        List<HddcB4SampleprojecttableEntity> hddcB4SampleprojecttableList = this.hddcB4SampleprojecttableRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcB4SampleprojecttableList) && hddcB4SampleprojecttableList.size() > 0) {
            for(HddcB4SampleprojecttableEntity hddcB4Sampleprojecttable : hddcB4SampleprojecttableList) {
                this.hddcB4SampleprojecttableRepository.delete(hddcB4Sampleprojecttable);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcB4SampleprojecttableQueryParams queryParams, HttpServletResponse response) {
        List<HddcB4SampleprojecttableEntity> yhDisasterEntities = hddcB4SampleprojecttableNativeRepository.exportYhDisasters(queryParams);
        List<HddcB4SampleprojecttableVO> list=new ArrayList<>();
        for (HddcB4SampleprojecttableEntity entity:yhDisasterEntities) {
            HddcB4SampleprojecttableVO yhDisasterVO=new HddcB4SampleprojecttableVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"采样工程表","采样工程表",HddcB4SampleprojecttableVO.class,"采样工程表.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcB4SampleprojecttableVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcB4SampleprojecttableVO.class, params);
            List<HddcB4SampleprojecttableVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcB4SampleprojecttableVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcB4SampleprojecttableVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcB4SampleprojecttableVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcB4SampleprojecttableEntity yhDisasterEntity = new HddcB4SampleprojecttableEntity();
            HddcB4SampleprojecttableVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcB4Sampleprojecttable(yhDisasterEntity);
        }
    }

}
