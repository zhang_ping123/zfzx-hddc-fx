package com.css.zfzx.sjcj.modules.hddcGeophySvyLine.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcGeophySvyLine.repository.HddcGeophysvylineNativeRepository;
import com.css.zfzx.sjcj.modules.hddcGeophySvyLine.repository.HddcGeophysvylineRepository;
import com.css.zfzx.sjcj.modules.hddcGeophySvyLine.repository.entity.HddcGeophysvylineEntity;
import com.css.zfzx.sjcj.modules.hddcGeophySvyLine.service.HddcGeophysvylineService;
import com.css.zfzx.sjcj.modules.hddcGeophySvyLine.viewobjects.HddcGeophysvylineQueryParams;
import com.css.zfzx.sjcj.modules.hddcGeophySvyLine.viewobjects.HddcGeophysvylineVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zhangcong
 * @date 2020-11-28
 */
@Service
public class HddcGeophysvylineServiceImpl implements HddcGeophysvylineService {

	@Autowired
    private HddcGeophysvylineRepository hddcGeophysvylineRepository;
    @Autowired
    private HddcGeophysvylineNativeRepository hddcGeophysvylineNativeRepository;

    @Override
    public JSONObject queryHddcGeophysvylines(HddcGeophysvylineQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcGeophysvylineEntity> hddcGeophysvylinePage = this.hddcGeophysvylineNativeRepository.queryHddcGeophysvylines(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcGeophysvylinePage);
        return jsonObject;
    }


    @Override
    public HddcGeophysvylineEntity getHddcGeophysvyline(String id) {
        HddcGeophysvylineEntity hddcGeophysvyline = this.hddcGeophysvylineRepository.findById(id).orElse(null);
         return hddcGeophysvyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeophysvylineEntity saveHddcGeophysvyline(HddcGeophysvylineEntity hddcGeophysvyline) {
        String uuid = UUIDGenerator.getUUID();
        hddcGeophysvyline.setUuid(uuid);
        hddcGeophysvyline.setCreateUser(PlatformSessionUtils.getUserId());
        hddcGeophysvyline.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeophysvylineRepository.save(hddcGeophysvyline);
        return hddcGeophysvyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcGeophysvylineEntity updateHddcGeophysvyline(HddcGeophysvylineEntity hddcGeophysvyline) {
        HddcGeophysvylineEntity entity = hddcGeophysvylineRepository.findById(hddcGeophysvyline.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcGeophysvyline);
        hddcGeophysvyline.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcGeophysvyline.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcGeophysvylineRepository.save(hddcGeophysvyline);
        return hddcGeophysvyline;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcGeophysvylines(List<String> ids) {
        List<HddcGeophysvylineEntity> hddcGeophysvylineList = this.hddcGeophysvylineRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcGeophysvylineList) && hddcGeophysvylineList.size() > 0) {
            for(HddcGeophysvylineEntity hddcGeophysvyline : hddcGeophysvylineList) {
                this.hddcGeophysvylineRepository.delete(hddcGeophysvyline);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcGeophysvylineQueryParams queryParams, HttpServletResponse response) {
        List<HddcGeophysvylineEntity> yhDisasterEntities = hddcGeophysvylineNativeRepository.exportYhDisasters(queryParams);
        List<HddcGeophysvylineVO> list=new ArrayList<>();
        for (HddcGeophysvylineEntity entity:yhDisasterEntities) {
            HddcGeophysvylineVO yhDisasterVO=new HddcGeophysvylineVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "地球物理测线-线", "地球物理测线-线", HddcGeophysvylineVO.class, "地球物理测线-线.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcGeophysvylineVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcGeophysvylineVO.class, params);
            List<HddcGeophysvylineVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcGeophysvylineVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcGeophysvylineVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcGeophysvylineVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcGeophysvylineEntity yhDisasterEntity = new HddcGeophysvylineEntity();
            HddcGeophysvylineVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcGeophysvyline(yhDisasterEntity);
        }
    }

}
