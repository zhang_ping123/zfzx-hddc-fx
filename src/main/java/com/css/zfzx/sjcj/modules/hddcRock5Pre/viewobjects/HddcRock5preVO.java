package com.css.zfzx.sjcj.modules.hddcRock5Pre.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author lhl
 * @date 2020-11-27
 */
@Data
public class HddcRock5preVO implements Serializable {

    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 岩体符号下标位
     */
    @Excel(name = "岩体符号下标位", orderNum = "4")
    private String nsb2;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 村
     */
    private String village;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 侵入时代
     */
    @Excel(name = "侵入时代", orderNum = "5")
    private Integer qdho;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "6")
    private String commentInfo;
    /**
     * 乡
     */
    private String town;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 岩体符号上标位
     */
    @Excel(name = "岩体符号上标位", orderNum = "7")
    private String nsb3;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 岩体合并
     */
    @Excel(name = "岩体合并", orderNum = "8")
    private String rockunion;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 岩体名称
     */
    @Excel(name = "岩体名称", orderNum = "9")
    private String rockname;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 岩体编号
     */
    private String id;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 岩体类别
     */
    @Excel(name = "岩体类别", orderNum = "10")
    private Integer symbol;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 备注
     */
    private String remark;
    /**
     * 岩体符号基础位
     */
    @Excel(name = "岩体符号基础位", orderNum = "11")
    private String nsb1;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 岩体描述
     */
    @Excel(name = "岩体描述", orderNum = "12")
    private String description;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段16
     */
    private String extends16;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String symbolName;
    private Integer qdhoName;
    private String rowNum;
    private String errorMsg;
}