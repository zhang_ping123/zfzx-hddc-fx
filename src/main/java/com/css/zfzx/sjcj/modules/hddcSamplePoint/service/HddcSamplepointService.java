package com.css.zfzx.sjcj.modules.hddcSamplePoint.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcSamplePoint.repository.entity.HddcSamplepointEntity;
import com.css.zfzx.sjcj.modules.hddcSamplePoint.viewobjects.HddcSamplepointQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */

public interface HddcSamplepointService {

    public JSONObject queryHddcSamplepoints(HddcSamplepointQueryParams queryParams, int curPage, int pageSize);

    public HddcSamplepointEntity getHddcSamplepoint(String id);

    public HddcSamplepointEntity saveHddcSamplepoint(HddcSamplepointEntity hddcSamplepoint);

    public HddcSamplepointEntity updateHddcSamplepoint(HddcSamplepointEntity hddcSamplepoint);

    public void deleteHddcSamplepoints(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcSamplepointQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
