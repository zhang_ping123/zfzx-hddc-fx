package com.css.zfzx.sjcj.modules.hddcwyGeomorStation.repository;

import com.css.zfzx.sjcj.modules.hddcwyFaultSvyPoint.repository.entity.HddcWyFaultsvypointEntity;
import com.css.zfzx.sjcj.modules.hddcwyGeomorStation.repository.entity.HddcWyGeomorstationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-12-01
 */
public interface HddcWyGeomorstationRepository extends JpaRepository<HddcWyGeomorstationEntity, String> {

    List<HddcWyGeomorstationEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId, String taskId, String projectId, String isValid);

    List<HddcWyGeomorstationEntity> findAllByCreateUserAndIsValid(String userId,String isValid);


    @Query(nativeQuery = true, value = "select * from hddc_wy_geomorstation where is_valid!=0 project_name in :projectIds")
    List<HddcWyGeomorstationEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_geomorstation where task_name in :projectIds")
    List<HddcWyGeomorstationEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);


}
