package com.css.zfzx.sjcj.modules.hddcB6_GeochemicalProjectTable.repository;

import com.css.zfzx.sjcj.modules.hddcB6_GeochemicalProjectTable.repository.entity.HddcB6GeochemicalprojecttableEntity;
import com.css.zfzx.sjcj.modules.hddcB6_GeochemicalProjectTable.viewobjects.HddcB6GeochemicalprojecttableQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
public interface HddcB6GeochemicalprojecttableNativeRepository {

    Page<HddcB6GeochemicalprojecttableEntity> queryHddcB6Geochemicalprojecttables(HddcB6GeochemicalprojecttableQueryParams queryParams, int curPage, int pageSize);

    List<HddcB6GeochemicalprojecttableEntity> exportYhDisasters(HddcB6GeochemicalprojecttableQueryParams queryParams);
}
