package com.css.zfzx.sjcj.modules.hddcGeomorphySvyReProf.service;

import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyReProf.repository.entity.HddcGeomorphysvyreprofEntity;
import com.css.zfzx.sjcj.modules.hddcGeomorphySvyReProf.viewobjects.HddcGeomorphysvyreprofQueryParams;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */

public interface HddcGeomorphysvyreprofService {

    public JSONObject queryHddcGeomorphysvyreprofs(HddcGeomorphysvyreprofQueryParams queryParams, int curPage, int pageSize);

    public HddcGeomorphysvyreprofEntity getHddcGeomorphysvyreprof(String id);

    public HddcGeomorphysvyreprofEntity saveHddcGeomorphysvyreprof(HddcGeomorphysvyreprofEntity hddcGeomorphysvyreprof);

    public HddcGeomorphysvyreprofEntity updateHddcGeomorphysvyreprof(HddcGeomorphysvyreprofEntity hddcGeomorphysvyreprof);

    public void deleteHddcGeomorphysvyreprofs(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGeomorphysvyreprofQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
    /**
     * 逻辑删除-根据项目id删除数据
     * @param projectIds
     */
    void deleteByProjectId(List<String> projectIds);

    /**
     * 逻辑删除-根据任务id删除数据
     * @param taskIds
     */
    void deleteByTaskId(List<String> taskIds);

    String judegeParams(String[] datas);

    void savehddcGeomorphysvyreprofServiceFromShpFiles(List<List<Object>> list, String provinceName, String cityName, String areaName, String savefilename);

    List<HddcGeomorphysvyreprofEntity> findAll();
}
