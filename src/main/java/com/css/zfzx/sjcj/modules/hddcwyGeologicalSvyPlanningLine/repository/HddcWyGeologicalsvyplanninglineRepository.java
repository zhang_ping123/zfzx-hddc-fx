package com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningLine.repository;

import com.css.zfzx.sjcj.modules.hddcwyGeologicalSvyPlanningLine.repository.entity.HddcWyGeologicalsvyplanninglineEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zhangping
 * @date 2020-12-01
 */
public interface HddcWyGeologicalsvyplanninglineRepository extends JpaRepository<HddcWyGeologicalsvyplanninglineEntity, String> {

    List<HddcWyGeologicalsvyplanninglineEntity> findAllByCreateUserAndTaskIdAndProjectIdAndIsValid(String userId,String taskId,String projectId,String isValid);

    List<HddcWyGeologicalsvyplanninglineEntity> findAllByCreateUserAndIsValid(String userId,String isValid);

    @Query(nativeQuery = true, value = "select * from hddc_wy_geologicalsvyplanningline where is_valid!=0 project_name in :projectIds")
    List<HddcWyGeologicalsvyplanninglineEntity> queryHddcA1InvrgnhasmaterialtablesByProjectId(List<String> projectIds);
    @Query(nativeQuery = true, value = "select * from hddc_wy_geologicalsvyplanningline where task_name in :projectIds")
    List<HddcWyGeologicalsvyplanninglineEntity> queryHddcA1InvrgnhasmaterialtablesByTaskId(List<String> projectIds);

}
