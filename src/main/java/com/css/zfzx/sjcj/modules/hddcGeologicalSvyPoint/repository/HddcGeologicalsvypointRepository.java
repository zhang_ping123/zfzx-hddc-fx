package com.css.zfzx.sjcj.modules.hddcGeologicalSvyPoint.repository;

import com.css.zfzx.sjcj.modules.hddcGeologicalSvyPoint.repository.entity.HddcGeologicalsvypointEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author lihelei
 * @date 2020-11-27
 */
public interface HddcGeologicalsvypointRepository extends JpaRepository<HddcGeologicalsvypointEntity, String> {
}
