package com.css.zfzx.sjcj.modules.hddcwyLava.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zhangcong
 * @date 2020-12-02
 */
@Data
public class HddcWyLavaVO implements Serializable {

    /**
     * 照片文件编号
     */
    @Excel(name = "照片文件编号", orderNum = "15")
    private String photoAiid;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 熔岩流名称
     */
    @Excel(name = "熔岩流名称", orderNum = "19")
    private String name;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 素描图原始文件编码
     */
    @Excel(name = "素描图原始文件编码", orderNum = "16")
    private String sketchArwid;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 熔岩流描述
     */
    @Excel(name = "熔岩流描述", orderNum = "14")
    private String description;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 熔岩流符号代码
     */
    @Excel(name = "熔岩流符号代码", orderNum = "20")
    private String type;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "4")
    private String area;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 照片集镜向及拍摄者说明文档
     */
    @Excel(name = "照片集镜向及拍摄者说明文档", orderNum = "25")
    private String photodescArwid;
    /**
     * 岩石名称
     */
    @Excel(name = "岩石名称", orderNum = "17")
    private String rockname;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 项目名称
     */
    @Excel(name="项目名称",orderNum = "7")
    private String projectName;
    /**
     * 熔岩流表面形态
     */
    @Excel(name = "熔岩流表面形态", orderNum = "12")
    private String surfacemorphology;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 岩石类型
     */
    @Excel(name = "岩石类型", orderNum = "22")
    private Integer rocktype;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 拍摄者
     */
    @Excel(name = "拍摄者", orderNum = "21")
    private String photographer;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "26")
    private String commentInfo;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "3")
    private String city;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "2")
    private String province;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 任务名称
     */
    @Excel(name="任务名称",orderNum = "8")
    private String taskName;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 熔岩流时代
     */
    @Excel(name = "熔岩流时代", orderNum = "24")
    private Integer age;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 熔岩流编号
     */
    @Excel(name = "熔岩流编号", orderNum = "1")
    private String id;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备注
     */
    private String remark;
    /**
     * 岩性描述
     */
    @Excel(name = "岩性描述", orderNum = "9")
    private String rockdescription;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 熔岩流规模
     */
    @Excel(name = "熔岩流规模", orderNum = "13")
    private String scope;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 熔岩流单元划分
     */
    @Excel(name = "熔岩流单元划分", orderNum = "11")
    private String unit;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 照片原始文件编号
     */
    @Excel(name = "照片原始文件编号", orderNum = "18")
    private String photoArwid;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 乡
     */
    @Excel(name="详细地址",orderNum = "5")
    private String town;
    /**
     * 素描图图像编码
     */
    @Excel(name = "素描图图像编码", orderNum = "23")
    private String sketchAiid;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 熔岩流结构分带
     */
    @Excel(name = "熔岩流结构分带", orderNum = "10")
    private String structurezone;
    /**
     * 备选字段5
     */
    @Excel(name="区域",orderNum = "6")
    private String extends5;
    /**
     * 村
     */
    private String village;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 经度
     */
    private Double lon;
    /**
     * 维度
     */
    private Double lat;


    private String provinceName;
    private String cityName;
    private String areaName;
    private String rowNum;
    private String errorMsg;
}