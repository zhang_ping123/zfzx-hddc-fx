package com.css.zfzx.sjcj.modules.hddcEpiMechanismResult.repository;

import com.css.zfzx.sjcj.modules.hddcEpiMechanismResult.repository.entity.HddcEpimechanismresultEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-28
 */
public interface HddcEpimechanismresultRepository extends JpaRepository<HddcEpimechanismresultEntity, String> {
}
