package com.css.zfzx.sjcj.modules.hddcB7_VolSvyPtObservationObject.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSONObject;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcB7_VolSvyPtObservationObject.repository.HddcB7VolsvyptobservationobjectNativeRepository;
import com.css.zfzx.sjcj.modules.hddcB7_VolSvyPtObservationObject.repository.HddcB7VolsvyptobservationobjectRepository;
import com.css.zfzx.sjcj.modules.hddcB7_VolSvyPtObservationObject.repository.entity.HddcB7VolsvyptobservationobjectEntity;
import com.css.zfzx.sjcj.modules.hddcB7_VolSvyPtObservationObject.service.HddcB7VolsvyptobservationobjectService;
import com.css.zfzx.sjcj.modules.hddcB7_VolSvyPtObservationObject.viewobjects.HddcB7VolsvyptobservationobjectQueryParams;
import com.css.zfzx.sjcj.modules.hddcB7_VolSvyPtObservationObject.viewobjects.HddcB7VolsvyptobservationobjectVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-27
 */
@Service
public class HddcB7VolsvyptobservationobjectServiceImpl implements HddcB7VolsvyptobservationobjectService {

	@Autowired
    private HddcB7VolsvyptobservationobjectRepository hddcB7VolsvyptobservationobjectRepository;
    @Autowired
    private HddcB7VolsvyptobservationobjectNativeRepository hddcB7VolsvyptobservationobjectNativeRepository;

    @Override
    public JSONObject queryHddcB7Volsvyptobservationobjects(HddcB7VolsvyptobservationobjectQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcB7VolsvyptobservationobjectEntity> hddcB7VolsvyptobservationobjectPage = this.hddcB7VolsvyptobservationobjectNativeRepository.queryHddcB7Volsvyptobservationobjects(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcB7VolsvyptobservationobjectPage);
        return jsonObject;
    }


    @Override
    public HddcB7VolsvyptobservationobjectEntity getHddcB7Volsvyptobservationobject(String id) {
        HddcB7VolsvyptobservationobjectEntity hddcB7Volsvyptobservationobject = this.hddcB7VolsvyptobservationobjectRepository.findById(id).orElse(null);
         return hddcB7Volsvyptobservationobject;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB7VolsvyptobservationobjectEntity saveHddcB7Volsvyptobservationobject(HddcB7VolsvyptobservationobjectEntity hddcB7Volsvyptobservationobject) {
        String uuid = UUIDGenerator.getUUID();
        hddcB7Volsvyptobservationobject.setUuid(uuid);
        hddcB7Volsvyptobservationobject.setCreateUser(PlatformSessionUtils.getUserId());
        hddcB7Volsvyptobservationobject.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB7VolsvyptobservationobjectRepository.save(hddcB7Volsvyptobservationobject);
        return hddcB7Volsvyptobservationobject;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcB7VolsvyptobservationobjectEntity updateHddcB7Volsvyptobservationobject(HddcB7VolsvyptobservationobjectEntity hddcB7Volsvyptobservationobject) {
        HddcB7VolsvyptobservationobjectEntity entity = hddcB7VolsvyptobservationobjectRepository.findById(hddcB7Volsvyptobservationobject.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcB7Volsvyptobservationobject);
        hddcB7Volsvyptobservationobject.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcB7Volsvyptobservationobject.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcB7VolsvyptobservationobjectRepository.save(hddcB7Volsvyptobservationobject);
        return hddcB7Volsvyptobservationobject;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcB7Volsvyptobservationobjects(List<String> ids) {
        List<HddcB7VolsvyptobservationobjectEntity> hddcB7VolsvyptobservationobjectList = this.hddcB7VolsvyptobservationobjectRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcB7VolsvyptobservationobjectList) && hddcB7VolsvyptobservationobjectList.size() > 0) {
            for(HddcB7VolsvyptobservationobjectEntity hddcB7Volsvyptobservationobject : hddcB7VolsvyptobservationobjectList) {
                this.hddcB7VolsvyptobservationobjectRepository.delete(hddcB7Volsvyptobservationobject);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcB7VolsvyptobservationobjectQueryParams queryParams, HttpServletResponse response) {
        List<HddcB7VolsvyptobservationobjectEntity> yhDisasterEntities = hddcB7VolsvyptobservationobjectNativeRepository.exportYhDisasters(queryParams);
        List<HddcB7VolsvyptobservationobjectVO> list=new ArrayList<>();
        for (HddcB7VolsvyptobservationobjectEntity entity:yhDisasterEntities) {
            HddcB7VolsvyptobservationobjectVO yhDisasterVO=new HddcB7VolsvyptobservationobjectVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list,"火山调查观测点观测对象","火山调查观测点观测对象",HddcB7VolsvyptobservationobjectVO.class,"火山调查观测点观测对象.xls",response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcB7VolsvyptobservationobjectVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcB7VolsvyptobservationobjectVO.class, params);
            List<HddcB7VolsvyptobservationobjectVO> list = result.getList();
            // Excel条数据
            //int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcB7VolsvyptobservationobjectVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcB7VolsvyptobservationobjectVO yhDisasterVO = iterator.next();
                    String error = "";
                    /*returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");*/
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList(List<HddcB7VolsvyptobservationobjectVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcB7VolsvyptobservationobjectEntity yhDisasterEntity = new HddcB7VolsvyptobservationobjectEntity();
            HddcB7VolsvyptobservationobjectVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcB7Volsvyptobservationobject(yhDisasterEntity);
        }
    }

}
