package com.css.zfzx.sjcj.modules.hddcStratigraphy5LinePre.viewobjects;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 * @author zyb
 * @date 2020-11-27
 */
@Data
public class HddcStratigraphy5linepreVO implements Serializable {

    /**
     * 备选字段10
     */
    private String extends10;
    /**
     * 审核状态（保存）
     */
    private String reviewStatus;
    /**
     * 删除标识
     */
    private String isValid;
    /**
     * 备选字段16
     */
    private String extends16;
    /**
     * 备选字段20
     */
    private String extends20;
    /**
     * 备选字段6
     */
    private String extends6;
    /**
     * 备选字段3
     */
    private String extends3;
    /**
     * 备选字段24
     */
    private String extends24;
    /**
     * 备选字段12
     */
    private String extends12;
    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date updateTime;
    /**
     * 备选字段2
     */
    private String extends2;
    /**
     * 备选字段21
     */
    private String extends21;
    /**
     * 地质界线类型
     */
    @Excel(name = "地质界线类型", orderNum = "4")
    private Integer geologyboundaryline;
    /**
     * 质检人
     */
    private String qualityinspectionUser;
    /**
     * 备选字段13
     */
    private String extends13;
    /**
     * 区（县）
     */
    @Excel(name = "区（县）", orderNum = "3")
    private String area;
    /**
     * 市
     */
    @Excel(name = "市", orderNum = "2")
    private String city;
    /**
     * 备选字段14
     */
    private String extends14;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 备选字段18
     */
    private String extends18;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     * 备选字段27
     */
    private String extends27;
    /**
     * 乡
     */
    private String town;
    /**
     * 线编号
     */
    private String id;
    /**
     * 编号
     */
    private String objectCode;
    /**
     * 备选字段30
     */
    private String extends30;
    /**
     * 修改人
     */
    private String updateUser;
    /**
     * 项目ID
     */
    private String projectId;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 审查意见
     */
    private String examineComments;
    /**
     * 质检时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date qualityinspectionDate;
    /**
     * 备选字段26
     */
    private String extends26;
    /**
     * 备选字段28
     */
    private String extends28;
    /**
     * 备选字段19
     */
    private String extends19;
    /**
     * 备选字段5
     */
    private String extends5;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 备选字段23
     */
    private String extends23;
    /**
     * 备选字段25
     */
    private String extends25;
    /**
     * 备选字段22
     */
    private String extends22;
    /**
     * 省
     */
    @Excel(name = "省", orderNum = "1")
    private String province;
    /**
     * 备选字段8
     */
    private String extends8;
    /**
     * 备选字段11
     */
    private String extends11;
    /**
     * 备选字段29
     */
    private String extends29;
    /**
     * 备选字段15
     */
    private String extends15;
    /**
     * 备注
     */
    private String remark;
    /**
     * 审查人
     */
    private String examineUser;
    /**
     * 质检状态
     */
    private String qualityinspectionStatus;
    /**
     * 备注
     */
    @Excel(name = "备注", orderNum = "5")
    private String commentInfo;
    /**
     * 质检原因
     */
    private String qualityinspectionComments;
    /**
     * 备选字段7
     */
    private String extends7;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 审查时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date examineDate;
    /**
     * 备选字段9
     */
    private String extends9;
    /**
     * 备选字段4
     */
    private String extends4;
    /**
     * 备选字段1
     */
    private String extends1;
    /**
     * 分区标识
     */
    private Integer partionFlag;
    /**
     * 备选字段17
     */
    private String extends17;
    /**
     * 村
     */
    private String village;

    private String provinceName;
    private String cityName;
    private String areaName;
    private String geologyboundarylineName;
    private String rowNum;
    private String errorMsg;
}