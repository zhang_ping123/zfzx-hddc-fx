package com.css.zfzx.sjcj.modules.hddcGeologicalSvyLine.repository;

import com.css.zfzx.sjcj.modules.hddcGeologicalSvyLine.repository.entity.HddcGeologicalsvylineEntity;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyLine.viewobjects.HddcGeologicalsvylineQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author lihelei
 * @date 2020-11-27
 */
public interface HddcGeologicalsvylineNativeRepository {

    Page<HddcGeologicalsvylineEntity> queryHddcGeologicalsvylines(HddcGeologicalsvylineQueryParams queryParams, int curPage, int pageSize);

    List<HddcGeologicalsvylineEntity> exportYhDisasters(HddcGeologicalsvylineQueryParams queryParams);
}
