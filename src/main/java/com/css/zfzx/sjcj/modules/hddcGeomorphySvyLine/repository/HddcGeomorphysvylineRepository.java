package com.css.zfzx.sjcj.modules.hddcGeomorphySvyLine.repository;

import com.css.zfzx.sjcj.modules.hddcGeomorphySvyLine.repository.entity.HddcGeomorphysvylineEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zyb
 * @date 2020-11-27
 */
public interface HddcGeomorphysvylineRepository extends JpaRepository<HddcGeomorphysvylineEntity, String> {
}
