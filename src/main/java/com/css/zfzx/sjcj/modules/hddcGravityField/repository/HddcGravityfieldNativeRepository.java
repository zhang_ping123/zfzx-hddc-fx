package com.css.zfzx.sjcj.modules.hddcGravityField.repository;

import com.css.zfzx.sjcj.modules.hddcGravityField.repository.entity.HddcGravityfieldEntity;
import com.css.zfzx.sjcj.modules.hddcGravityField.viewobjects.HddcGravityfieldQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-26
 */
public interface HddcGravityfieldNativeRepository {

    Page<HddcGravityfieldEntity> queryHddcGravityfields(HddcGravityfieldQueryParams queryParams, int curPage, int pageSize);

    List<HddcGravityfieldEntity> exportYhDisasters(HddcGravityfieldQueryParams queryParams);
}
