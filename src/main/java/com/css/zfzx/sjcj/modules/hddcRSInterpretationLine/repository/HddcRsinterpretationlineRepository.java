package com.css.zfzx.sjcj.modules.hddcRSInterpretationLine.repository;

import com.css.zfzx.sjcj.modules.hddcRSInterpretationLine.repository.entity.HddcRsinterpretationlineEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhangping
 * @date 2020-11-30
 */
public interface HddcRsinterpretationlineRepository extends JpaRepository<HddcRsinterpretationlineEntity, String> {
}
