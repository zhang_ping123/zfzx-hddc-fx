package com.css.zfzx.sjcj.modules.hddcRelocationISCatalog.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.common.utils.EasyPoiExcelUtil;
import com.css.zfzx.sjcj.common.utils.UpdateUtil;
import com.css.zfzx.sjcj.modules.hddcRelocationISCatalog.repository.HddcRelocationiscatalogNativeRepository;
import com.css.zfzx.sjcj.modules.hddcRelocationISCatalog.repository.HddcRelocationiscatalogRepository;
import com.css.zfzx.sjcj.modules.hddcRelocationISCatalog.repository.entity.HddcRelocationiscatalogEntity;
import com.css.zfzx.sjcj.modules.hddcRelocationISCatalog.service.HddcRelocationiscatalogService;
import com.css.zfzx.sjcj.modules.hddcRelocationISCatalog.viewobjects.HddcRelocationiscatalogQueryParams;
import com.css.zfzx.sjcj.modules.hddcRelocationISCatalog.viewobjects.HddcRelocationiscatalogVO;
import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import com.css.bpm.platform.utils.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zyb
 * @date 2020-11-28
 */
@Service
public class HddcRelocationiscatalogServiceImpl implements HddcRelocationiscatalogService {

	@Autowired
    private HddcRelocationiscatalogRepository hddcRelocationiscatalogRepository;
    @Autowired
    private HddcRelocationiscatalogNativeRepository hddcRelocationiscatalogNativeRepository;

    @Override
    public JSONObject queryHddcRelocationiscatalogs(HddcRelocationiscatalogQueryParams queryParams, int curPage, int pageSize) {
        Page<HddcRelocationiscatalogEntity> hddcRelocationiscatalogPage = this.hddcRelocationiscatalogNativeRepository.queryHddcRelocationiscatalogs(queryParams, curPage, pageSize);
        JSONObject jsonObject = PlatformPageUtils.formatPageData(hddcRelocationiscatalogPage);
        return jsonObject;
    }


    @Override
    public HddcRelocationiscatalogEntity getHddcRelocationiscatalog(String id) {
        HddcRelocationiscatalogEntity hddcRelocationiscatalog = this.hddcRelocationiscatalogRepository.findById(id).orElse(null);
         return hddcRelocationiscatalog;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcRelocationiscatalogEntity saveHddcRelocationiscatalog(HddcRelocationiscatalogEntity hddcRelocationiscatalog) {
        String uuid = UUIDGenerator.getUUID();
        hddcRelocationiscatalog.setUuid(uuid);
        hddcRelocationiscatalog.setCreateUser(PlatformSessionUtils.getUserId());
        hddcRelocationiscatalog.setCreateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcRelocationiscatalogRepository.save(hddcRelocationiscatalog);
        return hddcRelocationiscatalog;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HddcRelocationiscatalogEntity updateHddcRelocationiscatalog(HddcRelocationiscatalogEntity hddcRelocationiscatalog) {
        HddcRelocationiscatalogEntity entity = hddcRelocationiscatalogRepository.findById(hddcRelocationiscatalog.getUuid()).get();
        UpdateUtil.copyNullProperties(entity,hddcRelocationiscatalog);
        hddcRelocationiscatalog.setUpdateUser(PlatformSessionUtils.getUserId());
        hddcRelocationiscatalog.setUpdateTime(PlatformDateUtils.getCurrentTimestamp());
        this.hddcRelocationiscatalogRepository.save(hddcRelocationiscatalog);
        return hddcRelocationiscatalog;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteHddcRelocationiscatalogs(List<String> ids) {
        List<HddcRelocationiscatalogEntity> hddcRelocationiscatalogList = this.hddcRelocationiscatalogRepository.findAllById(ids);
        if(!PlatformObjectUtils.isEmpty(hddcRelocationiscatalogList) && hddcRelocationiscatalogList.size() > 0) {
            for(HddcRelocationiscatalogEntity hddcRelocationiscatalog : hddcRelocationiscatalogList) {
                this.hddcRelocationiscatalogRepository.delete(hddcRelocationiscatalog);
            }
        }
    }

    @Override
    public List<DictItemEntity> getValidDictItemsByDictCode(String dictCode) {
        List<DictItemEntity> validDictItems = PlatformAPI.getDictAPI().getValidDictItemsByDictCode(dictCode);
        return validDictItems;
    }

    @Override
    public void exportFile(HddcRelocationiscatalogQueryParams queryParams, HttpServletResponse response) {
        List<HddcRelocationiscatalogEntity> yhDisasterEntities = hddcRelocationiscatalogNativeRepository.exportYhDisasters(queryParams);
        List<HddcRelocationiscatalogVO> list=new ArrayList<>();
        for (HddcRelocationiscatalogEntity entity:yhDisasterEntities) {
            HddcRelocationiscatalogVO yhDisasterVO=new HddcRelocationiscatalogVO();
            BeanUtils.copyProperties(entity,yhDisasterVO);
            list.add(yhDisasterVO);
        }
        EasyPoiExcelUtil.exportExcel(list, "小震重新定位目录-点", "小震重新定位目录-点", HddcRelocationiscatalogVO.class, "小震重新定位目录-点.xls", response);
    }

    @Override
    public String exportExcel(MultipartFile file, HttpServletResponse response) {
        try {
            // 返回的消息
            StringBuilder returnMsg = new StringBuilder();
            // 导入的参数信息
            ImportParams params = new ImportParams();
            // 设置简析的第一行
            params.setHeadRows(1);
            // 是否需要校验
            params.setNeedVerify(true);
            // 获取到Excel数据
            ExcelImportResult<HddcRelocationiscatalogVO> result = ExcelImportUtil.importExcelMore(file.getInputStream(), HddcRelocationiscatalogVO.class, params);
            List<HddcRelocationiscatalogVO> list = result.getList();
            // Excel条数据
            int firstList = list.size();
            StringBuilder sb = new StringBuilder();
            //保存
            saveDisasterList(list, sb);
            returnMsg.append("成功导入" + (result.getList().size() ));
            returnMsg.append(sb);
            if (result.isVerifyFail()) {
                // 校验返回失败行信息
                Iterator<HddcRelocationiscatalogVO> iterator = result.getFailList().iterator();
                while (iterator.hasNext()) {
                    HddcRelocationiscatalogVO yhDisasterVO = iterator.next();
                    String error = "";
                    returnMsg.append("第" + yhDisasterVO.getRowNum() + "行" + yhDisasterVO.getErrorMsg() + "<br/>");
                    returnMsg.append(error);
                }
            }
            return returnMsg.toString();
        } catch (Exception e) {

            return "导入失败，请检查数据正确性";
        }
    }
    /**
     * 批量存入库表并且返回错误数量
     *
     * @param list
     * @param message
     * @return
     */
    public void saveDisasterList( List<HddcRelocationiscatalogVO> list, StringBuilder message) {
        for (int i = 0; i < list.size(); i++) {
            HddcRelocationiscatalogEntity yhDisasterEntity = new HddcRelocationiscatalogEntity();
            HddcRelocationiscatalogVO yhDisasterVO = list.get(i);
            // 将VO数据映射拷贝到库表中
            BeanUtils.copyProperties(yhDisasterVO, yhDisasterEntity);
            saveHddcRelocationiscatalog(yhDisasterEntity);
        }
    }

}
