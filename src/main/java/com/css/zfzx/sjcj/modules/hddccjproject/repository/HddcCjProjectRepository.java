package com.css.zfzx.sjcj.modules.hddccjproject.repository;

import com.css.zfzx.sjcj.modules.hddccjproject.repository.entity.HddcCjProjectEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * @author zhangping
 * @date 2020-11-26
 */
public interface HddcCjProjectRepository extends JpaRepository<HddcCjProjectEntity, String> {

    @Query(nativeQuery = true,value = "select * from base.hddc_cj_project where person_ids like concat('%',:personid,'%')")
    List<HddcCjProjectEntity> findHddcCjProjectEntityByPersonId(@Param("personid") String personid);

}
