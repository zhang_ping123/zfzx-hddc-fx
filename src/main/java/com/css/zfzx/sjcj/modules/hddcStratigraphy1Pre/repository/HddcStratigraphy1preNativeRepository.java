package com.css.zfzx.sjcj.modules.hddcStratigraphy1Pre.repository;

import com.css.zfzx.sjcj.modules.hddcStratigraphy1Pre.repository.entity.HddcStratigraphy1preEntity;
import com.css.zfzx.sjcj.modules.hddcStratigraphy1Pre.viewobjects.HddcStratigraphy1preQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-28
 */
public interface HddcStratigraphy1preNativeRepository {

    Page<HddcStratigraphy1preEntity> queryHddcStratigraphy1pres(HddcStratigraphy1preQueryParams queryParams, int curPage, int pageSize);

    List<HddcStratigraphy1preEntity> exportYhDisasters(HddcStratigraphy1preQueryParams queryParams);
}
