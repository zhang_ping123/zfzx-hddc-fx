package com.css.zfzx.sjcj.modules.hddcGeologicalSvyLine.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyLine.repository.entity.HddcGeologicalsvylineEntity;
import com.css.zfzx.sjcj.modules.hddcGeologicalSvyLine.viewobjects.HddcGeologicalsvylineQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author lihelei
 * @date 2020-11-27
 */

public interface HddcGeologicalsvylineService {

    public JSONObject queryHddcGeologicalsvylines(HddcGeologicalsvylineQueryParams queryParams, int curPage, int pageSize);

    public HddcGeologicalsvylineEntity getHddcGeologicalsvyline(String id);

    public HddcGeologicalsvylineEntity saveHddcGeologicalsvyline(HddcGeologicalsvylineEntity hddcGeologicalsvyline);

    public HddcGeologicalsvylineEntity updateHddcGeologicalsvyline(HddcGeologicalsvylineEntity hddcGeologicalsvyline);

    public void deleteHddcGeologicalsvylines(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGeologicalsvylineQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
