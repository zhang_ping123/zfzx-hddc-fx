package com.css.zfzx.sjcj.modules.hddcB1TrenchBelongSamplePrj.repository;

import com.css.zfzx.sjcj.modules.hddcB1TrenchBelongSamplePrj.repository.entity.HddcB1TrenchbelongsampleprjEntity;
import com.css.zfzx.sjcj.modules.hddcB1TrenchBelongSamplePrj.viewobjects.HddcB1TrenchbelongsampleprjQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-30
 */
public interface HddcB1TrenchbelongsampleprjNativeRepository {

    Page<HddcB1TrenchbelongsampleprjEntity> queryHddcB1Trenchbelongsampleprjs(HddcB1TrenchbelongsampleprjQueryParams queryParams, int curPage, int pageSize);

    List<HddcB1TrenchbelongsampleprjEntity> exportYhDisasters(HddcB1TrenchbelongsampleprjQueryParams queryParams);
}
