package com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtTable.repository;

import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtTable.repository.entity.HddcB1FPaleoeqevttableEntity;
import com.css.zfzx.sjcj.modules.hddcB1FPaleoEQEvtTable.viewobjects.HddcB1FPaleoeqevttableQueryParams;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @author zyb
 * @date 2020-11-30
 */
public interface HddcB1FPaleoeqevttableNativeRepository {

    Page<HddcB1FPaleoeqevttableEntity> queryHddcB1FPaleoeqevttables(HddcB1FPaleoeqevttableQueryParams queryParams, int curPage, int pageSize);

    List<HddcB1FPaleoeqevttableEntity> exportYhDisasters(HddcB1FPaleoeqevttableQueryParams queryParams);
}
