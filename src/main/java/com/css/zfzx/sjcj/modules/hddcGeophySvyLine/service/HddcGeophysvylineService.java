package com.css.zfzx.sjcj.modules.hddcGeophySvyLine.service;

import com.alibaba.fastjson.JSONObject;
import com.css.zfzx.sjcj.modules.hddcGeophySvyLine.repository.entity.HddcGeophysvylineEntity;
import com.css.zfzx.sjcj.modules.hddcGeophySvyLine.viewobjects.HddcGeophysvylineQueryParams;
import com.css.bpm.platform.sys.dict.repository.entity.DictItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author zhangcong
 * @date 2020-11-28
 */

public interface HddcGeophysvylineService {

    public JSONObject queryHddcGeophysvylines(HddcGeophysvylineQueryParams queryParams, int curPage, int pageSize);

    public HddcGeophysvylineEntity getHddcGeophysvyline(String id);

    public HddcGeophysvylineEntity saveHddcGeophysvyline(HddcGeophysvylineEntity hddcGeophysvyline);

    public HddcGeophysvylineEntity updateHddcGeophysvyline(HddcGeophysvylineEntity hddcGeophysvyline);

    public void deleteHddcGeophysvylines(List<String> ids);

    List<DictItemEntity> getValidDictItemsByDictCode(String dictCode);

    void exportFile(HddcGeophysvylineQueryParams queryParams, HttpServletResponse response);

    String exportExcel(MultipartFile file, HttpServletResponse response);
}
