package com.css.zfzx.sjcj.cache;

import com.css.bpm.platform.utils.PlatformDateUtils;

import java.util.concurrent.atomic.AtomicInteger;

public class DivisionCache {
    // 加固工程自增
    private static AtomicInteger jgAtomicInteger = new AtomicInteger(1);
    private static AtomicInteger jgAtomicInteger2 = new AtomicInteger(1);


    /**
     * 项目编号
     *
     * @return
     */
    public synchronized static String reinforceXMCode(String areacode) {
        StringBuilder stringBuilder = new StringBuilder();
        String andAdd = jgAtomicInteger.getAndAdd(1) + "";
        /*stringBuilder.append(PlatformDateUtils.getCurrentDateStr().substring(0, 10).replace("-", ""));
        stringBuilder.append("XM00000", 0, 6 - andAdd.length());
        stringBuilder.append(andAdd);*/
        stringBuilder.append(areacode);
        stringBuilder.append(PlatformDateUtils.getCurrentDateStr().substring(0, 8).replace("-", ""));
        if(andAdd.length()==1){
            andAdd="000"+andAdd;
        }else if(andAdd.length()==2){
            andAdd="00"+andAdd;
        }else if(andAdd.length()==3){
            andAdd="0"+andAdd;
        }
        stringBuilder.append(andAdd);
        return stringBuilder.toString();
    }

    /**
     * 任务编号
     *
     * @return
     */
    public synchronized static String reinforceRWCode(String projectid) {
        StringBuilder stringBuilder = new StringBuilder();
        String andAdd = jgAtomicInteger2.getAndAdd(1) + "";
            /*stringBuilder.append(PlatformDateUtils.getCurrentDateStr().substring(0, 10).replace("-", ""));
            stringBuilder.append("RW00000", 0, 6 - andAdd.length());
            stringBuilder.append(andAdd);*/
        stringBuilder.append(projectid);
        stringBuilder.append(PlatformDateUtils.getCurrentDateStr().substring(0, 8).replace("-", ""));
        if(andAdd.length()==1){
            andAdd="000"+andAdd;
        }else if(andAdd.length()==2){
            andAdd="00"+andAdd;
        }else if(andAdd.length()==3){
            andAdd="0"+andAdd;
        }
        stringBuilder.append(andAdd);
        return stringBuilder.toString();
    }
}
