package com.css.zfzx.sjcj.common.utils;

/**
 * TODO
 *
 * @author 王昊杰
 * @version 1.0
 * @date 2021/3/9  16:14
 */


public class CheckObjFields {
    public static String propertyChange(String property) {
        StringBuilder sb=new StringBuilder();
        char[] chars = property.toCharArray();
        for (char c : chars) {
            if (Character.isUpperCase(c)){
                sb.append(("_"+c).toLowerCase());
                continue;
            }
            sb.append(c);
        }
        return sb.toString();
    }
}
