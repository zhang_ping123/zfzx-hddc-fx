package com.css.zfzx.sjcj.common.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DateUtils {
    /**
     * 获取时间最小值
     *
     * @param listDate
     * @return String yyyy年MM月dd日
     */
    public static String getMinTime(List<Date> listDate) {
        List<Long> list = new ArrayList<>();
        DateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
        for (Date date : listDate) {
            Long time = date.getTime();
            list.add(time);
        }
        Long min = Collections.min(list);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(min);
        return sdf.format(calendar.getTime());
    }

    /**
     * 获取时间最大值
     *
     * @param listDate
     * @return String yyyy年MM月dd日
     */
    public static String getMaxTime(List<Date> listDate) {
        List<Long> list = new ArrayList<>();
        DateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
        for (Date date : listDate) {
            Long time = date.getTime();
            list.add(time);
        }
        Long min = Collections.max(list);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(min);
        return sdf.format(calendar.getTime());
    }

    /**
     * 获取时间差天数
     *
     * @param startTime
     * @param endTime
     * @return
     */
    public static long getDay(String startTime, String endTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
        try {
            long start = sdf.parse(startTime).getTime();
            long end = sdf.parse(endTime).getTime();
            long dayCount = (end - start) / (24 * 3600 * 1000);
            return dayCount;
        } catch (ParseException e) {
            e.printStackTrace();
            return 0L;
        }
    }

    public static void main(String[] args) {
        String begin = "2020年06月30日";
        String end = "2020年07月03日";
        long day = getDay(begin, end);
        System.out.println(day);
    }
}
