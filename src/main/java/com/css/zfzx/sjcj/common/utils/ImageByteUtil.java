package com.css.zfzx.sjcj.common.utils;

import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.FileImageOutputStream;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author leon
 * @Title:
 * @Description: 图片与byte数组互转util
 * @date 2020/8/10 14:57
 */
public class ImageByteUtil {

    /**
     * 实现图片与byte数组之间的互相转换
     *
     * @param args
     */
    public static void main(String[] args) {
        //定义路径
        String path = "F:\\blank.jpg";
        byte[] data = image2byte(path);
        System.out.println(data.length);
    }

    /**
     * 将图片转换为byte数组
     *
     * @param path 图片路径
     * @return
     */
    public static byte[] image2byte(String path) {
        //定义byte数组
        byte[] data = null;
        //输入流
        FileImageInputStream input = null;
        try {
            input = new FileImageInputStream(new File(path));
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int numBytesRead = 0;
            while ((numBytesRead = input.read(buf)) != -1) {
                output.write(buf, 0, numBytesRead);
            }
            data = output.toByteArray();
            output.close();
            input.close();
        } catch (FileNotFoundException ex1) {
            ex1.printStackTrace();
        } catch (IOException ex1) {
            ex1.printStackTrace();
        }
        return data;
    }

    //byte数组到图片
    public void byte2image(byte[] data, String path) {
        if (data.length < 3 || path.equals("")) return;
        try {
            FileImageOutputStream imageOutput = new FileImageOutputStream(new File(path));
            imageOutput.write(data, 0, data.length);
            imageOutput.close();
            System.out.println("Make Picture success,Please find image in " + path);
        } catch (Exception ex) {
            System.out.println("Exception: " + ex);
            ex.printStackTrace();
        }
    }

    //byte数组到16进制字符串
    public String byte2string(byte[] data) {
        if (data == null || data.length <= 1) return "0x";
        if (data.length > 200000) return "0x";
        StringBuffer sb = new StringBuffer();
        int buf[] = new int[data.length];
        //byte数组转化成十进制
        for (int k = 0; k < data.length; k++) {
            buf[k] = data[k] < 0 ? (data[k] + 256) : (data[k]);
        }
        //十进制转化成十六进制
        for (int k = 0; k < buf.length; k++) {
            if (buf[k] < 16) sb.append("0" + Integer.toHexString(buf[k]));
            else sb.append(Integer.toHexString(buf[k]));
        }
        return "0x" + sb.toString().toUpperCase();
    }


    /**
     * 导出图片
     *
     * @param request
     * @param response
     */
    @RequestMapping("/exportPicture")
    public void exportPicture(HttpServletRequest request, HttpServletResponse response) throws Exception {
        // 定义根路径
        String rootPath = request.getServletContext().getRealPath(File.separator);
        // 创建文件
        File file = new File(rootPath + "temp_download");
        // 判断文件是否存在，如果不存在，则创建此文件夹
        if (!file.exists()) {
            file.mkdir();
        }
        String name = "图片压缩包下载";
        String fileName = name + new Date().getTime();
        String zipFileName = fileName + ".zip";
        File zipFile = null;
        String path = rootPath + "temp_download";

        //调用工具类获取图片
        byte[] data = ImageByteUtil.image2byte("F:\\blank.jpg");
        //new一个文件对象用来保存图片，默认保存当前工程根目录
        if (data != null) {
            File imageFile = new File(path + File.separator + fileName + ".jpg");
            //创建输出流
            FileOutputStream outStream = new FileOutputStream(imageFile);
            //写入数据
            outStream.write(data);
            //关闭输出流
            outStream.close();
        }
        try {
            //获取创建好的图片文件
            File imageFile = new File(path + "/" + fileName + ".jpg");
            // 打成压缩包
            zipFile = new File(path + "/" + zipFileName);
            FileOutputStream zipFos = new FileOutputStream(zipFile);
            ArchiveOutputStream archOut = new ArchiveStreamFactory().createArchiveOutputStream(ArchiveStreamFactory.ZIP, zipFos);
            if (archOut instanceof ZipArchiveOutputStream) {
                ZipArchiveOutputStream zos = (ZipArchiveOutputStream) archOut;
                ZipArchiveEntry zipEntry = new ZipArchiveEntry(imageFile, imageFile.getName());
                zos.putArchiveEntry(zipEntry);
                zos.write(FileUtils.readFileToByteArray(imageFile));
                zos.closeArchiveEntry();
                zos.flush();
                zos.close();
            }
            // 压缩完删除txt文件
            if (imageFile.exists()) {
                imageFile.delete();
            }
            // 输出到客户端
            OutputStream out = null;
            out = response.getOutputStream();
            response.reset();
            response.setHeader("Content-Disposition", "attachment;filename=" + new String(zipFileName.getBytes("GB2312"), "ISO-8859-1"));
            response.setContentType("application/octet-stream; charset=utf-8");
            response.setCharacterEncoding("UTF-8");
            out.write(FileUtils.readFileToByteArray(zipFile));
            out.flush();
            out.close();

            // 输出客户端结束后，删除压缩包
            if (zipFile.exists()) {
                zipFile.delete();
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ArchiveException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    /**
     * 将文件写入到zip文件中
     *
     * @param inputFile
     * @param outputstream
     * @throws Exception
     */
    public static void zipFile(File inputFile, ZipOutputStream outputstream) throws IOException, ServletException {
        try {
            if (inputFile.exists()) {
                if (inputFile.isFile()) {
                    FileInputStream inStream = new FileInputStream(inputFile);
                    BufferedInputStream bInStream = new BufferedInputStream(inStream);
                    ZipEntry entry = new ZipEntry(inputFile.getName());
                    outputstream.putNextEntry(entry);

                    final int MAX_BYTE = 10 * 1024 * 1024; // 最大的流为10M
                    long streamTotal = 0; // 接受流的容量
                    int streamNum = 0; // 流需要分开的数量
                    int leaveByte = 0; // 文件剩下的字符数
                    byte[] inOutbyte; // byte数组接受文件的数据

                    streamTotal = bInStream.available(); // 通过available方法取得流的最大字符数
                    streamNum = (int) Math.floor(streamTotal / MAX_BYTE); // 取得流文件需要分开的数量
                    leaveByte = (int) streamTotal % MAX_BYTE; // 分开文件之后,剩余的数量

                    if (streamNum > 0) {
                        for (int j = 0; j < streamNum; ++j) {
                            inOutbyte = new byte[MAX_BYTE];
                            // 读入流,保存在byte数组
                            bInStream.read(inOutbyte, 0, MAX_BYTE);
                            outputstream.write(inOutbyte, 0, MAX_BYTE); // 写出流
                        }
                    }
                    // 写出剩下的流数据
                    inOutbyte = new byte[leaveByte];
                    bInStream.read(inOutbyte, 0, leaveByte);
                    outputstream.write(inOutbyte);
                    outputstream.closeEntry();
                    bInStream.close(); // 关闭
                    inStream.close();
                }
            } else {
                throw new ServletException("文件不存在！");
            }
        } catch (IOException e) {
            throw e;
        }
    }


}
