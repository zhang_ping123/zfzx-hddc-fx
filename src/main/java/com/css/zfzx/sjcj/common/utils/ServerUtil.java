package com.css.zfzx.sjcj.common.utils;

import com.css.bpm.platform.org.role.repository.entity.RoleEntity;

import java.util.List;

public class ServerUtil {

    /**
     * 判断某用户是否存在角色
     *
     * @param roleEntities
     * @param code
     * @return
     */
    public static boolean isContaintRole(List<RoleEntity> roleEntities, String code) {
        boolean flag = false;
        for (RoleEntity role : roleEntities) {
            if (code.equals(role.getRoleCode())) {
                flag = true;
                break;
            }
        }
        if (flag) {
            return true;
        } else {
            return false;
        }
    }
}
