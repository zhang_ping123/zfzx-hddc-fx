package com.css.zfzx.sjcj.common.division;

import com.css.bpm.platform.api.local.PlatformAPI;
import com.css.bpm.platform.base.response.RestResponse;
import com.css.bpm.platform.org.division.repository.entity.DivisionEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author leon
 * @Title:
 * @Description:
 * @date 2020/11/4 16:20
 */
@RestController
@Slf4j
public class Division {
    /**
     * 行政区划
     *
     * @return
     */
    @GetMapping("divisions/{divisionid}/subdivisions")
    public RestResponse getDisvion(@PathVariable("divisionid") String divisionId) {
        RestResponse restResponse = null;
        try {
            List<DivisionEntity> subDivisions = PlatformAPI.getDivisionAPI().getSubDivisions(divisionId);
            restResponse = RestResponse.succeed(subDivisions);
        } catch (Exception e) {
            String errormessage = "查询失败";
            log.error(errormessage, e);
            restResponse = RestResponse.fail(errormessage);
        }
        return restResponse;
    }
}
