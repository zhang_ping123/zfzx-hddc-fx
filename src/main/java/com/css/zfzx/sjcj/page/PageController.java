package com.css.zfzx.sjcj.page;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class PageController {

    @RequestMapping({"/platform/register"})
    public String home(HttpServletRequest request) throws Exception {

        return "core/platform/page/register/register";
    }

    @RequestMapping({"/platform/login"})
    public String login(HttpServletRequest request) throws Exception {

        return "core/platform/page/login/login";
    }
}
