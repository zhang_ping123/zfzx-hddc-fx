/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50624
 Source Host           : localhost:3306
 Source Schema         : base

 Target Server Type    : MySQL
 Target Server Version : 50624
 File Encoding         : 65001

 Date: 30/10/2020 17:56:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for dev_directory
-- ----------------------------
DROP TABLE IF EXISTS `dev_directory`;
CREATE TABLE `dev_directory`  (
  `dir_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键，UUID',
  `dir_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类编码，唯一',
  `dir_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类名称',
  `parent_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父分类ID',
  `all_parent_id` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类全路径，层级码',
  `sort` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `description` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`dir_id`) USING BTREE,
  UNIQUE INDEX `idx_dev_directory_code`(`dir_code`) USING BTREE,
  INDEX `idx_dev_directory_sort`(`sort`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '模块分类表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dev_directory
-- ----------------------------
INSERT INTO `dev_directory` VALUES ('root', 'root', '中国软件与技术服务股份有限公司', '', 'root', 1, '根目录', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-12 06:17:31', NULL, NULL);

-- ----------------------------
-- Table structure for dev_generator_config
-- ----------------------------
DROP TABLE IF EXISTS `dev_generator_config`;
CREATE TABLE `dev_generator_config`  (
  `config_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '配置id,主键,uuid',
  `dir_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类ID',
  `config_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '配置编码，唯一',
  `config_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '配置名称',
  `template_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板编码',
  `menu_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单id',
  `config` blob NULL COMMENT 'json配置',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `generate_time` datetime(0) NULL DEFAULT NULL COMMENT '生成时间',
  PRIMARY KEY (`config_id`) USING BTREE,
  UNIQUE INDEX `idx_dev_generator_config_code`(`config_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '模块配置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for dev_meta_column
-- ----------------------------
DROP TABLE IF EXISTS `dev_meta_column`;
CREATE TABLE `dev_meta_column`  (
  `column_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键，UUID，列ID',
  `table_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '元数据表ID',
  `db_field_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据库字段名称',
  `db_field_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据库字段类型',
  `db_field_length` int(11) NOT NULL COMMENT '数据库字段长度',
  `is_primary_key` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否主键，1:是，0:否',
  `is_nullable` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否可空，1:是,0:否',
  `java_field_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'java字段名称',
  `java_field_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'java字段类型',
  `java_field_comment` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段备注',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `status` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '元数据列' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for dev_meta_column_extend
-- ----------------------------
DROP TABLE IF EXISTS `dev_meta_column_extend`;
CREATE TABLE `dev_meta_column_extend`  (
  `column_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键,UUID',
  `foreign_table_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '关联表ID',
  `foreign_column_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '关联字段ID',
  `foreign_type` varchar(7) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '关联类型，related:关联表，sub:主子表',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `custom_config` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展类型',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '元数据列扩展表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for dev_meta_table
-- ----------------------------
DROP TABLE IF EXISTS `dev_meta_table`;
CREATE TABLE `dev_meta_table`  (
  `table_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键，元数据表ID',
  `table_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '表名',
  `db_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据库名',
  `java_class_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Java类名',
  `table_comment` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表备注',
  `dir_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '目录ID',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '元数据表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for org_certification
-- ----------------------------
DROP TABLE IF EXISTS `org_certification`;
CREATE TABLE `org_certification`  (
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `pwd` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `salt` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '盐',
  `locked` varchar(17) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '锁定状态。inactive：未激活，normal:正常,too_many_failures:登录失败次数过多锁定',
  `login_fail_num` int(11) NULL DEFAULT 0 COMMENT '登录失败次数',
  `login_fail_time` datetime(0) NULL DEFAULT NULL COMMENT '登录失败时间',
  `is_default_pwd` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否默认密码,0:否,1:是',
  `pwd_update_time` datetime(0) NULL DEFAULT NULL COMMENT '密码时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户认证信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of org_certification
-- ----------------------------
INSERT INTO `org_certification` VALUES ('3951bd7ab34f4cafa230c3c674bacd72', '$s0$b0101$ujJDp3dhF31K4Tq+k8WUPg==$wGmFnbsN0Xihbsf/BHAlqV2CBQ2ZuemCVR7KCPAaQkY=', '4cb94c9efbb848a783833adc5c870e97', 'NORMAL', 0, NULL, NULL, '2020-05-18 16:36:33', '3951bd7ab34f4cafa230c3c674bacd72', '2020-05-18 18:14:28');
INSERT INTO `org_certification` VALUES ('8db57604a1e44451850249ad34099263', '$s0$b0101$V8FRTdxLWuKMBr1ex/ZVnA==$hw90JqliGPjD5O8BmHFZ+0BTU3iAcpSRX98NMpSxbpw=', '8ef1002e564f43989789f3937ff25ef5', 'NORMAL', 0, NULL, NULL, '2020-05-18 19:44:31', '8db57604a1e44451850249ad34099263', '2020-05-18 19:54:00');
INSERT INTO `org_certification` VALUES ('c8f1ba6c7cf842409aba43206e9f7442', '$s0$b0101$EMzFbqiBcI1Za6sRX3EJzQ==$godwYZxeYo6g32iqtzm8IX7JpDvAb3dlZS3BUCz87UM=', '2b5358d32ed74f008057b7b495d2fdc7', 'NORMAL', 0, NULL, '1', '2020-05-18 16:36:33', NULL, NULL);
INSERT INTO `org_certification` VALUES ('edfac0350de749da8ee149f60551b9d3', '$s0$b0101$o1raRcUn7kduFlhfq1x3DA==$w2sofGTgMmAx7VIdEyMpk+npv7iWzYtTpYaNqalEzR0=', 'd8f7008fc7a14958b8db2ee54f09b49c', 'NORMAL', 0, NULL, NULL, '2020-05-18 18:25:41', 'edfac0350de749da8ee149f60551b9d3', '2020-05-18 18:25:41');

-- ----------------------------
-- Table structure for org_certification_history
-- ----------------------------
DROP TABLE IF EXISTS `org_certification_history`;
CREATE TABLE `org_certification_history`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '历史密码id',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `pwd` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '历史密码',
  `salt` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '盐',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_org_history_certification`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '历史密码表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of org_certification_history
-- ----------------------------
INSERT INTO `org_certification_history` VALUES ('18684750a6da47bfa100528d967dd646', 'c8f1ba6c7cf842409aba43206e9f7442', '$s0$b0101$EMzFbqiBcI1Za6sRX3EJzQ==$godwYZxeYo6g32iqtzm8IX7JpDvAb3dlZS3BUCz87UM=', '2b5358d32ed74f008057b7b495d2fdc7', '2020-10-30 17:50:18');
INSERT INTO `org_certification_history` VALUES ('43284750a6da47bfa100528d967dd969', '3951bd7ab34f4cafa230c3c674bacd72', '$s0$b0101$ujJDp3dhF31K4Tq+k8WUPg==$wGmFnbsN0Xihbsf/BHAlqV2CBQ2ZuemCVR7KCPAaQkY=', '4cb94c9efbb848a783833adc5c870e97', '2020-10-30 17:50:18');
INSERT INTO `org_certification_history` VALUES ('ef04ed6c6c1247f59cce43fb87939a39', '8db57604a1e44451850249ad34099263', '$s0$b0101$V8FRTdxLWuKMBr1ex/ZVnA==$hw90JqliGPjD5O8BmHFZ+0BTU3iAcpSRX98NMpSxbpw=', '8ef1002e564f43989789f3937ff25ef5', '2020-10-30 17:50:18');
INSERT INTO `org_certification_history` VALUES ('f55fa2ac18764fe6b992f0cc2a2092fb', 'edfac0350de749da8ee149f60551b9d3', '$s0$b0101$o1raRcUn7kduFlhfq1x3DA==$w2sofGTgMmAx7VIdEyMpk+npv7iWzYtTpYaNqalEzR0=', 'd8f7008fc7a14958b8db2ee54f09b49c', '2020-10-30 17:50:18');

-- ----------------------------
-- Table structure for org_config
-- ----------------------------
DROP TABLE IF EXISTS `org_config`;
CREATE TABLE `org_config`  (
  `id` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键,root',
  `org_extend_config` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '机构扩展字段配置',
  `dept_extend_config` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '部门扩展字段配置',
  `user_extend_config` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '人员扩展字段配置',
  `create_user` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '组织用户扩展信息配置' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for org_dept
-- ----------------------------
DROP TABLE IF EXISTS `org_dept`;
CREATE TABLE `org_dept`  (
  `dept_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组织id，主键，uuid',
  `dept_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组织编码，逻辑唯一',
  `dept_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组织名称',
  `leader_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组织负责人id',
  `parent_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上级组织id',
  `all_parent_id` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组织全路径，层次码',
  `dept_level` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门等级',
  `sort` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `dept_type` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组织类型：org：机构，dept：部门',
  `division_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '行政区划，机构属性',
  `telephone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话，机构属性',
  `address` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址，机构属性',
  `zip_code` char(6) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮编，机构属性',
  `depth` int(11) NULL DEFAULT NULL COMMENT '组织机构等级数',
  `extend1` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段1',
  `extend2` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段2',
  `extend3` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段3',
  `extend4` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段4',
  `extend5` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段5',
  `extend6` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段6',
  `extend7` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段7',
  `extend8` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段8',
  `extend9` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段9',
  `extend10` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段10',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`dept_id`) USING BTREE,
  UNIQUE INDEX `idx_org_dept_code`(`dept_code`) USING BTREE,
  INDEX `idx_org_dept_pid`(`parent_id`) USING BTREE,
  INDEX `idx_org_dept_sort`(`sort`) USING BTREE,
  FULLTEXT INDEX `idx_org_dept_allpid`(`all_parent_id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '组织机构表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of org_dept
-- ----------------------------
INSERT INTO `org_dept` VALUES ('root', 'root', '中国软件与技术服务股份有限公司', NULL, NULL, 'root', NULL, 1, 'org', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-21 13:49:26', NULL, NULL);

-- ----------------------------
-- Table structure for org_dept_gw
-- ----------------------------
DROP TABLE IF EXISTS `org_dept_gw`;
CREATE TABLE `org_dept_gw`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门岗位id，主键，uuid',
  `dept_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门id',
  `gw_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位id',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_org_dept_gw_dept_id`(`dept_id`) USING BTREE,
  INDEX `idx_org_dept_gw_gw_id`(`gw_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门岗位表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for org_dept_gw_user
-- ----------------------------
DROP TABLE IF EXISTS `org_dept_gw_user`;
CREATE TABLE `org_dept_gw_user`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门岗位用户id，主键，uuid',
  `dept_gw_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门岗位id',
  `dept_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门id',
  `gw_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位id',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_org_dept_gw_user_dept_gw_id`(`dept_gw_id`) USING BTREE,
  INDEX `idx_org_dept_gw_user_dept_id`(`dept_id`) USING BTREE,
  INDEX `idx_org_dept_gw_user_gw_id`(`gw_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门岗位用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for org_dept_user
-- ----------------------------
DROP TABLE IF EXISTS `org_dept_user`;
CREATE TABLE `org_dept_user`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组织用户id，主键，uuid',
  `dept_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门id',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `is_main` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否主职,0:兼职,1:主职',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_org_dept_user_dept_id`(`dept_id`) USING BTREE,
  INDEX `idx_org_dept_user_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '组织用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of org_dept_user
-- ----------------------------
INSERT INTO `org_dept_user` VALUES ('04f1ee3ce5454ae8b185a2621c30789e', 'root', 'c8f1ba6c7cf842409aba43206e9f7442', '1', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-18 20:27:34', NULL, NULL);
INSERT INTO `org_dept_user` VALUES ('295e3d49059f47538f11ee8c21cad6e5', 'root', 'edfac0350de749da8ee149f60551b9d3', '1', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-19 14:38:46', NULL, NULL);

-- ----------------------------
-- Table structure for org_division
-- ----------------------------
DROP TABLE IF EXISTS `org_division`;
CREATE TABLE `org_division`  (
  `division_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '区划id',
  `division_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `division_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '区划名称',
  `parent_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上级区划',
  `all_parent_id` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `division_level` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '0:国家,1:省级,2:地级,3:县级,4:乡级,5:村级',
  `division_type` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Province:省，Municipality：直辖市, Autonomous Region:自治区，Special Administrative Region：特别行政区,Prefecture-level City 地级市,Prefecture 地区,Autonomous Prefecture 自治州,League 盟,District 市辖区,County 县 ,Autonomous County 自治县,County-level City 县级市,Banner 旗,Autonomous Banner,自治旗,Forestry Area 林区,Special District 特区,Town 镇,Township 乡,Ethnic Township 民族乡,Subdistrict 街道,Sumu 苏木,Ethnic Sumu 民族苏木 ,District Public Office 区公所,Administrative Village 行政村，Community 社区，House 居 ，Gacha 嘎查',
  `region` char(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '地区,NC:华北,NE:东北,EC:华东,CC:华中,SC:华南,SW:西南,NW:西北',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`division_id`) USING BTREE,
  UNIQUE INDEX `idx_org_div_code`(`division_code`) USING BTREE,
  INDEX `idx_org_div_pid`(`parent_id`) USING BTREE,
  FULLTEXT INDEX `idx_org_div_allpid`(`all_parent_id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '行政区划表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of org_division
-- ----------------------------
INSERT INTO `org_division` VALUES ('root', 'root', '中华人民共和国', '', 'root', '0', 'Country', '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:17', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:17');

-- ----------------------------
-- Table structure for org_gw
-- ----------------------------
DROP TABLE IF EXISTS `org_gw`;
CREATE TABLE `org_gw`  (
  `gw_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位id，主键，uuid',
  `gw_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位编码，逻辑唯一',
  `gw_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称',
  `sort` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `description` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`gw_id`) USING BTREE,
  UNIQUE INDEX `idx_org_gw_code`(`gw_code`) USING BTREE,
  INDEX `idx_org_gw_sort`(`sort`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for org_private_group_dept
-- ----------------------------
DROP TABLE IF EXISTS `org_private_group_dept`;
CREATE TABLE `org_private_group_dept`  (
  `group_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '群组id',
  `group_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '群组编码',
  `group_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '群组名称',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `sort` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `description` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`group_id`) USING BTREE,
  INDEX `idx_org_private_group_dept_group_code`(`group_code`) USING BTREE,
  INDEX `idx_org_private_group_dept_sort`(`sort`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '个人部门群组表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for org_private_group_dept_item
-- ----------------------------
DROP TABLE IF EXISTS `org_private_group_dept_item`;
CREATE TABLE `org_private_group_dept_item`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `group_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '群组id',
  `dept_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组织id',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_org_private_group_dept_item_group_id`(`group_id`) USING BTREE,
  INDEX `idx_org_private_group_dept_item_dept_id`(`dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '个人部门群组明细表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for org_private_group_user
-- ----------------------------
DROP TABLE IF EXISTS `org_private_group_user`;
CREATE TABLE `org_private_group_user`  (
  `group_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '群组id',
  `group_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '群组编码',
  `group_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '群组名称',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `sort` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `description` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`group_id`) USING BTREE,
  INDEX `idx_org_private_group_user_group_code`(`group_code`) USING BTREE,
  INDEX `idx_org_private_group_user_sort`(`sort`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '个人用户群组表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for org_private_group_user_item
-- ----------------------------
DROP TABLE IF EXISTS `org_private_group_user_item`;
CREATE TABLE `org_private_group_user_item`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `group_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '群组id',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_org_private_group_user_item_group_id`(`group_id`) USING BTREE,
  INDEX `idx_org_private_group_user_item_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '个人用户群组明细表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for org_public_group_dept
-- ----------------------------
DROP TABLE IF EXISTS `org_public_group_dept`;
CREATE TABLE `org_public_group_dept`  (
  `group_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '群组id',
  `group_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '群组编码',
  `group_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '群组名称',
  `sort` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `description` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`group_id`) USING BTREE,
  UNIQUE INDEX `idx_org_public_group_dept_group_code`(`group_code`) USING BTREE,
  INDEX `idx_org_public_group_dept_sort`(`sort`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '公共部门群组表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for org_public_group_dept_item
-- ----------------------------
DROP TABLE IF EXISTS `org_public_group_dept_item`;
CREATE TABLE `org_public_group_dept_item`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `group_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '群组id',
  `dept_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组织id',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_org_public_group_dept_item_group_id`(`group_id`) USING BTREE,
  INDEX `idx_org_public_group_dept_item_dept_id`(`dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '公共部门群组明细表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for org_public_group_user
-- ----------------------------
DROP TABLE IF EXISTS `org_public_group_user`;
CREATE TABLE `org_public_group_user`  (
  `group_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '群组id',
  `group_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '群组编码',
  `group_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '群组名称',
  `sort` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `description` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`group_id`) USING BTREE,
  UNIQUE INDEX `idx_org_public_group_user_group_code`(`group_code`) USING BTREE,
  INDEX `idx_org_public_group_user_sort`(`sort`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '公共用户群组表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for org_public_group_user_item
-- ----------------------------
DROP TABLE IF EXISTS `org_public_group_user_item`;
CREATE TABLE `org_public_group_user_item`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `group_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '群组id',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_org_public_group_user_item_group_id`(`group_id`) USING BTREE,
  INDEX `idx_org_public_group_user_item_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '公共用户群组明细表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for org_role
-- ----------------------------
DROP TABLE IF EXISTS `org_role`;
CREATE TABLE `org_role`  (
  `role_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色id，主键，uuid',
  `role_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色编码，唯一',
  `role_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `sort` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `description` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `app_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '应用系统id',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`role_id`) USING BTREE,
  UNIQUE INDEX `idx_org_role_code`(`role_code`) USING BTREE,
  INDEX `idx_org_role_sort`(`sort`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of org_role
-- ----------------------------
INSERT INTO `org_role` VALUES ('3c282d0211b54952ac6294a6a34ded31', 'sysadmin', '系统管理员', NULL, '', '1', 'root', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 14:48:51', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:33:20');
INSERT INTO `org_role` VALUES ('6b20b8cf0a2a4ffc88798b9a4d0d4b3f', 'secadmin', '安全管理员', NULL, '', '1', 'root', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 14:58:45', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:35:07');
INSERT INTO `org_role` VALUES ('8ea96d5614a84aab92b9e2adfa044462', 'auditadmin', '审计管理员', NULL, '', '1', 'root', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 15:02:05', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:12:36');
INSERT INTO `org_role` VALUES ('f895299310764dcf9c3afec8ce6fb39d', 'root', 'supersysadmin', NULL, '', '1', 'root', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-13 20:26:30', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-13 20:27:06');

-- ----------------------------
-- Table structure for org_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `org_role_menu`;
CREATE TABLE `org_role_menu`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色菜单id，主键，uuid',
  `role_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色id',
  `menu_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单id',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_org_role_menu_role_id`(`role_id`) USING BTREE,
  INDEX `idx_org_role_menu_menu_id`(`menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色菜单表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of org_role_menu
-- ----------------------------
INSERT INTO `org_role_menu` VALUES ('0a149e073d8e4776a70ef734fce77869', '6b20b8cf0a2a4ffc88798b9a4d0d4b3f', '9eb76da7c98e40709c916ec8c27e9c49', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 15:01:01', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:35:07');
INSERT INTO `org_role_menu` VALUES ('1f0c99df1f6d48349801f96ec847606a', '3c282d0211b54952ac6294a6a34ded31', '7c9cd13b0b1648e887df3b007020db81', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 14:54:54', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:33:20');
INSERT INTO `org_role_menu` VALUES ('271afdbd114a494b89aca319fbb41712', '6b20b8cf0a2a4ffc88798b9a4d0d4b3f', '6e9bb9dc49d543369055dd38f39c25f8', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 15:01:01', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:35:07');
INSERT INTO `org_role_menu` VALUES ('44826a72f976456fba409d35d15b866e', '6b20b8cf0a2a4ffc88798b9a4d0d4b3f', '512277186ff1439cbe87f441a6d7931b', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 15:01:01', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:35:07');
INSERT INTO `org_role_menu` VALUES ('7a98720b9239414599d038b65ca4f183', '3c282d0211b54952ac6294a6a34ded31', '4924e8c0669f40d0854a62fc7b6b92a8', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 14:54:54', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:33:20');
INSERT INTO `org_role_menu` VALUES ('7cc0e5a1cf834f39bbf9c8715c6c57d5', '3c282d0211b54952ac6294a6a34ded31', '85d66613efe44648be50874f10b67c2e', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 14:54:54', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:33:20');
INSERT INTO `org_role_menu` VALUES ('80e71f261fc74b5db2360a0a426612f2', '3c282d0211b54952ac6294a6a34ded31', 'e583a9838da34bcd8a72c959d0b05abf', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 14:56:13', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:33:20');
INSERT INTO `org_role_menu` VALUES ('83d618870c794a9299ecf4c8721c4d64', '6b20b8cf0a2a4ffc88798b9a4d0d4b3f', 'e65eba4dbedc4408a4810ccda457902a', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 15:01:01', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:35:07');
INSERT INTO `org_role_menu` VALUES ('84fdf339f2c649ffb3393014c7563b9d', '6b20b8cf0a2a4ffc88798b9a4d0d4b3f', '58910294425a4dd2b85ec949d7d838b9', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 15:01:01', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:35:07');
INSERT INTO `org_role_menu` VALUES ('877c5fa4bf0946aca8a4daf6827fa9e8', '8ea96d5614a84aab92b9e2adfa044462', 'ba42b8d79e89428db49ffea6350656f7', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-18 10:16:55', NULL, NULL);
INSERT INTO `org_role_menu` VALUES ('8a008bdeb57045bdb4a50f154a687b76', '3c282d0211b54952ac6294a6a34ded31', 'root', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 14:54:54', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:33:20');
INSERT INTO `org_role_menu` VALUES ('8b88c1cdbe334bb8928a6c11ea791c53', '6b20b8cf0a2a4ffc88798b9a4d0d4b3f', 'root', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 15:01:01', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:35:07');
INSERT INTO `org_role_menu` VALUES ('8f90863ece604caca175306768294b0a', '3c282d0211b54952ac6294a6a34ded31', '98ecdfb17e4d47768ac9b92f42e9e6b6', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 14:54:54', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:33:20');
INSERT INTO `org_role_menu` VALUES ('92ca4dbc3a2b4d259d1643eeda817d90', '8ea96d5614a84aab92b9e2adfa044462', 'b13c2feb59ba4b6094bfee2929deba3c', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 15:03:08', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-18 10:16:55');
INSERT INTO `org_role_menu` VALUES ('93f11a5e615446a58deb3d3eedf5b9a0', '3c282d0211b54952ac6294a6a34ded31', 'c9d971c3ce21455c98ddc2acfd4ae277', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 14:54:54', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:33:20');
INSERT INTO `org_role_menu` VALUES ('9c1ef96bc29d45268a89e7d2d11a8dea', '6b20b8cf0a2a4ffc88798b9a4d0d4b3f', '6cf47d23184944819dda7d6240ba3a04', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 15:01:01', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:35:07');
INSERT INTO `org_role_menu` VALUES ('9db08779c90a4bd9911b8f467beb4e01', '6b20b8cf0a2a4ffc88798b9a4d0d4b3f', 'cbe91853c89442e186a46a99ff624c6d', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 15:01:01', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:35:07');
INSERT INTO `org_role_menu` VALUES ('9f4c0d272add4ced980a1e8c9879b78b', '3c282d0211b54952ac6294a6a34ded31', '319bc75511704b3f994a1e5cc53543de', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 14:54:54', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:33:20');
INSERT INTO `org_role_menu` VALUES ('a8dfc46912e04e74b7cd7e56dd8bb266', '3c282d0211b54952ac6294a6a34ded31', '7e0a77ef7c8b4a878bcdc5edacb405fc', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 14:54:54', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:33:20');
INSERT INTO `org_role_menu` VALUES ('bce8c5c01f6a49a49072de736da6014b', '8ea96d5614a84aab92b9e2adfa044462', 'e65eba4dbedc4408a4810ccda457902a', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-18 10:16:55', NULL, NULL);
INSERT INTO `org_role_menu` VALUES ('bfe4b8c6eee64cfb86b2d86e19a41023', '3c282d0211b54952ac6294a6a34ded31', 'e65eba4dbedc4408a4810ccda457902a', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 14:54:54', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:33:20');
INSERT INTO `org_role_menu` VALUES ('c1958d4e9d144371a09fee2ed953e07f', '8ea96d5614a84aab92b9e2adfa044462', 'root', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-18 10:16:55', NULL, NULL);
INSERT INTO `org_role_menu` VALUES ('c292dfffa39a427e9b9c125a8872472b', '8ea96d5614a84aab92b9e2adfa044462', '2cd2cb5d1558457086154a58b9d71ac9', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 15:03:08', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-18 10:16:55');
INSERT INTO `org_role_menu` VALUES ('c4cfeb2bb30f446b951d0ad0c86771e9', '3c282d0211b54952ac6294a6a34ded31', '9017e8550c4c405386fa4b134a6bd828', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 14:54:54', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:33:20');
INSERT INTO `org_role_menu` VALUES ('c7930994bdbd4b8bb0437ae95016f51a', '8ea96d5614a84aab92b9e2adfa044462', 'a6088f9843db499f8b387235d4776bd4', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 15:03:08', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-18 10:16:55');
INSERT INTO `org_role_menu` VALUES ('eb87ed266b2547f6acff6ad32d9bd881', '8ea96d5614a84aab92b9e2adfa044462', 'c72f02e626ff42faa4a3a22b9ab0e349', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-18 10:16:55', NULL, NULL);
INSERT INTO `org_role_menu` VALUES ('ec6be1fd3e624cc89298caa4d1b06be9', '6b20b8cf0a2a4ffc88798b9a4d0d4b3f', '9017e8550c4c405386fa4b134a6bd828', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 15:01:01', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:35:07');
INSERT INTO `org_role_menu` VALUES ('f1dfffb150974cac9f028ec5e5b76bf4', '6b20b8cf0a2a4ffc88798b9a4d0d4b3f', '7c9cd13b0b1648e887df3b007020db81', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 15:01:01', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:35:07');
INSERT INTO `org_role_menu` VALUES ('f8ccc000eeb44462a8e7ed46c4b7fcfd', '3c282d0211b54952ac6294a6a34ded31', 'f517de06c3964b3fbb8f180317fd307b', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 14:54:54', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:33:20');
INSERT INTO `org_role_menu` VALUES ('febe5152a4b84858843123422fd1b7d2', '3c282d0211b54952ac6294a6a34ded31', '008866dc45d54d64b6af32aab8ac2056', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 14:54:54', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 11:33:20');

-- ----------------------------
-- Table structure for org_role_user
-- ----------------------------
DROP TABLE IF EXISTS `org_role_user`;
CREATE TABLE `org_role_user`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色用户id，主键，uuid',
  `role_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色id',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_org_role_user_role_id`(`role_id`) USING BTREE,
  INDEX `idx_org_role_user_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of org_role_user
-- ----------------------------
INSERT INTO `org_role_user` VALUES ('1016d7245ec14c249032b081bcd47d2b', '8ea96d5614a84aab92b9e2adfa044462', '8db57604a1e44451850249ad34099263', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 15:02:55', NULL, NULL);
INSERT INTO `org_role_user` VALUES ('5340a9d22f6e4d0db45d08369b2afa7b', 'f895299310764dcf9c3afec8ce6fb39d', 'c8f1ba6c7cf842409aba43206e9f7442', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-13 20:43:50', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-13 20:43:32');
INSERT INTO `org_role_user` VALUES ('be89c1d2f09b4314b43f508059911a15', '6b20b8cf0a2a4ffc88798b9a4d0d4b3f', '3951bd7ab34f4cafa230c3c674bacd72', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 14:58:56', NULL, NULL);
INSERT INTO `org_role_user` VALUES ('e6fc463b603a47cd93224b1b8717609e', '3c282d0211b54952ac6294a6a34ded31', 'edfac0350de749da8ee149f60551b9d3', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 14:50:31', NULL, NULL);

-- ----------------------------
-- Table structure for org_synch_log
-- ----------------------------
DROP TABLE IF EXISTS `org_synch_log`;
CREATE TABLE `org_synch_log`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键，uuid',
  `type` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '同步表名',
  `last_synch_time` datetime(0) NOT NULL COMMENT '创建时间（上一次同步时间）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '同步信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for org_user
-- ----------------------------
DROP TABLE IF EXISTS `org_user`;
CREATE TABLE `org_user`  (
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `user_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户编码',
  `user_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名称',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别:0:女,1:男',
  `birthday` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生日，格式YYYY-MM-DD',
  `sort` int(11) NULL DEFAULT 999 COMMENT '顺序号',
  `secret_level` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '0:无 1:秘密 2:机密 3:绝密',
  `id_card` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证',
  `theme` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主题,blue:蓝色，red:红色',
  `office_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '办公电话',
  `mobile_phone` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '移动电话',
  `fax` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '传真',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `zw_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职务id',
  `direct_leader` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '直属领导',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `extend1` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段1',
  `extend2` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段2',
  `extend3` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段3',
  `extend4` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段4',
  `extend5` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段5',
  `extend6` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段6',
  `extend7` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段7',
  `extend8` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段8',
  `extend9` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段9',
  `extend10` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展字段10',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `idx_org_user_code`(`user_code`) USING BTREE,
  UNIQUE INDEX `idx_org_user_id_card`(`id_card`) USING BTREE,
  UNIQUE INDEX `idx_org_user_mobile_phone`(`mobile_phone`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of org_user
-- ----------------------------
INSERT INTO `org_user` VALUES ('3951bd7ab34f4cafa230c3c674bacd72', 'secadmin', '安全管理员', NULL, NULL, 999, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 14:58:26', NULL, NULL);
INSERT INTO `org_user` VALUES ('8db57604a1e44451850249ad34099263', 'auditadmin', '审计管理员', NULL, NULL, 999, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 15:02:34', NULL, NULL);
INSERT INTO `org_user` VALUES ('c8f1ba6c7cf842409aba43206e9f7442', 'superadmin', '管理员', NULL, NULL, 999, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `org_user` VALUES ('edfac0350de749da8ee149f60551b9d3', 'sysadmin', '系统管理员', NULL, NULL, 999, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 14:49:39', NULL, NULL);

-- ----------------------------
-- Table structure for org_zw
-- ----------------------------
DROP TABLE IF EXISTS `org_zw`;
CREATE TABLE `org_zw`  (
  `zw_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '职务id，主键，uuid',
  `zw_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '职务编码，唯一',
  `zw_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '职务名称',
  `sort` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `description` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`zw_id`) USING BTREE,
  UNIQUE INDEX `idx_org_zw_code`(`zw_code`) USING BTREE,
  INDEX `idx_org_zw_sort`(`sort`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '职务表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BLOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CRON_EXPRESSION` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TIME_ZONE_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('DefaultQuartzScheduler', 'deptJob', '2', '0 0/5 * * * ? ', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('DefaultQuartzScheduler', 'userJob', '1', '0 0/5 * * * ? ', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ENTRY_ID` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `ENTRY_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_DURABLE` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_UPDATE_DATA` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('DefaultQuartzScheduler', 'deptJob', '2', NULL, 'com.css.bpm.platform.org.synch.job.DeptJob', '0', '0', '0', '0', NULL);
INSERT INTO `qrtz_job_details` VALUES ('DefaultQuartzScheduler', 'userJob', '1', NULL, 'com.css.bpm.platform.org.synch.job.UserJob', '0', '0', '0', '0', NULL);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LOCK_NAME` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `LOCK_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `STR_PROP_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `INT_PROP_1` int(11) NULL DEFAULT NULL,
  `INT_PROP_2` int(11) NULL DEFAULT NULL,
  `LONG_PROP_1` bigint(20) NULL DEFAULT NULL,
  `LONG_PROP_2` bigint(20) NULL DEFAULT NULL,
  `DEC_PROP_1` decimal(13, 4) NULL DEFAULT NULL,
  `DEC_PROP_2` decimal(13, 4) NULL DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PRIORITY` int(11) NULL DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_TYPE` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) NULL DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) NULL DEFAULT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `SCHED_NAME`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('DefaultQuartzScheduler', 'deptJob', '2', 'deptJob', '2', NULL, 0, 0, 5, 'WAITING', 'CRON', 0, 0, NULL, 0, 0x30);
INSERT INTO `qrtz_triggers` VALUES ('DefaultQuartzScheduler', 'userJob', '1', 'userJob', '1', NULL, 0, 0, 5, 'WAITING', 'CRON', 0, 0, NULL, 0, 0x30);

-- ----------------------------
-- Table structure for sys_attachment_chunk
-- ----------------------------
DROP TABLE IF EXISTS `sys_attachment_chunk`;
CREATE TABLE `sys_attachment_chunk`  (
  `attach_chunk_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '附件分片id',
  `attach_md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件md5',
  `attach_chunk` int(11) NULL DEFAULT NULL COMMENT '附件分片',
  `attach_path` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件分片路径',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`attach_chunk_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用于临时存储附件的分片信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_attachment_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_attachment_info`;
CREATE TABLE `sys_attachment_info`  (
  `attach_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '附件id',
  `attach_name` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件名',
  `attach_type` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件类型',
  `attach_size` bigint(20) NULL DEFAULT NULL COMMENT '附件大小',
  `attach_path` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件路径',
  `attach_md5` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件md5码',
  `is_completed` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否上传完成',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否删除',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `file_dir_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件路径标识',
  PRIMARY KEY (`attach_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用于存储附件的相关信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键,uuid',
  `system_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '系统名称',
  `copyright` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '版权',
  `logo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `hierarchy_switch` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  `welcome_ctrl` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '欢迎页ctrl',
  `welcome_html` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '欢迎页html',
  `create_user` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_user` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统配置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('root', '中软业务流程管理平台', '@Copyright 中国软件与技术服务股份有限公司', '/static/core/platform/page/login/image/logo.png', '0', 'static/core/platform/page/welcome/js/welcomeCtrl', 'static/core/platform/page/welcome/welcome.html', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `dict_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典id，主键，uuid',
  `dict_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典编码，唯一',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典名称',
  `dict_dir_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典目录id',
  `sort` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `description` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `idx_sys_dict_dict_code`(`dict_code`) USING BTREE,
  INDEX `idx_sys_dict_sort`(`sort`) USING BTREE,
  INDEX `idx_sys_dict_dict_dir_id`(`dict_dir_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据字典表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('1bca5e16e93f49dfaa06de993ccdf4a6', 'secretLevel', '用户密级', '42b404a8cadb43bdab5492aa6d38e75a', NULL, '1', NULL, 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', '', NULL);
INSERT INTO `sys_dict` VALUES ('43cdd3e37868461ca654d67e2092d8ae', 'auditLevel', '审计级别', '4ce10191b11e4462b2a8cd244c43100a', NULL, '1', NULL, 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('5bdd7a4a443441cb84fa9b9ffdb9c28e', 'root:auditModule', '业务流程管理平台审计模块', '4ce10191b11e4462b2a8cd244c43100a', NULL, '1', NULL, 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', NULL);
INSERT INTO `sys_dict` VALUES ('c266cbf55e054423a55fe42095944845', 'root:auditType', '业务流程管理平台操作类型', '4ce10191b11e4462b2a8cd244c43100a', NULL, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', '', NULL);
INSERT INTO `sys_dict` VALUES ('ecfd338031e8488796863bdda745c052', 'deptLevel', '机构级别', '42b404a8cadb43bdab5492aa6d38e75a', NULL, '1', NULL, 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_directory
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_directory`;
CREATE TABLE `sys_dict_directory`  (
  `dict_dir_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典目录id，主键，uuid',
  `dict_dir_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典目录编码，唯一',
  `dict_dir_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典目录名称',
  `parent_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上级字典目录id',
  `all_parent_id` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典目录全路径，层次码',
  `sort` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `description` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`dict_dir_id`) USING BTREE,
  UNIQUE INDEX `idx_sys_dict_directory_dict_dir_code`(`dict_dir_code`) USING BTREE,
  INDEX `idx_sys_dict_directory_sort`(`sort`) USING BTREE,
  INDEX `idx_sys_dict_directory_parent_id`(`parent_id`) USING BTREE,
  FULLTEXT INDEX `idx_sys_dict_directory_all_parent_id`(`all_parent_id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据字典目录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dict_directory
-- ----------------------------
INSERT INTO `sys_dict_directory` VALUES ('42b404a8cadb43bdab5492aa6d38e75a', 'platformDirectory', '平台字典', 'root', 'root,42b404a8cadb43bdab5492aa6d38e75a', 1, '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-27 18:07:02', NULL, NULL);
INSERT INTO `sys_dict_directory` VALUES ('4ce10191b11e4462b2a8cd244c43100a', 'audit', '审计日志', 'root', 'a1796c33fcee4f8185de1081aed8bb2e,4ce10191b11e4462b2a8cd244c43100a', NULL, '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-14 19:28:34', '', NULL);
INSERT INTO `sys_dict_directory` VALUES ('root', 'root', '中国软件与技术服务股份有限公司', '', 'root', NULL, '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_item
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_item`;
CREATE TABLE `sys_dict_item`  (
  `dict_item_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典项id，主键，uuid',
  `dict_item_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典项编码，唯一',
  `dict_item_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典项名称',
  `dict_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典id',
  `sort` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `description` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`dict_item_id`) USING BTREE,
  INDEX `idx_sys_dict_item_dict_item_code`(`dict_item_code`) USING BTREE,
  INDEX `idx_sys_dict_item_sort`(`sort`) USING BTREE,
  INDEX `idx_sys_dict_item_dict_id`(`dict_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据字典项表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dict_item
-- ----------------------------
INSERT INTO `sys_dict_item` VALUES ('1154f2dffe0947b5a312f10d50deccaa', 'authorization', '授权', 'c266cbf55e054423a55fe42095944845', 5, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('1a8ef228481d4d07af1e16a2307e53f2', 'accountManage', '账号管理', '5bdd7a4a443441cb84fa9b9ffdb9c28e', 10, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('1e2294156a6d459ba32d6f48cb562f32', 'dictManage', '字典管理', '5bdd7a4a443441cb84fa9b9ffdb9c28e', 12, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('2aaa48eebdf84e7eae46aebb48efc32a', 'pwdConfigManage', '密码策略管理', '5bdd7a4a443441cb84fa9b9ffdb9c28e', 15, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('30f5f06f911749beb227012d4f2e6bf4', 'zwManage', '职务管理', '5bdd7a4a443441cb84fa9b9ffdb9c28e', 5, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('3f1523829ede4577aec8ed324d4c7727', '2', '县级', 'ecfd338031e8488796863bdda745c052', 3, '1', NULL, 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('466266529f5b4bd693c538c37f8a93a9', 'gwManage', '岗位管理', '5bdd7a4a443441cb84fa9b9ffdb9c28e', 3, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('4c5a8f8790d1453da8be08c4d00649db', 'resetPassword', '重置密码', 'c266cbf55e054423a55fe42095944845', 6, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('4dd642b253594daf97f4c42e8d6069b0', '1', '市级', 'ecfd338031e8488796863bdda745c052', 2, '1', NULL, 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('5501a98348fe49fa9a4405f9ae092d28', '0', '无', '1bca5e16e93f49dfaa06de993ccdf4a6', 1, '1', NULL, 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('75d91c301235419e972541ccdbe2fbd0', 'divisionManage', '行政区划管理', '5bdd7a4a443441cb84fa9b9ffdb9c28e', 13, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('79d1bd494cf4457db7d981844aafecba', '1', '秘密', '1bca5e16e93f49dfaa06de993ccdf4a6', 2, '1', NULL, 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('7a99e965b3654ccf9e61cf91f865284a', 'delete', '删除', 'c266cbf55e054423a55fe42095944845', 2, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('81fd0ba33f91422f9d4d1c544dfd2797', '0', '省级', 'ecfd338031e8488796863bdda745c052', 1, '1', NULL, 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('89efa6d5e8654eab81c6da71a7d97efc', 'resourceManage', '资源管理', '5bdd7a4a443441cb84fa9b9ffdb9c28e', 11, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('8e5317264ab24e80ba2dc4790652d838', 'roleResourceManage', '角色资源管理', '5bdd7a4a443441cb84fa9b9ffdb9c28e', 7, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('92921aaa70e745869b3516883098303b', 'update', '修改', 'c266cbf55e054423a55fe42095944845', 3, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('95ecf330eef44e95b1a37e0dc0b9fdcb', 'publicDeptGroup', '公共部门群组', '5bdd7a4a443441cb84fa9b9ffdb9c28e', 8, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('9c5fa98db5a44f33b3182bd458105665', 'menuManage', '菜单管理', '5bdd7a4a443441cb84fa9b9ffdb9c28e', 1, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('a12fb7d6210b4581b75d319da8455a15', 'sysAppManage', '子系统管理', '5bdd7a4a443441cb84fa9b9ffdb9c28e', 16, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('a2766024a03d4d63a5d09f1b9eceb124', 'roleManage', '角色管理', '5bdd7a4a443441cb84fa9b9ffdb9c28e', 6, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('aeea111fb9034881af7ff1a6b4f2ea48', '3', '绝密', '1bca5e16e93f49dfaa06de993ccdf4a6', 4, '1', NULL, 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('b7400d8ae1514449aa01e42c6a1371e7', 'userLocked', '改变用户状态', 'c266cbf55e054423a55fe42095944845', 7, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('bdbe41bf10ec4d4fb7956bc12da8e23f', 'deptManage', '部门管理', '5bdd7a4a443441cb84fa9b9ffdb9c28e', 2, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('c3082fc570d64180a45974e0ceb6ea2e', 'normal', '一般', '43cdd3e37868461ca654d67e2092d8ae', NULL, '1', NULL, 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('c3a4f32b0ce24d8694b61976dd3c42d9', 'userManage', '用户管理', '5bdd7a4a443441cb84fa9b9ffdb9c28e', 4, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('d10c9f931e6c42c2bc0e66da5412a6ff', '2', '机密', '1bca5e16e93f49dfaa06de993ccdf4a6', 3, '1', NULL, 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('daf8d8f426734a4eb155f34fd0253ec5', 'serious', '严重', '43cdd3e37868461ca654d67e2092d8ae', NULL, '1', NULL, 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_dict_item` VALUES ('dc9a7895a06545d4b1580067008134b4', 'publicUserGroup', '公共用户群组', '5bdd7a4a443441cb84fa9b9ffdb9c28e', 9, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('dea403671f7b49a883cea4be743886f7', 'insert', '新增', 'c266cbf55e054423a55fe42095944845', 1, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('e90596bc617c476f9ca23fbe49b7e1d9', 'import', '导入', 'c266cbf55e054423a55fe42095944845', 4, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');
INSERT INTO `sys_dict_item` VALUES ('f6a6d43f29d84762bd09abc7bf640b38', 'sysConfigManage', '系统参数配置', '5bdd7a4a443441cb84fa9b9ffdb9c28e', 14, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18');

-- ----------------------------
-- Table structure for sys_fail_audit
-- ----------------------------
DROP TABLE IF EXISTS `sys_fail_audit`;
CREATE TABLE `sys_fail_audit`  (
  `audit_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '日志表id',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `audit_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '操作类型',
  `audit_module` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '审计模块',
  `audit_data` blob NULL COMMENT '审计数据',
  `app_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '系统id',
  `audit_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求url',
  `client_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访问ip',
  `audit_level` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '日志级别',
  `audit_time` datetime(0) NOT NULL COMMENT '审计时间',
  `extend_data` blob NULL COMMENT '扩展数据',
  `description` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '操作描述',
  PRIMARY KEY (`audit_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作失败日志表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_login_fail_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_fail_log`;
CREATE TABLE `sys_login_fail_log`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键，uuid',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户id',
  `user_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户编码',
  `client_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ip地址',
  `fail_code` varchar(17) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录失败原因,ACCOUNT_NOT_EXIST:用户不存在, PASSWORD_ERROR:密码错误,TOO_MANY_FAILURES:失败次数过多已锁定,INACTIVE:用户未激活',
  `fail_time` datetime(0) NULL DEFAULT NULL COMMENT '登出时间',
  `app_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统id'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '登录失败日志' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_login_success_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_success_log`;
CREATE TABLE `sys_login_success_log`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键，uuid',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `client_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ip地址',
  `login_time` datetime(0) NOT NULL COMMENT '登录时间',
  `logout_time` datetime(0) NULL DEFAULT NULL COMMENT '登出时间',
  `app_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统id'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '登录成功日志' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_login_success_log
-- ----------------------------
INSERT INTO `sys_login_success_log` VALUES ('8ffbcb8165254d52b7ea1ad7dbdce067', 'c8f1ba6c7cf842409aba43206e9f7442', '10.13.191.9', '2020-10-30 09:55:48', NULL, 'root');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单id',
  `menu_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上级菜单id',
  `all_parent_id` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `menu_param` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '传递参数',
  `menu_open` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '打开方式',
  `menu_router` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单路由',
  `menu_ctrl` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单控制器地址',
  `menu_url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单资源地址',
  `menu_type` varchar(9) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型，directory:菜单目录,menu:菜单项',
  `app_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '应用系统id',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `sort` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `description` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `is_enable` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`menu_id`) USING BTREE,
  INDEX `idx_menu_router`(`menu_router`) USING BTREE,
  INDEX `idx_parent_id`(`parent_id`) USING BTREE,
  INDEX `idx_sort`(`sort`) USING BTREE,
  FULLTEXT INDEX `idx_all_parent_id`(`all_parent_id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('008866dc45d54d64b6af32aab8ac2056', '组织用户参数配置', '7c9cd13b0b1648e887df3b007020db81', 'root,e65eba4dbedc4408a4810ccda457902a,7c9cd13b0b1648e887df3b007020db81,008866dc45d54d64b6af32aab8ac2056', NULL, '1', 'org_configCtrl', 'static/core/platform/org/config/orgConfigCtrl', 'static/core/platform/org/config/views/orgConfigForm.html', 'menu', 'root', NULL, 15, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('00f8fe7989df4713ab61349a636d0d62', '可视化开发', '9705660d3a9e438fa6423bb3444a590c', 'root,9705660d3a9e438fa6423bb3444a590c,00f8fe7989df4713ab61349a636d0d62', NULL, NULL, '', '', '', 'directory', 'root', '', 2, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-09-17 10:14:58', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('010cfcb26e024f0fbce5da20e5ccf552', '日期', 'b0f1bf57f61040268277eee1212b9531', '0,051fbdf5deb54da18171b68c014a0c4c,b0f1bf57f61040268277eee1212b9531,010cfcb26e024f0fbce5da20e5ccf552', NULL, '1', 'dateTimePicker', 'static/core/platform/examples/thirdParty/dateTimePicker/dateTimePickerCtrl', 'static/core/platform/examples/common.html', 'menu', 'root', NULL, 2, '日期', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:07:09', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('0afc2165bc914969bc623f87c304add8', '组织选择', 'c569285b356743f9864e4ffb0fd8402f', 'root,1559ee41f2974e2cbd94ba5a0ae98507,c569285b356743f9864e4ffb0fd8402f,0afc2165bc914969bc623f87c304add8', NULL, '1', 'selectOrg', 'static/core/platform/examples/util/selectOrg/selectOrgCtrl', 'static/core/platform/examples/common.html', 'menu', 'root', NULL, 9, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:12:14', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('139752e0a0384ad3b51f2b392a73e721', '服务器监控', '2cd2cb5d1558457086154a58b9d71ac9', 'root,e65eba4dbedc4408a4810ccda457902a,a6088f9843db499f8b387235d4776bd4,2cd2cb5d1558457086154a58b9d71ac9,139752e0a0384ad3b51f2b392a73e721', NULL, '1', 'actuatorCtrl', 'static/core/platform/monitor/actuator/actuatorCtrl', 'static/core/platform/monitor/actuator/views/index.html', 'menu', 'root', NULL, 55, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-06-24 17:18:25', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1559ee41f2974e2cbd94ba5a0ae98507', '开发样例', 'root', 'root,1559ee41f2974e2cbd94ba5a0ae98507', NULL, '1', '', '', '', 'directory', 'root', 'fa fa-fw fa-battery-4', 2, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:04:46', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('249fe96b28ab48eea3cc0efbff82223c', '离线业务开发', '00f8fe7989df4713ab61349a636d0d62', 'root,9705660d3a9e438fa6423bb3444a590c,00f8fe7989df4713ab61349a636d0d62,249fe96b28ab48eea3cc0efbff82223c', '', '1', 'visual_offlineCtrl', 'static/core/codegenerator/visual/offline/offlineCtrl', 'static/core/codegenerator/visual/offline/views/index.html', 'menu', 'root', NULL, 1, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-09-17 06:36:25', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('2618a93fae2d471f85e4bb926f973863', '附件上传', 'c569285b356743f9864e4ffb0fd8402f', 'root,1559ee41f2974e2cbd94ba5a0ae98507,c569285b356743f9864e4ffb0fd8402f,2618a93fae2d471f85e4bb926f973863', NULL, '1', 'fileUpload', 'static/core/platform/examples/util/fileUploader/fileUploaderCtrl', 'static/core/platform/examples/common.html', 'menu', 'root', NULL, 12, '附件上传', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:09:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('2b902f547da24ab7aa68fb28a5e57a24', '右键菜单', 'c569285b356743f9864e4ffb0fd8402f', 'root,1559ee41f2974e2cbd94ba5a0ae98507,c569285b356743f9864e4ffb0fd8402f,2b902f547da24ab7aa68fb28a5e57a24', NULL, '1', 'menuComp', 'static/core/platform/examples/util/menu/menuCompCtrl', 'static/core/platform/examples/common.html', 'menu', 'root', NULL, 10, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:12:51', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('2cd2cb5d1558457086154a58b9d71ac9', '监控展示', 'a6088f9843db499f8b387235d4776bd4', 'root,e65eba4dbedc4408a4810ccda457902a,a6088f9843db499f8b387235d4776bd4,2cd2cb5d1558457086154a58b9d71ac9', NULL, '1', '', '', '', 'directory', 'root', '', 1, '监控展示', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-14 16:41:44', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('3164e28f68184cacb777420ce6f4bad5', 'Util', 'c569285b356743f9864e4ffb0fd8402f', 'root,1559ee41f2974e2cbd94ba5a0ae98507,c569285b356743f9864e4ffb0fd8402f,3164e28f68184cacb777420ce6f4bad5', NULL, '1', 'util', 'static/core/platform/examples/util/util/utilCtrl', 'static/core/platform/examples/common.html', 'menu', 'root', NULL, 1, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:06:55', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('319bc75511704b3f994a1e5cc53543de', '数据字典', '7c9cd13b0b1648e887df3b007020db81', 'root,e65eba4dbedc4408a4810ccda457902a,7c9cd13b0b1648e887df3b007020db81,319bc75511704b3f994a1e5cc53543de', NULL, '1', 'sys_dictCtrl', 'static/core/platform/sys/dict/dictCtrl', 'static/core/platform/sys/dict/views/index.html', 'menu', 'root', NULL, 5, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('3cf503875543442a8d436ed72fa0e34a', '网页播放器', 'b0f1bf57f61040268277eee1212b9531', '0,051fbdf5deb54da18171b68c014a0c4c,b0f1bf57f61040268277eee1212b9531,3cf503875543442a8d436ed72fa0e34a', NULL, '1', 'ckplayer', 'static/core/platform/examples/thirdParty/ckplayer/ckplayerCtrl', 'static/core/platform/examples/common.html', 'menu', 'root', NULL, 7, '网页播放器', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:14:39', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('43d46ddbc22240858031c490c77f04a4', 'combotree', 'c569285b356743f9864e4ffb0fd8402f', 'root,1559ee41f2974e2cbd94ba5a0ae98507,c569285b356743f9864e4ffb0fd8402f,43d46ddbc22240858031c490c77f04a4', NULL, '1', 'combotree', 'static/core/platform/examples/util/combotree/combotreeCtrl', 'static/core/platform/examples/common.html', 'menu', 'root', NULL, 3, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:08:30', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('4924e8c0669f40d0854a62fc7b6b92a8', '用户管理', '9017e8550c4c405386fa4b134a6bd828', 'root,e65eba4dbedc4408a4810ccda457902a,9017e8550c4c405386fa4b134a6bd828,4924e8c0669f40d0854a62fc7b6b92a8', NULL, '1', 'org_userCtrl', 'static/core/platform/org/user/userCtrl', 'static/core/platform/org/user/views/user.html', 'menu', 'root', NULL, 15, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('512277186ff1439cbe87f441a6d7931b', '角色资源配置', '9017e8550c4c405386fa4b134a6bd828', 'root,e65eba4dbedc4408a4810ccda457902a,9017e8550c4c405386fa4b134a6bd828,512277186ff1439cbe87f441a6d7931b', NULL, '1', 'sys_resource_auth', 'static/core/platform/sys/roleresource/roleResourceCtrl', 'static/core/platform/sys/roleresource/views/roleResourceIndex.html', 'menu', 'root', '', 65, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('517c34c949a94c02ad82d9b552a20ea1', '图标', 'b0f1bf57f61040268277eee1212b9531', '0,051fbdf5deb54da18171b68c014a0c4c,b0f1bf57f61040268277eee1212b9531,517c34c949a94c02ad82d9b552a20ea1', NULL, '1', 'fontawesome', 'static/core/platform/examples/thirdParty/fontawesome/fontCtrl', 'static/core/platform/examples/common.html', 'menu', 'root', NULL, 5, '图标', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:12:45', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('521d1eb1915049458ae172b4ce53021b', 'Redis管理', '2cd2cb5d1558457086154a58b9d71ac9', 'root,e65eba4dbedc4408a4810ccda457902a,a6088f9843db499f8b387235d4776bd4,2cd2cb5d1558457086154a58b9d71ac9,521d1eb1915049458ae172b4ce53021b', NULL, '1', 'redisManagerCtrl', 'static/core/platform/monitor/redis/manager/redisManagerCtrl', 'static/core/platform/monitor/redis/manager/views/redisManagerIndex.html', 'menu', 'root', NULL, 8, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-06-08 21:16:01', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-06-08 21:17:52');
INSERT INTO `sys_menu` VALUES ('58910294425a4dd2b85ec949d7d838b9', '账号管理', '9017e8550c4c405386fa4b134a6bd828', 'root,e65eba4dbedc4408a4810ccda457902a,9017e8550c4c405386fa4b134a6bd828,58910294425a4dd2b85ec949d7d838b9', NULL, '1', 'org_userActiveCtrl', 'static/core/platform/org/useractive/userActiveCtrl', 'static/core/platform/org/useractive/views/index.html', 'menu', 'root', NULL, 50, '1', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('5eff80983f7747ffbe8334a79c68d972', 'combo', 'c569285b356743f9864e4ffb0fd8402f', 'root,1559ee41f2974e2cbd94ba5a0ae98507,c569285b356743f9864e4ffb0fd8402f,5eff80983f7747ffbe8334a79c68d972', NULL, '1', 'combo', 'static/core/platform/examples/util/combo/comboCtrl', 'static/core/platform/examples/common.html', 'menu', 'root', NULL, 2, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:07:56', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('5f495dadc71a4f45bb11f220d276621f', '弹出框', 'c569285b356743f9864e4ffb0fd8402f', 'root,1559ee41f2974e2cbd94ba5a0ae98507,c569285b356743f9864e4ffb0fd8402f,5f495dadc71a4f45bb11f220d276621f', NULL, '1', 'dialog', 'static/core/platform/examples/util/dialog/dialogCtrl', 'static/core/platform/examples/common.html', 'menu', 'root', NULL, 7, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:11:07', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('6607560a26d54d4c985d18bc990e97d5', '个人设置', '9017e8550c4c405386fa4b134a6bd828', 'root,e65eba4dbedc4408a4810ccda457902a,9017e8550c4c405386fa4b134a6bd828,6607560a26d54d4c985d18bc990e97d5', NULL, '1', 'org_personalCtrl', 'static/core/platform/org/personal/personalCtrl', 'static/core/platform/org/personal/views/personalForm.html', 'menu', 'root', NULL, 55, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('6976f9d712ef4ba3b5834c1413f89cc2', '元数据基础管理', '944cc7eb25eb45d28b1f9c3a8a9fc627', 'root,9705660d3a9e438fa6423bb3444a590c,796f701c36914b10a38023f0d79061a8,944cc7eb25eb45d28b1f9c3a8a9fc627,6976f9d712ef4ba3b5834c1413f89cc2', '', '1', 'metadata_metaBasicCtrl', 'static/core/codegenerator/metadata/basic/metaBasicCtrl', 'static/core/codegenerator/metadata/basic/views/index.html', 'menu', 'root', NULL, 1, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-09-17 10:18:30', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('6cf47d23184944819dda7d6240ba3a04', '子系统管理', '7c9cd13b0b1648e887df3b007020db81', 'root,e65eba4dbedc4408a4810ccda457902a,7c9cd13b0b1648e887df3b007020db81,6cf47d23184944819dda7d6240ba3a04', NULL, '1', 'sys_subapp', 'static/core/platform/sys/syssubapp/sysSubappCtrl', 'static/core/platform/sys/syssubapp/views/sysSubappIndex.html', 'menu', 'root', NULL, 30, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('6e9bb9dc49d543369055dd38f39c25f8', '用户密码策略', '7c9cd13b0b1648e887df3b007020db81', 'root,e65eba4dbedc4408a4810ccda457902a,7c9cd13b0b1648e887df3b007020db81,6e9bb9dc49d543369055dd38f39c25f8', NULL, '1', 'sys_pwdconfigCtrl', 'static/core/platform/sys/pwdconfig/sysPwdConfigCtrl', 'static/core/platform/sys/pwdconfig/views/sysPwdConfigForm.html', 'menu', 'root', NULL, 25, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('6f903ff8a2bd4567a71d7e8125c07a9c', '元数据基础管理', '68032a34ec62444baffdbfd988f3d070', 'root,67874b44b24949608b1414df4885d2ca,68032a34ec62444baffdbfd988f3d070,6f903ff8a2bd4567a71d7e8125c07a9c', NULL, '1', 'metaBaseCtrl', 'static/core/codegenerator/metadata/basic/metaBasicCtrl', 'static/core/codegenerator/metadata/basic/views/index.html', 'menu', 'root', NULL, 1, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-22 06:11:02', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-29 08:20:54');
INSERT INTO `sys_menu` VALUES ('796f701c36914b10a38023f0d79061a8', '配置管理', '9705660d3a9e438fa6423bb3444a590c', 'root,9705660d3a9e438fa6423bb3444a590c,796f701c36914b10a38023f0d79061a8', NULL, NULL, '', '', '', 'directory', 'root', '', 1, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-09-17 10:14:39', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('7a69418010fa4071a4698d6d26f7a1f0', '组织同步', '9017e8550c4c405386fa4b134a6bd828', 'root,e65eba4dbedc4408a4810ccda457902a,9017e8550c4c405386fa4b134a6bd828,7a69418010fa4071a4698d6d26f7a1f0', NULL, '1', 'synchCtrl', 'static/core/platform/org/synch/synchCtrl', 'static/core/platform/org/synch/views/synch.html', 'menu', 'root', NULL, 100, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-08-06 17:14:24', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-08-06 17:14:59');
INSERT INTO `sys_menu` VALUES ('7c9cd13b0b1648e887df3b007020db81', '参数配置', 'e65eba4dbedc4408a4810ccda457902a', 'root,e65eba4dbedc4408a4810ccda457902a,7c9cd13b0b1648e887df3b007020db81', NULL, '1', '', '', '', 'directory', 'root', 'fa fa-fw fa-at', 10, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('7e0a77ef7c8b4a878bcdc5edacb405fc', '行政区划', '7c9cd13b0b1648e887df3b007020db81', 'root,e65eba4dbedc4408a4810ccda457902a,7c9cd13b0b1648e887df3b007020db81,7e0a77ef7c8b4a878bcdc5edacb405fc', NULL, '1', 'sys_divisionCtrl', 'static/core/platform/org/division/divisionCtrl', 'static/core/platform/org/division/views/index.html', 'menu', 'root', NULL, 10, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('7fdf0b563fa54337b77633a00f07c783', '通讯录', '9017e8550c4c405386fa4b134a6bd828', 'root,e65eba4dbedc4408a4810ccda457902a,9017e8550c4c405386fa4b134a6bd828,7fdf0b563fa54337b77633a00f07c783', NULL, '1', 'telCtrl', 'static/core/platform/org/teldirectory/telCtrl', 'static/core/platform/org/teldirectory/views/tel.html', 'menu', 'root', NULL, 50, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-12 19:02:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('8218b65fcf0d45e9bb02536d3a46e3d0', '元数据高级管理', '944cc7eb25eb45d28b1f9c3a8a9fc627', 'root,9705660d3a9e438fa6423bb3444a590c,796f701c36914b10a38023f0d79061a8,944cc7eb25eb45d28b1f9c3a8a9fc627,8218b65fcf0d45e9bb02536d3a46e3d0', '', '1', 'metadata_metaExtendCtrl', 'static/core/codegenerator/metadata/extend/metaExtendCtrl', 'static/core/codegenerator/metadata/extend/views/index.html', 'menu', 'root', NULL, 2, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-09-17 10:19:45', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('85d66613efe44648be50874f10b67c2e', '菜单管理', '9017e8550c4c405386fa4b134a6bd828', 'root,e65eba4dbedc4408a4810ccda457902a,9017e8550c4c405386fa4b134a6bd828,85d66613efe44648be50874f10b67c2e', NULL, '1', 'sys_menuCtrl', 'static/core/platform/sys/menu/menuCtrl', 'static/core/platform/sys/menu/views/index.html', 'menu', 'root', NULL, 40, '菜单管理', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('8f329aac3bf84189b09edee78fdfdc72', 'loading', 'c569285b356743f9864e4ffb0fd8402f', 'root,1559ee41f2974e2cbd94ba5a0ae98507,c569285b356743f9864e4ffb0fd8402f,8f329aac3bf84189b09edee78fdfdc72', NULL, '1', 'loading', 'static/core/platform/examples/util/loading/loadingCtrl', 'static/core/platform/examples/common.html', 'menu', 'root', NULL, 8, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:11:35', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('9017e8550c4c405386fa4b134a6bd828', '组织权限', 'e65eba4dbedc4408a4810ccda457902a', 'root,e65eba4dbedc4408a4810ccda457902a,9017e8550c4c405386fa4b134a6bd828', NULL, '1', '', '', '', 'directory', 'root', 'fa fa-fw fa-bank', 5, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('944cc7eb25eb45d28b1f9c3a8a9fc627', '元数据管理', '796f701c36914b10a38023f0d79061a8', 'root,9705660d3a9e438fa6423bb3444a590c,796f701c36914b10a38023f0d79061a8,944cc7eb25eb45d28b1f9c3a8a9fc627', NULL, NULL, '', '', '', 'directory', 'root', '', 2, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-09-17 10:17:01', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('9705660d3a9e438fa6423bb3444a590c', '开发中心', 'root', 'root,9705660d3a9e438fa6423bb3444a590c', NULL, NULL, '', '', '', 'directory', 'root', 'fa fa-fw fa-gears', 23, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-09-17 10:13:28', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-09-17 10:13:59');
INSERT INTO `sys_menu` VALUES ('98ecdfb17e4d47768ac9b92f42e9e6b6', '系统参数配置', '7c9cd13b0b1648e887df3b007020db81', 'root,e65eba4dbedc4408a4810ccda457902a,68f41a97106e4069af808d72a95387f2,98ecdfb17e4d47768ac9b92f42e9e6b6', NULL, '1', 'sys_configCtrl', 'static/core/platform/sys/config/sysConfigCtrl', 'static/core/platform/sys/config/views/sysConfigForm.html', 'menu', 'root', NULL, 20, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('9c0c8d793e4e4ebbbce2446d50812eed', 'alert', 'c569285b356743f9864e4ffb0fd8402f', 'root,1559ee41f2974e2cbd94ba5a0ae98507,c569285b356743f9864e4ffb0fd8402f,9c0c8d793e4e4ebbbce2446d50812eed', NULL, '1', 'alert', 'static/core/platform/examples/util/alert/alertCtrl', 'static/core/platform/examples/common.html', 'menu', 'root', NULL, 4, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:08:58', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('9eb76da7c98e40709c916ec8c27e9c49', '岗位管理', '9017e8550c4c405386fa4b134a6bd828', 'root,e65eba4dbedc4408a4810ccda457902a,9017e8550c4c405386fa4b134a6bd828,9eb76da7c98e40709c916ec8c27e9c49', NULL, '1', 'org_gwCtrl', 'static/core/platform/org/gw/gwCtrl', 'static/core/platform/org/gw/views/index.html', 'menu', 'root', NULL, 25, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('a6088f9843db499f8b387235d4776bd4', '运维中心', 'e65eba4dbedc4408a4810ccda457902a', 'root,e65eba4dbedc4408a4810ccda457902a,a6088f9843db499f8b387235d4776bd4', NULL, '1', '', '', '', 'directory', 'root', '', 56666, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-14 16:40:29', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-14 16:41:04');
INSERT INTO `sys_menu` VALUES ('a958c7137f27436d8a2dd9685c09fe13', '个人群组', '9017e8550c4c405386fa4b134a6bd828', 'root,e65eba4dbedc4408a4810ccda457902a,9017e8550c4c405386fa4b134a6bd828,a958c7137f27436d8a2dd9685c09fe13', NULL, '1', 'org_privateGroupCtrl', 'static/core/platform/org/privategroup/privateGroupCtrl', 'static/core/platform/org/privategroup/views/index.html', 'menu', 'root', NULL, 35, '个人群组', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('ac8651f916e74ab4bf0965f7b08f427c', '滚动条美化', 'b0f1bf57f61040268277eee1212b9531', '0,051fbdf5deb54da18171b68c014a0c4c,b0f1bf57f61040268277eee1212b9531,ac8651f916e74ab4bf0965f7b08f427c', NULL, '1', 'mcScrollbar', 'static/core/platform/examples/thirdParty/mCustomScrollbar/mcScrollbarCtrl', 'static/core/platform/examples/common.html', 'menu', 'root', NULL, 6, '滚动条美化', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:13:42', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('b0f1bf57f61040268277eee1212b9531', '第三方组件', '1559ee41f2974e2cbd94ba5a0ae98507', '0,051fbdf5deb54da18171b68c014a0c4c,b0f1bf57f61040268277eee1212b9531', NULL, '1', '', '', '', 'directory', 'root', '', 3, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:03:29', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('b13c2feb59ba4b6094bfee2929deba3c', '审计日志查询', '2cd2cb5d1558457086154a58b9d71ac9', 'root,e65eba4dbedc4408a4810ccda457902a,a6088f9843db499f8b387235d4776bd4,2cd2cb5d1558457086154a58b9d71ac9,b13c2feb59ba4b6094bfee2929deba3c', NULL, '1', 'auditCtrl', 'static/core/platform/sys/audit/auditCtrl', 'static/core/platform/sys/audit/views/index.html', 'menu', 'root', NULL, 10, '审计日志查询', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-14 16:42:40', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-14 16:50:53');
INSERT INTO `sys_menu` VALUES ('ba42b8d79e89428db49ffea6350656f7', '登录日志', '2cd2cb5d1558457086154a58b9d71ac9', 'root,e65eba4dbedc4408a4810ccda457902a,a6088f9843db499f8b387235d4776bd4,2cd2cb5d1558457086154a58b9d71ac9,ba42b8d79e89428db49ffea6350656f7', NULL, '1', 'loginlogCtrl', 'static/core/platform/sys/loginlog/loginlogCtrl', 'static/core/platform/sys/loginlog/views/index.html', 'menu', 'root', NULL, 52, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 16:04:58', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-16 10:19:51');
INSERT INTO `sys_menu` VALUES ('bab4c6d65e7b448a9db6070769beb4f1', '组织机构', '9017e8550c4c405386fa4b134a6bd828', 'root,e65eba4dbedc4408a4810ccda457902a,9017e8550c4c405386fa4b134a6bd828,bab4c6d65e7b448a9db6070769beb4f1', NULL, '1', 'org_deptCtrl', 'static/core/platform/org/dept/deptCtrl', 'static/core/platform/org/dept/views/dept.html', 'menu', 'root', NULL, 10, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('bea8b17a8777474f81f31d7845a35eac', '图片裁剪', 'c569285b356743f9864e4ffb0fd8402f', 'root,1559ee41f2974e2cbd94ba5a0ae98507,c569285b356743f9864e4ffb0fd8402f,bea8b17a8777474f81f31d7845a35eac', NULL, '1', 'crop', 'static/core/platform/examples/util/crop/cropCtrl', 'static/core/platform/examples/common.html', 'menu', 'root', NULL, 11, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:13:27', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('c09e2a6b69014b63a3ef08fae379b6ad', 'confirm', 'c569285b356743f9864e4ffb0fd8402f', 'root,1559ee41f2974e2cbd94ba5a0ae98507,c569285b356743f9864e4ffb0fd8402f,c09e2a6b69014b63a3ef08fae379b6ad', NULL, '1', 'confirm', 'static/core/platform/examples/util/confirm/confirmCtrl', 'static/core/platform/examples/common.html', 'menu', 'root', NULL, 6, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:10:22', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('c2aadcbd67bc43059914a83a62e32169', 'Redis监控', '2cd2cb5d1558457086154a58b9d71ac9', 'root,e65eba4dbedc4408a4810ccda457902a,a6088f9843db499f8b387235d4776bd4,2cd2cb5d1558457086154a58b9d71ac9,c2aadcbd67bc43059914a83a62e32169', NULL, '1', 'redisCtrl', 'static/core/platform/monitor/redis/monitor/redisMonitorCtrl', 'static/core/platform/monitor/redis/monitor/views/redisMonitorIndex.html', 'menu', 'root', NULL, 7, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-06-05 02:37:26', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-06-08 21:12:14');
INSERT INTO `sys_menu` VALUES ('c569285b356743f9864e4ffb0fd8402f', 'Util', '1559ee41f2974e2cbd94ba5a0ae98507', 'root,1559ee41f2974e2cbd94ba5a0ae98507,c569285b356743f9864e4ffb0fd8402f', NULL, '1', '', '', '', 'directory', 'root', '', 1, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:05:05', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('c72f02e626ff42faa4a3a22b9ab0e349', '在线人员', '2cd2cb5d1558457086154a58b9d71ac9', 'root,e65eba4dbedc4408a4810ccda457902a,a6088f9843db499f8b387235d4776bd4,2cd2cb5d1558457086154a58b9d71ac9,c72f02e626ff42faa4a3a22b9ab0e349', NULL, '1', 'onlineCtrl', 'static/core/platform/org/online/onlineCtrl', 'static/core/platform/org/online/views/online.html', 'menu', 'root', NULL, 51, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-13 19:04:41', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-05-15 10:32:03');
INSERT INTO `sys_menu` VALUES ('c9d971c3ce21455c98ddc2acfd4ae277', '资源管理', '9017e8550c4c405386fa4b134a6bd828', 'root,e65eba4dbedc4408a4810ccda457902a,9017e8550c4c405386fa4b134a6bd828,c9d971c3ce21455c98ddc2acfd4ae277', NULL, '1', 'sys_resource', 'static/core/platform/sys/resource/sysResourceCtrl', 'static/core/platform/sys/resource/views/sysResourceIndex.html', 'menu', 'root', NULL, 60, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('cbe91853c89442e186a46a99ff624c6d', '角色管理', '9017e8550c4c405386fa4b134a6bd828', 'root,e65eba4dbedc4408a4810ccda457902a,9017e8550c4c405386fa4b134a6bd828,cbe91853c89442e186a46a99ff624c6d', NULL, '1', 'org_roleCtrl', 'static/core/platform/org/role/roleCtrl', 'static/core/platform/org/role/views/index.html', 'menu', 'root', NULL, 20, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('cf47fe43734949c898fa01e14676a902', 'cron表达式', 'c569285b356743f9864e4ffb0fd8402f', 'root,1559ee41f2974e2cbd94ba5a0ae98507,c569285b356743f9864e4ffb0fd8402f,cf47fe43734949c898fa01e14676a902', NULL, '1', 'cronExpression', 'static/core/platform/examples/util/cronExpression/cronExpressionCtrl', 'static/core/platform/examples/util/cronExpression/views/cronExpression.html', 'menu', 'root', NULL, 16, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-08-04 19:54:36', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-08-04 20:06:06');
INSERT INTO `sys_menu` VALUES ('d60e4966881d416bbd582666b9cd9aa8', '数据库监控', '2cd2cb5d1558457086154a58b9d71ac9', 'root,e65eba4dbedc4408a4810ccda457902a,a6088f9843db499f8b387235d4776bd4,2cd2cb5d1558457086154a58b9d71ac9,d60e4966881d416bbd582666b9cd9aa8', NULL, '1', 'druidCtrl', 'static/core/platform/monitor/druid/druidCtrl', 'static/core/platform/monitor/druid/views/index.html', 'menu', 'root', NULL, 55, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-07-16 14:59:48', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('dd1ef89b9c8b4134a68ffd4ba0727aed', '图表', 'b0f1bf57f61040268277eee1212b9531', '0,051fbdf5deb54da18171b68c014a0c4c,b0f1bf57f61040268277eee1212b9531,dd1ef89b9c8b4134a68ffd4ba0727aed', NULL, '1', 'ECharts', 'static/core/platform/examples/thirdParty/ECharts/eChartsCtrl', 'static/core/platform/examples/common.html', 'menu', 'root', NULL, 4, '图表', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:11:50', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('dd7e9de4e2c5499e961f2b857e3bf3b2', '树', 'b0f1bf57f61040268277eee1212b9531', '0,051fbdf5deb54da18171b68c014a0c4c,b0f1bf57f61040268277eee1212b9531,dd7e9de4e2c5499e961f2b857e3bf3b2', NULL, '1', 'zTree', 'static/core/platform/examples/thirdParty/zTree/zTreeCtrl', 'static/core/platform/examples/common.html', 'menu', 'root', NULL, 1, '树', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:04:41', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:07:27');
INSERT INTO `sys_menu` VALUES ('e0ae39f912134ab8ac5b5c9e460266ec', '表单验证', 'b0f1bf57f61040268277eee1212b9531', '0,051fbdf5deb54da18171b68c014a0c4c,b0f1bf57f61040268277eee1212b9531,e0ae39f912134ab8ac5b5c9e460266ec', NULL, '1', 'validate', 'static/core/platform/examples/thirdParty/validate/validateCtrl', 'static/core/platform/examples/common.html', 'menu', 'root', NULL, 3, '表单验证', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:08:33', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:08:41');
INSERT INTO `sys_menu` VALUES ('e57d1bbca695488ca11ac3c4e81fc359', '侧边栏', 'c569285b356743f9864e4ffb0fd8402f', 'root,1559ee41f2974e2cbd94ba5a0ae98507,c569285b356743f9864e4ffb0fd8402f,e57d1bbca695488ca11ac3c4e81fc359', NULL, '1', 'slidebar', 'static/core/platform/examples/util/slidebar/slidebarCtrl', 'static/core/platform/examples/common.html', 'menu', 'root', NULL, 5, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-04-20 15:09:53', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('e583a9838da34bcd8a72c959d0b05abf', '职务管理', '9017e8550c4c405386fa4b134a6bd828', 'root,e65eba4dbedc4408a4810ccda457902a,9017e8550c4c405386fa4b134a6bd828,e583a9838da34bcd8a72c959d0b05abf', NULL, '1', 'org_zwCtrl', 'static/core/platform/org/zw/zwCtrl', 'static/core/platform/org/zw/views/index.html', 'menu', 'root', NULL, 45, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('e65eba4dbedc4408a4810ccda457902a', '系统管理', 'root', 'root,e65eba4dbedc4408a4810ccda457902a', NULL, '1', '', '', '', 'directory', 'root', 'fa fa-fw fa-area-chart', 1, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, '2020-10-30 17:50:18');
INSERT INTO `sys_menu` VALUES ('e96405adca334de7b848b6d95cdc7e53', '二维码', 'c569285b356743f9864e4ffb0fd8402f', 'root,1559ee41f2974e2cbd94ba5a0ae98507,c569285b356743f9864e4ffb0fd8402f,e96405adca334de7b848b6d95cdc7e53', NULL, '1', 'qrcodel', 'static/core/platform/examples/util/qrcode/QRCodeCtrl', 'static/core/platform/examples/common.html', 'menu', 'root', NULL, 11, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-07-07 21:39:03', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-07-07 21:44:42');
INSERT INTO `sys_menu` VALUES ('f12387287f9347259d5f10fd93e88878', '工作时间配置', '7c9cd13b0b1648e887df3b007020db81', 'root,e65eba4dbedc4408a4810ccda457902a,7c9cd13b0b1648e887df3b007020db81,f12387287f9347259d5f10fd93e88878', NULL, '1', 'workTimeCtrl', 'static/core/platform/sys/worktime/workTimeCtrl', 'static/core/platform/sys/worktime/views/index.html', 'menu', 'root', NULL, 56, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('f517de06c3964b3fbb8f180317fd307b', '公共群组', '9017e8550c4c405386fa4b134a6bd828', 'root,e65eba4dbedc4408a4810ccda457902a,9017e8550c4c405386fa4b134a6bd828,f517de06c3964b3fbb8f180317fd307b', NULL, '1', 'org_publicGroupCtrl', 'static/core/platform/org/publicgroup/publicGroupCtrl', 'static/core/platform/org/publicgroup/views/index.html', 'menu', 'root', NULL, 30, '公共群组', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('fe61b1b310304840949679ccf78e358d', '请求监控', '2cd2cb5d1558457086154a58b9d71ac9', 'root,e65eba4dbedc4408a4810ccda457902a,a6088f9843db499f8b387235d4776bd4,2cd2cb5d1558457086154a58b9d71ac9,fe61b1b310304840949679ccf78e358d', NULL, '1', 'requestUriCtrl', 'static/core/platform/monitor/requestUri/requestUriCtrl', 'static/core/platform/monitor/requestUri/views/index.html', 'menu', 'root', NULL, 67, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-07-21 20:24:38', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('fed45b0bb06346228182fad00614f163', '模块分类管理', '796f701c36914b10a38023f0d79061a8', 'root,9705660d3a9e438fa6423bb3444a590c,796f701c36914b10a38023f0d79061a8,fed45b0bb06346228182fad00614f163', '', '1', 'directoryCtrl', 'static/core/codegenerator/directory/directoryCtrl', 'static/core/codegenerator/directory/views/index.html', 'menu', 'root', NULL, 1, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-09-17 10:16:30', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('root', '中软业务流程管理系统', '', 'root', NULL, '1', '', '', '', 'directory', 'root', NULL, 1, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, '2020-10-30 17:50:18');

-- ----------------------------
-- Table structure for sys_pwd_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_pwd_config`;
CREATE TABLE `sys_pwd_config`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `pwd_length` int(11) NULL DEFAULT NULL COMMENT '密码长度',
  `pwd_rule` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'unlimited' COMMENT '密码规则,unlimited:不限;numeric:数字,letter:字母,chars:字符,anytwo任意两种或以上',
  `pwd_expired` int(11) NULL DEFAULT NULL COMMENT '密码有效时间',
  `is_modify` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '首次登陆是否修改',
  `is_history` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '是否允许使用历史密码，比对前5次',
  `default_pwd` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '111111' COMMENT '默认密码,111111',
  `too_many_failures` int(11) NOT NULL DEFAULT 5 COMMENT '最大失败次数，超过次数后锁定',
  `locked_time` int(11) NOT NULL DEFAULT 2 COMMENT '登录次数过多锁定时间，小时数',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '密码配置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_pwd_config
-- ----------------------------
INSERT INTO `sys_pwd_config` VALUES ('root', NULL, 'unlimited', NULL, '0', '1', '111111', 5, 2, 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:18', NULL, NULL);

-- ----------------------------
-- Table structure for sys_resource
-- ----------------------------
DROP TABLE IF EXISTS `sys_resource`;
CREATE TABLE `sys_resource`  (
  `res_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键，uuid',
  `res_url` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资源地址，唯一',
  `res_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资源名称',
  `res_dir_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所属资源目录',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否有效：0无效 1有效',
  `sort` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `description` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`res_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '资源表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_resource_auth
-- ----------------------------
DROP TABLE IF EXISTS `sys_resource_auth`;
CREATE TABLE `sys_resource_auth`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键，uuid',
  `role_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色id',
  `res_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资源id',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否有效：0无效 1有效',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '资源授权表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_resource_directory
-- ----------------------------
DROP TABLE IF EXISTS `sys_resource_directory`;
CREATE TABLE `sys_resource_directory`  (
  `res_dir_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键，uuid',
  `res_dir_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资源目录编码',
  `res_dir_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资源目录名称',
  `parent_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上级资源目录id',
  `all_parent_id` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '资源目录全路径，层次码',
  `sort` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否有效：0无效 1有效',
  `description` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  PRIMARY KEY (`res_dir_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '资源目录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_resource_directory
-- ----------------------------
INSERT INTO `sys_resource_directory` VALUES ('root', 'root', '中国软件与技术服务股份管有限公司', NULL, 'root', NULL, '1', '', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:19', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:19');

-- ----------------------------
-- Table structure for sys_subapp
-- ----------------------------
DROP TABLE IF EXISTS `sys_subapp`;
CREATE TABLE `sys_subapp`  (
  `app_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键，uuid',
  `app_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '子系统编码,唯一',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '子系统名称',
  `app_address` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '子系统地址',
  `public_key` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公钥',
  `private_key` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '私钥',
  `sort` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `description` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `is_valid` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否有效,0:无效,1:有效',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`app_id`) USING BTREE,
  UNIQUE INDEX `idx_sys_subapp_code`(`app_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '子系统表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_subapp
-- ----------------------------
INSERT INTO `sys_subapp` VALUES ('appRoot', 'appRoot', '中国软件与技术股份有限公司服务', '\\', 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCwuiC4ghHLGxx3ifJX3Rtu7ZmpzBMdUBXr1NQlF9evNhL8nG8wlu80QEpPyKXN8tDk3vsYYnhqHA1XmPaxMp8jR1xe7x7N5hOAqWIYgH7ftw9qarQgzbii0TUgj6ekgU8VQdb0dL5C2eWQK9n9qoqwAv42qKLMecXyCAOmZowGYQIDAQAB', 'MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALC6ILiCEcsbHHeJ8lfdG27tmanMEx1QFevU1CUX1682EvycbzCW7zRASk/Ipc3y0OTe+xhieGocDVeY9rEynyNHXF7vHs3mE4CpYhiAft+3D2pqtCDNuKLRNSCPp6SBTxVB1vR0vkLZ5ZAr2f2qirAC/jaoosx5xfIIA6ZmjAZhAgMBAAECgYEAhHhwcH2nIszGsOApVS07FBJsHbgIF+/hDq6w+sD9fquU3d/RBOYOlecfDOEFT/QUG4r9QtEFG9y9L41MVZTxbXwy7jPaKy2EO3rhGUE9wjT+lufJ+y5LhP3np9rdzrokpsSFJPs5QbY8GHTBo4pX5C8p1+s/5HHN6L7QKfM0G/ECQQDeDuC4dcdsGo2Ps0ctHl1UN/FfSn3DT31r2K5h8jb7pd5LFZjU4f3zXLgCSeAVozULt1sxWGexftxAyLOIobW9AkEAy71ywVay2/eEPJxKiqLty5vPdGSNCEfprGWScuJFtCCT3kIHt5Hz3nv4GOr7TBtVjfFXVJXDygrck454QtnDdQJAQwKYW/Hm8T9+EqxWQU5VebG8HDZ9hy/4uE/WJ+aRIxWl5GCrEyrzmLoVKVOj1MPAoT1+voBgIlcXHlGyo/uwvQJATDSbNKe6FL6a7pTC6OISMcMgsHdfrQZA5+PCCEiMK9i3lj16Z3YnEkOqQ5+4yPm8YXAw6wlthyfS4zo5zz9mdQJBAMXtL6hOzJz4Ku+v+d/svVor4HNcBDd0aTVXVqTiw5+OrwY6xYw3HugSLFbnP28B6npkyuzb7mdVSugfJR3o0Ig=', 1, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-13 11:20:43', NULL, NULL);
INSERT INTO `sys_subapp` VALUES ('root', 'root', '中软业务流程管理系统', 'http://localhost:8080', 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC0KIcpNLoKwar/pEqugU6UyAKJZR4jrZzybv0XvDcVHAQucmcetFFawiGETu7o87jxpYVn86h/5wizETQyMkZLJgbkXsvUeapyw/DuQ2mwNtcGpisKiOQfBjenVlB2PrOMEz0b+ddPnEPGQfKvl6/NW6BqsPNoCgTZr1KCTELdFwIDAQAB', 'MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALQohyk0ugrBqv+kSq6BTpTIAollHiOtnPJu/Re8NxUcBC5yZx60UVrCIYRO7ujzuPGlhWfzqH/nCLMRNDIyRksmBuRey9R5qnLD8O5DabA21wamKwqI5B8GN6dWUHY+s4wTPRv510+cQ8ZB8q+Xr81boGqw82gKBNmvUoJMQt0XAgMBAAECgYAELax02DQ4a6V7qGPTCEy6RqlIZn/8c9pgBRL5qfPdGXk/WofpvmTkpfwBBZOk84ImmKAveQc/cCxTeuKrB1959Lwvfp46zxZwBJyXRD0PZYJJFFTMI7n8X4L+cuOdYG2516DYUz/DrD2gsTURdO3TeKEaaY+/Ao7Vfw4ifBLCgQJBAOUznB7UjENXnI9NXLw4Ts6Rr8384NfvrxMit4e+OJTHbVkbs9846btD8KCIof7tzHJmuEEFuu1sNvH2q9LERnMCQQDJOPhIbvUgjkWGDVt+YUYG+h/V19obtnELcOa7XE0mtH7V+P5R2IQxn1cskgaGA/ULfI2cBy4SLi84tIE38AHNAkEAxlWlqEwW4ANFWAAC2mEDWN9auS41RsaCwlVNaapifeQgrHY7bVNrdn/xDomNXPNb8jcPBD04J1F4LmNdBjKQHwJAS0hEI51XmkmAG0oOC/dKorubg37RPAMCwhwIRtqehreKqHUSeP5/Xq1ViPhM9oCughCkTlalVunugtGNcqcZlQJAMsBqfrGgUa5OHEo1Lp4apFXy/yyENJ3vWFFJhBDDQMJav+OUjxiWyjaauBAqKr7M5Y7FvyTdG+NEVaNXtNuVdQ==', 1, '', '1', 'c8f1ba6c7cf842409aba43206e9f7442', '2020-10-30 17:50:19', NULL, NULL);

-- ----------------------------
-- Table structure for sys_success_audit
-- ----------------------------
DROP TABLE IF EXISTS `sys_success_audit`;
CREATE TABLE `sys_success_audit`  (
  `audit_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '日志表id',
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `audit_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '操作类型',
  `audit_module` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '审计模块',
  `audit_data` blob NULL COMMENT '审计数据',
  `app_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '系统id',
  `audit_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求url',
  `client_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访问ip',
  `audit_level` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '日志级别',
  `audit_time` datetime(0) NOT NULL COMMENT '审计时间',
  `extend_data` blob NULL COMMENT '扩展数据',
  `description` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '操作描述',
  PRIMARY KEY (`audit_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作成功日志表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_worktime_calendar
-- ----------------------------
DROP TABLE IF EXISTS `sys_worktime_calendar`;
CREATE TABLE `sys_worktime_calendar`  (
  `work_cal_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键，uuid',
  `work_year` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '年份',
  `work_month` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '月份',
  `work_date` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '工作日期',
  `day_of_week` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '星期几',
  `is_work_day` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否为工作日 0假期 1工作日',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`work_cal_id`) USING BTREE,
  INDEX `idx_sys_worktime_calendar_work_year`(`work_year`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '工作日期表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_worktime_workhour
-- ----------------------------
DROP TABLE IF EXISTS `sys_worktime_workhour`;
CREATE TABLE `sys_worktime_workhour`  (
  `work_hour_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键，uuid',
  `begin_time` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '开始时间',
  `end_time` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '结束时间',
  `description` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '时间段描述',
  `create_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_user` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`work_hour_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '工作时间表' ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
