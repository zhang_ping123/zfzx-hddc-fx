define(['jquery'], function(){

    var PUBLIC_KEY = "";
    var LOGIN_CAPTCHA = false;
	var init = function () {
	    initSecret();
        // 初始化布局相关
        initLayout();
        // 初始化事件
        initEvent();
        // 初始化验证码
        captcha = new Captcha();
        captcha.init();

    };
    var initSecret = function(){
        PUBLIC_KEY = $("#publickey").val();
        LOGIN_CAPTCHA = $("#isCaptcha").val()
    };
    // 初始化布局相关
    function initLayout() {
    	// 触发响应式布局的关键点（浏览器宽度）
    	var bigSize = 1440,
    		smallSize = 650;
    	// 响应式布局
        response();
        // 布局设置好后再显示整个页面，防止页面抖动
        $('.login-body').fadeIn(100);
    	// 动态计算宽高
        calculatePosition();

        // 页面触发resize事件时，重新布局
        $(window).resize(function(){
        	response();
        	calculatePosition();
        });

        // 响应式布局
        function response(){
            var bodyWidth = $('body').width();
            if(bodyWidth > bigSize){
                $('.style-change').attr('href', getServer() + '/static/app/platform/page/login/css/login-large-screen.css');
            }else if(bodyWidth <= bigSize && bodyWidth >= smallSize){
                $('.style-change').attr('href', getServer() + '/static/app/platform/page/login/css/login-medium-screen.css');
            }else{
            	$('.style-change').attr('href', getServer() + '/static/app/platform/page/login/css/login-small-screen.css');
            }
        }

        // 计算某些元素位置
        function calculatePosition(){
        	var bodyWidth = $('body').width();
        	if (bodyWidth > smallSize) {
        		var $leftIntroduce = $(".left-introduce");
        		var leftContentHeight = $leftIntroduce.innerHeight();
        		$leftIntroduce.css('marginTop', -leftContentHeight / 2 + 'px');

        		var $loginIn = $(".login-in-box");
        		var loginInHeight = $loginIn.innerHeight();
        		$loginIn.css('marginTop', -loginInHeight / 2 + 'px');
        	}else{
        		var bodyHeight = $('body').height();
        		var $loginIn = $(".login-in-box");
        		var loginHeight = $loginIn.height();
        		var marginTop;
        		if (bodyHeight > loginHeight) {
        			marginTop = (bodyHeight - loginHeight) / 3
        			$loginIn.css('marginTop', marginTop + 'px');
        		}
        	}
        }
    }

    // 缓存jquery对象
	var $userName = $("#web_login_name"),	// 用户名
		$password = $(".password-input"),			// 密码
		$form = $('#web_login_form'),		// 表单
		$submitBtn = $('#web_btn_submit'),	// 提交按钮
		$register = $('#web_btn_register'),	// 注册按钮
		$errorTextWrap = $('#errorInfo');		// 错误信息

	// 验证码对象
	var captcha = null;

	// 初始化事件
    function initEvent() {
    	// 登录按钮事件绑定
    	$submitBtn.bind('click', function(){
    		debugger;
        	submitForm();
        });
        // 注册按钮事件绑定
    	$register.bind('click', function(){
    		debugger;
        	window.location.href = getServer()+"/platform/register"
        });
        // 给用户名、密码、验证码绑定按回车触发登录提交事件
        $userName.on("keypress", function(event){
        	if (event.keyCode == '13') {
        		submitForm();
            }
        });
        $password.on("keypress", function(event){
        	if (event.keyCode == '13') {
        		submitForm();
            }
        });
    }

    // 表单提交
    function submitForm() {
    	// 先验证表单数据
        if(!validate()) return;
        loadingStart();
        // 提交请求
    	$.ajax({
            url: getServer() + "/platform/loginIn",
            type: "post",
            data: getPostData(),
            dataType: "json",
            success: function (res) {
                loginCallback(res );
            }
        });
    }

    // 提交时loading start
    function loadingStart() {
    	$submitBtn.html("登录中……");
    	window.setTimeout(loadingEnd, 5000); // 5秒后自动关闭
    }

    // 提交返回时loading end
    function loadingEnd() {
    	$submitBtn.html("立即登录");
    }


    //输入框验证
    function validate() {
    	var result = true;

    	var userName = $userName.val();
    	var password = $password.val();
    	var captchaValue = captcha.getValue();
    	if (LOGIN_CAPTCHA == 'true') {
    		if($.trim(userName) == ''
    			|| $.trim(password) == ''
    			|| $.trim(captchaValue) == ''){

    			result = false;
    			setErrorText("请输入用户名、密码、验证码");
			}
    	} else {
    		if($.trim(userName) == ''
    			|| $.trim(password) == ''){

    			result = false;
    			setErrorText("请输入用户名、密码");
			}
    	}

        return result;
    };

    // 设置错误信息
    function setErrorText(text) {
    	$errorTextWrap.html(text);
    }

    // 验证码类
    function Captcha() {
    	// 验证码输入框
    	this.$captcha = $("#web_captcha");
    	// 验证码图片
		this.$captchaImg = $('#web_captcha_img');
    }

    // 初始化
    Captcha.prototype.init = function() {
    	var _this = this;

    	// 点击验证码图片刷新验证码
        _this.$captchaImg.bind('click', function(){
            _this.refresh();
        });

        // 给验证码绑定回车提交事件
    	_this.$captcha.on("keypress", function(event){
        	if (event.keyCode == '13') {
        		submitForm();
            }
        });
    }

    // 刷新
    Captcha.prototype.refresh = function() {
    	var _this = this;

    	if (LOGIN_CAPTCHA == 'true') {
    		// 更新图片的src
    		_this.$captchaImg.attr('src', _this.$captchaImg.attr('src') + '?t=' + new Date().getTime());
    		// 清空验证码输入框的值
    		_this.$captcha.val('');
    	}
    }

    // 获取值
    Captcha.prototype.getValue = function() {
    	var _this = this;

    	return $.trim(_this.$captcha.val());
    }

    // 表单内容清空
    function resetFrom() {
    	// 置空所有input
    	$form.find('input').val('');
    	// 登录名获取焦点
        $userName.focus();
    };

    // 获取登录提交数据
    function getPostData() {
    	var userCode = $.trim($userName.val());
    	// 加密
        var password = $.trim($password.val());

        var encrypt = new JSEncrypt();
        encrypt.setPublicKey(PUBLIC_KEY);
        password = encrypt.encrypt(password);

        var data = {
            userCode: userCode,
            password: password,
            captcha: captcha.getValue()
        };

        return data
    };

    // 登录回调
    function loginCallback(res) {
        var status = res.status;
    	if(status == "SUCCESS"){ // 验证成功

            if(top.RELOGIN && typeof(top.RELOGIN.reLoginSuccess)=="function"){ // 如果是ajax请求，是在页面出现弹出框，所以这时登录成功之后需要调用relogin的回调接口
                //调用框架的方法，这样遮罩层才能盖住整个页面
                top.RELOGIN.reLoginSuccess();
            }else{
                window.location.hash ? window.location.reload() : (window.location = res.loginSuccessUrl);
            }
        } else {
        	var errorText = '';
        	if(status == "INACTIVE"){
                errorText = '用户未激活';
                resetFrom();
            }else if(status == "ACCOUNT_NOT_EXIST"){
        		errorText = '账号或密码错误';
        		resetFrom();
        	}else if(status == "PASSWORD_ERROR"){
        		errorText = '账号或密码错误';
        		if (res.retryTimes > 0) {
        			errorText += '，还能登录' + res.retryTimes + '次';
        		}
        		resetFrom();
        	}else if(status == "TOO_MANY_FAILURES"){
        		errorText = '登录失败次数过多，账号已被冻结，请' + res.remainLockedTime + '后再尝试';
        	}else if(status == "ADMIN_LOCKED"){
        	    errorText = "账号已被管理员冻结!";
            }
        	else if(status == "CAPTHCA_ERROR"){
        		errorText = '输入的验证码错误';
        	}else if(status == "SESSION_EXPIRED"){
        		alert('会话已过期');
                window.location.reload();
        	}else if(status == "ERROR"){
                errorText = res.message;
        	}
        	setErrorText(errorText);
            // 只要出错就刷新验证码
        	captcha.refresh();
        	loadingEnd();
        }
    };

    return {
    	init: init
    }
});