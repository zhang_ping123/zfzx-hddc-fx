/**
 * 引用JS和CSS头文件
 */
var rootPath = getRootPath(); //项目路径
//TODO rootPath待处理
rootPath = "";
var storage = window.localStorage;
storage.setItem("rootPath",rootPath);

/**
 * 动态加载CSS和JS文件
 */
var dynamicLoading = {
    css: function(path,style){
        if(!path || path.length === 0){
            throw new Error('argument "path" is required!');
        }
        if(style){
            document.write('<link rel="stylesheet" type="text/css" href="' + path + '" class="'+style+'">');
        }else{
            document.write('<link rel="stylesheet" type="text/css" href="' + path + '">');
        }
    },
    js: function(path, charset){
        if(!path || path.length === 0){
            throw new Error('argument "path" is required!');
        }
        document.write('<script charset="' + (charset ? charset : "utf-8") + '" src="' + path + '"></script>');
    }
};

/**
 * 取得项目路径
 * @author wul
 */
function getRootPath() {
    //取得主机地址后的目录
    var pathName = window.document.location.pathname;
    //取得项目名
    var name = pathName.substring(0, pathName.substr(1).indexOf("/") + 1);
    return name;
}


//动态加载项目 JS文件
//底部 结束
dynamicLoading.js(rootPath + "/static/core/platform/lib/jsencrypt/jsencrypt.min.js", "utf-8");
dynamicLoading.js(rootPath + "/static/core/platform/lib/requirejs/require.js", "utf-8");
//底部模板调用 结束

<!-- register样式-->
//dynamicLoading.css(rootPath + "/static/core/platform/page/login/css/login-common.css");
dynamicLoading.css(rootPath + "/static/fwcore/platform/page/register/css/register-large-screen.css","style-change");


