<!--  定义全局变量 -->

var storage = window.localStorage;
var rootPath = storage.getItem("rootPath");
function getServer() {
    return rootPath;
}
var baseUrl = getServer() || "/";
require.config({
    baseUrl : baseUrl,
    paths : {
        "jquery":"/static/core/platform/lib/jquery/jquery-2.1.3.min",
        "EasyUI":"static/core/platform/lib/easyui/jquery.easyui.min",
        "EasyUI-lang":"static/core/platform/lib/easyui/locale/easyui-lang-zh_CN",
        "Register":"/static/fwcore/platform/page/register/js/register",
        "Bootstrap":"static/core/platform/lib/bootstrap/js/bootstrap.min",
        "Validate":"/static/core/platform/lib/jquery/plugins/validate/jquery.validate.min",
        "combotree":"static/core/platform/components/util/combotree",                
        "ZTree":"static/core/platform/lib/zTree/js/jquery.ztree.all-3.5",
    	"ZTreeExhide":"static/core/platform/lib/zTree/js/jquery.ztree.exhide-3.5.min",
    	"ArtTemplate":"static/core/platform/lib/artTemplate/template",
        "ArtTemplateNative":"static/core/platform/lib/artTemplate/template-native",
    	"UtilDir":"static/core/platform/components/util",
	 	"ZTreeCss":"static/core/platform/lib/zTree/css/zTreeStyle/csTreeStyle"	 	        
    },
    shim:{
        "Bootstrap":["jquery"],
        "EasyUI-lang":["EasyUI"],
        "EasyUI":["jquery"],
        "Angular":{"exports":"angular"},
        "Angular-ui-router":["Angular"],
        "ZTree":["jquery"],
        "DateCN":["Date"],
        //"JQuery.validate.extra":["JQuery.validate"],
        //"JQuery.validate.message":["JQuery.validate"],
        "Uploader":["WebUploader"],
        "ZTreeExhide":["ZTree"]
    },
    //urlArgs:"v=0.9.2",
    map:{
        '*':{
            'css':"static/core/platform/lib/requirejs/plugin/require-css2/css",
//          'text':"static/core/platform/lib/requirejs/plugin/text"
        }
    }
});
require(["static/fwcore/platform/page/register/js/register"], function(Register){
    Register.init();
});