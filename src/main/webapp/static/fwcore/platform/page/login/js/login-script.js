<!--  定义全局变量 -->

var storage = window.localStorage;
//var rootPath = storage.getItem("platform_rootPath");
var rootPath =document.getElementById("contextPath").value;
function getServer() {
    return rootPath;
}
var baseUrl = getServer() || "/";
require.config({
    baseUrl : baseUrl,
    paths : {
        "jquery":"/hdcfx/static/core/platform/lib/jquery/jquery-2.1.3.min",
        "Login":"/hdcfx/static/fwcore/platform/page/login/js/login"
    }
});
require(["Login"], function(Login){
    Login.init();
});