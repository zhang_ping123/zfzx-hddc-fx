define([
	"UtilDir/util",
	"UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
	"Date","DateCN", "css!DateCss",
	"EasyUI","EasyUI-lang"
    ],function(Util, Tool, SelectOrg, SearchBlock){

    var sysPath =  getServer() + "/static/app/zdyh/yhschool";

    var yhSchoolFormHtml = sysPath + "/views/yhSchoolForm.html";
	var getYhSchoolController = function(){
		return getServer() + "/yhSchools";
	};

	/**
	 * 页面初始化
	 */
	var init = function(){
        initSearchBlock();
        queryBtnBind();
		initProvinceSelect();
		initCitySelect();
		initAreaSelect();
		initBuildingLevelSelect();
		initYhLevelSelect();
		initStatusSelect();
		initCreateTimeDate();
		createYhSchoolGrid();
	};
	var initProvinceSelect = function () {
		$.ajax({
			url: getYhSchoolController() + "/getValidDictItemsByDictCode/" + "auditLevel",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("provinceSelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initCitySelect = function () {
		$.ajax({
			url: getYhSchoolController() + "/getValidDictItemsByDictCode/" + "auditLevel",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("citySelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initAreaSelect = function () {
		$.ajax({
			url: getYhSchoolController() + "/getValidDictItemsByDictCode/" + "auditLevel",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("areaSelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initBuildingLevelSelect = function () {
		$.ajax({
			url: getYhSchoolController() + "/getValidDictItemsByDictCode/" + "auditLevel",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("buildingLevelSelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initYhLevelSelect = function () {
		$.ajax({
			url: getYhSchoolController() + "/getValidDictItemsByDictCode/" + "auditLevel",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("yhLevelSelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initStatusSelect = function () {
		$.ajax({
			url: getYhSchoolController() + "/getValidDictItemsByDictCode/" + "auditLevel",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("statusSelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initCreateTimeDate = function() {
		$("#createTimeCondition").datetimepicker({
			//设置使用语言：cn是自定义的中文版本，还可以扩展其他语言版本
			language : "cn",
			//输出格式化
			format : 'yyyy-mm-dd',
			//直接选择‘今天’
			todayBtn : true,
			//设置最精确的时间选择视图
			minView : 'month',
			//高亮当天日期
			todayHighlight : true,
			//选择完毕后自动关闭
			autoclose : true
		});
	};

		var initProvinceSelectFrom = function () {
		$.ajax({
			url: getYhSchoolController() + "/getValidDictItemsByDictCode/" + "auditLevel",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("province");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initCitySelectFrom = function () {
		$.ajax({
			url: getYhSchoolController() + "/getValidDictItemsByDictCode/" + "auditLevel",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("city");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initAreaSelectFrom = function () {
		$.ajax({
			url: getYhSchoolController() + "/getValidDictItemsByDictCode/" + "auditLevel",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("area");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initBuildingLevelSelectFrom = function () {
		$.ajax({
			url: getYhSchoolController() + "/getValidDictItemsByDictCode/" + "auditLevel",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("buildingLevel");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initYhLevelSelectFrom = function () {
		$.ajax({
			url: getYhSchoolController() + "/getValidDictItemsByDictCode/" + "auditLevel",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("yhLevel");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};

	var initSearchBlock = function(){
        SearchBlock.init("searchBlock");
	};
	var queryBtnBind = function(){
        $("#btnSearch").click(function () {
            createYhSchoolGrid();
        });
        $("#btnReset").click(function () {
			$("#cztNameCondition").val("");
	            $("#provinceSelect").val("");
	            $("#citySelect").val("");
	            $("#areaSelect").val("");
	            $("#buildingLevelSelect").val("");
	            $("#yhLevelSelect").val("");
	            $("#statusSelect").val("");
				$("#createTimeCondition").val("");
				$("#createUserCondition").val("");
			});
	};
	var createYhSchoolGrid= function() {
        $("#yhSchoolGrid").datagrid({
            url:getYhSchoolController() + "/queryYhSchools",
			method:"GET",
            fitColumns: true,
            autoRowHeight: false,
            columns:[[
                {field: "ck", checkbox: true},
				{
					field:'cztName',
					title:'承灾体名称',
					width:'10%',
					align:'center',
					formatter:function(value,rowData,rowIndex){
						return '<a href="#" onclick="_editYhSchool(\'' + rowData.id + '\');"> '+rowData.cztName+' </a> '
					}
				},
				{
					field:'province',
					title:'省',
					width:'10%',
					align:'center',
				},
				{
					field:'city',
					title:'市',
					width:'10%',
					align:'center',
				},
				{
					field:'area',
					title:'区',
					width:'10%',
					align:'center',
				},
				{
					field:'buildingArea',
					title:'建筑面积',
					width:'10%',
					align:'center',
				},
				{
					field:'buildingHeight',
					title:'建筑高度',
					width:'8%',
					align:'center',
				},
				{
					field:'buildingLevel',
					title:'建筑等级',
					width:'10%',
					align:'center',
				},
				{
					field:'yhLevel',
					title:'隐患等级',
					width:'10%',
					align:'center',
				},
				{
					field:'status',
					title:'状态',
					width:'7%',
					align:'center',
				},
				{
					field:'createTime',
					title:'创建时间',
					width:'7%',
					sortable: true,
					align:'center',
				},
				{
					field:'createUser',
					title:'创建人',
					width:'7%',
					align:'center',
				},
            ]],
            toolbar: [{
                iconCls: 'fa fa-plus-circle',
                text:"添加",
                handler: function(){
                    addYhSchool();
                }
            },{
                iconCls: 'fa fa-trash-o',
                text:"删除",
                handler: function(){
                    deleteYhSchool();
                }
            }],
            queryParams:{
				cztName: $("#cztNameCondition").val(),
                province: $("#provinceSelect").val(),
                city: $("#citySelect").val(),
                area: $("#areaSelect").val(),
                buildingLevel: $("#buildingLevelSelect").val(),
                yhLevel: $("#yhLevelSelect").val(),
                status: $("#statusSelect").val(),
				createTime: $("#createTimeCondition").val(),
				createUser: $("#createUserCondition").val(),
            },
            pagination: true,
            pageSize: 10
        });
    };
	var formValidator = function(){
		$("#yhSchoolForm").validate({
			rules : {
				cztName : {
					required : true,
				},
				longitude : {
					required : true,
				},
				latitude : {
					required : true,
				},
				province : {
					required : true,
				},
				city : {
					required : true,
				},
				area : {
					required : true,
				},
				buildingArea : {
					required : true,
				},
				buildingHeight : {
					required : true,
				},
				buildingLevel : {
					required : true,
				},
				yhLevel : {
					required : true,
				},
				id : {
					required : true,
				},
				status : {
					required : true,
				},
				isValid : {
					required : true,
				},
				updateUser : {
					required : true,
				},
				updateTime : {
					required : true,
				},
				createUser : {
					required : true,
				},
				createTime : {
					required : true,
				},
			},
			messages : {
				cztName : {
					required : "承灾体名称不允许为空!",
				},
				longitude : {
					required : "经度不允许为空!",
				},
				latitude : {
					required : "纬度不允许为空!",
				},
				province : {
					required : "省不允许为空!",
				},
				city : {
					required : "市不允许为空!",
				},
				area : {
					required : "区不允许为空!",
				},
				buildingArea : {
					required : "建筑面积不允许为空!",
				},
				buildingHeight : {
					required : "建筑高度不允许为空!",
				},
				buildingLevel : {
					required : "建筑等级不允许为空!",
				},
				yhLevel : {
					required : "隐患等级不允许为空!",
				},
				id : {
					required : "主键ID不允许为空!",
				},
				status : {
					required : "状态不允许为空!",
				},
				isValid : {
					required : "是否有效,0:无效,1:有效不允许为空!",
				},
				updateUser : {
					required : "修改人不允许为空!",
				},
				updateTime : {
					required : "修改时间不允许为空!",
				},
				createUser : {
					required : "创建人不允许为空!",
				},
				createTime : {
					required : "创建时间不允许为空!",
				},
			}
		});
	};
	var getYhSchool = function(id){
		$.ajax({
			url: getYhSchoolController() + "/"+id,
			type: "get",
			success: function (data) {
				Tool.deserialize("yhSchoolForm", data);
			}
		});
	};

	var addYhSchool = function () {
		var slidebar = Util.slidebar({
			url: yhSchoolFormHtml,
			width: "800px",
			cache: false,
			close : true,
			afterLoad: function () {
				initProvinceSelectFrom();
				initCitySelectFrom();
				initAreaSelectFrom();
				initBuildingLevelSelectFrom();
				initYhLevelSelectFrom();
				formValidator();
				$("#saveBtn").on("click", function () {
					if($("#yhSchoolForm").valid()){
						var data = Tool.serialize("yhSchoolForm");
						$.ajax({
							url: getYhSchoolController() ,
                            contentType:"application/json",
							data: JSON.stringify(data),
							type: "post",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
                                createYhSchoolGrid();
							}
						});
					}
				});
			}
		});
	};
	window._editYhSchool = function(yhSchoolId) {
		var slidebar = Util.slidebar({
			url: yhSchoolFormHtml,
			width: "800px",
			cache: false,
			close : true,
			afterLoad: function () {
				initProvinceSelectFrom();
				initCitySelectFrom();
				initAreaSelectFrom();
				initBuildingLevelSelectFrom();
				initYhLevelSelectFrom();
				formValidator();
				getYhSchool(yhSchoolId);
				$("#saveBtn").on("click", function () {
					if($("#yhSchoolForm").valid()){
						var data = Tool.serialize("yhSchoolForm");
						$.ajax({
							url: getYhSchoolController(),
                            contentType:"application/json",
                            data: JSON.stringify(data),
							type: "put",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
								createYhSchoolGrid();
							}
						});
					}
				});
			}
		});
	};
	var deleteYhSchool = function() {
		var rows = $("#yhSchoolGrid").datagrid("getSelections");
		if (rows == null || rows.length == 0) {
			Util.alert("请选择一行数据!");
			return;
		}
		Util.confirm("是否要删除选中的数据?", function() {
			var ids = "";
			$.each(rows, function(i, row){
				ids += row.id + ",";

			});
			ids = ids.substr(0,ids.length - 1);
			$.ajax({
				url: getYhSchoolController() ,
				data: {
					ids : ids
				},
				type: "delete",
				success: function (data) {
					createYhSchoolGrid();
				}
			});
		}, function() {
			return;
		});

	};

	return {
		init:init
	};
});
