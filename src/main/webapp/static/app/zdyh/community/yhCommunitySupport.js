define([
	"UtilDir/util",
	"UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
	"Date","DateCN", "css!DateCss",
	"EasyUI","EasyUI-lang"
    ],function(Util, Tool, SelectOrg, SearchBlock){

    var sysPath =  getServer() + "/static/app/zdyh/community";

    var yhCommunityFormHtml = sysPath + "/views/yhCommunityForm.html";
	var getYhCommunityController = function(){
		return getServer() + "/yhCommunitys";
	};

	/**
	 * 页面初始化
	 */
	var init = function(){
        initSearchBlock();
        queryBtnBind();
        initcreateProvince();
		initBuildingLevelSelect();
		initYhLevelSelect();
		initStatusSelect();
		initCreateTimeDate();
		createYhCommunityGrid();
	};
    var initcreateProvince = function () {
        var html = "";
        $("#citySelect").append(html);
        $("#areaSelect").append(html);
        $.ajax({
            url: "/app/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#provinceSelect").append(html);
            }
        });
        $("#provinceSelect").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#citySelect option").remove();
            $("#citySelect").append(html);
            $("#areaSelect option").remove();
            $("#areaSelect").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#citySelect").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: "/app/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#citySelect").append(html);
                }
            });
        });
        $("#citySelect").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#areaSelect option").remove();
            $("#areaSelect").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: "/app/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#areaSelect").append(html);
                }
            });
        });
    }
	var initProvinceSelect = function () {
		$.ajax({
			url: getYhCommunityController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("provinceSelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initAreaSelect = function () {
		$.ajax({
			url: getYhCommunityController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("areaSelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initCitySelect = function () {
		$.ajax({
			url: getYhCommunityController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("citySelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initBuildingLevelSelect = function () {
		$.ajax({
			url: getYhCommunityController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("buildingLevelSelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initYhLevelSelect = function () {
		$.ajax({
			url: getYhCommunityController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("yhLevelSelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initStatusSelect = function () {
		$.ajax({
			url: getYhCommunityController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("statusSelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initCreateTimeDate = function() {
		$("#createTimeCondition").datetimepicker({
			//设置使用语言：cn是自定义的中文版本，还可以扩展其他语言版本
			language : "cn",
			//输出格式化
			format : 'yyyy-mm-dd',
			//直接选择‘今天’
			todayBtn : true,
			//设置最精确的时间选择视图
			minView : 'month',
			//高亮当天日期
			todayHighlight : true,
			//选择完毕后自动关闭
			autoclose : true
		});
	};
		var initProvinceSelectFrom = function () {
		$.ajax({
			url: getYhCommunityController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("province");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initCitySelectFrom = function () {
		$.ajax({
			url: getYhCommunityController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("city");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initAreaSelectFrom = function () {
		$.ajax({
			url: getYhCommunityController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("area");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initBuildingLevelSelectFrom = function () {
		$.ajax({
			url: getYhCommunityController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("buildingLevel");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initYhLevelSelectFrom = function () {
		$.ajax({
			url: getYhCommunityController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("yhLevel");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};

	var initSearchBlock = function(){
        SearchBlock.init("searchBlock");
	};
	var queryBtnBind = function(){
        $("#btnSearch").click(function () {
            createYhCommunityGrid();
        });
        $("#btnReset").click(function () {
			$("#cztNameCondition").val("");
	            $("#provinceSelect").val("");
	            $("#areaSelect").val("");
	            $("#citySelect").val("");
	            $("#buildingLevelSelect").val("");
	            $("#yhLevelSelect").val("");
	            $("#statusSelect").val("");
				$("#createTimeCondition").val("");
				$("#createUserCondition").val("");
			});
	};
	var createYhCommunityGrid= function() {
        $("#yhCommunityGrid").datagrid({
            url:getYhCommunityController() + "/queryYhCommunitys",
			method:"GET",
            fitColumns: true,
            autoRowHeight: false,
            columns:[[
            	{field: "ck", checkbox: true},
				{
					field:'cztName',
					title:'承灾体名称',
					width:'10%',
					align:'center',
					formatter:function(value,rowData,rowIndex){
						return '<a href="#" onclick="_editYhCommunity(\'' + rowData.id + '\');"> '+rowData.cztName+' </a> '
					}
				},
				{
					field:'province',
					title:'省',
					width:'10%',
					align:'center',
				},
				{
					field:'city',
					title:'市',
					width:'10%',
					align:'center',
				},
				{
					field:'area',
					title:'区',
					width:'10%',
					align:'center',
				},
				{
					field:'buildingArea',
					title:'建筑面积',
					width:'10%',
					align:'center',
				},
				{
					field:'buildingHeight',
					title:'建筑高度',
					width:'8%',
					align:'center',
				},
				{
					field:'buildingLevel',
					title:'建筑等级',
					width:'10%',
					align:'center',
				},
				{
					field:'yhLevel',
					title:'隐患等级',
					width:'10%',
					align:'center',
				},
				{
					field:'status',
					title:'状态',
					width:'7%',
					align:'center',
				},
				{
					field:'createTime',
					title:'创建时间',
					width:'7%',
					sortable: true,
					align:'center',
				},
				{
					field:'createUser',
					title:'创建人',
					width:'7%',
					align:'center',
				},
            ]],
            toolbar: [{
                iconCls: 'fa fa-plus-circle',
                text:"添加",
                handler: function(){
                    addYhCommunity();
                }
            },{
                iconCls: 'fa fa-trash-o',
                text:"删除",
                handler: function(){
                    deleteYhCommunity();
                }
            }],
            queryParams:{
				cztName: $("#cztNameCondition").val(),
                province: $("#provinceSelect").val(),
                area: $("#areaSelect").val(),
                city: $("#citySelect").val(),
                buildingLevel: $("#buildingLevelSelect").val(),
                yhLevel: $("#yhLevelSelect").val(),
                status: $("#statusSelect").val(),
				createTime: $("#createTimeCondition").val(),
				createUser: $("#createUserCondition").val(),
            },
            pagination: true,
            pageSize: 10
        });
    };
	var formValidator = function(){
		$("#yhCommunityForm").validate({
			rules : {
				cztName : {
					required : true,
				},
				longitude : {
					required : true,
				},
				latitude : {
					required : true,
				},
				province : {
					required : true,
				},
				city : {
					required : true,
				},
				area : {
					required : true,
				},
				buildingArea : {
					required : true,
				},
				buildingHeight : {
					required : true,
				},
				buildingLevel : {
					required : true,
				},
				yhLevel : {
					required : true,
				},
				id : {
					required : true,
				},
				isValid : {
					required : true,
				},
				status : {
					required : true,
				},
				createTime : {
					required : true,
				},
				createUser : {
					required : true,
				},
				updateUser : {
					required : true,
				},
				updateTime : {
					required : true,
				},
			},
			messages : {
				cztName : {
					required : "承灾体名称不允许为空!",
				},
				longitude : {
					required : "经度不允许为空!",
				},
				latitude : {
					required : "纬度不允许为空!",
				},
				province : {
					required : "省不允许为空!",
				},
				city : {
					required : "市不允许为空!",
				},
				area : {
					required : "区不允许为空!",
				},
				buildingArea : {
					required : "建筑面积不允许为空!",
				},
				buildingHeight : {
					required : "建筑高度不允许为空!",
				},
				buildingLevel : {
					required : "建筑等级不允许为空!",
				},
				yhLevel : {
					required : "隐患等级不允许为空!",
				},
				id : {
					required : "主键ID不允许为空!",
				},
				isValid : {
					required : "是否有效,0:无效,1:有效不允许为空!",
				},
				status : {
					required : "状态不允许为空!",
				},
				createTime : {
					required : "创建时间不允许为空!",
				},
				createUser : {
					required : "创建人不允许为空!",
				},
				updateUser : {
					required : "修改人不允许为空!",
				},
				updateTime : {
					required : "修改时间不允许为空!",
				},
			}
		});
	};
	var getYhCommunity = function(id){
		$.ajax({
			url: getYhCommunityController() + "/"+id,
			type: "get",
			success: function (data) {
				Tool.deserialize("yhCommunityForm", data);
			}
		});
	};

	var addYhCommunity = function () {
		var slidebar = Util.slidebar({
			url: yhCommunityFormHtml,
			width: "800px",
			cache: false,
			close : true,
			afterLoad: function () {
				initProvinceSelectFrom();
				initCitySelectFrom();
				initAreaSelectFrom();
				initBuildingLevelSelectFrom();
				initYhLevelSelectFrom();
				formValidator();
				$("#saveBtn").on("click", function () {
					if($("#yhCommunityForm").valid()){
						var data = Tool.serialize("yhCommunityForm");
						$.ajax({
							url: getYhCommunityController() ,
                            contentType:"application/json",
							data: JSON.stringify(data),
							type: "post",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
                                createYhCommunityGrid();
							}
						});
					}
				});
			}
		});
	};
	window._editYhCommunity = function(yhCommunityId) {
		var slidebar = Util.slidebar({
			url: yhCommunityFormHtml,
			width: "800px",
			cache: false,
			close : true,
			afterLoad: function () {
				initProvinceSelectFrom();
				initCitySelectFrom();
				initAreaSelectFrom();
				initBuildingLevelSelectFrom();
				initYhLevelSelectFrom();
				formValidator();
				getYhCommunity(yhCommunityId);
				$("#saveBtn").on("click", function () {
					if($("#yhCommunityForm").valid()){
						var data = Tool.serialize("yhCommunityForm");
						$.ajax({
							url: getYhCommunityController(),
                            contentType:"application/json",
                            data: JSON.stringify(data),
							type: "put",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
								createYhCommunityGrid();
							}
						});
					}
				});
			}
		});
	};
	var deleteYhCommunity = function() {
		var rows = $("#yhCommunityGrid").datagrid("getSelections");
		if (rows == null || rows.length == 0) {
			Util.alert("请选择一行数据!");
			return;
		}
		Util.confirm("是否要删除选中的数据?", function() {
			var ids = "";
			$.each(rows, function(i, row){
				ids += row.id + ",";

			});
			ids = ids.substr(0,ids.length - 1);
			$.ajax({
				url: getYhCommunityController() ,
				data: {
					ids : ids
				},
				type: "delete",
				success: function (data) {
					createYhCommunityGrid();
				}
			});
		}, function() {
			return;
		});

	};

	return {
		init:init
	};
});
