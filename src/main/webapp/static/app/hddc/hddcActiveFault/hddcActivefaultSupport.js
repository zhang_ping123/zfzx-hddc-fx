define([
	"UtilDir/util",
	"UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
    "static/app/hddc/hddccjcommon/hddccjCommon",
    "static/app/hddc/hddcActiveFault/statisticsEcherts",
    "UtilDir/loading",
    "static/app/hddc/hddcActiveFault/amapSearchDistrict",
    "static/app/hddc/hddcActiveFault/tmap",
    "css!static/app/hddc/hddcActiveFault/administrativeregions",
	"Date","DateCN", "css!DateCss",
	"EasyUI","EasyUI-lang"
    ],function(Util, Tool, SelectOrg, SearchBlock,HddccjCommon,statisticsEcherts,Loading){
    var sysPath =  getServer() + "/static/app/hddc/hddcActiveFault";

    var hddcActivefaultFormHtml = sysPath + "/views/hddcActivefaultForm.html";
	var getHddcActivefaultController = function(){
		return getServer() + "/hddc/hddcActivefaults";
	};

    var getHddcFxBoundaryController = function(){
        return getServer() + "/boundary";
    };

    var map=null;
    var lay1=null;
    var lay2=null;
    var lay3 =null;
    var lay4 =null;
    var lay5 =null;
    var lay6 =null;
    var lay7 =null;

	/**
	 * 页面初始化
	 */
	var init = function(){
        //initSearchBlock();
        //queryBtnBind();
        //initcreateProvince();
        //HddccjCommon.initProjectSelect("projectNameCondition"); //初始化查询的项目名称
		//createHddcActivefaultGrid();
        initcreateProvince();
        if(map==null) {
            //初始化地图对象

            /*
            var imageURL = "http://t0.tianditu.gov.cn/img_w/wmts?" +
                "SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=img&STYLE=default&TILEMATRIXSET=w&FORMAT=tiles" +
                "&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}&tk=acce9f1241bb60d5164939ee53173caf";
             */
            //天地图矢量
            var imageURL1="https://fxpc.mem.gov.cn/data_preparation/171dd1d5-bbb3-483a-a7f6-50d6d98cee1f/d9ffc071-50dd-4780-a617-673d17921e22/wmts?service=wmts&request=gettile&version=1.0.0&layer=vec&style=default&tilematrixset=c&format=tiles&tilematrix={z}&tilerow={y}&tilecol={x}&geokey=94E99EB4FEB9B48CD5348F7BCF5BB620";
            lay1 = new T.TileLayer(imageURL1, {minZoom: 1, maxZoom: 18});
            //天地图矢量注记
            var imageURL2="https://fxpc.mem.gov.cn/data_preparation/171dd1d5-bbb3-483a-a7f6-50d6d98cee1f/13f8b69d-98dc-4276-bbf0-9c4c372acabc/wmts?service=wmts&request=gettile&version=1.0.0&layer=cva&style=default&tilematrixset=c&format=tiles&tilematrix={z}&tilerow={y}&tilecol={x}&geokey=94E99EB4FEB9B48CD5348F7BCF5BB620";
            lay2 = new T.TileLayer(imageURL2, {minZoom: 1, maxZoom: 18});
            //天地图影像
            var imageURL3="https://fxpc.mem.gov.cn/data_preparation/171dd1d5-bbb3-483a-a7f6-50d6d98cee1f/16646cf6-305e-45b3-9be9-81f7dfc9813f/wmts?service=wmts&request=gettile&version=1.0.0&layer=img&style=default&tilematrixset=c&format=tiles&tilematrix={z}&tilerow={y}&tilecol={x}&geokey=94E99EB4FEB9B48CD5348F7BCF5BB620";
            lay3 = new T.TileLayer(imageURL3, {minZoom: 1, maxZoom: 18});
            //天地图影像注记
            var imageURL4="https://fxpc.mem.gov.cn/data_preparation/171dd1d5-bbb3-483a-a7f6-50d6d98cee1f/0f7d6202-bdad-4cc7-a3b1-e86ec950f35d/wmts?service=wmts&request=gettile&version=1.0.0&layer=cia&style=default&tilematrixset=c&format=tiles&tilematrix={z}&tilerow={y}&tilecol={x}&geokey=94E99EB4FEB9B48CD5348F7BCF5BB620";
            lay4 = new T.TileLayer(imageURL4, {minZoom: 1, maxZoom: 18});
            //天地图地形
            var imageURL5="https://fxpc.mem.gov.cn/data_preparation/171dd1d5-bbb3-483a-a7f6-50d6d98cee1f/9a10d9a2-e2be-45bb-8ddc-4fa3004022e4/wmts?service=wmts&request=gettile&version=1.0.0&layer=ter&style=default&tilematrixset=c&format=tiles&tilematrix={z}&tilerow={y}&tilecol={x}&geokey=94E99EB4FEB9B48CD5348F7BCF5BB620";
            lay5 = new T.TileLayer(imageURL5, {minZoom: 1, maxZoom: 18});
            //天地图地形注记
            var imageURL6="https://fxpc.mem.gov.cn/data_preparation/171dd1d5-bbb3-483a-a7f6-50d6d98cee1f/2f817c65-a621-48d7-91d7-fb4fd12657a8/wmts?service=wmts&request=gettile&version=1.0.0&layer=cat&style=default&tilematrixset=c&format=tiles&tilematrix={z}&tilerow={y}&tilecol={x}&geokey=94E99EB4FEB9B48CD5348F7BCF5BB620";
            lay6 = new T.TileLayer(imageURL6, {minZoom: 1, maxZoom: 18});
            //0.8米影像
            var imageURL7="https://fxpc.mem.gov.cn/data_preparation/171dd1d5-bbb3-483a-a7f6-50d6d98cee1f/4d481300-7a88-4b5b-9bb7-424ff6a5abea/wmts?service=WMTS&request=GetTile&version=1.0.0&layer=img08&style=default&tilematrixset=c&format=tiles&tilematrix={z}&tilerow={y}&tilecol={x}&geokey=94E99EB4FEB9B48CD5348F7BCF5BB620";
            lay7 = new T.TileLayer(imageURL7, {minZoom: 1, maxZoom: 18});
            var config = {
                projection: "EPSG:4326",
                minZoom: 4,
                layers: [lay1,lay2]
                //layers: [lay1,lay2,lay3,lay4]
            };
            //lay1.visible=false;
            //https://fxpc.mem.gov.cn/wmts?service=wmts&request=gettile&version=1.0.0&layer=cva&style=default&tilematrixset=c&format=tiles&tilematrix=9&tilerow=75&tilecol=423
            map = new T.Map("hddctask-map",config);
            map.clearOverLays();
            //设置显示地图的中心点和级别
            map.centerAndZoom(new T.LngLat(116.40969,39.89945),4);


            var control=new T.Control.Zoom();
            map.addControl(control);

            //创建比例尺控件对象
            var scale = new T.Control.Scale();
            map.addControl(scale);

            map.enableScrollWheelZoom();
            map.enableDrag();

            //创建鹰眼控件对象

            /*
            var miniMap = new T.Control.OverviewMap({
                isOpen: true,
                size: new T.Point(150, 150)
            });
            map.addControl(miniMap);

             */

            var bodyWidth = window.innerWidth-15;//1920px
            var mapHeight =  window.innerHeight-40;//796px
            var mapWidth = bodyWidth;//1469px

            $("#hddctask-mapDiv").css("height",mapHeight + "px");
            $("#hddctask-mapDiv").css("width",mapWidth + "px");
            $("#hddctask-map").css("height",mapHeight + "px");
            $("#hddctask-map").css("width",mapWidth + "px");

            map.checkResize();
            $("#toolbox-toolsDiv").css("z-index", "1000");
            $("#toolbox-toolsDiv").show();

            $("#administrative-regionsDiv").css("z-index", "1001");
            $("#administrative-regionsDiv").css("display", "block");
            $("#administrative-regionsDiv").show();

            $("#maptype_displaybutton").css("z-index", "900");
            //$("#maptype_displaybutton").css("display", "block");
            $("#maptype_displaybutton").show();

        }
        map.clearOverLays();
        initAreaRegions();
        initMapEvent();
        initActiveFaultData();
	};

    function arearegion(name,regions){
        this.name=name;
        this.regions=regions;
    }

    var allAreaRegions=[];
	function initAreaRegions(){
        allAreaRegions=[];
        allAreaRegions.length=0;
        //addcityandarea();
        //addprovince();
    }

    /*function addcityandarea() {
        var dataStr=allProvinceAndCityAndAreaRegion.initRegins;
        //debugger;
        var xzs=dataStr.split("[xz]");
        for(var i=0;i<xzs.length;i++){
            var xz=xzs[i];
            var singxzs=xz.split(":");
            if(singxzs.length>=2){
                var name=singxzs[0];
                //console.log(name);
                if(name.indexOf("省")>0||name.indexOf("自治区")>0){
                    //debugger;
                    // console.log(name);
                    continue;
                }
                var xzregions=singxzs[1].split("|||");
                if(xzregions.length>0){
                    var xzregion=xzregions[0];//暂时只取第一个范围
                    var lonlats=xzregion.split(";");
                    var points=[];
                    for(var j=0;j<lonlats.length;j++){
                        if(lonlats[j]==null||lonlats[j]==""){
                            continue;
                        }
                        var lonlat=lonlats[j].split(",");
                        //for(var ii=0;ii<lonlat.length)

                        if(lonlat.length>0) {
                            var pp = [];
                            pp.push(parseFloat(lonlat[0]));
                            pp.push(parseFloat(lonlat[1]));
                            //pp[0] = lonlat[0];
                            //pp[1] = lonlat[1];
                            points.push(pp);
                        }
                    }
                    var ar = new arearegion(name,points);
                    allAreaRegions.push(ar);
                }
            }
        }
    }

    function addprovince() {
        var dataStr=allProvinceAndCityAndAreaRegion.initPorvinceRegions;
        debugger;
        var xzs=dataStr.split("[xz]");
        for(var i=0;i<xzs.length;i++){
            var xz=xzs[i];
            var singxzs=xz.split(":");
            if(singxzs.length>=2){
                var name=singxzs[0];
                var xzregions=singxzs[1].split("|||");
                if(xzregions.length>0){
                    var xzregion=xzregions[0];//暂时只取第一个范围
                    var lonlats=xzregion.split(";");
                    var points=[];
                    for(var j=0;j<lonlats.length;j++){
                        if(lonlats[j]==null||lonlats[j]==""){
                            continue;
                        }
                        var lonlat=lonlats[j].split(",");
                        //for(var ii=0;ii<lonlat.length)

                        if(lonlat.length>0) {
                            var pp = [];
                            pp.push(parseFloat(lonlat[0]));
                            pp.push(parseFloat(lonlat[1]));
                            //pp[0] = lonlat[0];
                            //pp[1] = lonlat[1];
                            points.push(pp);
                        }
                    }
                    var ar = new arearegion(name,points);
                    allAreaRegions.push(ar);
                }
            }
        }
    }*/

    var initcreateProvince = function () {
        var html = "";
        $("#citySelect").append(html);
        $("#areaSelect").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#provinceSelect").append(html);
            }
        });
        $("#provinceSelect").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#citySelect option").remove();
            $("#citySelect").append(html);
            $("#areaSelect option").remove();
            $("#areaSelect").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#citySelect").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#citySelect").append(html);
                }
            });
        });
        $("#citySelect").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#areaSelect option").remove();
            $("#areaSelect").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#areaSelect").append(html);
                }
            });
        });
    }
    var initcreateProvinceForm = function () {
        var html = "";
        $("#city").append(html);
        $("#area").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#province").append(html);
            }
        });
        $("#province").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#city option").remove();
            $("#city").append(html);
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#city").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#city").append(html);
                }
            });
        });
        $("#city").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#area").append(html);
                }
            });
        });
    }

	function initMapEvent(){
        $("#imgoperate-DisplayDivSelect").unbind("click");
        $("#imgoperate-DisplayDivSelect").click(function () {
            if ($("#imgoperate-DisplayDivSelect").attr("src") == "/hdcfx/static/app/hddc/hddccjtask/images/maptype0.png"){
                $("#imgoperate-DisplayDivSelect").attr("src", "/hdcfx/static/app/hddc/hddccjtask/images/maptype1.png");
                $("#maptype-maptypeDisplayDiv").css("display", "block");
            }else{
                $("#imgoperate-DisplayDivSelect").attr("src", "/hdcfx/static/app/hddc/hddccjtask/images/maptype0.png");
                $("#maptype-maptypeDisplayDiv").css("display", "none");
            }
            $("#maptype-maptypeDisplayDiv").css("z-index", "1100");
        });
        $("#maptype1-DisplayImage").unbind("click");
        $("#maptype1-DisplayImage").click(function () {
            map.removeLayer(lay3);
            map.removeLayer(lay4);
            map.removeLayer(lay5);
            map.removeLayer(lay6);
            map.removeLayer(lay7);
            $("#maptype-maptypeDisplayDiv").css("display", "none");
            $("#imgoperate-DisplayDivSelect").attr("src", "/hdcfx/static/app/hddc/hddccjtask/images/maptype0.png");
        });
        $("#maptype2-DisplayImage").unbind("click");
        $("#maptype2-DisplayImage").click(function () {
            hitIndex=0;
            debugger;
            map.removeLayer(lay5);
            map.removeLayer(lay6);
            map.removeLayer(lay7);
            map.addLayer(lay3);
            map.addLayer(lay4);
            $("#maptype-maptypeDisplayDiv").css("display", "none");
            $("#imgoperate-DisplayDivSelect").attr("src", "/hdcfx/static/app/hddc/hddccjtask/images/maptype0.png");
        });
        $("#maptype3-DisplayImage").unbind("click");
        $("#maptype3-DisplayImage").click(function () {
            hitIndex=0;
            map.removeLayer(lay3);
            map.removeLayer(lay4);
            map.removeLayer(lay7);
            map.addLayer(lay5);
            map.addLayer(lay6);
            $("#maptype-maptypeDisplayDiv").css("display", "none");
            $("#imgoperate-DisplayDivSelect").attr("src", "/hdcfx/static/app/hddc/hddccjtask/images/maptype0.png");
        });
        $("#maptype4-DisplayImage").unbind("click");
        $("#maptype4-DisplayImage").click(function () {
            hitIndex=0;
            map.removeLayer(lay3);
            map.removeLayer(lay4);
            map.removeLayer(lay5);
            map.removeLayer(lay6);
            map.addLayer(lay7);
            $("#maptype-maptypeDisplayDiv").css("display", "none");
            $("#imgoperate-DisplayDivSelect").attr("src", "/hdcfx/static/app/hddc/hddccjtask/images/maptype0.png");
        });

        $("#imghzdbx-DisplayDiv").unbind("click");
        $("#imghzdbx-DisplayDiv").click(function () {
            $("#statisticalanalysis-contextMenuDiv").hide();
            document.getElementById("input-card").style.display='none';
            $("#imgadministrativeregions-DisplayDiv").attr("src", "/hdcfx/static/app/hddc/hddccjtask/images/xingzhengquyuoff.png");
            if ($("#imghzdbx-DisplayDiv").attr("src") == "/hdcfx/static/app/hddc/hddccjtask/images/dbx.png") {
                $("#imghzdbx-DisplayDiv").attr("src", "/hdcfx/static/app/hddc/hddccjtask/images/dbx-r.png");
                createRectTaskRegion();
            } else {
                $("#imghzdbx-DisplayDiv").attr("src", "/hdcfx/static/app/hddc/hddccjtask/images/dbx.png");
                //$("#statisticalanalysis-ContextMenuLi").hide();
                if(polygon!=null){
                    map.removeOverLay(polygon);
                    polygon=null;
                }
                if(polygonTaskTool!=null){
                    polygonTaskTool.close();
                    polygonTaskTool.clear();
                    polygonTaskTool=null;
                }
            }

        });
        $("#imgadministrativeregions-DisplayDiv").unbind("click");
        $("#imgadministrativeregions-DisplayDiv").click(function () {
            $("#statisticalanalysis-contextMenuDiv").hide();
            $("#imghzdbx-DisplayDiv").attr("src", "/hdcfx/static/app/hddc/hddccjtask/images/dbx.png");
            //$("#statisticalanalysis-ContextMenuLi").hide();
            if(polygon!=null){
                map.removeOverLay(polygon);
                polygon=null;
            }
            if(polygonTaskTool!=null){
                polygonTaskTool.close();
                polygonTaskTool.clear();
                polygonTaskTool=null;
            }
            if ($("#imgadministrativeregions-DisplayDiv").attr("src") == "/hdcfx/static/app/hddc/hddccjtask/images/xingzhengquyuoff.png") {
                $("#imgadministrativeregions-DisplayDiv").attr("src", "/hdcfx/static/app/hddc/hddccjtask/images/xingzhengquyuon.png");
                document.getElementById("input-card").style.display='block';
                $("#input-card").css("z-index", "1001");

            } else {
                document.getElementById("input-card").style.display='none';
                $("#imgadministrativeregions-DisplayDiv").attr("src", "/hdcfx/static/app/hddc/hddccjtask/images/xingzhengquyuoff.png");
            }
        });
        //行政区收起操作
        $("#close-regionDiv").click(function () {
            document.getElementById("input-card").style.display='none';
            $("#imgadministrativeregions-DisplayDiv").attr("src", "/hdcfx/static/app/hddc/hddccjtask/images/xingzhengquyuoff.png");

        });
        $("#statiscics-regionDiv").click(function () {
            var province=$('#provinceSelect').val();
            var city=$('#citySelect').val();
            var area=$('#areaSelect').val();
            var selectArea=null;
            if(province==null){
                Util.alert("请先选择要统计的行政区");
                return;
            }else if(province!=null&&city==null){
                //selectArea=getselectArea(province);
                getselectAreaAll(province,null,null,0);
            }else if(city!=null&&area==null){
                getselectAreaAll(province,city,null,1);
            }else if(area!=null){
                getselectAreaAll(province,city,area,2);
            }
            /*if(selectArea==null){
                Util.alert("所选行政区内没有活断层。");
                return;
            }
            debugger;
            statisticalDatas=[];
            statisticalDatas.length=0;
            for (var i = 0; i < activefaults.length; i++) {
                var info = activefaults[i];
                for(var j=0;j<info.points.length;j++) {
                    var afpoint=[];
                    afpoint[0]=info.points[j].getLng();
                    afpoint[1]=info.points[j].getLat();
                    if (initPointInsidePolygon(selectArea.regions, afpoint)) {
                        statisticalDatas.push(info);
                        break;
                    }
                }
            }
            if (statisticalDatas != null && statisticalDatas.length > 0) {
                _viewCensusStatisticsInfo();
            }else{
                Util.alert("所选区域没有活动断层数据。");
            }*/
        });
        statisticalanalysisTMapEvent();
    }

    function displayresultDatas(name,data,type){
	    var coords=data;
	    if(type==0){
            coords = data[name];
        }
        coords = coords.split("|");
        var points = [];
        for (var j = 0; j < coords.length; j++) {
            var lonlats = coords[j].split(";");
            for (var i = 0;i < lonlats.length;i++) {
                var lonlat = lonlats[i].split(",");
                var lon = parseFloat(lonlat[0]);
                var lat = parseFloat(lonlat[1]);
                var pp = [];
                pp.push(lon);
                pp.push(lat);
                points.push(pp);
            }
        }
        var selectArea = new arearegion(name,points);
        if(selectArea==null){
            Util.alert("所选行政区内没有活断层。");
            return;
        }
        debugger;
        statisticalDatas=[];
        statisticalDatas.length=0;
        for (var i = 0; i < activefaults.length; i++) {
            var info = activefaults[i];
            for(var j=0;j<info.points.length;j++) {
                var afpoint=[];
                afpoint[0]=info.points[j].getLng();
                afpoint[1]=info.points[j].getLat();
                if (initPointInsidePolygon(selectArea.regions, afpoint)) {
                    statisticalDatas.push(info);
                    break;
                }
            }
        }
        if (statisticalDatas != null && statisticalDatas.length > 0) {
            _viewCensusStatisticsInfo();
        }else{
            Util.alert("所选区域没有活动断层数据。");
        }
    }

    function getselectArea(name){
	    var selectTheArea=null;
	    for(var i=0;i<allAreaRegions.length;i++){
	        var region=allAreaRegions[i];
	        if(name==region.name){
                selectTheArea = region;
                break;
            }
        }
	    return selectTheArea;
    }

    function getselectAreaAll(province,city,area,type){
        if(type==0){
            $.ajax({
                url: getHddcFxBoundaryController() + "/getSingleProvinceBoundary?provinceName=" + province,
                type: "get",
                success: function (data) {
                    displayresultDatas(province,data,0);
                }
            });
        }else if(type==1){
            $.ajax({
                url: getHddcFxBoundaryController() + "/getSingleCityBoundary?provinceName=" + province + "&cityName=" + city,
                type: "get",
                success: function (data) {
                    displayresultDatas(city,data,1);
                }
            });
        }else if(type==2){
            $.ajax({
                url: getHddcFxBoundaryController() + "/getSingleAreaBoundary?provinceName=" + province + "&cityName=" + city + "&areaName=" + area,
                type: "get",
                success: function (data) {
                    displayresultDatas(area,data,2);
                }
            });
        }
    }

    function statisticalanalysisTMapEvent() {
        debugger;
        //地图鼠标点击事件
        map.addEventListener("click", function (p, btn) {
            var Lon = p.lnglat.getLng();
            var Lat = p.lnglat.getLat();
            if (btn == 1) {//鼠标左键
                //获取鼠标位置的经纬度
                //$("#statisticalanalysis-MousePositionLon").val(Lon);
                //$("#statisticalanalysis-MousePositionLat").val(Lat);
            }
        });
        map.addEventListener("contextmenu", function (p) {
            //将像素坐标转换成经纬度坐标
            var Lon = p.lnglat.getLng();
            var Lat = p.lnglat.getLat();

            $("#statisticalanalysis-contextMenuDiv").hide();
            var hitPoint = [];
            hitPoint[0] = Lon;
            hitPoint[1] = Lat;
            //map.pointToPixel
            debugger;
            if (statisticalPoints != null && statisticalPoints.length > 2) {
                if (initPointInsidePolygon(statisticalPoints, hitPoint)) {
                    //initShowContextMenu("#statisticalanalysis", 1, p.layerPoint.x, p.layerPoint.y);
                    var lnglat = p.lnglat;
                    var pp = map.lngLatToContainerPoint(lnglat);
                    var left = pp.x + "px";
                    var top = pp.y + "px";
                    $("#statisticalanalysis-contextMenuDiv").css("left",left);
                    $("#statisticalanalysis-contextMenuDiv").css("top",top);
                    //显示右键菜单
                    $("#statisticalanalysis-contextMenuDiv").show();
                    statisticalCreateContextMenu();
                    return;
                }else{
                    $("#statisticalanalysis-contextMenuDiv").hide();
                }
            }
        });
    }

    function statisticalCreateContextMenu() {
        //设置右键菜单点击事件
        $("#statisticalanalysis-ContextMenuLi").off("click");
        $("#statisticalanalysis-ContextMenuLi").on("click", function () {
            if (statisticalDatas != null && statisticalDatas.length > 0) {
                $("#statisticalanalysis-contextMenuDiv").hide();
                //alert(statisticalDatas.length);
                _viewCensusStatisticsInfo();
            }else{
                $("#statisticalanalysis-contextMenuDiv").hide();
                Util.alert("所选区域没有活动断层数据。");
            }

        });
    }

    window._viewCensusStatisticsInfo = function() {
        var slidebar = Util.slidebar({
            url: hddcActivefaultFormHtml,
            width: "500px",
            cache: false,
            close : true,

            afterOpen: function () {
                $("#circleactivefaultNum").val(statisticalDatas.length);
                $('.nav-map.nav-tabs').hide();
                // console.log(activefaults);
                var chartsData = [];
                var colors = [];
                for(var i = 0;i<statisticalDatas.length;i++){
                    // 判断chartsData中是否存在这个color
                    var res = chartsData.findIndex(function (item) {
                        return item.name == statisticalDatas[i].color;
                    })
                    if(res == -1){
                        var obj = {name:statisticalDatas[i].color,value:1}
                        chartsData.push(obj);
                    }else{
                        chartsData[res].value ++;
                    }
                }
                ////红色：全新世，紫色：晚更新世，绿色：早中更新世，黑色：前第四世
                /*for(var i = 0;i<chartsData.length;i++){
                    colors.push(chartsData[i].name);
                    if(chartsData[i].name == "#F63A3A"){
                        chartsData[i].name = "全新世";
                    }else if(chartsData[i].name == "#B605D7"){
                        chartsData[i].name = "晚更新世";
                    }else if(chartsData[i].name == "#00BD34"){
                        chartsData[i].name = "早中更新世";
                    }else if(chartsData[i].name == "#5B5353"){
                        chartsData[i].name = "前第四世";
                    }
                }*/
                var allcolors=["#F63A3A",
                               "#B605D7",
                               "#00BD34",
                               "#5B5353",
                               "#18195B",
                               "#65D75D",
                               "#40F0F6",
                               "#F631C0",
                               "#215B58",
                               "#9E12D7",
                               "#D6EEF6"];
                for(var i = 0;i<chartsData.length;i++){
                    if(i<allcolors.length-1){
                        colors.push(allcolors[i]);
                    }else{
                        colors.push(allcolors[i%allcolors.length]);
                    }
                }

                statisticsEcherts.createMyPieEcharts({
                    id:"pieChart",
                    centerTitle:"活动断层",
                    data:chartsData,
                    color:colors

                })
            },
            afterLoad: function () {
            }
        });
    };

    function initShowContextMenu(str, type, x, y) {
	    debugger;
        //设置右键菜单位置
        var left = x;
        //获取右键菜单位置left最大值
        var maxLeft = $("body").width() - ($(str + "-leftContextMenuDiv").width() + 1);
        if (left > maxLeft) {
            left = maxLeft;
        }

        var top = y - $(str + "-mapDiv").offset();
        //获取右键菜单位置top最大值
        var maxTop = $("body").height() - ($(str + "-leftContextMenuDiv").height() + 1);
        if (top > maxTop) {
            top = maxTop;
        }
        if (type == 1) {
            $(str + "-contextMenuDiv").css("left", x-15 + "px");
            $(str + "-contextMenuDiv").css("top", y+40 + "px");
            $(str + "-contextMenuDiv").show();
        }
    }

    var polygonTaskTool;
    var polygonArrays=[];
    var statisticalPoints;
    var statisticalDatas=[];
    var polygon = null;
    function createRectTaskRegion(){
        $("#statisticalanalysis-contextMenuDiv").hide();
        if(polygon!=null){
            map.removeOverLay(polygon);
            polygon=null;
        }
        var config = {
            strokeColor: "red", //折线颜色
            fillColor: "#FFFFFF",    //填充颜色。当参数为空时，折线覆盖物将没有填充效果
            strokeWeight: "3px", //折线的宽度，以像素为单位
            strokeOpacity: 1,  //折线的透明度，取值范围0 - 1
            fillOpacity: 0.2,        //填充的透明度，取值范围0 - 1
            showLabel: false             //是否显示页面，默认为true.
        };
        polygonTaskTool=new T.PolygonTool(map,config);
        polygonTaskTool.open();
        polygonTaskTool.addEventListener("draw", onDrawHZPolygon);

        function onDrawHZPolygon(bounds) {
            polygon = new T.Polygon(bounds.currentLnglats, {
                strokeColor: "red",
                strokeWeight: 3,
                strokeOpacity: 1,
                fillOpacity: 0.2
            });
            map.addOverLay(polygon);//添加多边形

            var points = lnglatdataswitch(polygon.getLngLats());
            statisticalPoints=points;
debugger;
            statisticalDatas=[];
            statisticalDatas.length=0;


            for (var i = 0; i < activefaults.length; i++) {
                var info = activefaults[i];
                //console.log(info.polyline);
                for(var j=0;j<info.points.length;j++) {
                    var afpoint=[];
                    afpoint[0]=info.points[j].getLng();
                    afpoint[1]=info.points[j].getLat();
                    if (initPointInsidePolygon(points, afpoint)) {
                        statisticalDatas.push(info);
                        break;
                    }
                }
            }

            polygonArrays.push(polygon);
            polygonTaskTool.close();
            polygonTaskTool.clear();
        }
    }

    function initPointInsidePolygon(pointsArray, LonLat) {
        var bool = false;//点不在面内
        if (LonLat != null) {
            var x = LonLat[0];
            var y = LonLat[1];
            var length = pointsArray.length;
            for (var i = 0, j = length - 1; i < length; j = i++) {
                var xi = pointsArray[i][0];
                var yi = pointsArray[i][1];
                var xj = pointsArray[j][0];
                var yj = pointsArray[j][1];
                var intersect = ((yi > y) != (yj > y))
                    && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
                if (intersect) {
                    bool = !bool;
                }
            }
        }
        return bool;
    }

    function lnglatdataswitch(points){
        var points1=[];
        if (points != null && points.length > 0) {
            for (var i = 0; i < points[0].length; i++) {
                var pp = [];
                pp.push(parseFloat(points[0][i].getLng()));
                pp.push(parseFloat(points[0][i].getLat()));
                points1.push(pp);
            }
        }
        return points1;
    }

    function activefault(name,color,points) {
        this.name=name;
        this.color=color;//红色：全新世，紫色：晚更新世，绿色：早中更新世，黑色：前第四世
        this.points=points;
    }

    //获取线路|面经纬度数组
    var getLonLatArray = function(LonLatString){
        var LonLatArray = [];//经纬度数组
        var strArray = LonLatString.split(";");
        if(strArray!=null && strArray.length>0){
            for(var i=0;i<strArray.length;i++){
                var str = strArray[i];
                var lonlatStrArray = str.split(",");
                if(lonlatStrArray!=null && lonlatStrArray.length==2){
                    var lonStr = lonlatStrArray[0];
                    var latStr = lonlatStrArray[1];
                    if(lonStr!="" && latStr!=""){
                        var lon = parseFloat(lonStr);
                        var lat = parseFloat(latStr);
                        if(lon>0 && lon<180 && lat>0 && lat<90){
                            //经纬度
                            var lonlat = [lon,lat];
                            //添加经纬度
                            LonLatArray.push(lonlat);
                        }
                    }
                }
            }
        }
        return LonLatArray;
    };

    var getLatestActivePeriodName = function (number) {
        if(number==33320000){
            return "Qh";
        }else if(number == 33320300){
            return "Qh3";
        }else if(number == 33320200){
            return "Qh2";
        }else if(number == 33310300){
            return "Qp3";
        }else if(number == 33310302){
            return "Qp32";
        }else if(number == 33310301){
            return "Qp31";
        }else if(number == 33310200){
            return "Qp2";
        }else if(number == 33310100){
            return "Qp1";
        }else if(number == 33310000){
            return "Qp";
        }else if(number == 33300000){
            return "Q";
        }else if(number == 33310120){
            return "Qp1+2";
        }else if(number == -33300000){
            return "Pre-Q";
        }else if(number == 33220000){
            return "N2";
        }else if(number == 33210000){
            return "N1";
        }else if(number == 33130000){
            return "E3";
        }else if(number == 33120000){
            return "E2";
        }else if(number == 33110000){
            return "E1";
        }else if(number == 33200000){
            return "N";
        }else if(number == 33100000){
            return "E";
        }else if(number == 33000000){
            return "Cz";
        }else if(number == 32300000){
            return "K";
        }else if(number == 32200000){
            return "J";
        }else if(number == 32100000){
            return "T";
        }else if(number == 32000000){
            return "Mz";
        }else if(number == 31600000){
            return "P";
        }else if(number == 31500000){
            return "C";
        }else if(number == 31400000){
            return "D";
        }else if(number == 31300000){
            return "S";
        }else if(number == 31200000){
            return "O";
        }else if(number == 31100000){
            return "Cambrian";
        }else if(number == 31000000){
            return "Pz";
        }else if(number == 23300000){
            return "Z";
        }else if(number == 20000000){
            return "Pt";
        }else if(number == 10000000){
            return "Ar";
        }else if(number == -31100000){
            return "Pre-Cambrian";
        }else if(number == ""||number == ""){
            return "年代不明";
        }else{
            return "年代不明";
        }
    };

    var activefaults=[];
    var isloadsuccess=false;
    function initActiveFaultData() {
        var loading = Loading({
            text : "正在加载数据，请稍后。"
        });
        loading.show();
        isloadsuccess=false;
        activefaults = [];
        activefaults.length = 0;
        $.ajax({
            url: getHddcActivefaultController() + "/queryAllHddcActiveFaultDatas",
            type: "get",
            async: true,
            success: function (data) {//latestactiveperiod
                var faultdata = null;
                for (var i = 0; i < data.length; i++) {
                    //var LonLatString = data[i].extends5;//经纬度字符串
                    var LonLatStrings = data[i].extends5;//经纬度字符串
                    if (LonLatStrings == null || LonLatStrings == undefined) {
                        continue
                    }
                    var color=getLatestActivePeriodName(data[i].latestactiveperiod);
                    var datas = LonLatStrings.split("#");
                    for (var j = 0; j < datas.length; j++) {
                        var LonLatString = datas[j];
                        if (LonLatString != null && LonLatString != "") {
                            var LonLatArray = getLonLatArray(LonLatString);//获取经纬度数组
                            var uuid = data[i].uuid;
                            //在地图上添加线
                            if (LonLatArray != null && LonLatArray.length > 0) {
                                var lnglatArray = [];
                                for (var m = 0; m < LonLatArray.length; m++) {
                                    var lonlat = LonLatArray[m];
                                    var lon = lonlat[0];
                                    var lat = lonlat[1];
                                    var lnglat = new T.LngLat(lon, lat);
                                    lnglatArray.push(lnglat);
                                }
                                //创建线对象
                                var line = new T.Polyline(lnglatArray);
                                //向地图上添加线
                                map.addOverLay(line);

                                var info = new activefault(name, color, lnglatArray);
                                activefaults.push(info);

                            }
                        }
                    }
                }
                isloadsuccess=true;
            }
        });
        var interval = setInterval(function(){
            if(isloadsuccess) {
                loading.close();
                clearInterval(interval);
            }
        },1000);

    };


    var initcreateProvince = function () {
        var html = "";
        $("#citySelect").append(html);
        $("#areaSelect").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#provinceSelect").append(html);
            }
        });
        $("#provinceSelect").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#citySelect option").remove();
            $("#citySelect").append(html);
            $("#areaSelect option").remove();
            $("#areaSelect").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#citySelect").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#citySelect").append(html);
                }
            });
        });
        $("#citySelect").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#areaSelect option").remove();
            $("#areaSelect").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#areaSelect").append(html);
                }
            });
        });
    }
		var initStrikedirectionSelectFrom = function () {
		$.ajax({
			url: getHddcActivefaultController() + "/getValidDictItemsByDictCode/" + "CVD-16-Direction",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("strikedirection");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initDirectionSelectFrom = function () {
		$.ajax({
			url: getHddcActivefaultController() + "/getValidDictItemsByDictCode/" + "CVD-16-Direction",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("direction");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initFeatureSelectFrom = function () {
		$.ajax({
			url: getHddcActivefaultController() + "/getValidDictItemsByDictCode/" + "FaultTypeCVD",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("feature");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initLatestactiveperiodSelectFrom = function () {
		$.ajax({
			url: getHddcActivefaultController() + "/getValidDictItemsByDictCode/" + "AgeCVD",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("latestactiveperiod");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};

	var initSearchBlock = function(){
        SearchBlock.init("searchBlock");
	};
	var queryBtnBind = function(){
        $("#btnSearch").click(function () {
            createHddcActivefaultGrid();
        });
        $("#btnReset").click(function () {
            $("#provinceSelect").val("");
	            $("#citySelect").val("");
	            $("#areaSelect").val("");
				$("#projectNameCondition").val("");
				$("#faultsegmentnameCondition").val("");
			});
	};
	var createHddcActivefaultGrid= function() {
        $("#hddcActivefaultGrid").datagrid({
            url:getHddcActivefaultController() + "/queryHddcActivefaults",
			method:"GET",
            fitColumns: true,
            autoRowHeight: false,
            columns:[[
                {field:"ck",checkbox:true},
				{
					field:'province',
					title:'省',
					width:'20%',
					align:'center',
				},
				{
					field:'city',
					title:'市',
					width:'20%',
					align:'center',
				},
				{
					field:'area',
					title:'区（县）',
					width:'20%',
					align:'center',
				},
                {
                    field:'id',
                    title:'断层编号',
                    width:'20%',
                    align:'center',
                    formatter:function(value,rowData,rowIndex){
                        return '<a href="#" onclick="_editHddcActivefault(\'' + rowData.uuid + '\');"> '+rowData.id+' </a> '
                    }
                },
                {
                    field:'name',
                    title:'断层名称',
                    width:'20%',
                    align:'center',

                },
				{
					field:'projectName',
					title:'项目名称',
					width:'20%',
					align:'center',

				},
				{
					field:'taskName',
					title:'任务名称',
					width:'20%',
					align:'center',
				},
            ]],
            toolbar: [{
                iconCls: 'fa fa-plus-circle',
                text:"添加",
                handler: function(){
                    addHddcActivefault();
                }
            },{
                iconCls: 'fa fa-trash-o',
                text:"删除",
                handler: function(){
                    deleteHddcActivefault();
                }
            }, {
                iconCls: 'fa fa-upload',
                text: "导入",
                handler: function () {
                    importForm();
                }
            }, {
                iconCls: 'fa fa-download',
                text: "导出",
                handler: function () {
                    exportForm();
                }
            }],
            queryParams:{
                province: $("#provinceSelect").val(),
                city: $("#citySelect").val(),
                area: $("#areaSelect").val(),
				projectName: $("#projectNameCondition").val(),
				faultsegmentname: $("#faultsegmentnameCondition").val(),
            },
            pagination: true,
            pageSize: 10
        });
    };
	var formValidator = function(){
		$("#hddcActivefaultForm").validate({
			rules : {
				province : {
					required : true,
				},
				city : {
					required : true,
				},
				area : {
					required : true,
				},
                id: {
                    required: true,
                },
				projectName : {
					required : true,
				},
				taskName : {
					required : true,
				},
				name : {
					required : true,
				},
                scale : {
                    maxlength : 4,
                    digits: true,
                },
                strike : {
                    maxlength : 4,
                    digits: true,
                    min : 0,
                    max : 359,
                },
                clination : {
                    maxlength : 4,
                    digits: true,
                    min : 0,
                    max : 90,
                },
                length : {
                    maxlength : 8,
                    number : true,
                },
                width : {
                    maxlength : 8,
                    number : true,
                },
                fracturebeltwidth : {
                    maxlength : 8,
                    number : true,
                },
                vdisplaceest : {
                    maxlength : 8,
                    number : true,
                },
                vdisplaceer : {
                    maxlength : 8,
                    number : true,
                },
                hdisplaceest : {
                    maxlength : 8,
                    number : true,
                },
                hdisplaceer : {
                    maxlength : 8,
                    number : true,
                },
                tdisplaceest : {
                    maxlength : 8,
                    number : true,
                },
                tdisplaceer : {
                    maxlength : 8,
                    number : true,
                },
                avevrate : {
                    maxlength : 8,
                    number : true,
                },
                avevrateer : {
                    maxlength : 8,
                    number : true,
                },
                avehrate : {
                    maxlength : 8,
                    number : true,
                },
                avehrateer : {
                    maxlength : 8,
                    number : true,
                },
                starttimenewest : {
                    maxlength : 8,
                    number : true,
                },
                newvrate : {
                    maxlength : 8,
                    number : true,
                },
                newvrateer : {
                    maxlength : 8,
                    number : true,
                },
                newhrate : {
                    maxlength : 8,
                    number : true,
                },
                newhrateer : {
                    maxlength : 8,
                    number : true,
                },
                maxvrate : {
                    maxlength : 8,
                    number : true,
                },
                maxvrateer : {
                    maxlength : 8,
                    number : true,
                },
                maxhrate : {
                    maxlength : 8,
                    number : true,
                },
                maxhrateer : {
                    maxlength : 8,
                    number : true,
                },
                eqeventcount : {
                    maxlength : 4,
                    digits: true,
                },
                eqeventribottom : {
                    maxlength : 4,
                    digits: true,
                },
                eqeventritop : {
                    maxlength : 4,
                    digits: true,
                },
                maxrupturelength : {
                    maxlength : 4,
                    digits: true,
                    min : 0,
                    max : 500,
                },
                averagerupturelength : {
                    maxlength : 4,
                    digits: true,
                    min : 0,
                    max : 500,
                },
                elapsetimeforlatesteq : {
                    maxlength : 4,
                    digits: true,
                },
                slipdepthest : {
                    maxlength : 4,
                    digits: true,
                },
                slipdepther : {
                    maxlength : 4,
                    digits: true,
                },
                averagesliprateest : {
                    maxlength : 8,
                    number : true,
                },
                averagesliprateer : {
                    maxlength : 8,
                    number : true,
                },
                creeprateest : {
                    maxlength : 8,
                    number : true,
                },
                creeprateer : {
                    maxlength : 8,
                    number : true,
                },
                coseismicmaxslipest : {
                    maxlength : 8,
                    number : true,
                },
                coseismicaverageslipest : {
                    maxlength : 8,
                    number : true,
                },
                coseismicmaxsliper : {
                    maxlength : 8,
                    number : true,
                },
                coseismicaveragesliper : {
                    maxlength : 8,
                    number : true,
                },
                latestcoseismicslipest : {
                    maxlength : 8,
                    number : true,
                },
                latestcoseismicsliper : {
                    maxlength : 8,
                    number : true,
                },
                nsb2 : {
                    maxlength : 4,
                    digits: true,
                },
                nsb3 : {
                    maxlength : 4,
                    digits: true,
                },
                showcode : {
                    maxlength : 4,
                    digits: true,
                },
			},
			messages : {
				province : {
					required : "省不允许为空!",
				},
				city : {
					required : "市不允许为空!",
				},
				area : {
					required : "区（县）不允许为空!",
				},
                id: {
                    required: "编号不允许为空!",
                },
				projectName : {
					required : "项目名称不允许为空!",
				},
				taskName : {
					required : "任务名称不允许为空!",
				},
				name : {
					required : "断层名称不允许为空!",
				},
                scale : {
                    required : "工作底图比例尺（分母）要长度不大于4的整数!",
                },
                strike : {
                    required : "断层走向[度]要大于0的小于359的长度不大于4的整数!",
                },
                clination : {
                    required : "断层走向[度]要大于0的小于90的长度不大于4的整数!",
                },
                length : {
                    required : "长度[公里]要不为空的长度不大于8的浮点数!",
                },
                width : {
                    required : "破碎带宽度[米]要不为空的长度不大于8的浮点数!",
                },
                fracturebeltwidth : {
                    required : "变形带宽度[米]要不为空的长度不大于8的浮点数!",
                },
                vdisplaceest : {
                    required : "垂直位移[米]要不为空的长度不大于8的浮点数!",
                },
                vdisplaceer : {
                    required : "误差要不为空的长度不大于8的浮点数!",
                },
                hdisplaceest : {
                    required : "走向水平位移[米]要不为空的长度不大于8的浮点数!",
                },
                hdisplaceer : {
                    required : "误差要不为空的长度不大于8的浮点数!",
                },
                tdisplaceest : {
                    required : "水平//张缩位移[米]要不为空的长度不大于8的浮点数!",
                },
                tdisplaceer : {
                    required : "误差要不为空的长度不大于8的浮点数!",
                },
                avevrate : {
                    required : "平均垂直速率[米]要不为空的长度不大于8的浮点数!",
                },
                avevrateer : {
                    required : "误差要不为空的长度不大于8的浮点数!",
                },
                avehrate : {
                    required : "平均水平速率[毫米/年]要不为空的长度不大于8的浮点数!",
                },
                avehrateer : {
                    required : "误差要不为空的长度不大于8的浮点数!",
                },
                starttimenewest : {
                    required : "最新速率起算时间要不为空的长度不大于8的浮点数!",
                },
                newvrate : {
                    required : "最新垂直速率[毫米/年]要不为空的长度不大于8的浮点数!",
                },
                newvrateer : {
                    required : "误差要不为空的长度不大于8的浮点数!",
                },
                newhrate : {
                    required : "最新水平速率[毫米/年]要不为空的长度不大于8的浮点数!",
                },
                newhrateer : {
                    required : "误差要不为空的长度不大于8的浮点数!",
                },
                maxvrate : {
                    required : "最大垂直速率[米]要不为空的长度不大于8的浮点数!",
                },
                maxvrateer : {
                    required : "误差要不为空的长度不大于8的浮点数!",
                },
                maxhrate : {
                    required : "最大水平速率[毫米/年]要不为空的长度不大于8的浮点数!",
                },
                maxhrateer : {
                    required : "误差要不为空的长度不大于8的浮点数!",
                },
                eqeventcount : {
                    required : "古地震事件次数要长度不大于4的整数!",
                },
                eqeventribottom : {
                    required : "古地震复发间隔要长度不大于4的整数!",
                },
                eqeventritop : {
                    required : "古地震复发间隔上限要长度不大于4的整数!",
                },
                maxrupturelength : {
                    required : "最大破裂长度要大于0的小于500的长度不大于4的整数!",
                },
                averagerupturelength : {
                    required : "平均破裂长度要大于0的小于500的长度不大于4的整数!",
                },
                elapsetimeforlatesteq : {
                    required : "最晚地震离逝时间要长度不大于4的整数!",
                },
                slipdepthest : {
                    required : "错动深度要长度不大于4的整数!",
                },
                slipdepther : {
                    required : "错动深度误差要长度不大于4的整数!",
                },
                averagesliprateest : {
                    required : "平均滑动速率[毫米/年]要不为空的长度不大于8的浮点数!",
                },
                averagesliprateer : {
                    required : "平均滑动速率误差要不为空的长度不大于8的浮点数!",
                },
                creeprateest : {
                    required : "蠕动速率[毫米/年]要不为空的长度不大于8的浮点数!",
                },
                creeprateer : {
                    required : "蠕动速率误差要不为空的长度不大于8的浮点数!",
                },
                coseismicmaxslipest : {
                    required : "最大同震位移 [米]要不为空的长度不大于8的浮点数!",
                },
                coseismicaverageslipest : {
                    required : "平均同震位移 [米]要不为空的长度不大于8的浮点数!",
                },
                coseismicmaxsliper : {
                    required : "最大同震位移误差要不为空的长度不大于8的浮点数!",
                },
                coseismicaveragesliper : {
                    required : "平均同震位移误差要不为空的长度不大于8的浮点数!",
                },
                latestcoseismicslipest : {
                    required : "最新地震平均位移要不为空的长度不大于8的浮点数!",
                },
                latestcoseismicsliper : {
                    required : "最新地震平均位移误差要不为空的长度不大于8的浮点数!",
                },
                nsb2 : {
                    required : "断层符号下标位要长度不大于4的整数!",
                },
                nsb3 : {
                    required : "断层符号上标位要长度不大于4的整数!",
                },
                showcode : {
                    required : "断层显示码要长度不大于4的整数!",
                },
			}
		});
	};
	var getHddcActivefault = function(id){
		$.ajax({
			url: getHddcActivefaultController() + "/"+id,
			type: "get",
			success: function (data) {
                editProvince(data.province, data.city, data.area);
                // 回显项目名称
                $("#projectName").val(data.projectName);
                // 回显任务名称
                HddccjCommon.initEditTaskSelect("taskName",data.projectName);
                $("#taskName").val(data.taskName);
				Tool.deserialize("hddcActivefaultForm", data);
			}
		});
	};
    //导入
    var importForm = function () {

        $("#uploadModal").modal();
        $("#uploadModal").on("shown.bs.modal", function () {
            var url = getServer() + "/excel/活动断层-线.xls";
            $("#downloadZwExcelTemplate").attr("href", url);
        });

        $('#uploadButton').off("click");
        $("#uploadButton").on("click", function () {
            var uploadinput = document.getElementById("uploadFile");
            if (uploadinput.value == "") {
                Util.alert("上传前请先选择文件!");
                return;
            }
            var formData = new FormData();
            formData.append("file", uploadinput.files[0]);
            $.ajax({
                url: getHddcActivefaultController() + "/importDisaster",
                data: formData,
                processData: false, //因为data值是FormData对象，不需要对数据做处理。
                contentType: false,
                type: "POST",
                success: function (data) {
                    $('#uploadModal').modal('hide');
                    uploadinput.value = null;
                    Util.alert(data);
                    createHddcActivefaultGrid();
                }
            });
        });
    };
    //导出
    var exportForm = function () {
        var province = $("#provinceSelect").val();
        var city = $("#citySelect").val();
        var area = $("#areaSelect").val();
        var projectName = $("#projectNameCondition").val();
        var faultsegmentname= $("#faultsegmentnameCondition").val();
        window.location.href = getHddcActivefaultController() + "/exportFile?province="+province+"&city="+city+"&area="+area+"&projectName="+projectName+"&faultsegmentname="+faultsegmentname;
    };
    var initcreateProvinceForm = function () {
        var html = "";
        $("#city").append(html);
        $("#area").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#province").append(html);
            }
        });
        $("#province").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#city option").remove();
            $("#city").append(html);
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#city").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#city").append(html);
                }
            });
        });
        $("#city").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#area").append(html);
                }
            });
        });
    }
	var addHddcActivefault = function () {
		var slidebar = Util.slidebar({
			url: hddcActivefaultFormHtml,
			width: "800px",
			cache: false,
			close : true,
			afterLoad: function () {
                initcreateProvinceForm();
				initStrikedirectionSelectFrom();
				initDirectionSelectFrom();
				initFeatureSelectFrom();
				initLatestactiveperiodSelectFrom();
				formValidator();
                HddccjCommon.initProjectSelect("projectName"); //添加页面项目名称下拉
                HddccjCommon.initTaskSelect("projectName", "taskName"); //添加页面任务名称根据项目名称切换
				$("#saveBtn").on("click", function () {
					if($("#hddcActivefaultForm").valid()){
						var data = Tool.serialize("hddcActivefaultForm");
						$.ajax({
							url: getHddcActivefaultController() ,
                            contentType:"application/json",
							data: JSON.stringify(data),
							type: "post",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
                                createHddcActivefaultGrid();
							}
						});
					}
				});
			}
		});
	};
    var editProvince = function (Province, City, Area) {
        debugger;
        var html = "";
        $("#city").append(html);
        $("#area").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    if (item.divisionName == Province) {
                        debugger;
                        var divisionId = item.divisionId;
                        var htmlCity = '';
                        if (Province == "北京市" || Province == "天津市" || Province == "上海市" || Province == "重庆市") {
                            $("#city").append("<option value='" + Province + "' exid='" + divisionId + "'>" + Province + "</option>");
                            $('#city').val(City);
                            var htmlArea = '';
                            $.ajax({
                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                contentType: "application/json",
                                type: "get",
                                success: function (data) {
                                    $.each(data, function (idx, item) {
                                        htmlArea += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                    });
                                    $("#area").append(htmlArea);
                                    $('#area').val(Area);
                                }
                            });

                        } else {
                            $.ajax({
                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                contentType: "application/json",
                                type: "get",
                                success: function (data) {
                                    debugger;
                                    $.each(data, function (idx, item) {
                                        if (item.divisionName == City) {
                                            var divisionId = item.divisionId;
                                            var htmlArea = '';
                                            $.ajax({
                                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                                contentType: "application/json",
                                                type: "get",
                                                success: function (data) {
                                                    $.each(data, function (idx, item) {
                                                        htmlArea += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                                    });
                                                    $("#area").append(htmlArea);
                                                    $('#area').val(Area);
                                                }
                                            });
                                        }
                                        htmlCity += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                    });
                                    $("#city").append(htmlCity);
                                    $('#city').val(City);
                                }
                            });
                        }
                    }
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#province").append(html);
                $('#province').val(Province);
            }
        });
        $("#province").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#city option").remove();
            $("#city").append(html);
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#city").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#city").append(html);
                }
            });
        });
        $("#city").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#area").append(html);
                }
            });
        });
    }
	window._editHddcActivefault = function(hddcActivefaultId) {
		var slidebar = Util.slidebar({
			url: hddcActivefaultFormHtml,
			width: "800px",
			cache: false,
			close : true,
			afterLoad: function () {
				initStrikedirectionSelectFrom();
				initDirectionSelectFrom();
				initFeatureSelectFrom();
				initLatestactiveperiodSelectFrom();
				formValidator();
                HddccjCommon.initProjectSelect("projectName"); //编辑页面项目名称下拉
                HddccjCommon.initTaskSelect("projectName","taskName"); //编辑页面任务名称根据项目名称切换
				getHddcActivefault(hddcActivefaultId);
				$("#saveBtn").on("click", function () {
					if($("#hddcActivefaultForm").valid()){
						var data = Tool.serialize("hddcActivefaultForm");
						$.ajax({
							url: getHddcActivefaultController(),
                            contentType:"application/json",
                            data: JSON.stringify(data),
							type: "put",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
								createHddcActivefaultGrid();
							}
						});
					}
				});
			}
		});
	};
	var deleteHddcActivefault = function() {
		var rows = $("#hddcActivefaultGrid").datagrid("getSelections");
		if (rows == null || rows.length == 0) {
			Util.alert("请选择一行数据!");
			return;
		}
		Util.confirm("是否要删除选中的数据?", function() {
			var ids = "";
			$.each(rows, function(i, row){
				ids += row.uuid + ",";

			});
			ids = ids.substr(0,ids.length - 1);
			$.ajax({
				url: getHddcActivefaultController() ,
				data: {
					ids : ids
				},
				type: "delete",
				success: function (data) {
					createHddcActivefaultGrid();
				}
			});
		}, function() {
			return;
		});

	};

	return {
		init:init
	};
});
