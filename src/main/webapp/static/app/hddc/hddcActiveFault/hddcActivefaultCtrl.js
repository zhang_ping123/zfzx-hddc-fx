define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcActiveFault/hddcActivefaultSupport"],function(hddcActivefaultSupport){
            hddcActivefaultSupport.init();
        });
    };
});