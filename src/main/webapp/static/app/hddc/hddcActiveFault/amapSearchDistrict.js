var amapSearchDistrict = {
	
	searchDistrict: function (key, view, Graphic, polygonGraphicsLayer) {
	
		var amap_url1 = "https://restapi.amap.com/v3/config/district?subdistrict=0&extensions=all&level=district&key=c2040d97f27cf8c07d32cd0feff3a173&s=rsv3&output=json&keywords=";
		var amap_url2 = "&platform=JS&logversion=2.0&sdkversion=1.4.15"
		var amap_url = amap_url1 + key + amap_url2;
		var httpRequest = new XMLHttpRequest();
		httpRequest.open('get', amap_url, false); //第二步：打开连接
		httpRequest.send(null);
		if (httpRequest.status === 200) {
			var result = JSON.parse(httpRequest.responseText);
			if (result.status == 1) {
				var districts = result.districts;
				if (districts.length > 0) {
					//for (var idx = 0;idx < districts.length;idx++) {
					for (var idx = 0;idx < 1;idx++) {//只显示一个
						var pointsstr = districts[idx]["polyline"];
						var lines = pointsstr.split("|"), obj_dict = {};
						for (var k = 0;k < lines.length;k++) {
							var points = this.getLonLatsByAMap(lines[k]);
							var polygon = {
								type: "polygon", // autocasts as new Polyline()
								rings: points
							};
							var widthwidth=0;
							if(!maptypeLevel)
								widthwidth=1.5;
							searchDistrictGraphic = new Graphic({
								geometry: polygon,
								symbol: {
									type: "simple-fill",
									color: [50, 205, 50, 0],
									//style: "solid",
									// color: [255, 255, 255, 0],
									outline: {
										// autocasts as new SimpleLineSymbol()
										//color: "#ffff00",
										color:"blue",
										width: widthwidth
									}
								}
							});
							
							polygonGraphicsLayer.add(searchDistrictGraphic);
							
							var rings = searchDistrictGraphic.geometry.rings[0], r_len = rings.length;
							obj_dict[r_len] = searchDistrictGraphic;
						}
						var max = -1;
						for (var key in obj_dict) {
							parseInt(key) > max ? (max = parseInt(key)) : max;
						}
						view.extent = obj_dict[max].geometry.extent;
					}
					//view.extent = graphic.geometry.extent;
					
					searchFault = false;
				} else {
					console.log("查询结果为空！");
				}
			} else {
				console.log("查询结果错误， status：", result.status);
			}
		}
	},
	
	isAmaplayer: function (baseLayers) {
		/*
		var items = baseLayers.items;
		for (var i = 0;i < items.length;i++) {
			if (items[i].id == "amaplayer" && items[i].visible) return true;
		}*/
		if (baseMapType == "gaode-DisplayImage") return true;
		return false;
	},
	
	getLonLatsByAMap: function (coodsStr) {
		var points = [];
		var lonlats = coodsStr.split(';');
		for (var i = 0; i < lonlats.length; i++) {
			var p = getLonLat(lonlats[i], ',');
			if (p) points.push(p);
		}
		return points;
	}
	
}


var amapCityList = {
	init: function (view, Graphic, polygonGraphicsLayer) {
		debugger;
		this.view = view;
		this.Graphic = Graphic;
		this.polygonGraphicsLayer = polygonGraphicsLayer;
		$.ajax({
			url: "https://restapi.amap.com/v3/config/district?subdistrict=1&showbiz=false&extensions=base&key=c2040d97f27cf8c07d32cd0feff3a173&s=rsv3&output=json&keywords=%E4%B8%AD%E5%9B%BD",
			type: "GET",
			dataType: "json",
			success: function(data) {
				debugger;
				amapCityList.getData(data.districts[0]);
			}
		});
	},
	getData: function (data, level) {
		debugger;
		var pointsstr = data.polyline;
		if (pointsstr) {
			var lines = pointsstr.split("|"), obj_dict = {};
			for (var k = 0;k < lines.length;k++) {
				var points = this.getLonLatsByAMap(lines[k]);
				var polygon = {
					type: "polygon", // autocasts as new Polyline()
					rings: points
				};
				if(selecttianditumaptype){
					//alert(123456);
					document.getElementById("tiandituregiondisplayinfo").style.display='block';
					selecttianditumaptype=false;
				}
				var widthwidth=0;
				if(!maptypeLevel)
					widthwidth=1.5;
				searchDistrictGraphic = new this.Graphic({
					geometry: polygon,
					symbol: {
						type: "simple-fill",
						color: [50, 205, 50, 0],
						//style: "solid",
						// color: [255, 255, 255, 0],
						outline: {
							// autocasts as new SimpleLineSymbol()
							//color: "#ffff00",
							color:"blue",
							width: widthwidth
						}
					}
				});
				this.polygonGraphicsLayer.add(searchDistrictGraphic);
				
				var rings = searchDistrictGraphic.geometry.rings[0], r_len = rings.length;
				obj_dict[r_len] = searchDistrictGraphic;
			}
			var max = -1;
			for (var key in obj_dict) {
				parseInt(key) > max ? (max = parseInt(key)) : max;
			}
			//this.view.extent = graphic.geometry.extent;
			this.view.extent = obj_dict[max].geometry.extent;
		} else {
			//console.log("查询结果无行政区域坐标数据！");
		}
		if (level === 'amap-province') {
			$('#amap-city').html('');
			$('#amap-district').html('');
			$('#amap-street').html('');
		} else if (level === 'amap-city') {
			$('#amap-district').html('');
			$('#amap-street').html('');
		} else if (level === 'amap-district') {
			$('#amap-street').html('');
		}

		var subList = data.districts;
		if (subList) {
			var contentSub = new Option('--请选择--');
			var curlevel = subList[0].level;
			var curList =  document.querySelector('#amap-' + curlevel);
			curList.add(contentSub);
			//debugger;
			if(curlevel=="province"){
				var pArray=["北京市","天津市","河北省","山西省","内蒙古自治区","辽宁省","吉林省","黑龙江省","上海市","江苏省","浙江省","安徽省","福建省","江西省","山东省","河南省","湖北省","湖南省","广东省","广西壮族自治区","海南省","重庆市","四川省","贵州省","云南省","西藏自治区","陕西省","甘肃省","青海省","宁夏回族自治区","新疆维吾尔自治区","台湾省","香港特别行政区","澳门特别行政区"];
				for(var i=0;i<pArray.length;i++){
					var name=pArray[i];
					for(var j=0;j<subList.length;j++){
						if(name==subList[j].name){
							var levelSub = subList[j].level;
							var cityCode = subList[j].citycode;
							contentSub = new Option(name);
							contentSub.setAttribute("value", levelSub);
							contentSub.center = subList[j].center;
							contentSub.adcode = subList[j].adcode;
							curList.add(contentSub);
							break;
						}
					}
				}
			}else{
				for (var i = 0, l = subList.length; i < l; i++) {
					var name = subList[i].name;
					var levelSub = subList[i].level;
					var cityCode = subList[i].citycode;
					contentSub = new Option(name);
					contentSub.setAttribute("value", levelSub);
					contentSub.center = subList[i].center;
					contentSub.adcode = subList[i].adcode;
					curList.add(contentSub);
				}
			}
		}
	},
	search: function (obj) {
		//this.polygonGraphicsLayer.removeAll();
		
		var option = obj[obj.options.selectedIndex];
		var keyword = option.text; //关键字
		var adcode = option.adcode;
		
		$.ajax({
			url: "https://restapi.amap.com/v3/config/district?subdistrict=1&showbiz=false&extensions=all&key=c2040d97f27cf8c07d32cd0feff3a173&s=rsv3&output=json&level=province&keywords=" + adcode,
			type: "GET",
			dataType: "json",
			success: function(data) {
				amapCityList.getData(data.districts[0], obj.id);
			}
		});
		
	},
	setCenter: function (obj){
		var lonlat = obj[obj.options.selectedIndex].center;
		var lon = Number(lonlat.split(",")[0]);
		var lat = Number(lonlat.split(",")[1]);
		this.view.center = [lon, lat];
	},
	getLonLatsByAMap: function (coodsStr) {
		var points = [];
		var lonlats = coodsStr.split(';');
		for (var i = 0; i < lonlats.length; i++) {
			var p = getLonLat(lonlats[i], ',');
			if (p) points.push(p);
		}
		return points;
	}
};












