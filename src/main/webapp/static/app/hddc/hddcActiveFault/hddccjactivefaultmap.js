define([
    "UtilDir/util",
    "UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
    "Date","DateCN", "css!DateCss",
    "EasyUI","EasyUI-lang"
],function(Util, Tool, SelectOrg, SearchBlock){

    var getHddcCjTaskController = function(){
        return getServer() + "/hddc/hddcCjTasks";
    };
    var map=null;
    function initMap() {
        if(map==null) {
            //初始化地图对象

            /*
            var imageURL = "http://t0.tianditu.gov.cn/img_w/wmts?" +
                "SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=img&STYLE=default&TILEMATRIXSET=w&FORMAT=tiles" +
                "&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}&tk=acce9f1241bb60d5164939ee53173caf";
             */
            var imageURL1="https://fxpc.mem.gov.cn/wmts?service=wmts&request=gettile&version=1.0.0&layer=vec&style=default&tilematrixset=c&format=tiles&tilematrix={z}&tilerow={y}&tilecol={x} ";
            var lay1 = new T.TileLayer(imageURL1, {minZoom: 1, maxZoom: 18});
            var imageURL2="https://fxpc.mem.gov.cn/wmts?service=wmts&request=gettile&version=1.0.0&layer=cva&style=default&tilematrixset=c&format=tiles&tilematrix={z}&tilerow={y}&tilecol={x} ";
            var lay2 = new T.TileLayer(imageURL2, {minZoom: 1, maxZoom: 18});
            var imageURL3="https://fxpc.mem.gov.cn/wmts?service=wmts&request=gettile&version=1.0.0&layer=img&style=default&tilematrixset=c&format=tiles&tilematrix={z}&tilerow={y}&tilecol={x} ";
            var lay3 = new T.TileLayer(imageURL3, {minZoom: 1, maxZoom: 18});
            var imageURL4="https://fxpc.mem.gov.cn/wmts?service=wmts&request=gettile&version=1.0.0&layer=cia&style=default&tilematrixset=c&format=tiles&tilematrix={z}&tilerow={y}&tilecol={x} ";
            var lay4 = new T.TileLayer(imageURL4, {minZoom: 1, maxZoom: 18});
            var config = {
                projection: "EPSG:4326",
                minZoom: 4,
                layers: [lay1,lay2]
                //layers: [lay1,lay2,lay3,lay4]
            };
            //lay1.visible=false;
            //https://fxpc.mem.gov.cn/wmts?service=wmts&request=gettile&version=1.0.0&layer=cva&style=default&tilematrixset=c&format=tiles&tilematrix=9&tilerow=75&tilecol=423
            map = new T.Map("hddctask-map",config);
            map.clearOverLays();
            //设置显示地图的中心点和级别
            map.centerAndZoom(new T.LngLat(116.40969,39.89945),4);





            var control=new T.Control.Zoom();
            map.addControl(control);

            //创建比例尺控件对象
            var scale = new T.Control.Scale();
            map.addControl(scale);

            map.enableScrollWheelZoom();
            map.enableDrag();

            //创建鹰眼控件对象
            var miniMap = new T.Control.OverviewMap({
                 isOpen: true,
                 size: new T.Point(150, 150)
             });
             map.addControl(miniMap);

            var bodyWidth = window.innerWidth-15;//1920px
            var mapHeight =  window.innerHeight-40;//796px
            var mapWidth = bodyWidth;//1469px

            $("#hddctask-mapDiv").css("height",mapHeight + "px");
            $("#hddctask-mapDiv").css("width",mapWidth + "px");
            $("#hddctask-map").css("height",mapHeight + "px");
            $("#hddctask-map").css("width",mapWidth + "px");

            map.checkResize();
            //$("#toolbox-toolsDiv").css("z-index", "1000");
            //$("#toolbox-toolsDiv").show();

        }
        map.clearOverLays();
        //initMapInterface();
    };
    


    return {
        initMap:initMap
    };
});