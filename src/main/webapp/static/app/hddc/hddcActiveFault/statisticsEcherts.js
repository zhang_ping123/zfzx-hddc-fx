define([
    "Echarts",
    "EasyUI","EasyUI-lang","jquery"
],function(Echarts){
    var echarts = {};

    /* 初始化echarts实例 */
    echarts.resizeEcharts = function(id){
        var myChart = Echarts.init(document.getElementById(id));
        myChart.resize();
    }

    /* 柱状图 */
    echarts.createBarEcharts = function (obj) {
        var myBarChart = Echarts.init(document.getElementById(obj.id));

        var option = {
            color:["#51bffd","#8eec7f","#d78e51","#c92840","#50bfff","#f7ba2a","#7B68EE","#13ce66","#8B4513","#8492a6","#fc8675","#008080","#0000FF","#FFFF00","#FF0000","#FFEFD5","#F08080"],
            title: {
                text:obj.title,
                left:'left',
                textStyle:{
                    color:'#666',
                    /*fontStyle:'normal',
                    fontWeight:'bold',*/
                    //字体大小
                    fontSize:20
                },
                subtextStyle:{
                    color:'#9f999f',
                    fontWeight:'bold',
                    fontSize:12
                }
            },
            tooltip: {
                trigger: 'axis',
                /*formatter:function(params, ticket, callback){
                    console.log(params);
                    console.log(ticket);
                }*/
            },
            grid: {
                left: '3%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'category',
                data: obj.x,
                axisLabel: {
                    textStyle: {
                        color: '#000'
                    }
                },
                axisTick: {
                    show: false
                },
                axisLine: {
                    show: false
                }
            },
            yAxis: {
                name: obj.yname,
                type: 'value'
            },
            series: obj.series
        };
        myBarChart.setOption(option,true);
    }

    /* 饼状图 */
    echarts.createPieEcharts = function(obj){
        var myPieChart = Echarts.init(document.getElementById(obj.id));

        var option = {
            color:["#51bffd","#8eec7f","#d78e51","#c92840","#50bfff","#f7ba2a","#7B68EE","#13ce66","#8B4513","#8492a6","#fc8675","#008080","#0000FF","#FFFF00","#FF0000","#FFEFD5","#F08080"],
            title: {
                text: obj.title,
                subtext: obj.subtitle,
                left: 'center',
                textStyle: { // 主标题的属性
                    fontSize: 30,//大小
                },
                subtextStyle: { //副标题的属性
                    color: '#25664A',
                    // 同主标题
                    fontSize: 25,//大小
                    fontWeight: '700'//粗细
                }
            },
            tooltip: {
                trigger: 'item',
                formatter: '{a} <br/>{b} : {c} ({d}%)'
            },
            legend: {
                left: 'center',
                top: 'bottom',
                data: obj.legenddata
                // data: ['rose1', 'rose2', 'rose3', 'rose4', 'rose5', 'rose6', 'rose7', 'rose8']
            },
            toolbox: {
                show: true,
                feature: {
                    mark: {show: true},
                    dataView: {show: true, readOnly: false},
                    magicType: {
                        show: true,
                        type: ['pie', 'funnel']
                    },
                    restore: {show: true},
                    saveAsImage: {show: true}
                }
            },
            series: [
                {
                    name: obj.series[0].name,
                    type: 'pie',
                    //radius: [0, '44%'],
                    radius: '50%',
                    center: ['40%', '60%'],
                    selectedMode: 'single',
                    /*label: {
                        normal: {
                            position: 'inner'
                        }
                    },*/
                    data: obj.series[0].data
                }
            ]
        };

        myPieChart.setOption(option,true);
    }

    /* 饼状图 */
    echarts.createMyPieEcharts = function(obj){
        var myPieChart = Echarts.init(document.getElementById(obj.id));

        var option = {
            // color:["#51bffd","#8eec7f","#d78e51","#c92840","#50bfff","#f7ba2a","#7B68EE","#13ce66","#8B4513","#8492a6","#fc8675","#008080","#0000FF","#FFFF00","#FF0000","#FFEFD5","#F08080"],
            color:obj.color,
            // title: {
            //     text: obj.title,
            //     subtext: obj.subtitle,
            //     left: 'center',
            //     textStyle: { // 主标题的属性
            //         fontSize: 30,//大小
            //     },
            //     subtextStyle: { //副标题的属性
            //         color: '#25664A',
            //         // 同主标题
            //         fontSize: 25,//大小
            //         fontWeight: '700'//粗细
            //     }
            // },
            tooltip: {
                trigger: 'item',
                formatter: '{a} <br/>{b} : {c} ({d}%)'
            },
            // legend: {
            //     left: 'center',
            //     top: 'bottom',
            //     data: obj.legenddata
            //     // data: ['rose1', 'rose2', 'rose3', 'rose4', 'rose5', 'rose6', 'rose7', 'rose8']
            // },
            toolbox: {
                show: true,
                feature: {
                    mark: {show: true},
                    dataView: {show: true, readOnly: false},
                    magicType: {
                        show: true,
                        type: ['pie', 'funnel']
                    },
                    restore: {show: true},
                    saveAsImage: {show: true}
                }
            },
            series: [
                {
                    name: obj.centerTitle,
                    type: 'pie',
                    //radius: [0, '44%'],
                    radius: '50%',
                    center: ['40%', '60%'],
                    selectedMode: 'single',
                    /*label: {
                        normal: {
                            position: 'inner'
                        }
                    },*/
                    data: obj.data
                }
            ]
        };

        myPieChart.setOption(option,true);
    }

    return echarts;
})