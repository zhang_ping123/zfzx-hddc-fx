define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcStratigraphy25LinePre/hddcStratigraphy25linepreSupport"],function(hddcStratigraphy25linepreSupport){
            hddcStratigraphy25linepreSupport.init();
        });
    };
});