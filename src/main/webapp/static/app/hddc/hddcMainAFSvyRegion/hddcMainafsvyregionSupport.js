define([
	"UtilDir/util",
	"UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
    "static/app/hddc/hddccjcommon/hddccjCommon",
	"Date","DateCN", "css!DateCss",
	"EasyUI","EasyUI-lang"
    ],function(Util, Tool, SelectOrg, SearchBlock,HddccjCommon){

    var sysPath =  getServer() + "/static/app/hddc/hddcMainAFSvyRegion";

    var hddcMainafsvyregionFormHtml = sysPath + "/views/hddcMainafsvyregionForm.html";
	var getHddcMainafsvyregionController = function(){
		return getServer() + "/hddc/hddcMainafsvyregions";
	};

	/**
	 * 页面初始化
	 */
	var init = function(){
        initSearchBlock();
        queryBtnBind();
        HddccjCommon.initProjectSelect("proNameSelect"); //初始化查询的项目名称
        initcreateProvince();
		createHddcMainafsvyregionGrid();
	};
    var initcreateProvince = function () {
        var html = "";
        $("#citySelect").append(html);
        $("#areaSelect").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#provinceSelect").append(html);
            }
        });
        $("#provinceSelect").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#citySelect option").remove();
            $("#citySelect").append(html);
            $("#areaSelect option").remove();
            $("#areaSelect").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#citySelect").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#citySelect").append(html);
                }
            });
        });
        $("#citySelect").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#areaSelect option").remove();
            $("#areaSelect").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#areaSelect").append(html);
                }
            });
        });
    }
    var initcreateProvinceForm = function () {
        var html = "";
        $("#city").append(html);
        $("#area").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#province").append(html);
            }
        });
        $("#province").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#city option").remove();
            $("#city").append(html);
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#city").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#city").append(html);
                }
            });
        });
        $("#city").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#area").append(html);
                }
            });
        });
    }
    var editProvince = function (Province, City, Area) {
        debugger;
        var html = "";
        $("#city").append(html);
        $("#area").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    if (item.divisionName == Province) {
                        debugger;
                        var divisionId = item.divisionId;
                        var htmlCity = '';
                        if (Province == "北京市" || Province == "天津市" || Province == "上海市" || Province == "重庆市") {
                            $("#city").append("<option value='" + Province + "' exid='" + divisionId + "'>" + Province + "</option>");
                            $('#city').val(City);
                            var htmlArea = '';
                            $.ajax({
                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                contentType: "application/json",
                                type: "get",
                                success: function (data) {
                                    $.each(data, function (idx, item) {
                                        htmlArea += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                    });
                                    $("#area").append(htmlArea);
                                    $('#area').val(Area);
                                }
                            });

                        } else {
                            $.ajax({
                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                contentType: "application/json",
                                type: "get",
                                success: function (data) {
                                    debugger;
                                    $.each(data, function (idx, item) {
                                        if (item.divisionName == City) {
                                            var divisionId = item.divisionId;
                                            var htmlArea = '';
                                            $.ajax({
                                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                                contentType: "application/json",
                                                type: "get",
                                                success: function (data) {
                                                    $.each(data, function (idx, item) {
                                                        htmlArea += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                                    });
                                                    $("#area").append(htmlArea);
                                                    $('#area').val(Area);
                                                }
                                            });
                                        }
                                        htmlCity += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                    });
                                    $("#city").append(htmlCity);
                                    $('#city').val(City);
                                }
                            });
                        }
                    }
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#province").append(html);
                $('#province').val(Province);
            }
        });
        $("#province").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#city option").remove();
            $("#city").append(html);
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#city").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#city").append(html);
                }
            });
        });
        $("#city").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#area").append(html);
                }
            });
        });
    }

	var initProvinceSelect = function () {
		$.ajax({
			url: getHddcMainafsvyregionController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("provinceSelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initCitySelect = function () {
		$.ajax({
			url: getHddcMainafsvyregionController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("citySelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initAreaSelect = function () {
		$.ajax({
			url: getHddcMainafsvyregionController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("areaSelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initProNameSelect = function () {
		$.ajax({
			url: getHddcMainafsvyregionController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("proNameSelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};

		var initProvinceSelectFrom = function () {
		$.ajax({
			url: getHddcMainafsvyregionController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("province");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initCitySelectFrom = function () {
		$.ajax({
			url: getHddcMainafsvyregionController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("city");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initAreaSelectFrom = function () {
		$.ajax({
			url: getHddcMainafsvyregionController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("area");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initProNameSelectFrom = function () {
		$.ajax({
			url: getHddcMainafsvyregionController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("proName");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initTaskNameSelectFrom = function () {
		$.ajax({
			url: getHddcMainafsvyregionController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("taskName");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initIsvolcanicSelectFrom = function () {
		$.ajax({
			url: getHddcMainafsvyregionController() + "/getValidDictItemsByDictCode/" + "truefalse",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("isvolcanic");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initSraSelectFrom = function () {
		$.ajax({
			url: getHddcMainafsvyregionController() + "/getValidDictItemsByDictCode/" + "truefalse",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("sra");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initSdaSelectFrom = function () {
		$.ajax({
			url: getHddcMainafsvyregionController() + "/getValidDictItemsByDictCode/" + "truefalse",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("sda");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initIsnumsimulationSelectFrom = function () {
		$.ajax({
			url: getHddcMainafsvyregionController() + "/getValidDictItemsByDictCode/" + "truefalse",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("isnumsimulation");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};

	var initSearchBlock = function(){
        SearchBlock.init("searchBlock");
	};
	var queryBtnBind = function(){
        $("#btnSearch").click(function () {
            createHddcMainafsvyregionGrid();
        });
        $("#btnReset").click(function () {
            $("#provinceSelect").val("");
	            $("#citySelect").val("");
	            $("#areaSelect").val("");
	            $("#proNameSelect").val("");
				$("#projectnameCondition").val("");
			});
	};
    //导入
    var importForm = function () {

        $("#uploadModal").modal();
        $("#uploadModal").on("shown.bs.modal", function () {
            var url = getServer() + "/excel/主要活动断层制图区-面.xls";
            $("#downloadZwExcelTemplate").attr("href", url);
        });

        $('#uploadButton').off("click");
        $("#uploadButton").on("click", function () {
            var uploadinput = document.getElementById("uploadFile");
            if (uploadinput.value == "") {
                Util.alert("上传前请先选择文件!");
                return;
            }
            var formData = new FormData();
            formData.append("file", uploadinput.files[0]);
            $.ajax({
                url: getHddcMainafsvyregionController() + "/importDisaster",
                data: formData,
                processData: false, //因为data值是FormData对象，不需要对数据做处理。
                contentType: false,
                type: "POST",
                success: function (data) {
                    $('#uploadModal').modal('hide');
                    uploadinput.value = null;
                    Util.alert(data);
                    createHddcMainafsvyregionGrid();
                }
            });
        });
    };
    //导出
    var exportForm = function () {
        var province = $("#provinceSelect").val();
        var city = $("#citySelect").val();
        var area = $("#areaSelect").val();
        var proName = $("#proNameSelect").val();
        var projectname= $("#projectnameCondition").val();
        window.location.href = getHddcMainafsvyregionController() + "/exportFile?province="+province+"&city="+city+"&area="+area+"&proName="+proName+"&projectname="+projectname;
    };
	var createHddcMainafsvyregionGrid= function() {
        $("#hddcMainafsvyregionGrid").datagrid({
            url:getHddcMainafsvyregionController() + "/queryHddcMainafsvyregions",
			method:"GET",
            fitColumns: true,
            autoRowHeight: false,
            columns:[[
                {field:"ck",checkbox:true},
				{
					field:'province',
					title:'省',
					width:'20%',
					align:'center',
				},
				{
					field:'city',
					title:'市',
					width:'20%',
					align:'center',
				},
				{
					field:'area',
					title:'区（县）',
					width:'20%',
					align:'center',
				},
				{
					field:'id',
					title:'编号',
					width:'20%',
					align:'center',
					formatter:function(value,rowData,rowIndex){
								return '<a href="#" onclick="_editHddcMainafsvyregion(\'' + rowData.uuid + '\');"> '+rowData.id+' </a> '
					}
				},
				{
					field:'projectname',
					title:'项目名称',
					width:'20%',
					align:'center',
				},
				{
					field:'proName',
					title:'项目名称',
					width:'20%',
					align:'center',
				},
				{
					field:'taskName',
					title:'任务名称',
					width:'20%',
					align:'center',
				},
				{
					field:'targetregionid',
					title:'目标区编号',
					width:'20%',
					align:'center',
				},
            ]],
            toolbar: [{
                iconCls: 'fa fa-plus-circle',
                text:"添加",
                handler: function(){
                    addHddcMainafsvyregion();
                }
            },{
                iconCls: 'fa fa-trash-o',
                text:"删除",
                handler: function(){
                    deleteHddcMainafsvyregion();
                }
            }, {
                iconCls: 'fa fa-upload',
                text: "导入",
                handler: function () {
                    importForm();
                }
            }, {
                iconCls: 'fa fa-download',
                text: "导出",
                handler: function () {
                    exportForm();
                }
            }],
            queryParams:{
                province: $("#provinceSelect").val(),
                city: $("#citySelect").val(),
                area: $("#areaSelect").val(),
                proName: $("#proNameSelect").val(),
				projectname: $("#projectnameCondition").val(),
            },
            pagination: true,
            pageSize: 10
        });
    };
	var formValidator = function(){
		$("#hddcMainafsvyregionForm").validate({
			rules : {
				province : {
					required : true,
				},
				city : {
					required : true,
				},
				area : {
					required : true,
				},
				id : {
					required : true,
				},
				projectname : {
					required : true,
				},
				proName : {
					required : true,
				},
				taskName : {
					required : true,
				},
				targetregionid : {
					required : true,
				},
				mappingarea : {
					maxlength : 4,
					digits: true,
				},
				studiedfaultcount : {
					maxlength : 4,
					digits: true,
				},
				afaultcount : {
					maxlength : 4,
					digits: true,
				},
				rsprocess : {
					maxlength : 4,
					digits: true,
				},
				fieldsvyptcount : {
                    maxlength : 4,
                    digits: true,
				},
				trenchcount : {
					maxlength : 4,
					digits: true,
				},
				drillcount : {
					maxlength : 4,
					digits: true,
				},
				drilllength : {
					maxlength : 4,
					digits: true,
				},
				gphwellcount : {
					maxlength : 4,
					digits: true,
				},
				collectedsamplecount : {
					maxlength : 4,
					digits: true,
				},
				samplecount : {
					maxlength : 4,
					digits: true,
				},
				datingsamplecount : {
					maxlength : 4,
					digits: true,
				},
				geomorphysvyprojectcount : {
					maxlength : 4,
					digits: true,
				},
				crustaldfmprojectcount : {
					maxlength : 4,
					digits: true,
				},
				geophysicalsvyprojectcount : {
					maxlength : 4,
					digits: true,
				},
				geochemicalprojectcount : {
					maxlength : 4,
					digits: true,
				},
				trenchvolume : {
					maxlength : 8,
					number : true,
				},
			},
			messages : {
				province : {
					required : "省不允许为空!",
				},
				city : {
					required : "市不允许为空!",
				},
				area : {
					required : "区（县）不允许为空!",
				},
				id : {
					required : "编号不允许为空!",
				},
				projectname : {
					required : "项目名称不允许为空!",
				},
				proName : {
					required : "项目名称不允许为空!",
				},
				taskName : {
					required : "任务名称不允许为空!",
				},
				targetregionid : {
					required : "目标区编号不允许为空!",
				},
				mappingarea : {
                    maxlength : "地质填图面积 [km²]长度不大于4的整数!",
                    digits:"地质填图面积 [km²]长度不大于4的整数!"
				},
				studiedfaultcount : {
                    maxlength : "研究断层总数长度不大于4的整数!",
                    digits : "研究断层总数长度不大于4的整数!",
				},
				afaultcount : {
                    maxlength : "活动断层条数长度不大于4的整数!",
                    digits : "活动断层条数长度不大于4的整数!",
				},
				rsprocess : {
                    maxlength : "遥感影像处理数目长度不大于4的整数!",
                    digits : "遥感影像处理数目长度不大于4的整数!",
				},
				fieldsvyptcount : {
                    maxlength : "野外观测点数长度不大于4的整数!",
                    digits : "野外观测点数长度不大于4的整数!",
				},
				trenchcount : {
                    maxlength : "钻孔数探槽数长度不大于4的整数!",
                    digits : "钻孔数探槽数长度不大于4的整数!",
				},
				drillcount : {
                    maxlength : "钻孔进尺 [米]长度不大于4的整数!",
                    digits : "钻孔进尺 [米]长度不大于4的整数!",
				},
				drilllength : {
                    maxlength : "地球物理测井数长度不大于4的整数!",
                    digits : "地球物理测井数长度不大于4的整数!",
				},
				gphwellcount : {
                    maxlength : "采集样品总数长度不大于4的整数!",
                    digits : "采集样品总数长度不大于4的整数!",
				},
				collectedsamplecount : {
                    maxlength : "送样总数长度不大于4的整数!",
                    digits : "送样总数长度不大于4的整数!",
				},
				samplecount : {
                    maxlength : "采集样品总数长度不大于4的整数!",
                    digits : "采集样品总数长度不大于4的整数!",
				},
				datingsamplecount : {
                    maxlength : "获得测试结果样品数长度不大于4的整数!",
                    digits : "获得测试结果样品数长度不大于4的整数!",
				},
				geomorphysvyprojectcount : {
                    maxlength : "微地貌测量工程总数长度不大于4的整数!",
                    digits : "微地貌测量工程总数长度不大于4的整数!",
				},
				seismicprojectcount : {
                    maxlength : "地震监测工程数长度不大于4的整数!",
                    digits : "地震监测工程数长度不大于4的整数!",
				},
				crustaldfmprojectcount : {
                    maxlength : "地形变监测工程数长度不大于4的整数!",
                    digits : "地形变监测工程数长度不大于4的整数!",
				},
				geophysicalsvyprojectcount : {
                    maxlength : "地球物理探测工程数长度不大于4的整数!",
                    digits : "地球物理探测工程数长度不大于4的整数!",
				},
				geochemicalprojectcount : {
                    maxlength : "地球化学探测工程数长度不大于4的整数!",
                    digits : "地球化学探测工程数长度不大于4的整数!",
				},
				trenchvolume : {
                    maxlength : "探测总土方量 [m³]长度不大于8的数字!",
                    digits : "探测总土方量 [m³]长度不大于4的整数!",
				},
			}
		});
	};
	var getHddcMainafsvyregion = function(id){
		$.ajax({
			url: getHddcMainafsvyregionController() + "/"+id,
			type: "get",
			success: function (data) {
                editProvince(data.province, data.city, data.area);
                // 回显项目名称
                $("#proName").val(data.proName);
                // 回显任务名称
                HddccjCommon.initEditTaskSelect("taskName",data.proName);
                $("#taskName").val(data.taskName);
				Tool.deserialize("hddcMainafsvyregionForm", data);
			}
		});
	};



	var addHddcMainafsvyregion = function () {
		var slidebar = Util.slidebar({
			url: hddcMainafsvyregionFormHtml,
			width: "580px",
			cache: false,
			close : true,
			afterLoad: function () {
                initcreateProvinceForm();
				initIsvolcanicSelectFrom();
				initSraSelectFrom();
				initSdaSelectFrom();
                HddccjCommon.initProjectSelect("proName"); //添加页面项目名称下拉
                HddccjCommon.initTaskSelect("proName", "taskName"); //添加页面任务名称根据项目名称切换
                initIsnumsimulationSelectFrom();
				formValidator();
				$("#saveBtn").on("click", function () {
				    debugger;
					if($("#hddcMainafsvyregionForm").valid()){
						var data = Tool.serialize("hddcMainafsvyregionForm");
						$.ajax({
							url: getHddcMainafsvyregionController() ,
                            contentType:"application/json",
							data: JSON.stringify(data),
							type: "post",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
                                createHddcMainafsvyregionGrid();
							}
						});
					}
				});
			}
		});
	};
	window._editHddcMainafsvyregion = function(hddcMainafsvyregionId) {
		var slidebar = Util.slidebar({
			url: hddcMainafsvyregionFormHtml,
			width: "580px",
			cache: false,
			close : true,
			afterLoad: function () {
				initIsvolcanicSelectFrom();
				initSraSelectFrom();
				initSdaSelectFrom();
				initIsnumsimulationSelectFrom();
				formValidator();
				getHddcMainafsvyregion(hddcMainafsvyregionId);
				$("#saveBtn").on("click", function () {
					if($("#hddcMainafsvyregionForm").valid()){
						var data = Tool.serialize("hddcMainafsvyregionForm");
						$.ajax({
							url: getHddcMainafsvyregionController(),
                            contentType:"application/json",
                            data: JSON.stringify(data),
							type: "put",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
								createHddcMainafsvyregionGrid();
							}
						});
					}
				});
			}
		});
	};
	var deleteHddcMainafsvyregion = function() {
		var rows = $("#hddcMainafsvyregionGrid").datagrid("getSelections");
		if (rows == null || rows.length == 0) {
			Util.alert("请选择一行数据!");
			return;
		}
		Util.confirm("是否要删除选中的数据?", function() {
			var ids = "";
			$.each(rows, function(i, row){
				ids += row.uuid + ",";

			});
			ids = ids.substr(0,ids.length - 1);
			$.ajax({
				url: getHddcMainafsvyregionController() ,
				data: {
					ids : ids
				},
				type: "delete",
				success: function (data) {
					createHddcMainafsvyregionGrid();
				}
			});
		}, function() {
			return;
		});

	};

	return {
		init:init
	};
});
