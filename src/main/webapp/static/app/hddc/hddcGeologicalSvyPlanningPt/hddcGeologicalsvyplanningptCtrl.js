define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGeologicalSvyPlanningPt/hddcGeologicalsvyplanningptSupport"],function(hddcGeologicalsvyplanningptSupport){
            hddcGeologicalsvyplanningptSupport.init();
        });
    };
});