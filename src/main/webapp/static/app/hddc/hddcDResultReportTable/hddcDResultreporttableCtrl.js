define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcDResultReportTable/hddcDResultreporttableSupport"],function(hddcDResultreporttableSupport){
            hddcDResultreporttableSupport.init();
        });
    };
});