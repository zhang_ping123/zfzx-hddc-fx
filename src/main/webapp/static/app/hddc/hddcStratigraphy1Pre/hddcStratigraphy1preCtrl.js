define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcStratigraphy1Pre/hddcStratigraphy1preSupport"],function(hddcStratigraphy1preSupport){
            hddcStratigraphy1preSupport.init();
        });
    };
});