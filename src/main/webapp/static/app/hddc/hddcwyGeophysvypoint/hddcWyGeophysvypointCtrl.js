define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcwyGeophysvypoint/hddcWyGeophysvypointSupport"],function(hddcWyGeophysvypointSupport){
            hddcWyGeophysvypointSupport.init();
        });
    };
});