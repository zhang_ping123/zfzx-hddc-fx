define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcwyGeomorphySvyRegion/hddcWyGeomorphysvyregionSupport"],function(hddcWyGeomorphysvyregionSupport){
            hddcWyGeomorphysvyregionSupport.init();
        });
    };
});