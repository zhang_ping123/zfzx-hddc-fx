define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcLava/hddcLavaSupport"],function(hddcLavaSupport){
            hddcLavaSupport.init();
        });
    };
});