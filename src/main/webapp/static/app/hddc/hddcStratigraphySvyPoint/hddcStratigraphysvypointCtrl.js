define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcStratigraphySvyPoint/hddcStratigraphysvypointSupport"],function(hddcStratigraphysvypointSupport){
            hddcStratigraphysvypointSupport.init();
        });
    };
});