define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcStratigraphy5Pre/hddcStratigraphy5preSupport"],function(hddcStratigraphy5preSupport){
            hddcStratigraphy5preSupport.init();
        });
    };
});