define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGeomorphyline/hddcGeomorphylineSupport"],function(hddcGeomorphylineSupport){
            hddcGeomorphylineSupport.init();
        });
    };
});