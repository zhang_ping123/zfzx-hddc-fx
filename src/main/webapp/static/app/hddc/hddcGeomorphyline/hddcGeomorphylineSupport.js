define([
	"UtilDir/util",
	"UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
    "static/app/hddc/hddccjcommon/hddccjCommon",
	"Date","DateCN", "css!DateCss",
	"EasyUI","EasyUI-lang"
    ],function(Util, Tool, SelectOrg, SearchBlock,HddccjCommon){

    var sysPath =  getServer() + "/static/app/hddc/hddcGeomorphyline";

    var hddcGeomorphylineFormHtml = sysPath + "/views/hddcGeomorphylineForm.html";
	var getHddcGeomorphylineController = function(){
		return getServer() + "/hddc/hddcGeomorphylines";
	};

	/**
	 * 页面初始化
	 */
	var init = function(){
        initSearchBlock();
        queryBtnBind();
        initcreateProvince();
        HddccjCommon.initProjectSelect("projectNameSelect"); //初始化查询的项目名称
		createHddcGeomorphylineGrid();
	};
    var initcreateProvince = function () {
        var html = "";
        $("#citySelect").append(html);
        $("#areaSelect").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#provinceSelect").append(html);
            }
        });
        $("#provinceSelect").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#citySelect option").remove();
            $("#citySelect").append(html);
            $("#areaSelect option").remove();
            $("#areaSelect").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#citySelect").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#citySelect").append(html);
                }
            });
        });
        $("#citySelect").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#areaSelect option").remove();
            $("#areaSelect").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#areaSelect").append(html);
                }
            });
        });
    }
    var initcreateProvinceForm = function () {
        var html = "";
        $("#city").append(html);
        $("#area").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#province").append(html);
            }
        });
        $("#province").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#city option").remove();
            $("#city").append(html);
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#city").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#city").append(html);
                }
            });
        });
        $("#city").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#area").append(html);
                }
            });
        });
    }
    var editProvince = function (Province, City, Area) {
        debugger;
        var html = "";
        $("#city").append(html);
        $("#area").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    if (item.divisionName == Province) {
                        debugger;
                        var divisionId = item.divisionId;
                        var htmlCity = '';
                        if (Province == "北京市" || Province == "天津市" || Province == "上海市" || Province == "重庆市") {
                            $("#city").append("<option value='" + Province + "' exid='" + divisionId + "'>" + Province + "</option>");
                            $('#city').val(City);
                            var htmlArea = '';
                            $.ajax({
                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                contentType: "application/json",
                                type: "get",
                                success: function (data) {
                                    $.each(data, function (idx, item) {
                                        htmlArea += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                    });
                                    $("#area").append(htmlArea);
                                    $('#area').val(Area);
                                }
                            });

                        } else {
                            $.ajax({
                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                contentType: "application/json",
                                type: "get",
                                success: function (data) {
                                    debugger;
                                    $.each(data, function (idx, item) {
                                        if (item.divisionName == City) {
                                            var divisionId = item.divisionId;
                                            var htmlArea = '';
                                            $.ajax({
                                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                                contentType: "application/json",
                                                type: "get",
                                                success: function (data) {
                                                    $.each(data, function (idx, item) {
                                                        htmlArea += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                                    });
                                                    $("#area").append(htmlArea);
                                                    $('#area').val(Area);
                                                }
                                            });
                                        }
                                        htmlCity += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                    });
                                    $("#city").append(htmlCity);
                                    $('#city').val(City);
                                }
                            });
                        }
                    }
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#province").append(html);
                $('#province').val(Province);
            }
        });
        $("#province").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#city option").remove();
            $("#city").append(html);
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#city").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#city").append(html);
                }
            });
        });
        $("#city").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#area").append(html);
                }
            });
        });
    }

    var initPhotoviewingtoSelectFrom = function () {
    $.ajax({
        url: getHddcGeomorphylineController() + "/getValidDictItemsByDictCode/" + "CVD16Direction",
        type: "get",
        success: function (data) {
            var mySelect = document.getElementById("photoviewingto");
            mySelect.add(new Option("请选择", ''), 0);
            for (var i = 0; i < data.length; i++) {
                var name = data[i].dictItemName;
                var code = data[i].dictItemCode;
                mySelect.add(new Option(name, code));
            }
        }
    });
};
    var initIssurfacerupturebeltSelectFrom = function () {
    $.ajax({
        url: getHddcGeomorphylineController() + "/getValidDictItemsByDictCode/" + "truefalse",
        type: "get",
        success: function (data) {
            var mySelect = document.getElementById("issurfacerupturebelt");
            mySelect.add(new Option("请选择", ''), 0);
            for (var i = 0; i < data.length; i++) {
                var name = data[i].dictItemName;
                var code = data[i].dictItemCode;
                mySelect.add(new Option(name, code));
            }
        }
    });
};
    var initCreatedateSelectFrom = function () {
		$.ajax({
			url: getHddcGeomorphylineController() + "/getValidDictItemsByDictCode/" + "AgeCVD",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("createdate");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};

	var initSearchBlock = function(){
        SearchBlock.init("searchBlock");
	};
	var queryBtnBind = function(){
        $("#btnSearch").click(function () {
            createHddcGeomorphylineGrid();
        });
        $("#btnReset").click(function () {
            $("#provinceSelect").val("");
	            $("#citySelect").val("");
	            $("#areaSelect").val("");
	            $("#projectNameSelect").val("");
				$("#geomorphynameCondition").val("");
			});
	};
    //导入
    var importForm = function () {

        $("#uploadModal").modal();
        $("#uploadModal").on("shown.bs.modal", function () {
            var url = getServer() + "/excel/地貌线-线.xls";
            $("#downloadZwExcelTemplate").attr("href", url);
        });

        $('#uploadButton').off("click");
        $("#uploadButton").on("click", function () {
            var uploadinput = document.getElementById("uploadFile");
            if (uploadinput.value == "") {
                Util.alert("上传前请先选择文件!");
                return;
            }
            var formData = new FormData();
            formData.append("file", uploadinput.files[0]);
            $.ajax({
                url: getHddcGeomorphylineController() + "/importDisaster",
                data: formData,
                processData: false, //因为data值是FormData对象，不需要对数据做处理。
                contentType: false,
                type: "POST",
                success: function (data) {
                    $('#uploadModal').modal('hide');
                    uploadinput.value = null;
                    Util.alert(data);
                    createHddcGeomorphylineGrid();
                }
            });
        });
    };
    //导出
    var exportForm = function () {
        var province = $("#provinceSelect").val();
        var city = $("#citySelect").val();
        var area = $("#areaSelect").val();
        var projectName = $("#projectNameSelect").val();
        var geomorphyname= $("#geomorphynameCondition").val();
        window.location.href = getHddcGeomorphylineController() + "/exportFile?province="+province+"&city="+city+"&area="+area+"&projectName="+projectName+"&geomorphyname="+geomorphyname;
    };
	var createHddcGeomorphylineGrid= function() {
        $("#hddcGeomorphylineGrid").datagrid({
            url:getHddcGeomorphylineController() + "/queryHddcGeomorphylines",
			method:"GET",
            fitColumns: true,
            autoRowHeight: false,
            columns:[[
                {field:"ck",checkbox:true},
                {
                    field: 'id',
                    title: '地貌线编号',
                    width: '20%',
                    align: 'center',
                    formatter: function (value, rowData, rowIndex) {
                        return '<a href="#" onclick="_editHddcGeomorphyline(\'' + rowData.uuid + '\');"> ' + rowData.id + ' </a> '
                    }
                },
                {
                    field:'geomorphyname',
                    title:'地貌名称',
                    width:'20%',
                    align:'center',
                },
				{
					field:'province',
					title:'省',
					width:'20%',
					align:'center',
				},
				{
					field:'city',
					title:'市',
					width:'20%',
					align:'center',
				},
				{
					field:'area',
					title:'区（县）',
					width:'20%',
					align:'center',
				},
				{
					field:'projectName',
					title:'项目名称',
					width:'20%',
					align:'center'
				},
				{
					field:'taskName',
					title:'任务名称',
					width:'20%',
					align:'center',
				},
				{
					field:'geomorphycode',
					title:'地貌代码',
					width:'20%',
					align:'center',
				},
            ]],
            toolbar: [{
                iconCls: 'fa fa-plus-circle',
                text:"添加",
                handler: function(){
                    addHddcGeomorphyline();
                }
            },{
                iconCls: 'fa fa-trash-o',
                text:"删除",
                handler: function(){
                    deleteHddcGeomorphyline();
                }
            }, {
                iconCls: 'fa fa-upload',
                text: "导入",
                handler: function () {
                    importForm();
                }
            }, {
                iconCls: 'fa fa-download',
                text: "导出",
                handler: function () {
                    exportForm();
                }
            }],
            queryParams:{
                province: $("#provinceSelect").val(),
                city: $("#citySelect").val(),
                area: $("#areaSelect").val(),
                projectName: $("#projectNameSelect").val(),
				geomorphyname: $("#geomorphynameCondition").val(),
            },
            pagination: true,
            pageSize: 10
        });
    };
	var formValidator = function(){
		$("#hddcGeomorphylineForm").validate({
			rules : {
				province : {
					required : true,
				},
				city : {
					required : true,
				},
				area : {
					required : true,
				},
				projectName : {
					required : true,
				},
				issurfacerupturebelt : {
					required : true,
				},
                length : {
                    maxlength : 8,
                    number : true,
                },
                width : {
                    maxlength : 8,
                    number : true,
                },
                height : {
                    maxlength : 8,
                    number : true,
                },
                maxverticaldisplacement : {
                    maxlength : 8,
                    number : true,
                },
                maxhorizenoffset : {
                    maxlength : 8,
                    number : true,
                },
                maxtdisplacement : {
                    maxlength : 8,
                    number : true,
                },
                id : {
                    required : true,
                },
			},
			messages : {
				province : {
					required : "省不允许为空!",
				},
				city : {
					required : "市不允许为空!",
				},
				area : {
					required : "区（县）不允许为空!",
				},
				projectName : {
					required : "项目名称不允许为空!",
				},
				issurfacerupturebelt : {
					required : "是否为已知地震的地表破裂不允许为空!",
				},
                length : {
                    required : "地表破裂（断塞塘等）长 [米]为最大长度不超过8位的数字!",
                },
                width : {
                    required : "地表破裂（断塞塘等）宽 [米]为最大长度不超过8位的数字!",
                },
                height : {
                    required : "地表破裂（断塞塘等）高/深 [米]为最大长度不超过8位的数字!",
                },
                maxverticaldisplacement : {
                    required : "最大垂直位移 [米]为最大长度不超过8位的数字!",
                },
                maxhorizenoffset : {
                    required : "最大水平位移 [米]为最大长度不超过8位的数字!",
                },
                maxtdisplacement : {
                    required : "最大水平/张缩位移 [米]为最大长度不超过8位的数字!",
                },
                id : {
                    required : "地貌线编号不允许为空!",
                },
            }
		});
	};
	var getHddcGeomorphyline = function(id){
		$.ajax({
			url: getHddcGeomorphylineController() + "/"+id,
			type: "get",
			success: function (data) {
                editProvince(data.province, data.city, data.area);
                // 回显项目名称
                $("#projectName").val(data.projectName);
                // 回显任务名称
                HddccjCommon.initEditTaskSelect("taskName",data.projectName);
                $("#taskName").val(data.taskName);
				Tool.deserialize("hddcGeomorphylineForm", data);
			}
		});
	};

	var addHddcGeomorphyline = function () {
		var slidebar = Util.slidebar({
			url: hddcGeomorphylineFormHtml,
			width: "580px",
			cache: false,
			close : true,
			afterLoad: function () {
                initcreateProvinceForm();
				initPhotoviewingtoSelectFrom();
				initIssurfacerupturebeltSelectFrom();
				initCreatedateSelectFrom();
                HddccjCommon.initProjectSelect("projectName"); //添加页面项目名称下拉
                HddccjCommon.initTaskSelect("projectName", "taskName"); //添加页面任务名称根据项目名称切换
                formValidator();
				$("#saveBtn").on("click", function () {
					if($("#hddcGeomorphylineForm").valid()){
						var data = Tool.serialize("hddcGeomorphylineForm");
						$.ajax({
							url: getHddcGeomorphylineController() ,
                            contentType:"application/json",
							data: JSON.stringify(data),
							type: "post",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
                                createHddcGeomorphylineGrid();
							}
						});
					}
				});
			}
		});
	};
	window._editHddcGeomorphyline = function(hddcGeomorphylineId) {
		var slidebar = Util.slidebar({
			url: hddcGeomorphylineFormHtml,
			width: "580px",
			cache: false,
			close : true,
			afterLoad: function () {
				initPhotoviewingtoSelectFrom();
				initIssurfacerupturebeltSelectFrom();
                HddccjCommon.initProjectSelect("projectName"); //编辑页面项目名称下拉
                HddccjCommon.initTaskSelect("projectName","taskName"); //编辑页面任务名称根据项目名称切换
                initCreatedateSelectFrom();
				formValidator();
				getHddcGeomorphyline(hddcGeomorphylineId);
				$("#saveBtn").on("click", function () {
					if($("#hddcGeomorphylineForm").valid()){
						var data = Tool.serialize("hddcGeomorphylineForm");
						$.ajax({
							url: getHddcGeomorphylineController(),
                            contentType:"application/json",
                            data: JSON.stringify(data),
							type: "put",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
								createHddcGeomorphylineGrid();
							}
						});
					}
				});
			}
		});
	};
	var deleteHddcGeomorphyline = function() {
		var rows = $("#hddcGeomorphylineGrid").datagrid("getSelections");
		if (rows == null || rows.length == 0) {
			Util.alert("请选择一行数据!");
			return;
		}
		Util.confirm("是否要删除选中的数据?", function() {
			var ids = "";
			$.each(rows, function(i, row){
				ids += row.uuid + ",";

			});
			ids = ids.substr(0,ids.length - 1);
			$.ajax({
				url: getHddcGeomorphylineController() ,
				data: {
					ids : ids
				},
				type: "delete",
				success: function (data) {
					createHddcGeomorphylineGrid();
				}
			});
		}, function() {
			return;
		});

	};

	return {
		init:init
	};
});
