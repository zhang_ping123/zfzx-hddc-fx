define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGeochemicalSvyPoint/hddcGeochemicalsvypointSupport"],function(hddcGeochemicalsvypointSupport){
            hddcGeochemicalsvypointSupport.init();
        });
    };
});