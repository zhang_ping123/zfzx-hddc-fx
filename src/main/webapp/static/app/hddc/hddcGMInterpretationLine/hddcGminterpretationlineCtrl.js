define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGMInterpretationLine/hddcGminterpretationlineSupport"],function(hddcGminterpretationlineSupport){
            hddcGminterpretationlineSupport.init();
        });
    };
});