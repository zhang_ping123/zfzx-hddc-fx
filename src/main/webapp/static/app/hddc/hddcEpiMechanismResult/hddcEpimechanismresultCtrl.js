define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcEpiMechanismResult/hddcEpimechanismresultSupport"],function(hddcEpimechanismresultSupport){
            hddcEpimechanismresultSupport.init();
        });
    };
});