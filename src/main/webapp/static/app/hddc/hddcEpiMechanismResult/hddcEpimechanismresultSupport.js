define([
	"UtilDir/util",
	"UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
    "static/app/hddc/hddccjcommon/hddccjCommon",
	"Date","DateCN", "css!DateCss",
	"EasyUI","EasyUI-lang"
    ],function(Util, Tool, SelectOrg, SearchBlock,HddccjCommon){

    var sysPath =  getServer() + "/static/app/hddc/hddcEpiMechanismResult";

    var hddcEpimechanismresultFormHtml = sysPath + "/views/hddcEpimechanismresultForm.html";
	var getHddcEpimechanismresultController = function(){
		return getServer() + "/hddc/hddcEpimechanismresults";
	};

	/**
	 * 页面初始化
	 */
	var init = function(){
        initSearchBlock();
        queryBtnBind();
        initcreateProvince();
        HddccjCommon.initProjectSelect("projectNameCondition"); //初始化查询的项目名称
		createHddcEpimechanismresultGrid();
	};
    var initcreateProvince = function () {
        var html = "";
        $("#citySelect").append(html);
        $("#areaSelect").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#provinceSelect").append(html);
            }
        });
        $("#provinceSelect").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#citySelect option").remove();
            $("#citySelect").append(html);
            $("#areaSelect option").remove();
            $("#areaSelect").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#citySelect").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#citySelect").append(html);
                }
            });
        });
        $("#citySelect").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#areaSelect option").remove();
            $("#areaSelect").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#areaSelect").append(html);
                }
            });
        });
    }

    var initStressregimeSelectFrom = function () {
		$.ajax({
			url: getHddcEpimechanismresultController() + "/getValidDictItemsByDictCode/" + "StressRegimeCVD",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("stressregime");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};

	var initSearchBlock = function(){
        SearchBlock.init("searchBlock");
	};
	var queryBtnBind = function(){
        $("#btnSearch").click(function () {
            createHddcEpimechanismresultGrid();
        });
        $("#btnReset").click(function () {
            $("#provinceSelect").val("");
	            $("#citySelect").val("");
	            $("#areaSelect").val("");
				$("#projectNameCondition").val("");
			});
	};
    //导入
    var importForm = function () {

        $("#uploadModal").modal();
        $("#uploadModal").on("shown.bs.modal", function () {
            var url = getServer() + "/excel/震源机制解数据-点.xls";
            $("#downloadZwExcelTemplate").attr("href", url);
        });

        $('#uploadButton').off("click");
        $("#uploadButton").on("click", function () {
            var uploadinput = document.getElementById("uploadFile");
            if (uploadinput.value == "") {
                Util.alert("上传前请先选择文件!");
                return;
            }
            var formData = new FormData();
            formData.append("file", uploadinput.files[0]);
            $.ajax({
                url: getHddcEpimechanismresultController() + "/importDisaster",
                data: formData,
                processData: false, //因为data值是FormData对象，不需要对数据做处理。
                contentType: false,
                type: "POST",
                success: function (data) {
                    $('#uploadModal').modal('hide');
                    uploadinput.value = null;
                    Util.alert(data);
                    createHddcEpimechanismresultGrid();
                }
            });
        });
    };
    //导出
    var exportForm = function () {
        var province = $("#provinceSelect").val();
        var city = $("#citySelect").val();
        var area = $("#areaSelect").val();
        var projectName = $("#projectNameCondition").val();
        window.location.href = getHddcEpimechanismresultController() + "/exportFile?province="+province+"&city="+city+"&area="+area+"&projectName="+projectName;
    };
	var createHddcEpimechanismresultGrid= function() {
        $("#hddcEpimechanismresultGrid").datagrid({
            url:getHddcEpimechanismresultController() + "/queryHddcEpimechanismresults",
			method:"GET",
            fitColumns: true,
            autoRowHeight: false,
            columns:[[
                {field:"ck",checkbox:true},
                {
                    field:'id',
                    title:'编号',
                    width:'20%',
                    align:'center',
                    formatter:function(value,rowData,rowIndex){
                        return '<a href="#" onclick="_editHddcEpimechanismresult(\'' + rowData.uuid + '\');"> '+rowData.id+' </a> '
                    }
                },
				{
					field:'province',
					title:'省',
					width:'20%',
					align:'center',
				},
				{
					field:'city',
					title:'市',
					width:'20%',
					align:'center',
				},
				{
					field:'area',
					title:'区（县）',
					width:'20%',
					align:'center',
				},
				{
					field:'projectName',
					title:'项目名称',
					width:'20%',
					align:'center',
				},
				{
					field:'taskName',
					title:'任务名称',
					width:'20%',
					align:'center',
				},
				{
					field:'occurrencedate',
					title:'发震日期',
					width:'20%',
					align:'center',
				},
				{
					field:'occurrencetime',
					title:'发震时间',
					width:'20%',
					align:'center',
				},
            ]],
            toolbar: [{
                iconCls: 'fa fa-plus-circle',
                text:"添加",
                handler: function(){
                    addHddcEpimechanismresult();
                }
            },{
                iconCls: 'fa fa-trash-o',
                text:"删除",
                handler: function(){
                    deleteHddcEpimechanismresult();
                }
            }, {
                iconCls: 'fa fa-upload',
                text: "导入",
                handler: function () {
                    importForm();
                }
            }, {
                iconCls: 'fa fa-download',
                text: "导出",
                handler: function () {
                    exportForm();
                }
            }],
            queryParams:{
                province: $("#provinceSelect").val(),
                city: $("#citySelect").val(),
                area: $("#areaSelect").val(),
				projectName: $("#projectNameCondition").val(),
            },
            pagination: true,
            pageSize: 10
        });
    };
	var formValidator = function(){
		$("#hddcEpimechanismresultForm").validate({
			rules : {
				province : {
					required : true,
				},
				city : {
					required : true,
				},
				area : {
					required : true,
				},
				projectName : {
					required : true,
				},
				taskName : {
					required : true,
				},
				occurrencedate : {
					required : true,
				},
				lon : {
                    range:[-180,180],
				    maxlength : 8,
				    number : true,
					required : true,
				},
				lat : {
                    range:[-90,90],
                    maxlength : 8,
                    number : true,
					required : true,
				},
				magnitude : {
                    maxlength : 8,
                    number : true,
					required : true,
				},
                depth : {
                    maxlength : 8,
                    number : true,
                },
                shazimuth : {
                    maxlength : 4,
                    digits: true,
                    min : 0,
                    max : 359,
                },
                plane1strike : {
                    maxlength : 4,
                    digits: true,
                    min : 0,
                    max : 359,
                },
                plane1dip : {
                    maxlength : 4,
                    digits: true,
                    min : 0,
                    max : 90,
                },
                plane1slip : {
                    maxlength : 4,
                    digits: true,
                    min : 0,
                    max : 359,
                },
                plane2strike : {
                    maxlength : 4,
                    digits: true,
                    min : 0,
                    max : 359,
                },
                plane2dip : {
                    maxlength : 4,
                    digits: true,
                    min : 0,
                    max : 90,
                },
                plane2slip : {
                    maxlength : 4,
                    digits: true,
                    min : 0,
                    max : 359,
                },
                pazimuth : {
                    maxlength : 4,
                    digits: true,
                    min : 0,
                    max : 359,
                },
                pplunge : {
                    maxlength : 4,
                    digits: true,
                    min : 0,
                    max : 90,
                },
                tazimuth : {
                    maxlength : 4,
                    digits: true,
                    min : 0,
                    max : 359,
                },
                tplunge : {
                    maxlength : 4,
                    digits: true,
                    min : 0,
                    max : 90,
                },
                bazimuth : {
                    maxlength : 4,
                    digits: true,
                    min : 0,
                    max : 359,
                },
                bplunge : {
                    maxlength : 4,
                    digits: true,
                    min : 0,
                    max : 90,
                },
                id : {
                    required : true,
                },
			},
			messages : {
				province : {
					required : "省不允许为空!",
				},
				city : {
					required : "市不允许为空!",
				},
				area : {
					required : "区（县）不允许为空!",
				},
				projectName : {
					required : "项目名称不允许为空!",
				},
				taskName : {
					required : "任务名称不允许为空!",
				},
				occurrencedate : {
					required : "发震日期不允许为空!",
				},
				lon : {
					required : "震中经度要不为空的长度不大于8的浮点数!",
				},
				lat : {
					required : "震中纬度要不为空的长度不大于8的浮点数!",
				},
				magnitude : {
					required : "震级要不为空的长度不大于8的浮点数!",
				},
                depth : {
                    required : "震源深度 [公里]要长度不大于8的浮点数!",
                },
                shazimuth : {
                    required : "水平最大主应力方位要大于0的小于359的长度不大于4的整数!",
                },
                plane1strike : {
                    required : "Ⅰ节面走向要大于0的小于359的长度不大于4的整数!",
                },
                plane1dip : {
                    required : "Ⅰ节面倾角要大于0的小于90的长度不大于4的整数!",
                },
                plane1slip : {
                    required : "Ⅰ节面滑动角要大于0的小于359的长度不大于4的整数!",
                },
                plane2strike : {
                    required : "Ⅱ节面走向要大于0的小于359的长度不大于4的整数!",
                },
                plane2dip : {
                    required : "Ⅱ节面倾角要大于0的小于90的长度不大于4的整数!",
                },
                plane2slip : {
                    required : "Ⅱ节面滑动角要大于0的小于359的长度不大于4的整数!",
                },
                pazimuth : {
                    required : "P轴方位要大于0的小于359的长度不大于4的整数!",
                },
                pplunge : {
                    required : "P轴倾角要大于0的小于90的长度不大于4的整数!",
                },
                pazimuth : {
                    required : "T轴方位要大于0的小于359的长度不大于4的整数!",
                },
                pplunge : {
                    required : "T轴倾角要大于0的小于90的长度不大于4的整数!",
                },
                pazimuth : {
                    required : "B轴方位要大于0的小于359的长度不大于4的整数!",
                },
                pplunge : {
                    required : "B轴倾角要大于0的小于90的长度不大于4的整数!",
                },
                id : {
                    required : "编号不允许为空!",
                },
			}
		});
	};
	var getHddcEpimechanismresult = function(id){
		$.ajax({
			url: getHddcEpimechanismresultController() + "/"+id,
			type: "get",
			success: function (data) {
                editProvince(data.province, data.city, data.area);
                // 回显项目名称
                $("#projectName").val(data.projectName);
                // 回显任务名称
                HddccjCommon.initEditTaskSelect("taskName",data.projectName);
                $("#taskName").val(data.taskName);
				Tool.deserialize("hddcEpimechanismresultForm", data);
			}
		});
	};
    var initcreateProvinceForm = function () {
        var html = "";
        $("#city").append(html);
        $("#area").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#province").append(html);
            }
        });
        $("#province").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#city option").remove();
            $("#city").append(html);
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#city").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#city").append(html);
                }
            });
        });
        $("#city").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#area").append(html);
                }
            });
        });
    }

	var addHddcEpimechanismresult = function () {
		var slidebar = Util.slidebar({
			url: hddcEpimechanismresultFormHtml,
			width: "800px",
			cache: false,
			close : true,
			afterLoad: function () {
                initcreateProvinceForm();
				initStressregimeSelectFrom();
				formValidator();
                HddccjCommon.initProjectSelect("projectName"); //添加页面项目名称下拉
                HddccjCommon.initTaskSelect("projectName", "taskName"); //添加页面任务名称根据项目名称切换
				$("#saveBtn").on("click", function () {
					if($("#hddcEpimechanismresultForm").valid()){
						var data = Tool.serialize("hddcEpimechanismresultForm");
						$.ajax({
							url: getHddcEpimechanismresultController() ,
                            contentType:"application/json",
							data: JSON.stringify(data),
							type: "post",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
                                createHddcEpimechanismresultGrid();
							}
						});
					}
				});
			}
		});
	};
    var editProvince = function (Province, City, Area) {
        debugger;
        var html = "";
        $("#city").append(html);
        $("#area").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    if (item.divisionName == Province) {
                        debugger;
                        var divisionId = item.divisionId;
                        var htmlCity = '';
                        if (Province == "北京市" || Province == "天津市" || Province == "上海市" || Province == "重庆市") {
                            $("#city").append("<option value='" + Province + "' exid='" + divisionId + "'>" + Province + "</option>");
                            $('#city').val(City);
                            var htmlArea = '';
                            $.ajax({
                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                contentType: "application/json",
                                type: "get",
                                success: function (data) {
                                    $.each(data, function (idx, item) {
                                        htmlArea += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                    });
                                    $("#area").append(htmlArea);
                                    $('#area').val(Area);
                                }
                            });

                        } else {
                            $.ajax({
                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                contentType: "application/json",
                                type: "get",
                                success: function (data) {
                                    debugger;
                                    $.each(data, function (idx, item) {
                                        if (item.divisionName == City) {
                                            var divisionId = item.divisionId;
                                            var htmlArea = '';
                                            $.ajax({
                                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                                contentType: "application/json",
                                                type: "get",
                                                success: function (data) {
                                                    $.each(data, function (idx, item) {
                                                        htmlArea += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                                    });
                                                    $("#area").append(htmlArea);
                                                    $('#area').val(Area);
                                                }
                                            });
                                        }
                                        htmlCity += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                    });
                                    $("#city").append(htmlCity);
                                    $('#city').val(City);
                                }
                            });
                        }
                    }
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#province").append(html);
                $('#province').val(Province);
            }
        });
        $("#province").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#city option").remove();
            $("#city").append(html);
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#city").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#city").append(html);
                }
            });
        });
        $("#city").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#area").append(html);
                }
            });
        });
    }
	window._editHddcEpimechanismresult = function(hddcEpimechanismresultId) {
		var slidebar = Util.slidebar({
			url: hddcEpimechanismresultFormHtml,
			width: "800px",
			cache: false,
			close : true,
			afterLoad: function () {
				initStressregimeSelectFrom();
				formValidator();
                HddccjCommon.initProjectSelect("projectName"); //编辑页面项目名称下拉
                HddccjCommon.initTaskSelect("projectName","taskName"); //编辑页面任务名称根据项目名称切换
				getHddcEpimechanismresult(hddcEpimechanismresultId);
				$("#saveBtn").on("click", function () {
					if($("#hddcEpimechanismresultForm").valid()){
						var data = Tool.serialize("hddcEpimechanismresultForm");
						$.ajax({
							url: getHddcEpimechanismresultController(),
                            contentType:"application/json",
                            data: JSON.stringify(data),
							type: "put",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
								createHddcEpimechanismresultGrid();
							}
						});
					}
				});
			}
		});
	};
	var deleteHddcEpimechanismresult = function() {
		var rows = $("#hddcEpimechanismresultGrid").datagrid("getSelections");
		if (rows == null || rows.length == 0) {
			Util.alert("请选择一行数据!");
			return;
		}
		Util.confirm("是否要删除选中的数据?", function() {
			var ids = "";
			$.each(rows, function(i, row){
				ids += row.uuid + ",";

			});
			ids = ids.substr(0,ids.length - 1);
			$.ajax({
				url: getHddcEpimechanismresultController() ,
				data: {
					ids : ids
				},
				type: "delete",
				success: function (data) {
					createHddcEpimechanismresultGrid();
				}
			});
		}, function() {
			return;
		});

	};

	return {
		init:init
	};
});
