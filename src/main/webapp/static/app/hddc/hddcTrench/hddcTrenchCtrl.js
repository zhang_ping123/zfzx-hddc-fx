define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcTrench/hddcTrenchSupport"],function(hddcTrenchSupport){
            hddcTrenchSupport.init();
        });
    };
});