define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcwyTrench/hddcWyTrenchSupport"],function(hddcWyTrenchSupport){
            hddcWyTrenchSupport.init();
        });
    };
});