define([
	"UtilDir/util",
	"UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
    "static/app/hddc/hddccjcommon/hddccjCommon",
	"Date","DateCN", "css!DateCss",
	"EasyUI","EasyUI-lang"
    ],function(Util, Tool, SelectOrg, SearchBlock,HddccjCommon){

    var sysPath =  getServer() + "/static/app/hddc/hddcC4MagnitudeEstimateTable";

    var hddcC4MagnitudeestimatetableFormHtml = sysPath + "/views/hddcC4MagnitudeestimatetableForm.html";
	var getHddcC4MagnitudeestimatetableController = function(){
		return getServer() + "/hddcC4Magnitudeestimatetables";
	};

	/**
	 * 页面初始化
	 */
	var init = function(){
        initSearchBlock();
        queryBtnBind();
		createHddcC4MagnitudeestimatetableGrid();
	};


	var initSearchBlock = function(){
        SearchBlock.init("searchBlock");
	};
	var queryBtnBind = function(){
        $("#btnSearch").click(function () {
            createHddcC4MagnitudeestimatetableGrid();
        });
        $("#btnReset").click(function () {
			$("#projectIdCondition").val("");
				$("#examineDateCondition").val("");
			});
	};
	var createHddcC4MagnitudeestimatetableGrid= function() {
        $("#hddcC4MagnitudeestimatetableGrid").datagrid({
            url:getHddcC4MagnitudeestimatetableController() + "/queryHddcC4Magnitudeestimatetables",
			method:"GET",
            fitColumns: true,
            autoRowHeight: false,
            columns:[[
				{
					field:'updateUser',
					title:'修改人',
					width:'20%',
					align:'center',
				},
				{
					field:'qualityinspectionComments',
					title:'质检原因',
					width:'20%',
					align:'center',
				},
				{
					field:'examineComments',
					title:'审查意见',
					width:'20%',
					align:'center',
				},
				{
					field:'projectId',
					title:'项目ID',
					width:'20%',
					align:'center',
				},
				{
					field:'maxm',
					title:'震级上限',
					width:'20%',
					align:'center',
				},
				{
					field:'qualityinspectionUser',
					title:'质检人',
					width:'20%',
					align:'center',
				},
				{
					field:'reviewStatus',
					title:'审核状态（保存）',
					width:'20%',
					align:'center',
				},
				{
					field:'createTime',
					title:'创建时间',
					width:'20%',
					align:'center',
				},
            ]],
            toolbar: [{
                iconCls: 'fa fa-plus-circle',
                text:"添加",
                handler: function(){
                    addHddcC4Magnitudeestimatetable();
                }
            },{
                iconCls: 'fa fa-trash-o',
                text:"删除",
                handler: function(){
                    deleteHddcC4Magnitudeestimatetable();
                }
            }],
            queryParams:{
				projectId: $("#projectIdCondition").val(),
				examineDate: $("#examineDateCondition").val(),
            },
            pagination: true,
            pageSize: 10
        });
    };
	var formValidator = function(){
		$("#hddcC4MagnitudeestimatetableForm").validate({
			rules : {
				qualityinspectionComments : {
					required : true,
				},
				examineComments : {
					required : true,
				},
				maxm : {
					required : true,
				},
				commentinfo : {
					required : true,
				},
				projectId : {
					required : true,
				},
				mweight : {
					required : true,
				},
				segmentid : {
					required : true,
				},
				examineDate : {
					required : true,
				},
				qualityinspectionUser : {
					required : true,
				},
				faultid : {
					required : true,
				},
			},
			messages : {
				qualityinspectionComments : {
					required : "质检原因不允许为空!",
				},
				examineComments : {
					required : "审查意见不允许为空!",
				},
				maxm : {
					required : "震级上限不允许为空!",
				},
				commentinfo : {
					required : "备注不允许为空!",
				},
				projectId : {
					required : "项目ID不允许为空!",
				},
				mweight : {
					required : "权重不允许为空!",
				},
				segmentid : {
					required : "断层分段编号不允许为空!",
				},
				examineDate : {
					required : "审查时间不允许为空!",
				},
				qualityinspectionUser : {
					required : "质检人不允许为空!",
				},
				faultid : {
					required : "断层编号不允许为空!",
				},
			}
		});
	};
	var getHddcC4Magnitudeestimatetable = function(id){
		$.ajax({
			url: getHddcC4MagnitudeestimatetableController() + "/"+id,
			type: "get",
			success: function (data) {
				Tool.deserialize("hddcC4MagnitudeestimatetableForm", data);
			}
		});
	};

	var addHddcC4Magnitudeestimatetable = function () {
		var slidebar = Util.slidebar({
			url: hddcC4MagnitudeestimatetableFormHtml,
			width: "580px",
			cache: false,
			close : true,
			afterLoad: function () {
				formValidator();
				$("#saveBtn").on("click", function () {
					if($("#hddcC4MagnitudeestimatetableForm").valid()){
						var data = Tool.serialize("hddcC4MagnitudeestimatetableForm");
						$.ajax({
							url: getHddcC4MagnitudeestimatetableController() ,
                            contentType:"application/json",
							data: JSON.stringify(data),
							type: "post",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
                                createHddcC4MagnitudeestimatetableGrid();
							}
						});
					}
				});
			}
		});
	};
	window._editHddcC4Magnitudeestimatetable = function(hddcC4MagnitudeestimatetableId) {
		var slidebar = Util.slidebar({
			url: hddcC4MagnitudeestimatetableFormHtml,
			width: "580px",
			cache: false,
			close : true,
			afterLoad: function () {
				formValidator();
				getHddcC4Magnitudeestimatetable(hddcC4MagnitudeestimatetableId);
				$("#saveBtn").on("click", function () {
					if($("#hddcC4MagnitudeestimatetableForm").valid()){
						var data = Tool.serialize("hddcC4MagnitudeestimatetableForm");
						$.ajax({
							url: getHddcC4MagnitudeestimatetableController(),
                            contentType:"application/json",
                            data: JSON.stringify(data),
							type: "put",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
								createHddcC4MagnitudeestimatetableGrid();
							}
						});
					}
				});
			}
		});
	};
	var deleteHddcC4Magnitudeestimatetable = function() {
		var rows = $("#hddcC4MagnitudeestimatetableGrid").datagrid("getSelections");
		if (rows == null || rows.length == 0) {
			Util.alert("请选择一行数据!");
			return;
		}
		Util.confirm("是否要删除选中的数据?", function() {
			var ids = "";
			$.each(rows, function(i, row){
				ids += row.faultid + ",";

			});
			ids = ids.substr(0,ids.length - 1);
			$.ajax({
				url: getHddcC4MagnitudeestimatetableController() ,
				data: {
					ids : ids
				},
				type: "delete",
				success: function (data) {
					createHddcC4MagnitudeestimatetableGrid();
				}
			});
		}, function() {
			return;
		});

	};

	return {
		init:init
	};
});
