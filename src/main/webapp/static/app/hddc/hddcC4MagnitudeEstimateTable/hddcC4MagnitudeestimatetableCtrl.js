define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcC4MagnitudeEstimateTable/hddcC4MagnitudeestimatetableSupport"],function(hddcC4MagnitudeestimatetableSupport){
            hddcC4MagnitudeestimatetableSupport.init();
        });
    };
});