define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcStation/hddcStationSupport"],function(hddcStationSupport){
            hddcStationSupport.init();
        });
    };
});