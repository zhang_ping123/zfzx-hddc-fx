define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcwyVolcanicSamplePoint/hddcWyVolcanicsamplepointSupport"],function(hddcWyVolcanicsamplepointSupport){
            hddcWyVolcanicsamplepointSupport.init();
        });
    };
});