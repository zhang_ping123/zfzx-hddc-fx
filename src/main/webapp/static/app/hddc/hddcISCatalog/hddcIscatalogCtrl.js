define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcISCatalog/hddcIscatalogSupport"],function(hddcIscatalogSupport){
            hddcIscatalogSupport.init();
        });
    };
});