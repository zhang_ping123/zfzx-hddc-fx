define([
	"UtilDir/util",
	"UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
	"Date","DateCN", "css!DateCss",
	"EasyUI","EasyUI-lang"
    ],function(Util, Tool, SelectOrg, SearchBlock){

    var sysPath =  getServer() + "/static/app/hddc/hddcwyGeochemicalSvyPoint";

    var hddcWyGeochemicalsvypointFormHtml = sysPath + "/views/hddcWyGeochemicalsvypointForm.html";
	var getHddcWyGeochemicalsvypointController = function(){
		return getServer() + "/hddc/hddcWyGeochemicalsvypoints";
	};

	/**
	 * 页面初始化
	 */
	var init = function(){
        initSearchBlock();
        queryBtnBind();
        initcreateProvince();
		createHddcWyGeochemicalsvypointGrid();
	};



    var initcreateProvince = function () {
        var html = "";
        $("#citySelect").append(html);
        $("#areaSelect").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#provinceSelect").append(html);
            }
        });
        $("#provinceSelect").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#citySelect option").remove();
            $("#citySelect").append(html);
            $("#areaSelect option").remove();
            $("#areaSelect").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#citySelect").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#citySelect").append(html);
                }
            });
        });
        $("#citySelect").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#areaSelect option").remove();
            $("#areaSelect").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#areaSelect").append(html);
                }
            });
        });
    }
    var initcreateProvinceForm = function () {
        var html = "";
        $("#city").append(html);
        $("#area").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#province").append(html);
            }
        });
        $("#province").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#city option").remove();
            $("#city").append(html);
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#city").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#city").append(html);
                }
            });
        });
        $("#city").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#area").append(html);
                }
            });
        });
    }


    var editProvince = function (Province, City, Area) {
        //debugger;
        var html = "";
        $("#city").append(html);
        $("#area").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    if (item.divisionName == Province) {
                        //debugger;
                        var divisionId = item.divisionId;
                        var htmlCity = '';
                        if (Province == "北京市" || Province == "天津市" || Province == "上海市" || Province == "重庆市") {
                            $("#city").append("<option value='" + Province + "' exid='" + divisionId + "'>" + Province + "</option>");
                            $('#city').val(City);
                            var htmlArea = '';
                            $.ajax({
                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                contentType: "application/json",
                                type: "get",
                                success: function (data) {
                                    $.each(data, function (idx, item) {
                                        htmlArea += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                    });
                                    $("#area").append(htmlArea);
                                    $('#area').val(Area);
                                }
                            });

                        } else {
                            $.ajax({
                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                contentType: "application/json",
                                type: "get",
                                success: function (data) {
                                    //debugger;
                                    $.each(data, function (idx, item) {
                                        if (item.divisionName == City) {
                                            var divisionId = item.divisionId;
                                            var htmlArea = '';
                                            $.ajax({
                                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                                contentType: "application/json",
                                                type: "get",
                                                success: function (data) {
                                                    $.each(data, function (idx, item) {
                                                        htmlArea += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                                    });
                                                    $("#area").append(htmlArea);
                                                    $('#area').val(Area);
                                                }
                                            });
                                        }
                                        htmlCity += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                    });
                                    $("#city").append(htmlCity);
                                    $('#city').val(City);
                                }
                            });
                        }
                    }
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#province").append(html);
                $('#province').val(Province);
            }
        });
        $("#province").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#city option").remove();
            $("#city").append(html);
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#city").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#city").append(html);
                }
            });
        });
        $("#city").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#area").append(html);
                }
            });
        });
    }

	var initProvinceSelect = function () {
		$.ajax({
			url: getHddcWyGeochemicalsvypointController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("provinceSelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initCitySelect = function () {
		$.ajax({
			url: getHddcWyGeochemicalsvypointController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("citySelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initAreaSelect = function () {
		$.ajax({
			url: getHddcWyGeochemicalsvypointController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("areaSelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};

		var initProvinceSelectFrom = function () {
		$.ajax({
			url: getHddcWyGeochemicalsvypointController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("province");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initCitySelectFrom = function () {
		$.ajax({
			url: getHddcWyGeochemicalsvypointController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("city");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initAreaSelectFrom = function () {
		$.ajax({
			url: getHddcWyGeochemicalsvypointController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("area");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};

	var initSearchBlock = function(){
        SearchBlock.init("searchBlock");
	};
	var queryBtnBind = function(){
        $("#btnSearch").click(function () {
            createHddcWyGeochemicalsvypointGrid();
        });
        $("#btnReset").click(function () {
            $("#provinceSelect").val("");
	            $("#citySelect").val("");
	            $("#areaSelect").val("");
				$("#projectNameCondition").val("");
				$("#labelinfoCondition").val("");
			});
	};
	var createHddcWyGeochemicalsvypointGrid= function() {
        $("#hddcWyGeochemicalsvypointGrid").datagrid({
            url:getHddcWyGeochemicalsvypointController() + "/queryHddcWyGeochemicalsvypoints",
			method:"GET",
            fitColumns: true,
            autoRowHeight: false,
            columns:[[
                {field:"ck",checkbox:true},
				{
					field:'province',
					title:'省',
					width:'20%',
					align:'center',
				},
				{
					field:'city',
					title:'市',
					width:'20%',
					align:'center',
				},
				{
					field:'area',
					title:'区（县）',
					width:'20%',
					align:'center',
				},
				{
					field:'id',
					title:'测点编号',
					width:'20%',
					align:'center',
				},
				{
					field:'fieldid',
					title:'野外编号',
					width:'20%',
					align:'center',
				},
				{
					field:'svylineid',
					title:'测线编号',
					width:'20%',
					align:'center',
				},
				{
					field:'labelinfo',
					title:'拐点标注名称',
					width:'20%',
					align:'center',
				},
				{
					field:'commentInfo',
					title:'备注',
					width:'20%',
					align:'center',
				},
            ]],
            toolbar: [{
                iconCls: 'fa fa-plus-circle',
                text:"添加",
                handler: function(){
                    addHddcWyGeochemicalsvypoint();
                }
            },{
                iconCls: 'fa fa-trash-o',
                text:"删除",
                handler: function(){
                    deleteHddcWyGeochemicalsvypoint();
                }
            }],
            queryParams:{
                province: $("#provinceSelect").val(),
                city: $("#citySelect").val(),
                area: $("#areaSelect").val(),
				projectName: $("#projectNameCondition").val(),
				labelinfo: $("#labelinfoCondition").val(),
            },
            pagination: true,
            pageSize: 10
        });
    };
	var formValidator = function(){
		$("#hddcWyGeochemicalsvypointForm").validate({
			rules : {
				province : {
					required : true,
				},
				city : {
					required : true,
				},
				area : {
					required : true,
				},
				id : {
					required : true,
				},
			},
			messages : {
				province : {
					required : "省不允许为空!",
				},
				city : {
					required : "市不允许为空!",
				},
				area : {
					required : "区（县）不允许为空!",
				},
				id : {
					required : "测点编号不允许为空!",
				},
			}
		});
	};
	var getHddcWyGeochemicalsvypoint = function(id){
		$.ajax({
			url: getHddcWyGeochemicalsvypointController() + "/"+id,
			type: "get",
			success: function (data) {
                editProvince(data.province, data.city, data.area);
                Tool.deserialize("hddcWyGeochemicalsvypointForm", data);
			}
		});
	};

	var addHddcWyGeochemicalsvypoint = function () {
		var slidebar = Util.slidebar({
			url: hddcWyGeochemicalsvypointFormHtml,
			width: "580px",
			cache: false,
			close : true,
			afterLoad: function () {
                initcreateProvinceForm();
				formValidator();
				$("#saveBtn").on("click", function () {
					if($("#hddcWyGeochemicalsvypointForm").valid()){
						var data = Tool.serialize("hddcWyGeochemicalsvypointForm");
						$.ajax({
							url: getHddcWyGeochemicalsvypointController() ,
                            contentType:"application/json",
							data: JSON.stringify(data),
							type: "post",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
                                createHddcWyGeochemicalsvypointGrid();
							}
						});
					}
				});
			}
		});
	};
	window._editHddcWyGeochemicalsvypoint = function(hddcWyGeochemicalsvypointId) {
		var slidebar = Util.slidebar({
			url: hddcWyGeochemicalsvypointFormHtml,
			width: "580px",
			cache: false,
			close : true,
			afterLoad: function () {
                initcreateProvinceForm();
				formValidator();
				getHddcWyGeochemicalsvypoint(hddcWyGeochemicalsvypointId);
				$("#saveBtn").on("click", function () {
					if($("#hddcWyGeochemicalsvypointForm").valid()){
						var data = Tool.serialize("hddcWyGeochemicalsvypointForm");
						$.ajax({
							url: getHddcWyGeochemicalsvypointController(),
                            contentType:"application/json",
                            data: JSON.stringify(data),
							type: "put",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
								createHddcWyGeochemicalsvypointGrid();
							}
						});
					}
				});
			}
		});
	};
	var deleteHddcWyGeochemicalsvypoint = function() {
		var rows = $("#hddcWyGeochemicalsvypointGrid").datagrid("getSelections");
		if (rows == null || rows.length == 0) {
			Util.alert("请选择一行数据!");
			return;
		}
		Util.confirm("是否要删除选中的数据?", function() {
			var ids = "";
			$.each(rows, function(i, row){
				ids += row.uuid + ",";

			});
			ids = ids.substr(0,ids.length - 1);
			$.ajax({
				url: getHddcWyGeochemicalsvypointController() ,
				data: {
					ids : ids
				},
				type: "delete",
				success: function (data) {
					createHddcWyGeochemicalsvypointGrid();
				}
			});
		}, function() {
			return;
		});

	};

	return {
		init:init
	};
});
