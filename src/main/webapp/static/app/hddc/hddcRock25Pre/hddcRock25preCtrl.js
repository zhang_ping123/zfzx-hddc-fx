define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcRock25Pre/hddcRock25preSupport"],function(hddcRock25preSupport){
            hddcRock25preSupport.init();
        });
    };
});