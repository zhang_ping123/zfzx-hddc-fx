define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGeoGeomorphySvyPoint/hddcGeogeomorphysvypointSupport"],function(hddcGeogeomorphysvypointSupport){
            hddcGeogeomorphysvypointSupport.init();
        });
    };
});