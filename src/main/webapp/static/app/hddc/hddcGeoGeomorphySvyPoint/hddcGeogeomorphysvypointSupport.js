define([
    "UtilDir/util",
    "UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
    "static/app/hddc/hddccjcommon/hddccjCommon",
    "Date", "DateCN", "css!DateCss",
    "EasyUI", "EasyUI-lang"
], function (Util, Tool, SelectOrg, SearchBlock,HddccjCommon) {

    var sysPath = getServer() + "/static/app/hddc/hddcGeoGeomorphySvyPoint";

    var hddcGeogeomorphysvypointFormHtml = sysPath + "/views/hddcGeogeomorphysvypointForm.html";
    var getHddcGeogeomorphysvypointController = function () {
        return getServer() + "/hddc/hddcGeogeomorphysvypoints";
    };

    /**
     * 页面初始化
     */
    var init = function () {
        initSearchBlock();
        queryBtnBind();
        initcreateProvince();
        HddccjCommon.initProjectSelect("projectNameCondition"); //初始化查询的项目名称
        createHddcGeogeomorphysvypointGrid();
    };
    var initProvinceSelect = function () {
        $.ajax({
            url: getHddcGeogeomorphysvypointController() + "/getValidDictItemsByDictCode/" + "",
            type: "get",
            success: function (data) {
                var mySelect = document.getElementById("provinceSelect");
                mySelect.add(new Option("请选择", ''), 0);
                for (var i = 0; i < data.length; i++) {
                    var name = data[i].dictItemName;
                    var code = data[i].dictItemCode;
                    mySelect.add(new Option(name, code));
                }
            }
        });
    };
    var initCitySelect = function () {
        $.ajax({
            url: getHddcGeogeomorphysvypointController() + "/getValidDictItemsByDictCode/" + "",
            type: "get",
            success: function (data) {
                var mySelect = document.getElementById("citySelect");
                mySelect.add(new Option("请选择", ''), 0);
                for (var i = 0; i < data.length; i++) {
                    var name = data[i].dictItemName;
                    var code = data[i].dictItemCode;
                    mySelect.add(new Option(name, code));
                }
            }
        });
    };
    var initAreaSelect = function () {
        $.ajax({
            url: getHddcGeogeomorphysvypointController() + "/getValidDictItemsByDictCode/" + "",
            type: "get",
            success: function (data) {
                var mySelect = document.getElementById("areaSelect");
                mySelect.add(new Option("请选择", ''), 0);
                for (var i = 0; i < data.length; i++) {
                    var name = data[i].dictItemName;
                    var code = data[i].dictItemCode;
                    mySelect.add(new Option(name, code));
                }
            }
        });
    };

    var initProvinceSelectFrom = function () {
        $.ajax({
            url: getHddcGeogeomorphysvypointController() + "/getValidDictItemsByDictCode/" + "",
            type: "get",
            success: function (data) {
                var mySelect = document.getElementById("province");
                mySelect.add(new Option("请选择", ''), 0);
                for (var i = 0; i < data.length; i++) {
                    var name = data[i].dictItemName;
                    var code = data[i].dictItemCode;
                    mySelect.add(new Option(name, code));
                }
            }
        });
    };
    var initCitySelectFrom = function () {
        $.ajax({
            url: getHddcGeogeomorphysvypointController() + "/getValidDictItemsByDictCode/" + "",
            type: "get",
            success: function (data) {
                var mySelect = document.getElementById("city");
                mySelect.add(new Option("请选择", ''), 0);
                for (var i = 0; i < data.length; i++) {
                    var name = data[i].dictItemName;
                    var code = data[i].dictItemCode;
                    mySelect.add(new Option(name, code));
                }
            }
        });
    };
    var initAreaSelectFrom = function () {
        $.ajax({
            url: getHddcGeogeomorphysvypointController() + "/getValidDictItemsByDictCode/" + "",
            type: "get",
            success: function (data) {
                var mySelect = document.getElementById("area");
                mySelect.add(new Option("请选择", ''), 0);
                for (var i = 0; i < data.length; i++) {
                    var name = data[i].dictItemName;
                    var code = data[i].dictItemCode;
                    mySelect.add(new Option(name, code));
                }
            }
        });
    };
    var initPhotoviewingtoSelectFrom = function () {
        $.ajax({
            url: getHddcGeogeomorphysvypointController() + "/getValidDictItemsByDictCode/" + "CVD16Direction",
            type: "get",
            success: function (data) {
                var mySelect = document.getElementById("photoviewingto");
                mySelect.add(new Option("请选择", ''), 0);
                for (var i = 0; i < data.length; i++) {
                    var name = data[i].dictItemName;
                    var code = data[i].dictItemCode;
                    mySelect.add(new Option(name, code));
                }
            }
        });
    };

    var initcreateProvince = function () {
        var html = "";
        $("#citySelect").append(html);
        $("#areaSelect").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#provinceSelect").append(html);
            }
        });
        $("#provinceSelect").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#citySelect option").remove();
            $("#citySelect").append(html);
            $("#areaSelect option").remove();
            $("#areaSelect").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#citySelect").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#citySelect").append(html);
                }
            });
        });
        $("#citySelect").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#areaSelect option").remove();
            $("#areaSelect").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#areaSelect").append(html);
                }
            });
        });
    }
    var initcreateProvinceForm = function () {
        var html = "";
        $("#city").append(html);
        $("#area").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#province").append(html);
            }
        });
        $("#province").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#city option").remove();
            $("#city").append(html);
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#city").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#city").append(html);
                }
            });
        });
        $("#city").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#area").append(html);
                }
            });
        });
    }


    var editProvince = function (Province, City, Area) {
        debugger;
        var html = "";
        $("#city").append(html);
        $("#area").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    if (item.divisionName == Province) {
                        debugger;
                        var divisionId = item.divisionId;
                        var htmlCity = '';
                        if (Province == "北京市" || Province == "天津市" || Province == "上海市" || Province == "重庆市") {
                            $("#city").append("<option value='" + Province + "' exid='" + divisionId + "'>" + Province + "</option>");
                            $('#city').val(City);
                            var htmlArea = '';
                            $.ajax({
                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                contentType: "application/json",
                                type: "get",
                                success: function (data) {
                                    $.each(data, function (idx, item) {
                                        htmlArea += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                    });
                                    $("#area").append(htmlArea);
                                    $('#area').val(Area);
                                }
                            });

                        } else {
                            $.ajax({
                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                contentType: "application/json",
                                type: "get",
                                success: function (data) {
                                    debugger;
                                    $.each(data, function (idx, item) {
                                        if (item.divisionName == City) {
                                            var divisionId = item.divisionId;
                                            var htmlArea = '';
                                            $.ajax({
                                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                                contentType: "application/json",
                                                type: "get",
                                                success: function (data) {
                                                    $.each(data, function (idx, item) {
                                                        htmlArea += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                                    });
                                                    $("#area").append(htmlArea);
                                                    $('#area').val(Area);
                                                }
                                            });
                                        }
                                        htmlCity += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                    });
                                    $("#city").append(htmlCity);
                                    $('#city').val(City);
                                }
                            });
                        }
                    }
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#province").append(html);
                $('#province').val(Province);
            }
        });
        $("#province").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#city option").remove();
            $("#city").append(html);
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#city").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#city").append(html);
                }
            });
        });
        $("#city").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#area").append(html);
                }
            });
        });
    }
    var initCreatedateSelectFrom = function () {
        $.ajax({
            url: getHddcGeogeomorphysvypointController() + "/getValidDictItemsByDictCode/" + "AgeCVD",
            type: "get",
            success: function (data) {
                var mySelect = document.getElementById("createdate");
                mySelect.add(new Option("请选择", ''), 0);
                for (var i = 0; i < data.length; i++) {
                    var name = data[i].dictItemName;
                    var code = data[i].dictItemCode;
                    mySelect.add(new Option(name, code));
                }
            }
        });
    };

    var initSearchBlock = function () {
        SearchBlock.init("searchBlock");
    };
    var queryBtnBind = function () {
        $("#btnSearch").click(function () {
            createHddcGeogeomorphysvypointGrid();
        });
        $("#btnReset").click(function () {
            $("#provinceSelect").val("");
            $("#citySelect").val("");
            $("#areaSelect").val("");
            $("#projectNameCondition").val("");
            $("#geomorphynameCondition").val("");
        });
    };
    //导入
    var importForm = function () {

        $("#uploadModal").modal();
        $("#uploadModal").on("shown.bs.modal", function () {
            var url = getServer() + "/excel/地质地貌调查观测点-点.xls";
            $("#downloadZwExcelTemplate").attr("href", url);
        });

        $('#uploadButton').off("click");
        $("#uploadButton").on("click", function () {
            var uploadinput = document.getElementById("uploadFile");
            if (uploadinput.value == "") {
                Util.alert("上传前请先选择文件!");
                return;
            }
            var formData = new FormData();
            formData.append("file", uploadinput.files[0]);
            $.ajax({
                url: getHddcGeogeomorphysvypointController() + "/importDisaster",
                data: formData,
                processData: false, //因为data值是FormData对象，不需要对数据做处理。
                contentType: false,
                type: "POST",
                success: function (data) {
                    $('#uploadModal').modal('hide');
                    uploadinput.value = null;
                    Util.alert(data);
                    createHddcGeogeomorphysvypointGrid();
                }
            });
        });
    };
    var createHddcGeogeomorphysvypointGrid = function () {
        $("#hddcGeogeomorphysvypointGrid").datagrid({
            url: getHddcGeogeomorphysvypointController() + "/queryHddcGeogeomorphysvypoints",
            method: "GET",
            fitColumns: true,
            autoRowHeight: false,
            columns: [[
                {field: "ck", checkbox: true},
                {
                    field: 'id',
                    title: '编号',
                    width: '20%',
                    align: 'center',
                    formatter: function (value, rowData, rowIndex) {
                        return '<a href="#" onclick="_editHddcGeogeomorphysvypoint(\'' + rowData.uuid + '\');"> ' + rowData.id + ' </a> '
                    }
                },
                {
                    field: 'geomorphyname',
                    title: '地貌点名称',
                    width: '20%',
                    align: 'center',
                },
                {
                    field: 'province',
                    title: '省',
                    width: '20%',
                    align: 'center',
                },
                {
                    field: 'city',
                    title: '市',
                    width: '20%',
                    align: 'center',
                },
                {
                    field: 'area',
                    title: '区（县）',
                    width: '20%',
                    align: 'center',
                },
                {
                    field: 'projectName',
                    title: '项目名称',
                    width: '20%',
                    align: 'center'
                },
                {
                    field: 'taskName',
                    title: '任务名称',
                    width: '20%',
                    align: 'center',
                },

            ]],
            toolbar: [{
                iconCls: 'fa fa-plus-circle',
                text: "添加",
                handler: function () {
                    addHddcGeogeomorphysvypoint();
                }
            }, {
                iconCls: 'fa fa-trash-o',
                text: "删除",
                handler: function () {
                    deleteHddcGeogeomorphysvypoint();
                }
            }, {
                iconCls: 'fa fa-upload',
                text: "导入",
                handler: function () {
                    importForm();
                }
            }, {
                iconCls: 'fa fa-download',
                text: "导出",
                handler: function () {
                    exportForm();
                }
            }],
            queryParams: {
                province: $("#provinceSelect").val(),
                city: $("#citySelect").val(),
                area: $("#areaSelect").val(),
                projectName: $("#projectNameCondition").val(),
                geomorphyname: $("#geomorphynameCondition").val(),
            },
            pagination: true,
            pageSize: 10
        });
    };
    var formValidator = function () {
        $("#hddcGeogeomorphysvypointForm").validate({
            rules: {
                province: {
                    required: true,
                },
                city: {
                    required: true,
                },
                area: {
                    required: true,
                },
                id: {
                    required: true,
                },
                projectName: {
                    required: true,
                },
                taskName: {
                    required: true,
                },
                length : {
                    maxlength : 8,
                    number : true,
                },
                width : {
                    maxlength : 8,
                    number : true,
                },
                height : {
                    maxlength : 8,
                    number : true,
                },
                verticaldisplacement : {
                    maxlength : 8,
                    number : true,
                },
                horizenoffset : {
                    maxlength : 8,
                    number : true,
                },
                tensiondisplacement : {
                    maxlength : 8,
                    number : true,
                },
                geomorphylndirection : {
                    maxlength : 4,
                    digits: true,
                    min : 0,
                    max : 359,
                },
            extends5 : {
                range:[-180,180],
                    number : true,
                    maxlength :8,
            },
            extends6 : {
                range:[-90,90],
                    number : true,
                    maxlength :8,
            },

            },
            messages: {
                province: {
                    required: "省不允许为空!",
                },
                city: {
                    required: "市不允许为空!",
                },
                area: {
                    required: "区（县）不允许为空!",
                },
                id: {
                    required: "编号不允许为空!",
                },
                projectName: {
                    required: "项目名称不允许为空!",
                },
                taskName: {
                    required: "任务名称不允许为空!",
                },
                length : {
                    required : "地表破裂（断塞塘等）长 [米]要长度不大于8的浮点数!",
                },
                width : {
                    required : "地表破裂（断塞塘等）宽 [米]要长度不大于8的浮点数!",
                },
                height : {
                    required : "地表破裂（断塞塘等）高/深 [米]要长度不大于8的浮点数!",
                },
                verticaldisplacement : {
                    required : "垂直位移 [米]要长度不大于8的浮点数!",
                },
                horizenoffset : {
                    required : "走向水平位移 [米]要长度不大于8的浮点数!",
                },
                tensiondisplacement : {
                    required : "水平//张缩位移 [米]要长度不大于8的浮点数!",
                },
                geomorphylndirection : {
                    required : "水平最大主应力方位要大于0的小于359的长度不大于4的整数!",
                },
                lastangle : {
                    required : "符号或标注旋转角度要长度不大于8的浮点数!",
                },
            },
        });
    };
    var getHddcGeogeomorphysvypoint = function (id) {
        $.ajax({
            url: getHddcGeogeomorphysvypointController() + "/" + id,
            type: "get",
            success: function (data) {
                editProvince(data.province, data.city, data.area);
                // 回显项目名称
                $("#projectName").val(data.projectName);
                // 回显任务名称
                HddccjCommon.initEditTaskSelect("taskName",data.projectName);
                $("#taskName").val(data.taskName);
                Tool.deserialize("hddcGeogeomorphysvypointForm", data);
            }
        });
    };

    var addHddcGeogeomorphysvypoint = function () {
        var slidebar = Util.slidebar({
            url: hddcGeogeomorphysvypointFormHtml,
            width: "800px",
            cache: false,
            close: true,
            afterLoad: function () {
                initcreateProvinceForm();
                initPhotoviewingtoSelectFrom();
                initCreatedateSelectFrom();
                formValidator();
                HddccjCommon.initProjectSelect("projectName"); //添加页面项目名称下拉
                HddccjCommon.initTaskSelect("projectName", "taskName"); //添加页面任务名称根据项目名称切换
                $("#saveBtn").on("click", function () {
                    if ($("#hddcGeogeomorphysvypointForm").valid()) {
                        var data = Tool.serialize("hddcGeogeomorphysvypointForm");
                        $.ajax({
                            url: getHddcGeogeomorphysvypointController(),
                            contentType: "application/json",
                            data: JSON.stringify(data),
                            type: "post",
                            success: function (data) {
                                Util.alert(data.message);
                                slidebar.close();
                                createHddcGeogeomorphysvypointGrid();
                            }
                        });
                    }
                });
            }
        });
    };
    window._editHddcGeogeomorphysvypoint = function (hddcGeogeomorphysvypointId) {
        var slidebar = Util.slidebar({
            url: hddcGeogeomorphysvypointFormHtml,
            width: "800px",
            cache: false,
            close: true,
            afterLoad: function () {
                initPhotoviewingtoSelectFrom();
                initCreatedateSelectFrom();
                formValidator();
                HddccjCommon.initProjectSelect("projectName"); //编辑页面项目名称下拉
                HddccjCommon.initTaskSelect("projectName","taskName"); //编辑页面任务名称根据项目名称切换
                getHddcGeogeomorphysvypoint(hddcGeogeomorphysvypointId);
                $("#saveBtn").on("click", function () {
                    if ($("#hddcGeogeomorphysvypointForm").valid()) {
                        var data = Tool.serialize("hddcGeogeomorphysvypointForm");
                        $.ajax({
                            url: getHddcGeogeomorphysvypointController(),
                            contentType: "application/json",
                            data: JSON.stringify(data),
                            type: "put",
                            success: function (data) {
                                Util.alert(data.message);
                                slidebar.close();
                                createHddcGeogeomorphysvypointGrid();
                            }
                        });
                    }
                });
            }
        });
    };
    var deleteHddcGeogeomorphysvypoint = function () {
        var rows = $("#hddcGeogeomorphysvypointGrid").datagrid("getSelections");
        if (rows == null || rows.length == 0) {
            Util.alert("请选择一行数据!");
            return;
        }
        Util.confirm("是否要删除选中的数据?", function () {
            var ids = "";
            $.each(rows, function (i, row) {
                ids += row.uuid + ",";

            });
            ids = ids.substr(0, ids.length - 1);
            $.ajax({
                url: getHddcGeogeomorphysvypointController(),
                data: {
                    ids: ids
                },
                type: "delete",
                success: function (data) {
                    createHddcGeogeomorphysvypointGrid();
                }
            });
        }, function () {
            return;
        });

    };

    return {
        init: init
    };
});
