define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcA6WaveformTable/hddcA6WaveformtableSupport"],function(hddcA6WaveformtableSupport){
            hddcA6WaveformtableSupport.init();
        });
    };
});