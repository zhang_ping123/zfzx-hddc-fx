define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcStratigraphy25Pre/hddcStratigraphy25preSupport"],function(hddcStratigraphy25preSupport){
            hddcStratigraphy25preSupport.init();
        });
    };
});