define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGeochemicalAbnPoint/hddcGeochemicalabnpointSupport"],function(hddcGeochemicalabnpointSupport){
            hddcGeochemicalabnpointSupport.init();
        });
    };
});