define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGeomorphySvySamplePoint/hddcGeomorphysvysamplepointSupport"],function(hddcGeomorphysvysamplepointSupport){
            hddcGeomorphysvysamplepointSupport.init();
        });
    };
});