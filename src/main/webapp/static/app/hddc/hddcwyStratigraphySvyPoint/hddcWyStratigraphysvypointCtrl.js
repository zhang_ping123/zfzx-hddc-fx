define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcwyStratigraphySvyPoint/hddcWyStratigraphysvypointSupport"],function(hddcWyStratigraphysvypointSupport){
            hddcWyStratigraphysvypointSupport.init();
        });
    };
});