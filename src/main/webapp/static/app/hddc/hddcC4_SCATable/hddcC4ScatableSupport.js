define([
	"UtilDir/util",
	"UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
    "static/app/hddc/hddccjcommon/hddccjCommon",
	"Date","DateCN", "css!DateCss",
	"EasyUI","EasyUI-lang"
    ],function(Util, Tool, SelectOrg, SearchBlock,HddccjCommon){

    var sysPath =  getServer() + "/static/app/hddc/hddcC4_SCATable";

    var hddcC4ScatableFormHtml = sysPath + "/views/hddcC4ScatableForm.html";
	var getHddcC4ScatableController = function(){
		return getServer() + "/hddcC4Scatables";
	};

	/**
	 * 页面初始化
	 */
	var init = function(){
        initSearchBlock();
        queryBtnBind();
		createHddcC4ScatableGrid();
	};

		var initQualityinspectionDateTimeDate = function() {
			$("#qualityinspectionDate").datetimepicker({
				//设置使用语言：cn是自定义的中文版本，还可以扩展其他语言版本
				language : "cn",
				//输出格式化
				format : 'yyyy-mm-dd',
				//直接选择‘今天’
				todayBtn : true,
				//设置最精确的时间选择视图
				minView : 'month',
				//高亮当天日期
				todayHighlight : true,
				//显示“上午”“下午”
				showMeridian : true,
				//选择完毕后自动关闭
				autoclose : true
			});
		};

	var initSearchBlock = function(){
        SearchBlock.init("searchBlock");
	};
	var queryBtnBind = function(){
        $("#btnSearch").click(function () {
            createHddcC4ScatableGrid();
        });
        $("#btnReset").click(function () {
			$("#siteclassgraphArwidCondition").val("");
				$("#createUserIdCondition").val("");
				$("#projectIdCondition").val("");
				$("#qualityinspectionStatusCondition").val("");
				$("#taskIdCondition").val("");
			});
	};
	var createHddcC4ScatableGrid= function() {
        $("#hddcC4ScatableGrid").datagrid({
            url:getHddcC4ScatableController() + "/queryHddcC4Scatables",
			method:"GET",
            fitColumns: true,
            autoRowHeight: false,
            columns:[[
				{
					field:'siteclassgraphArwid',
					title:'典型场地分类图原始编号',
					width:'20%',
					align:'center',
				},
				{
					field:'objectCode',
					title:'编号',
					width:'20%',
					align:'center',
				},
				{
					field:'qualityinspectionStatus',
					title:'质检状态',
					width:'20%',
					align:'center',
				},
				{
					field:'updateUser',
					title:'修改人',
					width:'20%',
					align:'center',
				},
				{
					field:'taskId',
					title:'任务ID',
					width:'20%',
					align:'center',
				},
            ]],
            toolbar: [{
                iconCls: 'fa fa-plus-circle',
                text:"添加",
                handler: function(){
                    addHddcC4Scatable();
                }
            },{
                iconCls: 'fa fa-trash-o',
                text:"删除",
                handler: function(){
                    deleteHddcC4Scatable();
                }
            }],
            queryParams:{
				siteclassgraphArwid: $("#siteclassgraphArwidCondition").val(),
				createUserId: $("#createUserIdCondition").val(),
				projectId: $("#projectIdCondition").val(),
				qualityinspectionStatus: $("#qualityinspectionStatusCondition").val(),
				taskId: $("#taskIdCondition").val(),
            },
            pagination: true,
            pageSize: 10
        });
    };
	var formValidator = function(){
		$("#hddcC4ScatableForm").validate({
			rules : {
				createUserId : {
					required : true,
				},
				remark : {
					required : true,
				},
				qualityinspectionUser : {
					required : true,
				},
				objectCode : {
					required : true,
				},
				siteclassdataArwid : {
					required : true,
				},
				projectId : {
					required : true,
				},
				qualityinspectionComments : {
					required : true,
				},
				qualityinspectionDate : {
					required : true,
				},
			},
			messages : {
				createUserId : {
					required : "创建人ID不允许为空!",
				},
				remark : {
					required : "备注不允许为空!",
				},
				qualityinspectionUser : {
					required : "质检人不允许为空!",
				},
				objectCode : {
					required : "编号不允许为空!",
				},
				siteclassdataArwid : {
					required : "典型呈递分类数据文件原始编号不允许为空!",
				},
				projectId : {
					required : "项目ID不允许为空!",
				},
				qualityinspectionComments : {
					required : "质检原因不允许为空!",
				},
				qualityinspectionDate : {
					required : "质检时间不允许为空!",
				},
			}
		});
	};
	var getHddcC4Scatable = function(id){
		$.ajax({
			url: getHddcC4ScatableController() + "/"+id,
			type: "get",
			success: function (data) {
				Tool.deserialize("hddcC4ScatableForm", data);
			}
		});
	};

	var addHddcC4Scatable = function () {
		var slidebar = Util.slidebar({
			url: hddcC4ScatableFormHtml,
			width: "580px",
			cache: false,
			close : true,
			afterLoad: function () {
				initQualityinspectionDateTimeDate();
				formValidator();
				$("#saveBtn").on("click", function () {
					if($("#hddcC4ScatableForm").valid()){
						var data = Tool.serialize("hddcC4ScatableForm");
						$.ajax({
							url: getHddcC4ScatableController() ,
                            contentType:"application/json",
							data: JSON.stringify(data),
							type: "post",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
                                createHddcC4ScatableGrid();
							}
						});
					}
				});
			}
		});
	};
	window._editHddcC4Scatable = function(hddcC4ScatableId) {
		var slidebar = Util.slidebar({
			url: hddcC4ScatableFormHtml,
			width: "580px",
			cache: false,
			close : true,
			afterLoad: function () {
				initQualityinspectionDateTimeDate();
				formValidator();
				getHddcC4Scatable(hddcC4ScatableId);
				$("#saveBtn").on("click", function () {
					if($("#hddcC4ScatableForm").valid()){
						var data = Tool.serialize("hddcC4ScatableForm");
						$.ajax({
							url: getHddcC4ScatableController(),
                            contentType:"application/json",
                            data: JSON.stringify(data),
							type: "put",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
								createHddcC4ScatableGrid();
							}
						});
					}
				});
			}
		});
	};
	var deleteHddcC4Scatable = function() {
		var rows = $("#hddcC4ScatableGrid").datagrid("getSelections");
		if (rows == null || rows.length == 0) {
			Util.alert("请选择一行数据!");
			return;
		}
		Util.confirm("是否要删除选中的数据?", function() {
			var ids = "";
			$.each(rows, function(i, row){
				ids += row.targetregionid + ",";

			});
			ids = ids.substr(0,ids.length - 1);
			$.ajax({
				url: getHddcC4ScatableController() ,
				data: {
					ids : ids
				},
				type: "delete",
				success: function (data) {
					createHddcC4ScatableGrid();
				}
			});
		}, function() {
			return;
		});

	};

	return {
		init:init
	};
});
