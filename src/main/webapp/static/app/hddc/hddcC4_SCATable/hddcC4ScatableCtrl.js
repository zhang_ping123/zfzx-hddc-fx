define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcC4_SCATable/hddcC4ScatableSupport"],function(hddcC4ScatableSupport){
            hddcC4ScatableSupport.init();
        });
    };
});