define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGeomorphySvyPoint/hddcGeomorphysvypointSupport"],function(hddcGeomorphysvypointSupport){
            hddcGeomorphysvypointSupport.init();
        });
    };
});