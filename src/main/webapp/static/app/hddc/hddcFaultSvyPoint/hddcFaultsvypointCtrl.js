define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcFaultSvyPoint/hddcFaultsvypointSupport"],function(hddcFaultsvypointSupport){
            hddcFaultsvypointSupport.init();
        });
    };
});