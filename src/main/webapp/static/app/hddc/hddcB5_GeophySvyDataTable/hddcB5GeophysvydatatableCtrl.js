define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcB5_GeophySvyDataTable/hddcB5GeophysvydatatableSupport"],function(hddcB5GeophysvydatatableSupport){
            hddcB5GeophysvydatatableSupport.init();
        });
    };
});