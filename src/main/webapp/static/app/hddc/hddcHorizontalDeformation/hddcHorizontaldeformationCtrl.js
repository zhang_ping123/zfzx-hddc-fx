define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcHorizontalDeformation/hddcHorizontaldeformationSupport"],function(hddcHorizontaldeformationSupport){
            hddcHorizontaldeformationSupport.init();
        });
    };
});