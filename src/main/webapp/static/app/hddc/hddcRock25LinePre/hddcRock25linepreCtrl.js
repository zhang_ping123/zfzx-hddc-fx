define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcRock25LinePre/hddcRock25linepreSupport"],function(hddcRock25linepreSupport){
            hddcRock25linepreSupport.init();
        });
    };
});