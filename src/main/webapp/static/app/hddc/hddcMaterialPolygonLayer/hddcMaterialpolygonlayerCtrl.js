define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcMaterialPolygonLayer/hddcMaterialpolygonlayerSupport"],function(hddcMaterialpolygonlayerSupport){
            hddcMaterialpolygonlayerSupport.init();
        });
    };
});