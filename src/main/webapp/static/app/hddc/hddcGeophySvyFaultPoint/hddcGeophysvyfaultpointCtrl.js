define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGeophySvyFaultPoint/hddcGeophysvyfaultpointSupport"],function(hddcGeophysvyfaultpointSupport){
            hddcGeophysvyfaultpointSupport.init();
        });
    };
});