define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcWorkRegion/hddcWorkregionSupport"],function(hddcWorkregionSupport){
            hddcWorkregionSupport.init();
        });
    };
});