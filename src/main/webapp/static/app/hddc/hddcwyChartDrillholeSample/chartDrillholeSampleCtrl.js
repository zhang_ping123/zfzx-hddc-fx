define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcwyChartDrillholeSample/chartDrillholeSampleSupport"],function(chartDrillholeSampleSupport){
            chartDrillholeSampleSupport.init();
        });
    };
});