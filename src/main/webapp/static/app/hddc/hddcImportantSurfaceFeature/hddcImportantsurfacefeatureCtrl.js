define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcImportantSurfaceFeature/hddcImportantsurfacefeatureSupport"],function(hddcImportantsurfacefeatureSupport){
            hddcImportantsurfacefeatureSupport.init();
        });
    };
});