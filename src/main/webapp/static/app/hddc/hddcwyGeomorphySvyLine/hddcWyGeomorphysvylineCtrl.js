define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcwyGeomorphySvyLine/hddcWyGeomorphysvylineSupport"],function(hddcWyGeomorphysvylineSupport){
            hddcWyGeomorphysvylineSupport.init();
        });
    };
});