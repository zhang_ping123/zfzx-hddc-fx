define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcVerticalDeformation/hddcVerticaldeformationSupport"],function(hddcVerticaldeformationSupport){
            hddcVerticaldeformationSupport.init();
        });
    };
});