define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGeologicalSvyLine/hddcGeologicalsvylineSupport"],function(hddcGeologicalsvylineSupport){
            hddcGeologicalsvylineSupport.init();
        });
    };
});