define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcRelocationISCatalog/hddcRelocationiscatalogSupport"],function(hddcRelocationiscatalogSupport){
            hddcRelocationiscatalogSupport.init();
        });
    };
});