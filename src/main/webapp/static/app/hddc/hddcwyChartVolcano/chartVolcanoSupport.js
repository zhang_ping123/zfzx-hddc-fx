define([
    "UtilDir/util",
    "UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
    "static/app/hddc/hddccjcommon/hddccjCommon",
    "UtilDir/dialog",
    "Echarts", "Date", "DateCN", "css!DateCss",
    "EasyUI", "EasyUI-lang"
], function (Util, Tool, SelectOrg, SearchBlock, HddccjCommon, Dialog, Echarts) {

    /**
     * 页面初始化
     */
    var init = function () {
        queryBtnBind();
        HddccjCommon.initProjectSelect("projectName");
        HddccjCommon.initTaskSelect("projectName", "taskName");
        initDatetimePicker();
        createVolcanoBarChart();
        downloadDialog();
    };

    var tables = [];

    var getHddcHsAppCountController = function () {
        return getServer() + "/hddcHsAppCount";
    };

    var createVolcanoBarChart = function () {
        var myChart = Echarts.init(document.getElementById('barChart'));
        // 查询条件
        var startTime = $("#date1").val();
        var endTime = $("#date2").val();
        var projectName = $("#projectName").val();
        var taskName = $("#taskName").val();
        $.ajax({
            url: getHddcHsAppCountController() + "/findAppCount",
            type: "get",
            data: {
                startTime: startTime,
                endTime: endTime,
                projectName: projectName,
                taskName: taskName
            },
            success: function (data) {
                var option = {
                    tooltip: {},
                    xAxis: {
                        name: '外业采集项',
                        data: data.data.x
                    },
                    grid: {
                        left: '15%',
                        right: '15%',
                        // top: '20%',
                        // bottom: '10%'
                    },
                    yAxis: {name: '工作量'},
                    series: [{
                        name: '销量',
                        type: 'bar',
                        data: data.data.y,
                        // data: [12,23,55,29,9,3],
                        barWidth: 30,
                        itemStyle: {
                            normal: {
                                //每个柱子的颜色即为colors数组里的每一项，如果柱子数目多于colors的长度，则柱子颜色循环使用该数组
                                color: function (params) {
                                    var colors = ['#00CCCC', '#CCCC00', '#FF9900', '#66CC66', '#0099FF', '#CC9999'];
                                    return colors[params.dataIndex];
                                }
                            },
                        },
                    }]
                };
                // 使用刚指定的配置项和数据显示图表。
                myChart.setOption(option);

                // 上传下载下拉框
                tables = data.data.x;
                uploadDialog();
            }
        });
    }

    var initDatetimePicker = function () {
        $("#date1").datetimepicker({
            //设置使用语言：cn是自定义的中文版本，还可以扩展其他语言版本
            language: "cn",
            //输出格式化
            format: 'yyyy-mm-dd',
            //直接选择‘今天’
            todayBtn: true,
            //设置最精确的时间选择视图
            minView: 'month',
            //高亮当天日期
            todayHighlight: true,
            //选择完毕后自动关闭
            autoclose: true
        });

        $("#date2").datetimepicker({
            //设置使用语言：cn是自定义的中文版本，还可以扩展其他语言版本
            language: "cn",
            //输出格式化
            format: 'yyyy-mm-dd',
            //直接选择‘今天’
            todayBtn: true,
            //设置最精确的时间选择视图
            minView: 'month',
            //高亮当天日期
            todayHighlight: true,
            //选择完毕后自动关闭
            autoclose: true
        });
    }

    var queryBtnBind = function () {
        $("#btnSearch").click(function () {
            createVolcanoBarChart();
        });
        $("#btnReset").click(function () {
            $("#date1").val("");
            $("#date2").val("");
            $("#projectName").val("");
            $("#taskName").val("");
        });
    }

    var getMapping = function (tableName) {
        var mapping = '';
        switch (tableName) {
            case "火山口-点": mapping = "hddcWyCraters";break;
            case "熔岩流-面": mapping = "hddcWyLavas";break;
            case "火山采样点-点": mapping = "hddcWyVolcanicsamplepoints";break;
            case "火山调查观测点-点": mapping = "hddcWyVolcanicsvypoints";break;
        }
        return mapping;
    }

    var downloadDialog = function () {
        $("#downLoad").click(function () {
            //为弹出框增加操作按钮
            var buttons = [];
            buttons.push({
                name: "下载",
                callback: function () {
                    var tableName = $("#downloadTable").val();
                    if(tableName == null){
                        Util.alert("下载前请先选择采集项!");
                        return;
                    }
                    // 查询条件
                    var startTime = $("#date1").val();
                    var endTime = $("#date2").val();
                    var projectName = $("#projectName").val();
                    var taskName = $("#taskName").val();
                    // 根据不同表 调接口
                    var downloadTable = $("#downloadTable").val();
                    var mapping = getMapping(downloadTable);
                    window.location.href = getServer() + "/hddc/"+ mapping +"/exportFile?startTime="+startTime+"&endTime="+endTime+"&projectName="+projectName+"&taskName="+taskName;
                    dialog.hide();
                }
            });
            var dialog = Dialog({
                id: "BaseDialog",
                title: "下载外业数据",
                width: "500px",
                height: "70px",
                dialogSize: "",               //modal-lg或modal-sm
                body: '<div class="form-group">\n' +
                    '                        <label for="taskName" style="left: 15%;top: 8px;" class="col-md-4 control-label"></span>请选择:</label>\n' +
                    '                        <div class="col-md-8">\n' +
                    '                            <select class="form-control" id="downloadTable">\n' +
                    '                            </select>\n' +
                    '                        </div>\n' +
                    '                    </div>',
                buttons: buttons
            });
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $.each(tables, function (idx, item) {
                html += "<option value=" + item + ">" + item + "</option>";
            });
            $("#downloadTable").append(html);
            //可以通过返回的dialog对象调用相关方法
            //dialog.setBody("修改内容");
            //dialog.show();
        });
    }

    // 上传
    var uploadDialog = function () {
        $("#upLoad").on("click", function () {
            $("#uploadModal").modal();
            //初始化下拉select
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $.each(tables, function (idx, item) {
                html += "<option value=" + item + ">" + item + "</option>";
            });
            $("#uploadSelect option").remove();
            $("#uploadSelect").append(html);

            // 下载数据模板
            $("#downloadZwExcelTemplate").on("click", function () {
                var tableName = $("#uploadSelect").val();
                if(tableName == null){
                    Util.alert("下载数据模板前请先选择采集项!");
                    return;
                }else{
                    var url = getServer() + "/excel/"+ tableName +".xls";
                    $("#downloadZwExcelTemplate").attr("href", url);
                }

            })

            $('#uploadButton').off("click");
            $("#uploadButton").on("click", function () {
                var uploadinput = document.getElementById("uploadFile");
                var uploadTable = document.getElementById("uploadSelect");
                if (uploadTable.value == "") {
                    Util.alert("上传前请先选择采集项!");
                    return;
                }else if(uploadinput.value == ""){
                    Util.alert("上传前请先选择文件!");
                    return;
                }
                var formData = new FormData();
                formData.append("file", uploadinput.files[0]);

                // 根据不同表 调接口
                var downloadTable = $("#uploadSelect").val();
                var mapping = getMapping(downloadTable);
                $.ajax({
                    url: getServer() + "/hddc/"+ mapping +"/importDisaster",
                    data: formData,
                    processData: false, //因为data值是FormData对象，不需要对数据做处理。
                    contentType: false,
                    type: "POST",
                    success: function (data) {
                        $('#uploadModal').modal('hide');
                        Util.alert(data);
                        createVolcanoBarChart();
                    }
                });
            });

        })
    };

    return {
        init: init
    };
});
