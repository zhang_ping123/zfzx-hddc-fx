define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcwyChartVolcano/chartVolcanoSupport"],function(chartVolcanoSupport){
            chartVolcanoSupport.init();
        });
    };
});