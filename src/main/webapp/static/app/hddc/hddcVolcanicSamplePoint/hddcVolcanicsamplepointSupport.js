define([
	"UtilDir/util",
	"UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
    "static/app/hddc/hddccjcommon/hddccjCommon",
	"Date","DateCN", "css!DateCss",
	"EasyUI","EasyUI-lang"
    ],function(Util, Tool, SelectOrg, SearchBlock,HddccjCommon){

    var sysPath =  getServer() + "/static/app/hddc/hddcVolcanicSamplePoint";

    var hddcVolcanicsamplepointFormHtml = sysPath + "/views/hddcVolcanicsamplepointForm.html";
	var getHddcVolcanicsamplepointController = function(){
		return getServer() + "/hddc/hddcVolcanicsamplepoints";
	};

	/**
	 * 页面初始化
	 */
	var init = function(){
        initSearchBlock();
        queryBtnBind();
		/*initProvinceSelect();
		initCitySelect();
		initAreaSelect();*/
		initcreateProvince();
        HddccjCommon.initProjectSelect("projectNameCondition"); //初始化查询的项目名称
		createHddcVolcanicsamplepointGrid();
	};
	var initcreateProvince = function () {
		var html = "";
		$("#citySelect").append(html);
		$("#areaSelect").append(html);
		$.ajax({
			url: getServer() + "/divisions/root/subdivisions",
			contentType: "application/json",
			type: "get",
			success: function (data) {
				$.each(data, function (idx, item) {
					html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
				});
				$("#provinceSelect").append(html);
			}
		});
		$("#provinceSelect").change(function () {
			var html = '<option value="" disabled selected style="display:none;">请选择</option>';
			$("#citySelect option").remove();
			$("#citySelect").append(html);
			$("#areaSelect option").remove();
			$("#areaSelect").append(html);
			var divisionId = $(this).find("option:selected").attr("exid");
			if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
				$("#citySelect").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
				return;
			}
			$.ajax({
				url: getServer() + "/divisions/" + divisionId + "/subdivisions",
				contentType: "application/json",
				type: "get",
				success: function (data) {
					$.each(data, function (idx, item) {
						html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
					});
					$("#citySelect").append(html);
				}
			});
		});
		$("#citySelect").change(function () {
			if ($(this).val() == "") return;
			var html = '<option value="" disabled selected style="display:none;">请选择</option>';
			$("#areaSelect option").remove();
			$("#areaSelect").append(html);
			var divisionId = $(this).find("option:selected").attr("exid");
			$.ajax({
				url: getServer() + "/divisions/" + divisionId + "/subdivisions",
				contentType: "application/json",
				type: "get",
				success: function (data) {
					$.each(data, function (idx, item) {
						html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
					});
					$("#areaSelect").append(html);
				}
			});
		});
	}
	var initcreateProvinceForm = function () {
		var html = "";
		$("#city").append(html);
		$("#area").append(html);
		$.ajax({
			url: getServer() + "/divisions/root/subdivisions",
			contentType: "application/json",
			type: "get",
			success: function (data) {
				$.each(data, function (idx, item) {
					html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
				});
				$("#province").append(html);
			}
		});
		$("#province").change(function () {
			var html = '<option value="" disabled selected style="display:none;">请选择</option>';
			$("#city option").remove();
			$("#city").append(html);
			$("#area option").remove();
			$("#area").append(html);
			var divisionId = $(this).find("option:selected").attr("exid");
			if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
				$("#city").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
				return;
			}
			$.ajax({
				url: getServer() + "/divisions/" + divisionId + "/subdivisions",
				contentType: "application/json",
				type: "get",
				success: function (data) {
					$.each(data, function (idx, item) {
						html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
					});
					$("#city").append(html);
				}
			});
		});
		$("#city").change(function () {
			if ($(this).val() == "") return;
			var html = '<option value="" disabled selected style="display:none;">请选择</option>';
			$("#area option").remove();
			$("#area").append(html);
			var divisionId = $(this).find("option:selected").attr("exid");
			$.ajax({
				url: getServer() + "/divisions/" + divisionId + "/subdivisions",
				contentType: "application/json",
				type: "get",
				success: function (data) {
					$.each(data, function (idx, item) {
						html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
					});
					$("#area").append(html);
				}
			});
		});
	}



	var editProvince = function (Province, City, Area) {
		debugger;
		var html = "";
		$("#city").append(html);
		$("#area").append(html);
		$.ajax({
			url: getServer() + "/divisions/root/subdivisions",
			contentType: "application/json",
			type: "get",
			success: function (data) {
				$.each(data, function (idx, item) {
					if (item.divisionName == Province) {
						debugger;
						var divisionId = item.divisionId;
						var htmlCity = '';
						if (Province == "北京市" || Province == "天津市" || Province == "上海市" || Province == "重庆市") {
							$("#city").append("<option value='" + Province + "' exid='" + divisionId + "'>" + Province + "</option>");
							$('#city').val(City);
							var htmlArea = '';
							$.ajax({
								url: getServer() + "/divisions/" + divisionId + "/subdivisions",
								contentType: "application/json",
								type: "get",
								success: function (data) {
									$.each(data, function (idx, item) {
										htmlArea += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
									});
									$("#area").append(htmlArea);
									$('#area').val(Area);
								}
							});

						} else {
							$.ajax({
								url: getServer() + "/divisions/" + divisionId + "/subdivisions",
								contentType: "application/json",
								type: "get",
								success: function (data) {
									debugger;
									$.each(data, function (idx, item) {
										if (item.divisionName == City) {
											var divisionId = item.divisionId;
											var htmlArea = '';
											$.ajax({
												url: getServer() + "/divisions/" + divisionId + "/subdivisions",
												contentType: "application/json",
												type: "get",
												success: function (data) {
													$.each(data, function (idx, item) {
														htmlArea += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
													});
													$("#area").append(htmlArea);
													$('#area').val(Area);
												}
											});
										}
										htmlCity += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
									});
									$("#city").append(htmlCity);
									$('#city').val(City);
								}
							});
						}
					}
					html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
				});
				$("#province").append(html);
				$('#province').val(Province);
			}
		});
		$("#province").change(function () {
			var html = '<option value="" disabled selected style="display:none;">请选择</option>';
			$("#city option").remove();
			$("#city").append(html);
			$("#area option").remove();
			$("#area").append(html);
			var divisionId = $(this).find("option:selected").attr("exid");
			if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
				$("#city").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
				return;
			}
			$.ajax({
				url: getServer() + "/divisions/" + divisionId + "/subdivisions",
				contentType: "application/json",
				type: "get",
				success: function (data) {
					$.each(data, function (idx, item) {
						html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
					});
					$("#city").append(html);
				}
			});
		});
		$("#city").change(function () {
			if ($(this).val() == "") return;
			var html = '<option value="" disabled selected style="display:none;">请选择</option>';
			$("#area option").remove();
			$("#area").append(html);
			var divisionId = $(this).find("option:selected").attr("exid");
			$.ajax({
				url: getServer() + "/divisions/" + divisionId + "/subdivisions",
				contentType: "application/json",
				type: "get",
				success: function (data) {
					$.each(data, function (idx, item) {
						html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
					});
					$("#area").append(html);
				}
			});
		});
	}
	var initProvinceSelect = function () {
		$.ajax({
			url: getHddcVolcanicsamplepointController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("provinceSelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initCitySelect = function () {
		$.ajax({
			url: getHddcVolcanicsamplepointController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("citySelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initAreaSelect = function () {
		$.ajax({
			url: getHddcVolcanicsamplepointController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("areaSelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};

		var initProvinceSelectFrom = function () {
		$.ajax({
			url: getHddcVolcanicsamplepointController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("province");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initCitySelectFrom = function () {
		$.ajax({
			url: getHddcVolcanicsamplepointController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("city");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initAreaSelectFrom = function () {
		$.ajax({
			url: getHddcVolcanicsamplepointController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("area");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initSymbolinfoSelectFrom = function () {
		$.ajax({
			url: getHddcVolcanicsamplepointController() + "/getValidDictItemsByDictCode/" + "SampleTestedMethodCVD",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("symbolinfo");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initIssinglesampleSelectFrom = function () {
		$.ajax({
			url: getHddcVolcanicsamplepointController() + "/getValidDictItemsByDictCode/" + "truefalse",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("issinglesample");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};

	var initSearchBlock = function(){
        SearchBlock.init("searchBlock");
	};
	var queryBtnBind = function(){
        $("#btnSearch").click(function () {
            createHddcVolcanicsamplepointGrid();
        });
        $("#btnReset").click(function () {
            $("#provinceSelect").val("");
	            $("#citySelect").val("");
	            $("#areaSelect").val("");
				$("#projectNameCondition").val("");
				$("#labelinfoCondition").val("");
			});
	};
    //导入
    var importForm = function () {

        $("#uploadModal").modal();
        $("#uploadModal").on("shown.bs.modal", function () {
            var url = getServer() + "/excel/火山采样点-点.xls";
            $("#downloadZwExcelTemplate").attr("href", url);
        });

        $('#uploadButton').off("click");
        $("#uploadButton").on("click", function () {
            var uploadinput = document.getElementById("uploadFile");
            if (uploadinput.value == "") {
                Util.alert("上传前请先选择文件!");
                return;
            }
            var formData = new FormData();
            formData.append("file", uploadinput.files[0]);
            $.ajax({
                url: getHddcVolcanicsamplepointController() + "/importDisaster",
                data: formData,
                processData: false, //因为data值是FormData对象，不需要对数据做处理。
                contentType: false,
                type: "POST",
                success: function (data) {
                    $('#uploadModal').modal('hide');
                    uploadinput.value = null;
                    Util.alert(data);
                    createHddcVolcanicsamplepointGrid();
                }
            });
        });
    };
    //导出
    var exportForm = function () {
        var province = $("#provinceSelect").val();
        var city = $("#citySelect").val();
        var area = $("#areaSelect").val();
        var projectName = $("#projectNameCondition").val();
        var labelinfo= $("#labelinfoCondition").val();
        window.location.href = getHddcVolcanicsamplepointController() + "/exportFile?province="+province+"&city="+city+"&area="+area+"&projectName="+projectName+"&labelinfo="+labelinfo;
    };
	var createHddcVolcanicsamplepointGrid= function() {
        $("#hddcVolcanicsamplepointGrid").datagrid({
            url:getHddcVolcanicsamplepointController() + "/queryHddcVolcanicsamplepoints",
			method:"GET",
            fitColumns: true,
            autoRowHeight: false,
            columns:[[
                {field:"ck",checkbox:true},
				{
					field:'id',
					title:'采样点编号',
					width:'20%',
					align:'center',
					formatter:function(value,rowData,rowIndex){
						return '<a href="#" onclick="_editHddcVolcanicsamplepoint(\'' + rowData.uuid + '\');"> '+rowData.id+' </a> '
					}
				},
				{
					field:'province',
					title:'省',
					width:'20%',
					align:'center',
				},
				{
					field:'city',
					title:'市',
					width:'20%',
					align:'center',
				},
				{
					field:'area',
					title:'区（县）',
					width:'20%',
					align:'center',
				},
				{
					field:'projectName',
					title:'项目名称',
					width:'20%',
					align:'center',
				},
				{
					field:'taskName',
					title:'任务名称',
					width:'20%',
					align:'center',
				},
				{
					field:'volcanicsvypointid',
					title:'火山调查观测点编号',
					width:'20%',
					align:'center',
				},
            ]],
            toolbar: [{
                iconCls: 'fa fa-plus-circle',
                text:"添加",
                handler: function(){
                    addHddcVolcanicsamplepoint();
                }
            },{
                iconCls: 'fa fa-trash-o',
                text:"删除",
                handler: function(){
                    deleteHddcVolcanicsamplepoint();
                }
            }, {
                iconCls: 'fa fa-upload',
                text: "导入",
                handler: function () {
                    importForm();
                }
            }, {
                iconCls: 'fa fa-download',
                text: "导出",
                handler: function () {
                    exportForm();
                }
            }],
            queryParams:{
                province: $("#provinceSelect").val(),
                city: $("#citySelect").val(),
                area: $("#areaSelect").val(),
				projectName: $("#projectNameCondition").val(),
				labelinfo: $("#labelinfoCondition").val(),
            },
            pagination: true,
            pageSize: 10
        });
    };
	var formValidator = function(){
		$("#hddcVolcanicsamplepointForm").validate({
			rules : {
				province : {
					required : true,
				},
				city : {
					required : true,
				},
				area : {
					required : true,
				},
				projectName : {
					required : true,
				},
				taskName : {
					required : true,
				},
				id : {
					required : true,
				},
			},
			messages : {
				province : {
					required : "省不允许为空!",
				},
				city : {
					required : "市不允许为空!",
				},
				area : {
					required : "区（县）不允许为空!",
				},
				projectName : {
					required : "项目名称不允许为空!",
				},
				taskName : {
					required : "任务名称不允许为空!",
				},
				id : {
					required : "采样点编号不允许为空!",
				},
			}
		});
	};
	var getHddcVolcanicsamplepoint = function(id){
		$.ajax({
			url: getHddcVolcanicsamplepointController() + "/"+id,
			type: "get",
			success: function (data) {
				editProvince(data.province, data.city, data.area);
                // 回显项目名称
                $("#projectName").val(data.projectName);
                // 回显任务名称
                HddccjCommon.initEditTaskSelect("taskName",data.projectName);
                $("#taskName").val(data.taskName);
				Tool.deserialize("hddcVolcanicsamplepointForm", data);
			}
		});
	};

	var addHddcVolcanicsamplepoint = function () {
		var slidebar = Util.slidebar({
			url: hddcVolcanicsamplepointFormHtml,
			width: "800px",
			cache: false,
			close : true,
			afterLoad: function () {
				/*initProvinceSelectFrom();
				initCitySelectFrom();
				initAreaSelectFrom();*/
				initcreateProvinceForm();
				initSymbolinfoSelectFrom();
				initIssinglesampleSelectFrom();
				formValidator();
                HddccjCommon.initProjectSelect("projectName"); //添加页面项目名称下拉
                HddccjCommon.initTaskSelect("projectName", "taskName"); //添加页面任务名称根据项目名称切换
				$("#saveBtn").on("click", function () {
					if($("#hddcVolcanicsamplepointForm").valid()){
						var data = Tool.serialize("hddcVolcanicsamplepointForm");
						$.ajax({
							url: getHddcVolcanicsamplepointController() ,
                            contentType:"application/json",
							data: JSON.stringify(data),
							type: "post",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
                                createHddcVolcanicsamplepointGrid();
							}
						});
					}
				});
			}
		});
	};
	window._editHddcVolcanicsamplepoint = function(hddcVolcanicsamplepointId) {
		var slidebar = Util.slidebar({
			url: hddcVolcanicsamplepointFormHtml,
			width: "800px",
			cache: false,
			close : true,
			afterLoad: function () {
				/*initProvinceSelectFrom();
				initCitySelectFrom();
				initAreaSelectFrom();*/
				initSymbolinfoSelectFrom();
				initIssinglesampleSelectFrom();
				formValidator();
                HddccjCommon.initProjectSelect("projectName"); //编辑页面项目名称下拉
                HddccjCommon.initTaskSelect("projectName","taskName"); //编辑页面任务名称根据项目名称切换
				getHddcVolcanicsamplepoint(hddcVolcanicsamplepointId);
				$("#saveBtn").on("click", function () {
					if($("#hddcVolcanicsamplepointForm").valid()){
						var data = Tool.serialize("hddcVolcanicsamplepointForm");
						$.ajax({
							url: getHddcVolcanicsamplepointController(),
                            contentType:"application/json",
                            data: JSON.stringify(data),
							type: "put",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
								createHddcVolcanicsamplepointGrid();
							}
						});
					}
				});
			}
		});
	};
	var deleteHddcVolcanicsamplepoint = function() {
		var rows = $("#hddcVolcanicsamplepointGrid").datagrid("getSelections");
		if (rows == null || rows.length == 0) {
			Util.alert("请选择一行数据!");
			return;
		}
		Util.confirm("是否要删除选中的数据?", function() {
			var ids = "";
			$.each(rows, function(i, row){
				ids += row.uuid + ",";

			});
			ids = ids.substr(0,ids.length - 1);
			$.ajax({
				url: getHddcVolcanicsamplepointController() ,
				data: {
					ids : ids
				},
				type: "delete",
				success: function (data) {
					createHddcVolcanicsamplepointGrid();
				}
			});
		}, function() {
			return;
		});

	};

	return {
		init:init
	};
});
