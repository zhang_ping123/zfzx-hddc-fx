define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcVolcanicSamplePoint/hddcVolcanicsamplepointSupport"],function(hddcVolcanicsamplepointSupport){
            hddcVolcanicsamplepointSupport.init();
        });
    };
});