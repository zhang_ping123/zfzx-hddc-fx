define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGeologicalSvyPoint/hddcGeologicalsvypointSupport"],function(hddcGeologicalsvypointSupport){
            hddcGeologicalsvypointSupport.init();
        });
    };
});