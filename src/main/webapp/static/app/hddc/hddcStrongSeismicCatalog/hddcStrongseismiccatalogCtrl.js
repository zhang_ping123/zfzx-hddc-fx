define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcStrongSeismicCatalog/hddcStrongseismiccatalogSupport"],function(hddcStrongseismiccatalogSupport){
            hddcStrongseismiccatalogSupport.init();
        });
    };
});