define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcwyGeologicalSvyPlanningLine/hddcWyGeologicalsvyplanninglineSupport"],function(hddcWyGeologicalsvyplanninglineSupport){
            hddcWyGeologicalsvyplanninglineSupport.init();
        });
    };
});