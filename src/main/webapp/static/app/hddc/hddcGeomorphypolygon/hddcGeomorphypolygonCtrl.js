define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGeomorphypolygon/hddcGeomorphypolygonSupport"],function(hddcGeomorphypolygonSupport){
            hddcGeomorphypolygonSupport.init();
        });
    };
});