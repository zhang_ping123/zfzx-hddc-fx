define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcwyGeomorStation/hddcWyGeomorstationSupport"],function(hddcWyGeomorstationSupport){
            hddcWyGeomorstationSupport.init();
        });
    };
});