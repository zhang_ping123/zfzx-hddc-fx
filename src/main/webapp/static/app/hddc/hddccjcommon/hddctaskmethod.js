define([
    "UtilDir/util",
    "UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
    "Date","DateCN", "css!DateCss",
    "EasyUI","EasyUI-lang"
],function(Util, Tool, SelectOrg, SearchBlock){
var getHddcCjTaskController = function(){
    return getServer() + "/hddc/hddcCjTasks";
};

function getAllTaskByProjectId(pid) {
    var taskdata=[];
    $.ajax({
        url:getHddcCjTaskController() + "/findHddcCjTaskEntityByProjectIds/"+pid,
        type: "get",
        async:false,
        success: function (data) {
            debugger;

            for(var i=0;i<data.length;i++){
                if(data[i].belongProjectID==pid){
                    taskdata.push(data[i]);
                }
            }

        }
    });
    return taskdata;
}

    return {
        getAllTaskByProjectId:getAllTaskByProjectId
    };
});