define([
    "UtilDir/util",
    "UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
    "Date","DateCN", "css!DateCss",
    "EasyUI","EasyUI-lang"
],function(Util, Tool, SelectOrg, SearchBlock){
    var getHddcCjProjectController = function(){
        return getServer() + "/hddc/hddcCjProjects";
    };
    var getHddcCjTaskController = function(){
        return getServer() + "/hddc/hddcCjTasks";
    };


    // 返回所有项目名称
    var projectData=null;
    function getAllProject(){
        $.ajax({
            url:getHddcCjProjectController() + "/queryAllHddcCjProjects",
            type: "get",
            async:false,
            success: function (data) {
                projectData =  data;
            }
        });
        return projectData;
    }

    // 初始化项目名称下拉框
    function initProjectSelect(selectId) {
        var projects = getAllProject();
        var html = '<option value="" disabled selected style="display:none;">请选择</option>';
        $.each(projects, function (idx, item) {
            html += "<option value='" + item.projectId + "'>" + item.projectName + "</option>";
        });
        $("#"+selectId+" option").remove();
        $("#"+selectId).append(html);
    }

    // 根据项目名称找项目id
    function getProjectIdByProjectName(pname) {
        var projectId=null;
        if(projectData!=null) {
            for (var i = 0; i < projectData.length; i++) {
                if(projectData[i].projectName==pname){
                    return projectData[i].projectId;
                }
            }
        }
        return projectId;
    }

    // 根据项目名称返回这个项目的所有任务
    function getAllTaskByProjectName(pname) {
        var projectId=getProjectIdByProjectName(pname);
        var taskdata=[];
        if(projectId!=null) {
            $.ajax({
                url: getHddcCjTaskController() + "/findHddcCjTaskEntityByProjectIds/"+projectId,
                type: "get",
                async: false,
                success: function (data) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].belongProject == pname) {
                            taskdata.push(data[i]);
                        }
                    }

                }
            });
        }
        return taskdata;
    }

    // 根据项目id返回这个项目的所有任务
    function getAllTaskByProjectId(pid) {
        var taskdata=[];
        if(pid!=null) {
            $.ajax({
                url: getHddcCjTaskController() + "/findHddcCjTaskEntityByProjectIds/"+pid,
                type: "get",
                async: false,
                success: function (data) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].belongProjectID == pid) {
                            taskdata.push(data[i]);
                        }
                    }

                }
            });
        }
        return taskdata;
    }

    // 返回所有任务
    function getAllTask() {
        var taskdata=[];
        $.ajax({
            url:getHddcCjTaskController() + "/queryAllHddcCjTasks",//queryAllHddcCjProjects
            type: "get",
            async:false,
            success: function (data) {
                taskdata = data;
            }
        });
        return taskdata;
    }

    // 添加初始化任务名称下拉框
    var initTaskSelect=function (projectSelectId, taskSelectId) {
        $("#"+projectSelectId).change(function () {
            var html = '<option value="" disabled selected style=\'display:none;\'>请选择</option>';
            var projectId = $("#"+projectSelectId).val();
            if(projectId!=null) {
                var taskdatas = getAllTaskByProjectId(projectId);
                debugger;
                $.each(taskdatas, function (idx, item) {
                    html += "<option value='" + item.taskId + "'>" + item.taskName + "</option>";
                });
                $("#"+taskSelectId+" option").remove();
                $("#"+taskSelectId).append(html);
            }
        });
    }


    // 编辑任务名称下拉
    var editDisplayTaskName = function(selectId,pid){
        var taskdata=[];
        var html = '';
        $.ajax({
            url:getHddcCjTaskController() + "/queryAllHddcCjTasks",//queryAllHddcCjProjects
            type: "get",
            async:false,
            success: function (data) {
                for(var i=0;i<data.length;i++){
                    if(data[i].belongProjectID==pid){
                        taskdata.push(data[i]);
                    }
                }
                $.each(taskdata, function (idx, item) {
                    html += "<option value='" + item.taskId + "'>" + item.taskName + "</option>";
                });
                $("#"+selectId+" option").remove();
                $("#"+selectId).append(html);
            }
        });
    }

    var initEditTaskSelect = function (selectId,projectName) {
        var allTasks = getAllTask();
        var html='';
        var taskSelete = [];
        for(var i=0;i<allTasks.length;i++){
            if(allTasks[i].belongProjectID==projectName){
                taskSelete.push(allTasks[i]);
            }
        }
        $.each(taskSelete, function (idx, item) {
            html += "<option value='" + item.taskId + "'>" + item.taskName + "</option>";
        });
        $("#"+selectId+" option").remove();
        $("#"+selectId).append(html);
    }


    var tipsInfo = function () {
        var tipsDom = $("#tips");
        var tips = "长度17位，前9位，行政区划代码6位+3位专题号，后8位自定义编码，字母数字均可，不足8位#补齐\n" +
            "3位专题号，用户自己设置。一般就是100，101，102...,201,202";
        if(tipsDom.length != 0){
            tipsDom.attr("title",tips);
        }
    }


    return {
        getAllProject:getAllProject,
        getAllTaskByProjectName:getAllTaskByProjectName,
        initProjectSelect:initProjectSelect,
        initTaskSelect:initTaskSelect,
        editDisplayTaskName:editDisplayTaskName,
        getAllTask:getAllTask,
        initEditTaskSelect:initEditTaskSelect,
        tipsInfo:tipsInfo
    };
});