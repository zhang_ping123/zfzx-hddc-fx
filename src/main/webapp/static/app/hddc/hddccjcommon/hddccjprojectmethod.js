define([
    "UtilDir/util",
    "UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
    "Date","DateCN", "css!DateCss",
    "EasyUI","EasyUI-lang"
],function(Util, Tool, SelectOrg, SearchBlock){
    var getHddcCjProjectController = function(){
        return getServer() + "/hddc/hddcCjProjects";
    };

    var projectData=null;
    function getAllProject(){

        $.ajax({
            url:getHddcCjProjectController() + "/queryAllHddcCjProjects",
            type: "get",
            async:false,
            success: function (data) {
                projectData =  data;
            }
        });
        /*
       $.ajax({
           url:getHddcCjProjectController() + "/findHddcCjProjectEntityByPersonId/"+"531ec1e7509c4e07b21bb36d254f3ea6",
           type: "get",
           async:false,
           success: function (data) {
               debugger;
               projectData =  data;
           }
       });
        */
        return projectData;
    }

    return {
        getAllProject:getAllProject
    };
});