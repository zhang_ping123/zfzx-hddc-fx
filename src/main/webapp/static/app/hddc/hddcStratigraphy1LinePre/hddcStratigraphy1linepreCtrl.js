define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcStratigraphy1LinePre/hddcStratigraphy1linepreSupport"],function(hddcStratigraphy1linepreSupport){
            hddcStratigraphy1linepreSupport.init();
        });
    };
});