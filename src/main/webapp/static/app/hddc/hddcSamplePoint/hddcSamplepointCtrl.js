define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcSamplePoint/hddcSamplepointSupport"],function(hddcSamplepointSupport){
            hddcSamplepointSupport.init();
        });
    };
});