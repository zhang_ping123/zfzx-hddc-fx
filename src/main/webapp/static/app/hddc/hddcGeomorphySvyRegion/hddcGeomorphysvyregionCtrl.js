define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGeomorphySvyRegion/hddcGeomorphysvyregionSupport"],function(hddcGeomorphysvyregionSupport){
            hddcGeomorphysvyregionSupport.init();
        });
    };
});