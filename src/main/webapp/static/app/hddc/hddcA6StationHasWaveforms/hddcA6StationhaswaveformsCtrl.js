define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcA6StationHasWaveforms/hddcA6StationhaswaveformsSupport"],function(hddcA6StationhaswaveformsSupport){
            hddcA6StationhaswaveformsSupport.init();
        });
    };
});