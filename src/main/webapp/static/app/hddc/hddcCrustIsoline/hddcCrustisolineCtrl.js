define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcCrustIsoline/hddcCrustisolineSupport"],function(hddcCrustisolineSupport){
            hddcCrustisolineSupport.init();
        });
    };
});