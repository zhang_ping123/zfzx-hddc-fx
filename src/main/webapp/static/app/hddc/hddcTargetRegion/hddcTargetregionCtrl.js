define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcTargetRegion/hddcTargetregionSupport"],function(hddcTargetregionSupport){
            hddcTargetregionSupport.init();
        });
    };
});