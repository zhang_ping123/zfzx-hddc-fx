define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcCollectedDrillHole/hddcCollecteddrillholeSupport"],function(hddcCollecteddrillholeSupport){
            hddcCollecteddrillholeSupport.init();
        });
    };
});