define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGeomorStation/hddcGeomorstationSupport"],function(hddcGeomorstationSupport){
            hddcGeomorstationSupport.init();
        });
    };
});