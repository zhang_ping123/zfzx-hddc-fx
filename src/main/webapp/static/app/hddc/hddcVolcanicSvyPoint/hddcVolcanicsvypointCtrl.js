define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcVolcanicSvyPoint/hddcVolcanicsvypointSupport"],function(hddcVolcanicsvypointSupport){
            hddcVolcanicsvypointSupport.init();
        });
    };
});