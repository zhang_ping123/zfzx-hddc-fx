define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcwySamplePoint/hddcWySamplepointSupport"],function(hddcWySamplepointSupport){
            hddcWySamplepointSupport.init();
        });
    };
});