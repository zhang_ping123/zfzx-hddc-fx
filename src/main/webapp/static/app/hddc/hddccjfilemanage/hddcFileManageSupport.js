define([
    "UtilDir/util",
    "UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
    "Date", "DateCN", "css!DateCss",
    "EasyUI", "EasyUI-lang"
], function (Util, Tool, SelectOrg, SearchBlock) {

    var sysPath = getServer() + "/static/app/hddc/hddccjfilemanage";

    var hddcFileManageFormHtml = sysPath + "/views/hddcFileManageForm.html";
    var getHddcFileManageController = function () {
        return getServer() + "/hddc/hddcFileManages";
    };

    /**
     * 页面初始化
     */
    var init = function () {
        uploadFile();
        createHddcFileManageGrid();
    };

    var uploadFile = function () {
        $("#uploadBtn").on("click", function () {
            var uploadinput = document.getElementById("uploadFile");
            var filenumber = document.getElementById("filenumber");
            if (uploadinput.value == "") {
                Util.alert("上传前请先选择文件!");
                return;
            }
            if (filenumber.value == "") {
                Util.alert("上传前请输入文件编码!");
                return;
            }
            var formData = new FormData();
            debugger
            formData.append("file", uploadinput.files[0]);
            // 追加文件编号
            formData.set("filenumber",filenumber.value);
            $.ajax({
                url: getHddcFileManageController() + "/upload",
                data: formData,
                processData: false, //因为data值是FormData对象，不需要对数据做处理。
                contentType: false,
                type: "POST",
                success: function (data) {
                    Util.alert(data);
                    uploadinput.value = "";
                    filenumber.value = "";
                    createHddcFileManageGrid();
                }
            });
        });
        // console.log(document.getElementById("fileIdCondition").files[0].name)
    };
    var createHddcFileManageGrid = function () {
        $("#hddcFileManageGrid").datagrid({
            url: getHddcFileManageController() + "/queryHddcFileManages",
            method: "GET",
            fitColumns: true,
            autoRowHeight: false,
            columns: [[
                {
                    field: 'fileId',
                    title: '文件编号',
                    width: '33%',
                    align: 'center',
                },
                {
                    field: 'fileName',
                    title: '文件名称',
                    width: '33%',
                    align: 'center',
                },
                {
                    field: 'deal',
                    title: '操作',
                    width: '33%',
                    align: 'center',
                    formatter: function (value, rowData, rowIndex) {
                        return '<a href="#" style="margin-right: 20px" onclick="downloadFile(\'' + rowData.fileName + '\');">下载</a>'
                            + '<a href="#" onclick="deleteFile(\'' + rowData.uuid + '\');">删除</a>'
                    }
                }
            ]],
            queryParams: {
                fileId: $("#fileIdCondition").val(),
                fileName: $("#fileNameCondition").val(),
            },
            pagination: true,
            pageSize: 10
        });
    };

    window.deleteFile = function(id) {
        console.log("shanchu");
        Util.confirm("是否要删除选中的数据?", function() {
            $.ajax({
                url: getHddcFileManageController() ,
                data: {
                    ids : id
                },
                type: "delete",
                success: function (data) {
                    createHddcFileManageGrid();
                }
            });
        }, function() {
            return;
        });

    };


    window.downloadFile = function(name) {
        window.location.href = getHddcFileManageController() + "/download/" + name;
        // $.ajax({
        //     url: getHddcFileManageController() + "/download/" + name ,
        //     type: "get",
        //     success: function (data) {
        //     }
        // });

    };


    return {
        init: init
    };
});
