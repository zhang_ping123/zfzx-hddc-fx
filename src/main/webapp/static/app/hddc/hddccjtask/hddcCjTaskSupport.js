define([
	"UtilDir/util",
	"UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
	"static/app/hddc/hddccjtask/hddccjtaskmap",
	"static/app/hddc/hddccjcommon/hddccjprojectmethod",
	"static/app/hddc/hddccjcommon/hddctaskmethod",
    "static/app/hddc/hddccjcommon/hddccjCommon",
	"static/app/hddc/hddccjtask/tmap",
	"Date","DateCN", "css!DateCss",
	"EasyUI","EasyUI-lang"
    ],function(Util, Tool, SelectOrg, SearchBlock,hddccjtaskmap,hddccjprojectmethod,hddctaskmethod,HddccjCommon){

    var sysPath =  getServer() + "/static/app/hddc/hddccjtask";

    var hddcCjTaskFormHtml = sysPath + "/views/hddcCjTaskForm.html";
	var getHddcCjTaskController = function(){
		return getServer() + "/hddc/hddcCjTasks";
	};

	var getHddcCjProjectController = function(){
		return getServer() + "/hddc/hddcCjProjects";
	};

	var mapdata=null;


	/**
	 * 页面初始化
	 */
	var init = function(){
        initSearchBlock();
        queryBtnBind();
		//initProvinceSelect();
		//initCitySelect();
		//initAreaSelect();
		initcreateProvince();
		createHddcCjTaskGrid();
		initcreateProject();
        HddccjCommon.initProjectSelect("projectNameCondition"); //初始化查询的项目名称
		$("#taskMapLi").css("display", "none");
	};

	$("#taskoperatelab").on('shown.bs.tab', function (e) {
		$("#taskMapLi").css("display", "none");
		initSearchBlock();
	});

	var mapflag=0;
	$("#taskMaplab").on('shown.bs.tab', function (e) {
		if(mapflag==0) {
			hddccjtaskmap.initMap(null);
		}else{
			mapflag=0;
			hddccjtaskmap.initMap(mapdata);
		}
	});

	var initProvinceSelect = function () {
		$.ajax({
			url: getHddcCjTaskController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("provinceSelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initCitySelect = function () {
		$.ajax({
			url: getHddcCjTaskController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("citySelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
	var initAreaSelect = function () {
		$.ajax({
			url: getHddcCjTaskController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("areaSelect");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};

		var initProvinceSelectFrom = function () {
		$.ajax({
			url: getHddcCjTaskController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("province");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initCitySelectFrom = function () {
		$.ajax({
			url: getHddcCjTaskController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("city");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initAreaSelectFrom = function () {
		$.ajax({
			url: getHddcCjTaskController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("area");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var initBelongProjectSelectFrom = function () {
		$.ajax({
			url: getHddcCjTaskController() + "/getValidDictItemsByDictCode/" + "",
			type: "get",
			success: function (data) {
				var mySelect = document.getElementById("belongProject");
				mySelect.add(new Option("请选择", ''), 0);
				for (var i = 0; i < data.length; i++) {
					var name = data[i].dictItemName;
					var code = data[i].dictItemCode;
					mySelect.add(new Option(name, code));
				}
			}
		});
	};
		var createPersonIdsUserSelectFrom = function () {
			SelectOrg.init({
				id: "personIdsName",
				multi: false,
				title: "请选择用户",
				hideTag: true,
				tagType: ["user"],
				callback: function (data) {
					if (data.user.length == 0){
						Util.alert("请选择用户");
						return;
					}
					$("#personIds").val(data.user[0].userId);
					$("#personIdsName").val(data.user[0].userName);
				}
			});
		};

	var initSearchBlock = function(){
        SearchBlock.init("searchBlock");
	};
	var queryBtnBind = function(){
        $("#btnSearch").click(function () {
            createHddcCjTaskGrid();
        });
        $("#btnReset").click(function () {
            $("#provinceSelect").val("");
	            $("#citySelect").val("");
	            $("#areaSelect").val("");
				$("#taskNameCondition").val("");
			});
	};
	var createHddcCjTaskGrid= function() {
        $("#hddcCjTaskGrid").datagrid({
            url:getHddcCjTaskController() + "/queryHddcCjTasks",
			method:"GET",
            fitColumns: true,
            autoRowHeight: false,
            columns:[[
				{field:"ck",checkbox:true},
                {
                    field:'taskId',
                    title:'任务ID',
                    width:'20%',
                    align:'center',
                },
                {
                    field:'taskName',
                    title:'任务名称',
                    width:'20%',
                    align:'center',
                    formatter:function(value,rowData,rowIndex){
                        return '<a href="#" onclick="_editHddcCjTask(\'' + rowData.id + '\');"> '+rowData.taskName+' </a> '
                    }
                },
				{
					field:'province',
					title:'省',
					width:'20%',
					align:'center',
				},
				{
					field:'city',
					title:'市',
					width:'20%',
					align:'center',
				},
				{
					field:'area',
					title:'区（县）',
					width:'20%',
					align:'center',
				},
				{
					field:'belongProject',
					title:'所属项目',
					width:'20%',
					align:'center',
				},
            ]],
            toolbar: [{
                iconCls: 'fa fa-plus-circle',
                text:"添加",
                handler: function(){
                    addHddcCjTask();
                }
            },{
                iconCls: 'fa fa-trash-o',
                text:"删除",
                handler: function(){
                    deleteHddcCjTask();
                }
            }],
            queryParams:{
                province: $("#provinceSelect").val(),
                city: $("#citySelect").val(),
                area: $("#areaSelect").val(),
                projectName: $("#projectNameCondition").val(),
                taskName: $("#taskNameCondition").val(),
            },
            pagination: true,
            pageSize: 10
        });
    };
	var formValidator = function(){
		$("#hddcCjTaskForm").validate({
			rules : {
				taskId : {
					required : true,
				},
				taskName : {
					required : true,
				},
			},
			messages : {
				taskId : {
					required : "任务ID不允许为空!",
				},
				taskName : {
					required : "任务名称不允许为空!",
				},
			}
		});
	};
	var getHddcCjTask = function(id){
		$.ajax({
			url: getHddcCjTaskController() + "/"+id,
			type: "get",
			success: function (data) {
				editProvince(data.province, data.city, data.area);
				addProjectData();
				$("#belongProject").val(data.belongProject);
				Tool.deserialize("hddcCjTaskForm", data);
			}
		});
	};

	var addProjectData = function () {
		var html = '<option value="" disabled selected style="display:none;">请选择</option>';
		$("#belongProject option").remove();
		$("#belongProject").append(html);
		if(projectData!=null) {
			for (var i = 0; i < projectData.length; i++) {
				html += "<option value='" + projectData[i].projectName + "' exid='" + projectData[i].projectId + "'>" + projectData[i].projectName + "</option>";
			}
		}
		$("#belongProject").append(html);
	}

	var addHddcCjTask = function () {
		var slidebar = Util.slidebar({
			url: hddcCjTaskFormHtml,
			width: "800px",
			cache: false,
			close : true,
			afterOpen: function () {

			},
			afterLoad: function () {
				//initProvinceSelectFrom();
				//initCitySelectFrom();
				//initAreaSelectFrom();
				document.getElementById("taskName").readOnly=false;
				document.getElementById("taskId").readOnly=false;
				initcreateProvinceForm();
				//$("#belongProject").val("");
				addProjectData();
				//initBelongProjectSelectFrom();
				//createPersonIdsUserSelectFrom();
				formValidator();


				$("#editmapinfoBtn").on("click", function () {
					var projectname=$("#belongProject").val();
					var projectid=null;
					for(var i=0;i<projectData.length;i++){
						if(projectname==projectData[i].projectName){
							projectid=projectData[i].projectId;
							break;
						}
					}

					$("#belongProjectID").val(projectid);
					var taskdatas = hddctaskmethod.getAllTaskByProjectId(projectid);
					var taskName=$("#taskName").val();
					var taskId=$("#taskId").val();
					for(var i =0;i<taskdatas.length;i++){
						if(taskdatas[i].taskId==taskId){
							alert("任务ID不能重复");
							return;
						}
						if(taskdatas[i].taskName==taskName){
							alert("任务名称不能重复");
							return;
						}
					}
					mapdata=Tool.serialize("hddcCjTaskForm");
					mapflag=1;
					$("#taskMapLi").css("display", "block");
					$('#myTab a[href="#taskMap"]').tab('show');
					//hddccjtaskmap.initMap();
				});

				$("#saveBtn").on("click", function () {
					if($("#hddcCjTaskForm").valid()){
						var projectname=$("#belongProject").val();
						var projectid=null;
						for(var i=0;i<projectData.length;i++){
							if(projectname==projectData[i].projectName){
								projectid=projectData[i].projectId;
								break;
							}
						}

						$("#belongProjectID").val(projectid);
						var taskdatas = hddctaskmethod.getAllTaskByProjectId(projectid);
						var taskName=$("#taskName").val();
						var taskId=$("#taskId").val();
						for(var i =0;i<taskdatas.length;i++){
							if(taskdatas[i].taskId==taskId){
								alert("任务ID不能重复");
								return;
							}
							if(taskdatas[i].taskName==taskName){
								alert("任务名称不能重复");
								return;
							}
						}
						var data = Tool.serialize("hddcCjTaskForm");
						debugger;
						$.ajax({
							url: getHddcCjTaskController() ,
                            contentType:"application/json",
							data: JSON.stringify(data),
							type: "post",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
                                createHddcCjTaskGrid();
							}
						});
					}
				});

			}
		});
	};
	window._editHddcCjTask = function(hddcCjTaskId) {
		var slidebar = Util.slidebar({
			url: hddcCjTaskFormHtml,
			width: "800px",
			cache: false,
			close : true,
			afterOpen: function () {

			},
			afterLoad: function () {
				//initProvinceSelectFrom();
				//initCitySelectFrom();
				//initAreaSelectFrom();
				document.getElementById("taskName").readOnly=true;
				document.getElementById("taskId").readOnly=true;
				initBelongProjectSelectFrom();
				//createPersonIdsUserSelectFrom();
				formValidator();
				getHddcCjTask(hddcCjTaskId);


				$("#editmapinfoBtn").on("click", function () {
					var projectname=$("#belongProject").val();
					var projectid=null;
					for(var i=0;i<projectData.length;i++){
						if(projectname==projectData[i].projectName){
							projectid=projectData[i].projectId;
							break;
						}
					}

					$("#belongProjectID").val(projectid);
					var taskdatas = hddctaskmethod.getAllTaskByProjectId(projectid);
					var taskName=$("#taskName").val();
					var taskId=$("#taskId").val();
					for(var i =0;i<taskdatas.length;i++){
						if(taskdatas[i].taskId==taskId){
							continue;
						}
						if(taskdatas[i].taskName==taskName){
							alert("任务名称不能重复");
							return;
						}
					}
					mapdata=Tool.serialize("hddcCjTaskForm");
					mapflag=1;
					$("#taskMapLi").css("display", "block");
					$('#myTab a[href="#taskMap"]').tab('show');

					//hddccjtaskmap.initMap();
				});
				$("#saveBtn").on("click", function () {
					if($("#hddcCjTaskForm").valid()){
						var projectname=$("#belongProject").val();
						var projectid=null;
						for(var i=0;i<projectData.length;i++){
							if(projectname==projectData[i].projectName){
								projectid=projectData[i].projectId;
								break;
							}
						}

						$("#belongProjectID").val(projectid);
						var taskdatas = hddctaskmethod.getAllTaskByProjectId(projectid);
						var taskName=$("#taskName").val();
						var taskId=$("#taskId").val();
						for(var i =0;i<taskdatas.length;i++){
							if(taskdatas[i].taskId==taskId){
								continue;
							}
							if(taskdatas[i].taskName==taskName){
								alert("任务名称不能重复");
								return;
							}
						}
						var data = Tool.serialize("hddcCjTaskForm");

						$.ajax({
							url: getHddcCjTaskController(),
                            contentType:"application/json",
                            data: JSON.stringify(data),
							type: "put",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
								createHddcCjTaskGrid();
							}
						});
					}
				});
			}
		});
	};
	var deleteHddcCjTask = function() {
		var rows = $("#hddcCjTaskGrid").datagrid("getSelections");
		if (rows == null || rows.length == 0) {
			Util.alert("请选择一行数据!");
			return;
		}
		Util.confirm("是否要删除选中的数据?", function() {
			var ids = "";
			$.each(rows, function(i, row){
				ids += row.id + ",";

			});
			ids = ids.substr(0,ids.length - 1);
			$.ajax({
				url: getHddcCjTaskController() ,
				data: {
					ids : ids
				},
				type: "delete",
				success: function (data) {
					createHddcCjTaskGrid();
				}
			});
		}, function() {
			return;
		});

	};

	var projectData=null;
	var initcreateProject = function () {
		debugger;
		projectData = hddccjprojectmethod.getAllProject();
		debugger;
		//projectData=hddccjprojectmethod.projectData;
/*
		var html = "";
		//belongProject
		$.ajax({
			url:getHddcCjProjectController() + "/queryAllHddcCjProjects",
			type: "get",
			success: function (data) {
				projectData=data;
			}
		});

 */

	}

	var initcreateProvince = function () {
		var html = "";
		$("#citySelect").append(html);
		$("#areaSelect").append(html);
		$.ajax({
			url: getServer() + "/divisions/root/subdivisions",
			contentType: "application/json",
			type: "get",
			success: function (data) {
				$.each(data, function (idx, item) {
					html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
				});
				$("#provinceSelect").append(html);
			}
		});
		$("#provinceSelect").change(function () {
			var html = '<option value="" disabled selected style="display:none;">请选择</option>';
			$("#citySelect option").remove();
			$("#citySelect").append(html);
			$("#areaSelect option").remove();
			$("#areaSelect").append(html);
			var divisionId = $(this).find("option:selected").attr("exid");
			if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
				$("#citySelect").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
				return;
			}
			$.ajax({
				url: getServer() + "/divisions/" + divisionId + "/subdivisions",
				contentType: "application/json",
				type: "get",
				success: function (data) {
					$.each(data, function (idx, item) {
						html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
					});
					$("#citySelect").append(html);
				}
			});
		});
		$("#citySelect").change(function () {
			if ($(this).val() == "") return;
			var html = '<option value="" disabled selected style="display:none;">请选择</option>';
			$("#areaSelect option").remove();
			$("#areaSelect").append(html);
			var divisionId = $(this).find("option:selected").attr("exid");
			$.ajax({
				url: getServer() + "/divisions/" + divisionId + "/subdivisions",
				contentType: "application/json",
				type: "get",
				success: function (data) {
					$.each(data, function (idx, item) {
						html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
					});
					$("#areaSelect").append(html);
				}
			});
		});
	}
	var initcreateProvinceForm = function () {
		var html = "";
		$("#city").append(html);
		$("#area").append(html);
		$.ajax({
			url: getServer() + "/divisions/root/subdivisions",
			contentType: "application/json",
			type: "get",
			success: function (data) {
				$.each(data, function (idx, item) {
					html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
				});
				$("#province").append(html);
			}
		});
		$("#province").change(function () {
			var html = '<option value="" disabled selected style="display:none;">请选择</option>';
			$("#city option").remove();
			$("#city").append(html);
			$("#area option").remove();
			$("#area").append(html);
			var divisionId = $(this).find("option:selected").attr("exid");
			if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
				$("#city").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
				return;
			}
			$.ajax({
				url: getServer() + "/divisions/" + divisionId + "/subdivisions",
				contentType: "application/json",
				type: "get",
				success: function (data) {
					$.each(data, function (idx, item) {
						html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
					});
					$("#city").append(html);
				}
			});
		});
		$("#city").change(function () {
			if ($(this).val() == "") return;
			var html = '<option value="" disabled selected style="display:none;">请选择</option>';
			$("#area option").remove();
			$("#area").append(html);
			var divisionId = $(this).find("option:selected").attr("exid");
			$.ajax({
				url: getServer() + "/divisions/" + divisionId + "/subdivisions",
				contentType: "application/json",
				type: "get",
				success: function (data) {
					$.each(data, function (idx, item) {
						html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
					});
					$("#area").append(html);
				}
			});
		});
	}

	var editProvince = function (Province, City, Area) {
		var html = "";
		$("#city").append(html);
		$("#area").append(html);
		$.ajax({
			url: getServer() + "/divisions/root/subdivisions",
			contentType: "application/json",
			type: "get",
			success: function (data) {
				$.each(data, function (idx, item) {
					if (item.divisionName == Province) {
						var divisionId = item.divisionId;
						var htmlCity = '';
						if (Province == "北京市" || Province == "天津市" || Province == "上海市" || Province == "重庆市") {
							$("#city").append("<option value='" + Province + "' exid='" + divisionId + "'>" + Province + "</option>");
							$('#city').val(City);
							var htmlArea = '';
							$.ajax({
								url: getServer() + "/divisions/" + divisionId + "/subdivisions",
								contentType: "application/json",
								type: "get",
								success: function (data) {
									$.each(data, function (idx, item) {
										htmlArea += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
									});
									$("#area").append(htmlArea);
									$('#area').val(Area);
								}
							});

						} else {
							$.ajax({
								url: getServer() + "/divisions/" + divisionId + "/subdivisions",
								contentType: "application/json",
								type: "get",
								success: function (data) {
									$.each(data, function (idx, item) {
										if (item.divisionName == City) {
											var divisionId = item.divisionId;
											var htmlArea = '';
											$.ajax({
												url: getServer() + "/divisions/" + divisionId + "/subdivisions",
												contentType: "application/json",
												type: "get",
												success: function (data) {
													$.each(data, function (idx, item) {
														htmlArea += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
													});
													$("#area").append(htmlArea);
													$('#area').val(Area);
												}
											});
										}
										htmlCity += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
									});
									$("#city").append(htmlCity);
									$('#city').val(City);
								}
							});
						}
					}
					html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
				});
				$("#province").append(html);
				$('#province').val(Province);
			}
		});
		$("#province").change(function () {
			var html = '<option value="" disabled selected style="display:none;">请选择</option>';
			$("#city option").remove();
			$("#city").append(html);
			$("#area option").remove();
			$("#area").append(html);
			var divisionId = $(this).find("option:selected").attr("exid");
			if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
				$("#city").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
				return;
			}
			$.ajax({
				url: getServer() + "/divisions/" + divisionId + "/subdivisions",
				contentType: "application/json",
				type: "get",
				success: function (data) {
					$.each(data, function (idx, item) {
						html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
					});
					$("#city").append(html);
				}
			});
		});
		$("#city").change(function () {
			if ($(this).val() == "") return;
			var html = '<option value="" disabled selected style="display:none;">请选择</option>';
			$("#area option").remove();
			$("#area").append(html);
			var divisionId = $(this).find("option:selected").attr("exid");
			$.ajax({
				url: getServer() + "/divisions/" + divisionId + "/subdivisions",
				contentType: "application/json",
				type: "get",
				success: function (data) {
					$.each(data, function (idx, item) {
						html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
					});
					$("#area").append(html);
				}
			});
		});
	}

	return {
		init:init
	};
});
