define([
    "UtilDir/util",
    "UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
    "Date","DateCN", "css!DateCss",
    "EasyUI","EasyUI-lang"
],function(Util, Tool, SelectOrg, SearchBlock){

    var getHddcCjTaskController = function(){
        return getServer() + "/hddc/hddcCjTasks";
    };
    var map=null;
    var projectTask=null;
    function initMap(data) {
        projectTask=data;
        if(map==null) {
            //初始化地图对象

            /*
            var imageURL = "http://t0.tianditu.gov.cn/img_w/wmts?" +
                "SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=img&STYLE=default&TILEMATRIXSET=w&FORMAT=tiles" +
                "&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}&tk=acce9f1241bb60d5164939ee53173caf";
             */
            var imageURL1="https://fxpc.mem.gov.cn/wmts?service=wmts&request=gettile&version=1.0.0&layer=vec&style=default&tilematrixset=c&format=tiles&tilematrix={z}&tilerow={y}&tilecol={x} ";
            var lay1 = new T.TileLayer(imageURL1, {minZoom: 1, maxZoom: 18});
            var imageURL2="https://fxpc.mem.gov.cn/wmts?service=wmts&request=gettile&version=1.0.0&layer=cva&style=default&tilematrixset=c&format=tiles&tilematrix={z}&tilerow={y}&tilecol={x} ";
            var lay2 = new T.TileLayer(imageURL2, {minZoom: 1, maxZoom: 18});
            var imageURL3="https://fxpc.mem.gov.cn/wmts?service=wmts&request=gettile&version=1.0.0&layer=img&style=default&tilematrixset=c&format=tiles&tilematrix={z}&tilerow={y}&tilecol={x} ";
            var lay3 = new T.TileLayer(imageURL3, {minZoom: 1, maxZoom: 18});
            var imageURL4="https://fxpc.mem.gov.cn/wmts?service=wmts&request=gettile&version=1.0.0&layer=cia&style=default&tilematrixset=c&format=tiles&tilematrix={z}&tilerow={y}&tilecol={x} ";
            var lay4 = new T.TileLayer(imageURL4, {minZoom: 1, maxZoom: 18});
            var config = {
                projection: "EPSG:4326",
                minZoom: 4,
                layers: [lay1]
                //layers: [lay1,lay2,lay3,lay4]
            };
            //https://fxpc.mem.gov.cn/wmts?service=wmts&request=gettile&version=1.0.0&layer=cva&style=default&tilematrixset=c&format=tiles&tilematrix=9&tilerow=75&tilecol=423
            map = new T.Map("hddctask-map",config);
            map.clearOverLays();
            //设置显示地图的中心点和级别
            map.centerAndZoom(new T.LngLat(116.40969,39.89945),4);





            var control=new T.Control.Zoom();
            map.addControl(control);

            //创建比例尺控件对象
            var scale = new T.Control.Scale();
            map.addControl(scale);

            map.enableScrollWheelZoom();
            map.enableDrag();

            //创建鹰眼控件对象
            var miniMap = new T.Control.OverviewMap({
                 isOpen: true,
                 size: new T.Point(150, 150)
             });
             map.addControl(miniMap);

            //创建地图类型控件对象
           // var ctrl = new T.Control.MapType();
           // map.addControl(ctrl);

            var bodyWidth = window.innerWidth-15;//1920px
            var mapHeight =  window.innerHeight-40;//796px
            var mapWidth = bodyWidth;//1469px

            //var height = window.innerHeight - 150;
            //var width = window.innerWidth - 15;

            $("#hddctask-mapDiv").css("height",mapHeight + "px");
            $("#hddctask-mapDiv").css("width",mapWidth + "px");
            $("#hddctask-map").css("height",mapHeight + "px");
            $("#hddctask-map").css("width",mapWidth + "px");

            map.checkResize();
            $("#toolbox-toolsDiv").css("z-index", "1000");
            $("#toolbox-toolsDiv").show();

            initTaskToolsEvent();
        }
        map.clearOverLays();
        lineArrays=[];
        lineArrays.length=0;
        polygonArrays=[];
        polygonArrays.length=0;
        initMapInterface();
    };
    
    function initMapInterface() {
        if(projectTask!=null){
            mapRegionResults=[];
            mapRegionResults.length=0;
            var results=projectTask.mapInfos;
            var dataArrays=results.split("type||");
            for(var i =0;i<dataArrays.length;i++){
                var typeArrays=dataArrays[i].split(":");
                if(typeArrays.length==2){
                    var type=typeArrays[0];
                    var drawArrays=typeArrays[1].split(";");
                    var points=[];
                    for(var j=0;j<drawArrays.length;j++){
                        var pointArray=drawArrays[j].split(",");
                        if(pointArray.length==2){
                            points.push(new T.LngLat(pointArray[0],pointArray[1]));
                        }

                    }
                    debugger;
                    if(type=="line"){
                        var line = new T.Polyline(points);
                        map.addOverLay(line);
                        line.enableEdit();
                        lineArrays.push(line);
                        /*
                        var points1 = lnglatdataswitch(points);
                        var result="type||line:";
                        for(var m=0;m<points1.length;m++){
                            result+=points1[m][0]+","+points1[m][1]+";";
                        }
                        mapRegionResults.push(result);

                         */
                    }else if(type=="polygon"){
                        var polygon = new T.Polygon(points,{
                            color: "blue", weight: 3, opacity: 0.5, fillColor: "#FFFFFF", fillOpacity: 0.5
                        });
                        //向地图上添加面
                        map.addOverLay(polygon);
                        polygon.enableEdit();
                        polygonArrays.push(polygon);
                        /*
                        var points1 = lnglatdataswitch(points);
                        var result="type||polygon:";
                        for(var m=0;m<points1.length;m++){
                            result+=points1[m][0]+","+points1[m][1]+";";
                        }
                        mapRegionResults.push(result);
                         */
                    }
                }
            }
        }
    }

    function initTaskToolsEvent(){
        /*
        $("#imghzdian-DisplayDiv").unbind("click");
        $("#imghzdian-DisplayDiv").click(function () {
            alert(1);
        });
         */
        $("#imghzxian-DisplayDiv").unbind("click");
        $("#imghzxian-DisplayDiv").click(function () {
            createLineTaskRegion();
        });
        /*
        $("#imghzjuxing-DisplayDiv").unbind("click");
        $("#imghzjuxing-DisplayDiv").click(function () {
            createRectTaskRegion();
        });
         */
        $("#imghzdbx-DisplayDiv").unbind("click");
        $("#imghzdbx-DisplayDiv").click(function () {
            createRectTaskRegion();
        });
        $("#imgjscz-DisplayDiv").unbind("click");
        $("#imgjscz-DisplayDiv").click(function () {
            for(var i=0;i<lineArrays.length;i++){
                var line=lineArrays[i];
                var points1 = lnglatdataswitch(line.getLngLats());
                var result="type||line:";
                for(var j=0;j<points1.length;j++){
                    result+=points1[j][0]+","+points1[j][1]+";";
                }
                mapRegionResults.push(result);
            }

            for(var i=0;i<polygonArrays.length;i++){
                debugger;
                var polygon=polygonArrays[i];
                var points1 = lnglatdataswitch(polygon.getLngLats()[0]);
                var result="type||polygon:";
                for(var j=0;j<points1.length;j++){
                    result+=points1[j][0]+","+points1[j][1]+";";
                }
                mapRegionResults.push(result);
            }
            var results="";
            for(var i=0;i<mapRegionResults.length;i++){
                results+=mapRegionResults[i]+";";
            }
debugger;
            projectTask.mapInfos=results;
            if(projectTask.id==null||projectTask.id==""){
                $.ajax({
                    url: getHddcCjTaskController() ,
                    contentType:"application/json",
                    data: JSON.stringify(projectTask),
                    type: "post",
                    success: function (data) {
                        Util.alert(data.message);
                    }
                });
            }else {
                $.ajax({
                    url: getHddcCjTaskController(),
                    contentType: "application/json",
                    data: JSON.stringify(projectTask),
                    type: "put",
                    success: function (data) {
                        Util.alert(data.message);
                    }
                });
            }
            //projectTask
        });
    }

    var mapRegionResults=[];

    function taskunionmap(data) {
        projectTask=data;
    }

    var lineTaskTool;
    var lineArrays=[];
    function createLineTaskRegion() {
        if(projectTask==null){
            alert("请先创建任务，从任务端进入地图。");
            return;
        }
        var config = {
            strokeColor: "red", //折线颜色
            fillColor: "#FFFFFF",    //填充颜色。当参数为空时，折线覆盖物将没有填充效果
            strokeWeight: "3px", //折线的宽度，以像素为单位
            strokeOpacity: 0.5,  //折线的透明度，取值范围0 - 1
            fillOpacity: 0.5        //填充的透明度，取值范围0 - 1.
        };
        lineTaskTool=new T.PolylineTool(map, config);
        lineTaskTool.open();
        lineTaskTool.addEventListener("draw", onDrawHZLine);
        function onDrawHZLine(points) {
            var line = new T.Polyline(points.currentLnglats);
            //向地图上添加线
            map.addOverLay(line);
            line.enableEdit();
            lineArrays.push(line);
            lineTaskTool.close();
            //var points1 = lnglatdataswitch(points.currentLnglats);
            /*
            var points1 = lnglatdataswitch(line.getLngLats());
            var result="type||line:";
            for(var i=0;i<points1.length;i++){
                result+=points1[i][0]+","+points1[i][1]+";";
            }
            mapRegionResults.push(result);
             */
        }
    }

    var rectTaskTool;
    function createRectTaskRegion(){
        rectTaskTool=new T.RectangleTool(map);
        rectTaskTool.open();
    }

    var polygonTaskTool;
    var polygonArrays=[];
    function createRectTaskRegion(){
        if(projectTask==null){
            alert("请先创建任务，从任务端进入地图。");
            return;
        }
        var config = {
            strokeColor: "red", //折线颜色
            fillColor: "#FFFFFF",    //填充颜色。当参数为空时，折线覆盖物将没有填充效果
            strokeWeight: "3px", //折线的宽度，以像素为单位
            strokeOpacity: 1,  //折线的透明度，取值范围0 - 1
            fillOpacity: 0.2,        //填充的透明度，取值范围0 - 1
            showLabel: false             //是否显示页面，默认为true.
        };
        polygonTaskTool=new T.PolygonTool(map,config);
        //TEvent.addListener(polygonTaskTool, "draw", onDrawHZPolygon);
        polygonTaskTool.open();
        polygonTaskTool.addEventListener("draw", onDrawHZPolygon);

        function onDrawHZPolygon(bounds) {
            //polygonTaskTool.clear();
            var polygon = new T.Polygon(bounds.currentLnglats, {
                strokeColor: "red",
                strokeWeight: 3,
                strokeOpacity: 1,
                fillOpacity: 0.2
            });
            map.addOverLay(polygon);//添加多边形
            polygon.enableEdit();
            polygonArrays.push(polygon);
            polygonTaskTool.close();
//currentLnglats
            /*
            //var points1 = lnglatdataswitch(statisticalPolygon.getLngLats());
            var points1 = lnglatdataswitch(bounds.currentLnglats);
            var result="type||polygon:";
            for(var i=0;i<points1.length;i++){
                result+=points1[i][0]+","+points1[i][1]+";";
            }
            mapRegionResults.push(result);
            debugger;
             */
        }
    }

    function lnglatdataswitch(points){
        var points1=[];
        if (points != null && points.length > 0) {
            for (var i = 0; i < points.length; i++) {
                var pp = [];
                pp.push(parseFloat(points[i].getLng()));
                pp.push(parseFloat(points[i].getLat()));
                points1.push(pp);
            }
        }
        return points1;
    }

    return {
        initMap:initMap
    };
});