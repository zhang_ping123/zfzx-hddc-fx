define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcwyChartGeologicalSurvey/chartGeologicalSurveySupport"],function(chartGeologicalSurveySupport){
            chartGeologicalSurveySupport.init();
        });
    };
});