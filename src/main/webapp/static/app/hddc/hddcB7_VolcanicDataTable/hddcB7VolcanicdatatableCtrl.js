define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcB7_VolcanicDataTable/hddcB7VolcanicdatatableSupport"],function(hddcB7VolcanicdatatableSupport){
            hddcB7VolcanicdatatableSupport.init();
        });
    };
});