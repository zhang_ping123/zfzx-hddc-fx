define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcwyChartGeomorphological/chartGeomorphologicalSupport"],function(chartGeomorphologicalSupport){
            chartGeomorphologicalSupport.init();
        });
    };
});