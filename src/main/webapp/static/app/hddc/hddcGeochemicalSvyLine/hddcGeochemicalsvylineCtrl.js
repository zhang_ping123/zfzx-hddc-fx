define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGeochemicalSvyLine/hddcGeochemicalsvylineSupport"],function(hddcGeochemicalsvylineSupport){
            hddcGeochemicalsvylineSupport.init();
        });
    };
});