define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcwyGeomorphySvySamplePoint/hddcWyGeomorphysvysamplepointSupport"],function(hddcWyGeomorphysvysamplepointSupport){
            hddcWyGeomorphysvysamplepointSupport.init();
        });
    };
});