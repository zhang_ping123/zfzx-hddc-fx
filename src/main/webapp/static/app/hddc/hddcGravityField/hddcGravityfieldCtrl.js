define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGravityField/hddcGravityfieldSupport"],function(hddcGravityfieldSupport){
            hddcGravityfieldSupport.init();
        });
    };
});