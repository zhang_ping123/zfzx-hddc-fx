define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGeoProfileLine/hddcGeoprofilelineSupport"],function(hddcGeoprofilelineSupport){
            hddcGeoprofilelineSupport.init();
        });
    };
});