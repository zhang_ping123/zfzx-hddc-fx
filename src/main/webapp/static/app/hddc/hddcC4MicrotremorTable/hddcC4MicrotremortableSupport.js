define([
	"UtilDir/util",
	"UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
    "static/app/hddc/hddccjcommon/hddccjCommon",
	"Date","DateCN", "css!DateCss",
	"EasyUI","EasyUI-lang"
    ],function(Util, Tool, SelectOrg, SearchBlock,HddccjCommon){

    var sysPath =  getServer() + "/static/app/hddc/hddcC4MicrotremorTable";

    var hddcC4MicrotremortableFormHtml = sysPath + "/views/hddcC4MicrotremortableForm.html";
	var getHddcC4MicrotremortableController = function(){
		return getServer() + "/hddcC4Microtremortables";
	};

	/**
	 * 页面初始化
	 */
	var init = function(){
        initSearchBlock();
        queryBtnBind();
		createHddcC4MicrotremortableGrid();
	};

		var initQualityinspectionDateTimeDate = function() {
			$("#qualityinspectionDate").datetimepicker({
				//设置使用语言：cn是自定义的中文版本，还可以扩展其他语言版本
				language : "cn",
				//输出格式化
				format : 'yyyy-mm-dd',
				//直接选择‘今天’
				todayBtn : true,
				//设置最精确的时间选择视图
				minView : 'month',
				//高亮当天日期
				todayHighlight : true,
				//显示“上午”“下午”
				showMeridian : true,
				//选择完毕后自动关闭
				autoclose : true
			});
		};
		var initExamineDateTimeDate = function() {
			$("#examineDate").datetimepicker({
				//设置使用语言：cn是自定义的中文版本，还可以扩展其他语言版本
				language : "cn",
				//输出格式化
				format : 'yyyy-mm-dd',
				//直接选择‘今天’
				todayBtn : true,
				//设置最精确的时间选择视图
				minView : 'month',
				//高亮当天日期
				todayHighlight : true,
				//显示“上午”“下午”
				showMeridian : true,
				//选择完毕后自动关闭
				autoclose : true
			});
		};

	var initSearchBlock = function(){
        SearchBlock.init("searchBlock");
	};
	var queryBtnBind = function(){
        $("#btnSearch").click(function () {
            createHddcC4MicrotremortableGrid();
        });
        $("#btnReset").click(function () {
			$("#projectIdCondition").val("");
				$("#unitCondition").val("");
				$("#createTimeCondition").val("");
				$("#instrumentCondition").val("");
			});
	};
	var createHddcC4MicrotremortableGrid= function() {
        $("#hddcC4MicrotremortableGrid").datagrid({
            url:getHddcC4MicrotremortableController() + "/queryHddcC4Microtremortables",
			method:"GET",
            fitColumns: true,
            autoRowHeight: false,
            columns:[[
				{
					field:'examineComments',
					title:'审查意见',
					width:'20%',
					align:'center',
				},
				{
					field:'qualityinspectionDate',
					title:'质检时间',
					width:'20%',
					align:'center',
				},
				{
					field:'qualityinspectionUser',
					title:'质检人',
					width:'20%',
					align:'center',
				},
				{
					field:'projectId',
					title:'项目ID',
					width:'20%',
					align:'center',
				},
				{
					field:'qualityinspectionComments',
					title:'质检原因',
					width:'20%',
					align:'center',
				},
				{
					field:'stationlatitude',
					title:'测点纬度',
					width:'20%',
					align:'center',
				},
				{
					field:'stationlongitude',
					title:'测点经度',
					width:'20%',
					align:'center',
				},
				{
					field:'frequency',
					title:'采样率',
					width:'20%',
					align:'center',
				},
				{
					field:'examineUser',
					title:'审查人',
					width:'20%',
					align:'center',
				},
				{
					field:'qualityinspectionStatus',
					title:'质检状态',
					width:'20%',
					align:'center',
				},
				{
					field:'taskId',
					title:'任务ID',
					width:'20%',
					align:'center',
				},
            ]],
            toolbar: [{
                iconCls: 'fa fa-plus-circle',
                text:"添加",
                handler: function(){
                    addHddcC4Microtremortable();
                }
            },{
                iconCls: 'fa fa-trash-o',
                text:"删除",
                handler: function(){
                    deleteHddcC4Microtremortable();
                }
            }],
            queryParams:{
				projectId: $("#projectIdCondition").val(),
				unit: $("#unitCondition").val(),
				createTime: $("#createTimeCondition").val(),
				instrument: $("#instrumentCondition").val(),
            },
            pagination: true,
            pageSize: 10
        });
    };
	var formValidator = function(){
		$("#hddcC4MicrotremortableForm").validate({
			rules : {
				examineComments : {
					required : true,
				},
				qualityinspectionDate : {
					required : true,
				},
				qualityinspectionUser : {
					required : true,
				},
				commentinfo : {
					required : true,
				},
				timewindow : {
					required : true,
				},
				qualityinspectionComments : {
					required : true,
				},
				unit : {
					required : true,
				},
				examineDate : {
					required : true,
				},
				mircrotremorArwid : {
					required : true,
				},
			},
			messages : {
				examineComments : {
					required : "审查意见不允许为空!",
				},
				qualityinspectionDate : {
					required : "质检时间不允许为空!",
				},
				qualityinspectionUser : {
					required : "质检人不允许为空!",
				},
				commentinfo : {
					required : "备注不允许为空!",
				},
				timewindow : {
					required : "测量时间长度不允许为空!",
				},
				qualityinspectionComments : {
					required : "质检原因不允许为空!",
				},
				unit : {
					required : "测量单位不允许为空!",
				},
				examineDate : {
					required : "审查时间不允许为空!",
				},
				mircrotremorArwid : {
					required : "三分量第脉动记录文件原始编号不允许为空!",
				},
			}
		});
	};
	var getHddcC4Microtremortable = function(id){
		$.ajax({
			url: getHddcC4MicrotremortableController() + "/"+id,
			type: "get",
			success: function (data) {
				Tool.deserialize("hddcC4MicrotremortableForm", data);
			}
		});
	};

	var addHddcC4Microtremortable = function () {
		var slidebar = Util.slidebar({
			url: hddcC4MicrotremortableFormHtml,
			width: "580px",
			cache: false,
			close : true,
			afterLoad: function () {
				initQualityinspectionDateTimeDate();
				initExamineDateTimeDate();
				formValidator();
				$("#saveBtn").on("click", function () {
					if($("#hddcC4MicrotremortableForm").valid()){
						var data = Tool.serialize("hddcC4MicrotremortableForm");
						$.ajax({
							url: getHddcC4MicrotremortableController() ,
                            contentType:"application/json",
							data: JSON.stringify(data),
							type: "post",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
                                createHddcC4MicrotremortableGrid();
							}
						});
					}
				});
			}
		});
	};
	window._editHddcC4Microtremortable = function(hddcC4MicrotremortableId) {
		var slidebar = Util.slidebar({
			url: hddcC4MicrotremortableFormHtml,
			width: "580px",
			cache: false,
			close : true,
			afterLoad: function () {
				initQualityinspectionDateTimeDate();
				initExamineDateTimeDate();
				formValidator();
				getHddcC4Microtremortable(hddcC4MicrotremortableId);
				$("#saveBtn").on("click", function () {
					if($("#hddcC4MicrotremortableForm").valid()){
						var data = Tool.serialize("hddcC4MicrotremortableForm");
						$.ajax({
							url: getHddcC4MicrotremortableController(),
                            contentType:"application/json",
                            data: JSON.stringify(data),
							type: "put",
							success: function (data) {
								Util.alert(data.message);
								slidebar.close();
								createHddcC4MicrotremortableGrid();
							}
						});
					}
				});
			}
		});
	};
	var deleteHddcC4Microtremortable = function() {
		var rows = $("#hddcC4MicrotremortableGrid").datagrid("getSelections");
		if (rows == null || rows.length == 0) {
			Util.alert("请选择一行数据!");
			return;
		}
		Util.confirm("是否要删除选中的数据?", function() {
			var ids = "";
			$.each(rows, function(i, row){
				ids += row.uuid + ",";

			});
			ids = ids.substr(0,ids.length - 1);
			$.ajax({
				url: getHddcC4MicrotremortableController() ,
				data: {
					ids : ids
				},
				type: "delete",
				success: function (data) {
					createHddcC4MicrotremortableGrid();
				}
			});
		}, function() {
			return;
		});

	};

	return {
		init:init
	};
});
