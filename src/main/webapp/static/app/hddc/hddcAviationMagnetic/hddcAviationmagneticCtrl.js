define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcAviationMagnetic/hddcAviationmagneticSupport"],function(hddcAviationmagneticSupport){
            hddcAviationmagneticSupport.init();
        });
    };
});