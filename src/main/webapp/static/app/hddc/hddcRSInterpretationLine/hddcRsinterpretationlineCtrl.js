define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcRSInterpretationLine/hddcRsinterpretationlineSupport"],function(hddcRsinterpretationlineSupport){
            hddcRsinterpretationlineSupport.init();
        });
    };
});