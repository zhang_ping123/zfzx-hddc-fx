define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGeomorphySvyLine/hddcGeomorphysvylineSupport"],function(hddcGeomorphysvylineSupport){
            hddcGeomorphysvylineSupport.init();
        });
    };
});