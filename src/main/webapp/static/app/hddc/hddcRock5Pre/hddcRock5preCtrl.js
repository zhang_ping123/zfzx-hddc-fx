define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcRock5Pre/hddcRock5preSupport"],function(hddcRock5preSupport){
            hddcRock5preSupport.init();
        });
    };
});