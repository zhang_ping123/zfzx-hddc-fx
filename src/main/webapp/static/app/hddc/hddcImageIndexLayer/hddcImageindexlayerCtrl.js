define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcImageIndexLayer/hddcImageindexlayerSupport"],function(hddcImageindexlayerSupport){
            hddcImageindexlayerSupport.init();
        });
    };
});