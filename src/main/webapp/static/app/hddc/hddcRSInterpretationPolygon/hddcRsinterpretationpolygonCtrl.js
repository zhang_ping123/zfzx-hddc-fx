define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcRSInterpretationPolygon/hddcRsinterpretationpolygonSupport"],function(hddcRsinterpretationpolygonSupport){
            hddcRsinterpretationpolygonSupport.init();
        });
    };
});