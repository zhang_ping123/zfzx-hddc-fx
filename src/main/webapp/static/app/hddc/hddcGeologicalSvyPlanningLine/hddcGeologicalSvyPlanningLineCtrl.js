define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGeologicalSvyPlanningLine/hddcGeologicalSvyPlanningLineSupport"],function(hddcGeologicalsvyplanninglineSupport){
            hddcGeologicalsvyplanninglineSupport.init();
        });
    };
});