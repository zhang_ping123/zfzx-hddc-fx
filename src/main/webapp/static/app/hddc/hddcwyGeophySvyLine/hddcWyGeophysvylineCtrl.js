define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcwyGeophySvyLine/hddcWyGeophysvylineSupport"],function(hddcWyGeophysvylineSupport){
            hddcWyGeophysvylineSupport.init();
        });
    };
});