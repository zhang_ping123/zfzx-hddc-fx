define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGeophySvyLine/hddcGeophysvylineSupport"],function(hddcGeophysvylineSupport){
            hddcGeophysvylineSupport.init();
        });
    };
});