define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcB4_SampleDataTable/hddcB4SampledatatableSupport"],function(hddcB4SampledatatableSupport){
            hddcB4SampledatatableSupport.init();
        });
    };
});