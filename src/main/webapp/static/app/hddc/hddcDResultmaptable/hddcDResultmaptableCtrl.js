define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcDResultmaptable/hddcDResultmaptableSupport"],function(hddcDResultmaptableSupport){
            hddcDResultmaptableSupport.init();
        });
    };
});