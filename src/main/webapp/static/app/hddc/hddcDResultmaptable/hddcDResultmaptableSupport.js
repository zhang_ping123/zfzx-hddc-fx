define([
	"UtilDir/util",
	"UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
    "static/app/hddc/hddccjcommon/hddccjCommon",
	"Date","DateCN", "css!DateCss",
	"EasyUI","EasyUI-lang"
    ],function(Util, Tool, SelectOrg, SearchBlock,HddccjCommon){

    var sysPath =  getServer() + "/static/app/hddc/hddcDResultmaptable";

    var hddcDResultmaptableFormHtml = sysPath + "/views/hddcDResultmaptableForm.html";
	var getHddcDResultmaptableController = function(){
		return getServer() + "/hddc/hddcDResultmaptables";
	};

	/**
	 * 页面初始化
	 */
	var init = function(){
        var pageHref = window.location.href;
        pageType = pageHref.slice(pageHref.length-1,pageHref.length);

        /**
         * pageType
         * 1--1:5万活动断层分布图
         * 4--1:5万活动断层分布图说明书
         * 5--1:5万活动断层分布图成果报告
         * 2--1:25万地震构造图编制
         * 6--1:25万地震构造图说明书
         * 7--1:25万地震构造图成果报告
         * 3--成果图件表-属性表管理
         * **/
        initSearchBlock();
        queryBtnBind();
        initcreateProvince();
		createHddcDResultmaptableGrid();
	};
    var pageType = 0;
    var initcreateProvince = function () {
        var html = "";
        $("#citySelect").append(html);
        $("#areaSelect").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#provinceSelect").append(html);
            }
        });
        $("#provinceSelect").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#citySelect option").remove();
            $("#citySelect").append(html);
            $("#areaSelect option").remove();
            $("#areaSelect").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#citySelect").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#citySelect").append(html);
                }
            });
        });
        $("#citySelect").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#areaSelect option").remove();
            $("#areaSelect").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#areaSelect").append(html);
                }
            });
        });
    }
    var initcreateProvinceForm = function () {
        var html = "";
        $("#city").append(html);
        $("#area").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#province").append(html);
            }
        });
        $("#province").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#city option").remove();
            $("#city").append(html);
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#city").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#city").append(html);
                }
            });
        });
        $("#city").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#area").append(html);
                }
            });
        });
    };
    var editProvince = function (Province, City, Area) {
        var html = "";
        $("#city").append(html);
        $("#area").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    if (item.divisionName == Province) {
                        var divisionId = item.divisionId;
                        var htmlCity = '';
                        if (Province == "北京市" || Province == "天津市" || Province == "上海市" || Province == "重庆市") {
                            $("#city").append("<option value='" + Province + "' exid='" + divisionId + "'>" + Province + "</option>");
                            $('#city').val(City);
                            var htmlArea = '';
                            $.ajax({
                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                contentType: "application/json",
                                type: "get",
                                success: function (data) {
                                    $.each(data, function (idx, item) {
                                        htmlArea += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                    });
                                    $("#area").append(htmlArea);
                                    $('#area').val(Area);
                                }
                            });

                        } else {
                            $.ajax({
                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                contentType: "application/json",
                                type: "get",
                                success: function (data) {
                                    $.each(data, function (idx, item) {
                                        if (item.divisionName == City) {
                                            var divisionId = item.divisionId;
                                            var htmlArea = '';
                                            $.ajax({
                                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                                contentType: "application/json",
                                                type: "get",
                                                success: function (data) {
                                                    $.each(data, function (idx, item) {
                                                        htmlArea += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                                    });
                                                    $("#area").append(htmlArea);
                                                    $('#area').val(Area);
                                                }
                                            });
                                        }
                                        htmlCity += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                    });
                                    $("#city").append(htmlCity);
                                    $('#city').val(City);
                                }
                            });
                        }
                    }
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#province").append(html);
                $('#province').val(Province);
            }
        });
        $("#province").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#city option").remove();
            $("#city").append(html);
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#city").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#city").append(html);
                }
            });
        });
        $("#city").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#area").append(html);
                }
            });
        });
    };

	var initSearchBlock = function(){
        SearchBlock.init("searchBlock");
	};
	var queryBtnBind = function(){
        $("#btnSearch").click(function () {
            createHddcDResultmaptableGrid();
        });
        $("#btnReset").click(function () {
            $("#provinceSelect").val("");
	            $("#citySelect").val("");
	            $("#areaSelect").val("");
				$("#citynameCondition").val("");
				$("#filenameCondition").val("");
				$("#idCondition").val("");
				$("#authorsCondition").val("");
			});
	};
	var createHddcDResultmaptableGrid= function() {
	    var tipString = '';
	    // pageType = 3  成果图件表-属性表管理
	    if (pageType == 3) {
            tipString = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp*不要在此页面上传1:5万活动断层分布图、1:25万地震构造图相关成果数据";
        }
        $("#hddcDResultmaptableGrid").datagrid({
            url:getHddcDResultmaptableController() + "/queryHddcDResultmaptables",
			method:"GET",
            fitColumns: true,
            autoRowHeight: false,
            columns:[[
                {field:"ck",checkbox:true},
				{
					field:'id',sortable:true,
					title:'编号',
					width:'20%',
					align:'center',
					formatter:function(value,rowData,rowIndex){
								return '<a href="#" onclick="_editHddcDResultmaptable(\'' + rowData.uuid + '\');"> '+rowData.id+' </a> '
					}
				},
				{
					field:'province',sortable:true,
					title:'省',
					width:'20%',
					align:'center',
				},
				{
					field:'city',sortable:true,
					title:'市',
					width:'20%',
					align:'center',
				},
				{
					field:'area',sortable:true,
					title:'区（县）',
					width:'20%',
					align:'center',
				},
				{
					field:'cityname',
					title:'探测城市',
					width:'20%',
					align:'center',
				},
				{
					field:'filename',
					title:'名称',
					width:'20%',
					align:'center',
				},
				{
					field:'authors',
					title:'上传者',
					width:'20%',
					align:'center',
				},
				{
					field:'updateTime',
					title:'上传日期',
                    sortable:true,
					width:'20%',
					align:'center',
                    // formatter:function(value,rowData,rowIndex){
					//     debugger
                    //     if (rowData.updateTime == null || rowData.updateTime == '')
                    //         return rowData.createTime;
                    //     else
                    //         return rowData.updateTime;
                    // }
				},
            ]],
            toolbar: [{
                iconCls: 'fa fa-plus-circle',
                text:"添加",
                handler: function(){
                    addHddcDResultmaptable();
                }
            },{
                iconCls: 'fa fa-trash-o',
                text:"删除",
                handler: function(){
                    deleteHddcDResultmaptable();
                }
            },
                {
                    text:tipString,
                }],
            queryParams:{
                province: $("#provinceSelect").val(),
                city: $("#citySelect").val(),
                area: $("#areaSelect").val(),
				cityname: $("#citynameCondition").val(),
				filename: $("#filenameCondition").val(),
				id: $("#idCondition").val(),
				authors: $("#authorsCondition").val(),
                pageType:pageType
            },
            pagination: true,
            pageSize: 10
        });
    };
	var formValidator = function(){
		$("#hddcDResultmaptableForm").validate({
			rules : {
				province : {
					required : true,
				},
				city : {
					//required : true,
				},
				area : {
					//required : true,
				},
				id : {
					required : true,
				},
				cityname : {
					required : true,
				},
				filename : {
					required : true,
				},
			},
			messages : {
				province : {
					required : "省不允许为空!",
				},
				city : {
					required : "市不允许为空!",
				},
				area : {
					//required : "区（县）不允许为空!",
				},
				id : {
					required : "编号不允许为空!",
				},
				cityname : {
					required : "探测城市不允许为空!",
				},
				filename : {
					required : "名称不允许为空!",
				},
			}
		});
	};
	var getHddcDResultmaptable = function(id){
		$.ajax({
			url: getHddcDResultmaptableController() + "/"+id,
			type: "get",
			success: function (data) {
                editProvince(data.province, data.city, data.area);
                // 回显项目名称
                // $("#projectName").val(data.projectName);
                // 回显任务名称
                // HddccjCommon.initEditTaskSelect("taskName",data.projectName);
                HddccjCommon.initEditTaskSelect("taskName",data.projectCode);
                data.projectName = data.projectCode;
                data.taskName = data.taskId;
                //$("#taskName").val(data.taskName);
				// 回显附件
                viewFileUpload(data.attachid);
                if(data.authors == '' || data.authors == null){
                    $.ajax({
                        url: getServer() + "/sys/getLoginUsers",
                        contentType:"application/json",
                        type: "get",
                        success: function (data1) {
                            data.authors = data1;
                            Tool.deserialize("hddcDResultmaptableForm", data);
                        }
                    });

                }else{

                    Tool.deserialize("hddcDResultmaptableForm", data);
                }
			}
		});
	};

    var FILE_TYPES = {
        image: {
            title: '图片文件',
            extensions: 'jpg,jpeg,bmp,png,ico',
            mimeTypes: 'image/bmp,image/jpeg,image/png,image/x-icon'
        },
        imagePdfVideo: {
            title: '图片文件',
            extensions: 'jpg,jpeg,bmp,png,ico,pdf,mp4,avi,mov,mpg',
            mimeTypes: 'image/bmp,image/jpeg,image/png,image/x-icon,application/pdf,video/mpeg,video/quicktime,video/x-msvideo,application/vnd.rn-realmedia-vbr,video/mp4'
        },
        doc: {
            title: '文档文件',
            extensions: 'doc,docx,xls,xlsx,ppt,pptx,pdf',
            mimeTypes: 'application/vnd.ms-excel.12,application/vnd.ms-word.document.12,application/vnd.ms-powerpoint.12,' +
                'application/pdf,application/vnd.ms-excel,application/vnd.ms-powerpoint,application/msword'
        },
        video: {
            title: '视频文件',
            extensions: 'mp4,avi,mov,mpg,doc,docx,xls,xlsx,ppt,pptx,pdf,jpg,jpeg,bmp,png,ico',
            mimeTypes: 'video/mpeg,video/quicktime,video/x-msvideo,'
                + 'application/vnd.rn-realmedia-vbr,video/mp4' + 'application/vnd.ms-excel.12,application/vnd.ms-word.document.12,application/vnd.ms-powerpoint.12,' +
                'application/pdf,application/vnd.ms-excel,application/vnd.ms-powerpoint,application/msword' + 'image/bmp,image/jpeg,image/png,image/x-icon'
        },
        audio: {
            title: '音频文件',
            extensions: 'wma,mp3,wav',
            mimeTypes: 'audio/x-ms-wma,audio/mpeg,audio/x-wav'
        },
        packet: {
            title: '压缩包',
            extensions: 'zip,rar',
            mimeTypes: 'application/zip,application/x-rar-compressed'
        },
        packet: {
            title: '压缩包和图片',
            extensions: 'zip,rar,png',
            mimeTypes: 'application/zip,application/x-rar-compressed,image/png'
        },
        // nothing:{
        //
        // }
    };

    var viewFileUpload = function (fileData) {
        require(['UtilDir/fileupload/multiFileUpload'], function (MFU) {
            var accept = {};
            if(pageType<3) accept = FILE_TYPES.packet;
            var attachIdData = "";
            var attachId = "";
            var fileArray = "";
            if(fileData == null){
                fileData = "";
            }
            if (fileData.length != 0) {
                fileArray = fileData.split(',');
                attachIdData = fileData.split(',');
            }
            var settings = {
                placeAt: "multiFileUpload",
                allowMC:false,
                fileSizeLimit: 1024*1024*1024,
                accept:accept,
                /*
	            1.0.2版本新增
	            extendColumn:[
	                {name:"文件分类",filed:"category",format:function(file){return file.name}},
	                {name:"备注",filed:"remark",format:function(file){return '机密'}}
	            ],*/
                data: attachIdData,
                onUploadSuccess: function (file) {
                    attachId += file.attachId + ",";
                    if (fileData.length == 0) {
                        $("#attachid").val(attachId.substr(0, attachId.length - 1));
                    } else {
                        $("#attachid").val(fileData + "," + attachId.substr(0, attachId.length - 1));
                    }

                },
                onDeleteSavedFile: function (file) {
                    var index = fileArray.indexOf(file.attachId);
                    fileArray.splice(index, 1);
                    $("#attachid").val(fileArray.toString());
                }
            };
            var MFUpload = MFU.init(settings);

        });
    };

    var fileUpload = function () {
        require(['UtilDir/fileupload/multiFileUpload'], function (MFU) {
            var accept = {};
            if(pageType<3) accept = FILE_TYPES.packet;
            var attachId = "";
            var settings = {
                placeAt: "multiFileUpload",
                allowMC:false,
                fileSizeLimit: 200*1024*1024,
                accept:accept,
                /*
	            1.0.2版本新增
	            extendColumn:[
	                {name:"文件分类",filed:"category",format:function(file){return file.name}},
	                {name:"备注",filed:"remark",format:function(file){return '机密'}}
	            ],*/
                //data:["5a50d697c0af806aa01e06120d04f977","6c01e4492f264befab4c0532811f8dff"],
                onUploadSuccess: function (file) {
                    attachId += file.attachId + ",";
                    $("#attachid").val(attachId.substr(0, attachId.length - 1));
                },
                onDeleteSavedFile: function (file) {
                    //console.log(file)
                }
            };
            var MFUpload = MFU.init(settings);

        });
    };

	var addHddcDResultmaptable = function () {
		var slidebar = Util.slidebar({
			url: hddcDResultmaptableFormHtml,
			width: "800px",
			cache: false,
			close : true,
			afterLoad: function () {
                initcreateProvinceForm();
                fileUpload();
                initDatetimePicker();
                HddccjCommon.initProjectSelect("projectName"); //添加页面项目名称下拉
                HddccjCommon.initTaskSelect("projectName", "taskName"); //添加页面任务名称根据项目名称切换
                formValidator();
                var title = $("#formTitle");
                var fileTips = $("#fileTips");
                // if(pageType == 1){
                //     title.text("1:5万活动断层分布图");
                // }else if(pageType == 2){
                //     title.text("1:25万地震构造图编制");
                // }else if(pageType == 3){
                //     title.text("成果图件表-属相表管理");
                // }else if(pageType == 4){
                //     title.text("成果图件表-属相表管理");
                // }else if(pageType == 5){
                //     title.text("成果图件表-属相表管理");
                // }else if(pageType == 3){
                //     title.text("成果图件表-属相表管理");
                // }else if(pageType == 3){
                //     title.text("成果图件表-属相表管理");
                // }
                switch (pageType) {
                    case '1':title.text("1:5万活动断层分布图");break;
                    case '2':title.text("1:25万地震构造图编制");break;
                    case '3':title.text("成果图件表-属性表管理");fileTips.css("display","none");break;
                    case '4':title.text("1:5万活动断层分布图说明书");fileTips.css("display","none");break;
                    case '5':title.text("1:5万活动断层分布图成果报告");fileTips.css("display","none");break;
                    case '6':title.text("1:25万地震构造图说明书");fileTips.css("display","none");break;
                    case '7':title.text("1:25万地震构造图成果报告");fileTips.css("display","none");break;

                }
                $.ajax({ //上传者
                    url: getServer() + "/sys/getLoginUsers",
                    contentType:"application/json",
                    type: "get",
                    success: function (data1) {
                        $("#authors").val(data1)
                    }
                });
				$("#saveBtn").on("click", function () {
					if($("#hddcDResultmaptableForm").valid()){
						var data = Tool.serialize("hddcDResultmaptableForm");
                        data.projectCode = $("#projectName").val();
                        data.taskId = $("#taskName").val();
                        data.extends20 = pageType;

                        if (data.attachid == '' || data.attachid == null){
                            Util.alert('请上传附件！');
                            return;
                        }

                        if($("#authors").val()=="" || $("#authors").val()==null){
                            $.ajax({
                                url: getServer() + "/sys/getLoginUsers",
                                contentType:"application/json",
                                type: "get",
                                success: function (data1) {
                                    data.authors = data1;
                                    $.ajax({
                                        url: getHddcDResultmaptableController(),
                                        contentType:"application/json",
                                        data: JSON.stringify(data),
                                        type: "post",
                                        success: function (data) {
                                            Util.alert(data.message);
                                            slidebar.close();
                                            createHddcDResultmaptableGrid();
                                        }
                                    });
                                }
                            });
                        }else{
                            debugger;
                            $.ajax({
                                url: getHddcDResultmaptableController(),
                                contentType:"application/json",
                                data: JSON.stringify(data),
                                type: "post",
                                success: function (data) {
                                    Util.alert(data.message);
                                    slidebar.close();
                                    createHddcDResultmaptableGrid();
                                }
                            });
                        }


					}
				});
			}
		});
	};
	window._editHddcDResultmaptable = function(hddcDResultmaptableId) {
		var slidebar = Util.slidebar({
			url: hddcDResultmaptableFormHtml,
			width: "800px",
			cache: false,
			close : true,
			afterLoad: function () {
				formValidator();
                initDatetimePicker();
                HddccjCommon.initProjectSelect("projectName"); //编辑页面项目名称下拉
                HddccjCommon.initTaskSelect("projectName","taskName"); //编辑页面任务名称根据项目名称切换
                getHddcDResultmaptable(hddcDResultmaptableId);
                var title = $("#formTitle");
                var fileTips = $("#fileTips");
                switch (pageType) {
                    case '1':title.text("1:5万活动断层分布图");break;
                    case '2':title.text("1:25万地震构造图编制");break;
                    case '3':title.text("成果图件表-属性表管理");fileTips.css("display","none");break;
                    case '4':title.text("1:5万活动断层分布图说明书");fileTips.css("display","none");break;
                    case '5':title.text("1:5万活动断层分布图成果报告");fileTips.css("display","none");break;
                    case '6':title.text("1:25万地震构造图说明书");fileTips.css("display","none");break;
                    case '7':title.text("1:25万地震构造图成果报告");fileTips.css("display","none");break;

                }
				$("#saveBtn").on("click", function () {
					if($("#hddcDResultmaptableForm").valid()){
						var data = Tool.serialize("hddcDResultmaptableForm");
                        data.projectCode = $("#projectName").val();
                        data.taskId = $("#taskName").val();
                        data.extends20 = pageType;

                        if (data.attachid == '' || data.attachid == null){
                            Util.alert('请上传附件！');
                            return;
                        }

                        if($("#authors").val()=="" || $("#authors").val()==null){
                            $.ajax({
                                url: getServer() + "/sys/getLoginUsers",
                                contentType:"application/json",
                                type: "get",
                                success: function (data1) {
                                    data.authors = data1;
                                    $.ajax({
                                        url: getHddcDResultmaptableController(),
                                        contentType:"application/json",
                                        data: JSON.stringify(data),
                                        type: "put",
                                        success: function (data) {
                                            Util.alert(data.message);
                                            slidebar.close();
                                            createHddcDResultmaptableGrid();
                                        }
                                    });
                                }
                            });
                        }else{
                            debugger;
                            $.ajax({
                                url: getHddcDResultmaptableController(),
                                contentType:"application/json",
                                data: JSON.stringify(data),
                                type: "put",
                                success: function (data) {
                                    Util.alert(data.message);
                                    slidebar.close();
                                    createHddcDResultmaptableGrid();
                                }
                            });
                        }

					}
				});
			}
		});
	};
	var deleteHddcDResultmaptable = function() {
		var rows = $("#hddcDResultmaptableGrid").datagrid("getSelections");
		if (rows == null || rows.length == 0) {
			Util.alert("请选择一行数据!");
			return;
		}
		Util.confirm("是否要删除选中的数据?", function() {
			var ids = "";
			$.each(rows, function(i, row){
				ids += row.uuid + ",";

			});
			ids = ids.substr(0,ids.length - 1);
			$.ajax({
				url: getHddcDResultmaptableController(),
                data: {
                    ids : ids
                },
				type: "delete",
				success: function (data) {
					createHddcDResultmaptableGrid();
				}
			});
		}, function() {
			return;
		});

	};
    var initDatetimePicker = function () {
        $("#finishedtime").datetimepicker({
            //设置使用语言：cn是自定义的中文版本，还可以扩展其他语言版本
            language: "cn",
            //输出格式化
            format: 'yyyy-mm-dd',
            //直接选择‘今天’
            todayBtn: true,
            //设置最精确的时间选择视图
            minView: 'month',
            //高亮当天日期
            todayHighlight: true,
            //选择完毕后自动关闭
            autoclose: true
        });

        $("#publishdate").datetimepicker({
            //设置使用语言：cn是自定义的中文版本，还可以扩展其他语言版本
            language: "cn",
            //输出格式化
            format: 'yyyy-mm-dd',
            //直接选择‘今天’
            todayBtn: true,
            //设置最精确的时间选择视图
            minView: 'month',
            //高亮当天日期
            todayHighlight: true,
            //选择完毕后自动关闭
            autoclose: true
        });
    }
	return {
		init:init
	};
});
