define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcDrillProfile/hddcDrillprofileSupport"],function(hddcDrillprofileSupport){
            hddcDrillprofileSupport.init();
        });
    };
});