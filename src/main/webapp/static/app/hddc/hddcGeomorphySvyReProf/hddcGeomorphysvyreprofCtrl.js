define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGeomorphySvyReProf/hddcGeomorphysvyreprofSupport"],function(hddcGeomorphysvyreprofSupport){
            hddcGeomorphysvyreprofSupport.init();
        });
    };
});