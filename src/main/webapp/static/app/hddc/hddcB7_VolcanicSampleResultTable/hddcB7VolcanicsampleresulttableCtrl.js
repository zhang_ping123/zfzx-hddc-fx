define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcB7_VolcanicSampleResultTable/hddcB7VolcanicsampleresulttableSupport"],function(hddcB7VolcanicsampleresulttableSupport){
            hddcB7VolcanicsampleresulttableSupport.init();
        });
    };
});