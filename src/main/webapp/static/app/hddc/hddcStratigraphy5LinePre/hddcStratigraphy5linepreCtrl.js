define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcStratigraphy5LinePre/hddcStratigraphy5linepreSupport"],function(hddcStratigraphy5linepreSupport){
            hddcStratigraphy5linepreSupport.init();
        });
    };
});