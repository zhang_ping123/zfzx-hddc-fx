define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcGeochemicalAbnSegment/hddcGeochemicalabnsegmentSupport"],function(hddcGeochemicalabnsegmentSupport){
            hddcGeochemicalabnsegmentSupport.init();
        });
    };
});