define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcDrillHole/hddcDrillholeSupport"],function(hddcDrillholeSupport){
            hddcDrillholeSupport.init();
        });
    };
});