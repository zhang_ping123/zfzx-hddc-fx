define([
    "UtilDir/util",
    "UtilDir/tool",
    "UtilDir/org/selectOrg",
    "UtilDir/searchBlock",
    "static/app/hddc/hddccjcommon/hddccjCommon",
    "Date", "DateCN", "css!DateCss",
    "EasyUI", "EasyUI-lang"
], function (Util, Tool, SelectOrg, SearchBlock, HddccjCommon) {

    var sysPath = getServer() + "/static/app/hddc/hddcDrillHole";

    var hddcDrillholeFormHtml = sysPath + "/views/hddcDrillholeForm.html";
    var getHddcDrillholeController = function () {
        return getServer() + "/hddc/hddcDrillholes";
    };

    /**
     * 页面初始化
     */
    var init = function () {
        initSearchBlock();
        queryBtnBind();
        initcreateProvince();
        HddccjCommon.initProjectSelect("projectNameCondition"); //初始化查询的项目名称
        createHddcDrillholeGrid();
    };
    var initcreateProvince = function () {
        var html = "";
        $("#citySelect").append(html);
        $("#areaSelect").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#provinceSelect").append(html);
            }
        });
        $("#provinceSelect").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#citySelect option").remove();
            $("#citySelect").append(html);
            $("#areaSelect option").remove();
            $("#areaSelect").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#citySelect").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#citySelect").append(html);
                }
            });
        });
        $("#citySelect").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#areaSelect option").remove();
            $("#areaSelect").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#areaSelect").append(html);
                }
            });
        });
    }
    var initcreateProvinceForm = function () {
        var html = "";
        $("#city").append(html);
        $("#area").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#province").append(html);
            }
        });
        $("#province").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#city option").remove();
            $("#city").append(html);
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#city").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#city").append(html);
                }
            });
        });
        $("#city").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#area").append(html);
                }
            });
        });
    };
    var initDrillsourceSelectFrom = function () {
        $.ajax({
            url: getHddcDrillholeController() + "/getValidDictItemsByDictCode/" + "DrillTypeCVD",
            type: "get",
            success: function (data) {
                var mySelect = document.getElementById("drillsource");
                mySelect.add(new Option("请选择", ''), 0);
                for (var i = 0; i < data.length; i++) {
                    var name = data[i].dictItemName;
                    var code = data[i].dictItemCode;
                    mySelect.add(new Option(name, code));
                }
            }
        });
    };
    var initIsgeophywellSelectFrom = function () {
        $.ajax({
            url: getHddcDrillholeController() + "/getValidDictItemsByDictCode/" + "truefalse",
            type: "get",
            success: function (data) {
                var mySelect = document.getElementById("isgeophywell");
                mySelect.add(new Option("请选择", ''), 0);
                for (var i = 0; i < data.length; i++) {
                    var name = data[i].dictItemName;
                    var code = data[i].dictItemCode;
                    mySelect.add(new Option(name, code));
                }
            }
        });
    };

    var initSearchBlock = function () {
        SearchBlock.init("searchBlock");
    };
    var queryBtnBind = function () {
        $("#btnSearch").click(function () {
            createHddcDrillholeGrid();
        });
        $("#btnReset").click(function () {
            $("#provinceSelect").val("");
            $("#citySelect").val("");
            $("#areaSelect").val("");
            $("#projectNameCondition").val("");
            $("#locationnameCondition").val("");
        });
    };
    //导入
    var importForm = function () {

        $("#uploadModal").modal();
        $("#uploadModal").on("shown.bs.modal", function () {
            var url = getServer() + "/excel/钻孔-点.xls";
            $("#downloadZwExcelTemplate").attr("href", url);
        });

        $('#uploadButton').off("click");
        $("#uploadButton").on("click", function () {
            var uploadinput = document.getElementById("uploadFile");
            if (uploadinput.value == "") {
                Util.alert("上传前请先选择文件!");
                return;
            }
            var formData = new FormData();
            formData.append("file", uploadinput.files[0]);
            $.ajax({
                url: getHddcDrillholeController() + "/importDisaster",
                data: formData,
                processData: false, //因为data值是FormData对象，不需要对数据做处理。
                contentType: false,
                type: "POST",
                success: function (data) {
                    $('#uploadModal').modal('hide');
                    uploadinput.value = null;
                    Util.alert(data);
                    createHddcDrillholeGrid();
                }
            });
        });
    };
    //导出
    var exportForm = function () {
        var province = $("#provinceSelect").val();
        var city = $("#citySelect").val();
        var area = $("#areaSelect").val();
        var projectName = $("#projectNameCondition").val();
        var locationname = $("#locationnameCondition").val();
        window.location.href = getHddcDrillholeController() + "/exportFile?province=" + province + "&city=" + city + "&area=" + area + "&projectName=" + projectName + "&locationname=" + locationname;
    };
    var createHddcDrillholeGrid = function () {
        $("#hddcDrillholeGrid").datagrid({
            url: getHddcDrillholeController() + "/queryHddcDrillholes",
            method: "GET",
            fitColumns: true,
            autoRowHeight: false,
            columns: [[
                {field: "ck", checkbox: true},
                {
                    field: 'id',
                    title: '钻孔编号',
                    width: '10%',
                    align: 'center',
                    formatter: function (value, rowData, rowIndex) {
                        return '<a href="#" onclick="_editHddcDrillhole(\'' + rowData.uuid + '\');"> ' + rowData.id + ' </a> '
                    }
                },
                {
                    field: 'province',
                    title: '省',
                    width: '10%',
                    align: 'center',
                },
                {
                    field: 'city',
                    title: '市',
                    width: '10%',
                    align: 'center',
                },
                {
                    field: 'area',
                    title: '区（县）',
                    width: '10%',
                    align: 'center',
                },
                {
                    field: 'projectName',
                    title: '项目名称',
                    width: '10%',
                    align: 'center',
                },
                {
                    field: 'taskName',
                    title: '任务名称',
                    width: '10%',
                    align: 'center',
                },
                {
                    field: 'profileid',
                    title: '钻孔剖面编号',
                    width: '10%',
                    align: 'center',
                },
                {
                    field: 'purpose',
                    title: '钻探目的',
                    width: '10%',
                    align: 'center',
                },
                {
                    field: 'drilldate',
                    title: '钻探日期',
                    width: '10%',
                    align: 'center',
                },
                {
                    field: 'fieldid',
                    title: '野外编号',
                    width: '10%',
                    align: 'center',
                },
                {
                    field: 'locationname',
                    title: '钻探地点',
                    width: '10%',
                    align: 'center',
                },
            ]],
            toolbar: [{
                iconCls: 'fa fa-plus-circle',
                text: "添加",
                handler: function () {
                    addHddcDrillhole();
                }
            }, {
                iconCls: 'fa fa-trash-o',
                text: "删除",
                handler: function () {
                    deleteHddcDrillhole();
                }
            }, {
                iconCls: 'fa fa-upload',
                text: "导入",
                handler: function () {
                    importForm();
                }
            }, {
                iconCls: 'fa fa-download',
                text: "导出",
                handler: function () {
                    exportForm();
                }
            }],
            queryParams: {
                province: $("#provinceSelect").val(),
                city: $("#citySelect").val(),
                area: $("#areaSelect").val(),
                projectName: $("#projectNameCondition").val(),
                locationname: $("#locationnameCondition").val(),
            },
            pagination: true,
            pageSize: 10
        });
    };
    var formValidator = function () {
        $("#hddcDrillholeForm").validate({
            rules: {
                province: {
                    required: true,
                },
                city: {
                    required: true,
                },
                area: {
                    required: true,
                },
                projectName: {
                    required: true,
                },
                locationname: {
                    required: true,
                },
                lon: {
                    range:[-180,180],
                    required: true,
                    number :true,
                    maxlength: 8,
                },
                lat: {
                    range:[-90,90],
                    required: true,
                    number :true,
                    maxlength: 8,
                },
                datingsamplecount: {
                    required: true,
                },
                samplecount: {
                    required: true,
                },
                purpose: {
                    required: true,
                },
                drilldate: {
                    required: true,
                },
                elevation: {
                    required: true,
                    number :true,
                    maxlength: 8,
                },
                depth: {
                    required: true,
                    number :true,
                    maxlength: 8,
                },
                coretotalthickness: {
                    required: true,
                    number :true,
                    maxlength: 8,
                },
                columnchartAiid: {
                    required: true,
                },
                columnchartArwid: {
                    required: true,
                },
                fieldid: {
                    required: true,
                },
                testedenviromentsamplecount: {
                    required: true,
                },
                enviromentsamplecount: {
                    required: true,
                },
                isgeophywell: {
                    required: true,
                },
                corephotoArwid: {
                    required: true,
                },
                drillsource : {
                    maxlength :4,
                    digits :true,
                },
                holocenethickness : {
                    number :true,
                    maxlength: 8,
                },
                uppleithickness : {
                    number :true,
                    maxlength: 8,
                },
                midpleithickness : {
                    number :true,
                    maxlength: 8,
                },
                lowpleithickness : {
                    number :true,
                    maxlength: 8,
                },
                prepleithickness  : {
                    number :true,
                    maxlength: 8,
                },
                collectedsamplecount : {
                    maxlength :4,
                    digits :true,
                },
                samplecount : {
                    maxlength :4,
                    digits :true,
                },
                datingsamplecount : {
                    maxlength :4,
                    digits :true,
                },
                collectedenviromentsamplecount : {
                    maxlength :4,
                    digits :true,
                },
                enviromentsamplecount : {
                    required :true,
                    maxlength :4,
                    digits :true,
                },
                testedenviromentsamplecount : {
                    required :true,
                    maxlength :4,
                    digits :true,
                },
                id: {
                    required : true,
                },
            },
            messages: {
                province: {
                    required: "省不允许为空!",
                },
                city: {
                    required: "市不允许为空!",
                },
                area: {
                    required: "区（县）不允许为空!",
                },
                projectName: {
                    required: "项目名称不允许为空!",
                },
                locationname: {
                    required: "钻探地点不允许为空!",
                },
                lon: {
                    required: "孔位经度长度不大于8的浮点数!",
                },
                lat: {
                    required: "孔位纬度长度不大于8的浮点数!",
                },
                datingsamplecount: {
                    required: "获得结果样品总数不允许为空!",
                },
                samplecount: {
                    required: "送样总数不允许为空!",
                },
                purpose: {
                    required: "钻探目的不允许为空!",
                },
                drilldate: {
                    required: "钻探日期不允许为空!",
                },
                elevation: {
                    required: "孔口标高 [米]长度不大于8的浮点数!",
                },
                depth: {
                    required: "孔深 [米]长度不大于8的浮点数!",
                },
                coretotalthickness: {
                    required: "岩芯总长 [米]长度不大于8的浮点数!",
                },
                columnchartAiid: {
                    required: "钻孔柱状图图像文件编号不允许为空!",
                },
                columnchartArwid: {
                    required: "钻孔柱状图原始档案编号不允许为空!",
                },
                fieldid: {
                    required: "野外编号不允许为空!",
                },
                testedenviromentsamplecount: {
                    required: "获得测试结果的环境与工程样品数不允许为空!",
                },
                enviromentsamplecount: {
                    required: "环境与工程样品送样总数不允许为空!",
                },
                isgeophywell: {
                    required: "是否开展地球物理测井不允许为空!",
                },
                corephotoArwid: {
                    required: "岩芯照片原始档案编号不允许为空!",
                },
                drillsource : {
                    required: "钻孔来源与类型长度不大于4的整数!",
                },
                holocenethickness : {
                    required: "全新统厚度 [米]长度不大于8的浮点数!",
                },
                uppleithickness : {
                    required: "上更新统厚度 [米]长度不大于8的浮点数!",
                },
                midpleithickness : {
                    required: "中更新统厚度 [米]长度不大于8的浮点数!",
                },
                lowpleithickness : {
                    required: "下更新统厚度 [米]长度不大于8的浮点数!",
                },
                prepleithickness : {
                    required: "前第四纪厚度 [米]长度不大于8的浮点数!",
                },
                collectedsamplecount : {
                    required: "采集样品总数长度不大于4的整数!",
                },
                samplecount : {
                    required: "送样总数长度不大于4的整数!",
                },
                datingsamplecount : {
                    required: "获得结果样品总数长度不大于4的整数!",
                },
                collectedenviromentsamplecount : {
                    required: "采集环境与工程样品数长度不大于4的整数!",
                },
                enviromentsamplecount : {
                    required: "环境与工程样品送样总数长度不大于4的整数!",
                },
                testedenviromentsamplecount : {
                    required: "获得测试结果的环境与工程样品数长度不大于4的整数!",
                },
                id : {
                    required : "钻孔编号不允许为空!",
                },
            }
        });
    };
    var getHddcDrillhole = function (id) {
        $.ajax({
            url: getHddcDrillholeController() + "/" + id,
            type: "get",
            success: function (data) {
                debugger;
                editProvince(data.province, data.city, data.area);
                // 回显项目名称
                $("#projectName").val(data.projectName);
                // 回显任务名称
                HddccjCommon.initEditTaskSelect("taskName",data.projectName);
                $("#taskName").val(data.taskName);
                Tool.deserialize("hddcDrillholeForm", data);
            }
        });
    };
    var editProvince = function (Province, City, Area) {
        debugger;
        var html = "";
        $("#city").append(html);
        $("#area").append(html);
        $.ajax({
            url: getServer() + "/divisions/root/subdivisions",
            contentType: "application/json",
            type: "get",
            success: function (data) {
                $.each(data, function (idx, item) {
                    if (item.divisionName == Province) {
                        debugger;
                        var divisionId = item.divisionId;
                        var htmlCity = '';
                        if (Province == "北京市" || Province == "天津市" || Province == "上海市" || Province == "重庆市") {
                            $("#city").append("<option value='" + Province + "' exid='" + divisionId + "'>" + Province + "</option>");
                            $('#city').val(City);
                            var htmlArea = '';
                            $.ajax({
                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                contentType: "application/json",
                                type: "get",
                                success: function (data) {
                                    $.each(data, function (idx, item) {
                                        htmlArea += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                    });
                                    $("#area").append(htmlArea);
                                    $('#area').val(Area);
                                }
                            });

                        } else {
                            $.ajax({
                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                contentType: "application/json",
                                type: "get",
                                success: function (data) {
                                    debugger;
                                    $.each(data, function (idx, item) {
                                        if (item.divisionName == City) {
                                            var divisionId = item.divisionId;
                                            var htmlArea = '';
                                            $.ajax({
                                                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                                                contentType: "application/json",
                                                type: "get",
                                                success: function (data) {
                                                    $.each(data, function (idx, item) {
                                                        htmlArea += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                                    });
                                                    $("#area").append(htmlArea);
                                                    $('#area').val(Area);
                                                }
                                            });
                                        }
                                        htmlCity += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                                    });
                                    $("#city").append(htmlCity);
                                    $('#city').val(City);
                                }
                            });
                        }
                    }
                    html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                });
                $("#province").append(html);
                $('#province').val(Province);
            }
        });
        $("#province").change(function () {
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#city option").remove();
            $("#city").append(html);
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            if ($(this).val() == "北京市" || $(this).val() == "天津市" || $(this).val() == "上海市" || $(this).val() == "重庆市") {
                $("#city").append("<option value='" + $(this).val() + "' exid='" + divisionId + "'>" + $(this).val() + "</option>");
                return;
            }
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#city").append(html);
                }
            });
        });
        $("#city").change(function () {
            if ($(this).val() == "") return;
            var html = '<option value="" disabled selected style="display:none;">请选择</option>';
            $("#area option").remove();
            $("#area").append(html);
            var divisionId = $(this).find("option:selected").attr("exid");
            $.ajax({
                url: getServer() + "/divisions/" + divisionId + "/subdivisions",
                contentType: "application/json",
                type: "get",
                success: function (data) {
                    $.each(data, function (idx, item) {
                        html += "<option value='" + item.divisionName + "' exid='" + item.divisionId + "'>" + item.divisionName + "</option>";
                    });
                    $("#area").append(html);
                }
            });
        });
    };
    var addHddcDrillhole = function () {
        var slidebar = Util.slidebar({
            url: hddcDrillholeFormHtml,
            width: "800px",
            cache: false,
            close: true,
            afterLoad: function () {
                initcreateProvinceForm();
                initDrillsourceSelectFrom();
                initIsgeophywellSelectFrom();
                HddccjCommon.initProjectSelect("projectName"); //添加页面项目名称下拉
                HddccjCommon.initTaskSelect("projectName", "taskName"); //添加页面任务名称根据项目名称切换
                formValidator();
                $("#saveBtn").on("click", function () {
                    if ($("#hddcDrillholeForm").valid()) {
                        var data = Tool.serialize("hddcDrillholeForm");
                        $.ajax({
                            url: getHddcDrillholeController(),
                            contentType: "application/json",
                            data: JSON.stringify(data),
                            type: "post",
                            success: function (data) {
                                Util.alert(data.message);
                                slidebar.close();
                                createHddcDrillholeGrid();
                            }
                        });
                    }
                });
            }
        });
    };
    window._editHddcDrillhole = function (hddcDrillholeId) {
        var slidebar = Util.slidebar({
            url: hddcDrillholeFormHtml,
            width: "800px",
            cache: false,
            close: true,
            afterLoad: function () {
                initDrillsourceSelectFrom();
                initIsgeophywellSelectFrom();
                formValidator();
                HddccjCommon.initProjectSelect("projectName"); //编辑页面项目名称下拉
                HddccjCommon.initTaskSelect("projectName","taskName"); //编辑页面任务名称根据项目名称切换
                getHddcDrillhole(hddcDrillholeId);
                $("#saveBtn").on("click", function () {
                    if ($("#hddcDrillholeForm").valid()) {
                        var data = Tool.serialize("hddcDrillholeForm");
                        $.ajax({
                            url: getHddcDrillholeController(),
                            contentType: "application/json",
                            data: JSON.stringify(data),
                            type: "put",
                            success: function (data) {
                                Util.alert(data.message);
                                slidebar.close();
                                createHddcDrillholeGrid();
                            }
                        });
                    }
                });
            }
        });
    };
    var deleteHddcDrillhole = function () {
        var rows = $("#hddcDrillholeGrid").datagrid("getSelections");
        if (rows == null || rows.length == 0) {
            Util.alert("请选择一行数据!");
            return;
        }
        Util.confirm("是否要删除选中的数据?", function () {
            var ids = "";
            $.each(rows, function (i, row) {
                ids += row.uuid + ",";

            });
            ids = ids.substr(0, ids.length - 1);
            $.ajax({
                url: getHddcDrillholeController(),
                data: {
                    ids: ids
                },
                type: "delete",
                success: function (data) {
                    createHddcDrillholeGrid();
                }
            });
        }, function () {
            return;
        });

    };

    return {
        init: init
    };
});
