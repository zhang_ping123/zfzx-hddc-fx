define(["jquery"],function($){
    return function($compile,$scope){
        require(["static/app/hddc/hddcDrillFaultPoint/hddcDrillfaultpointSupport"],function(hddcDrillfaultpointSupport){
            hddcDrillfaultpointSupport.init();
        });
    };
});