/**
 * Created by lenovo on 2014/10/30.
 */
define(function(){
    //subhome.jsp依赖验证组件，而home.jsp不依赖
    /*if(typeof( $.validator)=="function"){
        //自定义密码验证规则
        $.validator.addMethod('PD_password', function (value, element) {
            var len = value.length;
            if(len<6){
                $(element).data('error-msg','长度不能少于6位');
                return false;
            }
            if(len>15){
                $(element).data('error-msg','长度不能大于15位');
                return false;
            }
            return true;
        }, function(params, element) {
            return $(element).data('error-msg');
        });
    }*/
    //项目中的全局配置
    require.config({
        paths : {
            "FwDir":"static/app/zdyh",
            "UtilDir":"static/core/platform/components/util",
            "ZTree":"static/core/platform/lib/zTree/js/jquery.ztree.all-3.5",
        	"ZTreeExhide":"static/core/platform/lib/zTree/js/jquery.ztree.exhide-3.5.min",
    	 	"ZTreeCss":"static/core/platform/lib/zTree/css/zTreeStyle/csTreeStyle",
        },
        shim:{
	        "Bootstrap":["jquery"],
	        "EasyUI-lang":["EasyUI"],
	        "EasyUI":["jquery"],
	        "Angular":{"exports":"angular"},
	        "Angular-ui-router":["Angular"],
	        "ZTree":["jquery"],
	        "DateCN":["Date"],
	        //"JQuery.validate.extra":["JQuery.validate"],
	        //"JQuery.validate.message":["JQuery.validate"],
	        "Uploader":["WebUploader"],
	        "ZTreeExhide":["ZTree"]
	    },
	    //urlArgs:"v=0.9.2",
	    map:{
	        '*':{
	            'css':"static/core/platform/lib/requirejs/plugin/require-css2/css",
	            'text':"static/core/platform/lib/requirejs/plugin/text"
	        }
	    }
    });
});

